function [number_of_waves,number_of_BT,unsuccess_BT,WaveLocations,BTPositions,...
    VIB,total_number_of_waves] = calculateParameters_ver_6(vm_igb,tcc_igb)

tau = 10;
tcc_igb = '/home/marques/exec/marques/wave/exp700c00_tcc_afull.igb';


BT_temp = 0;
BT_unSucc_temp=0;
important_bt_temp=0;

%%%%%% Load anatomy and segment it %%%%%%%%
Anatomy = igbload(tcc_igb);
Anatomy = Anatomy(1:5:end,1:5:end,1:5:end);
Anatomy_binary_RA = (Anatomy == 113);
Anatomy_binary_LA = (Anatomy == 114);
Anatomy_binary_Fibers = (Anatomy == 116);
Anatomy_binary_BB = (Anatomy == 117);
Anatomy_binary_Fibrosis = (Anatomy == 250);
Anatomy_binary = Anatomy_binary_RA + Anatomy_binary_LA + ...
    Anatomy_binary_Fibers+Anatomy_binary_Fibrosis+Anatomy_binary_BB;

% Replace zeros with NaN. This is very important when you want to
% used bwlabeln command. If zeros are not replaced with NaN the
% function will detect them as a group of elements

Anatomy_binary (Anatomy_binary ~= 1)=0;
Anatomy_NaN = double(Anatomy_binary);
Anatomy_NaN (Anatomy_NaN ~= 1) = NaN;

% This section is to separate the epicardial surface (every element which
% doesnot belong to endocardial bundles) and endocardial surface

[xx,yy,zz] = ind2sub(size(Anatomy_binary), find(Anatomy_binary));
coordinates = [xx,yy,zz];
Anatomy_binary_epicardiumSurface = (Anatomy ==114 | Anatomy == 113 | ...
    Anatomy == 116 |Anatomy == 117 |Anatomy == 119 | Anatomy == 250);

Anatomy_binary_endocardiumSurface = (Anatomy == 115);

Anatomy_binary_epicardiumSurface = double(Anatomy_binary_epicardiumSurface);
Anatomy_binary_epi = double(Anatomy_binary_epicardiumSurface);
Anatomy_binary_endo = double(Anatomy_binary_endocardiumSurface);

% create an anatomy only for visualization perpuose 
% Visulaization was done only to visualy inspect the outcome of wave and BT
% detections. Normally is not be used.
Anatomy_binary_show = (Anatomy >= 113);
Anatomy_binary_show = double(Anatomy_binary_show);
Anatomy_binary_epi (Anatomy_binary_epi ~= 1) = NaN;
Anatomy_binary_endo (Anatomy_binary_endo ~= 1) = NaN;
Anatomy_binary_show (Anatomy_binary_show ~= 1) = NaN;



map_temp = colormap();
map_temp(1,:) = [0 0.1 0.1];

% Loop over sim time to 
vm_igb = '/scratch/marques/wave/exp906c74_2_5.igb'; % 
WaveLocationsFile = fopen('/home/marques/exec/marques/wave/wave_locations_100ms.txt','wb');
BTPositionsFile = fopen('/home/marques/exec/marques/wave/bt_locations_100ms.txt','wb');

for T=1:1:2500
    T
    V  = igbload(vm_igb, 0,0,0, T);
    V_tau = igbload(vm_igb, 0,0,0, T+tau);
    
    % Wave detection algorithm ...
    normalized_verified_Voltage_3D(:,:,:) = ...
        (V - min(min(min(V))))./(max(max(max(V))) - min(min(min(V)))); % Normalizing the Vm between 0 and 1. 
                                                                       %This increases the precision for blabeln function.
                                                                       
    normalized_verified_Voltage_3D_tau(:,:,:) =...
        (V_tau - min(min(min(V_tau))))./(max(max(max(V_tau))) - min(min(min(V_tau)))); % V_tau will be used later to check the size of deteted BTs. 
                                                                                       % This needs to be done to check whether a BT is propagated successfuly or not.
                                                                                       % If it was not propagated will be categorized as unsuccessful BT.
    
    normalaized_verified_Voltage_3D_epi = normalized_verified_Voltage_3D.*Anatomy_binary_epi;
    normalaized_verified_Voltage_3D_epi_tau = normalized_verified_Voltage_3D_tau.*Anatomy_binary_epi;
    normalaized_verified_Voltage_3D_endo = normalized_verified_Voltage_3D.*Anatomy_binary_endo;
    colormaped_image_epi = uint8(normalaized_verified_Voltage_3D_epi.*255) + 1;
    normalaized_verified_Voltage_3D_endo = normalaized_verified_Voltage_3D_endo.*Anatomy_binary_endo;
    colormaped_image_endo = uint8(normalaized_verified_Voltage_3D_endo.*255) + 1;
    %Voltage threshold
    for z_segment = 1 : size(Anatomy , 3)
        BWa_epi(:,:,z_segment) = im2bw(normalaized_verified_Voltage_3D_epi(:,:,z_segment),0.8);
        BWa_endo(:,:,z_segment) = im2bw(normalaized_verified_Voltage_3D_endo(:,:,z_segment),0.8);
        BWa_epi_tau(:,:,z_segment) = im2bw(normalaized_verified_Voltage_3D_epi_tau(:,:,z_segment),0.8);
    end
    [R_epi,count_epi(T)] = bwlabeln(BWa_epi,26);
    count_epi(T);
    [R_endo,count_endo(T)] = bwlabeln(BWa_endo,26);
    [R_epi_tau,count_epi_tau(T)] = bwlabeln(BWa_epi_tau,26); %%%% As discussed with Uli in the meeting 11.02.2016, 
                                                             %%%% we do not count for the detected waves in the endocardium. 
                                                             %%%% We only use the number of waves on the epicardial surface 
                                                             %%%% as the number quantifying the degree of complexity.
    %%%% BT detection section:
    %%%% 1- Check for the wave sizes.
    %%%% 2- If size of a wave was smaller that 10 elements and bigger
    %%%%    thatn 5. It may be a BT.
    %%%% 3- If that wave was overlapping with another wave on the endocardium
    %%%%    then it is a BT condidate. 
    %%%% 4- Finally we check the BT candidate to see its size was increased
    %%%%    after 10ms. If yes it classified as a successfull BT.
    %%%%    Else will be considered as an unsuccessfull one. 
    %%%%    Note: The size of the BT should not be increased too much. This shows that
    %%%%    it was merged to a wave.
    
    for N_waves = 1 : count_epi(T)
        [r, c, v]= ind2sub(size(R_epi), find(R_epi == N_waves));

        toWrite= [ones(length(r),1)*T,...
                ones(length(r),1)*(N_waves),...
                r, c, v]'; %'   
        fprintf(WaveLocationsFile,'%d %d %d %d %d\n',toWrite);

        if size(r , 1)*size(c , 1)*size(v , 1)< 10 &&  size(r , 1)*size(c , 1)*size(v , 1) > 5 %%% Check the wave sizes
            disp(N_waves)
            BTdetected = 1;
            BWa_endo_BT = BWa_endo;
            [R_BT, count] = bwlabeln(BWa_endo_BT,26);
            R_BT = double (R_BT + 1);
            R_epi_temp = zeros(size(R_epi));
            R_epi_temp(r,c,v) = 1;
            R_epi_temp_tau = zeros(size(R_epi));
            im = imdilate(R_epi_temp,ones(2,2,2));
            im_test = imdilate(R_epi_temp,ones(5,5,5));
            BWa_overlap_BTandEndo = im .* BWa_endo_BT;   %%%% Check if it is overalpping with a wave in the endocardium.
            if sum(sum(sum(BWa_overlap_BTandEndo)))
                [r_BT, c_BT, v_BT] = ind2sub (size(im),find(im));
                waveIndex_endo = max(max(max(R_endo(r_BT,c_BT,v_BT))));
                R_endo_temp = zeros(size(R_endo));
                R_endo_temp(R_endo == waveIndex_endo) = 1;
                [R_epi_BT,count_BT] = bwlabeln(im_test,26);
                R_epi_BT= R_epi_BT .* 10;
                [R_endo_BT,count_endo] = bwlabeln(R_endo_temp,26);
                R_total = R_endo_temp + R_epi_BT;
                BT_temp = BT_temp + 1;
                waveIndex_epi_tau = max(max(max(R_epi_tau(r,c,v))));
                R_epi_temp_tau (R_epi_tau ==  waveIndex_epi_tau ) = 1;
                size_BT_tau = sum(sum(sum(R_epi_temp_tau)));
                size_BT = sum(sum(sum(R_epi_temp)));
                if size_BT_tau < size_BT * 4      %%%% Check if the BT is propagated (its size increased)
                    BT_unSucc_temp  = BT_unSucc_temp + 1;
                elseif size_BT_tau <= size_BT * 10 && size_BT_tau >= size_BT * 4
                    important_bt_temp = important_bt_temp + 1;
                    [r, c, v]= ind2sub(size(R_epi), find(R_epi == N_waves));
                    toWrite = [ones(length(r),1)*T,...
                                ones(length(r),1)*important_bt_temp,...
                                r, c, v]';
                    fprintf(BTPositionsFile,'%d %d %d %d %d\n',toWrite);
                end
                BT(T) = count_BT;
                    

                wave_anatomy_imposed = double(R_total + 1).*Anatomy_binary_show;
                wave_anatomy_imposed(wave_anatomy_imposed==0) = NaN;
                %[hpat] = PATCH_3Darray(wave_anatomy_imposed,'col',map_temp);

            end
        end
    end
    important_bt(T) = important_bt_temp; 
    BT_unSucc(T)= BT_unSucc_temp;
    BT(T) = BT_temp;
    BT_temp = 0;
    BT_unSucc_temp=0;
    important_bt_temp=0;
    R_epi_nonBinary = double(R_epi + 1);
    R_endo_nonBinary = double(R_endo + 1);
    wave_anatomy_imposed = R_epi_nonBinary.*Anatomy_binary_show;
    wave_anatomy_imposed = R_endo_nonBinary.*Anatomy_binary_show;
    wave_anatomy_imposed(wave_anatomy_imposed==0) = NaN;
end


number_of_BT = sum(BT);
unsuccess_BT = sum( BT_unSucc)
VIB = sum(important_bt)
number_of_waves = sum(count_epi-1);
total_number_of_waves = count_epi-1;
fclose(WaveLocationsFile);
fclose(BTPositionsFile);

