#!/bin/bash

# Array of experiment names
exp_names=(
    # "EX0025_A00_P04"    
    "EX0025_A70_P08"
    "EX0025_A00_R0_P04"
    # "EX0025_A00_P05"      
    # "EX0025_A00_P08"      
    # "EX0025_A00_R0_P15"      
    # "EX0025_A00_R0_P16"  
)


OUTPATH=/users/vgonalve/exec/EX0025/wavetracker
DATAPATH=/users/vgonalve/exec/EX0025
# TCCFILE=/users/vgonalve/propagEssentials/anatomy/model30/exp700c00_tcc_afull.igb.gz
# TCCFILE=/users/vgonalve/exec/EX0023/EX0023_Ox20_P04_tcc_afull.igb.gz
TCCFILE=/users/vgonalve/exec/EX0025/EX0025_A00_P04_tcc_afull.igb.gz

# ANATOMYFILE=/users/vgonalve/exec/anatomy/model30/model24-30-heart-cell-fib00m70-pvi.igb 
# ANATOMYFILE=/users/vgonalve/exec/anatomy/model29/model24-29-heart-cell-pvi.igb 
# ANATOMYFILE=/users/vgonalve/exec/anatomy/model30/exp906a13_A1.igb 
ANATOMYFILE=/users/vgonalve/exec/anatomy/model29/NoFibrosis_R0.igb
# ANATOMYFILE=/users/vgonalve/exec/anatomy/model29/SevereFibrosis_F1_R0.igb


TBEGIN=0
TEND=14999 # TODO: -1 DOES NOT WORK HERE
DOWNRATIO=5
NREADERS=12 # not more than 12 for full resolution or memory issues happen

TIME=01:00:00
SUBMIT=true

# Loop through the experiment names and create copies of the script
for exp_name in "${exp_names[@]}"; do
    # Get proper anatomy file

    # Generate the new script name
    new_script_name="wt_${exp_name}.sh"

    # Create a copy of the original script
    cp WaveTrackerTemplate.sh "${new_script_name}"

    # Replace the parameters above 
    sed -i "s/experimentName/${exp_name}/g" "${new_script_name}"
    sed -i "s/jobName/wt_${exp_name}/g" "${new_script_name}"
    sed -i "s/requestedHours/${TIME}/g" "${new_script_name}"

    sed -i "s|timeBegin|${TBEGIN}|g" "${new_script_name}"
    sed -i "s|timeEnd|${TEND}|g" "${new_script_name}"
    sed -i "s|param_downRatio|${DOWNRATIO}|g" "${new_script_name}"
    sed -i "s|param_n_readers|${NREADERS}|g" "${new_script_name}"

    sed -i "s|dataPath|${DATAPATH}|g" "${new_script_name}"
    sed -i "s|outPath|${OUTPATH}|g" "${new_script_name}"
    sed -i "s|tccFile|${TCCFILE}|g" "${new_script_name}"
    sed -i "s|anatomyFile|${ANATOMYFILE}|g" "${new_script_name}"

    chmod +x "${new_script_name}"

    echo "Created script: ${new_script_name}"

    if $SUBMIT
    then
        sbatch ${new_script_name}
    fi

done
