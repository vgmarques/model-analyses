#%%
import os,sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import multiprocessing as mp
import pickle
from scipy.spatial.distance import pdist,squareform

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb


## Relevant parameters 

RelevantTrajectories = {}
MinimumDurationTrajectory = 0#100 # ms
SegmentDuration = 1000 # ms
tBegin = 1000

#%% Load all projected PSs per experiment in a certain folder
exp = 'EX0008'
BTPATH = '/mnt/d/vgmar/model_data/%s/bt_detection'%exp
experimentNames = []
for file in os.listdir(BTPATH):
    if file.startswith(exp) and file.endswith('_BT_1mm.csv'):
        experimentNames.append(file.split('_BT_1mm.csv')[0])


#%% Segment PS databases per segment

SegmentDFs = {}
for experimentIndex,experimentName in enumerate(experimentNames):

    InputDF = pd.read_csv(os.path.join(BTPATH,experimentName+'_BT_1mm.csv'))
    InputDF = InputDF[InputDF.columns[1:]]

    tSteps = np.unique(InputDF['t']).astype(int)
    if tBegin is None:
        tBegin = tSteps.min()
    nSegments = (tSteps.max()-tBegin)//SegmentDuration

    SegDF = []
    for t in range(nSegments):
        SegDF.append(InputDF.loc[(InputDF['t']>=tBegin+t*SegmentDuration)&\
                    (InputDF['t']<tBegin+(t+1)*SegmentDuration)&\
                    # (InputDF['vip']==0)&\
                    (InputDF['b']==0),['t','x','y','z','b']])
    SegmentDFs[experimentIndex] = SegDF

#%% Now, for each segmented dataframe, get median position of relevant trajectories

OutputDF = pd.DataFrame(columns = ['experiment','segment','x','y','z','bt'])
k = 0
for experimentIndex,experimentName in enumerate(experimentNames):

    for segmentIndex,segment in enumerate(SegmentDFs[experimentIndex]):
        positions = np.array(segment.loc[:,['x','y','z']])
        positions = np.unique(positions, axis=0)
        if len(positions)==0: continue

        # Exclude very similar BTs
        btDistance = squareform(pdist(positions))<=2
        finalPos = []
        for row in btDistance:
            if np.sum(row)<2: continue # I asked for at least 2 repetitions
            finalPos.append(np.mean(positions[row,:],axis=0))
        positions = np.unique(np.array(finalPos,int),axis=0)

        for pos in positions:
            averageTrajectoryDF = pd.DataFrame({'experiment':[experimentName],
                                               'segment':[segmentIndex],
                                               'x':[int(pos[0])],'y':[int(pos[1])],'z':[int(pos[2])],
                                               'bt':[k]})
            k += 1
            
            OutputDF = OutputDF.append(averageTrajectoryDF,ignore_index =True)


#%% Convert all x,y,z positions to anatomy indexes
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
# %% Get anatomical indexes
anatomyInds = []

positions = np.asarray(OutputDF[['x','y','z']])
for i,row in enumerate(positions):
    x,y,z = row
    idx = np.linalg.norm(Anatomy-np.array(row,int),axis=1)

    anatomyInds.append(np.where(idx==np.min(idx))[0][0])

OutputDF = OutputDF.assign(anatomyIndex = anatomyInds)

#%% Save

OutputDF.to_csv(os.path.join(BTPATH,
                '%s_AverageBTPosition.csv'%exp))

