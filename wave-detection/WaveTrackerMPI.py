#!/usr/bin/env python
#%%
import os,sys
os.chdir('/users/vgonalve/model-analyses/wave-detection')
import numpy as np
from mpi4py import MPI

from time import time
import subprocess
# from copy import deepcopy
import logging
# import psutil
from datetime import datetime
import pickle


from skimage.measure import label as bwlabeln 
# from skimage.morphology import binary_closing as imclosing
from skimage.morphology import binary_dilation as imdilation
from skimage.morphology import binary_erosion as imerosion



sys.path.append('../aux-functions')
import IgbHandling as igb
from UserInterfaceFunctions import MyParser


# def get_memory_usage():
#     process = psutil.Process()
#     memory_info = process.memory_info()
#     return memory_info.rss/1e6  # Returns the resident set size (RSS) memory usage in bytes


def SegmentCellAnatomy(cell):
    nx,ny,nz = cell.shape
    EndoNodes = igb.GetAtrialSurface(cell,
                         innerTypes = [113,114,115,116,117,118,119,250],
                         outerTypes = [104],
                         linear_indexes = True,
                         mode = 'SP')

    EndoSurfaceNodes = np.zeros((nx+1,ny+1,nz+1),dtype=bool)
    EndoSurfaceNodes[igb.Idx2Coord(EndoNodes,(nx+1,ny+1,nz+1))] = True

    return EndoSurfaceNodes
### Stack overflow
import cProfile

def profile(filename=None, comm=MPI.COMM_WORLD):
  def prof_decorator(f):
    def wrap_f(*args, **kwargs):
      pr = cProfile.Profile()
      pr.enable()
      result = f(*args, **kwargs)
      pr.disable()

      if filename is None:
        pr.print_stats()
      else:
        filename_r = filename + ".{}".format(comm.rank)
        pr.dump_stats(filename_r)

      return result
    return wrap_f
  return prof_decorator

def GetFinalWaveLabels(LinkingDict,SizeDict):
    
    # Get node names for easier dictionary access
    nodeNames = list(LinkingDict.keys())
    tSteps =  np.asarray(nodeNames)[:,0]
    # Arrange in ascending order
    nodeNames = [nodeNames[i] for i in np.argsort(tSteps)]
    tSteps = tSteps[np.argsort(tSteps)]

    waveLabelsDictionary = {}
    lastWaveCode=0
    for nodeIndex,nodeName in enumerate(nodeNames):
        t = tSteps[nodeIndex]
        # If in first step, initiate the dict
        if t==tSteps[0]:
            waveLabelsDictionary[nodeName] = (t,lastWaveCode)
            lastWaveCode += 1
        
        # Find connecting waves
        connectingWaves = LinkingDict[nodeName]

        if nodeName not in waveLabelsDictionary.keys():
            # New wave
            waveLabelsDictionary[nodeName] = (t,lastWaveCode)
            lastWaveCode += 1
        
        if t==tSteps[-1]: #Last one, should not expect info on following waves
            break
        
        if len(connectingWaves)==1:
            # Single connecting wave
            waveLabelsDictionary[connectingWaves[0]] = (t+1,waveLabelsDictionary[nodeName][1])
        elif len(connectingWaves)>1:
            # Wave Split
            allSizes = [SizeDict[cW] for cW in connectingWaves]
            largestWave = np.where(allSizes==np.max(allSizes))[0][0]

            for cI,cW in enumerate(connectingWaves):
                if cI==largestWave:
                    # Continuation of wave
                    waveLabelsDictionary[cW] = (t+1,waveLabelsDictionary[nodeName][1])
                else:
                    # New waves
                    waveLabelsDictionary[cW] = (t+1,lastWaveCode)
                    lastWaveCode += 1

    waveCodes=np.array(list(waveLabelsDictionary.values()))
    waveTimes = {wC:waveCodes[np.where(waveCodes[:,1]==wC),0] for wC in np.unique(waveCodes[:,1])}
    return waveTimes,waveCodes


#%%
MESSAGES_DICT = {'Wave_detection':1,
                 }

#%%
# @profile(filename="profile_out")
def Reader(args):
    MSG_INFO=0
    MSG_V=1
    MSG_VTAU=2

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    distributorRank=size-2
    loggerRank=size-1

    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    ) 
    logging.info("Reader initialized. Rank {}.".format(rank))
    
    rangesReading = np.round(np.linspace(args.tBegin,args.tEnd,args.n_readers+1)).astype(int)
    rangesReading = np.hstack([rangesReading[:-1].reshape(-1,1),rangesReading[1:].reshape(-1,1)+args.tau])
    rangesReading = rangesReading[rank]

    if rank==args.n_readers-1:
        rangesReading[1] -= args.tau #Last one cannot go over tEnd

    fname_signals = args.vmFile
    tInt = args.tInt

    tBegin = rangesReading[0]
    tEnd = rangesReading[1]
    tString = str(tBegin)+':'+str(args.tInt)+':'+str(tEnd)   
    logging.info('Opening %s'%args.vmFile)
    args.tau = int(args.tau//tInt)

    # Reads the data and puts in queue
    ti = time()
    logging.info('Started reading')
    logging.info('Reading interval: %d:%d:%d'%(tBegin,args.tInt,tEnd))
    Requests = []
    with subprocess.Popen(["gzip","-dc",args.vmFile],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"-q","--ds",str(args.downRatio), 
                "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # read the header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)
            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes

            if tEnd==-1:
                tEnd=args.tBegin+hdr['t']

            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau),int)
            for i in range(args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)

            # Start putting information in the ProcessingQueue   
            ww = 0  
            wk = args.n_readers + ww  
            logging.info('Started sending data')
            for t in range(0,int((tEnd-tBegin)/tInt)-args.tau):
                V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                V_tau = np.swapaxes(V_tau,0,2)
                V = Buffer[:,:,:,t%args.tau]
                outDict = { 'tstep':t*tInt+tBegin,
                            'V':Buffer[:,:,:,t%args.tau],
                            'V_tau':V_tau,
                            'hdr':hdr
                            }
                
                comm.send(outDict, dest=distributorRank,tag=10)
                # update for next iteration
                Buffer[:,:,:,(t+args.tau-1)%args.tau]=V_tau
                
    # Send finish messages
    comm.send(None, dest=distributorRank,tag=10)
    logging.info('Elapsed: %0.3f'%(time()-ti))
    logging.info('Finished: %s'%formatted_datetime)


def Distributor(args):
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    distributorRank = size-2
    loggerRank = size-1
    nWorkers = size-3-args.n_readers
    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    ) 
    Buffer = []
    AvailableWorkers =set()
    success=False
    freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)

    finished = 0
    WaitForReader = True
    maxBuffer=args.MaxQueue
    while True:      
        success,rank = freeWorkerReq.test()
        if success:
            AvailableWorkers.add(rank)
            freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)
            success=False
            # logging.info('MEM: %d Mb'%get_memory_usage())


        elif (len(Buffer)==maxBuffer and not success) or not WaitForReader:
            rank=freeWorkerReq.wait()
            AvailableWorkers.add(rank)
            freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)


        # Get stuff from readerm add to buffer
        if WaitForReader and len(Buffer)<maxBuffer:
            reader_status = MPI.Status()
            buf = comm.recv(source=MPI.ANY_SOURCE,tag=10,status=reader_status)
            reader_rank=reader_status.Get_source()
            if buf is None:
                finished += 1
                logging.info('FINISHED: %s'%finished)
                if finished==args.n_readers:
                    WaitForReader = False
            else:
                Buffer.append(buf)

        logging.info('QUEUE: %d'%len(Buffer))

        # if there are workers, send to them
        while len(Buffer)>0 and len(AvailableWorkers)>0:
            comm.send(Buffer.pop(),dest =AvailableWorkers.pop())

        if not WaitForReader and len(Buffer)==0:
            while len(AvailableWorkers)!=nWorkers:
                rank=freeWorkerReq.wait()
                AvailableWorkers.add(rank)
                freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)
                logging.info('In: %s'%AvailableWorkers)

            for wk in range(args.n_readers,distributorRank):
                comm.isend(None, dest=wk) #FIXME
            break



#%%
# @profile(filename="profile_out")
def Processing(args,Anatomy_Epi): #,Anatomy_Endo):
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size=comm.Get_size()
    loggerRank = size-1
    distributorRank = size-2
    dictManagerRank = size-3

    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    ) 
    # Dilation kernels for avoiding isolated points
    # connectKernel = np.ones((4,4,4),dtype=bool)   # was 4,4,4
    # connectKernelEpi = np.ones((2*args.downRatio+1,
    #                             2*args.downRatio+1,
    #                             2*args.downRatio+1),dtype=bool)# was 3,3,3   
    # anatomyShape = Anatomy_Endo.shape
    anatomyShape = Anatomy_Epi.shape
    
    #Listening
    while True:

        logging.info('%d: Waiting to send'%rank)
        comm.isend(rank, dest=distributorRank,tag=11).wait()
        logging.info('%d: sent'%rank)
        inDict = comm.recv(source=distributorRank) #tag=MESSAGE_TEST,  
        if inDict is None:
            logging.info('%d; I got the finish message'%rank)
            # finished += 1
            # if finished ==args.n_readers:
            comm.send(None, dest=loggerRank,tag=12) #FIXME
            comm.send(None, dest=dictManagerRank,tag=13)
            break
        t       = inDict['tstep']
        V  = inDict['V']
        V_tau   = inDict['V_tau']
        hdr     = inDict['hdr'] 


        waveTh = (args.waveThreshold-hdr['zero'])/hdr['facteur'] 

        # Positions exceeding thresholds for wave and bt detection (-60 / -20 mV)
        V = V>=waveTh
        V_tau = V_tau>=waveTh

        # Epi layer
        ## t
        V_3D_Epi = V*Anatomy_Epi

        # Connected components analysis in Epi layer
        R_Epi = bwlabeln(V_3D_Epi,connectivity=3,background=0,return_num=False) # was true before

        uniqueWavesEpi,sizeWavesEpi = np.unique(R_Epi[R_Epi!=0],return_counts=True)
        NumberOfEpiWaves = len(uniqueWavesEpi)

        ## t+tau
        V_3D_Epi_tau = V_tau*Anatomy_Epi#np.logical_and(V,Anatomy_Epi)#V*Anatomy_Epi

        # Connected components analysis in Epi layer
        R_Epi_tau = bwlabeln(V_3D_Epi_tau,connectivity=3,background=0,return_num=False) # was true before
        
        uniqueWavesEpi_tau,sizeWavesEpi_tau = np.unique(R_Epi_tau[R_Epi_tau!=0],return_counts=True)
        NumberOfEpiWaves = len(uniqueWavesEpi)

        # For each wave in t, check overlap with t+tau
        # Put in a dictionary that is actually a graph
        waveLabels_epi = {}
        waveSizes_epi = {}
        for wI,waveLabel in enumerate(uniqueWavesEpi):
            overlap = np.logical_and(R_Epi==waveLabel,R_Epi_tau.astype(bool))
            overlapLabels,labelCounts = np.unique(R_Epi_tau[overlap],return_counts=True)
            waveLabels_epi[(t,waveLabel)] = [(t+args.tau,oL) for oL in overlapLabels]
            waveSizes_epi[(t,waveLabel)] = sizeWavesEpi[wI]
        
        # Send to the dict manager to get a final label
        dictManagerMessage = (t,'epi',waveLabels_epi,waveSizes_epi)
        comm.send(dictManagerMessage, dest=dictManagerRank,tag=13)

        # Save number of waves
        loggerMessage = ('wave',[t,NumberOfEpiWaves])
        comm.send(loggerMessage, dest=loggerRank,tag=12)

def DictManager(args):
    '''
    The purpose of this processor is to unify the pairwise comparisons between timesteps into a single
    wave tracking algorithm
    '''
    ti = time()
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size=comm.Get_size()
    loggerRank = size-1
    distributorRank = size-2
    nWorkers = size-3-args.n_readers
    finishedProcessors=0

    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    ) 

    WaveLinking_Epi = {}
    WaveSize_Epi={}
    # WaveLinking_Endo = {}
    # WaveSize_Endo={}
    k=0
    while True:
        # Wait for an epicardial message
        processor_status = MPI.Status()
        message = comm.recv(None,source=MPI.ANY_SOURCE,tag=13,status=processor_status)
        # Check if processors are done
        if message is None:
            finishedProcessors += 1
            if finishedProcessors==nWorkers:
                break
            else:
                logging.info('Finished processes: %d'%finishedProcessors)
                continue

        # Otherwise, check for wave data

        t,layer,waveLabels,waveSizes = message

        if layer=='epi':
            logging.info('calculating epi')
            WaveLinking_Epi = WaveLinking_Epi | waveLabels
            WaveSize_Epi = WaveSize_Epi | waveSizes

            if k%100==0:
                with open('%s/%s_EpiLinks.pkl'%(args.outputPath,args.experimentName), 'wb') as fout:
                    pickle.dump(WaveLinking_Epi, fout, pickle.HIGHEST_PROTOCOL)
                with open('%s/%s_EpiSizes.pkl'%(args.outputPath,args.experimentName),'wb') as fout:
                    pickle.dump(WaveSize_Epi,fout,pickle.HIGHEST_PROTOCOL)
        k +=1
        # elif layer=='endo':
        #     logging.info('calculating endo')
        #     WaveLinking_Endo = WaveLinking_Endo | waveLabels
        #     WaveSize_Endo = WaveSize_Endo | waveSizes
        #     with open('%s/%s_EndoLinks.pkl'%(args.outputPath,args.experimentName), 'wb') as fout:
        #         pickle.dump(WaveLinking_Endo, fout, pickle.HIGHEST_PROTOCOL)
        #     with open('%s/%s_EndoSizes.pkl'%(args.outputPath,args.experimentName),'wb') as fout:
        #         pickle.dump(WaveSize_Endo,fout,pickle.HIGHEST_PROTOCOL)

    ## After it is done, calculate the final wave durations
    waveTimes_Epi,waveCodes_Epi = GetFinalWaveLabels(WaveLinking_Epi,WaveSize_Epi)
    with open('%s/%s_epi.wavelabels'%(args.outputPath,args.experimentName),'wb') as fout:
        pickle.dump(waveTimes_Epi,fout,pickle.HIGHEST_PROTOCOL)
        pickle.dump(waveCodes_Epi,fout,pickle.HIGHEST_PROTOCOL)

    # waveTimes_Endo,waveCodes_Endo = GetFinalWaveLabels(WaveLinking_Endo,WaveSize_Endo)
    # with open('%s/%s_endo.wavelabels'%(args.outputPath,args.experimentName),'wb') as fout:
    #     pickle.dump(waveTimes_Endo,fout,pickle.HIGHEST_PROTOCOL)
    #     pickle.dump(waveCodes_Endo,fout,pickle.HIGHEST_PROTOCOL)
    logging.info('Total elapsed time; %0.3f s'%(time()-ti))



#%%
# @profile(filename="profile_out")
def Logger(args):
    ti = time()
    comm = MPI.COMM_WORLD
    size=comm.Get_size()
    nWorkers = size-3-args.n_readers # 3 "extra" processes: distributor, dict manager, logger
    rank = comm.Get_rank()
    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    ) 
    
    message= (None, None)
    MESSAGE_LOG=2
    finishedProcessors = 0

    # foutBT =  open(args.outputFile,'wb')
    # foutBT.write(b't,x,y,z,l,v')
    foutWave = open('%s/%s.wavecount'%(args.outputPath,args.experimentName),'wb')
    # foutWave.write(b't,endo,epi')
    foutWave.write(b't,epi')

    while True:
        
        probe = comm.iprobe()
        if not probe: continue
        status = MPI.Status()
        message = comm.recv(source=MPI.ANY_SOURCE,tag=12, status=status)
        # message = pickle.loads(message)
        logging.info(message)

        if message is None:
            finishedProcessors += 1
            if finishedProcessors==nWorkers:
                break
            else:
                continue

        layer,message = message

        if message is not None and layer=='wave':
            # t,nEndo,nEpi = message
            # foutWave.write(b'\n%d,%d,%d'%(t,nEndo,nEpi))
            # foutWave.flush()
            t,nEpi = message
            foutWave.write(b'\n%d,%d'%(t,nEpi))
            foutWave.flush()

        else:
            finishedProcessors +=1
        if finishedProcessors==nWorkers:
            break

    foutWave.close()
    # SortOutput(args.outputFile)
    SortOutput('%s/%s.wavecount'%(args.outputPath,args.experimentName))
    current_datetime = datetime.now()
    logging.info('Total elapsed time; %0.3f s'%(time()-ti))
    logging.info('Finished')


def SortOutput(filename):       
    with open(filename,'rb') as fi:
        input = fi.read().splitlines()
        header = input[0]
        lines =np.asarray([L.decode().split(',') for L in input[1:]],dtype=int)
        lines = lines[np.argsort(lines[:,0])]
    with open(filename,'wb') as fout:
        fout.write(header)
        for L in lines:
            fout.write(b'\n')
            for eI,element in enumerate(L):
                if eI!=len(L)-1:
                    fout.write(b'%d,'%element)
                else:
                    fout.write(b'%d'%element)


if __name__ == '__main__':
    parser = MyParser(description='Breakthrough detection based on wave detection',
                    usage='python BreakthroughDetection.py [OPTIONS] vmFile tccFile anatomyFile',
                    fromfile_prefix_chars='+',
                    epilog='Latest changes: 2021-11-01')

    # Required arguments
    parser.add_argument('vmFile',action='store',type=str,
                        help = 'Full path + name of vm file (.iga.gz) containing the data.')    
    parser.add_argument('tccFile',action='store',type=str,
                        help = 'Full path + name of tcc file. Any tcc of the same anatomy will do.')
    parser.add_argument('anatomyFile',action='store',type=str,
                        help = 'Full path + name of anatomy file (.igb.gz).')

    # Optional arguments
    ## Thresholds
    parser.add_argument('-BTThreshold','-btt',dest= 'BTThreshold',action='store',default =-20,
                        type=float,help = 'Threshold for waveFRONT detection. Default -20 mV')
    parser.add_argument('-waveThreshold','-wvt',dest= 'waveThreshold',action='store',default =-60,
                        type=float,help = 'Threshold for wave detection. Default -60 mV')
    ## Temporal and spatial resolution
    parser.add_argument('-downRatio',action='store',default =2,
                        type=int,help = 'Spatial downsample rate. Resolution 1mm is ok for wave couting. Default 2.')
    parser.add_argument('-tau',action='store',default =10,
                        type=int,help = 'Temporal interval for comparing BT growth. Works better if dividable by downRatio. Default 10 ms.')
        
    ## Time interval
    parser.add_argument('-tBegin','-tb',dest= 'tBegin',action='store',default =0,
                        type=int,help = 'Initial time (in ms). Default 0')
    parser.add_argument('-tEnd','-te',dest= 'tEnd',action='store',default =-1,
                        type=int,help = 'Final time (in ms). Default -1')
    parser.add_argument('-tInt','-ti',dest= 'tInt',action='store',default =1,
                        type=int,help = 'Time interval between frames (in ms). Default 1')

    ## Computer specific paths and details
    parser.add_argument('-MaxQueue',action='store',default =40,
                        type=int,help = 'Maximum queue size. Default 40')   
    parser.add_argument('-n_readers',action='store',default =4,
                        type=int,help = 'Number of reading processes for parallelization. Default 4 (Daint)')    
    parser.add_argument('-outputPath','-op',dest= 'outputPath',
                        action='store',type=str,default ='.',
                        help = 'Full path for output files.') 
    parser.add_argument('-iga2igb',action='store',type=str,default ='iga2igb',
                        help = 'Path to iga2igb if needed.') 
    args = parser.parse_args()

    args.experimentName = os.path.split(args.vmFile)[-1].split('_vm')[0] 
    # args.outputFile = os.path.join(args.outputPath,args.experimentName+'.btd')
    # ### Debug
    # class options:
    #     def __init__(self):
    #         self.tccFile='/home/vgmarques/data/model-data/debug/exp700c00_tcc_afull.igb.gz' # We use tcc because the data is in nodes, not cells
    #         self.anatomyFile='/home/vgmarques/data/model-data/anatomy/model30/model24-30-heart-cell.igb'
    #         self.vmFile='/home/vgmarques/data/model-data/debug/exp906c71_short.iga.gz'
    #         self.outputFile='./out_test.btd' #!
    #         self.n_readers= 4
    #         self.downRatio = 2 #Previously: 5
    #         self.BTThreshold=-20
    #         self.waveThreshold=-60
    #         self.tau=1
    #         self.iga2igb='iga2igb'
    #         self.tBegin=0
    #         self.tInt=1
    #         self.tEnd=100
    #         self.MaxQueue=40
    #         self.outputPath='.'
    #         self.experimentName = 'test1'
    
    
    # args = options()
    os.system('mkdir %s/log'%args.outputPath)

    ###

    # MPI.Init()
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    logging.basicConfig(filename='%s/log/%s_r%02d.log'%(args.outputPath,args.experimentName,rank),
        level=logging.INFO,                     # Set the logging level to INFO or desired level
        format="%(asctime)s - %(message)s",     # Format the log message with the timestamp
        datefmt="%Y-%m-%d %H:%M:%S"             # Format the timestamp's date and time
    )    
    # Get the current date and time
    current_datetime = datetime.now()
    formatted_datetime = current_datetime.strftime("%Y-%m-%d %H:%M:%S")

    logging.info('Starting time: %s'%(formatted_datetime))
    logging.info('Rank %d'%(rank))

    loggerRank = size-1
    distributorRank=size-2
    dictManagerRank = size-3

    if rank < args.n_readers:
        Reader(args)

    elif np.isin(rank, np.arange(args.n_readers,dictManagerRank,dtype=int)):
        ti = time()
        #####
        logging.info('Loading anatomy')

        Anatomy_tcc,_ = igb.Load(args.tccFile)
        Anatomy_tcc = np.swapaxes(Anatomy_tcc,0,-1)

        ## Separate bundles from rest 
        Anatomy_NoBundles = np.isin(Anatomy_tcc,[113,114,116,117,119,250])
        Anatomy_Bundles = np.isin(Anatomy_tcc,[115])

        # #% Load anatomy from cell file and segment endo and epi surfaces
        AnatomyCell,_ = igb.Load(args.anatomyFile)
        AnatomyCell = np.swapaxes(AnatomyCell,0,-1)
        if AnatomyCell.shape[0]==650:
            AnatomyCell = AnatomyCell[:,:,:350] #FIXME: This should not be hard coded!!!!
        else:
            AnatomyCell = AnatomyCell[:,:,:200] #FIXME: This should not be hard coded!!!!


        EndoNodes = SegmentCellAnatomy(AnatomyCell)

        # Get bundles and merge with endocardium, get everything else and merge with epicardium
        Anatomy_Epi = np.logical_and(Anatomy_NoBundles,~EndoNodes)
        # Anatomy_Endo = np.logical_or(EndoNodes,Anatomy_Bundles)

        downRatio = args.downRatio
        # Anatomy_Endo = np.logical_and(Anatomy_Endo[::downRatio,::downRatio,::downRatio],
        #                             Anatomy_tcc[::downRatio,::downRatio,::downRatio]).astype(bool)
        Anatomy_Epi = Anatomy_Epi[::downRatio,::downRatio,::downRatio].astype(bool)
        Anatomy_tcc = [] # Free memory
        AnatomyCell = [] # Free memory
        logging.info('Anatomy loaded')

        ####
        logging.info('Time to read anatomy: %0.3f'%(time()-ti))
        # logging.info('MEM: %d Mb'%get_memory_usage())
        # processing(comm,rank,args.n_readers,loggerRank)
        # Processing(args,Anatomy_Epi,Anatomy_Endo)
        Processing(args,Anatomy_Epi)
    elif rank==dictManagerRank:
        DictManager(args)
    elif rank==distributorRank:
        Distributor(args)
    elif rank==loggerRank: 
        Logger(args)
