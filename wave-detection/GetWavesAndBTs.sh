#!/bin/bash -l
#
#SBATCH --job-name=jobName
#SBATCH --time=requestedHours
#SBATCH --nodes=2
#SBATCH --ntasks=24
#SBATCH --cpus-per-task=1
#SBATCH --signal=TERM@5
#SBATCH --mail-type=ALL
#SBATCH --constraint=gpu
#SBATCH --account=s1074

# This script automates the process of getting BT data

module load cray-python

EXPNAME=experimentName

CODEPATH=/users/vgonalve/model-analyses/wave-detection #Path of the scripts
OUTPATH=outPath # Output
DATAPATH=dataPath
TCCFILE=tccFile
ANATOMYFILE=anatomyFile


TBEGIN=timeBegin
TEND=timeEnd # TODO: -1 DOES NOT WORK HERE
TINT=1

BTPARAMS='-downRatio=param_downRatio
          -BTThreshold=-20
          -waveThreshold=-60
          -tau=10
          -tBegin='${TBEGIN}'
          -tInt='${TINT}'
          -tEnd='${TEND}'
          -MaxQueue=40
          -n_readers=12'



cd $CODEPATH

srun -n40 BreakthroughDetection_MPI.py ${BTPARAMS} -outputPath=${OUTPATH} \
${DATAPATH}/${EXPNAME}_vm_afull.iga.gz ${TCCFILE} ${ANATOMYFILE}

# done
# exit 0