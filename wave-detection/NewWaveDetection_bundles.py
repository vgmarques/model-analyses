# %% Import libraries
import os,sys
import numpy as np
import subprocess
from copy import deepcopy
from skimage.measure import label as bwlabeln 
from skimage.morphology import binary_dilation as imdilation
import multiprocessing as mp

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import quick_visualization as qv

#%% Define helper functions
def SegmentCellAnatomy(cell):
    nx,ny,nz = cell.shape
    EndoNodes = igb.GetAtrialSurface(cell,
                         innerTypes = [113,114,115,116,117,118,119],
                         outerTypes = [104],
                         linear_indexes = True,
                         mode = 'SP')

    EndoSurfaceNodes = np.zeros((nx+1,ny+1,nz+1),dtype=bool)
    EndoSurfaceNodes[igb.Idx2Coord(EndoNodes,(nx+1,ny+1,nz+1))] = True

    EpiNodes = igb.GetAtrialSurface(cell,
                         innerTypes = [113,114,115,116,117,118,119],
                         outerTypes = [0,1,102,120,161,162],
                         linear_indexes = True,
                         mode = 'SP')
    EpiSurfaceNodes = np.zeros((nx+1,ny+1,nz+1),dtype=bool)
    EpiSurfaceNodes[igb.Idx2Coord(EpiNodes,(nx+1,ny+1,nz+1))] = True

    return EndoSurfaceNodes,EpiSurfaceNodes

def Norm2Vals(signal,vals=[0,1]):
    nSignal = (vals[1]-vals[0])*(signal-signal.min())/(signal.max()-signal.min())+vals[0]
    return nSignal



#%% Segmentation of the anatomy
'''
In this part of the code, the anatomy is segmented into the different parts

Initially, this segmentation is based only on cell type, but atrial surface will come into
play at some point
'''
EndoBTs = 0
EpiBTs = 0




#%% Reader for iga files, should work in a separate process, feeding the reader queue

def Reader(args,ProcessingQueue):
    print('Reader initialized')
    fname_signals = args.vm_iga
    tBegin = args.tBegin
    tEnd = args.tEnd
    tInt = args.tInt

    tString = str(tBegin)+':'+str(tInt)+':'+str(tEnd)   

    # Reads the data and puts in queue
    with subprocess.Popen(["gzip","-dc",args.vm_iga],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"--ds","5",
                "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # read the header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)

            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes

            if tEnd==-1:
                tEnd=hdr['t']
                print('tEnd: %d'%tEnd)
            # Loop over data

            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau))
            for i in range(args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)*hdr['facteur'] + hdr['zero']
            # Start putting information in the ProcessingQueue
            # This will be used to standardize the output and save intermediate steps
            # V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
            # V_tau = np.swapaxes(V_tau,0,2)*hdr['facteur'] + hdr['zero']
            # V = Buffer[:,:,:,0]
            
            for t in range(0,int((tEnd-tBegin)/tInt)+1-args.tau):
                V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                V_tau = np.swapaxes(V_tau,0,2)*hdr['facteur'] + hdr['zero']
                V = Buffer[:,:,:,0]
                ProcessingQueue.put(('BT_detection',[t,V,V_tau]))
                Buffer = np.roll(Buffer,-1,axis=-1)
                Buffer[:,:,:,-1]=V_tau

    [ProcessingQueue.put(('kill',None)) for i in range(args.n_processes)];


#%% Processing 

def Processing(args,Anatomy_NoBundles,Anatomy_Bundles,ProcessingQueue,LoggerQueue):
    #While loop
    while True:
        # Get message from queue
        QOut = ProcessingQueue.get(True)
        message,data = QOut
        # If kill: get out of loop, kill worker
        if message=='initialize':
            print('Worker initialized')
   
        elif message=='kill':
            print('Worker killed') # can be passed to logger to save 
            LoggerQueue.put(('process_done',None))
            break
        elif message=='BT_detection':
            ## Get the voltages at V and V+tau
            t,V,V_tau = data

            ## Normalization #FIXME not necessary
            V_norm = Norm2Vals(V,[0,1])
            V_tau_norm = Norm2Vals(V_tau,[0,1])

            ## Get potentials at the surfaces of interest
            V_3D_NoBundles = V_norm*Anatomy_NoBundles
            V_tau_3D_NoBundles = V_tau_norm*Anatomy_NoBundles

            V_3D_Bundles = V_norm*Anatomy_Bundles
            V_tau_3D_Bundles = V_tau_norm*Anatomy_Bundles

            ## Voltage threshold to get depolarized portion FIXME use threshold as mV
            BWa_NoBundles = deepcopy(V_3D_NoBundles.astype(float))
            BWa_NoBundles[BWa_NoBundles<args.threshold] = 0
            BWa_NoBundles[BWa_NoBundles!=0] = 1 

            BWa_tau_NoBundles = deepcopy(V_tau_3D_NoBundles.astype(float))
            BWa_tau_NoBundles[BWa_tau_NoBundles<args.threshold] = 0
            BWa_tau_NoBundles[BWa_tau_NoBundles!=0] = 1
            #
            BWa_Bundles = deepcopy(V_3D_Bundles.astype(float))
            BWa_Bundles[BWa_Bundles<args.threshold] = 0
            BWa_Bundles[BWa_Bundles!=0] = 1 

            BWa_tau_Bundles = deepcopy(V_tau_3D_Bundles.astype(float))
            BWa_tau_Bundles[BWa_tau_Bundles<args.threshold] = 0
            BWa_tau_Bundles[BWa_tau_Bundles!=0] = 1 


            ## Connected components analysis
            R_NoBundles,NumberOfWaves = bwlabeln(BWa_NoBundles,connectivity=3,background=0,return_num=True)
            R_tau_NoBundles = bwlabeln(BWa_tau_NoBundles,connectivity=3,background=0)
            #
            R_Bundles, nWavesEndo = bwlabeln(BWa_Bundles,connectivity=3,background=0,return_num=True)
            R_tau_Bundles, _ = bwlabeln(BWa_tau_Bundles,connectivity=3,background=0,return_num=True)


            ### Endo Epi BTs
            for waveCode in range(1,NumberOfWaves+1): #Because the wave labels start at 1
                # Step 1: check if a small wave appeared on the chosen layer
                r,c,v = np.where(R_NoBundles==waveCode) 
                if len(r)>5 and len(r)<10:
                    BT_Detected = True
                    # Step 2: Check if the detected wave overlapped with any wave on the other layer
                    R_temp_NB = deepcopy(R_NoBundles)
                    R_temp_NB[R_temp_NB!=waveCode] = 0
                    R_temp_NB[R_temp_NB==waveCode] = 1
                    #
                    R_temp_B = deepcopy(R_Bundles)
                    R_temp_B[R_temp_B!=0] = 1
                    #
                    Overlap = R_temp_NB+R_temp_B
                    R_Overlap, nWavesOverlap = bwlabeln(Overlap,connectivity=3,background=0,return_num=True)
                    #
                    if nWavesOverlap < 1+nWavesEndo: # Overlap happened (one wave (the selected)+all in other layer)
                        # Step 3: Check if the wave grew when compared to the time t+tau
                        TimeOverlapEpi = np.zeros_like(R_NoBundles,dtype=bool)
                        TimeOverlapEpi[r,c,v] = 1
                        sizeEpi = np.sum(TimeOverlapEpi)
                        overlappingWave = np.unique(TimeOverlapEpi*R_tau_NoBundles)
                        if len(overlappingWave)>2:
                            if len(overlappingWave)==2: print('Epi wave split, check t=%d, wave %d'%(t,waveCode))
                            continue
                        else:
                            overlappingWave = overlappingWave[1]
                        sizeEpi_tau = np.sum(R_tau_NoBundles==overlappingWave)
                        if sizeEpi_tau>=4*sizeEpi: #FIXME: If I want the code the same as Ali's, change this
                            LoggerQueue.put(('epiBT',[t,r,c,v]))


            ### Epi Endo BTs
            for waveCode in range(1,nWavesEndo+1): #Because the wave labels start at 1
                # Step 1: check if a small wave appeared on the chosen layer
                r,c,v = np.where(R_Bundles==waveCode) 
                if len(r)>5 and len(r)<10:
                    BT_Detected = True
                    # Step 2: Check if the detected wave overlapped with any wave on the other layer
                    R_temp_B = deepcopy(R_Bundles)
                    R_temp_B[R_temp_B!=waveCode] = 0
                    R_temp_B[R_temp_B==waveCode] = 1
                    #
                    R_temp_NB = deepcopy(R_NoBundles)
                    R_temp_NB[R_temp_NB!=0] = 1
                    #
                    Overlap = R_temp_NB+R_temp_B
                    R_Overlap, nWavesOverlap = bwlabeln(Overlap,connectivity=3,background=0,return_num=True)
                    #
                    if nWavesOverlap < 1+NumberOfWaves: # Overlap happened (one wave (the selected)+all in other layer)
                        # Step 3: Check if the wave grew when compared to the time t+tau
                        TimeOverlapEpi = np.zeros_like(R_Bundles,dtype=bool)
                        TimeOverlapEpi[r,c,v] = 1
                        sizeEpi = np.sum(TimeOverlapEpi)
                        overlappingWave = np.unique(TimeOverlapEpi*R_tau_Bundles)
                        if len(overlappingWave)!=2:
                            if len(overlappingWave)==2: print('Endo wave split, check t=%d, wave %d'%(t,waveCode))
                            continue
                        elif len(overlappingWave)==2:
                            overlappingWave = overlappingWave[1]
                        
                        sizeEpi_tau = np.sum(R_tau_Bundles==overlappingWave)
                        if sizeEpi_tau>=4*sizeEpi: #FIXME: If I want the code the same as Ali's, change this
                            # print(waveCode)
                            LoggerQueue.put(('endoBT',[t,r,c,v]))


#%%
def Logger(LoggerQueue):
    # While loop
    while True:
        QOut = LoggerQueue.get(True)
        message,data = QOut
        # If initialize, open data
        if message=='initialize':
            EndoBT = 0
            EpiBT = 0
            print('Logger initialized')
        if message=='endoBT':
            t,r,c,v = data
            EndoBT += 1
            print('t= %d; Number of endo BTs: %d'%(t,EndoBT))
        if message=='epiBT':
            t,r,c,v = data
            EpiBT += 1
            print('t= %d; Number of epi BTs: %d'%(t,EpiBT))
        elif message=='process_done':
            print('Logger killed') 
            break

## !!Attention!! The labels begin at 1
'''
This is enough for the wave tracking algorithm.
- R_NoBundles gives us the number of waves (epicardial waves are enough to quantify
complexity, and in this case we simply ignore the bundles)
- R_tau_NoBundles enables us to connect the waves over time to check for BTs. Also 
useful to track waves, but maybe the delta t is too large for that
- R_Bundles is used for the BTs. should be replaced by a version considering EndoNodes
'''


def main(args): 
    print('Loading anatomy')
    ## Load anatomy from tcc and segment
    Anatomy_tcc,_ = igb.Load(args.tcc_igb)
    Anatomy_tcc = np.swapaxes(Anatomy_tcc,0,-1)

    ## Separate bundles from rest 
    Anatomy_NoBundles = np.isin(Anatomy_tcc,[113,114,116,117,119,250])
    Anatomy_NoBundles = Anatomy_NoBundles[::5,::5,::5] # Downsample

    Anatomy_Bundles = np.isin(Anatomy_tcc,[115])
    Anatomy_Bundles = Anatomy_Bundles[::5,::5,::5]

    Anatomy_tcc = [] # free memory

    # #% Load anatomy from cell file and segment endo and epi surfaces
    #FIXME: if I want to follow Ali's code, skip this ...
    AnatomyCell,_ = igb.Load(args.anatomy)
    AnatomyCell = np.swapaxes(AnatomyCell,0,-1)
    AnatomyCell = AnatomyCell[:,:,:350]

    EndoNodes,EpiNodes = SegmentCellAnatomy(AnatomyCell)
    EndoNodes= EndoNodes[::5,::5,::5] # Downsample
    EpiNodes = EpiNodes[::5,::5,::5] # Downsample

    # AnatomyCell= [] # free memory

    # Let's see what happens merging the whole endocardium with the bundles
    Anatomy_Bundles = np.logical_or(EndoNodes,Anatomy_Bundles)
    Anatomy_NoBundles = np.logical_or(EpiNodes,Anatomy_NoBundles)

    # FIXME ...until here


    with mp.Manager() as DataManager:
        # Create the two Queues
        ProcessingQueue = DataManager.Queue(maxsize=2)#maxsize=20
        [ProcessingQueue.put(('initialize',None))for i in range(args.n_processes)];
        LoggerQueue = DataManager.Queue()#maxsize=20
        LoggerQueue.put(('initialize',args))

        #Data In
        DataReader = mp.Process(target = Reader,args = (args,ProcessingQueue,))
        # Data processing
        ProcessingPool = mp.Pool(args.n_processes,Processing,(args,Anatomy_NoBundles,Anatomy_Bundles,ProcessingQueue,LoggerQueue,))
        # Data out
        LoggerProcess = mp.Process(target = Logger,args = (LoggerQueue,))

        LoggerProcess.start()
        # Open and wait for end 
        DataReader.start()
        
        DataReader.join()
        ProcessingPool.close()
        ProcessingPool.join()

    # Notes for next week:
    # - We need to register the position of the BTs
    # - I don't need to do the wave tracking now, but I have to store the BT positions
    # to compare with Ali's algorithm. They should be similar

#%%       


if __name__ == '__main__':
    #% User Input
    class options:
        def __init__(self,host='daint'):
            if host=='daint':
                self.tcc_igb='/users/vgonalve/propagEssentials/anatomy/model30/exp700c00_tcc_afull.igb.gz' # We use tcc because the data is in nodes, not cells
                self.anatomy='/users/vgonalve/propagEssentials/anatomy/model30/model24-30-heart-cell.igb'
                self.vm_iga='/users/vgonalve/exec/exp906/exp906c12_vm_afull.iga.gz'
                self.n_processes= 2
            elif host=='wsl':
                self.tcc_igb='/mnt/d/vgmar/model_data/exp906/tests/exp700c00_tcc_afull.igb.gz' # We use tcc because the data is in nodes, not cells
                self.anatomy='/mnt/d/vgmar/model_data/anatomy/model30_210325/model24-30-heart-cell.igb.gz'
                self.vm_iga='/mnt/d/vgmar/model_data/exp906/tests/exp906c12_500ms.iga.gz'
                self.n_processes= 2
            elif host=='usi':
                self.tcc_igb='/home/marques/exec/marques/wave/exp906c74_tcc_ds5.igb.gz'
                # #self.vm_igb='/home/marques/exec/marques/wave/exp906c74_vm_ds5.igb'
                # self.vm_iga='/home/marques/exec/marques/psdetection/exp906c82_vm_afull.iga.gz'
                # self.fout_wave='/home/marques/exec/marques/wave/exp906c82_waves_10s.bin'
                # # self.fout_bt='/home/marques/exec/marques/wave/bt_locations.txt'
            self.threshold=0.6
            self.tau=10
            self.iga2igb='iga2igb'
            self.tBegin=0
            self.tInt=1
            self.tEnd=100

    #
    args = options('daint')
    main(args)



# %% quick visualization

xx,yy,zz = np.where(R_NoBundles!=0)
test_epi = np.asarray([xx,yy,zz]).T

xx,yy,zz = np.where(R_Bundles!=0)
test_endo = np.asarray([xx,yy,zz]).T

ActorEpi,_ = qv.MakePolyDataActor(test_epi,np.ones((1,len(test_epi))),vmin=0,vmax=1)
ActorEndo,_ = qv.MakePolyDataActor(test_endo,np.zeros(len(test_endo)).reshape(1,-1),vmin=0,vmax=1)

ActorEpi.GetProperty().SetOpacity(0.2)
ActorEndo.GetProperty().SetPointSize(15)
qv.QuickRenderWindowInteractor([ActorEndo,ActorEpi])

# %%
