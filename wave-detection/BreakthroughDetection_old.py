"""
Created on Mon Nov 01 2021

DESCRIPTION: This script detects breakthroughs (BTs) given a Vm and a tcc file.

It is based on "image" analysis and connected components. The anatomy is divided into endocardium 
(bundles + atrial tissue touching blood) and epicardium (everything else, including interatrial bundles).
This means that the endocardium is mostly a single layer, while the epicardium is more 3D.

The algorithm works by detecting waves (Vm > a threshold = waveThreshold) in both layers. It then checks 
the size of these waves, which are connected by connected components. For the small waves (<150 for epi, 
<80 for endo), we check whether they overlap with a wavefront in the other layer (Vm > a threshold = BTThreshold).
If that is the case, a breakthrough has been detected. We then check the increase in size of this breakthrough
after tau ms to determine its importance. If it increased too much (25x the original size), it means the BT
has merged with a larger wave. If it increased less than that, it is classified as a very important BT that
changes the conduction pattern.

The implementation here is in parallel, with a reader process, many workers for the signal processing and
a logger function for saving everything.

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
# %% Import libraries
import os,sys
import numpy as np
import subprocess
from copy import deepcopy
from skimage.measure import label as bwlabeln 
from skimage.morphology import binary_closing as imclosing
from skimage.morphology import binary_dilation as imdilation
from skimage.morphology import binary_erosion as imerosion
import multiprocessing as mp
from datetime import datetime


upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
from egm_processing import Norm2Vals
from UserInterfaceFunctions import MyParser

# Define helper functions
def SegmentCellAnatomy(cell):
    nx,ny,nz = cell.shape
    EndoNodes = igb.GetAtrialSurface(cell,
                         innerTypes = [113,114,115,116,117,118,119,250],
                         outerTypes = [104],
                         linear_indexes = True,
                         mode = 'SP')

    EndoSurfaceNodes = np.zeros((nx+1,ny+1,nz+1),dtype=bool)
    EndoSurfaceNodes[igb.Idx2Coord(EndoNodes,(nx+1,ny+1,nz+1))] = True

    return EndoSurfaceNodes

#%% Reader for iga files, should work in a separate process, feeding the reader queue

def Reader(args,ProcessingQueue):
    print('Reader initialized')
    fname_signals = args.vmFile
    tBegin = args.tBegin
    tEnd = args.tEnd
    tInt = args.tInt

    tString = str(tBegin)+':'+str(tInt)+':'+str(tEnd)   
    print('Opening %s'%args.vmFile)
    args.tau = int(args.tau//tInt)

    # Reads the data and puts in queue
    with subprocess.Popen(["gzip","-dc",args.vmFile],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"--ds",str(args.downRatio),
                "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # read the header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)

            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes

            if tEnd==-1:
                tEnd=args.tBegin+hdr['t']
                print('tEnd: %d'%tEnd)

            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau))
            for i in range(args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)

            # Start putting information in the ProcessingQueue            
            for t in range(0,int((tEnd-tBegin)/tInt)-args.tau):
                V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                # V_tau = deepcopy(tmp)
                V_tau = np.swapaxes(V_tau,0,2)
                V = Buffer[:,:,:,t%args.tau]

                ProcessingQueue.put(('Wave_detection',[t*tInt+tBegin,
                                                       Buffer[:,:,:,t%args.tau],# = V
                                                       V_tau,
                                                       hdr]))
                # Buffer = np.roll(Buffer,-1,axis=-1)
                Buffer[:,:,:,(t+args.tau-1)%args.tau]=V_tau

    for i in range(args.n_processes): ProcessingQueue.put(('kill',None)) 


#%% Processing 

def Processing(args,Anatomy_Epi,Anatomy_Endo,ProcessingQueue,LoggerQueue):
    # Dilation kernels for avoiding isolated points
    connectKernel = np.ones((4,4,4),dtype=bool)   
    connectKernelEpi = np.ones((3,3,3),dtype=bool)   
    anatomyShape = Anatomy_Endo.shape
    
    #Listening
    while True:
        # Get message from queue
        QOut = ProcessingQueue.get(True)
        message,data = QOut

        # Choose action
        if message=='initialize':
            print('Worker initialized')
            anatomyShape = Anatomy_Endo.shape
        elif message=='kill':
            print('Worker killed') # can be passed to logger to save 
            LoggerQueue.put(('process_done',None))
            break

        elif message=='Wave_detection':
            ## Get the voltages at V and V+tau
            t,V_temp,V_tau,hdr = data 
            waveTh = (args.waveThreshold-hdr['zero'])/hdr['facteur'] 
            BTTh = (args.BTThreshold-hdr['zero'])/hdr['facteur'] 

            # Positions exceeding thresholds for wave and bt detection (-60 / -20 mV)
            xWave,yWave,zWave = np.where(V_temp>=waveTh) # wave
            xBT,yBT,zBT = np.where(V_temp>=BTTh) # wavefront
            V_temp=[] # memory
            
            # Positions exceeding thresholds for wave detection (-60 mV) for the t+tau
            xWave_tau,yWave_tau,zWave_tau = np.where(V_tau>=waveTh)
            V_tau=[] # memory

            # Wave detection (times t and t+tau)
            V = np.zeros(anatomyShape,dtype=bool)
            V[xWave,yWave,zWave] = 1
            
            V_tau = np.zeros(anatomyShape,dtype=bool)
            V_tau[xWave_tau,yWave_tau,zWave_tau] = 1

            # wavefront for BT detection (only at time t)
            V_BT = np.zeros(anatomyShape,dtype=bool)
            V_BT[xBT,yBT,zBT] = 1


            # Epi layer
            # Get potentials at the surfaces of interest

            V_3D_Epi = V*Anatomy_Epi#np.logical_and(V,Anatomy_Epi)#V*Anatomy_Epi
            V_BT_Epi = V_BT*Anatomy_Epi#np.logical_and(V_BT,Anatomy_Epi)#V_BT*Anatomy_Epi

            # Dilate a bit to avoid connection problems
            V_3D_Epi = imdilation(V_3D_Epi,connectKernelEpi)
            


            # Connected components analysis in Epi layer
            R_Epi = bwlabeln(V_3D_Epi,connectivity=3,background=0,return_num=False) # was true before
            R_mask = imerosion(R_Epi,connectKernelEpi)
            R_Epi = R_mask*R_Epi*Anatomy_Epi
            
            uniqueWavesEpi,sizeWavesEpi = np.unique(R_Epi[R_Epi!=0],return_counts=True)
            NumberOfEpiWaves = len(uniqueWavesEpi)

            # Endo layer
            '''
            The trick here is that a single layer in low resolution often yields broken waves
            Thus, I dilate the waves to be approximately the original resolution, do the 
            connected component analysis and then erode again

            The resulting eroded waves are already marked with the correct label, even if in the 
            final version they are not "connected"

            When checking the overlap, that is not an issue because we merge with the full low-res
            atrium, which will give a single "wave block" in case there is a BT
            '''
            ## Get potentials at the surfaces of interest
            V_3D_Endo = V*Anatomy_Endo#np.logical_and(V,Anatomy_Endo)#
            V_BT_Endo = V_BT*Anatomy_Endo#np.logical_and(V_BT,Anatomy_Endo)#

            V_3D_Endo = imdilation(V_3D_Endo,connectKernel)

            # Connected Components endo layer
            R_Endo = bwlabeln(V_3D_Endo,connectivity=3,background=0,return_num=False)
            R_mask = imerosion(R_Endo,connectKernel)
            R_Endo = R_mask*R_Endo*Anatomy_Endo #Get only points at anatomy
            uniqueWavesEndo,sizeWavesEndo = np.unique(R_Endo[R_Endo!=0].flatten(),return_counts=True)
            NumberOfEndoWaves = len(uniqueWavesEpi)

            # Save number of waves
            LoggerQueue.put(('saveWaves',[t,NumberOfEndoWaves,NumberOfEpiWaves]))
            # Check if any endocardial wave is within range. If not, just continue
            # Step 1: check if a small wave appeared on the chosen layer
            sizeBT_Endo = ((sizeWavesEndo<=80)&(sizeWavesEndo>0))

            if sizeBT_Endo.any():
                waveCodes = uniqueWavesEndo[sizeBT_Endo]

                # Get info from t+tau layer
                V_tau_3D_Endo = np.logical_and(V_tau,Anatomy_Endo)
                V_tau_3D_Endo = imdilation(V_tau_3D_Endo,connectKernel)     

                R_tau_Endo, _ = bwlabeln(V_tau_3D_Endo,connectivity=3,background=0,return_num=True)
                R_mask = imerosion(R_tau_Endo,connectKernel)
                R_tau_Endo = R_mask*R_tau_Endo*Anatomy_Endo

                #Because the wave labels start at 1
                for waveCode in waveCodes:
                    r,c,v = np.where(R_Endo==waveCode)
                    ProcessingQueue.put(('BT_detection_Endo',(t,waveCode,[r,c,v],V_BT_Epi,R_tau_Endo)))#R_Endo,V_BT_Epi,R_tau_Endo,Anatomy_Endo,sizeEndo)))



            # Check if any epicardial wave is within range. If not, just continue
            # Step 1: check if a small wave appeared on the chosen layer
            sizeBT_Epi = ((sizeWavesEpi>=5)&(sizeWavesEpi<=150))
            if sizeBT_Epi.any():
                # nWavesEpi = len(uniqueWavesEpi)-1
                waveCodes = uniqueWavesEpi[sizeBT_Epi]

                # Get info from t+tau layer
                V_tau_3D_Epi = np.logical_and(V_tau,Anatomy_Epi)#V_tau*Anatomy_Epi
                
                V_tau_3D_Epi = imdilation(V_tau_3D_Epi,connectKernelEpi)                # Dilate a bit to avoid connection problems and connect waves
                R_tau_Epi = bwlabeln(V_tau_3D_Epi,connectivity=3,background=0)
                R_mask = imerosion(R_tau_Epi,connectKernelEpi)
                
                R_tau_Epi = R_mask*R_tau_Epi*Anatomy_Epi

                for waveCode in waveCodes:
                    r,c,v = np.where(R_Epi==waveCode)
                    ProcessingQueue.put(('BT_detection_Epi',(t,waveCode,[r,c,v],V_BT_Endo,R_tau_Epi)))
                    # ProcessingQueue.put(('BT_detection_Epi',(t,waveCode,R_Epi,V_BT_Endo,R_tau_Epi,Anatomy_Epi,sizeEpi)))

            

        elif message=='BT_detection_Endo':

            # t,waveCode,R_Endo,V_BT_Epi,R_tau_Endo,Anatomy_Endo,sizeEndo = data 
            t,waveCode,waveCoords,V_BT_Epi,R_tau_Endo = data
            r,c,v = waveCoords
            sizeEndo=len(r)
            BT_Detected = True
            # Step 2: Check if the detected wave overlapped with any wavefront on the other layer
            R_temp_B = np.zeros(Anatomy_Endo.shape,dtype=int)
            R_temp_B[r,c,v] = 1
            R_temp_B = imdilation(R_temp_B,connectKernel)     

            Overlap = R_temp_B*V_BT_Epi
            overlappingWave = np.unique(Overlap)
            if len(overlappingWave) >1: # Overlap happened (one wave (the selected)+all in other layer)
                
                # Step 3: Check if the wave grew when compared to the time t+tau
                TimeOverlapEndo = imerosion(R_temp_B,connectKernel)   
                futureWave = np.unique(TimeOverlapEndo*R_tau_Endo)
                
                if len(futureWave)!=2:
                    if len(futureWave)>2: print('Endo wave split, check t=%d, wave %d'%(t,waveCode))
                    continue
                elif len(futureWave)==2:
                    futureWave = futureWave[1]
                    sizeEndo_tau = np.sum(R_tau_Endo==futureWave)

                if sizeEndo_tau>=10*sizeEndo: 
                    if  sizeEndo_tau<=25*sizeEndo: 
                        VIBT=1 
                    else: 
                        VIBT=0
                    # if len(r)!=0:
                    LoggerQueue.put(('endoBT',[t,r,c,v,VIBT]))

        elif message=='BT_detection_Epi':
            ### Endo Epi BTs
            t,waveCode,waveCoords,V_BT_Endo,R_tau_Epi = data
            r,c,v = waveCoords
            sizeEpi=len(r)
            BT_Detected = True

            # Step 2: Check if the detected wave overlapped with any wavefront on the other layer
            R_temp_NB = np.zeros(anatomyShape,dtype=bool)
            R_temp_NB[r,c,v] = True
            R_temp_NB = imdilation(R_temp_NB,connectKernel)   
            #
            Overlap = R_temp_NB*V_BT_Endo
            overlappingWave = np.unique(Overlap)
            #
            if len(overlappingWave) >1: # Overlap happened (one wave (the selected)+all in other layer)
                # Step 3: Check if the wave grew when compared to the time t+tau
                TimeOverlapEpi = imerosion(R_temp_NB,connectKernel)   
                futureWave = np.unique(TimeOverlapEpi*R_tau_Epi)
                if len(futureWave)!=2:
                    if len(futureWave)>2: print('Epi wave split, check t=%d, wave %d'%(t,waveCode))
                    continue
                elif len(futureWave)==2:
                    futureWave = futureWave[1]
                sizeEpi_tau = np.sum(R_tau_Epi==futureWave)

                if sizeEpi_tau>=10*sizeEpi: 
                    if  sizeEpi_tau<=25*sizeEpi:
                        VIBT=1
                        print('VIBT')
                    else:
                        VIBT=0
                    LoggerQueue.put(('epiBT',[t,r,c,v,VIBT]))

#%%
def Logger(LoggerQueue):
    wrapUp = False
    # While loop
    while True:
        if not wrapUp:
            QOut = LoggerQueue.get(True)
        else:
            QOut = LoggerQueue.get(True,timeout=10)
        message,data = QOut
        # If initialize, open data
        if message=='initialize':
            args = data
            basename = args.vmFile.split('/')[-1].split('_')[0]
            basename = os.path.join(args.outputPath,basename)
            EndoBT = 0
            EpiBT = 0
            KilledWorkers = 0
            datestr = 'Start '+datetime.now().strftime("%d-%m-%Y %Hh%Mm%Ss")+'\n'
            print(datestr)
            OutputFile =  open(basename+'.btd', 'wb')
            OutputFile.write(datestr.encode())
            OutputFile.write(b'\nt,x,y,z,b,vip')
            OutputFile.flush()

            # File with wave number
            OutputFileWave =  open(basename+'.wavecount', 'wb')
            OutputFileWave.write(datestr.encode())
            OutputFileWave.write(b'\nt,endo,epi')
            OutputFileWave.flush()

            print('Logger initialized')
        if message=='process_done':
            KilledWorkers += 1
            wrapUp = True
            if KilledWorkers==args.n_processes:
                LoggerQueue.put(('kill',None))
        if message=='endoBT':
            t,r,c,v,VIBT = data
            r = np.round(np.median(r))
            c = np.round(np.median(c))
            v = np.round(np.median(v))
            if VIBT: EndoBT += 1
            if not np.isnan((t,r,c,v,VIBT)).any():
                OutputFile.write(b'\n%d,%d,%d,%d,%d,%d'%(t,r,c,v,0,VIBT))
                OutputFile.flush()
                print('\n%d %d %d %d %d'%(t,r,c,v,0))
        if message=='epiBT':
            t,r,c,v,VIBT = data
            r = np.round(np.median(r))
            c = np.round(np.median(c))
            v = np.round(np.median(v))
            if VIBT: EpiBT += 1

            if not np.isnan((t,r,c,v,VIBT)).any():
                OutputFile.write(b'\n%d,%d,%d,%d,%d,%d'%(t,r,c,v,1,VIBT))
                OutputFile.flush()
                print(' %d %d %d %d %d\n'%(t,r,c,v,1))
        if message=='saveWaves':
            t,nEndo,nEpi = data
            OutputFileWave.write(b'\n%d,%d,%d'%(t,nEndo,nEpi))
            OutputFileWave.flush()


        elif message=='kill':
            datestr = '\nEnd '+datetime.now().strftime("%d-%m-%Y %Hh%Mm%Ss")
            OutputFile.write(datestr.encode())
            OutputFile.close()
            OutputFileWave.write(datestr.encode())
            OutputFileWave.close()
            print('Logger killed') 
            print('Number of important endo BTs: %d'%EndoBT)
            print('Number of important epi BTs: %d'%EpiBT)
            break

def main(args): 
    ## Segmentation of the anatomy
    '''
    In this part of the code, the anatomy is segmented
    The tcc file provides positions of bundles, while the cell file
    provides the regions in touch with the blood.
    '''
    print('Loading anatomy')

    Anatomy_tcc,_ = igb.Load(args.tccFile)
    Anatomy_tcc = np.swapaxes(Anatomy_tcc,0,-1)

    ## Separate bundles from rest 
    Anatomy_NoBundles = np.isin(Anatomy_tcc,[113,114,116,117,119,250])
    Anatomy_Bundles = np.isin(Anatomy_tcc,[115])

    # #% Load anatomy from cell file and segment endo and epi surfaces
    AnatomyCell,_ = igb.Load(args.anatomyFile)
    AnatomyCell = np.swapaxes(AnatomyCell,0,-1)
    AnatomyCell = AnatomyCell[:,:,:350]

    EndoNodes = SegmentCellAnatomy(AnatomyCell)

    # Get bundles and merge with endocardium, get everything else and merge with epicardium
    Anatomy_Epi = np.logical_and(Anatomy_NoBundles,~EndoNodes)
    Anatomy_Endo = np.logical_or(EndoNodes,Anatomy_Bundles)

    downRatio = args.downRatio
    Anatomy_Endo = np.logical_and(Anatomy_Endo[::downRatio,::downRatio,::downRatio],
                                Anatomy_tcc[::downRatio,::downRatio,::downRatio]).astype(bool)
    Anatomy_Epi = Anatomy_Epi[::downRatio,::downRatio,::downRatio].astype(bool)
    Anatomy_tcc = [] # free memory
    AnatomyCell = [] # free memory
    print('Anatomy loaded')

    print('Starting parallel pool')
    with mp.Manager() as DataManager:
        # Create the queues
        ProcessingQueue = DataManager.Queue() #maxsize=50)#maxsize=20
        [ProcessingQueue.put(('initialize',None))for i in range(args.n_processes)];
        LoggerQueue = DataManager.Queue()#maxsize=20
        LoggerQueue.put(('initialize',args))

        #Data In
        DataReader = mp.Process(target = Reader,args = (args,ProcessingQueue,))
        # Data processing
        ProcessingPool = mp.Pool(args.n_processes,Processing,(args,Anatomy_Epi,Anatomy_Endo,ProcessingQueue,LoggerQueue,))
        # Data out
        LoggerProcess = mp.Process(target = Logger,args = (LoggerQueue,))

        LoggerProcess.start()
        # Open and wait for end 
        DataReader.start()
        
        DataReader.join()
        ProcessingPool.close()
        ProcessingPool.join()

#%%       
if __name__ == '__main__':
    parser = MyParser(description='Breakthrough detection based on wave detection',
                    usage='python BreakthroughDetection.py [OPTIONS] vmFile tccFile anatomyFile',
                    fromfile_prefix_chars='+',
                    epilog='Latest changes: 2021-11-01')

    # Required arguments
    parser.add_argument('vmFile',action='store',type=str,
                        help = 'Full path + name of vm file (.iga.gz) containing the data.')    
    parser.add_argument('tccFile',action='store',type=str,
                        help = 'Full path + name of tcc file. Any tcc of the same anatomy will do.')
    parser.add_argument('anatomyFile',action='store',type=str,
                        help = 'Full path + name of anatomy file (.igb.gz).')

    # Optional arguments
    ## Thresholds
    parser.add_argument('-BTThreshold','-btt',dest= 'BTThreshold',action='store',default =-20,
                        type=float,help = 'Threshold for waveFRONT detection. Default -20 mV')
    parser.add_argument('-waveThreshold','-wvt',dest= 'waveThreshold',action='store',default =-60,
                        type=float,help = 'Threshold for wave detection. Default -60 mV')
    ## Temporal and spatial resolution
    parser.add_argument('-downRatio',action='store',default =2,
                        type=int,help = 'Spatial downsample rate. Default 2.')
    parser.add_argument('-tau',action='store',default =10,
                        type=int,help = 'Temporal interval for comparing BT growth. Works better if dividable by downRatio. Default 10 ms.')
        
    ## Time interval
    parser.add_argument('-tBegin','-tb',dest= 'tBegin',action='store',default =0,
                        type=int,help = 'Initial time (in ms). Default 0')
    parser.add_argument('-tEnd','-te',dest= 'tEnd',action='store',default =-1,
                        type=int,help = 'Final time (in ms). Default -1')
    parser.add_argument('-tInt','-ti',dest= 'tInt',action='store',default =1,
                        type=int,help = 'Time interval between frames (in ms). Default 1')

    ## Computer specific paths and details
    parser.add_argument('-n_processes',action='store',default =22,
                        type=int,help = 'Number of processes for parallelization. Default 22 (Daint)')    
    parser.add_argument('-outputPath','-op',dest= 'outputPath',
                        action='store',type=str,default ='.',
                        help = 'Full path for output files.') 
    parser.add_argument('-iga2igb',action='store',type=str,default ='iga2igb',
                        help = 'Path to iga2igb if needed.') 
    args = parser.parse_args()
    main(args)



# For debug
#     % User Input
# class options:
#     def __init__(self,host='daint'):
#         if host=='daint':
#             self.tccFile='/users/vgonalve/propagEssentials/anatomy/model30/exp700c00_tcc_afull.igb.gz' # We use tcc because the data is in nodes, not cells
#             self.anatomyFile='/users/vgonalve/exec/anatomy/model30/model24-30-heart-cell.igb.gz'
#             self.vmFile='/users/vgonalve/exec/WaveDetection/exp906c14_vm_afull.iga.gz'
#             self.outputPath='/users/vgonalve/model-analyses/wave-detection/' #!
#             self.n_processes= 22
#         elif host=='wsl':
#             self.tccFile='/mnt/d/vgmar/model_data/exp906/tests/exp700c00_tcc_afull.igb.gz' # We use tcc because the data is in nodes, not cells
#             self.anatomyFile='/mnt/d/vgmar/model_data/anatomy/model30_210325/model24-30-heart-cell.igb.gz'
#             self.vmFile='/mnt/d/vgmar/model_data/exp913/exp913f73_ds2.iga.gz'#exp906c12_500ms.iga.gz' # exp906c74_2.5s_downsampled.iga.gz' #
#             self.outputFile='/mnt/d/vgmar/model_data/exp906/tests/tmp2.btd'
#             self.n_processes= 2
#         elif host=='usi':
#             self.tccFile='/home/marques/exec/marques/wave/exp906c74_tcc_ds5.igb.gz'

#         self.downRatio = 2 #Previously: 5
#         self.BTThreshold=-20
#         self.waveThreshold=-60
#         self.tau=10
#         self.iga2igb='iga2igb'
#         self.tBegin=0
#         self.tInt=1
#         self.tEnd=-1

# args = options('daint')