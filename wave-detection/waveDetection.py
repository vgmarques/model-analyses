#%% 
import os,sys
import numpy as np
import subprocess
from copy import deepcopy
from skimage.measure import label as bwlabeln 
from skimage.morphology import binary_dilation as imdilation
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import quick_visualization as qv

#%% 
def Norm2Vals(signal,vals=[0,1]):
    nSignal = (vals[1]-vals[0])*(signal-signal.min())/(signal.max()-signal.min())+vals[0]
    return nSignal

def ConnectWavesSpace(V,V_tau,Anatomy_binary_epi,Anatomy_binary_endo):
    # Normalizing the Vm between 0 and 1
    normalized_verified_Voltage_3D = Norm2Vals(V,[0,1])
    normalized_verified_Voltage_3D_tau =  Norm2Vals(V_tau,[0,1])
    #
    # Get relevant values for each portion of anatomy
    normalized_verified_Voltage_3D_epi = normalized_verified_Voltage_3D*Anatomy_binary_epi
    normalized_verified_Voltage_3D_epi_tau = normalized_verified_Voltage_3D_tau*Anatomy_binary_epi
    normalized_verified_Voltage_3D_endo = normalized_verified_Voltage_3D*Anatomy_binary_endo
    #colormaped_image_epi = np.asarray(normalized_verified_Voltage_3D_epi.*255,dtype =np.uint8) + 1
    #normalized_verified_Voltage_3D_endo = normalized_verified_Voltage_3D_endo.*Anatomy_binary_endo;
    #colormaped_image_endo = uint8(normalaized_verified_Voltage_3D_endo.*255) + 1;
    #
    # Voltage Threshold
    BWa_epi = deepcopy(normalized_verified_Voltage_3D_epi)
    BWa_epi[BWa_epi<args.threshold] = 0
    BWa_epi[BWa_epi!=0] = 1 
    #
    BWa_epi_tau = deepcopy(normalized_verified_Voltage_3D_epi_tau)
    BWa_epi_tau[BWa_epi_tau<args.threshold] = 0
    BWa_epi_tau[BWa_epi_tau!=0] = 1
    #
    BWa_endo = deepcopy(normalized_verified_Voltage_3D_endo)
    BWa_endo[BWa_endo<args.threshold] = 0
    BWa_endo[BWa_endo!=0] = 1 
    # Connected Components
    R_epi = bwlabeln(BWa_epi,connectivity=3)
    #
    R_epi_tau = bwlabeln(BWa_epi_tau,connectivity=3)
    #
    R_endo = bwlabeln(BWa_endo,connectivity=3)
    #
    return R_epi, R_epi_tau, R_endo

def ConnectWavesTime(R_epi,PreviousR_epi,TotalNumberOfWaves,T):
    # Get labels in current and previous frame
    labels_epi = np.unique(R_epi[R_epi!=0])
    previous_labels = np.unique(PreviousR_epi[PreviousR_epi!=0])
    #
    outputMatrix = np.empty(shape=(0,5),dtype = np.int16)# Columns: t, wave, x,y,z
    assignment=np.zeros_like(labels_epi,dtype=int)*np.nan
    sizeWaves = np.zeros_like(assignment)
    for N_waves in range(1,len(labels_epi)+1):
        # Wave connection portion
        r, c, v= np.where(R_epi == N_waves)
        sizeWaves[N_waves-1]=len(r)
        #
        toWrite= np.asarray([np.ones_like(r)*T,np.ones_like(r)*N_waves,r, c, v]).T
        outputMatrix = np.append(outputMatrix,toWrite,axis=0)
        #
        overlap = np.asarray([np.sum((R_epi==N_waves)*(PreviousR_epi==cwav)) for cwav in previous_labels]) # overlap between cwav and all others in next time step
        match = np.where(overlap!=0)[0]
        #
        # If there is only one match
        if len(match)==1:
            cwav = previous_labels[match[0]] 
        elif len(match)>1: # IF there are multiple overlaps
            # find maximum overlap
            maxOL=np.where(overlap[match]==np.max(overlap[match]))[0][0]
            cwav = previous_labels[match[maxOL]] 
        else:
            cwav=None
        # else:
        #     print('new wave?')
            # Assign new wave
        #
        if not np.isin(cwav,assignment) and cwav is not None:
            assignment[N_waves-1]=cwav
            print('t = %d, wave %d assigned to wave %d'%(T,N_waves,cwav))
        elif cwav is not None:
            ind = np.where(assignment==cwav)[0]
            if len(r)>sizeWaves[int(ind)]:
                assignment[N_waves-1]=cwav #Change code if new wave is larger
                assignment[ind] = np.nan
                print('t = %d, wave %d assigned to wave %d'%(T,N_waves,cwav))
                print('t = %d, wave %d reassigned to new wave'%(T,N_waves))
            #Else: do nothing, will check later
        #
        if len(r)*len(c)*len(v)< 10 and len(r)*len(c)*len(v) > 5:
            print('Run detect BTs')
    #
    # Check if any waves were generated
    ii = np.isnan(assignment)
    if np.sum(ii)!=0:
        for j in np.where(ii)[0]:
            TotalNumberOfWaves = TotalNumberOfWaves + 1
            assignment[j]=TotalNumberOfWaves
            print('t = %d, new wave %d created'%(T,TotalNumberOfWaves))
    #
    # Put into file
    # Convert output matrix to new wave indexes
    tmp_WaveInds= np.zeros(outputMatrix.shape[0])
    tmp_R_epi = np.zeros_like(R_epi)
    for i,wave in enumerate(labels_epi):
        inds = outputMatrix[:,1]==wave
        tmp_WaveInds[inds] = int(assignment[i])
        tmp_R_epi[R_epi==wave] = int(assignment[i])
    outputMatrix[:,1] = tmp_WaveInds
    R_epi = tmp_R_epi
    #
    #
    return R_epi,outputMatrix

def BackwardConnectWavesTime(Data):
    # This code is essentially the same as going forward, but expects a matrix with the
    # structure t, w, x,y,z, where w is the assigned wave codes, x y z are its positions 
    # and t is time
    Data_r = np.flipud(Data)
    BackwardTrackedData = np.copy(Data_r)
    times_r = np.flipud(np.unique(Data_r[:,0]))
    ## Origin
    ox,oy,oz = np.min(np.array([np.min(Data[:,2:],axis=0),np.min(Data[:,2:],axis=0)]),axis=0)
    #
    ## Size
    lx,ly,lz = np.max(np.array([np.max(Data[:,2:],axis=0),np.max(Data[:,2:],axis=0)]),axis=0)-np.array([ox,oy,oz])
    #
    NWaves = Data_r[:,1].max()+1
    for i,t in enumerate(times_r[:-1]):
        #Get indexes and make a matrix for the current and previous time steps
        ## Current
        indsCurrent = Data_r[Data_r[:,0]==t,:]
        wavesCurrent = indsCurrent[:,1]
        indsCurrent = indsCurrent[:,2:]
        #
        ## Previous
        indsPrevious = Data_r[Data_r[:,0]==t-1,:]
        wavesPrevious = indsPrevious[:,1]
        indsPrevious = indsPrevious[:,2:]
        sizePrevious = [np.sum(wavesPrevious==wave) for wave in np.unique(wavesPrevious)]
        #
        # Make matrices of same size for comparison
        ## Put wave information into matrices
        matrixCurrent = np.zeros(shape = (lx+1,ly+1,lz+1))
        matrixCurrent[indsCurrent[:,0]-ox,
                    indsCurrent[:,1]-oy,
                    indsCurrent[:,2]-oz] = wavesCurrent
        wavesCurrent = np.unique(wavesCurrent)
        #
        matrixPrevious = np.zeros_like(matrixCurrent)
        matrixPrevious[indsPrevious[:,0]-ox,
                        indsPrevious[:,1]-oy,
                        indsPrevious[:,2]-oz] = wavesPrevious
        wavesPrevious = np.unique(wavesPrevious)
        #
        # Calculate overlap, do same procedure as in ConnectWavesTime        
        # NOTE: the codes of the waves will be different from the originals
        assignment=np.zeros_like(wavesPrevious,dtype=int)*np.nan
        # Overlap over waves in the previous time instant, to compare with current ones
        for j,pwav in enumerate(wavesPrevious):
            overlap = np.asarray([np.sum((matrixPrevious==pwav)*(matrixCurrent==cwav)) for cwav in wavesCurrent]) # overlap between cwav and all others in next time step
            match = np.where(overlap!=0)[0]            
            # Get the code of the matching wave
            if len(match)==1:
                cwav = wavesCurrent[match[0]] 
            elif len(match)>1:
                # find maximum overlap
                maxOL=np.where(overlap[match]==np.max(overlap[match]))[0][0]
                cwav = wavesCurrent[match[maxOL]] 
            else:
                continue
            # Check whether the wave has been assigned before, if so choose the one with 
            # largest overlap
            if not np.isin(cwav,assignment):
                assignment[j]=cwav
                print('backwards t = %d, wave %d assigned to wave %d'%(t,pwav,cwav))
            else:
                ind = np.where(assignment==cwav)[0]
                if sizePrevious[j]>sizePrevious[int(ind)]:
                    assignment[j]=cwav #Change code if new wave is larger
                    assignment[ind] = np.nan
                    print('backwards t = %d, wave %d assigned to wave %d'%(t,pwav,cwav))
                    print('backwards t = %d, wave %d reassigned to new wave'%(t,wavesPrevious[ind]))
        #
        # Check if any waves were generated
        ii = np.isnan(assignment)
        if np.sum(ii)!=0:
            for j in np.where(ii)[0]:
                assignment[j]=NWaves
                print('backwards t = %d, new wave %d created'%(t,NWaves))
                NWaves = NWaves + 1
        #
        # Replace the waves in the dataframe
        assignment = np.array(assignment,dtype=int)
        for ii in range(len(assignment)):
            BackwardTrackedData[BackwardTrackedData[:,1]==wavesPrevious[ii],1] = assignment[ii]
        #
        #
    # Order waves
    BackwardTrackedData = np.flipud(BackwardTrackedData)
    # 
    CodeForwards = Data[0,1]
    IndForwards = 1
    CodeBackwards = BackwardTrackedData[0,1]
    IndBackwards = 1
    for i in range(BackwardTrackedData.shape[0]):
        # Backwards
        if BackwardTrackedData[i,1]==CodeBackwards:
            BackwardTrackedData[i,1]= IndBackwards
        else:
            IndBackwards+=1
            CodeBackwards = BackwardTrackedData[i,1]
            BackwardTrackedData[i,1] = IndBackwards
        # Forward
        if Data[i,1]==CodeForwards:
            Data[i,1]= IndForwards
        else:
            IndForwards+=1
            CodeForwards = Data[i,1]
            Data[i,1] = IndForwards    
    return BackwardTrackedData, Data
#%% Main

# def main(args):
#%%
BT_temp = 0
BT_unSucc_temp=0
important_bt_temp=0

## Load anatomy and segment

Anatomy,_ = igb.Load(args.anatomy)
Anatomy = np.swapaxes(Anatomy,0,-1)

Anatomy_binary_RA = Anatomy == 113
Anatomy_binary_LA = Anatomy == 114
Anatomy_binary_Fibers = Anatomy == 116
Anatomy_binary_BB = Anatomy == 117
Anatomy_binary_Fibrosis = Anatomy == 250
Anatomy_binary = Anatomy_binary_RA + Anatomy_binary_LA + Anatomy_binary_Fibers+Anatomy_binary_Fibrosis+Anatomy_binary_BB

## % Replace zeros with NaN. 
# This is very important when you want to used bwlabeln command. 
# If zeros are not replaced with NaN the function will detect them as a group of elements

Anatomy_binary[Anatomy_binary != 1]=0
Anatomy_NaN = np.array(Anatomy_binary,dtype =float)
Anatomy_NaN[Anatomy_NaN != 1] = np.nan

#%% 
## This section is to separate the epicardial surface (every element which 
# doesnot belong to endocardial bundles) and endocardial surface

xx,yy,zz = np.where(Anatomy_binary)
coordinates = [xx,yy,zz]

Anatomy_binary_epicardiumSurface = np.isin(Anatomy,[113,114,116,117,119,250])
Anatomy_binary_endocardiumSurface = np.isin(Anatomy,[115])

Anatomy_binary_epi = np.array(Anatomy_binary_epicardiumSurface,dtype =float)
Anatomy_binary_endo = np.array(Anatomy_binary_endocardiumSurface,dtype =float)

## Loop over sim time to 

# for T in range(0,7490):

tString = '%s:%s:%s'%(args.tBegin,args.tInt,args.tEnd)
count_epi = []
count_epi_tau = []
count_endo = []

WavesFile = open(args.fout_wave,'wb')
header = 't wave x y z'
WavesFile.write(header.encode())
with subprocess.Popen(["gzip","-dc",args.vm_iga],stdout=subprocess.PIPE) as gz:
    with subprocess.Popen([args.iga2igb,"--ds","5",
            "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
        hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)
        # size of a single slice in time
        nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
        dtype = np.short
        csize = nx*ny*nz*dtype().nbytes
        # Create buffer with first tau values
        Buffer = np.zeros((nx,ny,nz,args.tau))
        for i in range(args.tau):
            vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
            Buffer[:,:,:,i]= np.swapaxes(vals,0,2)*hdr['facteur'] + hdr['zero']
        # V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
        # V_tau_next = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
        #
        for T in range(args.tBegin,args.tBegin+hdr['t']-args.tau):

            V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
            V_tau = np.swapaxes(V_tau,0,2)* hdr['facteur'] + hdr['zero']
            V = Buffer[:,:,:,0]
            #
            #Connect waves in space
            R_epi, R_epi_tau, R_endo = ConnectWavesSpace(V,V_tau,Anatomy_binary_epi,Anatomy_binary_endo)
            #
            # Store some information
            labels_epi = np.unique(R_epi[R_epi!=0])
            count_epi.append(len(labels_epi))
            print(labels_epi)
            #
            labels_epi_tau = np.unique(R_epi_tau[R_epi_tau!=0])
            count_epi_tau.append(len(labels_epi_tau))
            #
            labels_endo = np.unique(R_endo[R_endo!=0])
            count_endo.append(len(labels_endo))
            #
            # Wave Connection over time
            if T==args.tBegin:
                previous_labels = labels_epi
                PreviousR_epi = R_epi
                TotalNumberOfWaves = len(previous_labels)
            #
            R_epi,outputMatrix = ConnectWavesTime(R_epi,PreviousR_epi,TotalNumberOfWaves,T)
            #
            outputMatrix.astype(np.int32).tofile(WavesFile)
            # At end, prepare for next round
            # previous_labels = assignment.astype(int)
            PreviousR_epi = R_epi
            Buffer = np.roll(Buffer,-1,axis=-1)
            Buffer[:,:,:,-1]=V_tau
WavesFile.close()
    #
    '''
    #%% Backward connection of waves
    # Read from file that was generated above
    fi = open(args.fout_wave,'rb')
    header = fi.read(len(header.encode()))
    Data = np.fromfile(fi,np.int32).reshape(-1,5)
    # Correct indexes both forwards and backwards
    BackwardTrackedData, Data = BackwardConnectWavesTime(Data)
    #
    # Check if everything is correct
    waves,nwav1 = np.unique(Data[:,1],return_counts=True)
    waves,nwav2 = np.unique(BackwardTrackedData[:,1],return_counts=True)
    nwav1=np.sort(nwav1)
    nwav2=np.sort(nwav2)
    if np.sum(nwav1!=nwav2)!=0:
        print('There was an issue, a function should be included here to correct it')
    else:
        print('All fine')
        # Save again to file, now with corrected indexes
        WavesFile = open(args.fout_wave,'wb')
        WavesFile.write(header.encode())
        Data.astype(np.int16).tofile(WavesFile)
        WavesFile.close()
    '''


# if __name__ == '__main__':

# %% Init
class options:
    def __init__(self,host='daint'):
        if host=='daint':
            self.fname_in='/users/vgonalve/exec/psdetection/exp906c04_psfinder.mat'
        elif host=='wsl':
            self.anatomy='/mnt/d/vgmar/model_data/anatomy/model30_210325/model24-30-heart-cell.igb'
        elif host=='usi':
            self.tcc_igb='/home/marques/exec/marques/wave/exp906c74_tcc_ds5.igb'
            #self.vm_igb='/home/marques/exec/marques/wave/exp906c74_vm_ds5.igb'
            self.vm_iga='/home/marques/exec/marques/psdetection/exp906c82_vm_afull.iga.gz'
            self.fout_wave='/home/marques/exec/marques/wave/exp906c82_waves_10s.bin'
            # self.fout_bt='/home/marques/exec/marques/wave/bt_locations.txt'
        self.threshold=0.6
        self.tau=10
        self.iga2igb='iga2igb'
        self.tBegin=0
        self.tInt=1
        self.tEnd=500
#
args = options('wsl')
    #
    # main(args)
# %%
'''
def connectComponents3D(volume,nbhood = 26):
    
    #  Finds connected components in a binary 3D space
    #  Parameters:
    #      volume: binary 3D object (np array)
    #      nbhood: neighborhood to be searched (work in progress)
    #      length: to customize 3x3 neighborhoods on the fly
    #  Returns: Dictionary with connected components 
    
    # Define neighborhood
    if nbhood==0:
        kernel = np.ones((3,3,length),np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2)) 
    if nbhood==26:
        kernel = np.ones((3,3,3),np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2))
    else:
        kernel = np.ones(nbhood,np.int8)
        k_i,k_j,k_k = kernel.shape
        center = int(np.floor(k_i/2))
    # 
    # Negate volume
    volume = np.array(volume,np.int8)
    volume[volume!=0] = -1
    #
    # Find points equal to -1
    fi,fj,fk = np.where(volume==-1)
    #
    I,J,K = volume.shape
    aux_volume = np.zeros((volume.shape[0]+center*2,
                            volume.shape[1]+center*2,
                            volume.shape[2]+center*2),dtype = int)
    aux_volume[center:-center,center:-center,center:-center] = volume
    #
    label_list = []
    label = 0
    for _ in range(2): # For some reason needs to run twice; FIXME
        for p in range(0,len(fi)):# For each point:
            #get mini volume
            target = [fi[p]+ center,fj[p]+center,fk[p]+center]
            minivol = aux_volume[target[0]-center:target[0]+center+1,
                                target[1]-center:target[1]+center+1,
                                target[2]-center:target[2]+center+1]
            # Is there a labelled point in the neighborhood?
            if label!=0:
                ch1 = np.isin(minivol,label_list)#np.array([item in minivol for item in label_list])
                if ch1.any():
                    # if Yes: is there more than one label?
                    singular = np.unique(minivol[minivol>0])
                    if singular.size>1:
                        # If yes: assign all points from both labels to the lowest label
                        new_lab = singular.min()
                        for obj in singular:
                            aux_volume[aux_volume==obj] = new_lab
                    elif singular.size!=0:
                        #If there is only one label, assign to it
                        aux_volume[target[0],target[1],target[2]] = int(singular[0])
                else:
                    # If No: assign new label to point
                    label+=1
                    label_list.append(label)
                    aux_volume[target[0],target[1],target[2]] = label    
            else:   
                # If No: assign new label to point
                label+=1 
                label_list.append(label)
                aux_volume[target[0],target[1],target[2]] = label
        #Correct labels afterwards
    outputVolume = aux_volume[center:-center,center:-center,center:-center]
    currentLabels = np.unique(outputVolume[outputVolume!=0])
    for i,label in enumerate(currentLabels):
        outputVolume[outputVolume==label] = i+1
        # print(label,i)
    return  outputVolume, np.unique(outputVolume[outputVolume!=0])



#%% Separate workers
def Reader(args,tString,ProcessingQueue,LoggerQueue):
    # Open Data
    with subprocess.Popen(["gzip","-dc",args.vm_iga],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"--ds","5",
			   "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # Read header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)
            print(hdr)
            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes
            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau))
            for T in range(args.tBegin,args.tBegin+args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)*hdr['facteur'] + hdr['zero']
                if i>0:
                    ProcessingQueue.put(('prep_wave',[T,Buffer[:,:,:,i-1],Buffer[:,:,:,i]]))
                else:
                    ProcessingQueue.put(('prep_wave',[T,None,Buffer[:,:,:,i]]))
            #
            # Start putting information in the ProcessingQueue
            for T in range(args.tBegin+args.tau,hdr['t']):
                # Get next sample
                V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                V_tau = np.swapaxes(V_tau,0,2)* hdr['facteur'] + hdr['zero']
                #
                # Wave Queue
                # Put in Queue
                ProcessingQueue.put(('process',[T,Buffer[:,:,:,0],V_tau]))
                #
                # Roll buffer
                Buffer = np.roll(Buffer,-1,axis=-1)
                Buffer[:,:,:,-1]=V_tau
            # print('Data Sent')
            # Add messages to all workers to kill
            [ProcessingQueue.put(('kill',None)) for i in range(args.n_processes)];

def Processing(ProcessingQueue):
    # Initialize
    count_epi = []
    count_epi_tau = []
    count_endo = []

    WavesFile = open(args.fout_wave,'wb')
    header = 't wave x y z'
    WavesFile.write(header.encode())
    while True:
        QOut = ProcessingQueue.get(True)
        message,data = QOut
        # Normalizing the Vm between 0 and 1
        if message=='process':
            normalized_verified_Voltage_3D = Norm2Vals(V,[0,1])
            normalized_verified_Voltage_3D_tau =  Norm2Vals(V_tau,[0,1])
            #
            # Get relevant values for each portion of anatomy
            normalized_verified_Voltage_3D_epi = normalized_verified_Voltage_3D*Anatomy_binary_epi
            normalized_verified_Voltage_3D_epi_tau = normalized_verified_Voltage_3D_tau*Anatomy_binary_epi
            normalized_verified_Voltage_3D_endo = normalized_verified_Voltage_3D*Anatomy_binary_endo
            #colormaped_image_epi = np.asarray(normalized_verified_Voltage_3D_epi.*255,dtype =np.uint8) + 1
            #normalized_verified_Voltage_3D_endo = normalized_verified_Voltage_3D_endo.*Anatomy_binary_endo;
            #colormaped_image_endo = uint8(normalaized_verified_Voltage_3D_endo.*255) + 1;
            #
            # Voltage Threshold
            BWa_epi = deepcopy(normalized_verified_Voltage_3D_epi)
            BWa_epi[BWa_epi<args.threshold] = 0
            BWa_epi[BWa_epi!=0] = 1 
            #
            BWa_epi_tau = deepcopy(normalized_verified_Voltage_3D_epi_tau)
            BWa_epi_tau[BWa_epi_tau<args.threshold] = 0
            BWa_epi_tau[BWa_epi_tau!=0] = 1
            #
            BWa_endo = deepcopy(normalized_verified_Voltage_3D_endo)
            BWa_endo[BWa_endo<args.threshold] = 0
            BWa_endo[BWa_endo!=0] = 1 
            #
            R_epi = bwlabeln(BWa_epi,connectivity=3)
            labels_epi = np.unique(R_epi[R_epi!=0])
            count_epi.append(len(labels_epi))
            print(labels_epi)
            #
            R_epi_tau = bwlabeln(BWa_epi_tau,connectivity=3)
            labels_epi_tau = np.unique(R_epi_tau[R_epi_tau!=0])
            count_epi_tau.append(len(labels_epi_tau))
            #
            R_endo = bwlabeln(BWa_endo,connectivity=3)
            labels_endo = np.unique(R_endo[R_endo!=0])
            count_endo.append(len(labels_endo))
            #
            # BT Detection section (copy Ali's comments later)
            #
            assignment=np.zeros_like(labels_epi,dtype=int)*np.nan
            sizeWaves = np.zeros_like(assignment)
            outputMatrix = np.empty(shape=(0,5),dtype = np.int16)# Columns: t, wave, x,y,z
            if T==0:
                previous_labels = labels_epi
                PreviousR_epi = R_epi
                TotalNumberOfWaves = len(previous_labels)
            for N_waves in range(1,len(labels_epi)+1):
                # Wave connection portion
                r, c, v= np.where(R_epi == N_waves)
                sizeWaves[N_waves-1]=len(r)
                #
                toWrite= np.asarray([np.ones_like(r)*T,np.ones_like(r)*N_waves,r, c, v]).T
                outputMatrix = np.append(outputMatrix,toWrite,axis=0)
                #
                overlap = np.asarray([np.sum((R_epi==N_waves)*(PreviousR_epi==cwav)) for cwav in previous_labels]) # overlap between cwav and all others in next time step
                match = np.where(overlap!=0)[0]
                #
                # If there is only one match
                if len(match)==1:
                    cwav = previous_labels[match[0]] 
                elif len(match)>1: # IF there are multiple overlaps
                    # find maximum overlap
                    maxOL=np.where(overlap[match]==np.max(overlap[match]))[0][0]
                    cwav = previous_labels[match[maxOL]] 
                else:
                    cwav=None
                # else:
                #     print('new wave?')
                    # Assign new wave
                #
                if not np.isin(cwav,assignment) and cwav is not None:
                    assignment[N_waves-1]=cwav
                    print('t = %d, wave %d assigned to wave %d'%(T,N_waves,cwav))
                elif cwav is not None:
                    ind = np.where(assignment==cwav)[0]
                    if len(r)>sizeWaves[int(ind)]:
                        assignment[N_waves-1]=cwav #Change code if new wave is larger
                        assignment[ind] = np.nan
                        print('t = %d, wave %d assigned to wave %d'%(T,N_waves,cwav))
                        print('t = %d, wave %d reassigned to new wave'%(T,N_waves))
                    #Else: do nothing, will check later
                #
                if len(r)*len(c)*len(v)< 10 and len(r)*len(c)*len(v) > 5:
                    BTdetected = 1
                    # BWa_endo_BT = BWa_endo
                    # [R_BT, count] = connectComponents3D(BWa_endo_BT,26)
                    # R_BT = np.asarray(R_BT + 1,dtype = float)
                    # R_epi_temp = np.zeros_like(R_epi)
                    # R_epi_temp[r,c,v] = 1
                    # R_epi_temp_tau = np.zeros_like(R_epi)
                    print('I am skipping BTs for the time being, but I got one at t=%d'%T)
                    #im = imdilate(R_epi_temp,ones(2,2,2))
                    #im_test = imdilate(R_epi_temp,ones(5,5,5))
                    #BWa_overlap_BTandEndo = im .* BWa_endo_BT
            #
            #
            # Check if any waves were generated
            ii = np.isnan(assignment)
            if np.sum(ii)!=0:
                for j in np.where(ii)[0]:
                    TotalNumberOfWaves = TotalNumberOfWaves + 1
                    assignment[j]=TotalNumberOfWaves
                    print('t = %d, new wave %d created'%(T,TotalNumberOfWaves))
            # Put into file
            # Convert output matrix to new wave indexes
            for i,wave in enumerate(labels_epi):
                inds = outputMatrix[:,1]==wave
                outputMatrix[inds,1] = int(assignment[i])
                R_epi[R_epi==wave] = int(assignment[i])
            #
            outputMatrix.astype(np.int16).tofile(WavesFile)
            #
            # At end
            previous_labels = assignment.astype(int)
            PreviousR_epi = R_epi
        elif message=='kill':
            WavesFile.close()

'''
