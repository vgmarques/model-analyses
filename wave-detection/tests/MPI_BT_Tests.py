#%%
import os,sys
import numpy as np
from mpi4py import MPI

from time import time#
import datetime
import subprocess
from copy import deepcopy


from skimage.measure import label as bwlabeln 
from skimage.morphology import binary_closing as imclosing
from skimage.morphology import binary_dilation as imdilation
from skimage.morphology import binary_erosion as imerosion



upperDir = os.path.dirname(os.path.dirname(os.getcwd()))
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb


def SegmentCellAnatomy(cell):
    nx,ny,nz = cell.shape
    EndoNodes = igb.GetAtrialSurface(cell,
                         innerTypes = [113,114,115,116,117,118,119,250],
                         outerTypes = [104],
                         linear_indexes = True,
                         mode = 'SP')

    EndoSurfaceNodes = np.zeros((nx+1,ny+1,nz+1),dtype=bool)
    EndoSurfaceNodes[igb.Idx2Coord(EndoNodes,(nx+1,ny+1,nz+1))] = True

    return EndoSurfaceNodes
### Stack overflow
import cProfile

def profile(filename=None, comm=MPI.COMM_WORLD):
  def prof_decorator(f):
    def wrap_f(*args, **kwargs):
      pr = cProfile.Profile()
      pr.enable()
      result = f(*args, **kwargs)
      pr.disable()

      if filename is None:
        pr.print_stats()
      else:
        filename_r = filename + ".{}".format(comm.rank)
        pr.dump_stats(filename_r)

      return result
    return wrap_f
  return prof_decorator

#%%
MESSAGES_DICT = {'Wave_detection':1,
                 }

#%%
#@profile(filename="profile_out")
def Reader(args):
    MSG_INFO=0
    MSG_V=1
    MSG_VTAU=2

    print('Reader initialized')
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()


    
    rangesReading = np.round(np.linspace(args.tBegin,args.tEnd,args.n_readers+1)).astype(int)
    rangesReading = np.hstack([rangesReading[:-1].reshape(-1,1),rangesReading[1:].reshape(-1,1)+args.tau])
    rangesReading = rangesReading[rank]

    if rank==args.n_readers-1:
        rangesReading[1] -= args.tau #Last one cannot go over tEnd

    fname_signals = args.vmFile
    tInt = args.tInt

    tBegin = rangesReading[0]
    tEnd = rangesReading[1]
    tString = str(tBegin)+':'+str(args.tInt)+':'+str(tEnd)   
    print('Opening %s'%args.vmFile)
    args.tau = int(args.tau//tInt)

    # Reads the data and puts in queue
    ti = time()
    print('\n%d: started reading'%rank)
    Requests = []
    with subprocess.Popen(["gzip","-dc",args.vmFile],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"-q","--ds",str(args.downRatio),
                "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # read the header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)
            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes

            if tEnd==-1:
                tEnd=args.tBegin+hdr['t']
                print('tEnd: %d'%tEnd)

            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau),int)
            for i in range(args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)

            # Start putting information in the ProcessingQueue   
            
            ww = 0  
            wk = args.n_readers + ww       
            for t in range(0,int((tEnd-tBegin)/tInt)-args.tau):
                V_tau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                # V_tau = deepcopy(tmp)
                V_tau = np.swapaxes(V_tau,0,2)
                V = Buffer[:,:,:,t%args.tau]
                outDict = {#'message':'Wave_detection',
                            'tstep':t*tInt+tBegin,
                            'V':Buffer[:,:,:,t%args.tau],
                            'V_tau':V_tau,
                            'hdr':hdr
                            }
                
                # print('Size: ',np.prod(V.shape)*dtype().nbytes)
                comm.send(outDict, dest=size-2,tag=10)
                # update for next iteration
                # ww = (ww+1)%args.n_processes
                # if len(Requests)==args.n_processes:
                #     for req in Requests: req.wait()
                #     # comm.waitall(Requests)
                #     Requests = []
                # wk = args.n_readers + ww  
                Buffer[:,:,:,(t+args.tau-1)%args.tau]=V_tau
                

    # for i in range(args.n_processes): ProcessingQueue.put(('reader_done',tBegin)) 
    # for wk in range(args.n_readers,args.n_readers+args.n_processes):
    #     comm.isend(None, dest=wk)
    comm.send(None, dest=size-2,tag=10)
    print('\n%d: Time to finish reading: %0.3f'%(rank,time()-ti))


def Test(args):

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    nWorkers = size-2-args.n_readers

    Buffer = []
    AvailableWorkers =set()#np.arange(args.n_readers,args.n_readers+args.n_processes,dtype=int))
    success=False
    freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)
    finished = 0
    WaitForReader = True
    maxBuffer=40
    while True:
        # probe = comm.probe()
        # if not probe: continu       
        success,rank = freeWorkerReq.test()
        if success:
            # rank=freeWorkerReq.wait()
            print('?????',rank)
            AvailableWorkers.add(rank)
            freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)
            success=False

        elif (len(Buffer)==maxBuffer and not success) or not WaitForReader:
            # print('I am in B')
            rank=freeWorkerReq.wait()
            # success,rank = freeWorkerReq.test()
            print('#####',rank)
            AvailableWorkers.add(rank)
            # freeWorkerReq.Free()
            # print(freeWorkerReq)
            freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)


        # Get stuff from readerm add to buffer
        if WaitForReader and len(Buffer)<maxBuffer:
            buf = comm.recv(source=MPI.ANY_SOURCE,tag=10)
            if buf is None:
                
                finished += 1
                print('Finished readers: ',finished)
                if finished==args.n_readers:
                    WaitForReader = False
            else:
                Buffer.append(buf)

        print(len(Buffer))

        # if there are workers, send to them
        while len(Buffer)>0 and len(AvailableWorkers)>0:
            # e =  AvailableWorkers.pop()
            # print('!!!!!!!!!!!!!!!!!!!', e)
            # if e is not None:
            comm.send(Buffer.pop(),dest =AvailableWorkers.pop())

        if not WaitForReader and len(Buffer)==0:
            while len(AvailableWorkers)!=nWorkers-1:
                rank=freeWorkerReq.wait()
                AvailableWorkers.add(rank)
                freeWorkerReq = comm.irecv(None,source=MPI.ANY_SOURCE,tag=11)
                with open('test.txt','a') as fout:
                    fout.write('In: ')
                    fout.write(str(len(AvailableWorkers)))
                    fout.write('/%d\n'%nWorkers)
          

            for wk in range(args.n_readers,size-2):
                comm.isend(None, dest=wk)               #FIXME
            break



#%%
#@profile(filename="profile_out")
def Processing(args,Anatomy_Epi,Anatomy_Endo):
    comm = MPI.COMM_WORLD
    loggerRank = comm.Get_size()-1
    testRank = comm.Get_size()-2
    BT_TAG = 1
    # Dilation kernels for avoiding isolated points
    connectKernel = np.ones((4,4,4),dtype=bool)   
    connectKernelEpi = np.ones((3,3,3),dtype=bool)   
    anatomyShape = Anatomy_Endo.shape
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    workerRanks = np.arange(args.n_readers,size-2,dtype=int)
    workerRanks = workerRanks[~np.isin(workerRanks,[rank])]
    # print(workerRanks)
    
    #Listening
    buf=None
    # request_recv = comm.irecv(buf,source=MPI.ANY_SOURCE, tag=BT_TAG) #tag=MESSAGE_TEST,
    # comm.isend(rank, dest=testRank,tag=11).wait()
    while True:
        # probe = comm.probe()
        # if not probe: 
        print('%d: Waiting to send'%rank)
        comm.isend(rank, dest=testRank,tag=11).wait()
            # continue
        print('%d: sent'%rank)
        # First, try to receive a BT_detection message from the processors
        # print('Receive: ', request_recv.test()[0])
        # test,msg = request_recv.test()
        # if not test: # if there is no BT yet (they have priority)
        #     # Get message from reader
        status = MPI.Status()
        inDict = comm.recv(source=testRank, status=status) #tag=MESSAGE_TEST,  
        # inDict = comm.recv(np.array((400,400,400),np.short), source=MPI.ANY_SOURCE).wait()   
        source_rank = status.Get_source()
        # else:
        # inDict=deepcopy(msg)
        # print('Message: ',inDict['message'])
        # request_recv = comm.irecv(buf,source=MPI.ANY_SOURCE, tag=BT_TAG) #tag=MESSAGE_TEST,
        # request_recv.Free()
        if inDict is None:
            print('%d; I got the finish message'%rank)
            # finished += 1
            # if finished ==args.n_readers:
            comm.send(None, dest=loggerRank) #FIXME
            break
            # else:
            #     continue

        # if inDict['message'] == 'Wave_detection':


        ## Get the voltages at V and V+tau
        t       = inDict['tstep']
        V_temp  = inDict['V']
        V_tau   = inDict['V_tau']
        hdr     = inDict['hdr'] 


        waveTh = (args.waveThreshold-hdr['zero'])/hdr['facteur'] 
        BTTh = (args.BTThreshold-hdr['zero'])/hdr['facteur'] 

        # Positions exceeding thresholds for wave and bt detection (-60 / -20 mV)
        xWave,yWave,zWave = np.where(V_temp>=waveTh) # wave
        xBT,yBT,zBT = np.where(V_temp>=BTTh) # wavefront
        V_temp=[] # memory
        
        # Positions exceeding thresholds for wave detection (-60 mV) for the t+tau
        xWave_tau,yWave_tau,zWave_tau = np.where(V_tau>=waveTh)
        V_tau=[] # memory

        # Wave detection (times t and t+tau)
        V = np.zeros(anatomyShape,dtype=bool)
        V[xWave,yWave,zWave] = 1
        
        V_tau = np.zeros(anatomyShape,dtype=bool)
        V_tau[xWave_tau,yWave_tau,zWave_tau] = 1

        # wavefront for BT detection (only at time t)
        V_BT = np.zeros(anatomyShape,dtype=bool)
        V_BT[xBT,yBT,zBT] = 1


        # Epi layer
        # Get potentials at the surfaces of interest

        V_3D_Epi = V*Anatomy_Epi#np.logical_and(V,Anatomy_Epi)#V*Anatomy_Epi
        V_BT_Epi = V_BT*Anatomy_Epi#np.logical_and(V_BT,Anatomy_Epi)#V_BT*Anatomy_Epi

        # Dilate a bit to avoid connection problems
        V_3D_Epi = imdilation(V_3D_Epi,connectKernelEpi)

        # Connected components analysis in Epi layer
        R_Epi = bwlabeln(V_3D_Epi,connectivity=3,background=0,return_num=False) # was true before
        R_mask = imerosion(R_Epi,connectKernelEpi)
        R_Epi = R_mask*R_Epi*Anatomy_Epi
        
        uniqueWavesEpi,sizeWavesEpi = np.unique(R_Epi[R_Epi!=0],return_counts=True)
        NumberOfEpiWaves = len(uniqueWavesEpi)

        # Endo layer
        '''
        The trick here is that a single layer in low resolution often yields broken waves
        Thus, I dilate the waves to be approximately the original resolution, do the 
        connected component analysis and then erode again

        The resulting eroded waves are already marked with the correct label, even if in the 
        final version they are not "connected"

        When checking the overlap, that is not an issue because we merge with the full low-res
        atrium, which will give a single "wave block" in case there is a BT
        '''
        ## Get potentials at the surfaces of interest
        V_3D_Endo = V*Anatomy_Endo#np.logical_and(V,Anatomy_Endo)#
        V_BT_Endo = V_BT*Anatomy_Endo#np.logical_and(V_BT,Anatomy_Endo)#

        V_3D_Endo = imdilation(V_3D_Endo,connectKernel)

        # Connected Components endo layer
        R_Endo = bwlabeln(V_3D_Endo,connectivity=3,background=0,return_num=False)
        R_mask = imerosion(R_Endo,connectKernel)
        R_Endo = R_mask*R_Endo*Anatomy_Endo #Get only points at anatomy
        uniqueWavesEndo,sizeWavesEndo = np.unique(R_Endo[R_Endo!=0].flatten(),return_counts=True)
        NumberOfEndoWaves = len(uniqueWavesEpi)

        # Save number of waves
        # LoggerQueue.put(('saveWaves',[t,NumberOfEndoWaves,NumberOfEpiWaves]))
        loggerNessage = ('wave',[t,NumberOfEndoWaves,NumberOfEpiWaves])
        comm.isend(loggerNessage, dest=loggerRank)
        # Check if any endocardial wave is within range. If not, just continue
        # Step 1: check if a small wave appeared on the chosen layer
        sizeBT_Endo = ((sizeWavesEndo<=80)&(sizeWavesEndo>0))

        if sizeBT_Endo.any():
            waveCodes = uniqueWavesEndo[sizeBT_Endo]

            # Get info from t+tau layer
            V_tau_3D_Endo = np.logical_and(V_tau,Anatomy_Endo)
            V_tau_3D_Endo = imdilation(V_tau_3D_Endo,connectKernel)     

            R_tau_Endo, _ = bwlabeln(V_tau_3D_Endo,connectivity=3,background=0,return_num=True)
            R_mask = imerosion(R_tau_Endo,connectKernel)
            R_tau_Endo = R_mask*R_tau_Endo*Anatomy_Endo

            #Because the wave labels start at 1
            # print('########## I am trying to send a BT message\n')
            for waveCode in waveCodes:
                r,c,v = np.where(R_Endo==waveCode)
                #FIXME: the data is too large to pass via these messages without annoying issues. maybe make linear already
                # Check 
                outDict = {#'message':'BT_detection',
                        'tstep':t,
                        'waveCode':waveCode,
                        'waveCoords':[r,c,v],
                        'V_BT':V_BT_Epi,
                        'R_tau':R_tau_Endo,
                        'layer':'endoBT',
                        'comm':comm,
                        'anatomyShape':anatomyShape,
                        'connectKernel':connectKernel}
                BTDetection(outDict,loggerRank)
                # request_send = comm.isend(outDict, dest=workerRanks[wI], tag=BT_TAG)
                # print(request_send.test())
                # request_send.Wait()         
                # request_send.Free()           
                # wI = (wI+1)%len(workerRanks)
                # print('########## I am trying to send a BT message to %d\n'%workerRanks[wI])


        # Check if any epicardial wave is within range. If not, just continue
        # Step 1: check if a small wave appeared on the chosen layer
        sizeBT_Epi = ((sizeWavesEpi>=5)&(sizeWavesEpi<=150))
        if sizeBT_Epi.any():
            # nWavesEpi = len(uniqueWavesEpi)-1
            waveCodes = uniqueWavesEpi[sizeBT_Epi]

            # Get info from t+tau layer
            V_tau_3D_Epi = np.logical_and(V_tau,Anatomy_Epi)#V_tau*Anatomy_Epi
            
            V_tau_3D_Epi = imdilation(V_tau_3D_Epi,connectKernelEpi)                # Dilate a bit to avoid connection problems and connect waves
            R_tau_Epi = bwlabeln(V_tau_3D_Epi,connectivity=3,background=0)
            R_mask = imerosion(R_tau_Epi,connectKernelEpi)
            
            R_tau_Epi = R_mask*R_tau_Epi*Anatomy_Epi
            

            for waveCode in waveCodes:
                r,c,v = np.where(R_Epi==waveCode)
                # ProcessingQueue.put(('BT_detection_Epi',(t,waveCode,[r,c,v],V_BT_Endo,R_tau_Epi)))
                outDict = {#'message':'BT_detection',
                        'tstep':t,
                        'waveCode':waveCode,
                        'waveCoords':[r,c,v],
                        'V_BT':V_BT_Endo,
                        'R_tau':R_tau_Epi,
                        'layer':'endoBT',
                        'comm':comm,
                        'anatomyShape':anatomyShape,
                        'connectKernel':connectKernelEpi}
                BTDetection(outDict,loggerRank)
        # comm.send(rank, dest=testRank,tag=11)

#@profile(filename="profile_out")                 
def BTDetection(inDict,loggerRank):

    # print('########## I received a BT message\n')
    # loggerMessage = '\nlayer: %s'%inDict['layer']

    ## Get the voltages at V and V+tau
    t       = inDict['tstep']
    waveCode  = inDict['waveCode']
    r,c,v   = inDict['waveCoords']
    V_BT   = inDict['V_BT']
    R_tau     = inDict['R_tau'] 
    layer     = inDict['layer'] 
    anatomyShape     = inDict['anatomyShape'] 
    connectKernel     = inDict['connectKernel'] 
    # overlappingWave = inDict['overlappingWave']
    ### Endo Epi BTs
    sizeEpi=len(r)

    # # Step 2: Check if the detected wave overlapped with any wavefront on the other layer
    R_temp_NB = np.zeros(anatomyShape,dtype=bool)
    R_temp_NB[r,c,v] = True
    R_temp_NB = imdilation(R_temp_NB,connectKernel)   
    #
    Overlap = R_temp_NB*V_BT
    overlappingWave = np.unique(Overlap)
    
    if len(overlappingWave) >1: # Overlap happened (one wave (the selected)+all in other layer)
        # Step 3: Check if the wave grew when compared to the time t+tau
        TimeOverlapEpi = imerosion(R_temp_NB,connectKernel)   
        futureWave = np.unique(TimeOverlapEpi*R_tau)
        if len(futureWave)!=2:
            if len(futureWave)>2: print('Wave split, check t=%d, wave %d'%(t,waveCode))
            # continue
        elif len(futureWave)==2:
            futureWave = futureWave[1]
        sizeEpi_tau = np.sum(R_tau==futureWave)

        if sizeEpi_tau>=10*sizeEpi: 
            if  sizeEpi_tau<=25*sizeEpi:
                VIBT=1
            else:
                VIBT=0
            r = np.round(np.median(r))
            c = np.round(np.median(c))
            v = np.round(np.median(v))
            loggerMessage = (layer,[t,r,c,v,VIBT])#'\nlayer: %s'%layer

            comm.isend(loggerMessage, dest=loggerRank)
            # LoggerQueue.put(('epiBT',[t,r,c,v,VIBT]))




#%%
#@profile(filename="profile_out")
def Logger(args):
    ti = time()
    comm = MPI.COMM_WORLD
    nWorkers = comm.Get_rank()-2-args.n_readers
    rank = comm.Get_rank()
    print('Logger Rank: ',rank)


    MESSAGE_LOG=2
    finishedProcessors = 0

    foutBT =  open(args.outputFile,'wb')
    foutBT.write(b't,x,y,z,l,v')
    foutWave = open(args.outputFile.split('.btd')[0]+'.wavecount','wb')
    foutWave.write(b't,endo,epi')

    while True:
        probe = comm.probe()
        if not probe: continue

        message = comm.recv(source=MPI.ANY_SOURCE)
        if message is None:
            finishedProcessors += 1
            if finishedProcessors==nWorkers:
                break
            else:
                continue

        layer,message = message
        if message is not None and layer!='wave':
            
            t,r,c,v,VIBT = message
            if layer=='endoBT':
                layer=0
            elif layer=='epiBT':
                layer=1
            if not np.isnan((t,r,c,v,VIBT)).any():
                foutBT.write(b'\n%d,%d,%d,%d,%d,%d'%(t,r,c,v,layer,VIBT))
                foutBT.flush()
        elif message is not None and layer=='wave':
            t,nEndo,nEpi = message
            foutWave.write(b'\n%d,%d,%d'%(t,nEndo,nEpi))
            foutWave.flush()

        else:
            finishedProcessors +=1
        if finishedProcessors==nWorkers:
            foutBT.flush()
            break
    foutBT.close()
    foutWave.close()
    SortOutput(args.outputFile)
    SortOutput(args.outputFile.split('.btd')[0]+'.wavecount')
    print('\n Total elapsed tine; %0.3f s'%(time()-ti))

def SortOutput(filename):       
    with open(filename,'rb') as fi:
        input = fi.read().splitlines()
        header = input[0]
        lines =np.asarray([L.decode().split(',') for L in input[1:]],dtype=int)
        lines = lines[np.argsort(lines[:,0])]
    with open(filename,'wb') as fout:
        fout.write(header)
        for L in lines:
            fout.write(b'\n')
            for eI,element in enumerate(L):
                if eI!=len(L)-1:
                    fout.write(b'%d,'%element)
                else:
                    fout.write(b'%d'%element)


if __name__ == '__main__':

    ### Debug
    class options:
        def __init__(self):
            self.tccFile='/users/vgonalve/exec/anatomy/model30/exp700c00_tcc_afull.igb' #/home/vgma rques/data/model-data/debug/' # We use tcc because the data is in nodes, not cells
            self.anatomyFile='/users/vgonalve/exec/anatomy/model30/model24-30-heart-cell.igb'
            self.vmFile='/users/vgonalve/exec/WaveDetection/exp906a05_vm_afull.iga.gz'
            self.outputFile='./out_test.btd' #!
            self.n_processes= 5
            self.n_readers= 4
            self.downRatio = 2 #Previously: 5
            self.BTThreshold=-20
            self.waveThreshold=-60
            self.tau=10
            self.iga2igb='iga2igb'
            self.tBegin=0
            self.tInt=1
            self.tEnd=200
    
    args = options()

    ###


    # MPI.Init()
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    # nReaders = 1
    # nWorkers = size-nReaders-1
    loggerRank = size-1 #args.n_readers+args.n_processes
    
    sys.stdout.write(
        "Hello, World! I am process %d of %d\n"
        % (rank, size))


    # print(rank)
    if rank < args.n_readers:
        with open('test.txt','a') as fout:
            fout.write('%d\n'%rank)
            if rank==0:
                fout.write(str(datetime.datetime.now()))
        Reader(args)

    elif rank>args.n_readers and rank<size-2:
        with open('test.txt','a') as fout:
            fout.write('%d\n'%rank)
            if rank==0:
                fout.write(str(datetime.datetime.now()))
        ti = time()
        #####
        print('Loading anatomy')

        Anatomy_tcc,_ = igb.Load(args.tccFile)
        Anatomy_tcc = np.swapaxes(Anatomy_tcc,0,-1)

        ## Separate bundles from rest 
        Anatomy_NoBundles = np.isin(Anatomy_tcc,[113,114,116,117,119,250])
        Anatomy_Bundles = np.isin(Anatomy_tcc,[115])    

        # #% Load anatomy from cell file and segment endo and epi surfaces
        AnatomyCell,_ = igb.Load(args.anatomyFile)
        AnatomyCell = np.swapaxes(AnatomyCell,0,-1)
        AnatomyCell = AnatomyCell[:,:,:350]

        EndoNodes = SegmentCellAnatomy(AnatomyCell)

        # Get bundles and merge with endocardium, get everything else and merge with epicardium
        Anatomy_Epi = np.logical_and(Anatomy_NoBundles,~EndoNodes)
        Anatomy_Endo = np.logical_or(EndoNodes,Anatomy_Bundles)

        downRatio = args.downRatio
        Anatomy_Endo = np.logical_and(Anatomy_Endo[::downRatio,::downRatio,::downRatio],
                                    Anatomy_tcc[::downRatio,::downRatio,::downRatio]).astype(bool)
        Anatomy_Epi = Anatomy_Epi[::downRatio,::downRatio,::downRatio].astype(bool)
        Anatomy_tcc = [] # Free memory
        AnatomyCell = [] # Free memory
        print('Anatomy loaded')

        ####
        print('\n Time read anatomy: %0.3f'%(time()-ti))

        # processing(comm,rank,args.n_readers,loggerRank)
        Processing(args,Anatomy_Epi,Anatomy_Endo)
    elif rank==size-2:
        Test(args)
    elif rank==size-1: 
        Logger(args)

    with open('test.txt','a') as fout:
        if rank==0:
            fout.write(str(datetime.datetime.now()))
    #MPI.Finalize()
