#%% 
import os,sys
import numpy as np
import subprocess
from copy import deepcopy
from skimage.measure import label as bwlabeln 
from skimage.morphology import binary_dilation as imdilation
import matplotlib.pyplot as plt
import multiprocessing as mp
import pickle
# import scipy.io as sio



upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import quick_visualization as qv
from WaveCalculators import Graph

#%% Functions

def ConnectWavesSpace(V,VTau,AnatomyBinary):
    # Voltage Threshold
    V_b = deepcopy(V)*AnatomyBinary
    V_b[V_b<args.threshold] = 0
    V_b = V_b.astype(bool)
    #
    VTau_b = deepcopy(VTau)*AnatomyBinary
    VTau_b[VTau_b<args.threshold] = 0
    VTau_b = VTau_b.astype(bool)
    # Connected Components
    Waves = bwlabeln(V_b,connectivity=3)
    #
    WavesTau = bwlabeln(VTau_b,connectivity=3)
        #
    return Waves, WavesTau

def ConnectWavesTime(Waves,WavesTau):
    #
    currentLabels = np.unique(Waves[Waves!=0])
    nextLabels = np.unique(WavesTau[WavesTau!=0])
    edgeList = []
    for cL in currentLabels:
        # Dilate current wave, check overlap with next wave
        WaveDil = deepcopy(Waves)
        WaveDil[WaveDil!=cL]=0
        WaveDil = imdilation(WaveDil.astype(bool),np.ones((5,5,5)))
        #
        # Find overlapping labels
        overlap = np.logical_and(WaveDil,WavesTau.astype(bool))
        overlapLabels,labelCounts = np.unique(WavesTau[overlap],return_counts=True)
        #
        # if len(overlapLabels)==1:
        #     edgeList.append([cL,overlapLabels[0]])
        # elif len(overlapLabels)>1:
        #     lInd = np.where(labelCounts==np.max(labelCounts))[0][0]
        #     edgeList.append([cL,overlapLabels[lInd]])
        if len(overlapLabels)>=1:
            for L in overlapLabels:
                edgeList.append([cL,L])

        else:
            edgeList.append([cL,0])
    #
    return edgeList

def MakeAndSaveGraph(args,nodeCodes,edgeList):

    nodeIndices = np.asarray(list(nodeCodes.keys()))
    connectionMatrix = np.zeros((len(nodeIndices),len(nodeIndices)),bool)

    lastValidTime = np.max(list(edgeList.keys()))-args.tau
    T0 = np.min(list(edgeList.keys()))

    # Add graph connections when connected forward (or backwards)
    connections = np.empty((0,2),int)
    #Forward
    for T,edgePairs in edgeList.items():
        if T==lastValidTime: break
        for pair in edgePairs:
            if pair[1]==0: continue

            nodeA = (T,pair[0])
            nodeB = (T+args.tau,pair[1])

            i = np.where([nodeA==val for val in nodeCodes.values()])[0][0]
            j = np.where([nodeB==val for val in nodeCodes.values()])[0][0]
            connections = np.vstack([connections,[i,j]])
    # Make Graph and group continuous waves
    G = Graph(connections,size = len(nodeIndices),connectionType = 'directional',connect = False) #weak to account for backwards
    ContinuousWaves = G.GetAllConnectedGroups()


    # Save wave groups and node codes, which are necessary to reconstruct the connections
    with open(os.path.join(args.outPath,args.experimentName+'.wavegraph'), 'wb') as output:
        pickle.dump(nodeCodes, output)
        pickle.dump(ContinuousWaves, output)
        pickle.dump(G, output)
    print('Saved graph data')


#%% Reader
def Reader(args,ProcessingQueue,LoggerQueue):
    ## Loop over sim time to 
    tString = '%s:%s:%s'%(args.tBegin,args.tInt,args.tEnd)

    with subprocess.Popen(["gzip","-dc",args.vm_iga],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"--ds","5",
                "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)

            LoggerQueue.put(('initialize',[hdr]))
            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes
            # Create buffer with first tau values
            Buffer = np.zeros((nx,ny,nz,args.tau))
            for i in range(args.tau):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                Buffer[:,:,:,i]= np.swapaxes(vals,0,2)*hdr['facteur'] + hdr['zero']
            #
            vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
            #
            nodeCodes = {}
            edgeLists = {}
            edgeListsBackwards = {}
            nodeInd = 0
            for T in range(args.tBegin,args.tBegin+hdr['t']-args.tau-1):
                VTau = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                VTau = np.swapaxes(VTau,0,2)* hdr['facteur'] + hdr['zero']
                V = Buffer[:,:,:,0]

                # Send to processing
                ProcessingQueue.put(('WaveConnection',[T,V,VTau,hdr]))
            
                # Roll and next iteration
                Buffer = np.roll(Buffer,-1,axis=-1)
                Buffer[:,:,:,-1]=VTau

    for i in range(args.n_processes): ProcessingQueue.put(('kill',None))

#%% Processing

def Processing(args,Anatomy_ds,ProcessingQueue,LoggerQueue):

    #Listening
    while True:
        # Get message from queue
        QOut = ProcessingQueue.get(True)
        message,data = QOut

        # Choose action
        if message=='initialize':
            print('Worker initialized')
        elif message=='kill':
            print('Worker killed') # can be passed to logger to save 
            LoggerQueue.put(('process_done',None))
            break

        elif message=='WaveConnection':
            ## Get the voltages at V and V+tau
            T,V,VTau,hdr = data 
            Waves, WavesTau = ConnectWavesSpace(V,VTau,Anatomy_ds)

            # Send waves for putting in Igb
            LoggerQueue.put(('MakeWaves',[T,Waves]))
            # LoggerQueue.put(('MakeIGB',[T,Waves]))
            #
            currentLabels = np.unique(Waves[Waves!=0])
            # Save coordinates of wave types as (T,W)
            LoggerQueue.put(('NodeCodes',[T,currentLabels]))

            # Create edges to future graph
            edges = ConnectWavesTime(Waves,WavesTau)
            LoggerQueue.put(('EdgeList',[T,edges]))


#%% Logger
def Logger(args,LoggerQueue,anatomyPositions):
    wrapUp = False
    KilledWorkers = 0
    # While loop
    while True:
        if not wrapUp:
            QOut = LoggerQueue.get(True)
        else:
            QOut = LoggerQueue.get(True,timeout=10)
        message,data = QOut
        # If initialize, open data
        if message=='initialize':
            hdr = data[0]
            nx,ny,nz,nt = hdr['x'],hdr['y'],hdr['z'],hdr['t']
            # outputIGB = np.zeros((nx,ny,nz,nt-1),dtype=np.uint8)

            OutputFile =  open(os.path.join(args.outPath,args.experimentName+'.waves'), 'wb')
            # OutputFile.write(b'%d %d %d %d'%(nt-1,nx,ny,nz))
            OutputFile.flush()
            nodeInd=0
            nodeCodes = {}
            
            edgeList = {}

        if message=='process_done':
            KilledWorkers += 1
            wrapUp = True
            if KilledWorkers==args.n_processes:
                LoggerQueue.put(('kill',None))
            
        if message=='MakeIGB':
            T,Waves = data
            outputIGB[:,:,:,T-args.tBegin] = Waves
        if message=='MakeWaves':
            T,Waves = data
            W = Waves[anatomyPositions[:,0],anatomyPositions[:,1],anatomyPositions[:,2]]
            # OutputFile.write(b'\n')
            for w in W:
                OutputFile.write(b'%d '%w)
            OutputFile.write(b'\n')

            OutputFile.flush()

        if message=='NodeCodes':
            T,currentLabels = data
            # Save coordinates of wave types as (T,W)
            for waveCode in currentLabels:
                nodeCodes[nodeInd] =(T,waveCode)
                nodeInd +=1
        if message=='EdgeList':
            T,edges = data
            # Create edges to future graph
            edgeList[T] = edges

        if message=='kill':
            # igb
            hdr_out = deepcopy(hdr)
            hdr_out['t'] = hdr_out['t']-1 
            hdr_out['type'] = 'char'
            hdr_out.pop('zero')
            hdr_out.pop('facteur')
            hdr_out.pop('comments')

            # igb.Write(os.path.join(args.outPath,args.outFileIGB),hdr_out,outputIGB)
            # os.system('gzip',os.path.join(args.outPath,args.outFileIGB))
            OutputFile.close()
            print('Saved IGB data')

            # Nodes and Edges
            MakeAndSaveGraph(args,nodeCodes,edgeList)
            break
    print('Logger killed') # can be passed to logger to save 


#%% Main
def main(args):

    ## Load anatomy and segment

    Anatomy,_ = igb.Load(args.anatomy)
    Anatomy = np.swapaxes(Anatomy,0,-1)

    Anatomy_binary = igb.GetNodesFromCells(Anatomy,[113,114,115,116,117,118,119,250])
    Anatomy_binary = Anatomy_binary[:,:,:351] # atria only

    Anatomy_ds = Anatomy_binary[::5,::5,::5]
    xx,yy,zz = np.where(Anatomy_ds)
    anatomyPositions = np.array([xx,yy,zz]).T

    np.save(os.path.join(args.outPath,'AnatomyPositions.npy'),anatomyPositions)
    print('Starting parallel pool')
    with mp.Manager() as DataManager:
        # Create the queues
        ProcessingQueue = DataManager.Queue(maxsize=4*args.n_processes)#maxsize=20
        [ProcessingQueue.put(('initialize',None))for i in range(args.n_processes)];
        LoggerQueue = DataManager.Queue()#maxsize=20
        # LoggerQueue.put(('initialize',args))

        #Data In
        DataReader = mp.Process(target = Reader,args = (args,ProcessingQueue,LoggerQueue,))
        # Data processing
        ProcessingPool = mp.Pool(args.n_processes,Processing,(args,Anatomy_ds,ProcessingQueue,LoggerQueue,))
        # Data out
        LoggerProcess = mp.Process(target = Logger,args = (args,LoggerQueue,anatomyPositions,))

        LoggerProcess.start()
        # Open and wait for end 
        DataReader.start()
        
        DataReader.join()
        ProcessingPool.close()
        ProcessingPool.join()

if __name__ == '__main__':

    #%% Init
    class options:
        def __init__(self,host='daint'):
            if host=='daint':
                self.experimentName = 'exp906c17'
                self.anatomy='/users/vgonalve/exec/anatomy/model30/model24-30-heart-cell.igb.gz'
                # self.fout_wave='/mnt/d/vgmar/model_data/exp906k05_waves.bin'
                self.vm_iga='/project/s1074/LongAF/Control/%s_vm_afull.iga.gz'%self.experimentName
                self.outPath = '/users/vgonalve/exec/wave-detection'
                self.outFileIGB = self.experimentName+'_Waves.igb'
                self.n_processes = 20

            elif host=='wsl':
                self.experimentName = 'exp906k05_Test'
                self.anatomy='/mnt/d/vgmar/model_data/anatomy/model30_210325/model24-30-heart-cell.igb'
                self.fout_wave='/mnt/d/vgmar/model_data/exp906k05_waves.bin'
                self.vm_iga='/mnt/d/vgmar/model_data/exp906/exp906k05_vm_afull.iga.gz'
                self.outPath = '/mnt/d/vgmar/model_data'
                self.outFileIGB = self.experimentName+'_Waves.igb'
                self.n_processes = 2
            elif host=='usi':
                self.tcc_igb='/home/marques/exec/marques/wave/exp906c74_tcc_ds5.igb'
                #self.vm_igb='/home/marques/exec/marques/wave/exp906c74_vm_ds5.igb'
                self.vm_iga='/home/marques/exec/marques/psdetection/exp906c82_vm_afull.iga.gz'
                self.fout_wave='/home/marques/exec/marques/wave/exp906c82_waves_10s.bin'
                # self.fout_bt='/home/marques/exec/marques/wave/bt_locations.txt'
            self.threshold=-60 # mV
            self.tau=1
            self.iga2igb='iga2igb'
            self.tBegin= 0
            self.tInt=1
            self.tEnd=10000
    #
    args = options('daint')
        #
    main(args)


#%% Visualization an testing

# SimWaves = np.zeros(500)
# fig,ax = plt.subplots(1)
# k = 1

# for wave in ContinuousWaves:
#     waveTimes = [nodeCodes[w][0] for w in wave]
#     ax.plot(waveTimes,np.ones(len(waveTimes))*k,linewidth = 2)
#     waveTimes = np.array(waveTimes)
#     k = k+1
#     SimWaves[waveTimes] += 1


# #% Now I have the "graphs" and the actual positions, time to connect them

# # Get some random positions
# t1 = 250
# Pos1 = np.asarray(np.where(waveIGB[:,:,:,t1-args.tBegin]==1)).T
# t2 = 340
# Pos2 = np.asarray(np.where(waveIGB[:,:,:,t2-args.tBegin]==1)).T

    
# number_of_rows = Pos1.shape[0]
# Idx1= np.random.choice(number_of_rows)
# A = Pos1[Idx1]

# number_of_rows = Pos2.shape[0]
# Idx2= np.random.choice(number_of_rows)
# B = Pos2[Idx2]

# # Get the indexes of these positions and the corresponding wave code
# # In practice, we will get the indexes like the formula below, not the other way around
# waveA = waveIGB[A[0],A[1],A[2],t1-args.tBegin]
# waveB = waveIGB[B[0],B[1],B[2],t2-args.tBegin]


# # Get graph coordinates of each wave
# nodeCodesList = np.array(list(nodeCodes.values()))

# waveAIndex = np.where((nodeCodesList[:,0]==t1) & (nodeCodesList[:,1]==waveA))[0][0]
# waveBIndex = np.where((nodeCodesList[:,0]==t2) & (nodeCodesList[:,1]==waveB))[0][0]


# # Now see if they are connected
# groupA  = np.where([waveAIndex in L for L in ContinuousWaves])[0][0]
# groupB  = np.where([waveBIndex in L for L in ContinuousWaves])[0][0]
# print(groupA,groupB)



# xx,yy,zz = np.where(Anatomy_ds)
# plotAnat = np.array([xx,yy,zz]).T

# signal = waveIGB[xx,yy,zz,t2-args.tBegin].reshape(1,-1)

# AnatomyActor,cbarActor = qv.MakePolyDataActor(plotAnat,signal,vmin=0,cmap='jet')

# qv.QuickRenderWindowInteractor([AnatomyActor],[cbarActor])