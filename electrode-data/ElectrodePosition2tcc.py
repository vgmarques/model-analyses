# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 16:31:46 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import numpy as np
import os,sys
import pickle5 as pickle

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
import IgbHandling as igb

#%% Choose file from which get the catheter centers. Has to be a .catG file
experimentName = 'exp906c12'
pathElec = 'D:\\vgmar\\model_data\\exp906\\electrode_data\\Control'
with open(os.path.join(pathElec,experimentName+'_CatheterData.catG'), 'rb') as output:
    CGroup = pickle.load(output)

#%% Load a full atria tcc file
tcc_fname = 'D:\\vgmar\\model_data\\anatomy\\exp906k71_tcc_afull.igb'
tcc,hdr = igb.Load(tcc_fname)
tcc = np.swapaxes(tcc,0,2)

#%% Prepare output tcc

Output = np.zeros_like(tcc,dtype = np.int16)

Catheters = CGroup.Catheters
scale_tcc = 0.2 # mm
scale_catheter = 1#mm

for i,position in enumerate(Catheters.keys()):
    catCenter = Catheters[position].CatheterCenter
    catCenter = catCenter*(scale_catheter/scale_tcc)
    catCenter = catCenter.astype(np.int16)
    Output[catCenter[0],catCenter[1],catCenter[2]] = i

#%% Save new Tcc
fname_out= os.path.join(os.path.dirname(tcc_fname),'CatheterPosition_tcc.igb')

outHeader = {'x':hdr['x'],
             'y':hdr['y'],
             'z':hdr['z'],
             'type':hdr['type'],
             'architecture':'lsb'}
    

igb.Write(fname_out,outHeader,Output)

