# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:31:21 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys
import numpy as np
import pickle

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))

from CatheterObjects import CatheterClass,CatheterGroup
import IgbHandling as igb
import UserInterfaceFunctions as ui

#%%    
def MakePositionDict(args):
    PositionsDict = {}
    for i,key in enumerate(args.catheterNames):
        PositionsDict[key] = [args.catheterCenters[i],args.catheterRotations[i]]
    return PositionsDict

def main(args,onlyPositions):
    print('Loading anatomy')
    # Load anatomy
    FullCell = igb.LoadCellFile(args.anatomyPath,args.modelName,validCells = args.validCells)
    Anatomy,dataIndexes = igb.GetDataIndexes(args.anatomyPath,args.modelName,FullCell,validCells = args.validCells)
    nz,ny,nx = FullCell.shape
    # Load Data
    print('Loading data')
    if not  onlyPositions:
        fnameData = os.path.join(args.dataPath,args.expName)
        egmDataAll,hdr = igb.Load(fnameData,returnRawSignal=True) # Workaround for memory issues
        egmDataAll = egmDataAll.squeeze()
        egmDataAll = egmDataAll[:,dataIndexes]
    
    #Make dict for positions
    print('Making Catheter dictionary')
    PositionsDict = MakePositionDict(args)

    # Make and save Catheter group
    print('Positioning Catheters')
    Group = CatheterGroup(args.catheterModel,PositionsDict,Anatomy,(nx,ny,nz))
    if not onlyPositions: 
        Group.GetCatheterData(Anatomy,egmDataAll,rawInput=True,hdr=hdr,
                                getCSData = args.getCSData,CSDistal=args.CSDistal,CSProximal = args.CSProximal) #last line is only used if argsCSData=True
        if args.getActivations:
            print('Getting activations...')
            Group.GetActivations(method='adaptiveThreshold')
    else:
        Group.GetCatheterPositions()

    if not onlyPositions:   
        if args.outputMatlab:
            fnameOut = os.path.join(args.outputPath,args.outputFile)
            Group.SaveMat(fnameOut)
            print('Catheter data written in %s.mat'%fnameOut)
        else:
            fnameOut = os.path.join(args.outputPath,args.outputFile)
            with open(fnameOut+'.catG', 'wb') as output:
                pickle.dump(Group, output, pickle.HIGHEST_PROTOCOL)
                print('Catheter data written in %s.catG'%fnameOut)
    else:
        return Group
    
    

if __name__ == '__main__':
    
    parser = ui.MyParser(description='Get model data from the output ve files',
                      fromfile_prefix_chars='+',
                      usage='python GetCatheterData.py +ELECTRODE_POSITIONS.par +PARAMETERS.par EGMDATA_ve_egm_endo_epi.igb.gz')


	###################### Required Arguments ######################     
    parser.add_argument('expName', type=str, 
                        help='Igb file with the EGM data.')      
    
	###################### Optional Arguments ######################
    parser.add_argument('-modelName',action='store',default = 'model24-30',type=str,
						help = 'Name of the model. Default model24-30')  
    
    # File IO
    parser.add_argument('-anatomyPath','-ap',dest= 'anatomyPath',
						action='store',default = '.',type=str,
						help = 'Path to the anatomy file.  Default \'.\'')    
    
    parser.add_argument('-dataPath','-dp',dest= 'dataPath',
						action='store',default = '.',type=str,
						help = 'Path to the file containing the data. Default \'.\'')  
    parser.add_argument('-outputPath','-op',dest= 'outputPath',
						action='store',default = '.',type=str,
						help = 'Path to the output folder. Default \'.\'')
    parser.add_argument('-outputFile','-o',dest = 'outputFile',action='store',
                        default = 'output',type=str,
                        help = 'Output file; will be either .mat or .catG. Default output')    
    parser.add_argument('--outputMatlab',action='store_true',
                        help='If present, saves Matlab file instead of .catG file')
    
    # Catheter Related
    parser.add_argument('-catheterModel',action='store',default = 'HDGrid-4',type=str,
						help = 'Model of catheter to be placed. Default HDGrid-4')
    
    parser.add_argument('-catheterNames',nargs='+',type=str,
                        default = ['LPV','RPV','PLA','ILA_1','ILA_2',
                                   'ILA_3','SRA','IRA','ALA','ALA_roof',
                                   'ALA_AS','LAA','RAA','PECT','CSO'],
                        help='Name of the catheters to be positioned.\
                            Default is ...')
    
    parser.add_argument('-catheterCenters',nargs='+',type=int,
                        default = [1690,2014,1214,8062,16384,
                                   9612,5847,14142,15230,4517,
                                   8167,20032,25318,20761,15713],
                        help='Linear indexes of the catheter centers with respect\
                            to the anatomy file.\
                            Default is ...')                            
    parser.add_argument('-catheterRotations',nargs='+',type=float,
                        default = [-np.pi/4,np.pi,np.pi,-np.pi/2,
                                   np.pi,np.pi/2,np.pi,0,-np.pi/4,
                                   np.pi,0,np.pi,np.pi,0,np.pi],
                        help='Linear indexes of the catheter centers with respect\
                            to the anatomy file.\
                            Default is ...')                            
    parser.add_argument('--getActivations',action='store_true',
                        help='If present, detects activations')
    parser.add_argument('--getCSData',action='store_true',
                        help='If present, stores CS electrode data')
    parser.add_argument('-CSProximal',type=int,
                        default = 15872,
                        help='Position of the proximal end of the CS catheter')        
    parser.add_argument('-CSDistal',type=int,
                        default = 25614,
                        help='Position of the distal end of the CS catheter. The last electrode is always here')    

    # Anatomy related
    parser.add_argument('-validCells','-vc', nargs='+', type=int,default = [50],
                        help='Cell types corresponding to the chosen layer\
                            Default 50 (endocardium)', required=False)         

    ################################################################
    args = parser.parse_args()
    
    if type(args.validCells)!=list:
        args.validCells= [args.validCells]
        
    if type(args.catheterNames)!=list:
        args.catheterNames= [args.catheterNames]
        args.catheterCenters= [args.catheterCenters]
        args.catheterCenters= [args.catheterRotations]      
    
    onlyPositions = False
    main(args,onlyPositions)

    ## Example of how to run
    # 
# catheterCenters=[1665,2206,1023,12831,11575,7814,13750,4438,8481,13886,6107,14827,20088,22929,16076]
# catheterRotations=[3.141592653589793,2.356194490192345,3.141592653589793,1.5707963267948966,3.141592653589793,3.141592653589793,-0.7853981633974483,0.5235987755982988,3.141592653589793,3.141592653589793,3.141592653589793,0,0.7853981633974483,3.141592653589793,1.5707963267948966]
# catheterNames=['RPV','LPV','PLA','ILA_1','ILA_2','ILA_3','ALA','LA_Roof','LA_AS','LAA','SRA','IRA','RAA','PECT','CSO']