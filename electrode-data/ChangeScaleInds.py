# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 10:42:13 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os,sys
import numpy as np
from numpy.linalg import norm
import vtk

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
import IgbHandling as igb
from CatheterObjects import CatheterClass,CatheterGroup
import quick_visualization as qv
from StandardVTKObjects import OneViewRenderWindow


#%% Load Igb

path = 'D:\\vgmar\\model_data\\anatomy\\model30Box'

FullCell,hdr = igb.Load(os.path.join(path,'model24-30Box-atrial-surface-n.igb'))

zz,yy,xx = np.where(FullCell!=0)
originalScale = 0.2
currentScale = 1


Anatomy = np.array([xx,yy,zz]).T

PatchLocations= np.array([[338, 200, 57],
                        [339, 122, 136],
                        [308, 284, 186],
                        [324, 333, 70],
                        [112, 229, 129],
                        [125, 122, 188]])*originalScale/currentScale


for i,row in enumerate(PatchLocations):
    dists= np.linalg.norm(Anatomy-row,axis=1)
    ind = np.where(dists==np.min(dists))[0][0]
    print(ind)
    PatchLocations[i] = Anatomy[ind,:]

#%% Visualize
AnatomyActor,_ = qv.MakePSActors(Anatomy,1)
PatchActor,_ = qv.MakePSActors(PatchLocations,1,sphereColor=(1,0,1))
PatchActor.GetProperty().SetPointSize(50)


renderer = vtk.vtkRenderer()
renderer.AddActor(AnatomyActor)
renderer.AddActor(PatchActor)
renderer.ResetCamera()
renWin = OneViewRenderWindow(renderer,windowSize = [1800,800],
                        OffScreenRendering = False)
interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(renWin)
renWin.Render()
interactor.Start()

#%% Inds

Idx = igb.Coord2Idx(PatchLocations.astype(int),np.swapaxes(FullCell,0,2).shape)
