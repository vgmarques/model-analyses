'''
This function is supposed to encompass all the modifications that will be made to a model to fit a new geometry

The steps are the following:
0) calculate UAC for the model and for the new geometry (endo and epi)

1) Define endo and epicardium: this is done based on the original wall thickness of the model (or possibly 
    with the original method Ali used)
2) Convert all relevant curves of the model to bevel curves with a circle as outline: This makes the job of
    moving them between geometries more straightforward. Still may not be necessary. The relevant curves are:
    fiber orientations, other orientations, pectinate muscles, LAA bundles
3) Move the curves from one geometry to the other
4) Add the Bachmann Bundle: defining the connecting points is easy, but not so much the portions between the atria

5) Add inter-atrial connections

6) Add the CS: Probably the best idea is to follow the method by Roney et al. then create a mesh that thins out from
    the base to the apex

7) Add CS connections

-- Not yet implemented

8) Adjust Fossa Ovalis (?)
9) Fix orientation for exporting
10) Export

'''

#%% Import libraries
import bpy
from mathutils import *
import bmesh
D = bpy.data
C = bpy.context

import sys,os
import numpy as np
import pyvista as pv
from copy import deepcopy
import pickle

sys.path.append(r'D:\vgmar\Documents\BitBucket\UAC\blender\old_scripts')
sys.path.append(r'D:\vgmar\Documents\BitBucket\UAC\blender')
sys.path.append(r'D:\vgmar\Documents\BitBucket\UAC\openep-py\openep\mesh')

# from torso_tools_v29 import create_submesh,heart_export_matrix,scaleout,torso_export_matrix
from ModelPersonalizationTools import *

from mesh_routines import get_free_boundaries

#%% Relevant filepaths and manual inputs
carpFolder = r'D:\vgmar\Documents\BitBucket\UAC\UAC_py\carp'#/mnt/d/vgmar/Documents/BitBucket/UAC/UAC_py/carp/'

### Manually set the names of the geometries
ReferenceGeometries = {'LA_epi':'Model_LA_epi',
                       'LA_endo':'Model_LA_endo',
                       'RA_epi':'Model_RA_epi',
                       'RA_endo':'Model_RA_endo',
                       'LAA':'Model_LAA_endo'}

TargetGeometries = {'LA':'ADAS_02_LA','RA':'ADAS_02_RA','LAA':'ADAS_02_LAA'}

referenceScale = 1e-3 # um
targetScale = 1e-3 # mm
bunny = 1e-1

curveMaterials = ['Bundle','Orientation','Endo_Bundle','Epi_Bundle']
correspondingCollections = {'Bundle':'Bundles',
                            'Orientation':'Orientations',
                            'Endo_Bundle':'Fiber orientation LA 1',
                            'Epi_Bundle':'Fiber orientation LA 2'}

LAAIndex = 30054# From paraview

#%% Import model geometry

ReferenceUAC = {}
for key,value in ReferenceGeometries.items():
    mesh,uac = LoadVtk(os.path.join(carpFolder,value,'%s.vtk'%value),scale=referenceScale,getUAC=True)
    ReferenceUAC[key] = uac
    ReferenceGeometries[key] = mesh

#%% Import patient geometry

TargetUAC = {}
for key,value in TargetGeometries.items():
    mesh,uac = LoadVtk(os.path.join(carpFolder,value,'%s.vtk'%value),scale=targetScale,getUAC=True)
    TargetUAC[key] = uac
    TargetGeometries[key] = mesh

LAAIndices = [TargetGeometries['LA'].find_closest_point(point) for point in TargetGeometries['LAA'].points]


#%% 1) Adjust wall thickness

# Get model wall thickness
GetWallThickness(ReferenceGeometries['LA_endo'],ReferenceGeometries['LA_epi'])
GetWallThickness(ReferenceGeometries['RA_endo'],ReferenceGeometries['RA_epi'])

# Map thickness to new geometry
MapScalarsUAC(ReferenceGeometries['LA_endo'],TargetGeometries['LA'],'thickness')
MapScalarsUAC(ReferenceGeometries['RA_endo'],TargetGeometries['RA'],'thickness')

#%% 2) Add artificial CS based on its opening in the RA mesh
# Get points from boundary
boundariesRA = AddOpeningOrientations(TargetGeometries['RA'],'RA',downsample=5,depth = 0.015,makeCurves=False)
csBoundary = boundariesRA[3]
csThickness = 1*targetScale/bunny

Verts,Faces,pathLA,pathCS = MakeCSMesh(csBoundary,np.array([0.705501,0.0502242]),
                                        ReferenceGeometries,TargetGeometries,layer='',downsampleRatio = 3,minDistanceCS = 0.02) # [0.887256,0.0169217]
MakeCSObject(Verts, Faces,len(csBoundary))

# Add CS to the RA mesh before extruding
csObject = D.objects['CS']

AddCStoRA(TargetGeometries['RA'],csObject,len(csBoundary),csThickness) #This crashes sometimes for inexplicable reasons

#%% Create geometries in Blender
# Put new geometries in dictionary

TargetGeometries['LA_endo'],TargetGeometries['LA_epi'] = BylayerFromThickness(TargetGeometries['LA'])
TargetGeometries['RA_endo'],TargetGeometries['RA_epi'] = BylayerFromThickness(TargetGeometries['RA']) # Note! this crashes for the RA: try run separately
#polydata = TargetGeometries['RA']


# LAA done separately
TargetGeometries['LAA'].point_data['thickness'] = TargetGeometries['LA'].point_data['thickness'][LAAIndices]
TargetGeometries['LAA_endo'],_ = BylayerFromThickness(TargetGeometries['LAA'])
TargetGeometries['LAA_endo'].point_data['apical'] = TargetGeometries['LAA'].point_data['apical']
TargetGeometries['LAA_endo'].point_data['radial'] = TargetGeometries['LAA'].point_data['radial']
# Smooth a bit
TargetGeometries['LA_endo'] = TargetGeometries['LA_endo'].smooth(n_iter=20,relaxation_factor=1)
TargetGeometries['LA_epi'] = TargetGeometries['LA_epi'].smooth(n_iter=20,relaxation_factor=1)
TargetGeometries['RA_endo'] = TargetGeometries['RA_endo'].smooth(n_iter=20,relaxation_factor=1)
TargetGeometries['RA_epi'] = TargetGeometries['RA_epi'].smooth(n_iter=20,relaxation_factor=1)

# # Align with model valves
TargetGeometries,R,translation,pointsCenter = AlignValveRings(ReferenceGeometries, TargetGeometries, ['LA_endo','LA_epi','RA_endo','RA_epi'])

# # Adjust LAA
TargetGeometries['LAA_endo'].points = TargetGeometries['LA_endo'].points[LAAIndices]

## Put new geometries in Blender

la_endo = BlenderMeshFromPyvista(TargetGeometries['LA_endo'],'LA Endo')
D.collections['tests'].objects.link(la_endo)

la_epi = BlenderMeshFromPyvista(TargetGeometries['LA_epi'],'LA Epi')
D.collections['tests'].objects.link(la_epi)

laa_endo = BlenderMeshFromPyvista(TargetGeometries['LAA_endo'],'LAA Endo')
D.collections['tests'].objects.link(laa_endo)

ra_endo = BlenderMeshFromPyvista(TargetGeometries['RA_endo'],'RA Endo')
D.collections['tests'].objects.link(ra_endo)

ra_epi = BlenderMeshFromPyvista(TargetGeometries['RA_epi'],'RA Epi')
D.collections['tests'].objects.link(ra_epi)


#%% 2) Convert all curves to circle outlines
# ConvertCurveOutlinesToCircle(['Orientations'])

# I did that manually for the remaining points

#%% 3) Project the curves onto the new geometry

# RA bundles
for o in D.objects:
    if o.type == 'CURVE' and any(m in o.data.materials for m in ['Bundle']):
        TransferBundleGeometry(o,ReferenceGeometries,TargetGeometries,layer='endo',
                               radiusProportion='volume', # or 'deformation'
                               onSurface=False,returnValues = False,smoothIterations=3)

# For the LAA, the strategy is different. The Bundles are mapped to the LAA coordinates
# Thus, I change the objects of the LAA to follow the specific coordinates
LAABundleNames = ['LAA_opening','Curve.004', 'Curve.005', 'Main_LAA', 'pect01.007', 'pect01.009', 'pect01.010', 'pect01.011', 'pect01.012']
# For the main LAA, both versions should be used to get to the final shape

for name in LAABundleNames:
    o = D.objects[name]
    TransferLAABundles(o,ReferenceGeometries['LAA'],TargetGeometries['LAA_endo'],
                            radiusProportion='constant', # or 'deformation'
                            onSurface=False,returnValues = False,smoothIterations=5)

# Fix the main LAA bundle, which is partially in the LA
atrialPortion = np.asarray([np.array(V.co) for V in D.objects['_Main_LAA'].data.splines[0].bezier_points[:7]])
LAAPortion = np.asarray([np.array(V.co) for V in D.objects['+Main_LAA'].data.splines[0].bezier_points[7:]])

finalCoordinates = np.vstack([atrialPortion,LAAPortion])
Collection = D.objects['+Main_LAA'].users_collection[0]
M = D.objects['+Main_LAA'].data.materials[0]
depth =  D.objects['+Main_LAA'].data.bevel_depth

# Delete old objects
try:
    bpy.ops.object.select_all(action='DESELECT')
except:
    bpy.ops.object.editmode_toggle()
#

for name in LAABundleNames:
    D.objects['_'+name].select_set(True)

D.objects['+Main_LAA'].select_set(True)
bpy.ops.object.delete()

curve = CreateCurveFromList(finalCoordinates,
                            '+Main_LAA',resolution = 12,proportionHandles = 0.25,
                            bevel = 'ROUND',bevel_depth = depth)
newObject = D.objects.new('+Main_LAA', curve)
newObject.active_material = M
# add object to scene collection
Collection.objects.link(newObject)
SmoothCurve(newObject,smoothLevel=20)
newObject.select_set(False)

#%% Orientations
for o in D.objects:
    if o.type == 'CURVE' and any(m in o.data.materials for m in ['Endo_Bundle']):
        TransferBundleGeometry(o,ReferenceGeometries,TargetGeometries,layer='endo',
                               radiusProportion='constant', # or 'deformation'
                               onSurface=True,returnValues = False,smoothIterations=5)

for o in D.objects:
    if o.type == 'CURVE' and any(m in o.data.materials for m in ['Epi_Bundle']):
        TransferBundleGeometry(o,ReferenceGeometries,TargetGeometries,layer='epi',
                               radiusProportion='volume', # or 'deformation'
                               onSurface=True,returnValues = False,smoothIterations=5)


for name in ['Curve.006','Curve.007','Curve.008','Curve.070','Curve.073','SeptoAtrial.005']: 
    o = D.objects[name]
    TransferBundleGeometry(o,ReferenceGeometries,TargetGeometries,layer='endo',
                        radiusProportion='constant', # or 'deformation'
                        onSurface=True,returnValues = False,smoothIterations=5)

# TODO: SeptoAtrial was moved by hand

# Veins
boundariesLA = AddOpeningOrientations(TargetGeometries['LA_endo'],'LA',downsample=5,depth = 0.015)
boundariesRA = AddOpeningOrientations(TargetGeometries['RA_endo'],'RA',downsample=5,depth = 0.015)

#NOTE: REMOVE CS OPENING (UPDATE: if CS is added earlier, not necessary)

#%% 4) Transfer the Bachmann Bundle
# Follow the same approach as for other bundles, but each point of the spline is mapped to one UAC

smoothIterations = 10
returnValues = False
onSurface = False
layer = 'epi'

for o in D.objects:
    if o.type == 'CURVE' and any(m in o.data.materials for m in ['BB']):
        if o.name in ['Curve.061','Curve.062','Curve.063']:
            TransferBundleGeometry(o,ReferenceGeometries,TargetGeometries,layer='epi',
                                        radiusProportion='constant', # or 'deformation'
                                        onSurface=True,smoothIterations=smoothIterations)
        else:
            TransferInterAtrialStructures(o,ReferenceGeometries,TargetGeometries,
                                        radiusProportion = 'volume',smoothIterations=smoothIterations)
# def MakeBMeshFromData(points,edges = [],faces=[],name='newMesh'):
#     mesh = D.meshes.new(name)
#     #
#     mesh.from_pydata(points, edges, faces)
#     mesh.validate()
#     #
#     object = D.objects.new(mesh.name, mesh)
# scene = C.scene
# scene.collection.objects.link(object)
# C.view_layer.objects.active = csObject
MakeBBMesh(D.objects['BB'],ReferenceGeometries,TargetGeometries)

def MakeBBMesh(bbObject,referenceGeometries,targetGeometries):
    ''' 
    bbObject has to have 3 vertex groups: LA, RA, and Septal
    Septal has to be assigned last!
    '''
    M = bbObject.matrix_world
    # Get mesh points
    me = bbObject.data
    bm = bmesh.new()
    bm.from_mesh(me)
    bm.verts.ensure_lookup_table()
    # Get vertex per valid groups (LA,RA)
    laIndex = bbObject.vertex_groups['LA'].index
    raIndex = bbObject.vertex_groups['RA'].index
    septIndex = bbObject.vertex_groups['Septal'].index
    #
    laVerts = []
    raVerts = []
    septVerts = []
    for v in bm.verts:
        if me.vertices[v.index].groups[0].group == laIndex:
            laVerts.append(v.index)
        elif me.vertices[v.index].groups[0].group == raIndex:
            raVerts.append(v.index)
        if len(me.vertices[v.index].groups)>1:
            septVerts.append(v.index)
    #
    # For points marked as RA: do UAC projection in RA
    raVertsUAC = np.zeros((len(raVerts),2))
    for index,point in enumerate(raVerts):
        point = M @ bm.verts[point].co
        closestPoint = referenceGeometries['RA_epi'].find_closest_point(np.array(point))
        raVertsUAC[index,0] = referenceGeometries['RA_epi'].point_data['alpha'][closestPoint]
        raVertsUAC[index,1] = referenceGeometries['RA_epi'].point_data['beta'][closestPoint]
    #
    # 3D coordinates
    raVertsTarget = CoordinatesFromUAC(raVertsUAC,targetGeometries['RA_epi'])
    for index,point in zip(raVerts,raVertsTarget):
        bm.verts[index].co = point
    # For points marked as LA: do UAC projection in LA
    laVertsUAC = np.zeros((len(laVerts),2))
    for index,point in enumerate(laVerts):
        point = M @ bm.verts[point].co
        closestPoint = referenceGeometries['LA_epi'].find_closest_point(np.array(point))
        laVertsUAC[index,0] = referenceGeometries['LA_epi'].point_data['alpha'][closestPoint]
        laVertsUAC[index,1] = referenceGeometries['LA_epi'].point_data['beta'][closestPoint]
    # 3D coordinates
    laVertsTarget = CoordinatesFromUAC(laVertsUAC,targetGeometries['LA_epi'])
    for index,point in zip(laVerts,laVertsTarget):
        bm.verts[index].co = point
    #
    bbMesh= D.meshes.new('_'+bbObject.name)
    bm.to_mesh(bbMesh)
    bbMesh.update()
    bm.clear()
    #
    outObject = D.objects.new(bbMesh.name,bbMesh)
    outObject.active_material = D.materials['BB']
    collection = D.collections['Bachmann bundle']
    collection.objects.link(outObject)
    #
    # Smooth BB
    # This is to smooth the septum
    bpy.context.view_layer.objects.active = outObject
    # Put object in edit mode
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    bpy.ops.mesh.select_all(action='DESELECT')
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    for i in septVerts:
        bm.verts[i].select = True  # select index 4
    # Show the updates in the viewport
    bmesh.update_edit_mesh(me, True)
    bpy.ops.mesh.vertices_smooth(factor=1,repeat=10) #septum
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.vertices_smooth(factor=1,repeat=5) #everyone
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)

# Get new 3D coordinates from UAC
# NOTE curves 061,062 and 063 should be LA only

#%% 5) Add inter-atrial connections
for o in D.objects:
    if o.type == 'CURVE' and any(m in o.data.materials for m in ['EBundle']):
        if o.name[:2]=='CS': continue # CS are done later
        TransferInterAtrialStructures(o,ReferenceGeometries,TargetGeometries,
                                    radiusProportion = 'constant',smoothIterations=smoothIterations)


#%% 6) Coronary sinus
# csBoundary = boundariesRA[3]

# Verts,Faces,pathLA,pathCS = MakeCSMesh(csBoundary,np.array([0.705501,0.0502242]),ReferenceGeometries,TargetGeometries,layer='epi',downsampleRatio = 3) # [0.887256,0.0169217]
# MakeCSObject(Verts, Faces,len(csBoundary))
# csObject = D.objects['CS_endo']

# csThickness = 1*targetScale/bunny


# boundariesRAEpi = AddOpeningOrientations(TargetGeometries['RA_epi'],'RA_epi',downsample=5,depth = 0.015)
# csEpiBound = boundariesRAEpi[3]
# ###

# #CS mesh
# #
# bm = bmesh.new()
# bm.from_mesh(csObject.data)
# bmesh.ops.triangulate(bm, faces=bm.faces)
# bm.verts.ensure_lookup_table()
# bm.faces.ensure_lookup_table()
# #
# verts = np.array([np.array(v.co) for v in bm.verts])
# faces = []
# for f in bm.faces:
#     S = f.verts
#     ff = []
#     for ele in S: ff.append(ele.index)
#     faces.append(ff)

# #
# faces = np.array(faces)
# faces = np.hstack([np.ones((len(faces),1),int)*3,faces]).ravel()
# ##
# csMeshEndo = pv.PolyData(verts,faces)

# FB = get_free_boundaries(csMeshEndo)
# FF = FB.separate_boundaries()

# targetN = csMeshEndo.compute_normals(point_normals=True,cell_normals=False)

# epiPointsCS = deepcopy(csMeshEndo.points) - targetN.point_data['Normals'] * csThickness 

# # Add vertices and faces of the actual epicardial boundary
# newFaces = np.hstack([np.arange(len(verts)-len(csBoundary),len(verts)).reshape(-1,1),
#                       np.arange(len(verts),len(verts)+len(csEpiBound)).reshape(-1,1),
#                       np.roll(np.arange(len(verts),len(verts)+len(csEpiBound)),-1).reshape(-1,1)])
# newFaces = np.hstack([np.ones((len(newFaces),1),int)*3,newFaces]).ravel()
# newFaces = np.hstack([faces,newFaces])
# epiPointsCS = np.vstack([epiPointsCS,csEpiBound])


# csMeshEpi = pv.PolyData(epiPointsCS,newFaces)

# D.collections['tests'].objects.link( BlenderMeshFromPyvista(csMeshEpi,'CS_Epi'))

#%% CS Connections
# _,_,pathLA,pathCS = MakeCSMesh(csBoundary,np.array([0.705501,0.0502242]),
#                                         ReferenceGeometries,TargetGeometries,layer='',downsampleRatio = 3,minDistanceCS = 0.02)

nConnections = 5
AddCSConnections(nConnections,TargetGeometries,pathLA)

#%% Make valve ring and merge PV ends

meshDict = TargetGeometries
MakeValveRing('LA',TargetGeometries)
MakeValveRing('RA',TargetGeometries)

# Zip PVs and VC
ZipBoundaries('LA',ignoreValve=True,closed = True)
ZipBoundaries('RA',ignoreValve=True)




#%% Close holes

CloseBoundaries(D.objects['RA Endo'])
CloseBoundaries(D.objects['LA Endo'])
CloseBoundaries(D.objects['RA Epi'])
CloseBoundaries(D.objects['LA Epi'])

#%% Make Fossa ovalis
foRadius = 6.5*targetScale/bunny
foEpiRadius = foRadius*1.1

centerR,centerL = ExtrudeFossaOvalis(D.objects['RA Epi Closed'],D.objects['LA Epi Closed'],D.objects['CS'],foEpiRadius)
#centerR = 19783
#centerL = 31816

ExtrudeFossaOvalis(D.objects['RA Endo'],D.objects['LA Endo'],D.objects['CS'],foRadius,centerR = centerR, centerL=centerL)

raObject = D.objects['RA Epi']
laObject = D.objects['LA Epi']
csObject = D.objects['CS']



CloseBoundaries(D.objects['RA Endo'])
CloseBoundaries(D.objects['RA Epi'])
CloseBoundaries(D.objects['LA Endo'])# Why are you crashing?
CloseBoundaries(D.objects['LA Epi']) # Why are you crashing?


#%% Make heart object with closed holes
col = bpy.data.collections.new('Export')
col.hide_viewport = False
bpy.context.scene.collection.children.link(col)

heartObjects = ['LA Endo','LA Epi','RA Endo','RA Epi','valveRingLA','valveRingRA']
groupNames = ['LA endo','LA epi','RA endo','RA epi','valve ring','valve ring']
MakeHeartObject(heartObjects,groupNames,outName='_heart_closed',outCollection='tests')

#%% TWO MANUAL OPERATIONS
# 1) open holes to make the normal _heart object
# 2) merge the faces of the fossa ovalis!

#%% Make heart object with open holes



MakeHeartObject(heartObjects,groupNames,outName='_heart',outCollection='tests')
#%% Make a heart object
from torso_tools_v29 import create_submesh

col = bpy.data.collections.new('Export')
col.hide_viewport = False
bpy.context.scene.collection.children.link(col)

# List objects that are part of "heart"
heartObjects = ['LA Endo','LA Epi','RA Endo','RA Epi','valveRingLA','valveRingRA']
groupNames = ['LA endo','LA epi','RA endo','RA epi','valve ring','valve ring']


def MakeHeartObject(heartObjects,vertexGroups,outName='heart',outCollection='tests'):
    lengths = []
    VV = []
    FF = []
    # Get vertices and faces of these meshes
    for index,name in enumerate(heartObjects):
        obj = D.objects[name]
        ff, vv = create_submesh(obj, [], modLevels=0)
        lengths.append(len(vv))
        for f in ff:
            FF.append([f[0]+len(VV),f[1]+len(VV),f[2]+len(VV)])
        for v in vv: VV.append(v)
    heartMesh = D.meshes.new(outName)
    #
    heartMesh.from_pydata(VV, [], FF)
    heartMesh.validate()
    heartObject = D.objects.new(heartMesh.name, heartMesh)
    col = bpy.data.collections[outCollection]
    col.objects.link(heartObject)
    bpy.context.view_layer.objects.active = heartObject
    #
    k = 0
    for index,name in enumerate(vertexGroups):
        vg = heartObject.vertex_groups.get(name)
        if vg is None: 
            vg = heartObject.vertex_groups.new(name=name)
        else:
            print('Repeated name: '+vg.name)
        # Merge duplicates (maybe relevant for CS)
        # Assig
        verts = [v for v in range(k,k+lengths[index])]
        k = k+lengths[index]
        vg.add(verts, 1.0, 'ADD')

