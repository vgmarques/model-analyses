try:
    import bpy
    from mathutils import *
    import bmesh
    D = bpy.data
    C = bpy.context
except:
    print('Not in Blender, limited functionality') 

import sys,os
import numpy as np
import pyvista as pv
from scipy.spatial import KDTree
from scipy.linalg import svd
from copy import deepcopy

from torso_tools_v29 import create_submesh

sys.path.append(r'D:\vgmar\Documents\BitBucket\UAC\openep-py\openep\mesh')
from mesh_routines import get_free_boundaries


#%% File I/O
def LoadVtk(filename,scale=1e-6,getUAC=True):
    if os.path.splitext(filename)[1] == '':
        filename = filename+'.vtk'
    #
    inMesh = pv.PolyData(filename)
    #
    # Scale to blender units
    bunny = 1e-1
    #
    Ms = Matrix.Scale(scale/bunny, 4)
    inMesh.points = np.array([Vector(pt) @Ms for pt in inMesh.points])
    #
    if getUAC:
        if ('alpha' in inMesh.point_data) and ('beta' in inMesh.point_data):
            UAC = np.array([inMesh.point_data['alpha'],inMesh.point_data['beta']]).T
        else:
            UAC = None
        return inMesh,UAC
    else:
        return inMesh

#%% Pyvista / Blender interface
def PolyDataFromBlender(blenderObj):
    bm = bmesh.new()
    bm.from_mesh(blenderObj.data)
    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()
    VV = [v.co for v in bm.verts]
    FF = [v.index for f in bm.faces for v in f.verts]
    FF =  np.array(FF).reshape(-1,3)
    FF = np.hstack([np.ones((len(FF),1),int)*3,FF])
    Poly = pv.PolyData(VV,FF.ravel())
    return Poly

def PyvistaFacesFromBlender(faces):
    pyvistaFaces = []
    for row in faces:
        pyvistaFaces.append(len(row))
        [pyvistaFaces.append(i) for i in row];
    return pyvistaFaces

def BlenderMeshFromPyvista(polydata,name):
    mesh = D.meshes.new(name)
    faces = list(polydata.faces.reshape(-1,4)[:,1:])
    #
    mesh.from_pydata(polydata.points, [], faces)
    mesh.update()
    object = D.objects.new(name, mesh)
    return object

#%% UAC

def GetUACCoordinates(points,targetMesh,kNeighbors = 3,appendage=False):
    if not appendage:
        targetUAC = np.array([targetMesh.point_data['alpha'],targetMesh.point_data['beta']]).T
    else:
        targetUAC = np.array([targetMesh.point_data['apical'],targetMesh.point_data['radial']]).T
    #
    uacOut = np.zeros((len(points),2))
    for i,point in enumerate(points):
        point = np.array(point)
        #
        closestInds = targetMesh.find_closest_point(point,kNeighbors)
        weights = 1/np.linalg.norm(targetMesh.points[closestInds]-point,axis=1)
        #
        uacOut[i,0] = np.sum(targetUAC[closestInds,0]*weights)/np.sum(weights)
        uacOut[i,1] = np.sum(targetUAC[closestInds,1]*weights)/np.sum(weights)
        #
    return uacOut

def CoordinatesFromUAC(uacCoordinates,targetMesh,appendage=False):
    # Make the target mesh 2D to sue pyvista functions
    if not appendage:
        targetUAC = np.array([targetMesh.point_data['alpha'],targetMesh.point_data['beta']]).T
    else:
        targetUAC = np.array([targetMesh.point_data['apical'],targetMesh.point_data['radial']]).T
            #
    targetUACMesh = pv.PolyData()
    targetUACMesh.points = np.hstack([targetUAC,np.zeros((len(targetUAC),1))])
    targetUACMesh.faces = targetMesh.faces
    # Find closest points
    queryPoints = np.hstack([uacCoordinates,np.zeros((len(uacCoordinates),1))])
    closestPoints = [targetUACMesh.find_closest_point(point) for point in queryPoints]
    # Get 3d coordinates
    Coordinates3D = targetMesh.points[closestPoints] # Ideally this would be linearly interpolated as well
    #
    return np.array(Coordinates3D)

def MapScalarsUAC(referenceMesh,targetMesh,scalarName,returnValues=False,kNeighbors=3):
    # Make UACs
    referenceUAC = np.array([referenceMesh.point_data['alpha'],referenceMesh.point_data['beta']]).T
    targetUAC = np.array([targetMesh.point_data['alpha'],targetMesh.point_data['beta']]).T
    #
    # Create "2D" mesh to be able to use pyvista tools
    refUACMesh = pv.PolyData()
    refUACMesh.points = np.hstack([referenceUAC,np.zeros((len(referenceUAC),1))])
    refUACMesh.faces = referenceMesh.faces
    refUACMesh.point_data[scalarName] = referenceMesh.point_data[scalarName]
    #
    # Get the cells of the reference UAC that contain the points of the target UAC
    queryPoints = np.hstack([targetUAC,np.zeros((len(targetUAC),1))])
    containingCells  = refUACMesh.find_containing_cell(queryPoints)
    #
    # Get the barycentric coordinates if there is a containing cell. Otherwise, mark as points that need to be linearly interpolated
    targetBarycenter = np.zeros((len(containingCells),3))
    toInterpolateIndexes = [] # no containing cell
    #
    for index,cellID in enumerate(containingCells):
        if cellID==-1:
            targetBarycenter[index,:] = -1
            toInterpolateIndexes.append(index)
            continue
        L = ConvertToBarycentric(queryPoints[index][:2],refUACMesh.points[refUACMesh.cell_point_ids(cellID),:2])
        targetBarycenter[index,:] = L.flatten()
    #
    # For the elements not in a cell, linearly interpolate
    ClosestInds = []
    for index in toInterpolateIndexes:
        closestInds = refUACMesh.find_closest_point(queryPoints[index],kNeighbors)
        ClosestInds.append(closestInds)
        dists = np.linalg.norm(refUACMesh.points[closestInds,:]-queryPoints[index],axis=1)
        targetBarycenter[index,:] = dists
    #
    # Interpolate either linearly or with barycenter coordinates
    newScalars = np.zeros(len(queryPoints))*np.nan
    kk = 0
    #
    for index in range(len(queryPoints)):
        currentWeights = targetBarycenter[index]
        if index in toInterpolateIndexes:
            #Distance interpolation
            oldScalars = refUACMesh.point_data[scalarName][ClosestInds[kk]]
            kk += 1
            weights = 1/currentWeights
            newScalars[index] = np.sum(oldScalars*weights)/np.sum(weights)   
        else:
            #Barycentric interpolation
            cellPoints = refUACMesh.cell_point_ids(containingCells[index])
            oldScalars = refUACMesh.point_data[scalarName][cellPoints]
            newScalars[index] = currentWeights[0]*oldScalars[0]+currentWeights[1]*oldScalars[1]+currentWeights[2]*oldScalars[2]
    #
    # Output
    if returnValues:
        return newScalars
    else:
        targetMesh.point_data[scalarName] = newScalars

#%% Intracardiac bundles and fiber orientations

def ProjectSliceCurve(splinePoints,targetMesh,projectionType='center',splineRadius = None):
    projectedSpline = []
    if projectionType=='center_and_lateral':
        projectedLeft = []
        projectedRight = []
    #
    for index,centerPoint in enumerate(splinePoints):
        centerPoint = np.array(centerPoint) # No Vector from blender
        closestCells,centerProjection = targetMesh.find_closest_cell(centerPoint,return_closest_point=True)
        #
        if projectionType=='center':
            projectedSpline.append(centerProjection)
            continue
        elif projectionType=='center_and_lateral':
            #
            V1 = centerProjection-centerPoint #Distance between point and projection
            V1 = V1 / np.linalg.norm(V1)
            #
            # Vs is an axial vector for the segment
            if index == 0:
                Vs = np.array(splinePoints[1])-centerPoint
            else:
                Vs = centerPoint-np.array(splinePoints[index-1])
            Vs = Vs/np.linalg.norm(Vs)
            # Lateral vector
            Vl = np.cross(Vs,V1)
            #
            pLeft = centerPoint+Vl*splineRadius
            pRight = centerPoint-Vl*splineRadius
            #
            # Project lateral points as well
            _,pLeft = targetMesh.find_closest_cell(pLeft,return_closest_point=True)
            _,pRight = targetMesh.find_closest_cell(pRight,return_closest_point=True)
            #
            # Put on lists
            projectedSpline.append(centerProjection)
            projectedLeft.append(pLeft)
            projectedRight.append(pRight)
        #
    #
    if projectionType=='center':
        return projectedSpline

    elif projectionType=='center_and_lateral':
        return projectedSpline,projectedLeft,projectedRight

def MovePointsFromSurface(surfaceCoords,distances,refMesh):
    modifiedCoords = np.zeros_like(surfaceCoords)
    for index,surfacePoint in enumerate(surfaceCoords):
        #
        cell = refMesh.find_containing_cell(surfacePoint)
        normal = refMesh.cell_normals[cell]
        modifiedCoords[index] = surfaceCoords[index]+distances[index]*normal
    #
    return modifiedCoords
 
def TransferBundleGeometry(object,referenceGeometries,targetGeometries,layer='endo',
                           radiusProportion='volume', # or 'deformation'
                           onSurface=False,returnValues = False,smoothIterations=0):
    #
    M = object.matrix_world
    #
    radius = object.data.bevel_depth
    spline = object.data.splines[0]
    bundlePoints = [M @ v.co for v in spline.bezier_points]
    #
    # See which geometry is closer
    if radiusProportion=='deformation':
        projectedSplineR,projectedLeftR,projectedRightR = ProjectSliceCurve(bundlePoints,referenceGeometries['RA_'+layer],
                                                                            splineRadius = radius,projectionType='center_and_lateral')
        projectedSplineL,projectedLeftL,projectedRightL = ProjectSliceCurve(bundlePoints,referenceGeometries['LA_'+layer],
                                                                            splineRadius = radius,projectionType='center_and_lateral')
    elif (radiusProportion=='volume') or (radiusProportion=='constant'):
        projectedSplineR = ProjectSliceCurve(bundlePoints,referenceGeometries['RA_'+layer])
        projectedSplineL = ProjectSliceCurve(bundlePoints,referenceGeometries['LA_'+layer])
    #
    #
    if np.mean(np.linalg.norm(np.array(projectedSplineR)-np.array(bundlePoints),axis=1)) < \
        np.mean(np.linalg.norm(np.array(projectedSplineL)-np.array(bundlePoints),axis=1)):
        refMesh = 'RA_'+layer
        if radiusProportion=='deformation':
            projectedSpline,projectedLeft,projectedRight = projectedSplineR,projectedLeftR,projectedRightR
        elif (radiusProportion=='volume') or (radiusProportion=='constant'):
            projectedSpline = projectedSplineR
        print('%s belongs to the right atrium'%object.name)
    else:
        refMesh = 'LA_'+layer
        if radiusProportion=='deformation':
            projectedSpline,projectedLeft,projectedRight = projectedSplineL,projectedLeftL,projectedRightL
        elif (radiusProportion=='volume') or (radiusProportion=='constant'):
            projectedSpline = projectedSplineL
        print('%s belongs to the left atrium'%object.name)
    #
    distanceToSurface = np.linalg.norm(np.array(bundlePoints)-projectedSpline,axis=1)
    #
    #UAC
    uacBundles = GetUACCoordinates(projectedSpline,referenceGeometries[refMesh])
    newCoordinates = CoordinatesFromUAC(uacBundles,targetGeometries[refMesh])  # Get the 3D positions
    #
    #
    #
    # Estimate radius
    if radiusProportion=='deformation':
        P1Projected = GetUACCoordinates(projectedLeft,referenceGeometries[refMesh])
        P2Projected = GetUACCoordinates(projectedRight,referenceGeometries[refMesh])
        p1Trans = CoordinatesFromUAC(P1Projected,targetGeometries[refMesh])
        p2Trans = CoordinatesFromUAC(P2Projected,targetGeometries[refMesh])
        radiusTrans = np.median(np.linalg.norm(p1Trans-p2Trans,axis=1))/2
        transferDistances = np.array(distanceToSurface)/radius*radiusTrans
    elif (radiusProportion=='volume'):
        originalVolume = referenceGeometries[refMesh].volume
        newVolume = targetGeometries[refMesh].volume
        # originalVolume/radius**2 = newVolume/radiusTrans**2
        radiusTrans = np.sqrt(newVolume/(originalVolume/radius**2)) # Constant volume
        #
        ratio = radiusTrans/radius
        transferDistances = np.array(distanceToSurface)*ratio*np.mean(np.array(object.scale))
    elif (radiusProportion=='constant'):
        radiusTrans= radius*np.mean(np.array(object.scale))
        transferDistances = np.array(distanceToSurface)
    #
    if not onSurface:
        newCoordinates = MovePointsFromSurface(newCoordinates,transferDistances,targetGeometries[refMesh])
    #
    if returnValues:
        return newCoordinates
    else:
        # Make new objects
        name = object.name
        Collection = object.users_collection[0]
        M = object.data.materials[0]
        #
        try:
            bpy.ops.object.select_all(action='DESELECT')
        except:
            bpy.ops.object.editmode_toggle()
        #
        object.select_set(True)
        # bpy.ops.object.delete()
        #
        # New Curve
        curve = CreateCurveFromList(np.array(newCoordinates),
                                    '_'+name,resolution = 12,proportionHandles = 0.25,
                                    bevel = 'ROUND',bevel_depth = radiusTrans)
        #
        newObject = D.objects.new('_'+name, curve)
        newObject.active_material = M
        # add object to scene collection
        Collection.objects.link(newObject)
        SmoothCurve(newObject,smoothLevel=smoothIterations)
        newObject.select_set(False)

def TransferLAABundles(object,referenceGeometry,targetGeometry,
                           radiusProportion='volume', # or 'deformation'
                           onSurface=False,returnValues = False,smoothIterations=0):
    #
    M = object.matrix_world
    #
    radius = object.data.bevel_depth
    spline = object.data.splines[0]
    bundlePoints = [M @ v.co for v in spline.bezier_points]
    #
    # See which geometry is closer
    if radiusProportion=='deformation':
        projectedSpline,projectedLeft,projectedRight = ProjectSliceCurve(bundlePoints,referenceGeometry,
                                                                            splineRadius = radius,projectionType='center_and_lateral')
    #
    elif (radiusProportion=='volume') or (radiusProportion=='constant'):
        projectedSpline = ProjectSliceCurve(bundlePoints,referenceGeometry)
    #
    distanceToSurface = np.linalg.norm(np.array(bundlePoints)-projectedSpline,axis=1)
    #
    #UAC
    uacBundles = GetUACCoordinates(projectedSpline,referenceGeometry,appendage=True)
    newCoordinates = CoordinatesFromUAC(uacBundles,targetGeometry,appendage=True)  # Get the 3D positions
    #
    #
    # Estimate radius
    if radiusProportion=='deformation':
        P1Projected = GetUACCoordinates(projectedLeft,referenceGeometry,appendage=True)
        P2Projected = GetUACCoordinates(projectedRight,referenceGeometry,appendage=True)
        p1Trans = CoordinatesFromUAC(P1Projected,targetGeometry,appendage=True)
        p2Trans = CoordinatesFromUAC(P2Projected,targetGeometry,appendage=True)
        radiusTrans = np.median(np.linalg.norm(p1Trans-p2Trans,axis=1))/2
        transferDistances = np.array(distanceToSurface)/radius*radiusTrans
    elif (radiusProportion=='volume'):
        originalVolume = referenceGeometry.volume
        newVolume = targetGeometry.volume
        # originalVolume/radius**2 = newVolume/radiusTrans**2
        radiusTrans = np.sqrt(newVolume/(originalVolume/radius**2)) # Constant volume
        #
        ratio = radiusTrans/radius
        transferDistances = np.array(distanceToSurface)*ratio
    elif (radiusProportion=='constant'):
        radiusTrans= radius
        transferDistances = np.array(distanceToSurface)
    #
    if not onSurface:
        if object.name=='LAA_opening':
            #This particular bundle is on the boundary between LAA and LA, and thus it looks funny if we transfer it normally
            # Instead, I get the LAA free boundary and extrude it a bit (maybe not even necessary)
            FB = get_free_boundaries(targetGeometry)
            newCoordinates = FB.separate_boundaries()
            newCoordinates = np.array(FB.points[newCoordinates[0][:,0]])[::5,:] 
            transferDistances = np.ones(len(newCoordinates))*np.mean(transferDistances)
        #
        newCoordinates = MovePointsFromSurface(newCoordinates,transferDistances,targetGeometry)
    #
    if returnValues:
        return newCoordinates
    else:
        # Make new objects
        name = object.name
        Collection = object.users_collection[0]
        M = object.data.materials[0]
        #
        try:
            bpy.ops.object.select_all(action='DESELECT')
        except:
            bpy.ops.object.editmode_toggle()
        #
        object.select_set(True)
        # bpy.ops.object.delete()
        #
        # New Curve
        curve = CreateCurveFromList(np.array(newCoordinates),
                                    '+'+name,resolution = 12,proportionHandles = 0.25,
                                    bevel = 'ROUND',bevel_depth = radiusTrans)
        #
        newObject = D.objects.new('+'+name, curve)
        newObject.active_material = M
        # add object to scene collection
        Collection.objects.link(newObject)
        SmoothCurve(newObject,smoothLevel=smoothIterations)
        newObject.select_set(False)

#%% Inter atrial structures (BB and connections)

def TransferInterAtrialStructures(object,referenceGeometries,targetGeometries,radiusProportion='volume', # or constant
                                  smoothIterations=0,returnValues=0,onSurface=False,layer='epi'):
    #
    M = object.matrix_world
    #
    radius = object.data.bevel_depth
    spline = object.data.splines[0]
    bundlePoints = [M @ v.co for v in spline.bezier_points]
    #
    distLA = np.array([np.nanmin(np.linalg.norm(referenceGeometries['LA_'+layer].points-np.array(point),axis=1)) for point in bundlePoints])
    distRA = np.array([np.nanmin(np.linalg.norm(referenceGeometries['RA_'+layer].points-np.array(point),axis=1))for point in bundlePoints])
    #
    closestChamber = distLA<distRA # if 0, closer to RA, if 1 closer to LA
    #
    #
    projectedSpline = np.zeros((len(bundlePoints),3))*-1
    uacBundles = np.zeros((len(bundlePoints),3))*-1
    newCoordinates = np.zeros((len(bundlePoints),3))*-1
    for index,point in enumerate(bundlePoints):
        if closestChamber[index]:
            refGeometry = referenceGeometries['LA_'+layer]
            targetGeometry = targetGeometries['LA_'+layer]
        else:
            refGeometry = referenceGeometries['RA_'+layer]
            targetGeometry = targetGeometries['RA_'+layer]
        #
        projectedPoint = ProjectSliceCurve(np.array(point).reshape(-1,3),refGeometry,projectionType='center',splineRadius = radius)
        #
        distanceToSurface = np.linalg.norm(np.array(point)-projectedPoint,axis=1)
        # UAC
        uacPoint = GetUACCoordinates(projectedPoint,refGeometry)
        newCoordinate = CoordinatesFromUAC(uacPoint,targetGeometry)
        if not onSurface:
            newCoordinates[index,:] = MovePointsFromSurface(newCoordinate,distanceToSurface,targetGeometry)
        else:
            newCoordinates[index,:] = newCoordinate
    #
    if radiusProportion=='constant':
        radiusTrans= radius*np.mean(np.array(object.scale))
    else:
        radiusTrans = targetGeometry.volume/refGeometry.volume*radius*np.mean(np.array(object.scale))
    #
    if returnValues:
        # return newCoordinates
        a=0
    else:
        # Make new objects
        name = object.name
        Collection = object.users_collection[0]
        M = object.data.materials[0]
        #
        try:
            bpy.ops.object.select_all(action='DESELECT')
        except:
            bpy.ops.object.editmode_toggle()
        #
        object.select_set(True)
        # bpy.ops.object.delete()
        #
        # New Curve
        curve = CreateCurveFromList(np.array(newCoordinates),
                                    '_'+name,resolution = 12,proportionHandles = 0.25,
                                    bevel = 'ROUND',bevel_depth = radiusTrans)
        #
        newObject = D.objects.new('_'+name, curve)
        newObject.active_material = M
        # add object to scene collection
        Collection.objects.link(newObject)
        SmoothCurve(newObject,smoothLevel=smoothIterations)
        newObject.select_set(False)

def AddCSConnections(nConnections,targetGeometries,pathLA):
    # Get points of interest
    pathLA = targetGeometries['LA_endo'].points[pathLA]
    csPoints = targetGeometries['RA_endo'].points[targetGeometries['RA_endo'].point_data['alpha']==-1] # CS points have no uac
    # Find last index of the CS
    csDist = np.linalg.norm(pathLA[-1]-csPoints,axis=1)
    csTip = np.where(csDist==np.min(csDist))[0][0]
    pathDist = np.linalg.norm(pathLA-csPoints[csTip,:],axis=1)
    lastInd = np.where(pathDist==np.min(pathDist))[0][0]
    #
    laInterval = len(pathLA[:lastInd])//nConnections
    Collection = D.collections['Orientations']
    for connectionIndex in range(nConnections):
        #find closest CS point
        dist = np.linalg.norm(pathLA[connectionIndex*laInterval]-csPoints,axis=1)
        csInd = np.where(dist==np.min(dist))[0][0]
        coordinates = np.vstack([pathLA[connectionIndex*laInterval],csPoints[csInd]])
        coordinates = np.array([Vector(c) for c in coordinates])
        curve = CreateCurveFromList(coordinates,
                                        '_CSconnection_%02d'%connectionIndex,resolution = 12,proportionHandles = 0.25,
                                        bevel = 'ROUND',bevel_depth = 0.015)
        newObject = D.objects.new(curve.name, curve)
        newObject.active_material = D.materials['EBundle']
        # add object to scene collection
        Collection.objects.link(newObject)
        newObject.select_set(False)

def MakeBBMesh(bbObject,referenceGeometries,targetGeometries):
    ''' 
    bbObject has to have 3 vertex groups: LA, RA, and Septal
    Septal has to be assigned last!
    '''
    M = bbObject.matrix_world
    # Get mesh points
    me = bbObject.data
    bm = bmesh.new()
    bm.from_mesh(me)
    bm.verts.ensure_lookup_table()
    # Get vertex per valid groups (LA,RA)
    laIndex = bbObject.vertex_groups['LA'].index
    raIndex = bbObject.vertex_groups['RA'].index
    septIndex = bbObject.vertex_groups['Septal'].index
    #
    laVerts = []
    raVerts = []
    septVerts = []
    for v in bm.verts:
        if me.vertices[v.index].groups[0].group == laIndex:
            laVerts.append(v.index)
        elif me.vertices[v.index].groups[0].group == raIndex:
            raVerts.append(v.index)
        if len(me.vertices[v.index].groups)>1:
            septVerts.append(v.index)
    #
    # For points marked as RA: do UAC projection in RA
    raVertsUAC = np.zeros((len(raVerts),2))
    distancesSurfaceRA = np.zeros(len(raVerts))
    for index,point in enumerate(raVerts):
        point = M @ bm.verts[point].co
        closestPoint = referenceGeometries['RA_endo'].find_closest_point(np.array(point))
        distancesSurfaceRA[index] = np.linalg.norm(np.array(point)-referenceGeometries['RA_endo'].points[closestPoint])
        raVertsUAC[index,0] = referenceGeometries['RA_endo'].point_data['alpha'][closestPoint]
        raVertsUAC[index,1] = referenceGeometries['RA_endo'].point_data['beta'][closestPoint]
    #
    # 3D coordinates
    raVertsTarget = CoordinatesFromUAC(raVertsUAC,targetGeometries['RA_endo'])
    for ii,(index,point) in enumerate(zip(raVerts,raVertsTarget)):
        cell = targetGeometries['RA_endo'].find_containing_cell(point)
        bm.verts[index].co = point-distancesSurfaceRA[ii]*targetGeometries['RA_endo'].cell_normals[cell] #normals point inward
    # For points marked as LA: do UAC projection in LA
    laVertsUAC = np.zeros((len(laVerts),2))
    distancesSurfaceLA = np.zeros(len(laVerts))
    for index,point in enumerate(laVerts):
        point = M @ bm.verts[point].co
        closestPoint = referenceGeometries['LA_endo'].find_closest_point(np.array(point))
        distancesSurfaceLA[index] = np.linalg.norm(np.array(point)-referenceGeometries['LA_endo'].points[closestPoint])
        laVertsUAC[index,0] = referenceGeometries['LA_endo'].point_data['alpha'][closestPoint]
        laVertsUAC[index,1] = referenceGeometries['LA_endo'].point_data['beta'][closestPoint]
    # 3D coordinates
    laVertsTarget = CoordinatesFromUAC(laVertsUAC,targetGeometries['LA_endo'])
    for ii,(index,point) in enumerate(zip(laVerts,laVertsTarget)):
        cell = targetGeometries['LA_endo'].find_containing_cell(point)
        bm.verts[index].co = point-distancesSurfaceLA[ii]*targetGeometries['LA_endo'].cell_normals[cell]#normals point inward
    #
    bbMesh= D.meshes.new('_'+bbObject.name)
    bm.to_mesh(bbMesh)
    bbMesh.update()
    bm.clear()
    #
    outObject = D.objects.new(bbMesh.name,bbMesh)
    outObject.active_material = D.materials['BB']
    collection = D.collections['Bachmann bundle']
    collection.objects.link(outObject)
    #
    # Smooth BB
    # This is to smooth the septum
    bpy.context.view_layer.objects.active = outObject
    # Put object in edit mode
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    # subdivide
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.subdivide(number_cuts=3)
    #smooth septum
    bpy.ops.mesh.select_all(action='DESELECT')
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    for i in septVerts:
        bm.verts[i].select = True  # select index 4
    # Show the updates in the viewport
    bmesh.update_edit_mesh(me, True)
    bpy.ops.mesh.vertices_smooth(factor=1,repeat=5) #septum
    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.vertices_smooth(factor=1,repeat=1) #everyone
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)

def ExtrudeFossaOvalisAutomatic(raObject,laObject,csObject,foRadius,centerR=None,centerL=None):
    # IMPORTANT: raObject and laObject must be closed manifolds!!!
    # Get names
    raName = raObject.name
    laName = laObject.name
    # Make PolyData
    raPoly = PolyDataFromBlender(raObject)
    laPoly = PolyDataFromBlender(laObject)
    csPoly = PolyDataFromBlender(csObject)
    #
    # Determine FO Center and belonging verts
    if centerR is None:
        validRAPoints = raPoly.points[:-csPoly.n_points]
        # Find closest point without considering the CS
        distancesFromLA = [np.nanmin(np.linalg.norm(point-laPoly.points,axis=1)) for point in validRAPoints]
        distancesFromLA = np.array(distancesFromLA)
        foCenterRA = np.where(distancesFromLA==np.nanmin(distancesFromLA))[0][0]
    else:
        foCenterRA = centerR
    foRAVerts = np.linalg.norm(raPoly.points[foCenterRA]-raPoly.points,axis=1)
    foRAVerts = np.where(foRAVerts<=foRadius)[0]
    # Delete verts
    DeleteVertsMesh(raObject,foRAVerts)
    # Find newly made boundary of fossa ovalis
    raPoly = PolyDataFromBlender(raObject)
    boundaries = AddOpeningOrientations(raPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    foRABoundary = boundaries[0]
    #
    boundaryIndex = np.array([raPoly.find_closest_point(p) for p in foRABoundary])
    for ii, boundary in enumerate(foRABoundary):
        closestInd = laPoly.find_closest_point(boundary)
        foRABoundary[ii] = np.mean([boundary,laPoly.points[closestInd]],axis=0)
    # Move ring
    MoveVertsMesh(raObject,boundaryIndex,foRABoundary)
    raPoly = PolyDataFromBlender(raObject)
    #
    # For the LA: move to one of the positions in the circle
    laPoly = PolyDataFromBlender(laObject)
    if centerL is None:
        foCenterLA = laPoly.find_closest_point(np.mean(foRABoundary,axis=0))
    else:
        foCenterLA = centerL
    foLAVerts = np.linalg.norm(laPoly.points[foCenterLA]-laPoly.points,axis=1)
    foLAVerts = np.where(foLAVerts<=foRadius)[0]
    #
    DeleteVertsMesh(laObject,foLAVerts)
    #
    laPoly = PolyDataFromBlender(laObject)
    boundaries = AddOpeningOrientations(laPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    boundaries = boundaries[0]
    #
    boundaryIndex = np.array([laPoly.find_closest_point(p) for p in boundaries])
    for ii, boundary in enumerate(boundaries):
        closestInd = raPoly.find_closest_point(boundary)
        boundaries[ii] = raPoly.points[closestInd]
    MoveVertsMesh(laObject,boundaryIndex,boundaries)
    return foCenterRA,foCenterLA

def ExtrudeFossaOvalis(raObject,laObject,raCenterCoords,foRadius,centerR=None,centerL=None):
    # IMPORTANT: raObject and laObject must be closed manifolds!!!
    # Get names
    raName = raObject.name
    laName = laObject.name
    # Make PolyData
    raPoly = PolyDataFromBlender(raObject)
    laPoly = PolyDataFromBlender(laObject)
    #
    # Determine FO Center and belonging verts
    if centerR is None:
        foCenterRA = raPoly.find_closest_point(raCenterCoords)
    else:
        foCenterRA = centerR
    foRAVerts = np.linalg.norm(raPoly.points[foCenterRA]-raPoly.points,axis=1)
    foRAVerts = np.where(foRAVerts<=foRadius)[0]
    # Delete verts
    DeleteVertsMesh(raObject,foRAVerts)
    # Find newly made boundary of fossa ovalis
    raPoly = PolyDataFromBlender(raObject)
    boundaries = AddOpeningOrientations(raPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    foRABoundary = boundaries[0]
    #
    boundaryIndex = np.array([raPoly.find_closest_point(p) for p in foRABoundary])
    for ii, boundary in enumerate(foRABoundary):
        closestInd = laPoly.find_closest_point(boundary)
        foRABoundary[ii] = np.mean([boundary,laPoly.points[closestInd]],axis=0)
    # Move ring
    MoveVertsMesh(raObject,boundaryIndex,foRABoundary)
    raPoly = PolyDataFromBlender(raObject)
    #
    # For the LA: move to one of the positions in the circle
    laPoly = PolyDataFromBlender(laObject)
    if centerL is None:
        foCenterLA = laPoly.find_closest_point(np.mean(foRABoundary,axis=0))
    else:
        foCenterLA = centerL
    foLAVerts = np.linalg.norm(laPoly.points[foCenterLA]-laPoly.points,axis=1)
    foLAVerts = np.where(foLAVerts<=foRadius)[0]
    #
    DeleteVertsMesh(laObject,foLAVerts)
    #
    laPoly = PolyDataFromBlender(laObject)
    boundaries = AddOpeningOrientations(laPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    boundaries = boundaries[0]
    #
    boundaryIndex = np.array([laPoly.find_closest_point(p) for p in boundaries])
    for ii, boundary in enumerate(boundaries):
        closestInd = raPoly.find_closest_point(boundary)
        boundaries[ii] = raPoly.points[closestInd]
    MoveVertsMesh(laObject,boundaryIndex,boundaries)
    return foCenterRA,foCenterLA

#%% Position of the atria in the torso

def AlignValveRings(referenceGeometries,targetGeometries,layers):
    #
    fitPointsRef = np.empty((0,3))
    for key in layers:
        mesh = referenceGeometries[key]
        FB = get_free_boundaries(mesh)
        FF = FB.separate_boundaries()
        boundarySize = [len(ff) for ff in FF]
        index = np.where(boundarySize==np.max(boundarySize))[0][0]
        boundary = np.array(FB.points[FF[index][:,0]])
        fitPointsRef = np.vstack([fitPointsRef,boundary])
    # Fit plane to points of both valves
    centerRef = np.mean(fitPointsRef,axis=0)
    fitPointsRef -= centerRef
    #
    U,s,V = svd(fitPointsRef)
    nRef = V.T[:,2] 
    uRef = V.T[:,0] 
    #
    # Repeat for the target geometry
    fitPointsTarget = np.empty((0,3))
    allPoints = np.empty((0,3))
    pointLengths = [0]
    for key,mesh in targetGeometries.items():
        if key not  in layers: continue
        allPoints = np.vstack([allPoints,mesh.points])
        pointLengths.append(pointLengths[-1]+len(mesh.points))
        #
        FB = get_free_boundaries(mesh)
        FF = FB.separate_boundaries()
        boundarySize = [len(ff) for ff in FF]
        index = np.where(boundarySize==np.max(boundarySize))[0][0]
        boundary = np.array(FB.points[FF[index][:,0]])
        fitPointsTarget = np.vstack([fitPointsTarget,boundary])
    # Fit plane to points of both valves
    centerTarget = np.mean(fitPointsTarget,axis=0)
    fitPointsTarget -= centerTarget
    #
    U,s,V = svd(fitPointsTarget)
    nTarget = V.T[:,2] 
    uTarget = V.T[:,0] 
    # Invert normal if they are not in the same "general direction"
    if np.dot(nTarget,nRef)<0:
        nTarget  = -nTarget
    # Make Rotation matrix to align valves
    R1 = MakeRotationalMatrix(nRef,nTarget)
    R2 = MakeRotationalMatrix(uRef,uTarget)
    R = np.matmul(R1,R2)
    #Rotate
    pointsCenter = np.mean(allPoints,axis=0)
    allPoints = np.matmul(allPoints-pointsCenter,R)+pointsCenter
    #
    outGeometries = deepcopy(targetGeometries)
    k = 0
    for i,(key,mesh) in enumerate(outGeometries.items()):
        if key in layers: 
            outGeometries[key].points = allPoints[pointLengths[k]:pointLengths[k+1],:]
            k +=1
    # # 
    # Now calculate the translation
    # For this, do again the valve plane from above
    fitPointsTarget = np.empty((0,3))
    for key in layers:
        mesh = outGeometries[key]
        FB = get_free_boundaries(mesh)
        FF = FB.separate_boundaries()
        boundarySize = [len(ff) for ff in FF]
        index = np.where(boundarySize==np.max(boundarySize))[0][0]
        boundary = np.asarray(FB.points[FF[index][:,0]])
        fitPointsTarget = np.vstack([fitPointsTarget,boundary])
    # Fit plane to points of both valves
    centerTarget = np.mean(fitPointsTarget,axis=0)
    # 
    # Get translation
    translation = centerRef-centerTarget
    #Transform
    for key,mesh in outGeometries.items():
        if key in layers: outGeometries[key].points = outGeometries[key].points + translation
    #
    return outGeometries,R,translation,pointsCenter

def AlignValveRingsB(referenceGeometries,targetGeometries,key='RA_endo',invertNormal=False):
    outMesh = deepcopy(targetGeometries[key])
    pointsCenter = np.mean(outMesh.points,axis=0)
    #
    # Get the valve plane for the selected layer in reference geometry
    FB = get_free_boundaries(referenceGeometries[key])
    FF = FB.separate_boundaries()
    boundarySize = [len(ff) for ff in FF]
    index = np.where(boundarySize==np.max(boundarySize))[0][0]
    valveRef = FB.points[FF[index][:,0]] 
    # Fit plane to points of both valves
    centerRef = np.mean(valveRef,axis=0)
    valveRef -= centerRef
    U,s,V = svd(valveRef)
    nRef = V.T[:,2] 
    uRef = V.T[:,0] 
    #
    # Get the valve plane for the selected layer in target geometry
    FB = get_free_boundaries(targetGeometries[key])
    FF = FB.separate_boundaries()
    boundarySize = [len(ff) for ff in FF]
    index = np.where(boundarySize==np.max(boundarySize))[0][0]
    valveTarget = FB.points[FF[index][:,0]] 
    # Fit plane to points of both valves
    centerTarget = np.mean(valveTarget,axis=0)
    valveTarget -= centerTarget
    #
    U,s,V = svd(valveTarget)
    nTarget = V.T[:,2] 
    uTarget = V.T[:,0] 
    # Invert normal if they are not in the same "general direction"
    if np.dot(nTarget,nRef)<0:
        nTarget  = -nTarget
        uTarget  = -uTarget
    #
    # Make Rotation to align valves and main axis
    R = MakeRotationalMatrix(nRef,nTarget)
    outMesh.points = np.matmul(outMesh.points-pointsCenter,R)+pointsCenter
    #
    # Align main axes
    U,s,V = svd(outMesh.points)
    uTarget = V.T[:,0] 
    #
    U,s,V = svd(referenceGeometries[key])
    uRef = V.T[:,0] 
    R = MakeRotationalMatrix(uRef,uTarget)
    outMesh.points = np.matmul(outMesh.points-pointsCenter,R)+pointsCenter
    #Translate
    FB = get_free_boundaries(outMesh)
    FF = FB.separate_boundaries()
    boundarySize = [len(ff) for ff in FF]
    index = np.where(boundarySize==np.max(boundarySize))[0][0]
    valveTarget = FB.points[FF[index][:,0]] #first index is the largest boundary
    centerTarget = np.mean(valveTarget,axis=0)
    #
    # Fit plane to points of both valves
    translation = centerRef-centerTarget
    outMesh.points = outMesh.points+translation
    #
    return outMesh

#%% Add orientations related to the openings

def AddOpeningOrientations(mesh,meshName,downsample=5,depth = 0.015,makeCurves=True):
    #
    FB = get_free_boundaries(mesh)
    FF = FB.separate_boundaries()
    #
    coords = [FB.points[inds[:,0]] for inds in FF]
    if makeCurves:     
        for index,coordinates in enumerate(coords):
            curve = CreateCurveFromList(coordinates[::downsample],
                                        'boundary_%s_%d'%(meshName,index),resolution = 4,proportionHandles = 0.1,
                                        bevel = 'ROUND',bevel_depth = depth,loop=True)
            #
            newObject = D.objects.new('boundary_%s_%d'%(meshName,index), curve)
            newObject.active_material = D.materials['Orientation']
            D.collections['Orientations'].objects.link(newObject)
            #
            SmoothCurve(newObject)
    return coords
    
#%% Make coronary sinus
def MakeCSMesh(csBoundary,csApexUAC,referenceGeometries,targetGeometries,layer='epi',downsampleRatio = 5,minDistanceCS = 0.02,conservationRatio=0.9):
    if layer!='': layer = '_'+layer
    # Get CS boundary, ring and normal
    csCenter = np.mean(csBoundary,axis=0)
    ringLength = len(csBoundary)
    U,s,V = svd(csBoundary)
    n = V.T[:,2]  
    #
    # Get CS endpoint in 3D
    csApexUAC = CoordinatesFromUAC(csApexUAC.reshape(-1,2),targetGeometries['LA'+layer],appendage=False)  # Get the 3D positions
    csApexUAC = csApexUAC[0]
    #
    # Find closest point in LA from the CS boundary
    usedGeometry = deepcopy(targetGeometries['LA'+layer]) #TODO test
    removeInds = usedGeometry.point_data['beta']>0.5 # anterior portion is removed
    usedGeometry.remove_points(removeInds,inplace=True)
    #
    closestPoints = np.array([usedGeometry.find_closest_point(point) for point in csBoundary])
    distances = np.array([np.linalg.norm(point-usedGeometry.points,axis=1) for point in csBoundary])
    minDistances = np.nanmin(distances,axis=1)
    closestPoint = closestPoints[np.where(minDistances==np.min(minDistances))[0][0]]
    #
    #Make geodesic path
    csPath = usedGeometry.geodesic(closestPoint,usedGeometry.find_closest_point(csApexUAC))
    pathIndexes = np.array([usedGeometry.find_closest_point(point) for point in csPath.points])
    pathLA = usedGeometry.points[pathIndexes]
    distancesAlongPath = np.array([usedGeometry.geodesic_distance(pathIndexes[0],pt) for pt in pathIndexes[1:]])
    distancesAlongPath = np.hstack([0,distancesAlongPath]) # first one has distance 0
    csPath = csPath.points
    #
    # Move curve to match CS center
    maxTransform = np.linalg.norm(csCenter-csPath[0]) # The first point will be moved the most, to match the center. The last point should not move at all
    minTransform = minDistanceCS # 0 is "touching" LA epi
    transform = (minTransform-maxTransform)*(distancesAlongPath-distancesAlongPath.min())/(distancesAlongPath.max()-distancesAlongPath.min())+maxTransform # linearly interpolate between 0 and maxTransform
    #
    tN = usedGeometry.compute_normals()
    normals = tN.point_data['Normals']
    #
    csCoordinates = np.zeros_like(csPath)
    for index,point in enumerate(csPath):
        if index==0:
            adjustDifference = csCenter-(np.array(point)-np.array(normals[pathIndexes[index]]*transform[index] ))
            csCoordinates[index,:] = csCenter
            continue    
        else:
            csCoordinates[index,:] = np.array(point)-np.array(normals[pathIndexes[index]]*transform[index] )+adjustDifference*(1-index/len(csPath)) #TODO test
    #
    # Make mesh
    #
    U,s,V = svd(csBoundary)
    csNormal = V.T[:,2]  
    #
    # Make faces pattern
    col1 = np.hstack([np.arange(ringLength),np.arange(ringLength)]).reshape(-1,1)
    col2 = np.hstack([np.arange(ringLength)+ringLength,np.roll(np.arange(ringLength),-1)]).reshape(-1,1)
    col3 = np.hstack([np.roll(np.arange(ringLength)+ringLength,-1),np.roll(np.arange(ringLength)+ringLength,-1)]).reshape(-1,1)
    facesPattern = np.hstack([col1,col2,col3])
    #
    Verts = []
    Faces = []
    normTransform = np.flipud(np.linspace(0,1,len(csCoordinates[::downsampleRatio,:])))#(transform-transform.min())/(transform.max()-transform.min())
    #
    for i,centerPoint in enumerate(csCoordinates[::downsampleRatio,:]):
        if i==0:
            for v in csBoundary:
                Verts.append(np.array(v))
                prevNormal = n
        else:
            #new Normal
            newNormal = centerPoint-csCoordinates[i-1,:]
            # TODO: test -> make new Normal average with previous normal
            newNormal = ((1-conservationRatio)*newNormal+conservationRatio*prevNormal)#np.mean([newNormal,prevNormal],axis=0)#
            newNormal = newNormal/np.linalg.norm(newNormal)
            #Rotate
            rotMatrix = MakeRotationalMatrix(newNormal,n)
            points = np.matmul(np.array(csBoundary-csCenter),rotMatrix)
            # Translate
            points = points + centerPoint#newNormal*(centerPoint-csCenter)
            # Scale
            for v in points:
                radialArray = v-centerPoint
                radialArray = radialArray*normTransform[i]
                Verts.append(np.array(centerPoint+radialArray))
            # Faces
            tmpFaces = facesPattern+(i-1)*ringLength
            for face in tmpFaces:
                Faces.append(face)
            prevNormal = newNormal
    #
    return Verts,Faces,pathIndexes,csCoordinates

def MakeCSObject(Verts,Faces,boundaryLength,smoothIterations = 10,doubleThreshold=0.001):
    csMesh = bpy.data.meshes.new("CS")
    #
    csMesh.from_pydata(Verts, [], Faces)
    csMesh.validate()
    #
    # Put in object
    csObject = bpy.data.objects.new(csMesh.name, csMesh)
    col = bpy.data.collections["Atria"]
    col.objects.link(csObject)
    bpy.context.view_layer.objects.active = csObject
    # Put object in edit mode
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    bpy.ops.mesh.select_all(action='SELECT')
    # Edit mesh
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    # Ignore the connecting points, which need to be the same
    for i in range(boundaryLength):
        bm.verts[i].select = False  # select index 4
    # Show the updates in the viewport
    bmesh.update_edit_mesh(me, True)
    # Smooth selected verts
    bpy.ops.mesh.vertices_smooth(factor=1,repeat=smoothIterations)
    # Close tip
    bpy.ops.mesh.select_all(action='DESELECT')
    tipPoints = np.arange(len(bm.verts)-boundaryLength,len(bm.verts))
    edgeList = []
    for edge in bm.edges:
        indexes = [edge.verts[0].index,edge.verts[1].index]
        if np.isin(indexes,tipPoints).all():
            edge.select=True
    try:
        bpy.ops.mesh.fill_grid()
    except:
        print('Manually close tip')
    # Remove duplicates
    bpy.ops.mesh.remove_doubles(threshold = doubleThreshold)
    # Force the normals to point inside, matching the pyvista data
    bpy.ops.mesh.normals_make_consistent(inside=True)
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)

def MergeCSRA(csObject,raMesh,boundaryLength,csThickness):
    # csObject = D.objects['CS_epi']
    # boundaryLength = len(csBoundary)
    # raMesh = deepcopy(TargetGeometries['RA'])
    origLength = len(raMesh.points)
    #
    bm = bmesh.new()
    bm.from_mesh(csObject.data)
    bmesh.ops.triangulate(bm, faces=bm.faces)
    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()
    #
    verts = np.array([np.array(v.co) for v in bm.verts])
    faces = []
    for f in bm.faces:
        S = f.verts
        ff = []
        for ele in S: ff.append(ele.index+origLength)
        faces.append(ff)
    #
    faces = np.array(faces)
    # The vertices of the interface are the same (the first vertices are the boundary (I hope))
    interfaceVerts = verts[:boundaryLength,:]
    vertInds = [raMesh.find_closest_point(v) for v in interfaceVerts]
    # Substitute in faces (the first vertices are the boundary (I hope))
    for csind,index in enumerate(vertInds): faces[faces==csind] = index
    # Add the triangle marker to the faces
    faces = np.hstack([np.ones((len(faces),1),int)*3,faces]).ravel()
    verts = verts[boundaryLength:]
    # Adjust raMesh
    raMesh.points = np.vstack([np.array(raMesh.points),verts])
    raMesh.faces = np.hstack([raMesh.faces,faces])
    for name in raMesh.point_data:
        if name=='Normals': continue
        if name == 'thickness':
            raMesh.point_data[name] = np.hstack([raMesh.point_data[name],np.ones(len(verts))*csThickness])
        else:
            raMesh.point_data[name] = np.hstack([raMesh.point_data[name],np.ones(len(verts))*np.nan])

def AddCStoRA(raMesh,csObject,boundaryLength,csThickness):
    # raMesh is a pyvista mesh!
    originalRASize = len(raMesh.points)
    # Use bmesh as a means to more easily access verts and faces
    bm = bmesh.new()
    bm.from_mesh(csObject.data)
    bmesh.ops.triangulate(bm, faces=bm.faces) # make sure we only have triangles
    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()
    #
    # Get coordinates from points and corresponding faces
    verts = np.array([np.array(v.co) for v in bm.verts])
    faces = []
    for f in bm.faces:
        S = f.verts
        ff = []
        for ele in S: ff.append(ele.index+originalRASize-boundaryLength) # the new faces start at the length of raMesh.points, minus the boundary length which will be excluded
        faces.append(ff)
    faces = np.array(faces)
    # The boundary points will be the same. Thus, merge them in faces
    for index,v in enumerate(verts[:boundaryLength]):
        ind = np.where((v[0]==raMesh.points[:,0])&(v[1]==raMesh.points[:,1])&(v[2]==raMesh.points[:,2]))[0][0]
        faces[faces==index+originalRASize-boundaryLength] = ind
    verts = verts[:-boundaryLength]
    # Make faces like in pyvista
    faces = np.hstack([np.ones((len(faces),1),int)*3,faces]).ravel()
    # Transfer new points, face, and point data to the mesh
    raMesh.points = np.vstack([raMesh.points,verts])
    raMesh.faces = np.hstack([raMesh.faces,faces])
    #
    for name in raMesh.point_data:
        if name=='Normals': continue
        if name == 'thickness':
            raMesh.point_data[name] = np.hstack([raMesh.point_data[name],np.ones(len(verts))*csThickness])
        else:
            raMesh.point_data[name] = np.hstack([raMesh.point_data[name],-np.ones(len(verts))]) #CS does not have UAC!! or region id

#%% Adjust boundaries (closing, zipping, creating valve rings)
def ZipBoundaries(atrium,ignoreValve=True, closed=False,valveIndex=0):
    if closed:
        endoPoly = PolyDataFromBlender(D.objects[atrium+' Endo Closed'])
        epiPoly = PolyDataFromBlender(D.objects[atrium+' Epi Closed'])
    else:
        endoPoly = PolyDataFromBlender(D.objects[atrium+' Endo'])
        epiPoly = PolyDataFromBlender(D.objects[atrium+' Epi'])
    #
    boundariesEndo = AddOpeningOrientations(endoPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    boundariesEpi = AddOpeningOrientations(epiPoly,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    if ignoreValve:
        inds = [i for i in range(len(boundariesEndo)) if i!=valveIndex]
        boundariesEndo = [boundariesEndo[i] for i in inds]
        boundariesEpi =  [boundariesEpi[i] for i in inds]
    if len(boundariesEndo)!=len(boundariesEpi): 
        RuntimeError('Different number of boundaries!')
        return -1
    #
    for boundaryIndex in range(len(boundariesEndo)):
        # Get indexes of endocardial Boundary
        veinEndoIndex = np.array([endoPoly.find_closest_point(point) for point in boundariesEndo[boundaryIndex]])
        # Get indexes of epicardial boundary
        veinEpiIndex = np.array([epiPoly.find_closest_point(point) for point in boundariesEpi[boundaryIndex]])
        #
        # Move epi to endo
        endoVerts = endoPoly.points
        #
        epiObject = D.objects[atrium+' Epi']
        bpy.context.view_layer.objects.active = epiObject
        #
        bpy.ops.object.mode_set(mode='EDIT',toggle=True)
        bpy.ops.mesh.select_all(action='DESELECT')
        #
        objEdit = bpy.context.edit_object
        me = objEdit.data
        bm = bmesh.from_edit_mesh(me)
        bm.verts.ensure_lookup_table()
        #
        for index,v in enumerate(bm.verts):
            if np.isin(index,veinEpiIndex):
                bm.verts[index].select=True
                endoInd = np.where(veinEpiIndex==index)[0][0]
                bm.verts[index].co = endoVerts[veinEndoIndex[endoInd]]
                bm.verts.ensure_lookup_table()
        bmesh.update_edit_mesh(me, True)  
        bpy.ops.object.mode_set(mode='OBJECT',toggle=True) 

def CloseBoundaries(object):
    outName  = object.name + ' Closed'
    outMesh = object.data.copy()
    outObject = D.objects.new(outName,outMesh)
    object.users_collection[0].objects.link(outObject)
    #
    pyvistaMesh = PolyDataFromBlender(object)
    boundaries = AddOpeningOrientations(pyvistaMesh,'tmp',downsample=5,depth = 0.015,makeCurves=False)
    #
    boundaryIndexes = []
    for b in boundaries:
        indexes = []
        for p in b:
            indexes.append(pyvistaMesh.find_closest_point(p))
        boundaryIndexes.append(indexes)
    #
    M = object.matrix_world
    bpy.context.view_layer.objects.active = outObject
    #
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    bpy.ops.mesh.select_all(action='DESELECT')
    # Edit mesh
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()
    # Create center point and new faces to connect with it
    for boundary in boundaryIndexes:
        coordList = []
        vertList = []
        for b in boundary: 
            coordList.append(bm.verts[b].co)
            vertList.append(bm.verts[b])
            bm.verts[b].select=True
        center = np.mean(np.array(coordList),axis=0) 
        bm.verts.new(center)
        bm.verts.ensure_lookup_table()
        bm.faces.ensure_lookup_table()
        # 
        for faceIndex in range(len(boundary)):
            bm.faces.new((bm.verts[-1],vertList[faceIndex],vertList[faceIndex-1]))
    bmesh.update_edit_mesh(me, True)
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)
          
def MakeValveRing(atrium,meshDict,valveIndex=0):
    #
    boundariesEndo = AddOpeningOrientations(meshDict[atrium+'_endo'],'tmp',downsample=5,depth = 0.015,makeCurves=False)
    boundariesEpi = AddOpeningOrientations(meshDict[atrium+'_epi'],'tmp',downsample=5,depth = 0.015,makeCurves=False)
    #
    # Valve ring
    # Get indexes of endocardial Boundary
    valveEndoIndex = np.array([meshDict[atrium+'_endo'].find_closest_point(point) for point in boundariesEndo[valveIndex]])
    # Get indexes of epicardial boundary
    valveEpiIndex = np.array([meshDict[atrium+'_endo'].find_closest_point(point) for point in boundariesEndo[valveIndex]])
    # Linear indexes
    indsEndo = np.arange(0,len(valveEndoIndex))
    indsEpi = np.arange(len(valveEndoIndex),len(valveEndoIndex)+len(valveEpiIndex))
    # Make faces
    faces = np.hstack([indsEndo.reshape(-1,1),
                        indsEpi.reshape(-1,1),
                        np.roll(indsEpi,-1).reshape(-1,1)])
    faces = np.vstack([faces,np.hstack([np.roll(indsEpi,-1).reshape(-1,1),
                                        indsEndo.reshape(-1,1),
                                        np.roll(indsEndo,-1).reshape(-1,1)])])
    faces = [f for f in faces]
    # Make verts
    bm = bmesh.new()
    bm.from_mesh(D.objects[atrium+' Endo'].data)
    bm.verts.ensure_lookup_table()
    endoPoints = np.array([np.array(v.co) for v in bm.verts])
    endoPoints = endoPoints[valveEndoIndex]
    #
    bm = bmesh.new()
    bm.from_mesh(D.objects[atrium+' Epi'].data)
    bm.verts.ensure_lookup_table()
    epiPoints = np.array([np.array(v.co) for v in bm.verts])
    epiPoints = epiPoints[valveEpiIndex]
    #
    points = np.vstack([endoPoints,epiPoints])
    # Make object
    heartMesh = D.meshes.new('valveRing'+atrium)
    heartMesh.from_pydata(points, [], faces)
    heartMesh.validate()
    obj = D.objects.new(heartMesh.name, heartMesh)
    col = bpy.data.collections['Atria']
    col.objects.link(obj)
    bpy.context.view_layer.objects.active = obj

#%% Functions for exporting (currently not really needed)
def MakeHeartObject(heartObjects,vertexGroups,outName='heart',outCollection='Atria'):
    lengths = []
    VV = []
    FF = []
    # Get vertices and faces of these meshes
    for index,name in enumerate(heartObjects):
        obj = D.objects[name]
        ff, vv = create_submesh(obj, [], modLevels=0)
        lengths.append(len(vv))
        for f in ff:
            FF.append([f[0]+len(VV),f[1]+len(VV),f[2]+len(VV)])
        for v in vv: VV.append(v)
    heartMesh = D.meshes.new(outName)
    #
    heartMesh.from_pydata(VV, [], FF)
    heartMesh.validate()
    heartObject = D.objects.new(heartMesh.name, heartMesh)
    col = bpy.data.collections[outCollection]
    col.objects.link(heartObject)
    bpy.context.view_layer.objects.active = heartObject
    #
    k = 0
    for index,name in enumerate(vertexGroups):
        vg = heartObject.vertex_groups.get(name)
        if vg is None: 
            vg = heartObject.vertex_groups.new(name=name)
        # Merge duplicates (maybe relevant for CS)
        # Assig
        verts = [v for v in range(k,k+lengths[index])]
        k = k+lengths[index]
        vg.add(verts, 1.0, 'ADD')

#%% Thickness mapping
def GetWallThickness(endo:pv.PolyData,epi:pv.PolyData,returnValues = False):
    kdTree = KDTree(epi.points)
    d_kdtree, idx = kdTree.query(endo.points)
    if returnValues:
        return d_kdtree
    else:
        endo.point_data['thickness'] = d_kdtree

def ConvertToBarycentric(point,trianglePoints):
    # Check wikipedia for info on the names
    r = point.reshape(2,1)
    r1 = trianglePoints[0,:].reshape(2,1)
    r2 = trianglePoints[1,:].reshape(2,1)
    r3 = trianglePoints[2,:].reshape(2,1)
    #
    T = np.array([[r1[0]-r3[0], r2[0]-r3[0]],[r1[1]-r3[1], r2[1]-r3[1]]]).squeeze()
    #
    L = np.matmul(np.linalg.inv(T),(r-r3))
    L = np.vstack([L,1-L[0]-L[1]])
    #
    return L

def ConvertCurveOutlinesToCircle(curveMaterials):
    # curveMaterials are the materials of the curves to be converted to circles. 
    CurvesObjects = [ o for o in D.objects if o.type == 'CURVE' and any(m in o.data.materials for m in curveMaterials) ]
    CurveCollections = [obj.users_collection[0] for obj in CurvesObjects]

    CurveDatas = [o.data for o in CurvesObjects]
    CurveMaterials = [c.materials[0] for c in CurveDatas]
    CurveSplines = [c.splines[0] for c in CurveDatas]
    for c in CurveDatas:
        if (c.name in D.objects) and not (c.name in D.curves):
            outline = outline.data
        else:
            outline = c.bevel_object
        CurveOutlines.append(outline)

    # Estimate radius and get world coordinates for the splines
    curveCoordinates = []
    outlineRadius = []
    for object,data,spline,outline in zip(CurvesObjects,CurveDatas,CurveSplines,CurveOutlines):
        print(object)
        if outline is None:  # Nothing to be done, already circle
            print(object.name + ' is already circle')
            curveCoordinates.append(None)
            outlineRadius.append(None)
            continue
        #
        M = object.matrix_world
        points = [M @ v.co for v in spline.bezier_points]
        curveCoordinates.append(points)
        #
        # Get radius of object
        #
        # try:
        #     outlineCurve = D.curves[outline.name]
        # except:
        #     outlineObject = D.objects[outline.name]
        #     outlineCurve = outlineObject.data
        #
        outlinePoints = [v.co*outline.scale for v in outline.data.splines[0].bezier_points]
        outlinePoints = np.asarray(outlinePoints)
        outlinePoints = outlinePoints-np.mean(outlinePoints,axis=0)
        outlineRadius.append(np.mean(np.linalg.norm(outlinePoints,axis=1)))

    #Delete old objects ...
    try:
        bpy.ops.object.select_all(action='DESELECT')
    except:
        bpy.ops.object.editmode_toggle()

    for obj in CurvesObjects: obj.select_set(True)
    bpy.ops.object.delete()

    # ... and create new ones
    for index,(points,radius) in enumerate(zip(curveCoordinates,outlineRadius)):
        name = CurvesObjects[index].name
        curve = CreateCurveFromList(np.array(points),
                                    name,resolution = 12,proportionHandles = 0.25,
                                    bevel = 'ROUND',bevel_depth = radius)
        #
        newObject = bpy.data.objects.new(name, curve)
        newObject.active_material = D.materials[CurveMaterials[index]]
        # add object to scene collection
        CurveCollections[index].objects.link(newObject)
        newObject.select_set(False)    

def BylayerFromThickness(polydata):
    # polydata is a pyvista PolyData object with 'thickness' in point data
    if 'thickness' not in polydata.point_data: 
        RuntimeError('Calculate thickness first')
        return -1
    # ComputeNormals
    targetN = polydata.compute_normals(point_normals=True,cell_normals=False) #FIXME this is the issue with the crash
    #
    endoPoints = polydata.points + targetN.point_data['Normals'] * polydata.point_data['thickness'].reshape(-1,1)/2  #RAThickness/2*scaleFactor#
    outEndo = pv.PolyData(endoPoints,polydata.faces)
    outEndo.point_data['alpha'] = polydata.point_data['alpha']
    outEndo.point_data['beta'] = polydata.point_data['beta']
    #
    epiPoints  = polydata.points - targetN.point_data['Normals'] * polydata.point_data['thickness'].reshape(-1,1)/2 #RAThickness/2*scaleFactor#
    outEpi = pv.PolyData(epiPoints,polydata.faces)
    outEpi.point_data['alpha'] = polydata.point_data['alpha']
    outEpi.point_data['beta'] = polydata.point_data['beta']
    #
    return outEndo,outEpi

#%% Linear algebra functions

def MakeRotationalMatrix(a,b = None):
    # a can be a vector ( to align with b) or an angle
    if b is not None:
        crossProduct = np.cross( a, b )
        sinA = np.linalg.norm(crossProduct)
        axis = crossProduct / sinA
        cosA = np.dot( a, b )
        oneMinusCosA = 1.0 - cosA
        #
        RotationalMatrix = np.array([[  axis[0] * axis[0] * oneMinusCosA + cosA,
                                        axis[1] * axis[0] * oneMinusCosA - (sinA * axis[2]),
                                        axis[2] * axis[0] * oneMinusCosA + (sinA * axis[1])],
                                        [axis[0] * axis[1] * oneMinusCosA + (sinA * axis[2]),
                                        axis[1] * axis[1] * oneMinusCosA + cosA,
                                        axis[2] * axis[1] * oneMinusCosA - (sinA * axis[0])],
                                        [axis[0] * axis[2] * oneMinusCosA - (sinA * axis[1]),
                                        axis[1] * axis[2] * oneMinusCosA + (sinA * axis[0]),
                                        axis[2] * axis[2] * oneMinusCosA + cosA]])
    else:
        RotationalMatrix = np.array([[np.cos(a), -np.sin(a)],[np.sin(a),np.cos(a)]])
    return RotationalMatrix

def MakeTranslationalMatrix(v):
    #v is the delta in the three dimensions
    T = np.eye(4)
    T[-1,:3] = v
    return T

#%% Auxiliary functions to manipulate curves and meshes
def MoveVertsMesh(object,indexes,newCoords):
    '''
    Move vertices given a list of indexes and their new positions
    '''
    bpy.context.view_layer.objects.active = object
    #
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    bpy.ops.mesh.select_all(action='DESELECT')
    #
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    for index,coord in zip(indexes,newCoords):
        bm.verts[index].co = coord
    bmesh.update_edit_mesh(me, True)  
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)

def DeleteVertsMesh(object,indexes):
    '''
    Delete vertices given a list of indexes
    '''
    bpy.context.view_layer.objects.active = object
    #
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    bpy.ops.mesh.select_all(action='DESELECT')
    #
    objEdit = bpy.context.edit_object
    me = objEdit.data
    bm = bmesh.from_edit_mesh(me)
    bm.verts.ensure_lookup_table()
    for index in indexes:
        bm.verts[index].select=True
    bpy.ops.mesh.delete()
    bmesh.update_edit_mesh(me, True)  
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)

def CreateCurveFromList(knotPositions,name,resolution = 12,proportionHandles = 0.25,
                        bevel = 'ROUND',bevel_depth = 0.1,loop=False):
    '''
    Creates 3D Bezier curve from list of points
    knotPositions can be a numpy array or a list of Vectors (I think)
    '''
    # Create curve and add properties
    curveData = D.curves.new(name=name, type='CURVE')
    curveData.dimensions = '3D'
    curveData.bevel_mode = bevel # or PROFILE or OBJECT
    curveData.bevel_depth = bevel_depth
    #
    # Get Spline and add knots
    splines = curveData.splines
    spline = splines.new(type='BEZIER')
    spline.use_cyclic_u = loop # Not a loop
    spline.resolution_u = resolution 
    #
    count = len(knotPositions)
    knots = spline.bezier_points
    knots.add(count=count - 1)
    knots_range = range(0, count, 1)
    #
    # Edit knots and handles
    handle_type = 'FREE' #  ['FREE', 'VECTOR', 'ALIGNED', 'AUTO']
    for kInd,knot in enumerate(knots):
        # Center handle
        knot.co = Vector(knotPositions[kInd])
        # Array connecting first and second point
        if kInd!=len(knots)-1:
            nextArray = knotPositions[kInd+1]-knotPositions[kInd]
        # else it is the same as the last one
        #
        knot.handle_left = Vector(knotPositions[kInd]-proportionHandles*nextArray)
        knot.handle_right = Vector(knotPositions[kInd]+proportionHandles*nextArray)
        #
        # Other handle details
        knot.handle_left_type = handle_type
        knot.handle_right_type = handle_type
    #
    return curveData

def SmoothCurve(curveObject,smoothLevel=5):
    '''
    Smooth a curve
    '''
    try:
        bpy.ops.object.select_all(action='DESELECT')
    except:
        bpy.ops.object.mode_set(mode='OBJECT',toggle=True)
        bpy.ops.object.select_all(action='DESELECT')
    # put in edit mode
    # newObject.select_set(True)
    C.view_layer.objects.active = curveObject
    bpy.ops.object.mode_set(mode='EDIT',toggle=True)
    #
    # Select curve
    bpy.ops.curve.select_all(action='SELECT')
    #Smooth
    for i in range(smoothLevel):  bpy.ops.curve.smooth()
    #
    bpy.ops.object.mode_set(mode='OBJECT',toggle=True)
    # add object to scene collection
    curveObject.select_set(False)
