#
# This code provides some common operations on the torso model,
# loading and saving parts in different file formats, and performing
# some simple transformations on them. It is not supposed to be fixed
# forever, modifications may be made to serve everyday needs. To
# facilitate modifications and debugging, most of the code is put here,
# in a separate module, to allow reloading.
# 

# Coordinates:
#
#  |model24/torso24-01.blend| still works in Halifax coordinates
#
#  |model24/torso24-02.blend| converted to MRI coordinates by manually
#  setting the transform of the torso to locX=-3, locY=-2.77,
#  locZ=3.28, RotX=-90, while keeping heart and lungs parented to it.
#

import bpy
import bmesh
from mathutils import Vector, Matrix
D = bpy.data
# third-party and standard modules
import numpy as np
import math
import json

# # my own modules
import sys
sys.path.append('/mnt/d/vgmar/Documents/GitHub/USI/model24')
sys.path.append(r'D:\vgmar\Documents\GitHub\USI\model24')
from utils_cp import listwrite

class Struct: pass

class TedException:
    def __init__(self, msg):
        self.msg = msg
        
# External files use 1-mm units. We scale everything to normal Blender
# sizes so that things like focal distances of cameras, ranges of
# lights, scaling parameters of textures do not have to be adapted too
# much.  A Blender unit will be 10 cm.
#
mm = 0.001
bunny = 0.1

# initialization code
#
print("loading mod_torso_editor")

# Abuse the popup menu function to pop up an error message:
#   Error("foo!")
#   Error("unexpected x=%d" % x)
#
#   Error() may be used for detected errors as well as to
#   handle exceptions. It uses the print_exc function from
#   module traceback to print all the information that would
#   be printed in case of an uncaught exception. Uncaught
#   exceptions cause the plugin to be closed and its GUI
#   removed from Blender.
#
def Error(msg):
    """Print traceback to stderr, and a message in a popup.

       Error(msg) may be used for detected errors as well as to
       handle exceptions."""
    print("Error: %s" % msg)
    str = traceback.format_exc()
    if str[0:4]=='None':
        print("no exception, no traceback")
    else:
        print(str)
    Draw.PupMenu("ERROR: %s%%t|OK" % msg)

def Warning(msg):
    Draw.PupMenu("WARNING: %s%%t|OK" % msg)

    
def find_object(name):
    #
    # (obj,users) = find_object(name)
    # 
    # get a handle to the object with the given name,
    # return the object and the number of users.
    # quietly set users=0 to indicate that it does not exist.
    #
    # The only way to know if an object with a certain name exists
    # is to try to Get() it.  If it does not exist, Object.Get()
    # raises an AttributeError exception.
    #

    obj = bpy.data.objects.get(name, None)
    users = getattr(obj, "users", 0)

    return obj,users


def get_object(name):
    #
    # complains (rather than exiting the GUI) if the object does not exist
    #
    obj,users = find_object("%s" % name)
    if users==0:        # never existed or deleted
        Error("object '%s' does not exist" % (name))
        return
    return(obj)


def get_object_mesh(name):
    #
    # mesh = get_object_mesh(name)
    #
    # get the mesh from the object with the given name;
    # complain if the object does not exist
    #
    (obj,users) = find_object("%s" % name)
    if users==0:        # never existed or deleted
        Error("object '%s' does not exist" % (name))
        return
    mesh = obj.getData()
    return(mesh)

def get_or_create_object_mesh(oname,silent=False):
    #
    # like get_object_mesh, but don't complain:
    # If the object exists, get its mesh data and clear it.
    # Else, create the mesh.
    #
    # Since Blender 2.42, if the object does exist, it may still be
    # considered as "deleted", in which case its users count
    # will be zero.
    #
    # For new version of Blender, see e.g.
    #
    #     https://blender.stackexchange.com/questions/125034
    #     https://blender.stackexchange.com/questions/27234
    #
    
    obj,users = find_object(oname)
    
    if users==0:        # never existed or deleted
        mesh = bpy.data.meshes.new(oname)  # create a new mesh
        obj = bpy.data.objects.new(oname, bm)  # creates new object
        bpy.context.collection.objects.link(obj)
        newmesh = True
        if not silent: print("created new mesh", obj.name)
    else:
        mesh = obj.data
        if mesh.is_editmode:
            bm = bmesh.from_edit_mesh(mesh)
            bmesh.ops.delete(bm,geom=bm.verts,context='VERTS')
            bmesh.ops.delete(bm,geom=bm.faces,context='FACES')
            bm.update_edit_mesh(mesh)
        else:
            bm = bmesh.new()
            bm.from_mesh(mesh)
            bmesh.ops.delete(bm,geom=bm.verts,context='VERTS')
            bmesh.ops.delete(bm,geom=bm.faces,context='FACES')
            bm.to_mesh(mesh)
            obj.data.update()
            newmesh = False
        if not silent: print("using existing mesh", obj.name)

    return (obj,mesh,newmesh)
    

def load_brick(oname, stuff, layer):
    #
    # Load a single "brick" from file, e.g. the torso or a lung.  Some
    # bricks require special treatment, such as the electrodes, which
    # in fact consist of many objects.
    #
    in_editmode = Window.EditMode()
    if in_editmode: Window.EditMode(0) # must leave edit mode before changing an active mesh
    
    print('oname = ' + oname)
    if oname=='bsm':                  # Amsterdam 64-lead BSM
        opt = Struct()
        opt.fname = "var_elecs.txt"   # input filename
        opt.prefix = "bsm_"           # prefix for electrode names
        opt.torso = 0                 # positions are in heart coordinates
        opt.normals = 0               # file does not specify normal orientations
        opt.project = 1               # project on torso surface
        opt.layer = 4                 # Blender layer to put them in
        load_elecs(oname, opt)
    elif oname=='mbsm':               # Maastricht BSM
        opt = Struct()
        opt.fname = "maas_elecs.txt"
        opt.prefix = "mbsm_"
        opt.torso = 1                 # positions are in MRI coords
        opt.normals = 1               # file specifies normal orientations
        opt.project = 1               # project on torso surface
        opt.layer = 4
        load_elecs(oname, opt)
    elif oname=='lux':               # complete 192-electrode BSM
        opt = Struct()
        opt.fname = "lux_elecs.txt"
        opt.prefix = "lux_"
        opt.torso = 1                 # positions are in MRI coords
        opt.normals = 0               # file specifies normal orientations
        opt.project = 1               # project on torso surface
        opt.layer = 4
        load_elecs(oname, opt)
    elif oname=='act':
        load_act()
    elif oname=='heart':
        # Don't actually load this one; this doesn't work yet for a
        # multi-material object. Need a special file format for that.
        # Don't even do sops() on it because it is a multi-material object.
        pass
    elif oname=='probe':
        load_probe(oname, layer)
    else:
    # Get an ordinary object from OBJ file.
        print("getting object")
        (obj,mesh,newmesh) = get_or_create_object_mesh(oname)
        print("reading OBJ file")
        try:
            filename = "var_%s.obj" % oname;
            mesh = objfile.read(mesh,filename)
        except IOError:
            filename = "var_%s.off" % oname;
            mesh = off.read(mesh,filename)
        print("sops(%s,%s,%s,%s,%d,%d)..." % (obj,mesh,oname,stuff,layer,newmesh))
        sops(obj,mesh,oname,stuff,layer,newmesh)
        print("done")

    # assuming zero rotation corresponds to frame 1, go to frame 1
    # to prevent a confusing jump later on
    #
    if in_editmode: Window.EditMode(1)        # be nice
    Blender.Set('curframe', 1)
    Blender.Redraw()
    message = "Successfully imported " + oname
    print(message)


def load_probe(oname, layer):
    #
    # create a presentable object to represent the probe positions.
    # The vertices that we read in are the actual measurement
    # positions, there are hundreds of them. We want to represent a
    # selection of them. The idea is to create a sort of catheter,
    # with electrodes at these selected points.
    #
    print("setting up the probe object")
    (obj,mesh,newmesh) = get_or_create_object_mesh(oname)
    filename = "var_%s.obj" % oname;
    mesh = objfile.read(mesh,filename)
    sops(obj, mesh, oname, "probe", layer, newmesh)
    mesh = Mesh.Get(oname)    # after transform

    mv = mesh.verts
    Nv = len(mv)
    N = 4             # nr of points on circumference
    r = 2.0           # radius
    esel = list(range(4,14))   # electrodes that get a metal painting
    
    # create vertices. Use pairs of original points to define the electrodes.
    #
    vv = []
    vsel = []     # selection of original vertices to use
    for i in range(0, Nv-2, 10): vsel += [i,i+1]
    vsel = vsel[1:-1]       # skip first and last electrode
    for i in vsel:
        zax = Vector(mv[i+1].co - mv[i].co).normalize()
        xax = zax.cross(Vector(1,0,0)).normalize()
        yax = zax.cross(xax)
        for j in range(N):
            a = float(j)/N*2*pi
            vv.append(Vector(mv[i].co) + xax*r*cos(a) + yax*r*sin(a))
    mesh.verts.delete(list(range(len(mesh.verts))))
    mesh.verts.extend(vv)
    Nc = len(vv)/N       # nr of vertex rings

    # create faces
    metal = False
    elec = 0
    for i in range(Nc-1):
        mesh.faces.extend([[i*N+j, (i+1)*N+j, (i+1)*N+(1+j)%N, i*N+(1+j)%N] for j in range(N)])
        if metal:
            elec += 1
            if elec in esel:
                Nf = len(mesh.faces)
                for j in range(N):
                    mesh.faces[Nf-1-j].mat = 1  # 1 here is 2 in the GUI
        metal = not metal
        
    print("probe complete: %d vertices, %d faces" % (len(mesh.verts), len(mesh.faces)))
    print("probe points selection: ")
    print([vsel[1:-1:2][i] for i in esel])  # select points corresponding to electrodes


def sops(obj,mesh,oname,stuff,layer,newmesh):
    #
    # set object properties.
    #
    # Adapt the object to our preferences, such as material,
    # scale, and layer.
    #
    if oname != 'combi':
        material = Material.Get(stuff)
        obj.setMaterials([material])        # must be a list
        obj.colbits = 1
        mesh.name = oname
        obj.name = oname

        # When working with DICOM images we are typically working in
        # shaded mode. Objects without texture then do not come out
        # properly in interactive view. We prevent this by limiting
        # their drawtype to solid.
        obj.drawType = Object.DrawTypes.SOLID
        
        if newmesh:
            # mesh.setMode("TwoSided")
            mods = obj.modifiers            # get the object's modifiers
            mod = mods.append(Modifier.Type.SUBSURF) # add a new subsurf modifier
            mod[Modifier.Settings.LEVELS] = 1 # nr of subsurf subdivision
                                              # levels in interactive display
            mod[Modifier.Settings.RENDLEVELS] = 2  # ... during rendering
            # mod[Modifier.Settings.EDITMODE] = False  # no subsurfing in edit mode
            # mod[Modifier.Settings.REALTIME] = False  # no subsurfing in interactive disp
        else:
            mods = obj.modifiers            # get the object's modifiers
            # nothing to do with it
            
        # put it in the desired layer. Layer is a bitmask.
        obj.Layer = 2**(layer-1)

    S = ScaleMatrix(mm/bunny, 4)
    if oname in ('atria', 'aamyo','poly', 'poly2', 'probe',
                 'curves', 'tubes', 'quivers',
                 'wquiv', 'equiv', 'Lcav'):
        # Exception for stuff that I may want to re-import after
        # saving, for example to see what the vertex selection and
        # subsurfacing has done with it.
        Mb = heart_matrix()
        R = Mb.copy().rotationPart().resize4x4()
        obj.setMatrix(R*S)
    else:
        obj.setMatrix(S)

    # # Make object "track target" the parent of all. This helps to
    # # rotate the whole heart (and torso) for an animation. First
    # # make sure it has zero orientation, otherwise things won't
    # # match.
    # tt = Blender.Object.Get("track target");
    # R = RotationMatrix(0, 4, "y")    # zero degrees, 4-dim, around y axis
    # tt.setMatrix(R)
    # tt.makeParent([obj], 0, 0)    # must be a list


def save_brick(bname):
    #
    # Save one brick to OBJ file.  Check for existence and call objfile.write().
    # Special treatment can be added for specific bricks: for the torso,
    # inner and outer surfaces of the skeletal muscle layer are created.
    #
    # No common check for existence: bname does not have to be the name
    # of any object. Generally it does, but there are exceptions such
    # as 'elecs'.
    #
    if   bname=='torso' : save_torso()
    elif bname=='heart' : save_heart(bname)
    elif bname=='zone'  : save_zone(bname)
    # elif pset[bname]['type']=='elecset' : save_elecs(bname)
    elif bname=='act' : write_act(bname)
    elif bname=="LAPGBLOCK": save_curves_by_type(["LAPGBLOCK"], "var_tubes_lapgblock.obj")
    elif bname=="LOMBlock": save_curves_by_type(["LOMBlock"], "var_tubes_lomblock.obj")
    elif bname=="Isthmus": save_curves_by_type(["Isthmus"], "var_tubes_isthmus.obj")
    elif bname=='probe' :
        obj = Object.Get(bname)
        mesh = obj.getData()
        M = heart_export_matrix(obj)
        ff = []
        vv = [scaleout(M, v.co) for v in mesh.verts]
        objfile.listwrite('var_probe.obj', ff, vv)
        print("saved probe")
    else:
        save_simple_brick(bname)      # save with Blender transform

def save_simple_brick(name):
    #
    # Simplest brick saver, for most of them.  Call write_submesh with
    # valid_groups=None, to save all vertices regardless of vertex
    # groups.
    #
    obj,users = find_object(name)
    if users != 0:
        write_submesh(obj, None, f"var_{name}.obj")
        # write_submesh_alt([obj], f"var_{name}.obj")

            
# Inner and outer expansion were -10,20 in the Montreal/Halifax
# model. For golem23 I used -10,30 because it's a big guy.
# torso_expand_elecs could be near torso_expand_outer to get the
# electrodes placed on a surface used for the BEM model. Currently it
# is zero, so that var_elecs.txt can be reloaded and then puts the
# electrodes on the original torso surface.
#            
torso_expand_inner = -10     # for var_muscle_in.obj
torso_expand_elecs = 0      # for the surface electrodes
torso_expand_outer = 30      # for var_muscle_out.obj

def save_torso():
    #
    # Specialized brick saver for the torso surface. Writes three surfaces:
    # the original one, as well as inner and outer surfaces of the blown-up
    # skeletal muscle layer
    #
    obj,users = find_object('torso')

    write_submesh(obj, None, "var_torso.obj", orient="heart")
    write_submesh(obj, None, "upright_torso.obj", orient="torso")

    # duplicate the mesh
    orig_mesh = obj.data
    mesh = orig_mesh.copy()
    obj.data = mesh
    
    try:
        # Adapt the mesh, link it to the object (write_submesh needs this),
        # and write it to OBJ file.
        torso_expand(mesh, torso_expand_inner)
        write_submesh(obj, None, "var_muscle_in.obj")
        
        torso_expand(mesh, torso_expand_outer)
        write_submesh(obj, None, "var_muscle_out.obj")
        
    finally:
        obj.data = orig_mesh
        bpy.data.meshes.remove(mesh)
    
def torso_expand(mesh, e):
    #
    # Inflate or deflate the torso surface to obtain the
    # inner and outer surfaces of the virtual skeletal
    # muscle layer.
    #
    # The surface is expanded along the vertex normals.
    #
    bm = bmesh.new()
    bm.from_mesh(mesh)

    for v in bm.verts:
        v.co += e * v.normal
    
    bm.to_mesh(mesh)
    bm.clear()
    bm.free()

    return mesh


def save_heart(obj):
    #
    # specialized saver for the heart, which may include aorta, venae
    # cavae, and pulmonary veins and arteries. Faces are characterized
    # by the vertex groups of their vertices, and used to create
    # several closed surfaces, each in its own output file.
    #

    # if "NCM" in obj.vertex_groups:
    #     write_submesh(obj, ["LV endo", "NCM"], "var_NCM.obj") # noncompact myocardium

    # 2018-06: export of MRI atria for comparison to Rhythmia data
    # separate the objects, only LA and RA endocardium, plus full atria
    #
    write_submesh(obj, ["LA endo"], "model24_laendo_mri.obj", orient="mri")
    write_submesh(obj, ["RA endo"], "model24_raendo_mri.obj", orient="mri")
    write_submesh(obj, ["LA endo", "LA epi", "RA endo", "RA epi"],
                  "model24_atria_mri.obj", orient="mri")

    # Envelope of everything in the heart. This one is not a manifold,
    # but this is perhaps not a problem with voxelgen.
    # write_submesh(obj, ["VV epi", "LA epi", "RA epi", "Lfence",
    #                     "Rfence", "LVAOC", "valve ring"], "var_heart.obj")
     
    # write_submesh(obj, ["LV endo", "RV endo", "VV epi"], "var_ventricles.obj")
    
    write_submesh(obj, ["LA endo", "LA epi", "RA endo", "RA epi"], "var_atria.obj")

    write_submesh(obj, ["LA endo", "LV endo", "LVAOC", "Lfence"], "var_Lcav.obj")
    
    write_submesh(obj, ["RA endo", "RV endo", "Rfence"], "var_Rcav.obj")
    
    # write_submesh(obj, ["PV", "vena cava", "aorta endo", "PA",
    #                     "Lfence", "Rfence", "cap"], "var_blood.obj")
    
    write_submesh(obj, ["valve ring"], "var_ring.obj")
    
    # # Pacing points
    # for i in range(1,21):
    #     write_submesh(obj, ["lmark_pacing%02d"%i], "lmark_pacing%02d.obj"%i)
    # # Freeze Pacing points
    # for i in range(1,2):
    #     write_submesh(obj, ["freeze_pacing%02d"%i], "freeze_pacing%02d.obj"%i)

    save_curves()
    
    save_matrix()

    save_act(obj)

    save_lmark(obj)

def save_zone(name):
    #
    # specialized saver for the zone object, which defines zones of
    # myocardium that may receive special treatment in the simulations.
    #
    obj,users = find_object(name)

    write_submesh(obj, ["zone1"], "var_zone1.obj")
    write_submesh(obj, ["zone2"], "var_zone2.obj")
    write_submesh(obj, ["zone3"], "var_zone3.obj")
    
    
def save_act(obj):
    # save activation points; replaces write_act()

    M = heart_export_matrix(obj)
    mesh = obj.data
    groups = {g.name:g.index for g in obj.vertex_groups if g.name.startswith('act_')}

    with open("var_act.txt", "wt") as fo:
        fo.write("# activation sites written by mod_torso_editor.py\n")
        for name,gid in groups.items():
            vi = [v.co for v in mesh.vertices if any(vg.group == gid for vg in v.groups)]
            pos = sum(vi,Vector([0,0,0])) / len(vi)
            pos = scaleout(M, pos)
            fo.write("%s %.3f %.3f %.3f\n" % (name, pos[0], pos[1], pos[2]))
    
def save_lmark(obj):
    # save landmark points for geometry matching; MRI orientation

    M = torso_export_matrix(obj)
    mesh = obj.data
    groups = {g.name:g.index for g in obj.vertex_groups if g.name.startswith('lmark_')}
    with open("var_lmark.txt", "wt") as fo:
        fo.write("# landmark sites written by mod_torso_editor.py\n")
        for name,gid in groups.items():
            vi = [v.co for v in mesh.vertices if any(vg.group == gid for vg in v.groups)]
            pos = sum(vi,Vector([0,0,0])) / len(vi)
            pos = scaleout(M, pos)
            fo.write("%s %.3f %.3f %.3f\n" % (name, pos[0], pos[1], pos[2]))


# This version of the write_submesh function subsurfs the mesh before
# dividing it. The advantage of submeshing first is that the partial
# meshes are consistent, which they would not be if they were
# submeshed separately. To make this work correctly we must make a
# selection of faces (rather than vertices) before submeshing, and
# extract the selection from the subsurfed mesh. If this were done
# with vertices, the selection would be slightly expanded at some
# places. For example, the ventricular myocardial surface would
# be extended with a part of the atrioventricular ring.
#
# Briefy, we duplicate the mesh, set the faces selection status,
# subsurf, remap the vertices, select the faces, and export.
#
def write_submesh(obi, valid_groups, fname, orient="heart",modLevel=2):
    ff, vv = create_submesh(obi, valid_groups,modLevel=modLevel)
    if orient=="heart":
        M = heart_export_matrix(obi)
    else:
        M = torso_export_matrix(obi)
    
    vvrot = [scaleout(M,v) for v in vv]
    listwrite(fname, ff, vvrot)

    print("Wrote %d faces %d vertices in %s" % (len(ff), len(vv), fname))

def write_submesh_alt(objList, fname, orient="heart"):
    ff, vv = create_submesh_alt(objList)
    if orient=="heart":
        M = heart_export_matrix(D.objects['_heart'])
    else:
        M = torso_export_matrix(D.objects['_heart'])
    
    vvrot = vv#[scaleout(M,v) for v in vv]
    listwrite(fname, ff, vvrot)

    print("Wrote %d faces %d vertices in %s" % (len(ff), len(vv), fname))

def create_submesh_alt(objList): #FF,vV = create_submesh_alt([D.objects['RA Endo']])
    FF = np.empty((0,3),int)
    VV = []
    #
    totalV = len(VV)
    for obj in objList:
        mesh = obj.data
        bm = bmesh.new()
        bm.from_mesh(mesh)
        bm.verts.ensure_lookup_table()
        bm.faces.ensure_lookup_table()
        currObjVV = [ list(v.co.xyz) for v in bm.verts ]
        M = heart_export_matrix(obj)
        VV.append([scaleout(M,v) for v in currObjVV])
        for p in bm.faces:
            FF = np.vstack([FF,np.array([p.verts[0].index+totalV,
                    p.verts[1].index+totalV,
                    p.verts[2].index+totalV]).reshape(1,3)])
        totalV = totalV+len(bm.verts)
    #
    VVout = [v for V in VV for v in V]
    FFout = list(FF)
    return FFout, VVout
    
def create_submesh(obi, valid_groups,modLevel=2):
    # We need a temporary object to mess with the selected status of
    # faces and to operate on with the subsurf modifier.
    #
    # Blender 2.9:  10+ years later we use a different strategy.
    # In recent versions of Blender it's possible to delete objects
    # and meshes quite easily, so we don't stress to much the memory
    # if we create a new object to play with and then we remove it.
    #
    # Therefore, the strategy is to duplicate the object and the mesh,
    # remove all modifiers, add our subsurf modifier, then export
    # the "evaluated" mesh.  The "evaluated" mesh is the one shown
    # in the viewport, which has modifiers applied, and may therefore
    # differ from the backbone mesh (it can even be a different type,
    # like a curve).
    #
    # NOTE: it is tempting to first remove vertices and then subsurf:
    # the result is however wrong.  In order to apply subsurf first and
    # keep track of the vertices in the right group, we select the faces
    # right before applying the modifier.
    #

    # duplicate the object (will delete later)
    #
    obj = obi.copy()
    obj.data = obi.data.copy()  # we want to modify the mesh
    obj.animation_data_clear()
    obj.modifiers.clear()  # remove all modifiers

    # add to "Export collection"
    # we need it on the viewport to apply modifiers
    #
    bpy.data.collections['Export'].objects.link(obj)

    # get indices of groups
    valid_groups = valid_groups or []
    gpi = [obj.vertex_groups[g].index for g in valid_groups]

    # in Edit mode, select faces
    mesh = obj.data
    bm = bmesh.new()
    bm.from_mesh(mesh)
    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()

    # NOTE: we should be able to select faces from vertices
    # as done in Edit mode in Blender: if we select all vertices
    # of a face, also face is selected.
    bm.select_mode = {'VERT'}
    bm.select_flush_mode()

    # clean selection
    for v in bm.verts:
        v.select_set(False)
        v.hide_set(False)

    # select vertices
    for v in bm.verts:
        v.select = any(gv.group in gpi for gv in mesh.vertices[v.index].groups)

    # select faces
    bm.select_flush(True)

    bm.to_mesh(mesh)
    mesh.update()

    bm.clear()

    # add the modifier
    #
    mod = obj.modifiers.new('submod',type='SUBSURF')
    mod.levels = modLevel

    # evaluate dependency graph
    #
    dg = bpy.context.evaluated_depsgraph_get()
    objeval = obj.evaluated_get(dg)

    # get the evaluated mesh
    #
    mesh = objeval.to_mesh(preserve_all_data_layers=True, depsgraph=dg)

    # remove vertices not in valid_group
    # Instead of selecting only valid vertices, here we
    # adopt the strategy of editing the mesh by removing
    # all faces in the selected groups.  Blender
    # will automatically take care of the topology.
    #
    if len(valid_groups) > 0:

        bm.from_mesh(mesh)
        bm.verts.ensure_lookup_table()
        bm.faces.ensure_lookup_table()

        bmesh.ops.delete(bm, geom=[ f for f in bm.faces if not f.select], context='FACES')

        bm.to_mesh(mesh)
        mesh.update()

        bm.clear()

    #bm.free()

    vv = [ list(v.co.xyz) for v in mesh.vertices ]
    ff = [ list(p.vertices) for p in mesh.polygons ]

    # clean-up
    # Remove command will also unlink from the rest of the
    # objects. Also mesh is removed automatically.
    # 
    objeval.to_mesh_clear()
    bpy.data.meshes.remove(obj.data, do_unlink=True)

    return ff, vv


# HeartBox is an object that should be aligned with the short-axis
# view. By transforming with the inverse of its matrix we put the
# heart in the anatomist's orientation. This is convenient for
# inspection of results in igbrowser, and gepetto relies on the z-axis
# being in the direction of the long axis in order to compute the
# fiber orientation angle alpha correctly.
#
# Transpose only the rotation part of the matrix; otherwise the
# translation part would end up in the 4th column where it could mess
# up the v component of vectors.
#
def heart_export_matrix(obj):
    Mb = heart_matrix()
    Mo = obj.matrix_basis
    M  = Mo.transposed() @ Mb

    return M

# This function returns the rotation matrix of HeartBox. We use the
# convention that MRI and heart coordinates differ only in rotation,
# so the translation part is removed.
#
def heart_matrix():
    bob, users = find_object("HeartBox")
    if users==0:
        Error("HeartBox object needed for heart output")
    
    # .to_3x3() returns a copy
    # NOTE (important!) Matrix indexing changed in Blender 2.62.
    # As explained on the wiki, matrix are trasposed compared to
    # older versions. This is due to the storage in Blender (column-major)
    # and the oddity of using this storage with standard linear algebra
    # operations. Here we adapted the scripts
    #
    # NOTE there are matrix_world, matrix_basis and matrix_local
    # available. 
    # - matrix_basis is the original input matrix
    # - matrix_world is the evaluated matrix with all constraints
    #   and parent matrices applied
    # - matrix_local don't know

    M = bob.matrix_basis.to_quaternion().to_matrix().to_4x4()

    return M

# Some objects we also want to save in MRI orientation. This function
# returns the trivial transform matrix needed for this purpose.
#
def torso_export_matrix(obj):
    R = Matrix.Identity(4)
    Mo = obj.matrix_basis
    M = Mo.transposed() @ R
    return M

# Save the rotation matrix to file for use by configure.py.  It is
# needed there to create a transform matrix for use with the datatex
# texturer, and may also be used to do the rotation afterwards, not on
# the .obj files themselves.
#
# Mb is a 4D matrix such that v*Mb trasforms local to global
# coordinates in Blender. We want a rotation matrix R such that R*v
# translates Blender global coordinates to voxel coordinates, i.e. the
# local coordinates of HeartBox. The change from right to left
# multiplication and the inverse yield two canceling transposes, so we
# just output the upper left 3x3 submatrix as our rotation matrix.
#
def save_matrix():
    bob, users = find_object("HeartBox")
    if users==0:
        Error("HeartBox object needed for matrix output")
    Mb = bob.matrix_basis
    js = dict()
    js['COMMENT'] = "written by mod_torso_editor.py"
    # rotation and translation part
    t,r,_ = Mb.decompose()
    js['rotation'] = [c.to_tuple() for c in r.to_matrix().col]
    js['translation'] = t.to_tuple()
    js['scale'] = bunny/mm
    with open("var_matrix.json", "wt") as fo:
        fo.write(json.dumps(js, indent=2))


# This is for bundles and fiber orientations in the atria. The OBJ
# files written here are read by voxelgen to add bundle tissue or
# modify the type of existing tissues. The var_curves.obj file
# contains the skeletons of the curves and is read by the local
# version of gepetto to set fiber orientations. The var_tubes.obj file
# is not used anywhere.
#
# The tubes have several different types:
#
# Bundle : most RA bundles, such as the Crista Terminalis
#
# Orientation : voxelgen will not add tissue, only change the type of
#    the thin-wall myocardium so that gepetto knows that it should
#    receive a fiber orientation.  CHANGE: in model24-26, the
#    distinction between oriented and non-oriented tissue will be made
#    by gepetto. Orientation tubes will act as both Endo and Epi tubes
#    for the LA.
#
# BB (since version 13) : Bachmann's bundle, (get a higher conductivity
#    than the others) 
#
# EBundle (since version 21) : for bundles that are only
#    filled outside the thin wall. The Bundle type has been modified to
#    be drawn only inside the cavity, and BB will also be drawn only
#    outside, but retains its higher conductivity.
#
# Block : not tissue, used to create a block line for the initiation of
#    spiral waves
#
# Epi_Bundle (since version 26) : for the outer 50% of the LA wall
#    
# Endo_Bundle (since version 26) : for the inner 50% of the LA wall;
#    these two sets of bundles were made by Ali Gharaviri (Maastricht,
#    fall 2014).
#    
def save_curves():
    save_curves_by_type(["Bundle"],      "var_tubes_bundle.obj") # outlines only...
    save_curves_by_type(["Orientation"], "var_tubes_orient.obj")
    save_curves_by_type(["BB"],          "var_tubes_BB.obj", "var_curves_BB.obj")
    save_curves_by_type(["EBundle"],     "var_tubes_ebundle.obj")
    save_curves_by_type(["Bundle", "EBundle", "Orientation", "BB"],
                        "var_tubes.obj", "var_curves.obj")  # outlines and skeletons

    save_curves_by_type(["Bundle"],
                        "var_tubes_embedded.obj", "var_curves_embedded.obj")
    
    save_curves_by_type(["Orientation", "Epi_Bundle"],  "var_tubes_epi.obj",  "var_curves_epi.obj")
    save_curves_by_type(["Orientation", "Endo_Bundle"], "var_tubes_endo.obj", "var_curves_endo.obj")
    
    # save_curves_by_type(["Block"], "var_tubes_%s.obj", separate=True)
    
    # for i in range(1,10):
    #     save_curves_by_type(["PVI_RC{}".format(i)], "var_tubes_pvirc{}.obj".format(i))
    
    # save_curves_by_type(["PVI"],      "var_tubes_pvi.obj")
    # save_curves_by_type(["PVIroof1"], "var_tubes_pviroof1.obj")
    # save_curves_by_type(["PVIroof2"], "var_tubes_pviroof2.obj")
    # save_curves_by_type(["PVIroof3"], "var_tubes_pviroof3.obj")
    # save_curves_by_type(["LAAi"],     "var_tubes_pviroof3.obj")


def save_curves_by_type(mnames, ofs, cfname=None, separate=False):
    #
    # This function saves the "tubes" that define fiber orientation
    # and major bundles in the atria.  It retrieves all the curve
    # objects from the current scene and handles them according to
    # their material type. Curves that define tube centers are
    # provided with a skin from their Bevel object, and exported as
    # meshes. The curves themselves are also exported.
    #
    # Blender 2.9x: we can use the concept of "evaluated" mesh to
    # easily export the curve and tube associated to the spline.
    #

    # gather all objects of CURVE type and with material in mnames
    #
    # The Curve's material determines whether it defines a bundle,
    # indicates a fiber orientation, or nothing at all (curves used
    # for other purposes).
    #
    # NOTE: mnames must be a list for the 'in' operator to do what
    # we intend. If mnames is a string or a 1-element tuple, it
    # will match a substring. An extra list() will convert a tuple
    # to a list BUT does the wrong thing with a string!!
    #
    mnames = list(mnames)
    curves = [ o for o in bpy.data.objects if o.type == 'CURVE' 
            and any(m in o.data.materials for m in mnames) ]
 
    if len(curves) == 0:
        print(f"Skipping curve \"{mnames}\" because it's empty...")

    Ncv = 0    # nr of vertices already exported in var_curves.obj
    Ntv = 0    # nr of vertices already exported in var_tubes.obj
    for c in curves:

        # we copy the object and data because we want to
        # make some changes to bevel. In this way the evaluated
        # mesh will be exactly the tube, whereas the base mesh
        # is just the curve
        #
        obj = c.copy()
        obj.data = c.data.copy()  # we want to modify the mesh
        obj.modifiers.clear()  # remove all modifiers
        obj.data.use_fill_caps = True  # be sure we have caps
        bpy.data.collections['Export'].objects.link(obj)

        # Create file name for output. In case multiple objects are separated, each
        # file is marked with the corresponding object name, according to the
        # format specified by the caller.
        #
        if separate:
            if ofs.count('%') != 1:
                Error("expected exactly one conversion specifier in file name")
            ofname = ofs % c.name
        else:
            ofname = ofs
        oname = str(c.name)
        M = heart_export_matrix(obj)

        # to export the tube, we evaluate the mesh with
        # correct bevel object and fill caps
        #
        dg = bpy.context.evaluated_depsgraph_get()
        objeval = obj.evaluated_get(dg)
        mesh = objeval.to_mesh()

        # skip if no vertices
        if mesh is None:
            print(f"Skipping tube \"{c.name}\" because it's empty...")
            continue

        vv = [ list(scaleout(M,v.co.xyz)) for v in mesh.vertices ]
        ff = [ [i+Ntv for i in p.vertices] for p in mesh.polygons ]

        print(f"Export tube  \"{oname}\" with {len(vv)} vertices")

        amode = not separate and (Ntv > 0)
        listwrite(ofname, ff, vv, name=oname, append=amode)
        Ntv += len(vv)

        objeval.to_mesh_clear()

        # to export the curve, we simply get the temporary
        # mesh and save it
        #
        if cfname:
            # M = c.matrix_world
            #
            spline = c.data.splines[0]

            vv = [ list(scaleout(M,v.co.xyz))  for v in spline.bezier_points ]
            ee = [list([i+1,i+1+1]) for i in range(len(vv)-1)]
            # cmesh = obj.to_mesh()

            # NOTE: we offset the vertex id in faces because we may
            # want to save multiple curves
            # vv = [ list(scaleout(M,v.co.xyz))  for v in cmesh.vertices ]
            # ee = [ [i+Ncv for i in e.vertices] for e in cmesh.edges ]
            # assert len(cmesh.polygons) == 0

            print(f"Export curve \"{oname}\" with {len(vv)} vertices")

            amode = not separate and (Ncv > 0)
            listwrite(cfname, ee, vv, name=oname, append=amode)
            Ncv += len(vv)

            obj.to_mesh_clear()

def scaleout(M, a):
    # This function translates, scales, and rotates the coordinates.
    #
    Ms = Matrix.Scale(bunny/mm, 4)
    b = Vector(a) @ M @ Ms

    return list(b.xyz)

######################################################################################
#
# Loading and saving (body surface) electrodes
#

# def load_elecs(oname, opt):
#     #
#     # The electrode positions come from a text file. A single
#     # OFF file provides the mesh that is used for each of them.
#     # See also save_elecs(). This is a simplified version for
#     # electrode positions without triangle numbers, June 2012.

#     # New, get instructions from a popup block:
#     #
#     params = Struct()
#     gui = Struct()

#     # buttons and default values
#     gui.fname   = Blender.Draw.Create(opt.fname)  # text input
#     gui.prefix  = Blender.Draw.Create(opt.prefix)
#     gui.torso   = Blender.Draw.Create(opt.torso)         # toggle
#     gui.normals = Blender.Draw.Create(opt.normals)
#     gui.project = Blender.Draw.Create(opt.project)
#     gui.layer   = Blender.Draw.Create(opt.layer)

#     # definition of the popup window
#     block = []
#     block.append(("filename: ", gui.fname, 0, 300, "name of the electrode position file"))
#     block.append(("prefix: ", gui.prefix, 0, 12, "prefix for electrode names"))
#     block.append(("torso",   gui.torso, "positions are in torso (MRI) coordinates"))
#     block.append(("normals", gui.normals, "file contains normal orientations"))
#     block.append(("project:", gui.project, "project on torso surface"))
#     block.append(("layer: ", gui.layer, 0, 19, "Blender layer to put the objects in"))

#     # get the parameters
#     retval = Blender.Draw.PupBlock("electrode position input", block)
#     params.bailout = (retval==0)
#     params.fname   = gui.fname.val
#     params.prefix  = gui.prefix.val
#     params.torso   = gui.torso.val==1
#     params.normals = gui.normals.val==1
#     params.project = gui.project==1
#     params.layer   = gui.layer.val

#     if params.bailout: return
#     Blender.Window.WaitCursor(1)  # this may take a few seconds
    
#     fi = open(params.fname, "rt")
#     tm = get_object_mesh('torso')

#     spheres = False

#     # We want to project on the torso mesh as it is output, not on the
#     # original, because the electrodes must fit on the output mesh.
#     # Get the torso mesh, submesh it in the same way as done for
#     # output, and create a new tmesh object with the subsurfed mehs in
#     # it.
#     torso_object = Object.Get("torso")
#     tmesh = torso_object.getData(mesh=True)
#     (ff, vv) = create_submesh(torso_object, None)
#     tmesh = Mesh.New()
#     tmesh.verts.extend(vv)
#     tmesh.faces.extend(ff)
    
#     Mt = torso_object.getMatrix()
    
#     if spheres:
#         dmesh = Mesh.New()
#         dmesh = off.read(dmesh, "dodeka.off")
#         for v in dmesh.verts:
#             for i in range(3): v.co[i] *= 0.007    # adapt size
#         for f in dmesh.faces: f.smooth = 1
#     else:
#         template_object = Object.Get("ElecTemplate")
#         dmesh = template_object.getData(mesh=True)

#     enames = []
#     while 1:
#         s = fi.readline()
#         if not s: break
#         if s[0] not in ('%','#'):
#             try:
#                 fields = s.split()
#                 pos = Vector(float(fields[1]), float(fields[2]), float(fields[3]))/100
#                 ename = fields[0];
#                 if params.normals:
#                     nor = Vector(float(fields[4]), float(fields[5]), float(fields[6]))
#       except ValueError:
#               Error('invalid line: %s' % s)
#       except:
#               Error("unforeseen exception while reading electrodes file")
#           name = params.prefix + ename
#           enames.append(ename)

#           N = len(enames)
#           Blender.Window.DrawProgressBar((N-1.0)/N, "loading electrodes")

#           try:
#               # we need a new mesh for each electrode; if we link a single
#           # mesh to all objects, we cannot easily set their materials
#           # differently
#               #
#           (obj,mesh,isnew) = get_or_create_object_mesh(name)
              
#               Mh = heart_matrix()
#               if not params.torso:
#                   # Positions in the file are in heart coordinates if saved by
#                   # mod_torso_editor. A rotation by Mh puts them in MRI
#                   # coordinates.
#                   pos = pos*Mh
              
#               if spheres:
#                   copy_mesh(mesh, dmesh)
#               else:
#                   obj.modifiers = template_object.modifiers
#                   replace_vertgroups(template_object, obj)     # before copy_mesh!
#                   mesh = Mesh.New()
#                   mesh.getFromObject(template_object,1) # with vertgroups, materials, smoothness, ...
#                   obj.link(mesh)
              
#           if params.prefix=='mtl_':
#               sops(obj, mesh, name, 'elec3', 4, isnew)
#           else:
#                   if spheres:
#                       if ename[0]=='V': sops(obj, mesh, name, 'elec2', params.layer, isnew)
#                       else:             sops(obj, mesh, name, 'elec1', params.layer, isnew)
#                       for f in mesh.faces: f.smooth = 1
#                   else:
#                       S = ScaleMatrix(1, 4)
#                       obj.setMatrix(S)
#                       obj.Layer = 2**(params.layer-1)
                      
#               if not params.normals:
#                   (t,dmin) = find_nearest_face(tmesh, Mt, pos)
#                   f = tmesh.faces[t]
#                   if f.no.length == 0:
#                       print("zero normal in face: ", f.v)
#                   nor = f.no * Mt.rotationPart()
                  
#               if params.project:
#                   (t,dmin) = find_nearest_face(tmesh, Mt, pos)
#                   f = tmesh.faces[t]
#                   pos = project_on_face(pos, Mt, f)
              
#               if spheres:
#                   R = RotationMatrix(0, 4, 'z')
#               else:
#                   z = Vector(0,0,1)
#                   angle = AngleBetweenVecs(z, nor)
#                   axis = z.cross(nor)
#                   R = RotationMatrix(angle, 4, 'r', axis)
              
#               if params.torso:
#                   T = TranslationMatrix(pos)
#               else:
#                   if spheres:
#                       T = TranslationMatrix(pos*100)  # correct for scale matrix set by sops()
#                   else:
#                       T = TranslationMatrix(pos)
#               obj.setMatrix(R*T)
#               obj.setDrawType(3)   # 3 = solid
#           except TedException as ex:
#               Error("problem placing electrode %s: %s" % (name, ex.msg))
#               break
#           except:
#               Error("unforeseen problem placing electrode %s", name)
#               break
      
#     fi.close
#     Blender.Window.WaitCursor(0)
#     Blender.Window.DrawProgressBar(1.0, "loading electrodes")

#     # remember which electrodes we loaded, so we can move them about
#     # and save them later.
#     #
#     pset[oname]['type'] = 'elecset'          # it's an electrode set
#     pset[oname]['enames'] = enames
#     pset[oname]['prefix'] = params.prefix
    
#     print("loaded electrodes: ", enames)


def save_elecs(name):
    #
    # Save an electrode set. The set is no longer hardcoded, names are
    # obtained when loading electrode positions and kept in the pset
    # object, which is stored in the registry. Moreover, there can be
    # multiple sets. The name given to this function identifies the
    # set; information about it comes from the pset array.
    #
    # Only a text file is written. It is used by the activate program
    # (gepetto.web) to produce a list of electrode positions in voxel
    # coordinates to extract data from torso simulations, and a 
    # savelist for use with propag.
    #
    with open("var_%selecs.txt" % pset[name]['prefix'], "w") as fo:
        fo.write("# electrode positions (millimetres) and names\n")
        fo.write("# written by mod_torso_editor.py\n")
        fo.write("# field 1: electrode name\n")
        fo.write("# field 2-4: coordinates (mm)\n")

        for e in pset[name]['enames']:
            #
            # The mesh coordinates are the same for each of the electrode
            # objects; the objects differ only in their transformation
            # matrix. So we save the translation part of the object matrix
            # (modified with the scaleout function to account for rotation
            # and scaling).
            #
            oname = "%s%s" % (pset[name]['prefix'], e)
            obj,users = find_object(oname)
            if users==0:
                Error('object %s does not exist' % oname)
            else:
                M = heart_export_matrix(obj)
                pos = scaleout(M, Vector([0,0,0]))
                fo.write("%s %.3f %.3f %.3f\n" % (e,pos[0], pos[1], pos[2]))
                

        
######################################################################################
#
# Utility functions
#
            
def find_nearest_face(mesh, M, ec):
    #
    # find the face in mesh whose center is nearest to the
    # coordinates ec
    #
    # The "zero normal" warning is sometimes triggered when the mesh
    # is not in a visible layer, or so it appears.
    #
    ddmin = 1e6                      # min distance squared
    t = 0                               # face nr
    nt = -1
    for f in mesh.faces:
        if f.no.length == 0:
            print("warning: face has zero normal")
            continue
        (cx,cy,cz) = (0.0, 0.0, 0.0)
        for v in f.v:
            vec = v.co*M
            cx += vec[0]
            cy += vec[1]
            cz += vec[2]
        fac = 1.0/len(f.v)
        dx = ec[0]-cx*fac
        dy = ec[1]-cy*fac
        dz = ec[2]-cz*fac
        dd = dx*dx + dy*dy + dz*dz  # squared distance to electrode
        if dd<ddmin:
            nt = t           #  nearest face
            ddmin = dd
        t = t+1
    dmin = sqrt(ddmin)
    if nt<0: raise TedException("found no face; dmin = %f" % (dmin))
    print("face %d distance %f" % (nt, dmin))
    return(nt,dmin)

def project_on_face(S, M, f):
    # given an MFace f, project the point specified by Vector S on it
    # along the face's normal.
    P = f.v[0].co * M
    nor = f.no * M.rotationPart()
    nor.normalize()
    Sp = S + (P-S).dot(nor) * nor
    return Sp


def get_face_midpoint(mesh,n):
    #
    # find the center coordinates of the given face
    #   
    f = mesh.faces[n]
    (cx,cy,cz) = (0.0, 0.0, 0.0)
    for v in f.v:
        cx += v.co[0]
        cy += v.co[1]
        cz += v.co[2]
    fac = 1.0/len(f.v)
    return(cx*fac, cy*fac, cz*fac)


def copy_mesh(omesh, imesh):
    #
    # Return a new mesh with the same faces and vertices as the
    # given mesh. Is there a neater way to do this?
    #
    for v in omesh.verts: v.delete()
    for f in omesh.faces: f.delete()
        
    for v in imesh.verts:
        omesh.verts.extend([v.co[:]])
    
    for f in imesh.faces:
        vv = []
        for v in f.verts:
            vv.append(v.index)
        omesh.faces.extend([vv])

    ## omesh = imesh.copy()
    
    return omesh

    
def move_part_to_layer(name, layer):
    
    """put the Part with the specified name in the specified layer.
       For Parts that consist of multiple objects, special treatment
       is done."""

    if pset[name]['type']=="elecset":
        n = move_elecs_to_layer(layer, name)
    else:
        n = set_layer_and_select(name,layer)
    Blender.Redraw()     # needed only once, even if multiple objects
                         # (e.g. the electrodes!) were moved

    if n==1:  print("moved", name, "to layer", layer)
    elif n>1: print("moved %d objects to layer %d" % (n,layer))
    else:  print(name, "has no users")

    
def move_elecs_to_layer(layer, name):

    """puts the electrodes in the specified layer and selects them"""

    print("moving electrode set %s to layer %d" % (name, layer))
    prefix = pset[name]['prefix']
    n = 0
    Blender.Window.WaitCursor(1)
    for e in pset[name]['enames']:
        oname = "%s%s" % (prefix, e)
        n += set_layer_and_select(oname,layer)
    Blender.Window.WaitCursor(0)
    return n


# This function moves an object to a desired layer, usually to hide it from
# view. It makes the object a Selected object when it is moved to a visible
# layer. This can be convenient especially when many objects (such as the
# surface electrodes) are moved.
#
def set_layer_and_select(name,layer):
    (obj,users) = find_object(name)
    if users>0:
        # put it in the desired layer. Layer is a bitmask.
        newlayer = 1<<(layer-1)
        obj.Layer = newlayer
        scn = Scene.GetCurrent()
        if layer in scn.layers:  # if it's an active layer in this scene
            obj.select(True)
        else:
            obj.select(False)   # unselect when invisible
        return 1
    else:
        return 0

# quick version of the above, assumes that the object exists
# and does not change the selection status
#
def set_layer(name, layer):
    obj = Object.Get(name)
    obj.Layer = 1<<(layer-1)
  

# A function to change the viewing direction in a 3D window. This is
# useful some of the useful viewing directions are not available
# directly under Numpad keys in Blender and others are
# non-intuitive. The viewnames array defines the valid inputs to the
# setview() function.
#
view_names = ["front",    "back",
              "left",     "right",
              "bottom",   "top",
              "image"]

view_tips  = ["coronal view (numpad 1)",  "view from back (Shift-numpad 1)",
              "sagittal view (numpad 3)", "view from right (Shift-numpad 3)",
              "axial view (Shift-numpad 7)",    "view from top",
              "view perpendicular to image"]

# This function computes the quaternion to rotate from the default
# view (x axis pointing to the right, y axis pointing up) to a given
# pair of vectors (u pointing to the right, v pointing up). 
#
def setvquat(u, v):
    u.normalize()
    v.normalize()
    if u.cross(v)>1e-10: Error("u and v are not orthogonal")
    # The matrix M' specifies the new axes in terms of the old,
    # which happen to be (1,0,0), (0,1,0), and (0,0,1).
    w = u.cross(v)
    M = Matrix(u,v,w)
    Window.SetViewQuat(M.transpose().toQuat())
    Window.Redraw()

# The view definitions are for MRI (DICOM) coordinates. Luckily, most
# of these correspond to standard Blender views available with numpad
# buttons.  numpad-7 is also a top view, but with the anterior side
# facing down
#
def setview(name):
    X = Vector(1,0,0)
    Y = Vector(0,1,0)
    Z = Vector(0,0,1)
    if   name=="top"   : setvquat(-X,-Y)
    elif name=="bottom": setvquat( X,-Y)
    elif name=="front" : setvquat( X, Z)
    elif name=="back"  : setvquat(-X, Z)
    elif name=="left"  : setvquat( Y, Z)
    elif name=="right" : setvquat(-Y, Z)
    elif name=="image" : view_current_image_orientation()
    else: Error("undefined view name in setview()")

# This function sets the view to look perpendicularly at one of the
# DICOM images that are currently visible, in the orientation
# specified by the DICOM file (which seems to conform to the
# orientation seen in clinical practice and in atlases of sectional
# anatomy).
#
# When multiple images are visible, this function will choose one that
# wasn't visible the last time it was called, and if there is none it
# will choose the one that it chose last. For this purpose it keeps
# track of chosen images, and cycles the list on each invocation.
#
def view_current_image_orientation():
    # Among all visible (state=1) image groups, set the view to the
    # group that was viewed last:
    k = 0      # index for newly visible groups
    Nvis = 0   # Nr of groups now visible
    Ngone = 0  # Nr of groups that went invisible since last time
    new = []   # list of visible and not-yet-viewed groups
    old = []   # list of visible and viewed groups
    unseen = 1e5  # "viewed" code for invisible image
    for n,rec in enumerate(img_groups):
        if rec["state"]==0:
            if rec["viewed"]<unseen: Ngone += 1
            rec["viewed"] = unseen
        else:
            Nvis += 1                 
            if rec["viewed"]==unseen: 
                rec["viewed"] = k   # new, get the lowest available index
                k += 1
                new.append(n)
            else:
                old.append(n)

    for n in old:
        # decrement index for cycling, but also
        # increment it by #new, and decremente it by #gone:
        img_groups[n]["viewed"] += k - 1 - Ngone
        
    for n,rec in enumerate(img_groups):
        if rec["viewed"] <= 0:          # 0 or -Ngone
            rec["viewed"] = Nvis
            du = Vector(rec["du"])
            dv = Vector(rec["dv"])
            setvquat(du, -dv)  # DICOM v axis points down
            break
    else:              # if the loop doesn't break
        Error("no active image group to set the view to")
            
    
#####################################################################################
#
# Functions for MRI/CT data
# 
    
# Importing MRI data, prepared in Matlab with prepare_mri.m
#
# Images come in groups, and groups come in series. A group is a set
# of images within the same series that have the same location and
# orientation. We assume that these constitute a movie. A series is a
# set of images that have the same value in their DICOM
# SeriesDescription field. Typically, there are series for
# localization of the heart, for a set of short-axis images, and for
# movies in 2-chamber, 3-chamber, and 4-chamber views.
#   
# For each series:
#   - add a big button
#   - for each group in the series:
#     - set up an image for each image in the group
#     - add a little button next to the big one
#
# The set_slices_visibility function arranges that only one image in
# each group is in the visible layer (layer 4). This function is also
# called from a script linked to the Scene, which is called on frame
# changes, so that the image can change with the frame number. Movies
# can then be played by quickly changing frames, for example by
# pressing the left or right arrow key; and animation of a movie is
# trivial.
#
# When different series have slices with the same position and
# orientation, prepare_mri.m will place these in a single group. The
# effect is that the corresponding buttons will be pressed and
# released simultaneously. Which slice is visible will depend on the
# frame number.
#
def load_images():

    global Series, Groups, groupmax

    print("Loading images...")
    
    sf = open("mri/sliceconf.json", "r")
    str = sf.read()
    root = json.loads(str)
    slices[:] = root["slices"]       # assign to global variable

    # Count members of groups and series
    #
    Series = dict()
    Groups = dict()
    for n,slice in enumerate(slices):
        name = slice["imname"]
        s = slice["series"]
        g = slice["group"]
        if s in Series:
            Series[s]["count"]+=1
            Series[s]["slices"].append(n)
        else:
            Series[s] = {"count" : 1, "slices" : [n], "groups" : dict()}
            Series[s]["view"] = slice["view"]
        if g not in Series[s]["groups"]:
            Series[s]["groups"][g] = len(Series[s]["groups"]) # value is order of entry
        if g in Groups:
            Groups[g]["Nmembers"] += 1
        else:
            Groups[g] = {"Nmembers" : 1,
                         "view" : slice["view"],
                         "du"   : tuple(slice["du"]),
                         "dv"   : tuple(slice["dv"])}
        slice["frame"] = Groups[g]["Nmembers"]   # frame nr = position within the group

    for n,slice in enumerate(slices):
        g = slice["group"]
        slice["grouped"] = Groups[g]["Nmembers"]>1  # true if part of a group with more than 1 member
        print("slice %d group %d frame %d grouped"%(n, g, slice["frame"]), slice["grouped"])

    # Copy the information from the local dictionaries Series and Groups into global
    # arrays img_series and img_groups, which will be stored in the registry.
    #
    img_series[:] = []
    for n,key in enumerate(Series):
        print("series %d, name=%s, %d members: " % (n, key, Series[key]["count"]))
        gg = list(Series[key]["groups"].keys())
        # sort keys (group nrs) according to values (order in JSON file):
        gs = sorted(gg, key=Series[key]["groups"].__getitem__)
        img_series.append({"index"    : n,
                           "name"     : key,
                           "Nmembers" : Series[key]["count"],
                           "members"  : Series[key]["slices"],
                           "state"    : 0,
                           "view"     : Series[key]["view"],
                           "groups"   : gs})

    groupmax = 0
    Ngroups = len(list(Groups.keys()))
    print("there are %d groups" % Ngroups)
    img_groups[:] = []
    for key in range(0,Ngroups):
        size = Groups[key]["Nmembers"]
        if size>1: print("group %3d: %d members" % (key, size))
        if size>groupmax: groupmax = size
        img_groups.append({"Nmembers" : size,
                           "state"    : 0,
                           "viewed"   : 999,
                           "du"       : Groups[key]["du"],
                           "dv"       : Groups[key]["dv"],
                           "members"  : [],
                           "view"     : Groups[key]["view"]})
    print("largest group has %d members" % groupmax)

    save_reg()    # save img_series in our registry

    Nslices = 0
    for slice in slices:

        Nslices = Nslices + 1
        imname = slice["imname"]
        im = Image.Load(imname)
        name = imname[:-4]     # a more descriptive name would be nice but
                               # object names currently cannot be longer than 20 characters

        v0 = Vector(slice["pos"])
        du = Vector(slice["du"])
        dv = Vector(slice["dv"])
        v1 = v0 + du
        v2 = v0 + du + dv
        v3 = v0 + dv
        f = 1.0

        # Create a new texture.
        # Re-use existing texture slots to keep the list clean.
        tname = "Image-%04d" % (Nslices-1)
        try:
            tex = Texture.Get(tname)
        except:
            tex = Texture.New()
            tex.name = tname
        tex.setType("Image")
        tex.image = im                # image for Rendering

        mat = Material.New()
        mat.setAlpha(1.0)
        mat.name = "ImageMat"
        mat.rgbCol = [0.5, 1.0, 0.9]
        mat.emit = 0.3       # make it glow a bit; also makes sure it's
                             # visible even when I forget to shine a lamp on it

        # assign a Texture object
        mat.setTexture(0, tex, Texture.TexCo.UV, Texture.MapTo.COL)

        # get back an MTex object (maps between mat and texture),
        # and set its properties
        #
        mtex = mat.textures[0]
        mtex.blendmode = Texture.BlendModes.MULTIPLY

        (obj,mesh,isnew) = get_or_create_object_mesh(name,silent=True)

        dz = (0, 0, 0.001)
        vv = [v0,v1,v2,v3, v0, v1, v2, v3]
        ff = [(0,1,2,3), (7,6,5,4)]
        mesh.verts.extend(vv)
        mesh.faces.extend(ff)
        mesh.materials = [mat]
        mesh.addUVLayer("Reset")
        mesh.activeUVLayer = "Reset"
        mesh.renderUVLayer = "Reset"

        # uv affects orientation both in 3D View and in Rendered image
        mesh.faces[0].uv = (vec(0,1), vec(1,1), vec(1,0), vec(0,0))
        mesh.faces[1].uv = (vec(0,0), vec(1,0), vec(1,1), vec(0,1))
        mesh.faces[0].mat = 0
        mesh.faces[1].mat = 0
        mesh.faces[0].image = im        # needed for 3D View
        mesh.faces[1].image = im
        mesh.update()

        obj.drawMode = Object.DrawModes.WIRE        # always give it a wireframe

    set_slices_visibility()       # make view consistent with GUI settings
    Blender.Window.RedrawAll()
    save_slices()
    

# function to put slices in the right layer based on the visibility
# state of their series and, if they are member of a group, on the
# frame number. This function is meant to be called on frame changes
# but can be used for other purposes too. For example, if it's fast
# enough for a frame change, it is also fast enough for EVT_TOGI and
# EVT_TOGG.
#
def set_slices_visibility():
    cf = Blender.Get("curframe")
    for n, slice in enumerate(slices):
        f = slice["frame"]      # frame nr where it belongs
        g = slice["group"]
        name = slice["imname"][:-4]
        obj = Object.Get(name)
        if img_groups[g]["state"]==1 and (slice["grouped"]==False or f==cf):
            #
            # This turns out to be a very fast way to switch an object
            # between visible and invisible states, while at the same
            # time making display fast while most objects are
            # invisible. Layer switching is much slower, and without
            # scaling it to 0 size displaying is slow.
            obj.setSize(1,1,1)
            obj.drawType = Object.DrawTypes.SHADED
        else:
            obj.setSize(0,0,0)
            obj.drawType = Object.DrawTypes.BOUNDBOX
    Blender.Redraw()
            
####################################################################
#
# GUI functions
#

# Events. The event number identifies both the event and the part to
# which it applies. The part is indicated by the event code modulo
# 100; the event is identified by the floored quotient. This applies
# even to events that have no part associated to them such as "exit"
# and "reload".
#
MAXP = 100
EVT_NOP      = 0
EVT_TOGGLE   = 1*MAXP     # set component visibility (move between layers 4 and 5)
EVT_LOAD     = 2*MAXP     # load component data 
EVT_SAVE     = 3*MAXP     # save component data
EVT_EXIT     = 4*MAXP     # exit this GUI
EVT_ADD      = 5*MAXP     # add a model component
EVT_RELOAD   = 6*MAXP     # reload mod_torso_editor
EVT_VIEW     = 8*MAXP     # set viewing orientation
EVT_LOAD_IMG = 9*MAXP     # load imaging data
EVT_TOGI     = 10*MAXP    # set series visibility
EVT_TOGG     = 11*MAXP    # set group visibility

# Function to draw/redraw the user interface.
#
def gui_draw():

    global pset
    dy = 18
    dx = 36
    bx = 8                # base position for first group of buttons
    bx2 = bx + 5*dx       # base position for second group
    bx3 = bx2 + 6*dx      # base position for third group
    by = 8
    BGL.glClear(BGL.GL_COLOR_BUFFER_BIT)

    # draw a row of buttons for each part
    #
    for p,d in pset.items():
        k = d['pos']
        Draw.Toggle(p, EVT_TOGGLE+k, bx+0*dx, by+k*dy, 2*dx, dy-2, d['state'], "")
        Draw.PushButton("load", EVT_LOAD+k, bx+2*dx, by+k*dy, dx, dy-2, "load this part")
        Draw.PushButton("save", EVT_SAVE+k, bx+3*dx, by+k*dy, dx, dy-2, "save this part")

    Draw.PushButton("add",    EVT_ADD,    bx+5*dx, by+0*dy, dx, dy-2, "add part")
    Draw.PushButton("reload", EVT_RELOAD, bx+7*dx, by+0*dy, dx, dy-2, "reload mod_torso_editor")
    Draw.PushButton("exit",   EVT_EXIT,   bx+9*dx, by+0*dy, dx, dy-2, "close wizard")

    
    # Buttons for the viewing direction.
    for k in range(len(view_names)):
        view = view_names[k]
        ttip = view_tips[k]
        Draw.PushButton(view, EVT_VIEW+k, bx3+2*dx*(k%2),
                        by+int(5-math.floor(k/2))*dy, 2*dx, dy-2, ttip)

    # Controls related to the MRI slices.
    #
    Draw.PushButton("load images", EVT_LOAD_IMG, bx2, int(by+5.5*dy), 4*dx, dy-2,
                    "load imaging data")
    for k,rec in enumerate(img_series):
        name = rec["name"]
        if rec["state"]: istate = 1        # need an integer
        else: istate = 0
        if "view" in rec: view = rec["view"]
        else: view = "unknown"
        Draw.Toggle(name, EVT_TOGI+k, bx2, by+(7+k)*dy, 4*dx, dy-2,
                    istate, 
                    "%s view, %d images" % (view, rec["Nmembers"]))
        pos = -1.5
        prev_view = rec["groups"][0]
        for i,g in enumerate(rec["groups"]):
            view = img_groups[g]["view"]
            try:
                N = img_groups[g]["Nmembers"]
                if N==1:
                    txt = "group %d, 1 image, %s" % (g,view)
                else:
                    txt = "group %d, %d images, %s" % (g,N,view)
            except:
                txt = "group %d" % g
            if "state" in img_groups[g]:
                gstate = img_groups[g]["state"]
            else:
                gstate = istate
                img_groups[g]["state"] = gstate
            if view!=prev_view:
                pos += 1.5
                prev_view = view
            else:
                pos += 1
            Draw.Toggle("", EVT_TOGG+g, int(bx2+4*dx+int(dy/2)+pos*dy), int(by+(7+k)*dy), int(dy-1), int(dy-2),
                        int(gstate), txt)

def gui_event(evt, val):            # mouse, key presses and such events
    try:
        if evt==Draw.WINQUIT:
            print("WINQUIT event: saving preferences")
            save_reg()
        elif evt==Draw.WINCLOSE:
            print("WINCLOSE event: saving preferences")
            save_reg()
        else:    
            pass
    except:
        Error("trouble")        # catching the exception avoids exiting the GUI


def gui_bevent(evt):	# button events wrapper to catch leaking exceptions
    global pset
    try:
        do_bevent(evt)
    except:
        Error("trouble")        # avoids exiting the GUI
        
        
def do_bevent(evt):		# button events

    global pset
    
    (e, p) = divmod(evt, MAXP)
    e = e*MAXP
    # print "event = ", e, p
    for pname,dico in pset.items():
        if dico['pos']==p: break
    if e == EVT_TOGGLE:             # toggle component visibility
        if dico['state']==0:
            move_part_to_layer(pname, 4)     # visible layer
            dico['state'] = 1;
        else:
            move_part_to_layer(pname, 5)      # hidden layer
            dico['state'] = 0;
        # as long as we don't have a reliable way to save preferences at exit
        # or when saving a file, do it after every change:
        #
        save_reg()
    elif e == EVT_TOGI:          # toggle image series visibility
        img_series[p]["state"] = 1 - img_series[p]["state"]
        for g in img_series[p]["groups"]:
            img_groups[g]["state"] = img_series[p]["state"]   # sync all groups in this series
        set_slices_visibility()
        save_reg()
    elif e == EVT_TOGG:          # toggle image group visibility
        img_groups[p]["state"] = 1 - img_groups[p]["state"]
        set_slices_visibility()
        save_reg()
    elif e == EVT_LOAD:
        load_brick(pname, dico['stuff'], 4) # name, material, layer
        dico['state'] = 1
    elif e == EVT_SAVE:
        save_brick(pname)
    elif e == EVT_RELOAD:
        importlib.reload(torso_editor.Ted)
    elif e == EVT_EXIT:
        result = Draw.PupMenu("OK?%t|exit wizard")
        if result>0:
            save_reg()
            Draw.Exit()
        return
    elif e == EVT_ADD:
        name = Draw.PupStrInput("name:", "", 80)
        if name:  pset[name] = {'stuff':"caps", 'state':1,
                                'pos':find_empty_slot(pset), 'type':'default'}
        save_reg()
    elif e == EVT_VIEW:
        setview(view_names[p])
    elif e == EVT_LOAD_IMG:
        try:
            Blender.Window.WaitCursor(1)  # load_images() may take seconds
            load_images()
        finally:
            Blender.Window.WaitCursor(0)
    else:
        print("unknown event", e)
        return
    Draw.Redraw()


def find_empty_slot(pset):
    taken = dict()
    for (name,val) in list(pset.items()): taken[val['pos']] = 1
    for i in range(1, len(pset)+2):
        if not i in taken:
            return i
    Error("This can't happen")

    
####################################################################
#
# Functions to handle our registry
#
# Don't use Blender's Registry: it cannot store nested dictionaries
# and it stores settings globally; we would rather have them stored in
# the .blend file. This can be done with Blender Text objects.
#
# We have two registries: one for the settings that change often, and
# another for the slices data, which are big and do not change
# often. This is because the slices data can take several seconds to
# save.
#
def open_reg():
    global regTxt, refs_reg
    try:
        regTxt = bpy.data.texts[regkey_prefs]
    except:
        print("no preferences found in .blend file")
        regTxt = bpy.data.texts.new(regkey_prefs)
    try:
        refs_reg = json.loads(regTxt.as_string())
    except:
        print("registry for preferences was present but empty")
        refs_reg = dict()

def open_slices():
    global sliceTxt, slice_reg
    try:
        sliceTxt = Blender.Text.Get(regkey_slices)
    except:
        print("no slice data found in .blend file")
        sliceTxt = Blender.Text.New(regkey_slices)
    strs = sliceTxt.asLines()
    regs = '\n'.join(strs)
    try:
        slice_reg = json.loads(regs)
    except:
        Warning("registry for slices was present but empty")
        slice_reg = dict()
        

# save to registry
#
def save_reg():
    regy = dict()
    regy['pset_prefs'] = pset.copy()
    regy['series_prefs'] = img_series[:]
    regy['group_prefs'] = img_groups[:]
    regTxt.clear()
    regTxt.write(json.dumps(regy, indent=1))

def save_slices():
    regy = dict()
    regy['slices'] = slices[:]
    regy['groupmax'] = groupmax
    sliceTxt.clear()
    sliceTxt.write(json.dumps(regy, indent=1))
    
# get stuff from registry
#
def load_reg():
    global prefs, img_series, img_groups
    
    try:    prefs = refs_reg['pset_prefs']        # state of the part set
    except: prefs = dict(); print("no pset_prefs found in registry")

    try:    img_series = refs_reg['series_prefs']        # Image view groups
    except: img_series = []; print("no view_prefs found in registry")

    try:    img_groups = refs_reg['group_prefs']  
    except: img_groups = []; print("no group_prefs found in registry")

def load_slices():
    global slices, groupmax
    
    try:    slices = slice_reg['slices']
    except: slices = [];   print("no slices found in registry")

    try:    groupmax = slice_reg['groupmax']
    except: groupmax = 1

    
    
####################################################################
#
# Initialization (main body of the module)
#

print("initializing model24/torso_editor.py")

# things that must exist...
#
groupmax = 1

# names of the Blender Text objects that are used for our registries
#
regkey_prefs  = 'model24_ted_reg';
regkey_slices = 'model24_ted_slices';

# open the registries and load them
open_reg()
# open_slices()
load_reg()
# load_slices()

# set defaults for user preferences
#
pset = dict()
pset['torso'] =    {'stuff':"skin",            'state':1, 'pos':0}
pset['llung'] =    {'stuff':"HIS bundle",      'state':0, 'pos':len(pset)}
pset['rlung'] =    {'stuff':"Purkinje",        'state':0, 'pos':len(pset)}
pset['elecs'] =    {'stuff':"caps",            'state':0, 'pos':len(pset)}
pset['heart'] =    {'stuff':"aorta",           'state':1, 'pos':len(pset)}
pset['eso'] =      {'stuff':"esophagus",       'state':0, 'pos':len(pset)}
pset['act'] =      {'stuff':"aorta",           'state':0, 'pos':len(pset)}
pset['probe'] =    {'stuff':"probe",           'state':0, 'pos':len(pset)}

# replace default settings by preferences, if present
# 
for name in pset:
    if name in prefs:
        pset[name].update(prefs[name])
    else:
        print("default settings used for %s" % name)
pset.update(prefs)

# save_reg();  # do it right now, for backup

# # This is to ensure that the situation corresponds with state toggles.
# # Check that objects exist; parts may be missing.
# #
# for p,d in pset.items():
#     if 'type' not in d: d['type'] = 'default'
#     (obj,users) = find_object(p)
#     if users>0:
#         if d['state']: move_part_to_layer(p, 4)
#         else:          move_part_to_layer(p, 5)

# # set up script to run on frame changes
# #
# scriptname = "model24_scene_script"
# try:
#     txt = Blender.Text.Get(scriptname)
#     txt.clear()
# except NameError:
#     txt = Blender.Text.New(scriptname)
# txt.write("""
# import Blender
# import mod_torso_editor as Ted
# if Blender.Get("curframe")>Ted.groupmax:
#   Blender.Set("curframe", 1)
# else:
#   Ted.set_slices_visibility()
# """)
# scn = Scene.GetCurrent()
# scn.clearScriptLinks()
# scn.addScriptLink(scriptname, "FrameChanged")

# # initialize the user interface
# #
# Draw.Register(gui_draw, gui_event, gui_bevent)
    
