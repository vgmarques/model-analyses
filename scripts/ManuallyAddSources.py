# -*- coding: utf-8 -*-
"""
Created on Fri Aug  6 13:42:34 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os,sys
import numpy as np
import pickle
import pandas as pd

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import quick_visualization as qv
import IgbHandling as igb

def readPSFile(experiment,path):
    PS_DF = pd.read_csv(os.path.join(path,experiment),sep=' ',
                        dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                        header = 1,names=['t','x','y','z','traj'],engine='c',
                        skipinitialspace=True,skiprows=1)
    ## Adjust scale
    scale = 0.2
    PS_DF['x'] *= scale
    PS_DF['y'] *= scale
    PS_DF['z'] *= scale
    ## Get durations of trajectories 
    unTraj = np.unique(PS_DF['traj'])
    #\n",
    trajDurations = np.zeros((len(unTraj),2),dtype=int)
    for i,traj in enumerate(unTraj):
        traj_df = PS_DF.query('traj=='+str(traj))
        uniqueTimes = np.unique(traj_df['t'])
        trajDurations[i,:] = [uniqueTimes.min(), uniqueTimes.max()]
        
    return trajDurations,unTraj

AnatomyPath = 'D:\\vgmar\\Documents\\GitHub\\USI\\model24'
PSPath = 'D:\\vgmar\\model_data\\exp909\\'
PSFile = 'exp909b304_pstracker_th700.txt'
fnameOut = 'exp909b304_pstracker_anchor3_th700.txt'
#%% Manually add "PSs" to dataframe corresponding to the position of the stationary source

## Anatomy file in high res with the position of the anchor
scale = 0.2
FullAnatomy,hdr0 = igb.Load(os.path.join(AnatomyPath,'model24-30-heart-cell-anchor03.igb'))
zz,yy,xx = np.where(FullAnatomy==[230])
circularPatch = np.array([xx,yy,zz]).T 
patchCenter = np.mean(circularPatch,axis=0) # This is the position of the source

## Now load PSs detected by the regular algorithm
PS_DF = pd.read_csv(os.path.join(PSPath,PSFile),sep=' ',
                        dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                        header = 1,names=['t','x','y','z','traj'],engine='c',
                        skipinitialspace=True,skiprows=0)
unTraj = np.unique(PS_DF['traj'])
## And determine the intervals in which the conduction was anchored around source
intervals = [[3300,12000]]

for lims in intervals:
    try:
        tmin = np.nanmax(PS_DF.loc[PS_DF['t']==lims[0],'traj'])
        PossibleTrajs = np.arange(tmin,tmin+100)
        traj = np.min(PossibleTrajs[~np.isin(PossibleTrajs,unTraj)])
    except:
        traj = PS_DF['traj'][0]-1
    print('The trajectory code for the stationary source is %d'%traj)

    time = np.arange(lims[0],lims[1]+1)
    for t in time:
        row = {'t':t,
               'x':patchCenter[0],
               'y':patchCenter[1],
               'z':patchCenter[2],
               'traj':traj}
        PS_DF = PS_DF.append(row,ignore_index = True)

PS_DF = PS_DF.sort_values(by='t')

PS_DF.to_csv(os.path.join(PSPath,fnameOut),sep = ' ',header = True,index = False)


