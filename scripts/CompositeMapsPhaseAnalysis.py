#%% Load libraries
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import svd  
import scipy.io as sio
from scipy.interpolate import griddata
import pandas as pd
import pickle
from copy import deepcopy

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

from detect_peaks import detect_peaks
import IgbHandling as igb
import egm_processing as egmp
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup
import DHHD
from SourceTrackingCalculator import SourceTrackingCalculator


#%% Load anatomy
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'#r'D:\\vgmar\model_data\anatomy\model30Box' #
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm

nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)

#%% Load data

# catheterPath = '/mnt/d/vgmar/model_data/EX0008'
catheterPath = '/mnt/d/vgmar/model_data/composite_mapping/exp906'

# basename = 'EX0008_FB0_FL200s01'
basename = 'exp906c84'


# with open(os.path.join(catheterPath,'%s_Overlap.catG'%basename),'rb') as output:
with open(os.path.join(catheterPath,'%s_CatheterData_Overlap.catG'%basename),'rb') as output:
    CatheterGroup =  pickle.load(output)


# %% For each catheter, perform phase analysis, store the number of PSs

# The phase analysis is conducted stepwise

GlobalPSPositions = np.empty((0,3))
GlobalNumberOfPs = np.empty((0))
AllElectrodes = np.empty((0,3))
AllElectrodeIndices = np.empty((0),int)

lSegment = 230 # ms
startingPoint = 17000 # ms

tStep = 0
for catheterName, CatheterObj in CatheterGroup.Catheters.items():
    print(tStep)
    AllElectrodes = np.vstack([AllElectrodes,CatheterObj.ElectrodeCoordinates])
    AllElectrodeIndices = np.hstack([AllElectrodeIndices,CatheterObj.ElectrodeIndexes])
    
    ShortObj = deepcopy(CatheterObj)
    ShortObj.EGMData = CatheterObj.EGMData[startingPoint+tStep*lSegment:startingPoint+(tStep+1)*lSegment,:]
    ShortObj.GetPhaseSignal(activationMethod='adaptiveThreshold',phaseMethod='sinusoidal')

    STC = SourceTrackingCalculator(1.5*np.pi)
    STC.DoubleRingPSDetection(ShortObj)
    # This is not computationally smart, but works. Should cut the signal, not the results
    NumberOfPS = np.sum(STC.PSLocations,axis=0) 
    tStep += 1

    # Get PS positions in 3D
    PSPositions = np.zeros(shape=(9,3))
    flatNumberOfPS = np.zeros(9)
    k = 0
    for col in range(3):
        for row in range(3):
            linearIndex = row+col*4
            selectedInds = np.array([linearIndex,linearIndex+1,linearIndex+4,linearIndex+4+1])
            PSPositions[k,:] = np.mean(CatheterObj.ElectrodeCoordinates[selectedInds,:],axis=0)
            flatNumberOfPS[k] = NumberOfPS[row,col]
            k += 1
    GlobalPSPositions = np.vstack([GlobalPSPositions,PSPositions])
    GlobalNumberOfPs = np.hstack([GlobalNumberOfPs,flatNumberOfPS])

#%% Interpolation to the positions of the electrodes

ElectrodePS = griddata(GlobalPSPositions, GlobalNumberOfPs, (AllElectrodes[:,0],AllElectrodes[:,1],AllElectrodes[:,2]),
                         method='nearest')



#%% Output
outPath = '/mnt/d/vgmar/model_data/composite_mapping/psdetection_catheters'

sio.savemat(os.path.join(outPath,'%s_PS.mat'%basename),
            {'pspositions':AllElectrodes,'pscount':ElectrodePS})

np.save(os.path.join(outPath,'AllElectrodeIndices.npy'),AllElectrodeIndices)

#%% 
# Interpolation for plotting
prefPath = '/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' # r'D:\\vgmar\model_data\composite_mapping'
interpIndices = np.load(os.path.join(prefPath,'mappedArea.npy')).squeeze()
interpPositions = Anatomy[interpIndices,:]

interpData = griddata(GlobalPSPositions, GlobalNumberOfPs,
                     (interpPositions[:,0],interpPositions[:,1],interpPositions[:,2]),
                      method='nearest')#[:,0]


currentSignal = np.zeros(len(Anatomy),dtype=float)*np.nan

currentSignal[interpIndices] = interpData
# currentSignal[AllElectrodeIndices] = ElectrodePS
# 
currentSignal = currentSignal[rearrangeInds]



AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
                                               smoothFilter=True,cmap='heatmap',
                                                returnData=True)
# AnatomyActor.GetProperty().SetOpacity(0.5)                                          
# psactor,_ = qv.MakePolyDataActor(AllElectrodes,ElectrodePS.reshape(1,-1),cmap = 'heatmap')

qv.QuickRenderWindowInteractor([AnatomyActor],[cbarActor])
                                           
# %%
