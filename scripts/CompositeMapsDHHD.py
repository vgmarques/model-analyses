#%% Load libraries
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import svd  
import scipy.io as sio
from scipy.interpolate import griddata

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

from detect_peaks import detect_peaks
import IgbHandling as igb
import egm_processing as egmp
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup
import DHHD

#%% Load anatomy
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'#r'D:\\vgmar\model_data\anatomy\model30Box' #
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm

nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)

#%% Load vector data and positions
prefPath = '/mnt/d/vgmar/model_data/composite_mapping/results_220408'#'/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' # r'D:\\vgmar\model_data\composite_mapping'

basename = 'Collision'+'Matrix' #'TwoSources.mat'#'SingleReentry.mat'#'Peripheral.mat'#'Focal.mat'
# Peripheral, StableRotor,Meander, Collision, 

ResultsMatrix = sio.loadmat(os.path.join(prefPath,basename+'.mat'))['matrix']
Positions = ResultsMatrix[:,:3]
Vectors =  ResultsMatrix[:,4:7]
# Positions = sio.loadmat(os.path.join(prefPath,'positions%s.mat'%basename))['positions']
# Vectors = sio.loadmat(os.path.join(prefPath,'vectors%s.mat'%basename))['vectors']
# CVavg = sio.loadmat(os.path.join(prefPath,'velocities'+basename))['velocities']


#%% Remove points that are not in the anatomy
anatomyIndices = list()
keepIndices = list()

for i,pos in enumerate(Positions):
    pos = pos.astype(int)
    index = np.where((np.abs(Anatomy[:,0]-pos[0])<1e-5) *
                                 (np.abs(Anatomy[:,1]-pos[1])<1e-5) *
                                 (np.abs(Anatomy[:,2]-pos[2])<1e-5) )[0]
    if len(index)!=0:  
        anatomyIndices.append(index)
        keepIndices.append(i)

Positions = Positions[keepIndices]
Vectors = Vectors[keepIndices]
# CVavg = CVavg[keepIndices]
# CVabs = np.linalg.norm(CVavg,axis=1)

#%% Project into 2D 
U,s,V = svd(Positions)
u = V.T[:,0] 
v = V.T[:,1] 
n = V.T[:,2]   
Rn =  egmp.MakeRotationalMatrix(n,np.array([0,0,1])) #  Create rotation matrix to align n with z axis

Vectors2D = np.matmul(Rn,Vectors.T).T
Vectors2D = Vectors2D[:,:2]
Positions2D = np.matmul(Rn,Positions.T).T 
Positions2D = Positions2D[:,:2]

#%% Decompose

NodeInfo = DHHD.GetNodeInfo(Positions2D)
S = DHHD.BuildS(NodeInfo)
Xi = DHHD.Build_Xi(NodeInfo,Vectors2D)
B,C = DHHD.BuildRHS(NodeInfo,Xi)
D,R = DHHD.SolveSystem(B,C,S,NodeInfo)

#%% Recover "lost" points and save
ResultsMatrix[:,-2] = griddata(Positions, D, (ResultsMatrix[:,0],
                                ResultsMatrix[:,1],
                                ResultsMatrix[:,2]), method='nearest')[:,0]

ResultsMatrix[:,-1] = griddata(Positions, R, (ResultsMatrix[:,0],
                                ResultsMatrix[:,1],
                                ResultsMatrix[:,2]), method='nearest')[:,0]

outPath = '/mnt/d/vgmar/model_data/composite_mapping/results_220408' 


sio.savemat(os.path.join(outPath,'%s_CompositeMapsDHHD.mat'%basename),
            {'matrix':ResultsMatrix})
# sio.savemat(os.path.join(outPath,'%s_CompositeMapsDHHD.mat'%basename),{'divergence':D,'curl':R,'positions':Positions})
#%% Separate into components

GradD = np.zeros(Positions2D.shape)
GradR = np.zeros(Positions2D.shape)
# NTri = node_info['NTri']
NNodes = NodeInfo['NPoint']

for i,pos in enumerate(Positions2D):
    
    nodes = NNodes[i][NNodes[i]!=i]

    Distances = np.linalg.norm(Positions2D[i]-Positions2D[nodes],axis=1)

    Angles = np.arctan2(Positions2D[nodes,1]-Positions2D[i,1],
                        Positions2D[nodes,0]-Positions2D[i,0])
    
    Ddf = D[nodes].T-D[i]

    GradD[i,0] = np.sum(Ddf*np.cos(Angles)*Distances**2)*1/np.sum(Distances**2)
    GradD[i,1] = np.sum(Ddf*np.sin(Angles)*Distances**2)*1/np.sum(Distances**2)

    Rdf = R[nodes].T-R[i]
    GradR[i,0] = np.sum(Rdf*np.cos(Angles)*Distances**2)*1/np.sum(Distances**2)
    GradR[i,1] = np.sum(Rdf*np.sin(Angles)*Distances**2)*1/np.sum(Distances**2)

GradR = np.array([-GradR[:,1],GradR[:,0]]).T
GradH = Vectors2D-GradD-GradR

#%% Find the peaks of rotational and divergent activity

peakIndicesRotation = detect_peaks(np.abs(R).flatten())
peakMagnitudeRotation = np.abs(R)[peakIndicesRotation]

peakMaxRotation = np.where(peakMagnitudeRotation==np.max(peakMagnitudeRotation))[0][0]
peakMaxRotation = peakIndicesRotation[peakMaxRotation]

print('Position with highest rotational component:', Positions[peakMaxRotation])

peakIndicesDivergence = detect_peaks(np.abs(D).flatten())
peakMagnitudeDivergence = np.abs(D)[peakIndicesDivergence]

peakMaxDivergence = np.where(peakMagnitudeDivergence==np.max(peakMagnitudeDivergence))[0][0]
peakMaxDivergence = peakIndicesDivergence[peakMaxDivergence]

print('Position with highest divergence component:', Positions[peakMaxDivergence])


#%% Project back into 3D
# Vectors2D = np.hstack([Vectors2D,np.zeros((len(Vectors2D),1))])
# Positions2D = np.hstack([Positions2D,np.zeros((len(Positions2D),1))])

GradD3 = np.hstack([GradD,np.zeros((len(GradD),1))])
GradR3 = np.hstack([GradR,np.zeros((len(GradR),1))])
GradH3 = np.hstack([GradH,np.zeros((len(GradH),1))])

GradD3 = np.matmul(np.linalg.inv(Rn),GradD3.T).T 
GradR3 = np.matmul(np.linalg.inv(Rn),GradR3.T).T 
GradH3 = np.matmul(np.linalg.inv(Rn),GradH3.T).T 


#%% Interpolate data for better plot

xmin,xmax = Anatomy[anatomyIndices,0].min(),Anatomy[anatomyIndices,0].max()
ymin,ymax = Anatomy[anatomyIndices,1].min(),Anatomy[anatomyIndices,1].max()
zmin,zmax = Anatomy[anatomyIndices,2].min(),Anatomy[anatomyIndices,2].max()

interpIndices = np.where(((Anatomy[:,0]>=xmin) & (Anatomy[:,0]<xmax)) &
                         ((Anatomy[:,1]>=ymin) & (Anatomy[:,1]<ymax)) &
                         ((Anatomy[:,2]>=zmin) & (Anatomy[:,2]<zmax)))

np.save(os.path.join(prefPath,'mappedArea.npy'),interpIndices)

interpPositions = Anatomy[interpIndices]

InterpolatedDivergence = griddata(Positions, D, (interpPositions[:,0],interpPositions[:,1],interpPositions[:,2]), method='nearest')[:,0]
InterpolatedRotation = griddata(Positions, R, (interpPositions[:,0],interpPositions[:,1],interpPositions[:,2]), method='nearest')[:,0]


#%%

Mx,Mn = np.max([R,D]),np.min([R,D])
Rnorm = (R-Mn)/(Mx-Mn)
Dnorm = (D-Mn)/(Mx-Mn)



currentSignal = np.zeros(len(Anatomy),dtype=float)*np.nan
# currentSignal[anatomyIndices] = np.abs(D)
# currentSignal[interpIndices] = InterpolatedDivergence
currentSignal[interpIndices] = np.abs(InterpolatedRotation)
currentSignal = currentSignal[rearrangeInds]



AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
                                               smoothFilter=True,cmap='viridis',vmax =10, vmin=0,
                                                returnData=True)

PeakPosition = qv.MakeGlyphActor(Positions[[peakMaxRotation,peakMaxDivergence],:].reshape(2,-1),
                                np.zeros((2,3)),magnitudes = None,
                                vmin=0,vmax=1,cmap='jet',source='sphere')
# # PreviousDirectionActor = qv.MakeGlyphActor(Positions,GradR3,magnitudes = Rnorm,
#                                                    vmin=0,vmax=1,cmap='jet',mode='scale')
# qv.QuickRenderWindowInteractor([AnatomyActor,PeakPosition],[cbarActor])

#%% Formal figure
from StandardVTKObjects import *
renderer = StandardViewRenderer([AnatomyActor],
                                cameraType='egmPosterior',
                                colorbarActor = cbarActor,
                                textActor = None,
                                viewportSplit = None)
PosWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
                        OffScreenRendering = False)  
PosWin.Render()

renderer = StandardViewRenderer([AnatomyActor],
                                cameraType='egmAnterior',
                                colorbarActor = cbarActor,
                                textActor = None,
                                viewportSplit = None)
AntWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
                        OffScreenRendering = False)
AntWin.Render()   

qv.QuickRenderWindowInteractor([AnatomyActor,PeakPosition],[cbarActor])

#%%

fig = plt.figure(figsize = (18,9))
ax1 = plt.subplot(1,2,1,projection='3d')
ax1.plot_trisurf(Positions2D[:,0],Positions2D[:,1], D.flatten(),triangles = NodeInfo['triangles'],cmap='viridis', edgecolor='none')
ax1.set_xlabel('Position (mm)',fontsize = 20)
ax1.set_ylabel('Position (mm)',fontsize = 20)
ax1.set_zlabel('Curl-free component',fontsize = 20)
ax1.tick_params(labelsize=12)

ax2 = plt.subplot(1,2,2,projection='3d')
ax2.plot_trisurf(Positions2D[:,0],Positions2D[:,1], np.abs(R).flatten(),triangles = NodeInfo['triangles'],cmap='viridis', edgecolor='none')
ax2.set_xlabel('Position (mm)',fontsize = 20)
ax2.set_ylabel('Position (mm)',fontsize = 20)
ax2.set_zlabel('Divergence-free component',fontsize = 20)
ax2.tick_params(labelsize=12)


