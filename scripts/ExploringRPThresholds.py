# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 10:06:49 2021

DESCRIPTION: Exploration of thresholds for distance matrices

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle5 as pickle
import scipy.signal as sig
from copy import deepcopy
import time
from scipy.linalg import svd  

from scipy.fft import fft2
from scipy.spatial.distance import pdist,squareform



upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))
import quick_visualization as qv
from RecurrenceComputation import RecurrenceComputation


#%%

# PSPATH = 'D:\\vgmar\\model_data\\exp909'#'6\\pstracker\\Control\\Filtered' #'D:\\vgmar\\model_data\\exp909'# 
PSPATH = 'D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered' #'D:\\vgmar\\model_data\\exp909'# 
# CATPATH='D:\\vgmar\\model_data\\exp909'#'6\\electrode_data\\Control'
CATPATH='D:\\vgmar\\model_data\\exp906\\electrode_data\\Control'
# RECPATH='D:\\vgmar\\model_data\\exp909'#'6\\recurrence\\Control'
RECPATH='D:\\vgmar\\model_data\\exp906\\recurrence\\Control'
SampleShift = 5
fs = 1000
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 2

experimentNames = []
for file in os.listdir(PSPATH):
    # if file.startswith('exp909b') and file.endswith('anchor3_th100.txt'):
    if file.startswith('exp906c') and file.endswith('.txt'):
        experimentNames.append(file.split('_')[0])
experimentNames = np.sort(experimentNames)
#%%
Thresholds = np.linspace(0,0.5,11) #np.array([0.05,0.1,0.15,0.25,0.3])#
RRs = np.zeros((len(Thresholds),16))
for experiment in experimentNames[10:11]:
    print(experiment)
    ## Calculate global distance matrix for catheter
    with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
        DistanceMatrices =  pickle.load(output)
        EstimatedAFCLs =  pickle.load(output)
        
    # Iterate over catheters
    for cId,cat in enumerate(DistanceMatrices.keys()):
        print(cat)
        estimatedAFCL = EstimatedAFCLs[cat]
        DM = DistanceMatrices[cat]
        
        for i,th in enumerate(Thresholds):
            RP, _, recurrenceThreshold, _ = \
                    recComp.ComputeRecurrencePlot(DM, fs, estimatedAFCL,recurrenceThreshold= th)
            # RP = recComp.ErodeRP(RP)
         
            
            RRs[i,cId] = (np.sum(RP)-RP.shape[0])/np.prod(RP.shape) # Main diagonal subtracted
#%%                
    
fig,ax = plt.subplots(1)
ax.plot(Thresholds,RRs[:,:10],'.-',markersize=10)
ax.plot(Thresholds,RRs[:,10:],'*-',markersize=10)
# ax.plot(Thresholds[1:],np.log(RRs[1:]),'.-')
ax.set_xlabel('Threshold',fontsize = 18)
ax.set_ylabel('Recurrence rate',fontsize = 18)
ax.tick_params(labelsize=16)
ax.set_title('%s'%(experiment))
ax.legend(list(DistanceMatrices.keys()),fontsize=18)
ax.set_xlim([0,0.5])
ax.set_ylim([0,0.5])
fig.tight_layout()

fig,ax = plt.subplots(1)
ax.plot(Thresholds[1:],np.diff(RRs[:,:10],axis=0),'.-',markersize=10)
ax.plot(Thresholds[1:],np.diff(RRs[:,10:],axis=0),'*-',markersize=10)
ax.set_xlabel('Threshold',fontsize = 18)
ax.set_ylabel('Recurrence rate diff',fontsize = 18)
ax.tick_params(labelsize=16)
ax.set_title('%s'%(experiment))
ax.legend(list(DistanceMatrices.keys()))
ax.set_xlim([0,0.5])
fig.tight_layout()

# At 0.5 threshold, essentially everyone has the "full" recurrences

# At 0.25 there seems to be the largest difference between RRs
# This value is different for each simulation

# The sum of all RRs until 0.5 is proportional to the average value of the DM
# (minus the mean of 0.5)

# This proportion is in a ratio of ~26 (RR/avg. DM)

# Conclusion: I would use three or four different thresholds in the range between
# 0.05 and 0.3. More than that makes the RPs increasingly similar

# th = [0.05,  0.15, 0.25] zB

#%%
th=0.02
test = []
for cId,cat in enumerate(DistanceMatrices.keys()):
    estimatedAFCL = EstimatedAFCLs[cat]
    DM = DistanceMatrices[cat]
    test.append(np.mean(np.abs(DM[~np.isnan(DM)]-0.5)))
    RP, _, recurrenceThreshold, _ = \
                    recComp.ComputeRecurrencePlot(DM, fs, estimatedAFCL,recurrenceThreshold= th)
    # RP = recComp.ErodeRP(RP,DM)
    fig,ax = plt.subplots(1)
    ax.imshow(RP,cmap = 'gray_r',origin='lower')#,extent=[0,12000,0,12000])
    ax.set_title(cat)
    # ax.set_xlim([3300,12000])
    # ax.set_ylim([3300,12000])


