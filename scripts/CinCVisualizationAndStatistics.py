# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 16:01:40 2021

DESCRIPTION: Visualization of CinC 2021 results

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Import libraries and make convenient functions

import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from scipy import stats

import multiprocessing as mp
from itertools import repeat
try:
    import pickle5 as pickle
except:
    import pickle
    
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'psdetection'))
sys.path.append(os.path.join(upperDir,'recurrence'))
import pslibrary as psl
import quick_visualization as qv
from RecurrenceComputation import RecurrenceComputation

fs = 1000
tBegin = 2500
tEnd = 15000
feature = 'recurrence_rate'
# %% Load Results from analysis
PSPATH =    '/mnt/d/vgmar/model_data/exp906/CinC/ps'
RECPATH =   '/mnt/d/vgmar/model_data/exp906/CinC/recurrence'
SAVEPATH =  '/mnt/d/vgmar/model_data/exp906/CinC/'
FILENAME =  'PSPresenceAndRQA_16Positions_th100.pkl'


with open(os.path.join(PSPATH,FILENAME),'rb') as output:
    PSDict_In = pickle.load(output)
    PSDict_Out = pickle.load(output)
    GlobalRR_In = pickle.load(output)
    GlobalRR_Out = pickle.load(output)
    WholeRQA = pickle.load(output)
    WholePercentages = pickle.load(output)
    
experimentNames  = np.array(list(GlobalRR_In.keys()))

Inds0 = np.arange(7)
Inds70 = np.arange(7,len(experimentNames))

#%% Adjust data to have information of all simulations
RR0_In = list()
RR0_Out = list()
N0_In = list()
N0_Out = list()

for EXP,experiment in enumerate(experimentNames[Inds0]):
    RRI = GlobalRR_In[experiment]
    RRO = GlobalRR_Out[experiment]
    tmp_ni = 0
    tmp_no = 0
    for cId, cat in enumerate(RRI.keys()):
        [RR0_In.append(RRI[cat][key][feature]) for key in RRI[cat]]
        [RR0_Out.append(RRO[cat][key][feature]) for key in RRO[cat]]
        tmp_ni += len(RRI[cat])
        tmp_no += len(RRO[cat])
    N0_In.append(tmp_ni)
    N0_Out.append(tmp_no)


RR0_In = np.asarray(RR0_In)
RR0_Out = np.asarray(RR0_Out)


RR70_In = list()
RR70_Out = list()
N70_In = list()
N70_Out = list()
for EXP,experiment in enumerate(experimentNames[Inds70]):
    RRI = GlobalRR_In[experiment]
    RRO = GlobalRR_Out[experiment]
    tmp_ni = 0
    tmp_no = 0
    for cId, cat in enumerate(RRI.keys()):
        [RR70_In.append(RRI[cat][key][feature]) for key in RRI[cat]]
        [RR70_Out.append(RRO[cat][key][feature]) for key in RRO[cat]]
        tmp_ni += len(RRI[cat])
        tmp_no += len(RRO[cat])
    N70_In.append(tmp_ni)
    N70_Out.append(tmp_no)

RR70_In = np.asarray(RR70_In)
RR70_Out = np.asarray(RR70_Out)


#%% Fig A - Visualization of boxplots for each simulation individually
ticks = list()
Nin = list()
Nout = list()
fig,ax  = plt.subplots(1,figsize=(16,8))

selectedExps = experimentNames[Inds0]
for EXP,experiment in enumerate(selectedExps):
    RRI = GlobalRR_In[experiment]
    tmpRRI = list()
    RRO = GlobalRR_Out[experiment]
    tmpRRO = list()
    #
    for cat in RRI.keys():
        [tmpRRI.append(RRI[cat][key][feature]) for key in RRI[cat]]
        [tmpRRO.append(RRO[cat][key][feature]) for key in RRO[cat]]
    #
    RRI = tmpRRI
    RRO = tmpRRO
    #
    Nin.append(len(RRI))
    Nout.append(len(RRO))
    #
    ax.boxplot([RRI],positions = [EXP-0.25],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='lightgreen', edgecolor='k'),
           medianprops = dict(color='k'))
     #   
    ax.boxplot([RRO],positions = [EXP+0.25],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='pink', edgecolor='k'),
           medianprops = dict(color='k'))
    #    
    ticks.append(EXP)


ticks = np.asarray(ticks)
for i in range(len(ticks)):
    ax.text(ticks[i]-.25,0,'n=%d'%Nin[i],color = 'k',fontsize = 12,ha='center')
    ax.text(ticks[i]+.25,0,'n=%d'%Nout[i],color = 'k',fontsize = 12,ha='center')

p1 = Rectangle((0.5, 0.5), 0.25,0.25, facecolor="lightgreen",
                    edgecolor="k", linewidth=1)
p2 = Rectangle((0.5, 0.5), 0.25,0.25, facecolor="pink",
                    edgecolor="k", linewidth=1)

ax.set_ylim([-0.01,1.2])
ax.set_xticks(ticks)
ax.tick_params(labelsize=16)
ax.set_xticklabels(selectedExps,rotation=45) #
ax.set_ylabel('Recurrence rate per AFCL',fontsize=16)
fig.legend([p1,p2],['PS Blocks','Non-PS Blocks'],fontsize=14)
fig.tight_layout()
fig.show()

#%% Fig. B - All data for each fibrosis group
fig,ax = plt.subplots(1,2,figsize=[10.71,  5.62])

ax[0].boxplot([RR0_In],positions = [-0.4],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='w', edgecolor='k'),
           medianprops = dict(color='k'))

ax[0].boxplot([RR0_Out],positions = [0.4],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='lightgray', edgecolor='k'),
           medianprops = dict(color='k'))

ax[0].set_ylabel('RR per AFCL', fontsize=20)
ax[0].set_ylim([0,1.2])
ax[0].set_xticklabels(['SU', 'SN'])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].spines['left'].set_linewidth(2)
ax[0].spines['bottom'].set_linewidth(2)
ax[0].tick_params(labelsize=20)
ax[0].set_yticks(np.arange(0,1.1,0.2))
ax[0].set_title('A) No fibrosis',fontsize=24)
ax[0].grid(True,axis='y')

ax[1].boxplot([RR70_In],positions = [-0.4],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='w', edgecolor='k'),
           medianprops = dict(color='k'))

ax[1].boxplot([RR70_Out],positions = [0.4],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='lightgray', edgecolor='k'),
           medianprops = dict(color='k'))

ax[1].set_ylabel('RR per AFCL', fontsize=20)
ax[1].set_ylim([0,1.2])
ax[1].set_xticklabels(['SU', 'SN'])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['left'].set_linewidth(2)
ax[1].spines['bottom'].set_linewidth(2)
ax[1].tick_params(labelsize=20)
ax[1].set_yticks(np.arange(0,1.1,0.2))
ax[1].set_title('B) Severe fibrosis',fontsize=24)
ax[1].grid(True,axis='y')

fig.tight_layout()
fig.show()

#%% Fic C - Recurrence plots with the intervals
lims = [9000,15000] #Select time interval

# Recurrence analysis because I just save the distance matrices
SampleShift = 5
recComp = RecurrenceComputation()
recComp.SampleShift = SampleShift
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1 # Only used if no threshold is passed

EXP = 13; experiment = experimentNames[EXP] # Choose experiment 

with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
    DistanceMatrices =  pickle.load(output)
    EstimatedAFCLs =  pickle.load(output)

meanAFCL = np.mean([EstimatedAFCLs[key] for key in EstimatedAFCLs])

cId =2; cat = list(DistanceMatrices.keys())[cId] # Choose catheter
afclLineLenght =int(EstimatedAFCLs[cat]*1000//SampleShift)

RP, _, recurrenceThreshold, _ = \
        recComp.ComputeRecurrencePlot(DistanceMatrices[cat], fs,
                              EstimatedAFCLs[cat],recurrenceThreshold= 0.15) # check threshold

RP = RP[tBegin//SampleShift:tEnd//SampleShift,:][:,tBegin//SampleShift:tEnd//SampleShift]


# Side note: rqa for the plotted electrode
RP_er = recComp.ErodeRP(RP,DistanceMatrices[cat])
rqa = recComp.ComputeRQA(RP_er,EstimatedAFCLs[cat],fs,minDiagonalLine = afclLineLenght)
rqa['recurrence_rate']
rqa['median_diagonal']

# Fig
fig,ax = plt.subplots(1,figsize=(11,9.6))
xi,yi = np.where(RP)
timeAxes = np.linspace(tBegin,tEnd,RP.shape[0])
ax.scatter(timeAxes[xi],timeAxes[yi],s=0.01,color='k')
    
# blockUpDil,blockDownDil =  psl.GetEdges(PSDict_In[experiment][:,cId])
# for bId in range(len(blockUpDil)):
#     if blockDownDil[bId]-blockUpDil[bId]<=5*meanAFCL*1000//SampleShift: continue
#     ax.add_patch(Rectangle((timeAxes[blockUpDil[bId]], timeAxes[blockUpDil[bId]]),
#                             timeAxes[blockDownDil[bId]]-timeAxes[blockUpDil[bId]],timeAxes [blockDownDil[bId]]-timeAxes[blockUpDil[bId]],
#                             fc='None',ec='g',linewidth=3))                

# blockUp,blockDown =  psl.GetEdges(PSDict_Out[experiment][:,cId])
# for bId in range(len(blockUp)):
#     if blockDown[bId]-blockUp[bId]<=5*meanAFCL*1000//SampleShift: continue
#     ax.add_patch(Rectangle((timeAxes[blockUp[bId]],timeAxes[ blockUp[bId]]),
#                             timeAxes[blockDown[bId]]-timeAxes[blockUp[bId]], timeAxes[blockDown[bId]]-timeAxes[blockUp[bId]],
#                             fc='None',ec='r',linewidth=3))

ax.set_xticks(np.arange(lims[0],lims[1]+1,1000))
ax.set_xlim(lims)
ax.set_yticks(np.arange(lims[0],lims[1]+1,1000))
ax.set_ylim(lims)
ax.set_ylabel('Time (ms)',fontsize = 18)
ax.set_xlabel('Time (ms)',fontsize = 18)
ax.tick_params(labelsize=16)
fig.suptitle(cat,fontsize=20)
fig.tight_layout()
fig.show()



#%% Fig D - Same as figure A for diagonal lines 142381 192938
diagonal_feature = 'longest_diagonal'
SampleShift=5
ticks = list()
Nin = list()
Nout = list()
fig,ax  = plt.subplots(1,figsize=(16,8))

selectedExps = experimentNames[Inds0]
for EXP,experiment in enumerate(selectedExps):
    with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
            DistanceMatrices =  pickle.load(output)
            EstimatedAFCLs =  pickle.load(output)
    meanAFCL = np.mean([EstimatedAFCLs[key] for key in EstimatedAFCLs])
    #
    RRI = GlobalRR_In[experiment]
    tmpRRI = list()
    RRO = GlobalRR_Out[experiment]
    tmpRRO = list()
    #
    for cat in RRI.keys():
        [tmpRRI.append(RRI[cat][key][diagonal_feature]/(RRI[cat][key]['NAFCL']*(meanAFCL*1000)/SampleShift)*100) for key in RRI[cat]]
        [tmpRRO.append(RRO[cat][key][diagonal_feature]/(RRO[cat][key]['NAFCL']*(meanAFCL*1000)/SampleShift)*100) for key in RRO[cat]]
    #
    RRI = tmpRRI
    RRO = tmpRRO
    #
    Nin.append(len(RRI))
    Nout.append(len(RRO))
    #
    ax.boxplot([RRI],positions = [EXP-0.25],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='lightgreen', edgecolor='k'),
           medianprops = dict(color='k'))
     #   
    ax.boxplot([RRO],positions = [EXP+0.25],widths= [0.2],patch_artist = True,
           boxprops = dict(linestyle='-', linewidth=0.6, facecolor='pink', edgecolor='k'),
           medianprops = dict(color='k'))
    #    
    ticks.append(EXP)


ticks = np.asarray(ticks)
for i in range(len(ticks)):
    ax.text(ticks[i]-.25,0,'n=%d'%Nin[i],color = 'k',fontsize = 12,ha='center')
    ax.text(ticks[i]+.25,0,'n=%d'%Nout[i],color = 'k',fontsize = 12,ha='center')

p1 = Rectangle((0.5, 0.5), 0.25,0.25, facecolor="lightgreen",
                    edgecolor="k", linewidth=1)
p2 = Rectangle((0.5, 0.5), 0.25,0.25, facecolor="pink",
                    edgecolor="k", linewidth=1)

# ax.set_ylim([-0.01,1.2])
ax.set_xticks(ticks)
ax.tick_params(labelsize=16)
ax.set_xticklabels(selectedExps,rotation=45) #
ax.set_ylabel('Longest diagonal (% of block)',fontsize=16)
fig.legend([p1,p2],['SU','SN'],fontsize=14)
fig.tight_layout()
fig.show()

#%% ################### Aditional features ###################

# Duration of the blocks
Durations_In = list()
Durations_Out = list()
selectedExps = np.array(list(PSDict_In.keys()))[Inds0]
for experiment in selectedExps:
    for cId in range(PSDict_In[experiment].shape[1]):
        blockUp,blockDown = psl.GetEdges(PSDict_In[experiment][:,cId])
        dur = (blockDown-blockUp)*SampleShift
        for element in dur: Durations_In.append(element)
    for cId in range(PSDict_Out[experiment].shape[1]):
        blockUp,blockDown = psl.GetEdges(PSDict_Out[experiment][:,cId])
        dur = (blockDown-blockUp)*SampleShift
        for element in dur: Durations_Out.append(element)

np.mean(Durations_In)/1000
np.std(Durations_In)/1000
np.mean(Durations_Out)/1000
np.std(Durations_Out)/1000


#%% ################### Statistics ###################

# SU and SN inside fibrosis groups

## Description
np.median(RR0_In)
print('[%0.2f;%0.2f]'%(np.percentile(RR0_In,25),np.percentile(RR0_In,75)))

np.median(RR0_Out)
print('[%0.2f;%0.2f]'%(np.percentile(RR0_Out,25),np.percentile(RR0_Out,75)))

np.median(RR70_In)
print('[%0.2f;%0.2f]'%(np.percentile(RR70_In,25),np.percentile(RR70_In,75)))

np.median(RR70_Out)
print('[%0.2f;%0.2f]'%(np.percentile(RR70_Out,25),np.percentile(RR70_Out,75)))

## Normality
W,p_values = stats.shapiro(RR0_In) # very much not normal
print(p_values)
W,p_values = stats.shapiro(RR0_Out) # very much not normal
print(p_values)
W,p_values = stats.shapiro(RR70_In) # very much not normal
print(p_values)
W,p_values = stats.shapiro(RR70_Out) # very much not normal
print(p_values)

## Tests
WilcoxonStat, p_value = stats.mannwhitneyu(RR0_In,RR0_Out)
print(p_values)
WilcoxonStat, p_value = stats.mannwhitneyu(RR70_In,RR70_Out)
print(p_values)

# SU and SN between fibrosis groups
## Tests
WilcoxonStat, p_value = stats.mannwhitneyu(RR0_In,RR70_In)
print(p_values)
WilcoxonStat, p_value = stats.mannwhitneyu(RR0_Out,RR70_Out)
print(p_values)

# SU and SN within experiments

RRI_All = list()
RRO_All = list()

selectedExps = experimentNames
for EXP,experiment in enumerate(selectedExps):
    RRI = GlobalRR_In[experiment]
    tmpRRI = list()
    RRO = GlobalRR_Out[experiment]
    tmpRRO = list()
    #
    for cat in RRI.keys():
        [tmpRRI.append(RRI[cat][key][feature]) for key in RRI[cat]]
        [tmpRRO.append(RRO[cat][key][feature]) for key in RRO[cat]]
    #
    RRI_All.append(tmpRRI)
    RRO_All.append(tmpRRO)


pvals = list()
for i in range(len(RRI_All)):
    WilcoxonStat, p_value = stats.mannwhitneyu(RRI_All[i],RRO_All[i])
    pvals.append(p_value)

# SU and SN within electrodes
RRI_All = list()
RRO_All = list()

selectedExps = experimentNames
for EXP,experiment in enumerate(selectedExps):
    RRI = GlobalRR_In[experiment]
    tmpRRI = list()
    RRO = GlobalRR_Out[experiment]
    tmpRRO = list()
    #
    for cat in RRI.keys():
        tmpRRI.append(np.mean([RRI[cat][key][feature] for key in RRI[cat]]))
        tmpRRO.append(np.mean([RRO[cat][key][feature] for key in RRO[cat]]))
    #
    RRI_All.append(tmpRRI)
    RRO_All.append(tmpRRO)



pvals = list()
for i in range(len(RRI_All)):
    WilcoxonStat, p_value = stats.wilcoxon(RRI_All[i],RRO_All[i])
    pvals.append(p_value)

