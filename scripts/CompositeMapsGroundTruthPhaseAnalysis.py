#%% Load libraries
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import svd  
import scipy.io as sio
from scipy.interpolate import griddata
import pandas as pd

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

from detect_peaks import detect_peaks
import IgbHandling as igb
import egm_processing as egmp
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup
import DHHD
from StandardVTKObjects import *


#%% Load anatomy
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'#r'D:\\vgmar\model_data\anatomy\model30Box' #
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm

nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)

#%% Load Trajectory 

psdetectionPath = '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Filtered'
# psdetectionPath = '/mnt/d/vgmar/model_data/EX0008/psdetection'

basename = 'exp906c84'
PSD = pd.read_csv(os.path.join(psdetectionPath,'%s_PSPositionsAndDistances.csv'%basename),skipinitialspace=True)
# basename = 'EX0008_FB0_FL200s01'
# PSD = pd.read_csv(os.path.join(psdetectionPath,'%s_PSPositionsAndDistances_Overlap.csv'%basename),skipinitialspace=True)

# timeInterval = [17000,24000]
timeInterval = [0,12000]
PSD = PSD.loc[(PSD['t']>=timeInterval[0]) & (PSD['t']<timeInterval[1])] 

Positions = np.array(PSD.loc[:,['x','y','z']],dtype = int)

#%% Get indexes of the trajectory positions (takes a while to compute)

Indexes = np.zeros(len(Positions),dtype = int)
for i,row in enumerate(Positions):
    index = np.where((Anatomy[:,0]==row[0])&(Anatomy[:,1]==row[1])&(Anatomy[:,2]==row[2]))[0][0]
    Indexes[i] = index

#%% Make Heatmap

HM = np.zeros(len(Anatomy),dtype =float)
for ind in Indexes:
    HM[ind] +=1
# HM = HM/(timeInterval[1]-timeInterval[0])

#%% Get only the values in the electrode positions

# positionPath = '/mnt/d/vgmar/model_data/EX0008/psdetection_catheters'
# # outPath = '/mnt/d/vgmar/model_data/EX0008/psdetection'
# outPath = '/mnt/d/vgmar/model_data/composite_mapping/psdetection_ground_truth'

# AllElectrodeIndices = np.load(os.path.join(positionPath,'AllElectrodeIndices.npy'))
# AllElectrodes = Anatomy[AllElectrodeIndices]

# ElectrodePSGT = HM[AllElectrodeIndices]

# sio.savemat(os.path.join(outPath,'%s_PSGroundTruth.mat'%basename),{'positions':AllElectrodes,'pscount':ElectrodePSGT})

#%% Plot
prefPath = '/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' 
interpIndices = np.load(os.path.join(prefPath,'mappedArea.npy'))


currentSignal =  HM
# currentSignal[~np.isin(np.arange(len(Anatomy)),interpIndices)] = np.nan
currentSignal = currentSignal[rearrangeInds]

# currentSignal = np.zeros(len(Anatomy))*np.nan
# currentSignal[AllElectrodeIndices] = ElectrodePSGT
# currentSignal = currentSignal[rearrangeInds]

AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
                                               smoothFilter=True,cmap='heatmap',vmin=0,#vmax = np.max(HM[interpIndices]),
                                                returnData=True)



renderer = StandardViewRenderer([AnatomyActor],
                                cameraType='egmPosterior',
                                colorbarActor = cbarActor,
                                textActor = None,
                                viewportSplit = None)
PosWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
                        OffScreenRendering = False)  
PosWin.Render()

renderer = StandardViewRenderer([AnatomyActor],
                                cameraType='egmAnterior',
                                colorbarActor = cbarActor,
                                textActor = None,
                                viewportSplit = None)
AntWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
                        OffScreenRendering = False)
AntWin.Render()   

qv.QuickRenderWindowInteractor([AnatomyActor],[cbarActor])

# %%
