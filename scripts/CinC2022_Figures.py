#%% Load libraries
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import svd  
import scipy.io as sio
from scipy.interpolate import griddata
import pandas as pd
import vtk
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

from detect_peaks import detect_peaks
import IgbHandling as igb
import egm_processing as egmp
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup
import DHHD
from StandardVTKObjects import *
from DirectionTrackingCalculator import VelocityCalculator,VelocityAnalyzer


#%% Load anatomy
# anatomyPath = r'D:\\vgmar\model_data\anatomy\model30Box' 
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'#
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm

nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)

#By chamber
interAtrialConnections,CSInds,LAInds,RAInds = igb.GetAnatomicalInds(anatomyPath,(nx,ny,nz),Anatomy)

initialPoints = np.array([20383, 15497, 5654, 1110, 21897, 12919, 24652, 18574, 3218, 21553, 5736,
                          2548, 8756, 22228, 17290, 9391, 9541, 19127, 15307, 12715]) # Determined by getting the center of 20 k-means clusters
LA_Seeds = np.where(np.isin(initialPoints,LAInds))[0] # 0,  1,  4,  5,  6,  7,  9, 13, 14, 17, 18, 19
RA_Seeds = np.where(np.isin(initialPoints,RAInds))[0] # 2,  3,  8, 10, 11, 12, 15, 16



# Uncomment until line 271 for first paper figures
# #%% Load Trajectory 

# # psdetectionPath = r'D:\vgmar\model_data\exp906\pstracker\Control\Filtered' # /mnt/d/vgmar/model_data/'
# psdetectionPath = '/mnt/d/vgmar/model_data/EX0008/psdetection/Filtered'
# # psdetectionPath = r'D:\\vgmar\model_data\EX0008\psdetection\Filtered'

# # basename = 'exp906c17'
# basename = 'EX0008_FB0_FL200s08'

# PSD = pd.read_csv(os.path.join(psdetectionPath,'%s_PSPositions.csv'%basename),skipinitialspace=True)
# # PSD = pd.read_csv(os.path.join(psdetectionPath,'%s_PSPositionsAndDistances.csv'%basename),skipinitialspace=True)

# timeInterval = [1000,11000]
# step = 2
# # timeInterval = [(step+1)*1000,(step+2)*1000]
# PSD = PSD.loc[(PSD['t']>=timeInterval[0]) & (PSD['t']<timeInterval[1])] 

# Positions = np.array(PSD.loc[:,['x','y','z']],dtype = int)

# #%% Get indexes of the trajectory positions (takes a while to compute)

# Indexes = np.zeros(len(Positions),dtype = int)
# for i,row in enumerate(Positions):
#     index = np.where((Anatomy[:,0]==row[0])&(Anatomy[:,1]==row[1])&(Anatomy[:,2]==row[2]))[0][0]
#     Indexes[i] = index

# #%% Make Heatmap

# HM = np.zeros(len(Anatomy),dtype =float)
# for ind in Indexes:
#     HM[ind] +=1

# #%% Load final positions
# resultsPath =  '/mnt/d/vgmar/model_data/EX0008/source_tracking_v3'#
# # resultsPath =  r'D:\vgmar\model_data\exp906\source_tracking'# 


# LAExps = np.array([0,1,2,3,4])
# RAExps = np.array([5,6,7])



# ClassificationResults = np.load(os.path.join(resultsPath,
#                                 basename+'_ClassificationOutput.npy')).astype(int)
# FinalPosition = np.load(os.path.join(resultsPath,
#                                 basename+'_SourceLocations.npy'))

# # import pandas as pd
# indices = pd.read_csv(os.path.join(resultsPath,basename+'_logfile.log'),sep=' ')
# AllCatCenters = indices.loc[:,['ip','tStep','pos']]

# #%% Heatmaps and found rotors


# currentSignal =  HM
# # currentSignal[~np.isin(np.arange(len(Anatomy)),interpIndices)] = np.nan
# currentSignal = currentSignal[rearrangeInds]

# # currentSignal = np.zeros(len(Anatomy))*np.nan
# # currentSignal[AllElectrodeIndices] = ElectrodePSGT
# # currentSignal = currentSignal[rearrangeInds]

# AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
#                                                smoothFilter=True,cmap='heatmap',
#                                                vmin=0,#vmax =1,
#                                                 returnData=True)
# AnatomyActor.GetProperty().SetOpacity(0.3)
# source = vtk.vtkSphereSource()
# source.SetRadius(1/5)

# codeToPlot =1
# plotIndices = ClassificationResults[:,0]==codeToPlot
# plotIndices = np.where(plotIndices)[0]
# positionsToPlot = FinalPosition[plotIndices,:]

# rotorActor = qv.MakeGlyphActor(positionsToPlot.reshape(-1,3),np.zeros((len(positionsToPlot),3)),
#                                 magnitudes = np.zeros(len(positionsToPlot)),source=source,
#                                 vmin=0,cmap='jet',#vmax=1,
#                                 mode='color')

# codeToPlot =3
# plotIndices = (ClassificationResults[:,0]==codeToPlot) + (ClassificationResults[:,0]==2)
# plotIndices = np.where(plotIndices)[0]
# positionsToPlot = FinalPosition[plotIndices,:]

# exitActor = qv.MakeGlyphActor(positionsToPlot,np.zeros((len(positionsToPlot),3)),
#                                 magnitudes = 0.5*np.ones(len(positionsToPlot)),source=source,
#                                 vmin=0,cmap='jet',vmax=1,
#                                 mode='color')

# # codeToPlot =0
# # plotIndices = ClassificationResults[:,0]==codeToPlot
# # positionsToPlot = FinalPosition[plotIndices,:]

# # missedActor = qv.MakeGlyphActor(positionsToPlot,np.zeros((len(positionsToPlot),3)),
# #                                 magnitudes = np.ones(len(positionsToPlot)),source=source,
# #                                 vmin=0,vmax=1,cmap='heatmap',
# #                                 mode='color')


# # renderer = StandardViewRenderer([AnatomyActor,rotorActor,exitActor,missedActor],
# #                                 cameraType='egmPosterior',
# #                                 colorbarActor = cbarActor,
# #                                 textActor = None,
# #                                 viewportSplit = None)
# # PosWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
# #                         OffScreenRendering = False)  
# # PosWin.Render()

# # renderer = StandardViewRenderer([AnatomyActor,rotorActor,exitActor,missedActor],
# #                                 cameraType='egmAnterior',
# #                                 colorbarActor = cbarActor,
# #                                 textActor = None,
# #                                 viewportSplit = None)
# # AntWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
# #                         OffScreenRendering = False)
# # AntWin.Render()   

# qv.QuickRenderWindowInteractor([AnatomyActor,rotorActor])
# qv.QuickRenderWindowInteractor([AnatomyActor,exitActor])
# #%% Plot the sequential grid placement

# ## Load experiment

# basename = 'EX0008_FB0_FL200s01'
# tBegin = 1000
# ve_file = '/mnt/d/vgmar/model_data/EX0008/%sf_ve_egm_endo_epi.igb.gz'%basename

# egmDataAll,hdr = igb.Load(ve_file,returnRawSignal=True) # Workaround for memory issues
# egmDataAll = egmDataAll.squeeze()
# egmDataAll = egmDataAll[tBegin:,veIndexes] # ignore 1s for initialization

# lSegment = 1000
# maxSteps = int(egmDataAll.shape[0]/lSegment)

# ## Define calculators
# vCalc = VelocityCalculator()
# vCalc.Radius = np.sqrt(3.5**2+3.5**2)
# vAnalyzer = VelocityAnalyzer()

# ## Define arrow
# source = vtk.vtkArrowSource()
# source.SetShaftRadius(0.1)
# source.SetTipLength(0.4)
# source.SetTipRadius(0.25)

# #%%
# seed = 0
# if seed in LA_Seeds:
#     chosenInds = LAInds
# else:
#     chosenInds = RAInds

# chosenAnatomy = Anatomy[chosenInds]

# positionsToPlot = AllCatCenters.loc[AllCatCenters['ip']==seed,'pos']
# # positionsToPlot = chosenInds[positionsToPlot]

# magnitudes = np.ones(16)

# electrodeActors = []
# for tStep,position in enumerate(positionsToPlot):
#     CatheterObj = CatheterClass('HDGrid-4')
#     CatheterObj.GetTissuePatch(position,chosenAnatomy)
#     CatheterObj.ProjectionFunction(chosenAnatomy,rotationAngle =0)

#     gridPositions = CatheterObj.ElectrodeCoordinates

#     # Get velocities
#     timeInterval = [(tStep%maxSteps)*lSegment, (tStep%maxSteps+1)*lSegment]
#     CatheterObj.GetSignals(egmDataAll[timeInterval[0]:timeInterval[1],chosenInds],rawInput=True, hdr=hdr)
#     CatheterObj.DetectActivations(method = 'adaptiveThreshold')

#     allActivations,allPositions,allElectrodeIndices = vCalc.AdjustSignalsCV(CatheterObj,useRealPositions=True)
#     xVelocity, yVelocity = vCalc.Compute2DActivationWavefront(allActivations, allPositions)
#     ConductionVelocity = vCalc.MakeCVDict(allActivations,allElectrodeIndices,xVelocity,yVelocity)
#     intervalPreferentiality = vAnalyzer.AnalyzeVelocity([0,1000],ConductionVelocity)
    
#     # Calculate variance of angles
#     Velocities3D = vAnalyzer.ComputeMeanVelocityV2(intervalPreferentiality,vCalc)


#     positionActor = qv.MakeGlyphActor(gridPositions,Velocities3D*0.8,
#                                     magnitudes = magnitudes*tStep,
#                                     vmin=0,vmax=3,
#                                     cmap='viridis',#source = source, 
#                                     mode='color')
#     electrodeActors.append(positionActor)

# #%% Plot
# currentSignal =  HM
# currentSignal = currentSignal[rearrangeInds]




# AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
#                                                smoothFilter=True,cmap='heatmap',
#                                                vmin=0,#vmax =1,
#                                                 returnData=True)
# # AnatomyActor.GetProperty().SetOpacity(0.9)


# qv.QuickRenderWindowInteractor(electrodeActors)

# renderer3 = StandardViewRenderer(electrodeActors,
#                                 cameraType='egmPosterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin3 = OneViewRenderWindow(renderer3,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin3.Render()

# electrodeActors.append(AnatomyActor)

# renderer2 = StandardViewRenderer([AnatomyActor],
#                                 cameraType='egmPosterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin2 = OneViewRenderWindow(renderer2,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin2.Render()

# %%
#%% Other paper

# #%% Load Phase results from catheters
# psPath = '/mnt/d/vgmar/model_data/composite_mapping/psdetection_catheters'#'/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' # r'D:\\vgmar\model_data\composite_mapping'


# data = sio.loadmat(os.path.join(psPath,'EX0008_FB0_FL200s01_PS.mat')) #exp906c84_PS.mat

# Positions = data['pspositions']
# NPSs = data['pscount'].flatten()

# prefPath = '/mnt/d/vgmar/model_data/composite_mapping/results_220408' 
# interpIndices = np.load(os.path.join(prefPath,'mappedArea.npy'))

# PlotPS = griddata(Positions, NPSs, (Anatomy[interpIndices,0],
#                                     Anatomy[interpIndices,1],
#                                     Anatomy[interpIndices,2]),
#                          method='nearest')

#%% Load other quantities
prefPath ='/mnt/d/vgmar/model_data/composite_mapping/results_220902'#'/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' # r'D:\\vgmar\model_data\composite_mapping'

basename = 'Meander' # Stable, Meander, Collision

data = sio.loadmat(os.path.join(prefPath,basename+'Map.mat'))

pos = data['processedVectorLocsH']
velocity = data['processedVectorsH']



data = sio.loadmat(os.path.join(prefPath,basename+'Score.mat'))

posH = data['processedVectorLocsH']
val = data['H']

# Peripheral, StableRotor,Meander, Collision, 


# # Order of columns: 
# # 0-3: positions
# # 3: True PS
# # 4-7: vectors
# # 7:Dispersion
# # 8: fractionation
# # 9: PS catheters
# # 10: Divergence
# # 11: Curl
# # 12: heterogeneity
# # ResultsMatrix = sio.loadmat(os.path.join(prefPath,basename+'_CompositeMapsDHHD.mat'))['matrix']
# ResultsMatrix = sio.loadmat(os.path.join(prefPath,basename+'.mat'))['matrix']
# Positions = ResultsMatrix[:,:3]
# Signal = np.abs(ResultsMatrix[:,-1].flatten())

prefPath = '/mnt/d/vgmar/model_data/composite_mapping/results_220408' 
interpIndices = np.load(os.path.join(prefPath,'mappedArea.npy'))

PlotSignal = griddata(posH, val, (Anatomy[interpIndices,0],
                                        Anatomy[interpIndices,1],
                                        Anatomy[interpIndices,2]),
                    method='linear')

PlotSignal = egmp.Norm2Vals(PlotSignal.flatten(),[0,1])
# print(np.nanmax(PlotSignal))
# print(np.nanmin(PlotSignal))

#%%
currentSignal = np.zeros(len(Anatomy))*np.nan
currentSignal[interpIndices] = PlotSignal
currentSignal = currentSignal[rearrangeInds]

AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
                                               smoothFilter=True,cmap='viridis',vmin=0,vmax = 1,#np.max(HM[interpIndices]),
                                                returnData=True)

source = vtk.vtkArrowSource()
source.SetShaftRadius(0.08)
source.SetTipLength(0.4)
source.SetTipRadius(0.2)


arrowActor = qv.MakeGlyphActor(pos[::1],velocity[::1],
                            # magnitudes = np.ones(len(pos))*0.25,
                            source=source,arrowColor='DarkBlue')

# renderer1 = StandardViewRenderer([AnatomyActor],
#                                 cameraType='egmPosterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin1 = OneViewRenderWindow(renderer1,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin1.Render()

# # renderer2 = StandardViewRenderer([AnatomyActor],
# #                                 cameraType='egmAnterior',
# #                                 colorbarActor = cbarActor,
# #                                 textActor = None,
# #                                 viewportSplit = None)
# # AntWin = OneViewRenderWindow(renderer2,windowSize = [1200,800],
# #                         OffScreenRendering = False)
# # AntWin.Render()   

renderer2 = StandardViewRenderer([arrowActor],
                                cameraType='egmPosterior',
                                colorbarActor = cbarActor,
                                textActor = None,
                                viewportSplit = None)
AntWin = OneViewRenderWindow(renderer2,windowSize = [1200,800],
                        OffScreenRendering = False)
AntWin.Render()   


qv.QuickRenderWindowInteractor([arrowActor])

#%% Load Ozan's results 

# prefPath ='/mnt/d/vgmar/model_data/composite_mapping/results_220408'#'/mnt/d/vgmar/model_data/composite_mapping/cincCompleteResults' # r'D:\\vgmar\model_data\composite_mapping'

# basename = 'Collision'+'Matrix' #'TwoSources.mat'#'SingleReentry.mat'#'Peripheral.mat'#'Focal.mat'
# # Peripheral, StableRotor,Meander, Collision, 

# ResultsMatrix = sio.loadmat(os.path.join(prefPath,basename+'_CompositeMapsDHHD.mat'))['matrix']
# Positions = ResultsMatrix[:,:3]
# Vectors =  ResultsMatrix[:,4:7]

# #%%
# currentSignal = np.zeros(len(Anatomy))
# AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
#                                                smoothFilter=True,cmap='heatmap',vmin=0,#vmax = np.max(HM[interpIndices]),
#                                                 returnData=True)
# AnatomyActor.GetProperty().SetOpacity(0.2)

# source = vtk.vtkArrowSource()
# source.SetShaftRadius(0.05)
# source.SetTipLength(0.25)
# source.SetTipRadius(0.15)
# compositeMaps = qv.MakeGlyphActor(Positions,Vectors,magnitudes = np.ones(len(Positions))*0.7,
#                                 source = source,mode='scale')

# renderer = StandardViewRenderer([AnatomyActor,compositeMaps],
#                                 cameraType='egmPosterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin.Render()

# renderer = StandardViewRenderer([AnatomyActor,compositeMaps],
#                                 cameraType='egmAnterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# AntWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
#                         OffScreenRendering = False)
# AntWin.Render()   

# qv.QuickRenderWindowInteractor([AnatomyActor,compositeMaps])

