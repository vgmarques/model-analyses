'''
This function creates and executes bash scripts to run PSFinder with the same
configuration for a given list of names
'''

import sys,os

# The name is composed by basename%02d, where %02d comes from the iteration list
# everything else has to be adjusted in the bash script itself before running
# Check "RunPSFinder.sh" as a guide

basename = 'exp906a'
iteration_lst = [5]#,12,13,14,15,17,18,19,70,71,72,74,75,82,83,84,87,88,89] # and 5

for i,code in enumerate(iteration_lst):

    with open ('tmp/btdetection_%s%02d.sh'%(basename,code), 'w') as rsh:
        rsh.write('''\
#!/bin/bash -l
#SBATCH --job-name="btdetection_%02d"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl
#SBATCH --time=15:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --constraint=gpu
#SBATCH --account=s1074

module load daint-gpu
module load cray-python
module load matplotlib

# Paths and filenames

ANATOMYPATH=/users/vgonalve/exec/anatomy/model30
DATAPATH=/users/vgonalve/exec/exp906
OUTPATH=/users/vgonalve/exec/exp906/btdetection
CELLFILE=${ANATOMYPATH}/model24-30-heart-cell-pvi.igb
TCCFILE=${DATAPATH}/exp906a%02d_tcc_afull.igb.gz

EXPNAME=exp906a%02d
FNAMEIN=${DATAPATH}/${EXPNAME}_vm_afull.iga.gz
FNAMEOUT=${OUTPATH}/${EXPNAME}.btd


# timing
TBEGIN=0
TEND=-1
TINT=1


NTHREADS=22 # The number of threads is limited by the memory

# Put all together
BTDETECTIONPARAMS='-tBegin='${TBEGIN}'
                    -tEnd='${TEND}'
                    -tInt='${TINT}'
                    -outputFile='${FNAMEOUT}'
                    -n_processes='${NTHREADS}
                

mkdir $OUTPATH
srun python BreakthroughDetection.py $BTDETECTIONPARAMS $FNAMEIN $TCCFILE $CELLFILE
'''%(code,code,code))
    
    # Run the script
    os.system('sbatch tmp/btdetection_%s%02d.sh'%(basename,code))



