#%% Import stuff
import os,sys
import numpy as np
import subprocess
# from copy import deepcopy
# from skimage.measure import label as bwlabeln 
# from skimage.morphology import binary_closing as imclosing
# from skimage.morphology import binary_dilation as imdilation
# from skimage.morphology import binary_erosion as imerosion
# import multiprocessing as mp

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import quick_visualization as qv

#%% Load Anatomy
AnatomyCell,_ = igb.Load('/mnt/d/vgmar/model_data/anatomy/model30_210325/model24-30-heart-cell.igb.gz')
AnatomyCell = np.swapaxes(AnatomyCell,0,-1)
AnatomyCell = AnatomyCell[:,:,:350]
downRatio = 2
AnatomyCell=AnatomyCell[::downRatio,::downRatio,::downRatio]
Anatomy = np.zeros_like(AnatomyCell)
Anatomy[np.isin(AnatomyCell,[113,114,115,116,117,118,119])] = 1


#%% Load inter-atrial connections and Bachmann bundle
EBundles,_ = igb.Load('/mnt/d/vgmar/Documents/GitHub/USI/model24/heart.igb')
EBundles = np.swapaxes(EBundles,0,-1)
EBundles = EBundles[:,:,:350]
EBundles = EBundles[::downRatio,::downRatio,::downRatio]
EBundles[~((EBundles==36))] = 0 #(EBundles==34)|
EBundles[ (EBundles==36)] = 1#(EBundles==34)|


# %% Select regions where the anatomy overlaps with the bundles
FakeBTRegions = Anatomy*EBundles
dx,dy,dz = np.where(FakeBTRegions)

#%% Load BT file
fname = '/mnt/d/vgmar/model_data/exp906/bt_detection/updated_algorithm/exp906c74_BTwv.btd'
fi = open(fname,'rb')
fdata = fi.read().decode()
fdata = fdata.split('\n')[:-1]
hdr = fdata[1].split(',')
fdata = fdata[2:]

btMatrix = np.zeros((len(fdata),len(hdr)),dtype=int)
for i,line in enumerate(fdata):
    tmp = np.asarray(line.split(','),dtype='int')
    btMatrix[i,:] = tmp

#%% Remove points in the regions

btLocations = btMatrix[:,1:4]

removeInds = np.isin(btLocations[:,0],dx)*np.isin(btLocations[:,1],dy)*np.isin(btLocations[:,2],dz)

FilteredBTMatrix = btMatrix[~removeInds]


#%% Save
with open(os.path.join(os.path.dirname(fname),
        os.path.splitext(fname)[0]+'_filtered.btd'), 'wb') as File:
    File.write(b't,x,y,z,b,vip\n')
    for row in range(len(FilteredBTMatrix)):
        if row!=len(FilteredBTMatrix)-1:
            File.write(b'%d,%d,%d,%d,%d,%d\n'%(FilteredBTMatrix[row,0],
                                                FilteredBTMatrix[row,1],
                                                FilteredBTMatrix[row,2],
                                                FilteredBTMatrix[row,3],
                                                FilteredBTMatrix[row,4],
                                                FilteredBTMatrix[row,5]))
        else:
            File.write(b'%d,%d,%d,%d,%d,%d'%(FilteredBTMatrix[row,0],
                                                FilteredBTMatrix[row,1],
                                                FilteredBTMatrix[row,2],
                                                FilteredBTMatrix[row,3],
                                                FilteredBTMatrix[row,4],
                                                FilteredBTMatrix[row,5]))




# #%% 
# # Anatomy[dx,dy,dz] = 1

# xx,yy,zz = np.where(Anatomy)
# plotAnatomy = np.array([xx,yy,zz]).T
# plotAnatomy = plotAnatomy[::10,:]
# test_val1 = np.zeros(len(plotAnatomy)).reshape(1,-1)

# # plotHoles = btLocations[removeInds,:]
# plotHoles = np.asarray([dx,dy,dz]).T
# test_val2 = np.ones(len(plotHoles)).reshape(1,-1)


# ActorEndo,_ = qv.MakePolyDataActor(plotAnatomy,test_val1,vmin=0,vmax=1)
# ActorHoles,_ = qv.MakePolyDataActor(plotHoles,test_val2,vmin=0,vmax=1)
# ActorEndo.GetProperty().SetOpacity(0.2)


# qv.QuickRenderWindowInteractor([ActorEndo,ActorHoles])  #ActorEpi,
# # %%

# %%
