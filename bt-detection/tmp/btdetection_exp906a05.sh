#!/bin/bash -l
#SBATCH --job-name="btdetection_05"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl
#SBATCH --time=15:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=24
#SBATCH --constraint=gpu
#SBATCH --account=s1074

module load daint-gpu
module load cray-python
module load matplotlib

# Paths and filenames

ANATOMYPATH=/users/vgonalve/exec/anatomy/model30
DATAPATH=/users/vgonalve/exec/exp906
OUTPATH=/users/vgonalve/exec/exp906/btdetection
CELLFILE=${ANATOMYPATH}/model24-30-heart-cell-pvi.igb
TCCFILE=${DATAPATH}/exp906a05_tcc_afull.igb.gz

EXPNAME=exp906a05
FNAMEIN=${DATAPATH}/${EXPNAME}_vm_afull.iga.gz
FNAMEOUT=${OUTPATH}/${EXPNAME}.btd


# timing
TBEGIN=0
TEND=-1
TINT=1


NTHREADS=22 # The number of threads is limited by the memory

# Put all together
BTDETECTIONPARAMS='-tBegin='${TBEGIN}'
                    -tEnd='${TEND}'
                    -tInt='${TINT}'
                    -outputFile='${FNAMEOUT}'
                    -n_processes='${NTHREADS}
                

mkdir $OUTPATH
srun python BreakthroughDetection.py $BTDETECTIONPARAMS $FNAMEIN $TCCFILE $CELLFILE
