# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 11:53:45 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
#%%
import sys,os
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
import pickle#5 as pickle
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))
from RecurrenceComputation import RecurrenceComputation
import egm_processing as egmp

#%% Load data
CATPATH='/mnt/d/vgmar/model_data/exp915'#D:\\vgmar\\model_data\\exp915'#\\electrode_data\\Control'
RECPATH='/mnt/d/vgmar/model_data/exp915'#\\recurrence\\Control'
# PSPATH = '/mnt/d/vgmar/model_data/exp915'#\\pstracker\\Control\\Filtered'

experiment = 'exp915f04'#'exp909b304'
cat='PLA'
Block = np.array([3000,7000])

CGroup = pickle.load(open(os.path.join(CATPATH,experiment+'_CatheterData.catG'),'rb'))
fs = CGroup.Catheters[cat].SamplingFrequency

## Calculate global distance matrix for catheter
with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
    DistanceMatrices =  pickle.load(output)
    EstimatedAFCLs =  pickle.load(output)

# Time intervals
# Blocks = pickle.load(open(os.path.join(PSPATH,'Exp906_LongSourceBlocks.pkl'),'rb'))[experiment]
# Blocks = pickle.load(open(os.path.join(PSPATH,'Exp909_LongSourceBlocks.pkl'),'rb'))[experiment]

k= 1
#%% Make relevant signals

Elec = CGroup.Catheters[cat]
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1

estimatedAFCL = EstimatedAFCLs[cat]
minLineLength = (estimatedAFCL*Elec.SamplingFrequency)//recComp.SampleShift

downsampledBlock = Block//recComp.SampleShift

partDM = DistanceMatrices[cat][downsampledBlock[0]:downsampledBlock[1],
                               downsampledBlock[0]:downsampledBlock[1]]

RP, recurrenceTime, recurrenceThreshold, partDM = \
            recComp.ComputeRecurrencePlot(partDM, fs, estimatedAFCL,recurrenceThreshold= 0.15)

recurrenceTime = recurrenceTime+Block[0]/1000                    
RPE = recComp.ErodeRP(RP,partDM,linear=True)

phaseSignal,_ = egmp.ComputeActivationPhaseSignal(Elec)

# phaseSignal = phaseSignal.T.reshape(4,4,-1)
# phaseSignal = np.transpose(phaseSignal,(1,0,2)) # for plotting purposes
xi = Elec.ElectrodesTemplate[:,0]
yi = Elec.ElectrodesTemplate[:,1]


#%% Animate
iniF = Block[0]
step = 10//recComp.SampleShift
nFrames =  len(recurrenceTime)//step #
sampleSteps = (recurrenceTime*1000).astype(int)

fig, ax = plt.subplots(1,2,figsize=(14.4,5.75),gridspec_kw={'width_ratios': [1, 1]})

# ax[0].imshow(RP,cmap='gray_r',origin='lower',
#              extent=[Block[0],Block[1],Block[0],Block[1]])
xx,yy = np.where(RPE)
ax[0].scatter(recurrenceTime[xx],recurrenceTime[yy],color='k',s=0.005)
ax[0].set_xticks(np.append(recurrenceTime[::500],Block[1]/1000))
ax[0].set_yticks(np.append(recurrenceTime[::500],Block[1]/1000))
ax[0].set_xlim([recurrenceTime[0],recurrenceTime[-1]])
ax[0].set_ylim([recurrenceTime[0],recurrenceTime[-1]])
vline = ax[0].axvline(recurrenceTime[0],color='r',linewidth=2)
ax[0].set_xlabel('Time (s)',fontsize = 18)
ax[0].set_ylabel('Time (s)',fontsize = 18)
ax[0].tick_params(labelsize=16)



ax[1].set(xlim=(yi[0]-1.5,yi[-1]+1.5), ylim=(xi[0]-2.5,xi[-1]+2.5))
ax[1].autoscale(False)

ax[1].set_xlabel('Position (mm)',fontsize = 18)
ax[1].set_ylabel('Position (mm)',fontsize = 18)
ax[1].tick_params(labelsize=16)

cax = ax[1].scatter(yi,xi,c =phaseSignal[sampleSteps[0],:],cmap = 'gnuplot',#origin='lower',
                # extent= [yi[0],yi[1],xi[0],xi[1]],
                marker='s',s=5000,
                vmin = -np.pi,vmax = np.pi)#,shading='nearest')

cbar = fig.colorbar(cax, ticks=[-np.pi, 0, np.pi],ax = ax[1])
cbar.set_label('Phase (rad)', fontsize = 24)
cbar.ax.set_yticklabels(['$-\pi$', '0', '$+\pi$'], fontsize = 24)   
fig.suptitle('Catheter %s - t= %d ms'%(cat,sampleSteps[0]), fontsize = 28)


def animSP(i):
    cax.set_array(phaseSignal[sampleSteps[i*step],:])
    vline.set_xdata(recurrenceTime[i*step])
    fig.suptitle('Catheter %s - t= %d ms'%(cat,sampleSteps[i*step]), fontsize = 28)
    return fig

anim = animation.FuncAnimation(fig, animSP, interval = 60, frames=nFrames,
                           repeat_delay = 200)

save = 1
if save:
    writer = animation.FFMpegWriter(fps = 25)
    anim.save(os.path.join(os.getcwd(),'Videos','%s_%s_%d.mp4'%(experiment,cat,k)), writer=writer)
    k = k+1
else:
    plt.show()

# %%
