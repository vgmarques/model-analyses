# -*- coding: utf-8 -*-
#%%
"""
Created on Tue Feb  2 09:25:17 2021

DESCRIPTION: This script generates VTK visualizations of the posterior and anterior portions of the
atria, saving screenshots every time step for posteriorly generating movies with ffmpeg or other method.

This version only has two layers (i.e. separates endo and epicardium), which need to be defined with cell 
types in the input anatomy

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import time
import sys
import os
import numpy as np
import subprocess
import argparse
import scipy.io as sio
import multiprocessing as mp

#%%
import vtk
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray

# Other packages
upperDir = os.path.dirname(os.getcwd())
sys.path.append('../aux-functions')
sys.path.append('../../local/bin')
#sys.path.append('/mnt/d/vgmar/Documents/GitHub/USI/model24/fibrosis')
#sys.path.append('/mnt/d/vgmar/Documents/GitHub/USI/model24/vis')

# from pyccmc import igb_read,igb_write # Not needed here, but inside igbhexagrid
from igbhexagrid import IGBUnstructuredGrid 
from StandardVTKObjects import *
import IgbHandling as igb
import UserInterfaceFunctions as ui
from DataClasses import PSCluster,PStrackerResults


def CreateFilters(EndoObject,EpiObject):
	EndoNormalGenerator = SmoothFilters(EndoObject)
	EpiNormalGenerator = SmoothFilters(EpiObject)
	return EndoNormalGenerator,EpiNormalGenerator

def MakeCameraDicts(args):
    cameraDictPos = {'Position':args.cPosP,
                     'FocalPoint':args.cFPP,
                     'ViewUp':args.cVUP,
                     'Zoom':args.cZoomP}
    cameraDictAnt = {'Position':args.cPosA,
                     'FocalPoint':args.cFPA,
                     'ViewUp':args.cVUA,
                     'Zoom':args.cZoomA}
    cameraDictRight = {'Position':args.cPosR,
                     'FocalPoint':args.cFPR,
                     'ViewUp':args.cVUR,
                     'Zoom':args.cZoomR}
    return cameraDictPos,cameraDictAnt,cameraDictRight

def MakePSActors(nparray,args,sphereColor = (0,1,1)):
	nparray = nparray*args.scale
	nCoords = nparray.shape[0]
	nElem = nparray.shape[1]
	#
	verts = vtk.vtkPoints()
	cells = vtk.vtkCellArray()
	scalars = None
	#
	PSData = vtk.vtkPolyData()
	#
	verts.SetData(numpy_to_vtk(nparray))
	cells_npy = np.vstack([np.ones(nCoords, dtype = 'int'),
						   np.arange(nCoords, dtype = 'int')]).T.flatten()
	cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
	PSData.SetPoints(verts)
	PSData.SetVerts(cells)
	#
	PSmapper = vtk.vtkPolyDataMapper()
	PSmapper.SetInputDataObject(PSData)
	#
	PSActor = vtk.vtkActor()
	PSActor.SetMapper(PSmapper)
	PSActor.GetProperty().SetRepresentationToPoints()
	PSActor.GetProperty().SetColor(sphereColor)
	PSActor.GetProperty().RenderPointsAsSpheresOn()
	PSActor.GetProperty().SetPointSize(args.sphereRadius)
	#
	return PSActor,PSData

def UpdatePSData(PSData,nparray):
	nparray = nparray*args.scale
	nCoords = nparray.shape[0]
	nElem = nparray.shape[1]
	#
	verts = vtk.vtkPoints()
	cells = vtk.vtkCellArray()
	scalars = None
	#
	verts.SetData(numpy_to_vtk(nparray))
	cells_npy = np.vstack([np.ones(nCoords, dtype = 'int'),
						   np.arange(nCoords, dtype = 'int')]).T.flatten()
	cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
	PSData.Reset()
	PSData.SetPoints(verts)
	PSData.SetVerts(cells)
	PSData.GetPointData().Modified()
	return PSData


def MakeBTActors(nparray,args,starColor = (0,1,0)):
    nparray = nparray*args.scale*2 #*2 scale for BTs
    nCoords = nparray.shape[0]
    nElem = nparray.shape[1]
    #
    verts = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    #
    poly = vtk.vtkPolyData()
    #
    verts.SetData(numpy_to_vtk(nparray))
    cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
                           np.arange(nCoords, dtype = np.int64)]).T.flatten()
    cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
    poly.SetPoints(verts)
    poly.SetVerts(cells)

    source = vtk.vtkConeSource()
    source.SetRadius(args.coneSize/10)
    source.SetResolution(4)
    source.SetHeight(args.coneSize/10)

    BTGlyph = vtk.vtkGlyph3D()
    BTGlyph.SetInputData(poly)
    BTGlyph.SetSourceConnection(source.GetOutputPort())
    BTGlyph.SetScaleFactor(3)
    #
    BTmapper = vtk.vtkPolyDataMapper()
    BTmapper.SetInputConnection(BTGlyph.GetOutputPort())
    #
    BTActor = vtk.vtkActor()
    BTActor.SetMapper(BTmapper)
    BTActor.GetProperty().SetColor(starColor)
    # BTActor.GetProperty().SetPointSize()
    #
    return BTActor,BTGlyph

def UpdateBTData(BTGlyph,nparray):
    nparray = nparray*args.scale*2 #*2 scale for BTs
    nCoords = nparray.shape[0]
    nElem = nparray.shape[1]
    #
    verts = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    #
    poly = vtk.vtkPolyData()
    #
    verts.SetData(numpy_to_vtk(nparray))
    cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
                            np.arange(nCoords, dtype = np.int64)]).T.flatten()
    cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
    poly.SetPoints(verts)
    poly.SetVerts(cells)

    BTGlyph.SetInputData(poly)
    BTGlyph.Modified()
    return BTGlyph


def InitializeRenderingElements(EndoObject,EndoVals,EpiObject,EpiVals,tStep,args):
    # Mappers
	DataMapperEndo = StandardAtrialMapper(EndoObject,len(EndoVals))
	DataMapperEpi = StandardAtrialMapper(EpiObject,len(EpiVals))
	# Actors
	DataActorEndo = StandardLayerActor(DataMapperEndo,opacity = 1)
	DataActorEpi =  StandardLayerActor(DataMapperEpi,opacity = 0.5)
	colorBar = ColorbarActor(DataMapperEndo,title = 'Vm (mV)')
	TextActor = TimeStampActor(tStep)
	if args.windowType=='side-by-side':
		# Renderers
		PosRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
										cameraType = args.cameraTypePos,
										colorbarActor = None,
										textActor = TextActor,
										viewportSplit = 'horizontal-left')
		AntRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
										cameraType = args.cameraTypeAnt,
										colorbarActor = colorBar,
										textActor = None,
										viewportSplit = 'horizontal-right')
		#RenderWindow
		renWin = TwoViewsRenderWindow(PosRen,AntRen,windowSize = [args.figWidth,args.figHeight])
	elif args.windowType=='vertical':
		# Renderers
		PosRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
										cameraType = args.cameraTypePos,
										colorbarActor = None,
										textActor = None,
										viewportSplit = 'vertical-up')
		# PosRen.ResetCamera()
		AntRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
										cameraType = args.cameraTypeAnt,
										colorbarActor = colorBar,
										textActor = TextActor,
										viewportSplit = 'vertical-down')
		# AntRen.ResetCamera()
		renWin = TwoViewsRenderWindow(PosRen,AntRen,windowSize =  [args.figWidth,args.figHeight])


	elif args.windowType=='oneWindow':
		PosRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
										cameraType = args.cameraTypePos,
										colorbarActor = None,
										textActor = None,
										viewportSplit = None)
		#RenderWindow
		renWin = OneViewRenderWindow(PosRen,windowSize =  [args.figWidth,args.figHeight])
		AntRen=PosRen

	return renWin,PosRen,AntRen,DataActorEndo,DataActorEpi,DataMapperEndo,DataMapperEpi,TextActor

def RightViewElements(EndoObject,EndoVals,EpiObject,EpiVals,tStep,args):
    # Mappers
    DataMapperEndo = StandardAtrialMapper(EndoObject,len(EndoVals))
    DataMapperEpi = StandardAtrialMapper(EpiObject,len(EpiVals))
    # Actors
    DataActorEndo = StandardLayerActor(DataMapperEndo,opacity = 1)
    DataActorEpi =  StandardLayerActor(DataMapperEpi,opacity = 0.5)
    colorBar = ColorbarActor(DataMapperEndo,title = 'Vm (mV)')
    TextActor = TimeStampActor(tStep)

    PosRen = StandardViewRenderer([DataActorEndo,DataActorEpi],
                                    cameraType = args.CameraTypeRight,
                                    colorbarActor = colorBar,
                                    textActor = TextActor,
                                    viewportSplit = None)
    #RenderWindow
    renWin = OneViewRenderWindow(PosRen,windowSize =  [args.figWidth,args.figHeight])
    return renWin,PosRen,DataMapperEndo,DataMapperEpi,TextActor



def MakeScreenshot(renWin,fname,fpath = './anims'):
	renWin.Render()	
	w2if = vtk.vtkWindowToImageFilter()
	w2if.SetInput(renWin)
	w2if.Update()
	writer = vtk.vtkPNGWriter()
	writer.SetFileName(os.path.join(fpath,fname+".png"))
	writer.SetInputConnection(w2if.GetOutputPort())
	writer.Write()

def Reader(args,EndoIdx,EpiIdx,DataQueue):
    print('Reader initialized')
    fname_signals = os.path.join(args.dataPath,args.expName)
    tBegin = args.tBegin
    tEnd = args.tEnd
    tInt = args.tInt

    tString = str(tBegin)+':'+str(tInt)+':'+str(tEnd)   

    # Reads the data and puts in queue

    with subprocess.Popen(["gzip","-dc",fname_signals],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([args.iga2igb,"-x","0:1:-1","-y","0:1:-1","-z","0:1:-1",
			   "-t",tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # read the header
            hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)

            # size of a single slice in time
            nx,ny,nz = hdr['x'],hdr['y'],hdr['z']
            dtype = np.short
            csize = nx*ny*nz*dtype().nbytes

            if tEnd==-1:
                tEnd=tBegin+hdr['t']*tInt-1 #FIXME: I think this is correct now, but have to check
                print('tEnd: %d'%tEnd)
            # Loop over data
            for t in range(0,int((tEnd-tBegin)/tInt+1)):
                vals = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nz,ny,nx))
                # Endocardium
                EndoVals = vals[EndoIdx[:,2],EndoIdx[:,1],EndoIdx[:,0]].astype('float32')
                EndoVals = hdr['facteur']*EndoVals + hdr['zero']
                # Epicardium
                EpiVals = vals[EpiIdx[:,2],EpiIdx[:,1],EpiIdx[:,0]].astype('float32')
                EpiVals = hdr['facteur']*EpiVals + hdr['zero']

                DataQueue.put(('data',(t,EndoVals,EpiVals)))
    [DataQueue.put(('kill',None)) for i in range(args.n_processes)];


def Plotter(args,EndoGrid,EpiGrid,PSpos,BTpos,DataQueue):
    print('Worker Initialized')

    # Initialize array for Endo and epi
    ## Endo
    bpts  = EndoGrid.Points
    EndoIdx = np.rint(bpts/args.scale).astype('int')

    vdataEndo = vtk.vtkFloatArray()
    vdataEndo.SetName("vmf")
    vdataEndo.SetNumberOfComponents(1)
    vdataEndo.SetNumberOfTuples(EndoIdx.shape[0])
    EndoGrid.GetPointData().AddArray(vdataEndo)
    EndoObject = EndoGrid.VTKObject

    ## Epi
    bpts  = EpiGrid.Points
    EpiIdx = np.rint(bpts/args.scale).astype('int')
    vdataEpi = vtk.vtkFloatArray()
    vdataEpi.SetName("vmf")
    vdataEpi.SetNumberOfComponents(1)
    vdataEpi.SetNumberOfTuples(EpiIdx.shape[0])
    EpiGrid.GetPointData().AddArray(vdataEpi)
    EpiObject = EpiGrid.VTKObject

    # Create filters
    EndoFiltered,EpiFiltered = CreateFilters(EndoObject,EpiObject)

    # Initialize the VTK objects
    renWin,PosRen,AntRen,_,_,DataMapperEndo,DataMapperEpi,TextActor = \
    InitializeRenderingElements(EndoFiltered.GetOutput(),np.zeros(EndoIdx.shape[0]),
                                EpiFiltered.GetOutput(),np.zeros(EpiIdx.shape[0]),
                                0,args)
    if args.plotRV:
        renWinRV,RenRV,DataMapperEndoRV,DataMapperEpiRV,TextActorRV = \
        RightViewElements(EndoFiltered.GetOutput(),np.zeros(EndoIdx.shape[0]),
                          EpiFiltered.GetOutput(),np.zeros(EpiIdx.shape[0]),
                            0,args) 

    if args.plotPS:
        if args.tBegin in PSpos.keys():
            PSActor,PSData = MakePSActors(np.asarray(PSpos[args.tBegin]),args)
        else:
            PSActor,PSData = MakePSActors(np.empty((0,3)),args) # This creates the actors but without points, I think
        # Add PS actor
        PosRen.AddActor(PSActor)
        AntRen.AddActor(PSActor)
        if args.plotRV: RenRV.AddActor(PSActor)
        #Update
        PosRen.Modified()
        AntRen.Modified()     
        if args.plotRV: RenRV.Modified()

    if args.plotBT:
        if args.tBegin in BTpos.keys():
            BTActor,BTGlyph = MakeBTActors(np.asarray(BTpos[args.tBegin]),args)
        else:
            BTActor,BTGlyph = MakeBTActors(np.empty((0,3)),args) # This creates the actors but without points, I think
        # Add PS actor
        PosRen.AddActor(BTActor)
        AntRen.AddActor(BTActor)
        if args.plotRV: RenRV.AddActor(BTActor)
        #Update
        PosRen.Modified()
        AntRen.Modified()     
        if args.plotRV: RenRV.Modified()

    # Listening
    while True:           
        QOut = DataQueue.get(True)
        message,data = QOut
        if message=='kill': 
            print('Worker killed') # can be passed to logger to save 
            break
        elif message=='data':
            t,EndoVals,EpiVals = data
            tStep = args.tBegin+t*args.tInt
            # Put stuff in arrays
            vdataEndo.SetVoidArray(EndoVals, len(EndoVals), 1)
            EndoObject.GetPointData().SetScalars(vdataEndo)
            #
            vdataEpi.SetVoidArray(EpiVals, len(EpiVals), 1)
            EpiObject.GetPointData().SetScalars(vdataEpi)
            #
            TextActor.SetInput('%04d ms'%(tStep))
            TextActor.Modified()
            ## Endo
            EndoObject.GetPointData().Modified()
            EndoFiltered.Update()
            DataMapperEndo.Update()
            ## Epi
            EpiObject.GetPointData().Modified()				
            EpiFiltered.Update()
            DataMapperEpi.Update()

            if args.plotPS: 
                if tStep in PSpos.keys():
                    UpdatePSData(PSData,np.asarray(PSpos[tStep]))
                else:
                    UpdatePSData(PSData,np.empty((0,3)))

            if args.plotBT:
                btList = np.empty((0,3),float)
                for btTime in range(tStep-args.tInt,tStep+1):
                    if btTime in BTpos.keys():
                        btList = np.vstack([btList,np.array(BTpos[btTime])])
                btList = np.array(btList)
                UpdateBTData(BTGlyph,btList)
            # If defining a new angle is needed
            # renWin.SetOffScreenRendering(False)
            # interactor = vtk.vtkRenderWindowInteractor()
            # interactor.SetRenderWindow(renWin)
            # renWin.Render()
            # interactor.Start()
            # print(PosRen.GetActiveCamera().GetPosition())
            # print(PosRen.GetActiveCamera().GetFocalPoint())
            # print(PosRen.GetActiveCamera().GetViewUp())
            # print(PosRen.GetActiveCamera().Zoom())

            MakeScreenshot(renWin,args.expName.split('_vm')[0]+'%04d'%(t+args.initialCode),fpath =args.outputPath)

            if args.plotRV:
                TextActorRV.SetInput('%04d ms'%(tStep))
                TextActorRV.Modified()
                ## Endo
                DataMapperEndoRV.Update()
                ## Epi
                DataMapperEpiRV.Update()

                MakeScreenshot(renWinRV,args.expName.split('_vm')[0]+'RV%04d'%(t+args.initialCode),fpath =args.outputPath)

#%%
def main(args):
    ti = time.time()
    print('Initial time:%.3f'%ti)

    fname_cell = os.path.join(args.anatomyPath,args.anatomyFile)
    print(fname_cell)
    scale = args.scale

    ##### Get UnstructuredGrid from anatomy (Endocardium)
   
    cvalid = [args.endoVal] # Code for endocardium cells in this igb
    grid = IGBUnstructuredGrid(fname_cell,valid_cells=cvalid,units=scale)
    EndoGrid = grid.to_vtkUnstructuredGrid() 
    EndoGrid = dsa.WrapDataObject(EndoGrid)
    bpts  = EndoGrid.Points
    EndoIdx = np.rint(bpts/scale).astype('int') # Redundant
    ##### Get UnstructuredGrid from anatomy (Epicardium)

    cvalid = [args.epiVal] # Code for epicardium cells in this igb
    grid = IGBUnstructuredGrid(fname_cell,valid_cells=cvalid,units=scale)
    EpiGrid = grid.to_vtkUnstructuredGrid() 
    EpiGrid = dsa.WrapDataObject(EpiGrid)
    bpts  = EpiGrid.Points
    EpiIdx = np.rint(bpts/scale).astype('int')

    #### Filament trajectories (at the moment, does not consider the trajectories)
    if args.plotPS:
        fp = open(os.path.join(args.psPath,args.psFile), "rt")
        lines = fp.readlines()
        fp.close()
        PSpos = dict()
        for line in lines[1:]:
            f = line.split()
            (t,x,y,z,_) = (int(f[0]), float(f[1]), float(f[2]), float(f[3]),int(f[4]))
            if t in PSpos:
                PSpos[t].append([x,y,z])
            else:
                PSpos[t] = [[x,y,z]]
    else:
        PSpos=None
    # Breaktroughs
    if args.plotBT:
        fp = open(os.path.join(args.btPath,args.btFile), "rt")
        lines = fp.readlines()
        fp.close()
        BTpos = dict()
        for line in lines[2:-1]:
            f = line.split(',')
            (t,x,y,z,layer,vip) = (int(f[0]), int(f[1]), int(f[2]), int(f[3]),int(f[4]),int(f[5]))
            if t in BTpos:
                if args.onlyVIP and vip:
                    BTpos[t].append([x,y,z])
                elif ~args.onlyVIP:
                    BTpos[t].append([x,y,z])
            else:
                if args.onlyVIP and vip:
                    BTpos[t] = [[x,y,z]]
                elif ~args.onlyVIP:
                    BTpos[t] = [[x,y,z]]   
    else:
        BTpos=None       


    with mp.Manager() as DataManager:
        DataQueue = DataManager.Queue(maxsize=15) #maxsize=15 DO NOT COMMIT
        # [DataQueue.put(('initialize',None))for i in range(args.n_processes)]; - I might do this but I don't think it is that important
        
        #Data In
        DataReader = mp.Process(target = Reader,args = (args,EndoIdx,EpiIdx,DataQueue,))
        # Plotting
        PlottingPool = mp.Pool(args.n_processes,Plotter,(args,EndoGrid,EpiGrid,PSpos,BTpos,DataQueue,))

        # Open and wait for end 
        DataReader.start()
        DataReader.join()

        PlottingPool.close()
        PlottingPool.join()

    print('\n Whole process lasted:%.3f s'%(time.time()-ti))

#%%
if __name__ == '__main__':

    parser = ui.MyParser(description='Generated 3D visualizations of the heart. A sequence of\
                    pngs is generated and needs to be merged by ffmpeg or other method.',
                        fromfile_prefix_chars='+',
                        usage = 'python ParallelModelVisualization.py ANATOMY_FILE.igb EXPNAME_vm_afull.iga.gz +ModelVisualizationVTKFils.par')


    # Required arguments
    parser.add_argument('anatomyFile',action='store',type=str,
                        help = 'File containing the anatomy. Should be divided in endo and epicardial layers.')

    parser.add_argument('expName',action='store',type=str,
                        help = 'Name of the experiment file (.iga.gz extension)')

    # Optional Arguments
    ## Paths
    parser.add_argument('-dataPath','-dp',dest= 'dataPath',
                        action='store',default = '.',type=str,
                        help = 'Path to .iga file containing the data. Default \'.\'')
    parser.add_argument('-anatomyPath','-ap',dest= 'anatomyPath',
                        action='store',default = '.',type=str,
                        help = 'Path to the anatomy file.  Default \'.\'')

    parser.add_argument('-outputPath','-op',dest= 'outputPath',
                        action='store',default = './anims',type=str,
                        help = 'Path to the output folder. Default \'./anims\'')
    ## PS related
    parser.add_argument('--plotPS',action='store_true',
                        help = 'Whether to plot the PS\'s')
    parser.add_argument('-psPath','-psp',dest= 'psPath',
                        action='store',default = '.',type=str,
                        help = 'Path to the file containing the PS data.  Default \'.\'')
    parser.add_argument('-psFile',type=str,
                        action='store',default = '',
                        help = 'Name of the file containing the PS data (with extension, should be a text file).')
    parser.add_argument('-sphereRadius',type=float,
                        action='store',default = 10,
                        help = 'Radius of the PS spheres')
    ## BT related
    parser.add_argument('--plotBT',action='store_true',
                        help = 'Whether to plot the BT\'s')
    parser.add_argument('-btPath','-btp',dest= 'btPath',
                        action='store',default = '.',type=str,
                        help = 'Path to the file containing the BT data.  Default \'.\'')
    parser.add_argument('-btFile',type=str,
                        action='store',default = '',
                        help = 'Name of the file containing the BT data (with extension, should be a text file).')
    parser.add_argument('-coneSize',type=float,
                        action='store',default = 10,
                        help = 'Size of the BT cones')                        
    ## Frames
    parser.add_argument('-initialCode','-ic',dest= 'initialCode',
                        action='store',default =0,type=int,
                        help = 'Initial code for png output. Default 0')						
    parser.add_argument('-tBegin','-tb',dest= 'tBegin',
                        action='store',default =0,type=int,
                        help = 'Initial time (in frames). Default 0')
    parser.add_argument('-tEnd','-te',dest= 'tEnd',
                        action='store',default =-1,type=int,
                        help = 'Final time (in frames). Default -1')
    parser.add_argument('-tInt','-ti',dest= 'tInt',
                        action='store',default =10,type=int,
                        help = 'Time interval between frames. Default 10')

    # Anatomy related
    parser.add_argument('-endoVal',dest= 'endoVal',
                        action='store',default =50,type=int,
                        help = 'Cell code corresponding to the endocardial layer. Default 50')
    parser.add_argument('-epiVal',dest= 'epiVal',
                        action='store',default =52,type=int,
                        help = 'Cell code corresponding to the epicardial layer. Default 52')
    
    # Display related
    parser.add_argument('--plotRV',action='store_true',
                        help = 'Plots the right view in addition to the regular view')
    parser.add_argument('-scale','-s',dest= 'scale',
                        action='store',default =0.2,type=float,
                        help = 'Scale factor. Default 0.2')
    parser.add_argument('-windowType','-wt',dest= 'windowType',
                        action='store',default = 'side-by-side',type=str,
                        choices=['side-by-side','vertical','oneWindow'],
                        help = 'Path to the output folder. Default \'./anims\'')	
    parser.add_argument('-figWidth','-fw',action='store',type=int,default =1800,
                        help = 'Figure width in pixels. Default 1800') 
    parser.add_argument('-figHeight','-fh',action='store',type=int,default =800,
                        help = 'Figure width in pixels. Default 800') 	

    parser.add_argument('-cPosP',action='store',nargs='+',type=float,
                        default =[108.09430696880418, -76.1951818138545,-149.05382459124448],
                        help = 'Camera position (x,y,z) for posterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cFPP',action='store',nargs='+',type=float,
                        default =[54.693900645253095, 37.85250878678161, 40.10619050431344],
                        help = 'Camera focal point (x,y,z) for posterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cVUP',action='store',nargs='+',type=float,
                        default =[-0.22367346377168093, 0.043813444269235405, -0.9736788812055263],
                        help = 'Camera view up (x,y,z) for posterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cZoomP',action='store',type=float,
                        default =1.2,
                        help = 'Camera zoom for posterior camera. Defaults to model24 posterior view')

    parser.add_argument('-cPosA',action='store',nargs='+',type=float,
                        default =[107.51474741673346, 305.1080215893674, 45.847554586203245],
                        help = 'Camera position (x,y,z) for anterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cFPA',action='store',nargs='+',type=float,
                        default =[63.225564671242715, 39.61607918958433, 40.33986524809277],
                        help = 'Camera focal point (x,y,z) for anterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cVUA',action='store',nargs='+',type=float,
                        default =[-0.22367346377168093, 0.043813444269235405, -0.9736788812055263],
                        help = 'Camera view up (x,y,z) for anterior camera. Defaults to model24 posterior view')
    parser.add_argument('-cZoomA',action='store',type=float,
                        default =1.2,
                        help = 'Camera zoom for anterior camera. Defaults to model24 posterior view')

    parser.add_argument('-cPosR',action='store',nargs='+',type=float,
                        default =[-160.21175566775457, 89.54452626561813, 105.79946321976377],
                        help = 'Camera position (x,y,z) for right view camera. Defaults to model24 right view')
    parser.add_argument('-cFPR',action='store',nargs='+',type=float,
                        default =[52.067371495331464, 38.632232415124484, 42.66863918292802],
                        help = 'Camera focal point (x,y,z) for right view camera. Defaults to model24 right view')
    parser.add_argument('-cVUR',action='store',nargs='+',type=float,
                        default =[0.12537549694129713, 0.9348034735673706, -0.3323002416085507],
                        help = 'Camera view up (x,y,z) for right view camera. Defaults to model24 right view')
    parser.add_argument('-cZoomR',action='store',type=float,
                        default =1.4,
                        help = 'Camera zoom for right view camera. Defaults to model24 right view')
                        
    # Others
    parser.add_argument('-iga2igb',dest= 'iga2igb',
                    action='store',default = 'iga2igb',type=str,
                    help = 'Path to the iga2igb executable, if needed. Default no path')
    parser.add_argument('-n_processes',dest= 'n_processes',
                    action='store',default = 2,type=int,
                    help = 'Number of parallel processes to generate the images')
                    
                    
    args = parser.parse_args()
    args.cameraTypePos,args.cameraTypeAnt,args.CameraTypeRight = MakeCameraDicts(args)
    # Run function
    main(args)


	# From terminal:
	# python ParallelModelVisualization.py two-layers-model24-30-heart-cell.igb exp906c74_vm_afull.iga.gz +ModelVisualizationVTKFils.par 
#%%
'''
For debugging only
 
class options:
    def __init__(self,opt='linux'):
        if opt=='linux':
            self.anatomyFile ='two-layers-model24-30-heart-cell.igb.gz'
            self.expName = 'exp906c71_short.iga.gz'
            self.dataPath = '/home/vgmarques/data/model-data/debug'
            self.anatomyPath = '/home/vgmarques/data/model-data/anatomy/model30'
            self.outputPath= './tmp'

            self.plotPS=True
            self.psPath='/home/vgmarques/data/model-data/debug'
            self.psFile='exp906c71_pstracker_th0.txt'
            self.sphereRadius=6

            self.plotBT=True
            self.onlyVIP=True
            self.btPath='/home/vgmarques/data/model-data/debug'
            self.btFile='exp906c71_short.btd'
            self.starRadius=10

            self.initialCode=0
            self.tBegin=0
            self.tEnd=-1
            self.tInt=10

            self.endoVal=51
            self.epiVal=52
            self.plotRV = True
            self.scale=0.2

            self.windowType='vertical'
            self.figWidth=900
            self.figHeight=800

            self.cPosP=[129.7, -110.78, -136.58]
            self.cFPP=[-11.32, 188.61, 210.25]
            self.cVUP=[-0.07, 0.83, -0.56]
            self.cZoomP=1.3

            self.cPosA=[133.2, 296.09, 14.21]
            self.cFPA=[132.87, 295.06, 14.32]
            self.cVUA=[-0.29, -0.06, -0.95]
            self.cZoomA=1.4

            self.cPosR=[-184.37, 45.18, 49.25]
            self.cFPR=[59.9, 45.18, 49.25]
            self.cVUR=[0, 1, 0]
            self.cZoomR=0.9

            self.iga2igb='iga2igb'
            self.n_processes=5

args = options('linux')
args.cameraTypePos,args.cameraTypeAnt,args.CameraTypeRight = MakeCameraDicts(args)

main(args)
'''
