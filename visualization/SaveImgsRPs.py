# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 14:50:43 2021

Notes:
    - The difference between the "true" standard deviation of the meandering and
    the std. of the distance to the closest PS is usually relatively low (<30% of the "true" std)
    - This is mostly valid for close catheters, since those far usually have low difference (why?)
    - Block notation needed to be improved to include the corresponding trajectory
    - RPs will be generated based on the specific trajectories. Which also means the distances
    should be calculated based on these trajectories
    # An indicative of whether more PSs are close is important
    
    - Bounding box is better than std
    - Focus on trajectories, and on all catheters closer than 20mm or the 3 closest
    - Catheters with more than 1 nearby PS will show up multiple times, which can be used for the analyses
    - Number of trajectory should be included in output for traceback

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle5 as pickle
import scipy.signal as sig
from copy import deepcopy
import time
from scipy.linalg import svd  

from scipy.fft import fft2
import vtk




upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))
import quick_visualization as qv
from StandardVTKObjects import *
from RecurrenceComputation import RecurrenceComputation

#%% Functions
def MakeScreenshot(renWin,fname,fpath = './anims'):
	renWin.Render()	
	w2if = vtk.vtkWindowToImageFilter()
	w2if.SetInput(renWin)
	w2if.Update()
	writer = vtk.vtkPNGWriter()
	writer.SetFileName(os.path.join(fpath,fname+".png"))
	writer.SetInputConnection(w2if.GetOutputPort())
	writer.Write()

#%% 

# PSPATH = 'D:\\vgmar\\model_data\\exp909'#'6\\pstracker\\Control\\Filtered' #'D:\\vgmar\\model_data\\exp909'# 
PSPATH = 'D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered' #'D:\\vgmar\\model_data\\exp909'# 
# CATPATH='D:\\vgmar\\model_data\\exp909'#'6\electrode_data\\Control'
CATPATH='D:\\vgmar\\model_data\\exp906\\electrode_data\\Control'
# RECPATH='D:\\vgmar\\model_data\\exp909'#'6\\recurrence\\Control'
RECPATH='D:\\vgmar\\model_data\\exp906\\recurrence\\Control'

IMGPATH = 'C:\\Users\\vgmar\\Imagens\\model_imgs'

SampleShift = 5
fs = 1000
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1

experimentNames = []
for file in os.listdir(PSPATH):
    # if file.startswith('exp909b') and file.endswith('.catG'):
    if file.startswith('exp906c') and file.endswith('.txt'):
        experimentNames.append(file.split('_')[0])
experimentNames = np.sort(experimentNames)


for experiment in experimentNames[8:9]:
    expFolder = os.path.join(IMGPATH,experiment)
    try:
        os.mkdir(expFolder)
    except FileExistsError:
        print('Folder exists')

    # InputDF = pd.read_csv(os.path.join(PSPATH,experiment+'_PSPositionsAndDistances.csv'),index_col=0)
    
    # Blocks = pickle.load(open(os.path.join(PSPATH,'Exp906_LongSourceBlocks.pkl'),'rb'))[experiment]
    # Blocks = pickle.load(open(os.path.join(PSPATH,'Exp909_LongSourceBlocks.pkl'),'rb'))[experiment]
    
    # relevantTrajs = list(Blocks.keys())
    
    CGroup = pickle.load(open(os.path.join(CATPATH,experiment+'_CatheterData.catG'),'rb'))
    fs = CGroup.Catheters['RPV'].SamplingFrequency
    
    ## Calculate global distance matrix for catheter
    with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
        DistanceMatrices =  pickle.load(output)
        EstimatedAFCLs =  pickle.load(output)
    #### Quantify distances and meandering
    
    #% Tests with the meandering 

    # for bId in range(len(relevantTrajs)):
    # Positions = InputDF.loc[InputDF['traj']==relevantTrajs[bId],:]
    
    # medPos = [np.median(Positions.loc[Positions['t']==t,:],axis=0) for t in np.unique(Positions['t'])]
    # medPos = pd.DataFrame(medPos,columns = Positions.columns)
    # U,s,V = svd(np.asarray(medPos[['x','y','z']]))

    
    # planePos =  np.dot(medPos[['x','y','z']],V[:,:2])
    # BoundingBoxArea = (np.percentile(planePos[:,0],100)-np.percentile(planePos[:,0],0))* \
    #                   (np.percentile(planePos[:,1],100)-np.percentile(planePos[:,1],0))
    
    # description = medPos.describe()
    # distMEAN = np.asarray(description[1:2]).flatten()[5:] # Get mean
    # closestTriangle = np.isin(np.arange(len(distMEAN)),np.argsort(distMEAN)[:3])
    # distanceTh = distMEAN<=20
    
    # inds = np.where(closestTriangle|distanceTh)[0]
    # closestCatheters = np.asarray(list(DistanceMatrices.keys()))[closestTriangle|distanceTh]
    
    for i,cat in enumerate(DistanceMatrices.keys()):
        estimatedAFCL = EstimatedAFCLs[cat]
        minLineLength = (estimatedAFCL*fs)//SampleShift
        
        downsampledBlock = [2500//SampleShift,30000//SampleShift]#Blocks[relevantTrajs[bId]]//SampleShift
        
        partDM = DistanceMatrices[cat][downsampledBlock[0]:downsampledBlock[1],
                                         downsampledBlock[0]:downsampledBlock[1]]
        
        RP, recurrenceTime, recurrenceThreshold, partDM = \
                    recComp.ComputeRecurrencePlot(partDM, fs, estimatedAFCL,recurrenceThreshold= 0.15)
        recurrenceTime= recurrenceTime+2.5
        
        RPE = recComp.ErodeRP(RP,partDM,linear=True)
        # trajLims = [2500,30000]#Blocks[relevantTrajs[bId]]

        fi,ax = plt.subplots(1,figsize = [11.25,10.25])
        xi,yi = np.where(RPE)
        ax.scatter(recurrenceTime[xi],recurrenceTime[yi],color='k',s=.005)
        # ax.imshow(RP,origin='lower',cmap = 'gray_r',extent = [trajLims[0],trajLims[1],trajLims[0],trajLims[1]])
        # ax.set_title(r'%s: duration %d ms, BBoxArea %0.2f $mm^2$'%(cat,medPos['t'].max()-medPos['t'].min(),BoundingBoxArea))
        ax.set_xlabel('Time (s)',fontsize = 18)
        ax.set_ylabel('Time (s)',fontsize = 18)
        ax.tick_params(labelsize=18)
        ax.set_xticks(recurrenceTime[::1000])
        ax.set_yticks(recurrenceTime[::1000])
        ax.set_xlim([recurrenceTime[0],recurrenceTime[-1]])
        ax.set_ylim([recurrenceTime[0],recurrenceTime[-1]])
        fi.tight_layout()
        fi.savefig(os.path.join(expFolder,'%s_%s_th15.png'%(experiment,cat)),dpi=500)
        plt.close()
                            
                            

#%%
# Check the position of the trajectory we are looking at
TrajectoryPositions= np.asarray(medPos[['x','y','z']])

AnatomyActor,_ = qv.MakePolyDataActor(CGroup.Anatomy,np.zeros(len(CGroup.Anatomy)).reshape(1,-1),
                                      vmin = 0,vmax = 10,cmap='heatmap')
ActorList = [AnatomyActor]
for cat in closestCatheters:
    Cat = CGroup.Catheters[cat]
    CatActor,_ = qv.MakePSActors(CGroup.Anatomy[Cat.CatheterIdx,:],1,sphereColor = (1,0,1))
    CatActor.GetProperty().SetPointSize(20)
    ActorList.append(CatActor)

TrajActor,cbarActor = qv.MakePolyDataActor(TrajectoryPositions,
                                           np.asarray(medPos['t']).reshape(1,-1),
                                           cmap='jet')
ActorList.append(TrajActor)

postRen = StandardViewRenderer(ActorList,'Posterior',colorbarActor = None,textActor = None,viewportSplit = 'horizontal-left')
antRen = StandardViewRenderer(ActorList,'Anterior',colorbarActor = cbarActor,textActor = None,viewportSplit = 'horizontal-right')
renWin = TwoViewsRenderWindow(postRen,antRen,windowSize = [1800,900],OffScreenRendering = False)


# renWin = qv.QuickRenderWindowInteractor(ActorList,[cbarActor],Interact=False)
 
MakeScreenshot(renWin,'test.png',fpath = '.')

#%% Tests with frequency

DM = RP.astype(np.float64)#[:10000,:10000]
DM[np.isnan(DM)]=0
fsi = fs/recComp.SampleShift


img_c2 = np.fft.fft2(DM,(2048,2048))
img_c3 = np.fft.fftshift(img_c2)
img_c3 = np.abs(img_c3)#[int(img_c3.shape[0]/2):,int(img_c3.shape[0]/2):]

freqs = np.linspace(-fsi/2,+fsi/2,img_c3.shape[0])

fig,ax = plt.subplots(1,figsize = (10,10))
ax.imshow(img_c3,cmap='gray',extent=[freqs[0],freqs[-1],freqs[0],freqs[-1]])
# plt.scatter(1/estimatedAFCL,1/estimatedAFCL,marker= 'o',facecolor = 'None',edgecolor='r')
ax.set_xlim([0,20])
ax.set_ylim([0,20])
ax.set_xlabel('Frequency (Hz)',fontsize = 20)
ax.set_ylabel('Frequency (Hz)',fontsize = 20)
ax.tick_params(labelsize=18)



plotFreqs = freqs#[int(img_c3.shape[0]/2):]
AFCL_ind = np.abs(plotFreqs-1/estimatedAFCL)
AFCL_ind = np.where(AFCL_ind==AFCL_ind.min())[0][0]

plotSig = img_c3#[int(img_c3.shape[0]/2):,int(img_c3.shape[0]/2):]
plotSig = [np.sum(plotSig[:,i]) for i in range(plotSig.shape[0])]

fig,ax = plt.subplots(1,figsize=(12,8))
ax.plot(plotFreqs,plotSig)
ax.axvline(plotFreqs[AFCL_ind],color = 'r',linestyle = '--',label='1/AFCL')
ax.set_xlim([0,20])
ax.set_xlabel('Frequency (Hz)',fontsize = 20)
ax.set_ylabel('Power',fontsize = 20)
ax.tick_params(labelsize=18)
fig.legend(loc=1,fontsize=18)
