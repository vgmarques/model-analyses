#!/bin/sh
#SBATCH --job-name="test_paraview2"
#SBATCH --account="s1074"
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl


########################################################################################################
#Run this script in jupyter.cscs.ch
#The script has not been adjusted to run directly from the terminal due to problems with the X server
########################################################################################################

# Only needed for daint, could be ommited if running somewhere where the python vtk module is installed
module load daint-gpu
module load cray-python

ANATOMY=two-layers-model24-30-heart-cell.igb
BASENAME=test_EX0005_C0_P
ID=00

EXPNAME=${BASENAME}${ID}

INPATHECG=/users/vgonalve/model-analyses/ecg-analysis
WD=/users/vgonalve/model-analyses/visualization
OUTPATH3D=/users/vgonalve/exec/anims
OUTPATH2D=/users/vgonalve/exec/anims/ecg

VIDEONAME=${EXPNAME}_w_ecg

PSFILE=${EXPNAME}_pstracker_th100.txt

export DISPLAY=:$ID
# killall Xvfb
Xvfb :$ID -screen 0 1600x1200x24 &


#3D animation
# srun -C gpu -n1 -c24 -A 's1074' --time=01:30:00 \
python ParallelModelVisualization.py ${ANATOMY} ${EXPNAME}_vm_afull.iga.gz  +ModelVisualizationVTKFils.par 
#--plotPS -psFile=${PSFILE}

# # #2D animation - make ECG and animation
# # TODO: run 30s for all videos
# cd $INPATHECG # <--------
# python convert_ecg.py +ConvertECG.par $BASENAME $ID # <--------

# cd $WD
# # srun -C gpu -n1 -c1 -A 's1074' --time=00:30:00 \
# python LeadPlotter.py +LeadPlotter.par $EXPNAME # <--------

killall Xvfb

# # Make video
cd $OUTPATH3D
#Adjust -start_number as needed
~/ffmpeg/ffmpeg -i ${EXPNAME}%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}A.mp4

# cd $OUTPATH2D
# ~/ffmpeg/ffmpeg -i ${EXPNAME}_Leads_%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}B.mp4 #<-----------

# rm ${OUTPATH3D}/${EXPNAME}_ecg_leadfields.mat
# # Join videos -  maybe we don't need to get a node for that. Also, better done outside daint
# #srun -C gpu -n1 -c1 -A 's778' --time=00:30:00 \
# ~/ffmpeg/ffmpeg -i ${VIDEONAME}A.mp4 -i ${VIDEONAME}B.mp4 -filter_complex hstack ${VIDEONAME}.mp4

# Clean (Better do manually in daint to avoid deleting things before making sure everything is ok)
#rm ${VIDEONAME}A.mp4
#rm ${VIDEONAME}B.mp4
#find -maxdepth 1 -type f -name ${EXPNAME}'*.png' -delete
#find -maxdepth 1 -type f -name ${ECGFILE}'*.png' -delete

#ffmpeg -i exp906c05_w_ecgA.mp4 -i exp906c05_w_ecgB.mp4 -filter_complex hstack exp906c05_w_ecg.mp4

