import os
import sys
import argparse

import numpy as np
import matplotlib.pyplot as plt
import scipy.io as sio

#sys.path.append('../fibrosis')
#from pyccmc import igb_read

class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def SetPlotElements(args):
    LeadConfigurations = {'ecg12':(6,2),
                          'standard-disp':(8,1),
                          'leadfields-standard':(8,1),
                          'leadfields-ecg12':(6,2)}

    SubplotLabels = {'ecg12':['I','II','III','aVR','aVL','aVF','V1','V2','V3','V4','V5','V6'],
                     'standard-disp':['Eso 5','I','II','V1','V3','V5','V6','D5'],
                     'leadfields-standard':['I','II','III','V1','V3','V5','V6','D5'],
                     'leadfields-ecg12':['I','V1','II','V2','III','V3','aVR','V4','aVL','V5','aVF','V6']}

    SubplotLayout = LeadConfigurations[args.leadConfig]
    Fig, Ax = plt.subplots(SubplotLayout[0],SubplotLayout[1],
                            figsize = [args.figWidth/args.dpi,args.figHeight/args.dpi]) 
    if len(Ax.shape)==1:
        for i,ax in enumerate(Ax[:-1]):

            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_linewidth(2)
            ax.tick_params(labelsize = 10)
            ax.set_xticks([])
            ax.set_yticks([0])
            ax.set_yticklabels([''])
            ax.set_ylabel(SubplotLabels[args.leadConfig][i],rotation = 0,labelpad=16)
            #AX.grid(1,which = 'major',axis = 'both')

        if args.showTimeAxis:
            # Time axis
            Ax[-1].spines['right'].set_visible(False)
            Ax[-1].spines['top'].set_visible(False)
            Ax[-1].spines['left'].set_linewidth(2)
            Ax[-1].spines['bottom'].set_linewidth(2)
            Ax[-1].set_yticks([0])
            Ax[-1].set_yticklabels([''])
            Ax[-1].tick_params(labelsize = 10) 
            Ax[-1].set_xlabel('Time (s)') 
            Ax[-1].set_ylabel(SubplotLabels[args.leadConfig][-1],rotation = 0,labelpad=16) 
        else:
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_linewidth(2)
            ax.tick_params(labelsize = 10)
            ax.set_xticks([])
            ax.set_yticks([0])
            ax.set_yticklabels([''])
            ax.set_ylabel(SubplotLabels[args.leadConfig][-1],rotation = 0,labelpad=16)

    else:
        k = 0
        for i,j in np.ndindex(Ax[:-1,:].shape):
            ax = Ax[i,j]
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['bottom'].set_visible(False)
            ax.spines['left'].set_linewidth(2)
            ax.tick_params(labelsize = 10)
            ax.set_xticks([])
            ax.set_yticks([0])
            ax.set_yticklabels([''])
            ax.set_ylabel(SubplotLabels[args.leadConfig][k],rotation = 0,labelpad=16)
            k = k+1
        
        # Time axis
        if args.showTimeAxis:
            for i in range(2):
                Ax[-1,i].spines['right'].set_visible(False)
                Ax[-1,i].spines['top'].set_visible(False)
                Ax[-1,i].spines['left'].set_linewidth(2)
                Ax[-1,i].spines['bottom'].set_linewidth(2)
                Ax[-1,i].set_yticks([0])
                Ax[-1,i].set_yticklabels([''])

                Ax[-1,i].tick_params(labelsize = 10) 
                Ax[-1,i].set_xlabel('Time (s)') 
                Ax[-1,i].set_ylabel(SubplotLabels[args.leadConfig][k+i],rotation = 0,labelpad=16)
        else:
            for i in range(2):
                ax = Ax[-1,i]
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.spines['bottom'].set_visible(False)
                ax.spines['left'].set_linewidth(2)
                ax.tick_params(labelsize = 10)
                #ax.set_xticks([])
                ax.set_yticks([0])
                ax.set_yticklabels([''])
                ax.set_ylabel(SubplotLabels[args.leadConfig][k+i],rotation = 0,labelpad=16)

    return Fig,Ax

def GetLeads(mbsm,bsm65,probe,layout):
    PreconfiguredLeadGroups = {'ecg12':{'I':  bsm65[:,62]-bsm65[:,63],
                                        'II': bsm65[:,64]-bsm65[:,63],
                                        'III':bsm65[:,64]-bsm65[:,62],
                                        'aVR':bsm65[:,63]-0.5*(bsm65[:,62]+bsm65[:,64]),
                                        'aVL':bsm65[:,62]-0.5*(bsm65[:,63]+bsm65[:,64]),
                                        'aVF':bsm65[:,64]-0.5*(bsm65[:,62]+bsm65[:,63]),
                                        'V1': bsm65[:,11],
                                        'V2': bsm65[:,17],
                                        'V3': bsm65[:,24],
                                        'V4': bsm65[:,30],
                                        'V5': bsm65[:,40],
                                        'V6': bsm65[:,44]
                                        },
                                'standard-disp':{'Eso 5': probe[:,99],
                                                    'I': bsm65[:,62]-bsm65[:,63],
                                                    'II':bsm65[:,64]-bsm65[:,63],
                                                    'V1':bsm65[:,11],
                                                    'V3':bsm65[:,24],
                                                    'V5':bsm65[:,40],
                                                    'V6':bsm65[:,44],
                                                    'D5':mbsm[:,92]
                                                    },
                                'leadfields-standard':{'I': mbsm[:,62]-mbsm[:,63],
                                                       'II':mbsm[:,64]-mbsm[:,63],
                                                       'III':mbsm[:,64]-mbsm[:,62],
                                                       'V1':mbsm[:,11],
                                                       'V3':mbsm[:,24],
                                                       'V5':mbsm[:,40],
                                                       'V6':mbsm[:,44],
                                                       'D5':mbsm[:,92]
                                                       },
                                'leadfields-ecg12':{'I':  mbsm[:,62]-mbsm[:,63],
                                                    'V1': mbsm[:,11],
                                                    'II': mbsm[:,64]-mbsm[:,63],
                                                    'V2': mbsm[:,17],
                                                    'III':mbsm[:,64]-mbsm[:,62],
                                                    'V3': mbsm[:,24],
                                                    'aVR':mbsm[:,63]-0.5*(mbsm[:,62]+mbsm[:,64]),
                                                    'V4': mbsm[:,30],
                                                    'aVL':mbsm[:,62]-0.5*(mbsm[:,63]+mbsm[:,64]),
                                                    'V5': mbsm[:,40],
                                                    'aVF':mbsm[:,64]-0.5*(mbsm[:,62]+mbsm[:,63]),
                                                    'V6': mbsm[:,44]
                                                    }                               
                                }

    return PreconfiguredLeadGroups[layout]




#def animateSignals(args):


def main(args):
    
    fs = args.samplingFrequency
    s2ms = 1000 # factor for converting seconds to milliseconds
    
    # Generate time axis and convert ms to frames
    initialFrame=   int(args.tBegin*s2ms/fs)
    finalFrame =    int(args.tEnd*s2ms/fs)
    frameInterval = int(args.tInt*s2ms/fs)

    print('Animating from %d to %d ms with %d ms intervals'%(args.tBegin,args.tEnd,args.tInt))

    # Create figure with the specified subplot configuration
    Fig,Ax = SetPlotElements(args)

    # Get Signals
    if args.option=='leadfields':
        dataInput = sio.loadmat(os.path.join(args.dataPath,args.expName+'_ecg_leadfields.mat'))
        mbsm = dataInput['bsm']
        bsm65 = np.zeros((2,65))
        probe = np.zeros((2,272))
    else:
        dataInput = sio.loadmat(os.path.join(args.dataPath,args.expName+'_ve.mat'))
        mbsm = dataInput['mbsm']
        bsm65 = dataInput['bsm65']
        probe = dataInput['probe']

    time = np.linspace(0,mbsm.shape[0]/fs,mbsm.shape[0])

    Leads = GetLeads(mbsm,bsm65,probe,args.leadConfig)

    # Plot the desired lines in gray
    maxVal = 0
    minVal = 0
    BlackLines = []
    for i,key in enumerate(Leads):
        if len(Ax.shape)==1:
            active_ax = Ax[i]
        else:
            active_ax = Ax[i//2,i%2]

        active_ax.plot(time[initialFrame:finalFrame],Leads[key][initialFrame:finalFrame],c = 'lightgrey')
        BlackLines.append(active_ax.plot(time[initialFrame],Leads[key][initialFrame],c = 'k')[0]) # Initialize the black plot which will overlap the first one

        maxVal = np.max([maxVal,Leads[key].max()])
        minVal = np.min([minVal,Leads[key].min()])
        
        #Symmetric axes
        lim = np.max([abs(Leads[key].min()),abs(Leads[key].max())])
        active_ax.set_ylim([-lim,lim])


        
    if args.normalizeAmplitude:
        lim = np.max([abs(minVal),abs(maxVal)])
        [active_ax.set_ylim([-lim,lim]) for active_ax in Ax]

    # Animate the black line in the frame interval determined


    if args.tEnd==-1:
        args.tEnd = Leads[key].shape[0]

    timeInterval = np.arange(initialFrame,finalFrame,frameInterval,dtype = int)
    for frame,tStep in enumerate(timeInterval):
        [BlackLines[i].set_data(time[:tStep], Leads[key][:tStep]) for i,key in enumerate(Leads)]
        Fig.canvas.draw()
        Fig.savefig(os.path.join(args.outputPath,args.expName+'_Leads_%04d.png'%frame))



if __name__ == '__main__':

    parser = MyParser(description='Python implementation of functions to plot individual leads in\
                    any desired combination.',
                    usage='python LeadPlotter.py [OPTIONS] expName',
                    fromfile_prefix_chars='+',
                    epilog='To add new lead configurations, please edit the following dictionaries (it is very intuitive):\
                         PreconfiguredLeadGroups, LeadConfigurations, SubplotLabels')


    # Required arguments

    parser.add_argument('expName',action='store',type=str,
                        help = 'Name of the experiment in the format exp***X##. Use convert_ecg.py before \
                        running to generatethe input file')


    # Optional arguments
    # Related to timing
    parser.add_argument('-tBegin','-tb',dest= 'tBegin',action='store',default =0,
                        type=int,help = 'Initial time (in ms). Default 0')
    parser.add_argument('-tEnd','-te',dest= 'tEnd',action='store',default =-1,
                        type=int,help = 'Final time (in ms). Default -1')
    parser.add_argument('-tInt','-ti',dest= 'tInt',action='store',default =10,
                        type=int,help = 'Time interval between frames (in ms). Default 10')
    parser.add_argument('-samplingFrequency',action='store',type=float,default = 1000,
                        help = 'Sampling frequency to determine frame interval in ms')
    
  
    # Paths and files
    parser.add_argument('-dataPath','-dp',dest= 'dataPath',
                        action='store',type=str,default ='.',
                        help = 'Path to the input files')   
    parser.add_argument('-outputPath','-op',dest= 'outputPath',
                        action='store',type=str,default ='./anims',
                        help = 'Path to the output files') 
    parser.add_argument('-option',action='store',type=str,default ='forward-solution',
                        choices = ['forward-solution','leadfields'],
                        help = 'Path to the files')  

    # Related to the appearance    
    parser.add_argument('-leadConfig',action='store',type=str,default = 'standard-disp',
                        choices=['ecg12','standard-disp','leadfields-standard','leadfields-ecg12'],
                        help = 'Define which leads will be plotted. Default is standard-disp')
    parser.add_argument('-figWidth','-fw',action='store',type=int,default =1800,
                        help = 'Figure width in pixels. Default 1800') 
    parser.add_argument('-figHeight','-fh',action='store',type=int,default =800,
                        help = 'Figure width in pixels. Default 800') 	
    parser.add_argument('-dpi',action='store',type=int,default =100,
                        help = 'Monitor\'s dots per inch. Default 100') 
    parser.add_argument('-normalizeAmplitude',action='store',type=bool,default =False,
                        help = 'Normalize the amplitude of all shown leads. Default False') 
    parser.add_argument('-showTimeAxis',action='store',type=bool,default =True,
                        help = 'Show time axis. Default True') 
    
    args = parser.parse_args()

    main(args)