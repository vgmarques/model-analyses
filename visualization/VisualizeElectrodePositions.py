# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 10:35:18 2021

DESCRIPTION: Visualization of catheters and atrial surface

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import sys
import os
import numpy as np
import argparse

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'electrode-data'))
import IgbHandling as igb
from igbhexagrid import IGBUnstructuredGrid  

import GetCatheterData as ged
from StandardVTKObjects import *

import vtk
from vtkmodules.numpy_interface import dataset_adapter as dsa

#%%
class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)
        
def MakeScreenshot(renWin,fname,fpath = './anims'):
	renWin.Render()	
	w2if = vtk.vtkWindowToImageFilter()
	w2if.SetInput(renWin)
	w2if.Update()
	writer = vtk.vtkPNGWriter()
	writer.SetFileName(os.path.join(fpath,fname+".png"))
	writer.SetInputConnection(w2if.GetOutputPort())
	writer.Write()

    
def InitializeRenderingElements(Object,SphereActors,LineActors,args):
    # Mappers
    DataMapperEndo = EGMAtrialMapper(Object,10, amplitudeRange = [0,10])
    # Actors
    DataActorEndo = StandardLayerActor(DataMapperEndo,opacity = 0.2)
    #
    # Renderers
    PosRen = StandardViewRenderer([DataActorEndo],
								cameraType = 'egmPosterior',
								colorbarActor = None,
								textActor = None,
								viewportSplit = 'half-left')
    PosRen.GetActiveCamera().Zoom(0.8)
    #
    AntRen = StandardViewRenderer([DataActorEndo],
								cameraType = 'egmAnterior',
								colorbarActor = None,
								textActor = None,
								viewportSplit = 'half-right')
    AntRen.GetActiveCamera().Zoom(0.8)
    #
    for i in range(len(SphereActors)):
        [PosRen.AddActor(SphereActor) for SphereActor in SphereActors[i]];
        [PosRen.AddActor(LineActor) for LineActor in LineActors[i]];
        #
        [AntRen.AddActor(SphereActor) for SphereActor in SphereActors[i]];
        [AntRen.AddActor(LineActor) for LineActor in LineActors[i]];       
    #
    #RenderWindow
    renWin = TwoViewsRenderWindow(PosRen,AntRen,windowSize =  [args.figWidth,args.figHeight],
                                  OffScreenRendering = False)
    #
    return renWin,PosRen,AntRen,DataActorEndo,DataMapperEndo


def PlaceCatheterVTK(Catheter,splineColor,sphereRadius=3):
    try:
        CatheterCoords= Catheter.CatheterCoordinates
    except:
        CatheterCoords= Catheter.ElectrodeCoordinates
    CatheterSplines = Catheter.PlotSplines
    #
    SphereActors = list()
    SphereMappers = list()
    #
    for i in range(CatheterCoords.shape[0]):
        # Sources
        Sphere = vtk.vtkSphereSource()
        Sphere.SetCenter(CatheterCoords[i,:])
        Sphere.SetRadius(sphereRadius)
        # Mapper
        SphereMappers.append(vtk.vtkPolyDataMapper())
        SphereMappers[i].SetInputConnection(Sphere.GetOutputPort())
        # Actor
        SphereActors.append(vtk.vtkActor())
        SphereActors[i].SetMapper(SphereMappers[i])
        if i==0 or i==3:
            SphereActors[i].GetProperty().SetColor([1,0,0])
            SphereActors[i].GetProperty().SetOpacity(0.7)
    #####
    LineMappers = list()
    LineActors = list()              
    #
    for i in range(len(CatheterSplines)):
        for j in range(len(CatheterSplines[i])):
            ## Line sources
            Line = vtk.vtkLineSource()
            Line.SetPoint1(CatheterCoords[CatheterSplines[i][j-1],:])
            Line.SetPoint2(CatheterCoords[CatheterSplines[i][j],:])
            # Make tube
            tubeFilter = vtk.vtkTubeFilter()
            tubeFilter.SetInputConnection(Line.GetOutputPort())
            tubeFilter.SetRadius(0.6*sphereRadius)
            tubeFilter.SetNumberOfSides(50)
            tubeFilter.Update()
            # Mapper
            LineMappers.append(vtk.vtkPolyDataMapper())
            LineMappers[-1].SetInputConnection(tubeFilter.GetOutputPort())
            # Actor
            LineActors.append(vtk.vtkActor())
            LineActors[-1].SetMapper(LineMappers[-1])
            LineActors[-1].GetProperty().SetColor(splineColor)
    #
    return SphereActors,LineActors

#%%
def main(args):
    
    print('Loading anatomy...')
    # Load the igb with the cell positions
    FullCell = igb.LoadCellFile(args.anatomyPath,args.modelName,args.validCells)
    Anatomy,veIndexes = igb.GetDataIndexes(args.anatomyPath,args.modelName,FullCell,validCells =args.validCells)
    scale = 1 # mm

    print('Done')

    # Initialize Catheter
    print('Placing catheter...')
    
    CatheterGroup = ged.main(args,onlyPositions=True)

    ######################################
    print('VTK portion started...')
    #### VTK portion
    grid = IGBUnstructuredGrid('None',valid_cells=[50,51],units=args.scale)
    grid.cell = FullCell

    Grid = grid.to_vtkUnstructuredGrid() 
    Grid = dsa.WrapDataObject(Grid)
    bpts  = Grid.Cells # This is different from the IGA version
    Idx = np.rint(bpts/args.scale).astype(int)
    
    Vdata = vtk.vtkFloatArray()
    Vdata.SetName("egm")
    Vdata.SetNumberOfComponents(1)
    Vdata.SetNumberOfTuples(Idx.shape[0])
    
    Grid.GetCellData().AddArray(Vdata) # We get cells instead of nodes here
    Object = Grid.VTKObject    

    vals = np.ones(int(bpts.shape[0]/9))
    Vdata.SetVoidArray(vals, len(vals), 1)
    Object.GetCellData().SetScalars(Vdata)
    
    
    ######################################
    colors = np.array([[0,0,0],[237,149,192],
                      [216,13,145], [111,49,145],
                      [47,118,185], [24,185,240],
                      [37,181,136], [35,177,76],
                      [203,219,66],[246,218,62],
                      [237,161,58], [198,107,51],
                      [137,32,40],[255,0,0],
                      [255,255,255],[0,255,0]])/255

    # Initialize Render Objects
    SphereActors = list()
    LineActors = list()
    for i,key in enumerate(CatheterGroup.Catheters):
        catheter = CatheterGroup.Catheters[key]
        Sphere,Line = PlaceCatheterVTK(catheter,colors[i,:],sphereRadius=1)
        SphereActors.append(Sphere)
        LineActors.append(Line)

    Filter = SmoothFilters(Object,numberOfIterations = 20, relaxationFactor = 0.2)

    renWin,PosRen,AntRen,DataActorEndo,DataMapper = \
    InitializeRenderingElements(Filter.GetOutput(),SphereActors,LineActors,args)

    # Screenshot
    #MakeScreenshot(renWin,args.expName[:10]+'_Catheters.png',fpath =args.outputPath)

    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(renWin)
    renWin.Render()
    interactor.Start()

#%%    
if __name__ == '__main__':
    
    # Parse args
    
    parser = MyParser(description='Generated 3D visualizations of the catheter on the heart.\
                      At the moment, only one frame is generated. NOTE: the way in which I plot is outdated,\
                          since it does not use Glyphs. But it works fine and consistent with the changes I usually make',
                      fromfile_prefix_chars='+',
                      usage = 'python VisualizeElectrodePositions.py +PARAMETERS.par modelName')

	# Required arguments

    parser.add_argument('modelName',action='store',default = 'model24-30',type=str,
						help = 'Name of the model')
    parser.add_argument('-catheterModel',action='store',default = 'HDGrid-4',type=str,
						help = 'Name of the catheter model')

    parser.add_argument('-validCells','-vc', nargs='+', type=int,default = [50,51],
                        help='Cell types from which data will be read', required=True)
    
	# Optional Arguments
    parser.add_argument('-catheterNames',nargs='+',type=str,
                        default = [],
                        help='Name of the catheters to be positioned.\
                            Default is ...')
    
    parser.add_argument('-catheterCenters',nargs='+',type=int,
                        default = [],
                        help='Linear indexes of the catheter centers with respect\
                            to the anatomy file.\
                            Default is ...')                            
    parser.add_argument('-catheterRotations',nargs='+',type=float,
                        default = [],
                        help='Linear indexes of the catheter centers with respect\
                            to the anatomy file.\
                            Default is ...')                            
    
    
    # File IO

    parser.add_argument('-anatomyPath','-ap',dest= 'anatomyPath',
						action='store',default = '.',type=str,
						help = 'Path to the anatomy file.  Default \'.\'')

    parser.add_argument('-outputPath','-op',dest= 'outputPath',
						action='store',default = './anims',type=str,
						help = 'Path to the output folder. Default \'./anims\'')
    
    # Visuals
    parser.add_argument('-scale','-s',dest= 'scale',
						action='store',default =0.2,type=np.float,
						help = 'Scale factor. Default 0.2')

    parser.add_argument('-windowType','-wt',dest= 'windowType',
						action='store',default = 'side-by-side',type=str,
						choices=['side-by-side','vertical'],
						help = 'Path to the output folder. Default \'./anims\'')	

    parser.add_argument('-figWidth','-fw',action='store',type=int,default =1800,
						help = 'Figure width in pixels. Default 1800') 
    parser.add_argument('-figHeight','-fh',action='store',type=int,default =800,
						help = 'Figure width in pixels. Default 800') 	

    args = parser.parse_args()
    
    if type(args.validCells)!=list:
        args.validCells= [args.validCells]
    args.onlyPositions=True
        

    main(args)
    
    '''Only for use in spyder
    import os 
class args:
    def a():
        a=0
args = args
args.dataPath='/mnt/d/vgmar/model_data/exp906' 
args.anatomyPath='/mnt/d/vgmar/model_data/anatomy/'#'D:\\vgmar\\Documents\\MEGAsync\\PhD\\Data\\Models\\anatomy' 
args.outputPath='.\\anims' 
args.modelName = 'model24-30Box'    
args.figWidth = 1600
args.figHeight = 800
args.initialCode = 0
args.catheterNames = ['RPV','LPV','PLA','ILA_1','ILA_2','ILA_3','ALA','LA_Roof','LA_AS','LAA','SRA','IRA','RAA','PECT','CSO']
args.catheterCenters = [1665,2206,1023,12831,11575,7814,13750,4438,8481,13886,6107,14827,20088,22929,16076]
args.catheterRotations = [3.141592653589793,2.356194490192345,3.141592653589793,1.5707963267948966,3.141592653589793,3.141592653589793,-0.7853981633974483,0.5235987755982988,3.141592653589793,3.141592653589793,3.141592653589793,0,0.7853981633974483,3.141592653589793,1.5707963267948966]
args.outputFile = None
args.catheterModel = 'HDGrid-4'
args.outputMatlab=False
    '''