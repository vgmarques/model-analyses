#!/bin/sh
#
#SBATCH --job-name=jobName
#SBATCH --time=requestedHours
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=24
#SBATCH --signal=TERM@5
#SBATCH --mail-type=ALL
#SBATCH --dependency=singleton
#SBATCH --constraint=gpu
#SBATCH --account=s1074


########################################################################################################
# Run this script with sbatch
########################################################################################################

# Only needed for daint, could be ommited if running somewhere where the python vtk module is installed
module load daint-gpu
module load cray-python
module load matplotlib

################### Experiment Parameters ####################

EXPNAME=experimentName
VIDEONAME=videoName

DATAPATH=DataPath #/users/vgonalve/exec/exp906 #/users/vgonalve/exec/EX0008 /project/s1074/LongAF/PVI ## datapath 


####### Timing, computation, and visualization parameters #######
TBEGIN=timeBegin
TEND=timeEnd
TINT=timeInt


FIGWIDTH=900
FIGHEIGHT=800

## 3D visualization parameters explanation
# tBegin        = starting timestep
# tEnd          = final timestep
# tInt          = timestep interval
# sphereRadius  = size of the phase singularity sphere
# iga2igb       = location of the iga2igb software
# windowType    = determines how the 3D shapes are organized. Options are 'vertical', 'side-by-side' or 'oneWindow'
# figWidth and figHeight = size of the output image
# n_processes   = Number of parallel processes. Due to memory limitations I keep this at 10 for daint
# endoVal       = Value for endocardial cells in the anatomy file
# epiVal        = Value for epicardial cells in the anatomy file


VISPARAMS='-initialCode=0
            -tBegin='${TBEGIN}'
            -tEnd='${TEND}'
            -tInt='${TINT}'
            -sphereRadius=6
            -iga2igb=iga2igb
            -windowType=vertical
            -figWidth='${FIGWIDTH}'
            -figHeight='${FIGHEIGHT}'
            -n_processes=10
            -endoVal=50
            -epiVal=52
            -scale=scaleValue'

## 2D visualization parameters explanation (meant for ECG plotting side by side)
# tBegin, tEnd, tInt = same as above
# samplingFrequency  = sampling frequency of the ECG
# leadConfig         = lead configuration
# option             = refers to how the ecg was saved
# normalizeAmplitude = if True, normalize the ECG amplitudes so all of them have approximately the same visual range
# showTimeAxis       = if True, the time axis is shown
# figWidth and figHeight = size of the output image
# dpi                = dots per inch

VISPARAMS2D='-tBegin='${TBEGIN}'
            -tEnd='${TEND}'
            -tInt='${TINT}'
            -samplingFrequency=1000
            -leadConfig=leadfields-standard
            -option=leadfields
            -normalizeAmplitude=True
            -showTimeAxis=True
            -figWidth='${FIGWIDTH}'
            -figHeight='${FIGHEIGHT}'
            -dpi=100'

# The camera configuration used to be fixed, now it is customizable
# The easiest way to get these parameters is opening the .igb in Paraview with 0.2 scale and click on Adjust Camera
# Usually some adjustments are needed for the zoom. The default parameters are the ones used previously
# Comment the bottom part if you don't want to plot the right view
# Update: I realized that I can reset the camera after setting the camera angle. Thus, the most important 
# parameter after the camera position is the zoom, which has to annoyingly be adjusted by trial and error.
# A zoom of 1.5 seems to work fine overall
# cPosP 55.8 53.89 -117.96
# -cFPP 55.8 53.89 39.9
# -cVUP 0 1 0
# -cZoomP 1.2
# --plotRV
CAMERAPARAMS='-cPosP 129.7 -110.78 -136.58
              -cFPP -11.32 188.61 210.25
              -cVUP -0.07 0.83 -0.56
              -cZoomP 1.5
              -cPosA 111.3 227.74 21.51
              -cFPA 110.97 226.71 21.62
              -cVUA -0.29 -0.06 -0.95
              -cZoomA 1.4
              plotRightView
              -cPosR -184.37 45.18 49.25
              -cFPR 59.9 45.18 49.25
              -cVUR 0 1 0
              -cZoomR 1.4'
WD=/users/vgonalve/model-analyses/visualization # Where the code is


####################### Anatomy Parameters #######################
# The anatomy file is a special .igb where all the cells from a certain layer
# get the same value. In my case, the endocardial and the epicardial layers
# have different values.

# This matters for the visualization because the opacity of the epicardial cells 
# is reduced, so both layers can be visualized. If this is not required, just 
# assign all the values to 'endoVal'

ANATOMY=anatomyFile
ANATOMYPATH=AnatomyPath # -anatomyPath

####################### 3D Plot Parameters #######################

export DISPLAY=:displayVariable
# killall Xvfb
Xvfb :displayVariable -screen 0 1600x1200x24 &

PSPATH=PsPath
PSFILE=PsFile
PLOTPS='PlotPS' #'--plotPS '-psPath=${PSPATH}' '-psFile=${PSFILE} # or ''

BTPATH=BtPath
BTFILE=BtFile
PLOTBT=PlotBT #'--plotBT '-btPath=${BTPATH}' '-btFile=${BTFILE} # or ''

OUTPATH3D=outPath3D

####################### 2D Plot Parameters #######################

# ECGCODEPATH=/users/vgonalve/model-analyses/ecg-analysis
# ECGDATAPATH=/project/s1074/LongAF/Control #-dataPath=

# ECGPARAMS='-option=leadfields
#             -finalCode=-1
#             -outPath='${OUTPATH2D}

# OUTPATH2D=/users/vgonalve/exec/anims/ecg #outputPath


#################################################################
########################### Functions ###########################

#
### 3D Plot $CAMERAPARAMS
cd $WD
srun python ParallelModelVisualization.py ${ANATOMY} ${EXPNAME}_vm_afull.iga.gz \
$VISPARAMS -outputPath=${OUTPATH3D} -anatomyPath=${ANATOMYPATH} \
-dataPath=${DATAPATH} ${CAMERAPARAMS} ${PLOTPS}

cd $OUTPATH3D
#Adjust -start_number as needed
# ~/ffmpeg/ffmpeg -i ${EXPNAME}%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}A.mp4
~/ffmpeg/ffmpeg -i ${EXPNAME}%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}A.mp4

~/ffmpeg/ffmpeg -i ${EXPNAME}RV%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}RV.mp4

### 2D Plot
# cd $ECGCODEPATH 
# python convert_ecg.py ${ECGPARAMS} -dataPath=${ECGDATAPATH} $BASENAME $ID 
# cd $WD
# srun python LeadPlotter.py ${VISPARAMS2D} -outputPath=${OUTPATH2D} -dataPath=${OUTPATH2D} $EXPNAME 

# cd $OUTPATH2D
# ~/ffmpeg/ffmpeg -i ${EXPNAME}_Leads_%04d.png -r 20 -q 2 -pix_fmt yuv420p  ${VIDEONAME}B.mp4 

### Make Videos
killall Xvfb

### Join videos

# ~/ffmpeg/ffmpeg -i ${VIDEONAME}A.mp4 -i ${VIDEONAME}B.mp4 -filter_complex hstack ${VIDEONAME}.mp4

# Clean (Better do manually in daint to avoid deleting things before making sure everything is ok)
#rm ${VIDEONAME}A.mp4
#rm ${VIDEONAME}B.mp4
#find -maxdepth 1 -type f -name ${EXPNAME}'*.png' -delete
#find -maxdepth 1 -type f -name ${ECGFILE}'*.png' -delete

#ffmpeg -i exp906c05_w_ecgA.mp4 -i exp906c05_w_ecgB.mp4 -filter_complex hstack exp906c05_w_ecg.mp4

