#!/bin/bash

# Array of experiment names
exp_names=(
    "EX0025_A00_R0_P15" # running
    "EX0025_A00_R0_P16" # running
    # "EX0026_C0_P04"
)


# DATAPATH=/users/vgonalve/exec/EX0022 #/users/vgonalve/exec/exp906 #/users/vgonalve/exec/EX0008 /project/s1074/LongAF/PVI ## datapath 
DATAPATH=/users/vgonalve/exec/EX0026


####### Timing, computation, and visualization parameters #######
TBEGIN=2000
TEND=-1
TINT=5


####################### Anatomy Parameters #######################
# The anatomy file is a special .igb where all the cells from a certain layer
# get the same value. In my case, the endocardial and the epicardial layers
# have different values.

# This matters for the visualization because the opacity of the epicardial cells 
# is reduced, so both layers can be visualized. If this is not required, just 
# assign all the values to 'endoVal'

ANATOMY=two-layers-model24-29-heart-cell.igb
ANATOMYPATH=/users/vgonalve/exec/anatomy/model29 # -anatomyPath

# ANATOMY=two-layers-model24-30-heart-cell.igb
# ANATOMYPATH=/users/vgonalve/exec/anatomy/model30 # -anatomyPath


####################### 3D Plot Parameters #######################
PLOTPS=false
PSPATH=/users/vgonalve/exec/EX0025/psdetection

BTPATH=/users/vgonalve/exec/EX0016/bt-detection

OUTPATH3D=/users/vgonalve/exec/anims

PLOTRV=true

SCALE=0.2
# SCALE=0.35
# 

TIME=02:00:00
SUBMIT=false

expIndex=0
# Loop through the experiment names and create copies of the script
for exp_name in "${exp_names[@]}"; do

    VIDEONAME=${exp_name}    

    # Generate the new script name
    new_script_name="animation_${exp_name}.sh"

    # Create a copy of the original script
    cp AnimationSubmission.sh "${new_script_name}"

    # Replace the parameters above 
    sed -i "s/experimentName/${exp_name}/g" "${new_script_name}"
    sed -i "s/videoName/${VIDEONAME}/g" "${new_script_name}"
    sed -i "s/jobName/animation_${exp_name}/g" "${new_script_name}"
    sed -i "s/requestedHours/${TIME}/g" "${new_script_name}"

    sed -i "s|timeBegin|${TBEGIN}|g" "${new_script_name}"
    sed -i "s|timeEnd|${TEND}|g" "${new_script_name}"
    sed -i "s|timeInt|${TINT}|g" "${new_script_name}"
    sed -i "s|displayVariable|${expIndex}|g" "${new_script_name}"

    sed -i "s|DataPath|${DATAPATH}|g" "${new_script_name}"
    sed -i "s|outPath3D|${OUTPATH3D}|g" "${new_script_name}"
    sed -i "s|AnatomyPath|${ANATOMYPATH}|g" "${new_script_name}"
    sed -i "s|anatomyFile|${ANATOMY}|g" "${new_script_name}"
    sed -i "s|scaleValue|${SCALE}|g" "${new_script_name}"

    PSFILE=${exp_name}_th100.pslist
    if $PLOTPS
    then
        PLOTPSKEY='--plotPS '-psPath=${PSPATH}' '-psFile=${PSFILE} # or ''
    else
        PLOTPSKEY=''
    fi


    sed -i "s|PsPath|${PSPATH}|g" "${new_script_name}"
    sed -i "s|PsFile|${PSFILE}|g" "${new_script_name}"
    sed -i "s|PlotPS|${PLOTPSKEY}|g" "${new_script_name}"

    BTFILE=${exp_name}.btd
    PLOTBT='' #'--plotBT '-btPath=${BTPATH}' '-btFile=${BTFILE} # or ''

    sed -i "s|BtPath|${BTPATH}|g" "${new_script_name}"
    sed -i "s|BtFile|${BTFILE}|g" "${new_script_name}"
    sed -i "s|PlotBT|${PLOTBT}|g" "${new_script_name}"
    
    if $PLOTRV
    then
        sed -i "s|plotRightView|--plotRV|g" "${new_script_name}"
    else
        sed -i "s|plotRightView|''|g" "${new_script_name}"
    fi
 

    chmod +x "${new_script_name}"

    echo "Created script: ${new_script_name}"

    if $SUBMIT
    then
        sbatch ${new_script_name}
    fi
    expIndex=$((expIndex +1))

done
