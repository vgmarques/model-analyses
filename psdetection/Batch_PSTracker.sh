#!/bin/bash

# Array of experiment names
exp_names=(    
    # "EX0025_A70_P00"      
    # "EX0025_A70_P01"      
    # "EX0025_A70_P02"      
    # "EX0025_A70_P03"      
    # "EX0025_A70_P04"      
    # "EX0025_A70_P05"      
    # "EX0025_A70_P06"      
    # "EX0025_A70_P08"      
    # "EX0025_A70_P12"      
    # "EX0025_A70_P13"      
    # "EX0025_A70_P14"      
    # "EX0025_A70_P17"  
    "EX0025_A00_R0_P03"      
    "EX0025_A00_R0_P04"      
    "EX0025_A00_R0_P15"      
    "EX0025_A00_R0_P16"   
)

# Paths and filenames
PSFINDERPATH=/users/vgonalve/exec/EX0025/psdetection

OUTPATH=/users/vgonalve/exec/EX0025/psdetection
DURATION_TH=100 #ms

TIME=02:00:00
SUBMIT=true

# Loop through the experiment names and create copies of the script
for exp_name in "${exp_names[@]}"; do
    BASENAME=${exp_name}
    # Generate the new script name
    new_script_name="PSTracker_${exp_name}.sh"

    # Create a copy of the original script
    cp RunPSTracker.sh "${new_script_name}"

    # Replace the parameters above 
    sed -i "s/experimentName/${exp_name}/g" "${new_script_name}"
    sed -i "s/jobName/psfinder_${exp_name}/g" "${new_script_name}"
    sed -i "s/requestedHours/${TIME}/g" "${new_script_name}"

    sed -i "s|psFinderPath|${PSFINDERPATH}|g" "${new_script_name}"
    sed -i "s|baseName|${BASENAME}|g" "${new_script_name}"
    sed -i "s|outPath|${OUTPATH}|g" "${new_script_name}"
    sed -i "s|durationTh|${DURATION_TH}|g" "${new_script_name}"

    chmod +x "${new_script_name}"

    echo "Created script: ${new_script_name}"

    if $SUBMIT
    then
        sbatch ${new_script_name}
    fi

done
