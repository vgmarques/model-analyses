'''
This function creates and executes bash scripts to run PSFinder with the same
configuration for a given list of names
'''

import sys,os

# The name is composed by basename%02d, where %02d comes from the iteration list
# everything else has to be adjusted in the bash script itself before running
# Check "RunPSFinder.sh" as a guide

basename = 'exp906a'
iteration_lst = [5,12,13,14,15,17,18,19,70,71,72,74,75,82,83,84,87,88,89] # and 5

for i,code in enumerate(iteration_lst):

    with open ('tmp/psfinder_%s%02d.sh'%(basename,code), 'w') as rsh:
        rsh.write('''\
#!/bin/bash -l
#SBATCH --job-name="psfinder_exp906a%02d"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl
#SBATCH --time=07:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --constraint=gpu
#SBATCH --account=s1074

module load daint-gpu
module load cray-python
module load matplotlib

# Paths and filenames

ANATOMYPATH=/users/vgonalve/exec/anatomy/model30
DATAPATH=/users/vgonalve/exec/exp906
OUTPATH=/users/vgonalve/exec/exp906/psdetection
CELLFILE=${ANATOMYPATH}/model24-30-heart-cell-pvi.igb

EXPNAME=exp906a%02d
FNAMEIN=${DATAPATH}/${EXPNAME}_vm_afull.iga.gz
FNAMEOUT=${OUTPATH}/${EXPNAME}.psfinder

SAVEINTERVAL=500 # sample interval between save files (to prevent losing everything if something fails)

# timing
TBEGIN=0
TEND=30000
TINT=1

# Phase calculation
TAU=10
VISO=-40 # obtained with estimateViso.py
NTHREADS=4 # The number of threads is limited by the memory

# Put all together
PSFINDERPARAMS='-tBegin='${TBEGIN}'
                -tEnd='${TEND}'
                -tInt='${TINT}'
                -Tau='${TINT}'
                -fname_out='${FNAMEOUT}'
                -n_processes='${NTHREADS}'
                -save_interval='${SAVEINTERVAL}'
                -Viso='${VISO}'
                -fname_cell='${CELLFILE}
                

mkdir $OUTPATH
srun python PSFinder.py $PSFINDERPARAMS $FNAMEIN
'''%(code,code))
    
    # Run the script
    os.system('sbatch tmp/psfinder_%s%02d.sh'%(basename,code))



