# -*- coding: utf-8 -*-
"""
Created on Wed Sep 22 09:51:07 2021

DESCRIPTION: Calculates distances between catheter centers and PSs

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
#%% Import stuff

## Python libraries
import os,sys
import numpy as np
import pandas as pd
from scipy.spatial.distance import cdist,pdist,squareform
import matplotlib.pyplot as plt
import pickle
import time
import multiprocessing as mp
from itertools import repeat

## My Libraries
upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
import IgbHandling as igb
import pslibrary as psl

## Additional functions

def MakeAdjacencyMatrix(tissuePatch,SpatialResolution):
    '''
    Generates the adjacency matrix for a cloud of points obtained from an igb

    Parameters
    ----------
    tissuePatch : N,3 np array
        Points of the tissue patch (3D but could be 2D).
    spatialResolution : float, optional
        The spatial resolution of the igb. The default is 1.

    Returns
    -------
    AdjacencyMatrix : TYPE
        DESCRIPTION.

    '''
    Distances = pdist(tissuePatch, 'euclidean').astype(np.int16)
    Distances[Distances<=SpatialResolution*np.sqrt(2)] = -1
    Distances[Distances>SpatialResolution*np.sqrt(2)] = 0
    Distances = -Distances.astype(np.int16)
    AdjacencyMatrix = squareform(Distances)
    return AdjacencyMatrix

def GeodesicDistances(Source,AdjacencyMatrix,EdgeWeight = 1):
    Distances = np.ones(AdjacencyMatrix.shape[0])*np.nan
    Distances[Source] = 0
    Vertex = Source
    InSet = np.ones_like(Distances);   
    Visited = []
    while True:
        #Include u to sptSet.
        Visited.append(Vertex)
        #Update distance value of all adjacent vertices of u. 
        #To update the distance values, iterate through all adjacent vertices. 
        #For every adjacent vertex v, if the sum of a distance value of u (from source) 
        #and weight of edge u-v, is less than the distance value of v, then update the distance value of v. 
        Neighbors = np.where(AdjacencyMatrix[Vertex,:]==1)[0]
        for i,v in enumerate(Neighbors):
            if Distances[Vertex]+EdgeWeight<Distances[v] or np.isnan(Distances[v]):
                Distances[v] = Distances[Vertex]+EdgeWeight
        InSet[Vertex] = np.nan
        if np.nansum(InSet)==0:
            break
        #check minimum distance
        temp = np.where(~np.isnan(Distances)*(~np.isnan(InSet)))[0]
        # Check if in set        
        try:
            Vertex = temp[np.where(Distances[temp]==np.nanmin(Distances[temp]))[0][0]]    
        except:
            Vertex = np.where(~np.isnan(InSet))[0][0]
    return Distances

#%% Paths and parameters
anatomyPath = "/users/vgonalve/exec/anatomy/model30Box"
savepath = '/users/vgonalve/exec/RPAnalyses/review/final'
psPath = '/project/s1074/exp906/psdetection'

FnameCell = os.path.join(anatomyPath,'model24-30Box-atrial-surface-n.igb')

experimentNames = []
for file in os.listdir(psPath):
    if file.startswith('exp906c') and file.endswith('th0.txt'):
        experimentNames.append(file.split('_')[0])
experimentNames = np.sort(experimentNames)

catheterCenters = [1256,2208,1023,14012,12093,7814,14259,4438,8481,14426,6107,15932 ,22286 ,22188,14613,14285]
catheterNames = ['RPV','LPV','PLA','ILA_1','ILA_2','ILA_3','ALA','LA_Roof','LA_AS','LAA','SRA','IRA','RAA','PECT','CSO','ARA']

NProcesses = 10
#%% Load Anatomy
FullCell,_ = igb.Load(FnameCell)
zz,yy,xx = np.where(np.isin(FullCell,[50,51]))
Anatomy = np.array([xx,yy,zz]).T

#%% Calculate adjacency matrix for anatomy
print('Calculating adjacency matrix')
ti = time.time()
AdjMat = MakeAdjacencyMatrix(Anatomy,1)
print('Elapsed time: %0.3f s'%(time.time()-ti))

#%% Calculate geodesic distance between catheter centers and anatomy
print('Calculating geodesic distances')
ti = time.time()
with mp.Pool(NProcesses) as pool:
    GeoDists = pool.starmap(GeodesicDistances, zip([catheterCenters[cat] for cat in range(len(catheterCenters))],
                                                  repeat(AdjMat)))
print('Elapsed time: %0.3f s'%(time.time()-ti))

#%% Loop over experiments


for experiment in experimentNames:
    # Get PSs
    PS_DF = psl.readPSFile(experiment,psPath,threshold=0) #<< ms threshold for trajectories
    unTraj = np.unique(PS_DF['traj'])
    unTimes = np.unique(PS_DF['t'])
    
    # Project the exact position of each point into the anatomy shell. 
    print('Projecting PSs')
    ti = time.time()

    Positions = np.asarray(PS_DF.loc[:,['x','y','z']])
    Positions = np.round(Positions)

    uniquePos,reconstructInds = np.unique(Positions,axis=0,return_inverse=True)

    projections = np.zeros((len(uniquePos),3))
    inds = list()
    for i,row in enumerate(uniquePos):
        dists = np.linalg.norm(row-Anatomy,axis=1)
        inds.append(np.where(dists==np.min(dists))[0][0])
        projections[i,:] = Anatomy[inds[-1],:]

    distanceInds = np.zeros(len(Positions),dtype = int)
    for ii,jj in enumerate(reconstructInds):
        Positions[ii,:] = projections[jj,:]
        distanceInds[ii] = inds[jj].astype(int)


    PS_DF = PS_DF.assign(x=Positions[:,0])
    PS_DF = PS_DF.assign(y=Positions[:,1])
    PS_DF = PS_DF.assign(z=Positions[:,2])

    print('Elapsed time projecting points: %0.3f s'%(time.time()-ti))
    
    # For each catheter, add columns
    for cat in range(len(catheterCenters)):
        selectedDists = GeoDists[cat][distanceInds].astype(int)
        PS_DF.insert(len(PS_DF.columns),('D%s'%catheterNames[cat]),selectedDists)
    # Save
    PS_DF.to_csv(os.path.join(savepath,experiment+'_PSPositionsAndDistances_th0.csv'))
    
    print('Saved %s'%experiment)
    
#%% For the high coverage version
'''
catheterNames = 'HD000;HD001;HD002;HD003;HD004;HD005;HD006;HD007;HD008;HD009;HD010;HD011;HD012;HD013;HD014;HD015;HD016;HD017;HD018;HD019;HD020;HD021;HD022;HD023;HD024;HD025;HD026;HD027;HD028;HD029;HD030;HD031;HD032;HD033;HD034;HD035;HD036;HD037;HD038;HD039;HD040;HD041;HD042;HD043;HD044;HD045;HD046;HD047;HD048;HD049;HD050;HD051;HD052;HD053;HD054;HD055;HD056;HD057;HD058;HD059;HD060;HD061;HD062;HD063;HD064;HD065;HD066;HD067;HD068;HD069;HD070;HD071;HD072;HD073;HD074;HD075;HD076;HD077;HD078;HD079;HD080;HD081;HD082;HD083;HD084;HD085;HD086;HD087;HD088;HD089;HD090;HD091;HD092;HD093;HD094;HD095;HD096;HD097;HD098;HD099;HD100;HD101;HD102;HD103;HD104;HD105;HD106;HD107;HD108;HD109;HD110;HD111;HD112;HD113;HD114;HD115;HD116;HD117;HD118;HD119;HD120;HD121;HD122;HD123;HD124;HD125;HD126;HD127;HD128;HD129;HD130;HD131;HD132;HD133;HD134;HD135;HD136;HD137;HD138;HD139;HD140;HD141;HD142;HD143;HD144;HD145;HD146;HD147;HD148;HD149;HD150;HD151;HD152;HD153;HD154;HD155;HD156;HD15'
catheterNames = catheterNames.split(';')

catheterCenters = '222;275;476;603;833;868;972;1371;1544;1591;1699;1741;1914;2075;2184;2678;2858;3032;3260;3357;3478;3503;3915;3970;4052;4501;4656;4714;4826;4929;5414;5442;5520;5563;5620;5628;5784;5875;6122;6528;6808;7072;7134;7204;7219;7357;7391;7424;7849;8099;8337;8524;9166;9319;9419;9539;9612;9668;9707;9790;9874;10005;10142;10280;10449;10509;10849;10937;10982;11051;11420;11714;11981;12131;12233;12366;12382;12466;12597;12657;13453;13521;13633;13697;13798;13863;13982;14071;14234;14299;14381;14510;14528;15150;15254;15760;15829;15852;15979;16125;16349;16644;16648;16900;16905;17403;17442;17506;17549;17730;17749;18172;18278;18616;18793;18870;18999;19092;19201;19282;19978;19995;20158;20173;20400;20792;20998;21066;21188;21293;22049;22929;23192;23238;23273;23366;23395;23687;23841;24013;24396;24869;24921;25376;25440;25581;25898;26132;26370;26461;26622;26674;1250;6359;11326;1975;6485;12145'
catheterCenters = np.asarray(catheterCenters.split(';'),dtype = int)
'''
