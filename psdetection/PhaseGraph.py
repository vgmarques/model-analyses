import os, sys
import numpy as np
import subprocess
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import scipy.io as sio

fname_vm = '/mnt/d/vgmar/model_data/exp906/exp906k71_vm_afull.iga.gz'

with subprocess.Popen(["gzip","-dc",fname_vm],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen(['iga2igb',"-q","--ignore-password-check","--sparse",
        '-t','140:5:190',"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:

            hdr,coords = igb.ReadLeanHeader(iga)
            print(hdr)
            # size of a single slice in time
            dtype = np.short 
            Ne = hdr['Ne']*dtype().nbytes

            # Buffer
            Buffer = np.zeros((hdr['Ne'],10))
            # Read first Tau cases case
            for i in range(10):
                Buffer[:,i]=np.frombuffer(iga.stdout.read(Ne),dtype=dtype)* hdr['facteur'] + hdr['zero']

sio.savemat('phasegraph.mat',{'B':Buffer})#,'facteur':hdr['facteur'],'zero':hdr['zero']})