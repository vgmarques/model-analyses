#!/bin/bash -l
#SBATCH --job-name=jobName
#SBATCH --mail-type=ALL
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl
#SBATCH --time=requestedHours
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=8
#SBATCH --constraint=gpu
#SBATCH --account=s1074

module load daint-gpu
module load cray-python
module load matplotlib

# Paths and filenames

ANATOMYPATH=anatomyPath
DATAPATH=dataPath
OUTPATH=outPath
CELLFILE=cellFile #model24-30-heart-cell-fib00m70-pvi.igb #model24-30-heart-cell-pvi.igb

EXPNAME=experimentName
FNAMEIN=${DATAPATH}/${EXPNAME}_vm_afull.iga.gz
FNAMEOUT=${OUTPATH}/${EXPNAME}.psfinder

SAVEINTERVAL=2500 # sample interval between save files (to prevent losing everything if something fails)

# timing
TBEGIN=0
TEND=-1
TINT=1

# Phase calculation
TAU=10
VISO=-40 # obtained with estimateViso.py
NTHREADS=4 # The number of threads is limited by the memory

# Put all together
PSFINDERPARAMS='-tBegin='${TBEGIN}'
                -tEnd='${TEND}'
                -tInt='${TINT}'
                -Tau='${TINT}'
                -fname_out='${FNAMEOUT}'
                -n_processes='${NTHREADS}'
                -save_interval='${SAVEINTERVAL}'
                -Viso='${VISO}'
                -fname_cell='${CELLFILE}
                # -n_readers=2'
                

mkdir $OUTPATH
python PSFinder.py $PSFINDERPARAMS $FNAMEIN