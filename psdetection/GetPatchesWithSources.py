#%% Load stuff
import os,sys
import numpy as np
import pandas as pd
import pickle 

from sklearn.decomposition import PCA
from scipy.spatial.distance import pdist,squareform
import matplotlib.pyplot as plt
from scipy.linalg import svd  
import time


upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
sys.path.append(os.path.join(upperFolder,'recurrence'))
import IgbHandling as igb
from CatheterObjects import CatheterClass,CatheterGroup
import egm_processing as egmp
import quick_visualization as qv


#%% Load Anatomy from endocardium
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,validCells)
Anatomy,dataIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells)

nz,ny,nx = FullCell.shape

#%% Load PS file
# These PF files were generated for the 16 catheter positions setting
# A similar one could be generated for the extensive coverage, but takes a lot of time
# Instead, I just use the projected SPs for calculating distances, and use
# simple euclidian distances for the task

psPath = '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Filtered'
expName = 'exp906c12'


PS_DF = pd.read_csv(os.path.join(psPath,expName+'_PSPositionsAndDistances.csv'))
PS_DF = PS_DF[['t','x','y','z','traj']]



PS_DF = pd.read_csv(os.path.join(psPath,expName+'_pstracker_th100.txt'),sep=' ',
                    dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                 header = 1,names=['t','x','y','z','traj'],engine='c',
                 skipinitialspace=True)
scale=0.2
PS_DF['x'] *= scale
PS_DF['y'] *= scale
PS_DF['z'] *= scale

# Filter shorter than 5 AFCL
unTraj = np.unique(PS_DF['traj'])
trajDurations = np.zeros((len(unTraj),2),dtype=int)

for i,traj in enumerate(unTraj):
    traj_df = PS_DF.query('traj=='+str(traj))
    uniqueTimes = np.unique(traj_df['t'])
    trajDurations[i,:] = [uniqueTimes.min(), uniqueTimes.max()]
    lims = trajDurations[i,:]-uniqueTimes[0]

trajDurations = trajDurations[:,1]-trajDurations[:,0]

LongTrajectories = unTraj[trajDurations>=5*130]

#%% Load catheter group for positions
# 
fpath = '/mnt/d/vgmar/model_data/exp906/electrode_data/Control/extensive'
fname = expName+'_CatheterData.catG'
with open(os.path.join(fpath,fname),'rb') as input:
    CGroup = pickle.load(input)
catNames = list(CGroup.Catheters.keys())


#%% Loop over trajectories to:

ti = time.time()
egmscale=1 # mm
CatheterObj = CatheterClass('MiniGrid')
OutputDict = dict()
for i,traj in enumerate(LongTrajectories):
    trajDF = PS_DF.loc[PS_DF['traj']==traj,['t','x','y','z']]
    positions = np.asarray([trajDF.loc[trajDF['t']==t,:].mean() for t in np.unique(trajDF['t'])])

    times = positions[:,0]
    positions = positions[:,1:].astype('int')

    # # NOTE: this next part is commented out because we do not need the patch if
    # # using electrodes only 

    # Get mean position
    meanPos = np.median(positions,axis=0).astype('int')
    projDist = np.linalg.norm(Anatomy-meanPos,axis=1)
    patchCenter = np.where(projDist==np.min(projDist))[0][0] 

    # # Get extent of the patch
    radius = np.max(np.linalg.norm(positions-meanPos,axis=1))
    radius = np.round(radius)

   

    # # Get patch indexes
    # ## Make fake electrode array
    CatheterObj.ElectrodesTemplate = np.array([[0,radius],[0,radius]])
    CatheterObj.ElectrodesTemplate = np.array([[0,radius],[0,radius]])

    CatheterObj.ExtraPoints = np.array([0,0]).reshape(1,-1)
    CatheterObj.GetTissuePatch(patchCenter,Anatomy)#,geodesicThreshold = int(radius))
    
    # # Save indexes wrt. to trajectory code
    # OutputDict[traj] = CatheterObj.PatchIndexes

    # Compare positions with catheter centers, get those where the rotor was
    # in the array for a while
    # NOTE: Ideally we can mark these intervals, maybe it is useful to save them
    
    inPatch = list()
    for name in catNames:
        catTest = CGroup.Catheters[name]
        CatCenter = catTest.GlobalPatchCenter
        dist = np.linalg.norm(positions-CatCenter,axis=1)
        if np.sum(dist<=7)!=0:
            inPatch.append(name)

    OutputDict[traj] = inPatch

print(time.time()-ti)


#%%
savePath = '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Patches'


with open(os.path.join(savePath,expName+'_650ms.patch'),'wb') as output:
    pickle.dump(OutputDict,output)




#%% K means clustering fun
from sklearn.cluster import KMeans

n4Cluster = int(np.floor(CatheterObj.Patch.shape[0]/4))

KMeanObj = KMeans(n4Cluster).fit(CatheterObj.Patch)

label = 100
label_inds = np.where(KMeanObj.labels_==label)[0]
Center = np.mean(CatheterObj.Patch[label_inds,:],axis=0)+CatheterObj.GlobalPatchCenter


#%% 
CatheterObj2 = CatheterClass('MiniGrid')
CatheterObj2.GetTissuePatch(Center,Anatomy,radiusMultiplier=2)
CatheterObj2.PlaceCatheter(Anatomy)
# %%      

anatomyActor,_ = qv.MakePolyDataActor(CatheterObj2.Patch+CatheterObj2.GlobalPatchCenter,
                KMeanObj.labels_.reshape(1,-1),cmap='heatmap')
anatomyActor.GetProperty().SetPointSize(5)

PatchActor,_ = qv.MakePolyDataActor(CatheterObj2.ElectrodeCoordinates,
                np.arange(len(CatheterObj2.ElectrodeCoordinates)).reshape(1,-1),vmin=0,
                vmax = len(CatheterObj2.ElectrodeCoordinates))

# PatchActor,_ = qv.MakePolyDataActor(test2,np.ones(len(test2)).reshape(1,-1),vmin=0,vmax = 1)

qv.QuickRenderWindowInteractor([anatomyActor,PatchActor])



# #%%
# pca = PCA(n_components=2)
# principalComponents = pca.fit_transform(CatheterObj.Patch)

# planePC = np.zeros((principalComponents.shape[0],3))
# planePC[:,:2] = principalComponents
# v1 = planePC[1,:]-planePC[0,:]
# v2 = planePC[2,:]-planePC[0,:]
# n = np.cross(v1, v2)

# PlaneDistances = squareform(pdist(principalComponents))

# keepList = []

# center = np.mean(CatheterObj.Patch,axis=0)
# for i in range(PlaneDistances.shape[0]):
#     tooCloseInds = np.where(PlaneDistances[i,:]<0.4)[0]
#     distToCenter = np.linalg.norm(CatheterObj.Patch[tooCloseInds]-center,axis=1)
#     distToCenter = np.round(distToCenter)
#     keepInd = np.where(distToCenter==np.min(distToCenter))[0][0]
#     keepList.append(tooCloseInds[keepInd])
#         # if len(tooCloseInds)>0:
#         #     for j in tooCloseInds[removeInds]: removeList.append(j)

# # removeList = np.unique(removeList)
# keepList = np.unique(keepList)

# #%% 

# fig,ax = plt.subplots(1,figsize=(10,10))
# ax.scatter(principalComponents[:,0],principalComponents[:,1],s=20,c='r')
# ax.scatter(principalComponents[keepList,0],principalComponents[keepList,1],marker='x',c='g',s=20)

# # %%
# zeroPatch = CatheterObj.Patch
# # test = np.array([[0,0,0],u,v,n])

# PatchActor1,_ = qv.MakePolyDataActor(zeroPatch,np.zeros(len(zeroPatch)).reshape(1,-1),vmin=0,vmax = 1)
# PatchActor2,_ = qv.MakePolyDataActor(zeroPatch[keepList,:],np.ones(np.sum(keepList)).reshape(1,-1),vmin=0,vmax = 1)
# centerActor,_ = qv.MakePolyDataActor(center.reshape(-1,3),np.zeros(len(center)).reshape(1,-1),cmap='heatmap')

# # PatchActor2,_ = qv.MakePolyDataActor(test,np.arange(len(test)).reshape(1,-1),vmin=0,vmax = 4)
# qv.QuickRenderWindowInteractor([PatchActor1,PatchActor2,centerActor])

# %% Test with loading the data
# datafile = '/mnt/d/vgmar/model_data/exp906/exp906c74f_ve_egm_endo_epi.igb.gz'

# Data,hdr = igb.Load(datafile,returnRawSignal=True)

with open(os.path.join(savePath,expName+'_650ms.patch'),'rb') as output:
    InputDict = pickle.load(output)

# Data = Data[:,dataIndexes]

# #%%
# traj = list(InputDict.keys())[0]
# timeRange = PS_DF.loc[PS_DF['traj']==traj,['t']]
# timeRange = np.asarray([timeRange.min(), timeRange.max()]).flatten()


# inds = InputDict[traj]
# patchData = Data[timeRange[0]:timeRange[1],inds]*hdr['facteur']+hdr['zero']


# PatchActor1,cbarActor = qv.MakePolyDataActor(Anatomy[inds,:],patchData[510,:].reshape(1,-1),cmap='egm')
# qv.QuickRenderWindowInteractor([PatchActor1],[cbarActor])

# # %%

# # %% Tests with frequency stuff
# from ECGData import ECGData

# Data = ECGData(None,patchData,1000)

#%% Load electrode data with extensive coverage


#%% 
traj = list(OutputDict.keys())[2]
inPatch = OutputDict[traj]
# Patch = Anatomy[InputDict[traj],:]

anatomyActor,_ = qv.MakePolyDataActor(Anatomy,np.zeros(len(Anatomy)).reshape(1,-1),cmap='heatmap')
anatomyActor.GetProperty().SetPointSize(5)
# PatchActor,_ = qv.MakePolyDataActor(Patch,np.ones(len(Patch)).reshape(1,-1),vmin=0,vmax = 1)

ElectrodeInds = [CGroup.Catheters[name].ElectrodeIndexes for name in inPatch]
ElectrodeInds = np.asarray(ElectrodeInds).flatten()
elecPoints = Anatomy[ElectrodeInds,:]

elecActor,_ = qv.MakePolyDataActor(elecPoints,np.zeros(len(elecPoints)).reshape(1,-1),vmin=0,vmax = 1)

qv.QuickRenderWindowInteractor([anatomyActor,elecActor])

#%%
catTest = CGroup.Catheters[inPatch[2]]
gridLocations = catTest.ElectrodesTemplate
signal = catTest.EGMData
initialFrame=2500
step=5
endFrame=7500
qv.Animate2DGrid(gridLocations,signal,
                    initialFrame,endFrame,step,
                    vmin=None,vmax=None,
                    colormap=None,save=1,saveFile=None,
                    cbarLabel='Amplitude (mV)')

# %%
