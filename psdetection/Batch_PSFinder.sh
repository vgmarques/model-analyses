#!/bin/bash

# Array of experiment names
exp_names=(
    # "EX0025_A70_P00"      
    # "EX0025_A70_P01"      
    # "EX0025_A70_P02"      
    # "EX0025_A70_P03"      
    # "EX0025_A70_P04"      
    # "EX0025_A70_P05"      
    # "EX0025_A70_P06"      
    # "EX0025_A70_P08"      
    # "EX0025_A70_P12"      
    # "EX0025_A70_P13"      
    # "EX0025_A70_P14"      
    # "EX0025_A70_P17"      
    "EX0025_A00_R0_P03"      
    "EX0025_A00_R0_P04"      
    "EX0025_A00_R0_P15"      
    "EX0025_A00_R0_P16"      
)

ANATOMYPATH=/users/vgonalve/exec/anatomy/model29
DATAPATH=/users/vgonalve/exec/EX0025
OUTPATH=/users/vgonalve/exec/EX0025/psdetection

TIME=04:00:00
SUBMIT=true

# Loop through the experiment names and create copies of the script
for exp_name in "${exp_names[@]}"; do
    # Get proper anatomy file
    # CELLFILE=${ANATOMYPATH}/${exp_name}.igb #model24-30-heart-cell-pvi.igb #exp906a05_A1.igb #model24-30-heart-cell-fib00m70-pvi.igb #
    CELLFILE=${ANATOMYPATH}/NoFibrosis_R0.igb #model24-30-heart-cell-pvi.igb #exp906a05_A1.igb #model24-30-heart-cell-fib00m70-pvi.igb #
    # CELLFILE=${ANATOMYPATH}/model24-30-heart-cell.igb #model24-30-heart-cell-pvi.igb #exp906a05_A1.igb #model24-30-heart-cell-fib00m70-pvi.igb #
    # CELLFILE=${ANATOMYPATH}/model24-30-heart-cell-fib00m70-pvi.igb
    # CELLFILE=${ANATOMYPATH}/model24-29-heart-cell-pvi.igb


    # Generate the new script name
    new_script_name="PSFinder_${exp_name}.sh"

    # Create a copy of the original script
    cp RunPSFinder.sh "${new_script_name}"

    # Replace the parameters above 
    sed -i "s/experimentName/${exp_name}/g" "${new_script_name}"
    sed -i "s/jobName/psfinder_${exp_name}/g" "${new_script_name}"
    sed -i "s/requestedHours/${TIME}/g" "${new_script_name}"

    sed -i "s|anatomyPath|${ANATOMYPATH}|g" "${new_script_name}"
    sed -i "s|dataPath|${DATAPATH}|g" "${new_script_name}"
    sed -i "s|outPath|${OUTPATH}|g" "${new_script_name}"
    sed -i "s|cellFile|${CELLFILE}|g" "${new_script_name}"

    chmod +x "${new_script_name}"

    echo "Created script: ${new_script_name}"

    if $SUBMIT
    then
        sbatch ${new_script_name}
    fi

done
