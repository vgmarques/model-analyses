# -*- coding: utf-8 -*-
"""
Created on Mon May  3 14:44:20 2021

DESCRIPTION: This function is used to standardize the output of pstracker and
reshape it to be usable by the plotting functions

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os, sys
import numpy as np
import scipy.io as sio
import pickle
# Other packages
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))

import UserInterfaceFunctions as ui
from DataClasses import PStrackerResults, PSCluster

#%% Functions

# def LoadPSTrackerOutput(fname_in):
#     DataIn = sio.loadmat(fname_in)['results'][0,0]
#     Results = PStrackerResults()
#     Results.Npoints = DataIn[0].squeeze()
#     persample_temp= DataIn[1]
#     #
#     # Matlab or Python origin (I am not sure this will always work. TODO)
#     if len(persample_temp[0])==1:
#         persample_temp = persample_temp[0]
#         indexCorrection=0
#         print('Python input')
#     else:
#         indexCorrection=1
#         print('Matlab input')
#     # The core of the results
#     Results.persample = PSCluster()
#     Results.persample.points = persample_temp['points'][0].squeeze()-indexCorrection
#     Results.persample.clusind = persample_temp['clusind'][0].squeeze()-indexCorrection
#     Results.persample.centers = persample_temp['centers'][0].squeeze()-indexCorrection
#     Results.persample.tras = persample_temp['tras'][0].squeeze()-indexCorrection
#     #
#     #Other stuff
#     Results.number_of_ps = DataIn[2].squeeze()
#     if indexCorrection!=1:
#         Results.Ntra=DataIn[4].flatten()[0]
#     else:
#         Results.Ntra=DataIn[3]['start'][0,0].flatten().shape[0]
    
#     Results.trax = np.zeros(shape = (Results.Ntra,2))
#     Results.trax[:,0] = DataIn[3]['start'][0,0].flatten()-indexCorrection
#     Results.trax[:,1] = DataIn[3]['end'][0,0].flatten()-indexCorrection
#     #
#     return Results
def LoadPSTrackerOutput(fname_in):
    with open(fname_in, 'rb') as input:
        Results = pickle.load(input)
        partialInfo = pickle.load(input)
    #
    return Results

#%% Main


def main(args):
    print('Converting outputs of %s to a text file.'%args.fname_in)
    print('Trajectories with less than %0.1f ms will be disconsidered.\n'%args.MinTrajectoryDuration)
    print('Reading data...')
    Results = LoadPSTrackerOutput(args.fname_in)
    traj = Results.persample.tras
    points = np.asarray(Results.persample.points,dtype='object')#Results.persample.points
    trax = Results.trax
    
    # Determine the valid trajectories by elliminating PSs lasting less than a threshold
    
    Durations = trax['end']-trax['start']
    ValidDurations = Durations>args.MinTrajectoryDuration/args.samplingInterval
    
    print('Filtering trajectories...')
    with open(args.fname_out, 'w') as output:
        output.write('t x y z traj\n')
        validPoints = list()
        for tStep in range(points.shape[0]):
            # print(traj)
            trajInds = np.unique(traj[tStep])#.astype(int)
            if trajInds.any() is None: continue #FIXME: I am not so sure about this line, but it seems to be working
            trajInds = trajInds.astype(int)
            for trajectory in trajInds:
                if not ValidDurations[trajectory]: 
                    continue
                pointInds = np.where(traj[tStep].flatten()==trajectory)[0] # Find the points belonging to that trajectory
                uPoints = points[tStep][pointInds,:]
                [output.write("%5d %.1f %.1f %.1f %5d\n" % (tStep*args.samplingInterval+args.tb,
                                                           row[0],row[1],row[2],
                                                           trajectory)) for row in uPoints]
    
    print('File %s has been saved'%args.fname_out)
    print('Done!')
#%% Arg Parsing
if __name__ == '__main__':

    parser = ui.MyParser(description='Converts the output of PSTracker to text format,\
                         selecting trajectories longer than a defined threshold. Text format\
                              is used by the plotting functions',
                        fromfile_prefix_chars='+',
                        usage = 'python ConvertPSTrackerOutput.py PSTRACKER_FILE.pstracker')
        
    parser.add_argument('fname_in',action='store',type=str,
                        help = 'File containing output of PSTracker (at the moment, only .pstracker extension)')
    parser.add_argument('-fname_out','-fo',dest='fname_out',action='store',type=str,default = 'output.pslist',
                        help = 'File to output the data. Default is output.txt')
    # Time related
    parser.add_argument('-MinTrajectoryDuration','-mt',dest='MinTrajectoryDuration',action='store',type=float,default = 0,
                        help = 'Minimal trajectory duration for including in output (in ms). Default 0') 
    parser.add_argument('-samplingInterval',action='store',type=int,default = 1,
                        help = 'Sampling interval used to find the PSs (in ms). Default 1')   
                
    parser.add_argument('-tBegin','-tb',dest= 'tb',
                        action='store',default =0,type=int,
                        help = 'Frame of the initial PS detection. Default 0')
    
    
    args = parser.parse_args()
    args.fname_out = os.path.join(os.path.split(args.fname_in)[0],args.fname_out)
    # Run function
    main(args)