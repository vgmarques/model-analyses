'''
This function is the Python adaptation of psfinder.m
'''


import sys
import os
import subprocess
import time
from time import sleep
import multiprocessing as mp
#from pyccmc import igb_read
import numpy as np
import numpy.matlib as npm
import scipy.io as sio

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb
import UserInterfaceFunctions as ui

#%% 
class PSCluster:
    def __init__(self,points = []):
        
        points = []
        self.centers = []
        self.clusind = []
        self.tras = []

class PStrackerResults:
    def __init__(self):
        self.Npoints = 0
        self.persample = PSCluster()
        self.number_of_ps = []
        self.trax = []
        self.Ntra=0

####################### psf_pathfinder.m
def psf_pathfinder(opt):
    print('running psf_pathfinder')
        #
    #Determine which voxels to include.
    vmask,_ = igb.Load(opt.fname_cell)
    vmask = np.swapaxes(vmask,0,2)
    vmask = np.isin(vmask,opt.ctypes)
    #
    vdims = vmask.shape # dimensions of the voxels array
    ndims = (vdims[0]+1,vdims[1]+1,vdims[2]+1)     #dimensions of the nodes array
    Nvoxels = np.sum(vmask!=0)
    #
    '''
    Get the indices the nodes where each of the 3 types of paths
    should be placed. This could be done elegantly using 3D
    convolutions but it takes a lot of memory and time. This method is
    50 times faster.
    '''
    xx,yy,zz = np.where(vmask!=0)
    #
    ii = igb.Coord2Idx(np.array([xx, yy, zz]).T,ndims)
    #
    pxy = np.array([ii, ii+ndims[0]*ndims[1]]).T
    pxy = np.sort(np.unique(pxy).astype('uint32')) #<------------- potential indexing issue
    Nxy = len(pxy)
    #
    pxz = np.array([ii, ii+ndims[0]]).T   
    pxz = np.sort(np.unique(pxz).astype('uint32')) 
    Nxz = len(pxz)
    #
    pyz = np.array([ii, ii+1]).T    
    pyz = np.sort(np.unique(pyz).astype('uint32')) #<------------- potential indexing issue
    Nyz = len(pyz)
        #
    '''
    3D coordinates of the neigbours wrt the central point, for each
    path. Make these integer too, so the paths array will be computed as
    uint32.
    '''
    nb_xy = np.array([[0,0,0], [ 1,0,0],  [1,1,0],  [0,1,0]],dtype = np.uint32) #<------------- potential indexing issue
    nb_xz = np.array([[0,0,0],  [1,0,0],  [1,0,1],  [0,0,1]],dtype = np.uint32)
    nb_yz = np.array([[0,0,0],  [0,1,0],  [0,1,1],  [0,0,1]],dtype = np.uint32)
    #
    # Relative index for each neighbour. We need the dimensions of the
    # nodes array for this.
    #
    nbi_xy = nb_xy[:,0] + ndims[0]*nb_xy[:,1] + ndims[0]*ndims[1]*nb_xy[:,2]
    nbi_xz = nb_xz[:,0] + ndims[0]*nb_xz[:,1] + ndims[0]*ndims[1]*nb_xz[:,2]
    nbi_yz = nb_yz[:,0] + ndims[0]*nb_yz[:,1] + ndims[0]*ndims[1]*nb_yz[:,2]
        #
        #Absolute index for all members of all paths
    paths = np.append(npm.repmat(pxy,4,1).T + npm.repmat(nbi_xy.T, Nxy,1),
                        npm.repmat(pxz,4,1).T + npm.repmat(nbi_xz.T, Nxz,1),axis=0)
    paths = np.append(paths, npm.repmat(pyz,4,1).T + npm.repmat(nbi_yz.T, Nyz,1),axis=0)
    #
    Npaths = paths.shape[0]
    print('%d paths for %d voxels\n'%(Npaths, Nvoxels))
    #
    #Path centers. We must explicitly convert to float.
    x_xy, y_xy, z_xy = igb.Idx2Coord(pxy.astype('float64'),ndims)
    x_xz, y_xz, z_xz = igb.Idx2Coord(pxz.astype('float64'),ndims)
    x_yz, y_yz, z_yz = igb.Idx2Coord(pyz.astype('float64'),ndims)
    #
    centers = np.append(np.array([x_xy+0.5, y_xy+0.5, z_xy]).T,
                        np.array([x_xz+0.5, y_xz, z_xz+0.5]).T,axis=0)
    centers = np.append(centers,np.array([x_yz, y_yz+0.5, z_yz+0.5]).T,axis=0)
    '''
    Check that the neigbours are indeed valid in the tcc file (i.e.
    propag made the same decisions for placing nodes as we made).
    
    nmask = np.swapaxes(igb.Load(opt.fname_tcc)[0],0,-1)
    nmask = np.isin(nmask, opt.ctypes)
    tx,ty,tz = igb.Idx2Coord(paths.flatten(),nmask.shape)
    if nmask[tx,ty,tz].all():
        print('tcc check passed\n')
    else:
        RuntimeError('tcc does not match anatomy')
    '''
    print('tcc check ignored!')
    return paths,centers,ndims


def Reader(opt,tString,maskind,ProcessingQueue,LoggerQueue):
    # Does not have to be in a while loop
    # Could work with messages as well, but I don't see the point

    # Open Data
    print('Opening %s'%opt.fname_vm)
    with subprocess.Popen(["gzip","-dc",opt.fname_vm],stdout=subprocess.PIPE) as gz:
        with subprocess.Popen([opt.iga2igb,"-q","--ignore-password-check","--sparse",
        '-t',tString,"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:
            # Read header, send to logger with LoggerQueue
            hdr,_ = igb.ReadLeanHeader(iga)
            print(hdr)
            # LoggerQueue.put(('header',hdr)) # Maybe send to processing to save memory
            if hdr['Ne']!=len(maskind): RuntimeError('Vm and tcc dimensions do not match')# TODO: send to everyone
            #
            dtype = np.short 
            Ne = hdr['Ne']*dtype().nbytes
            # Create buffer with first tau values
            Buffer = np.zeros((hdr['Ne'],opt.Tau),dtype=dtype)
            for i in range(opt.Tau):
                Buffer[:,i]=np.frombuffer(iga.stdout.read(Ne),dtype=dtype)#*hdr['facteur'] + hdr['zero']
            
            # Start putting information in the ProcessingQueue
            # This will be used to standardize the output and save intermediate steps
            print('Processes are being sent')
            for T in range(0,hdr['Nt']-opt.Tau):
                Vn = np.frombuffer(iga.stdout.read(Ne),dtype=dtype)#* hdr['facteur'] + hdr['zero']
                ProcessingQueue.put(('phase',[T,Buffer[:,0],Vn,hdr['facteur'],hdr['zero']]))
                Buffer = np.roll(Buffer,-1,axis=-1)
                Buffer[:,-1]=Vn
                if T%100==0: print('\n tstep:%d'%T)
            # print('Data Sent')
            # Add messages to all workers to kill
            [ProcessingQueue.put(('kill',None)) for i in range(opt.n_processes)];

def Processing(ProcessingQueue,LoggerQueue,opt,path_begin,path_end,path_centers):
    #While loop
    while True:
        # Get message from queue
        QOut = ProcessingQueue.get(True)
        message,data = QOut
        # If kill: get out of loop, kill worker
        if message=='initialize':
            print('Worker initialized')
            hdr = None
        elif message=='header':
            if hdr is None:
                hdr = data
            else:
                ProcessingQueue.put(('header',hdr))
            
        elif message=='kill':
            print('Worker killed') # can be passed to logger to save 
            LoggerQueue.put(('process_done',None))
            break
        elif message=='phase':
            # If phase: do the phase processing
            index,V,Vn,facteur,zero = data
            Viso= (opt.Viso-zero)/facteur
            theta = np.arctan2(V-Viso, Vn-Viso) 
            # print(theta)
            s = theta[path_begin]-theta[path_end]
            sw = np.pi - (-s-np.pi)%(2*np.pi)
            pint = np.sum(sw,axis=-1) # There was an np.abs in my code here, but not in Ali's
            si = np.where((pint>3/2*np.pi) + (pint<-3/2*np.pi))[0] 
            try:
                xyz = np.unique(path_centers[si,:], axis=0)
            except:
                xyz = np.empty(0)
            # Send to Logger
            LoggerQueue.put(('results',[index,xyz]))

def Logger(LoggerQueue):
    import pickle
    # While loop
    while True:
        QOut = LoggerQueue.get(True)
        message,data = QOut
        # If initialize, open data
        if message=='initialize':
            print('Logger initialized')
            opt,info = data
            Results = PStrackerResults()
            Results.Npoints =0
            persample_temp = {}
            KilledWorkers = 0
            # Stuff for partial saves
            currentSave=opt.tBegin
            # totalElements = int((opt.tEnd-opt.tBegin-opt.Tau)/opt.tInt)
            #totalSave = totalElements//opt.save_interval + int(totalElements%opt.save_interval!=0)
            totalSave = None

        if message=='partial_save':
            info['tend'] = time.time()
            info['Tau'] = opt.Tau
            info['Viso'] = opt.Viso

            key_list = np.sort(list(persample_temp.keys()))
            Results.persample.points = [persample_temp[index] for index in key_list ] #!!!


            print('Partially saving')
            # Results['opt'] = opt
            root, extension = os.path.splitext(opt.fname_out)
            filename = root+'_%05d_%05d'%(currentSave,currentSave+opt.save_interval)+extension
            with open(filename, 'wb') as output:
                pickle.dump(Results, output, pickle.HIGHEST_PROTOCOL)
                pickle.dump(info, output, pickle.HIGHEST_PROTOCOL)


            # Reinitialize
            Results = PStrackerResults()
            Results.Npoints =0
            persample_temp = {}
            currentSave += opt.save_interval

        # If process_done, add to a value and check if concluded
        if message=='process_done':
            KilledWorkers += 1
            if KilledWorkers==opt.n_processes:
                if totalSave is None:
                    totalSave=currentSave + opt.save_interval
                LoggerQueue.put(('kill',(currentSave,totalSave)))
        # If kill, kill process
        if message=='kill':
            # Save
            info['tend'] = time.time()
            info['Tau'] = opt.Tau
            info['Viso'] = opt.Viso
            print('Total elapsed time:%0.2f s'%(info['tend']-info['tstart']) )

            # Organize persample
            key_list = np.sort(list(persample_temp.keys()))
            Results.persample.points = [persample_temp[index] for index in key_list ] #!!!

            ##Save the files ##
            # Results = {'results':Results, **info}
            # Results['opt'] = opt
            root, extension = os.path.splitext(opt.fname_out)
            # filename = root+str(currentSave)+'_'+str(totalSave)+extension
            # sio.savemat(filename,Results)
            filename = root+'_%05d_%05d'%(currentSave,totalSave)+extension
            with open(filename, 'wb') as output:
                pickle.dump(Results, output, pickle.HIGHEST_PROTOCOL)
                pickle.dump(info, output, pickle.HIGHEST_PROTOCOL)
            
            print('Final save: '+ filename)
            print('Logger killed')
            break
        # if results, put results in the desired shape
        if message=='results':
            # print('Results received')
            index,xyz = data
            Results.Npoints  += xyz.shape[0]
            persample_temp[index] = xyz
            if len(persample_temp)==opt.save_interval:
                LoggerQueue.put(('partial_save',None))
        # if header, ?
        if message=='header':
            # print('I received the header, did not do anything with it.')
            hdr = data




#################################################################################
#%% "Main"

def main(opt): 
    '''
    Python implementation of psfinder.m

    I am avoiding all the ifs related to reading directly igb files. Only iga.gz is possible as input
    Only the method based on the anatomy file is allowed (not on tcc)
    Also, I modified a little bit the options in opts since I could not make sense of all of the options
    This should be improved in the future
    '''
    #
    info = {}
    info['hostname'] = os.uname()[1]
    info['tstart'] = time.time()
    #
    ###################################### Path detection ######################################
    # New method, much faster. It works with the anatomy file
    #(in voxels) and checks the resulting paths against the tcc file.
    ii, path_centers, ndims = psf_pathfinder(opt)   
    #
    # Map indices in mask (full 3D block) to indices in theta (1D list of only the data elements)
    maskind = np.unique(ii.flatten()) #indices of data elements in V
    Map = np.zeros(ndims)
    mx,my,mz = igb.Idx2Coord(maskind,ndims)
    Map[mx,my,mz] = np.arange(0,len(maskind))
    #
    ix,iy,iz = igb.Idx2Coord(ii,ndims)
    path_ii = Map[ix,iy,iz]
    path_ii = np.array(path_ii,dtype = int)

    # For optimized phase integration
    path_end = np.roll(path_ii,-1,axis=-1)
    ##################################### Vm file reading #####################################
    # Only lean format from iga.gz

    print('Time elapsed before opening data:%0.3f s'%(time.time()-info['tstart']))
    #
    tBegin = opt.tBegin
    tEnd = opt.tEnd
    tInt = opt.tInt
    tString = str(tBegin)+':'+str(tInt)+':'+str(tEnd) 
    
    # Create a manager
    # DataManager = mp.Manager()
    with mp.Manager() as DataManager:
        # Create the two Queues
        queueMax = 100
        ProcessingQueue = DataManager.Queue(maxsize=queueMax)#maxsize=20
        LoggerQueue = DataManager.Queue()#maxsize=20
        LoggerQueue.put(('initialize',[opt,info]))

        #Data In
        DataReader = mp.Process(target = Reader,args = (opt,tString,maskind,ProcessingQueue,LoggerQueue,))
        # Data processing
        ProcessingPool = mp.Pool(opt.n_processes,Processing,(ProcessingQueue,LoggerQueue,opt,path_ii,path_end,path_centers,))
        # if opt.n_processes>queueMax:
        [ProcessingQueue.put(('initialize',None))for i in range(opt.n_processes)];
        # Data out
        LoggerProcess = mp.Process(target = Logger,args = (LoggerQueue,))

        LoggerProcess.start()
        # Open and wait for end 
        DataReader.start()
        
        DataReader.join()
        ProcessingPool.close()
        ProcessingPool.join()


if __name__ == '__main__':

    parser = ui.MyParser(description='Parallel implementation of phase singularity detection algorithm. Check https://doi.org/10.1063/1.1497505 for more info.',
                        fromfile_prefix_chars='+',
                        usage = 'python psfinder.py +ParameterFile.par [PARAMETERS] expName')
    
    parser.add_argument('fname_vm',action='store',type=str,
                    help = '.iga.gz file with the transmembrane potentials (vm file).')

    #File I/O
    parser.add_argument('-fname_cell',action='store',type=str,default='model24-30-heart-cell.igb',
                    help = '.igb file with the cell information (i.e. anatomy).')
    parser.add_argument('-fname_out',action='store',type=str,default='-',
                    help = 'Name for the output .mat file (with extension)')
    # parser.add_argument('-fname_tcc',action='store',type=str,default='',
    #                 help = 'tcc igb file. Only used to check the path detection; not used in this version at all')  
    parser.add_argument('-n_processes',
                        action='store',default =10,type=int,
                        help = 'Number of parallel processes. Default 10.')
    parser.add_argument('-save_interval',
                        action='store',default =2500,type=int,
                        help = 'Intervals for partial saving. Default is 2500')
    # Configurations
    parser.add_argument('-ctypes',nargs='+',type=int,
                        default = [113,114,115,116,117,118,119,250],
                        help='Allowed cell types. Default: atrial cells and fibrosis')  
    parser.add_argument('-tBegin','-tb',dest= 'tBegin',
                        action='store',default =2500,type=int,
                        help = 'Initial time (ms). Default 2500')
    parser.add_argument('-tEnd','-te',dest= 'tEnd',
                        action='store',default =-1,type=int,
                        help = 'Final time (ms). Default -1')
    parser.add_argument('-tInt','-ti',dest= 'tInt',
                        action='store',default =1,type=int,
                        help = 'Time interval between samples. Default 1')
    parser.add_argument('-Viso',
                        action='store',default =-40,type=float,
                        help = 'Average amplitude for phase calculation. Can be obtained with estimateViso.py. Default -40.')
    parser.add_argument('-Tau',
                        action='store',default =10,type=int,
                        help = 'Interval between phase comparisons. Default 10 ms')
    parser.add_argument('-iga2igb',dest= 'iga2igb',
                    action='store',default = 'iga2igb',type=str,
                    help = 'Path to the iga2igb executable, if needed. Default no path')
    
    # parser.add_argument('-Nt','-ti',dest= 'tInt',
    #                     action='store',default =10,type=int,
    #                     help = 'Maximum allowed number of trajectories')

    opt = parser.parse_args()

    if opt.fname_out=='-':
        opt.fname_out=opt.fname_vm[:-16]+'_psfinder.mat'

    opt.Tau = int(opt.Tau/opt.tInt) # To adjust the tau
    main(opt)

    #On daint:
    ## srun -C gpu -n1 -c15 -A 's1074' --time=05:00:00 python psfinder_par.py


    '''
    For debugging only

class options:
    def __init__(self,opt='usi'):
        if opt=='daint':
            self.fname_cell = '/users/vgonalve/exec/anatomy/model30/model24-30-heart-cell.igb'
            self.fname_tcc = '/users/vgonalve/exec/psdetection/exp906k71_tcc_afull.igb'
            self.ctypes = np.array([113,114,115,116,117,118,119,250])
            self.fname_vm = '/users/vgonalve/exec/exp906/exp906a05_vm_afull.iga.gz'
            self.fname_out='/users/vgonalve/exec/exp906/tmp/test_psfinder.mat'
            self.n_processes=10
            self.save_interval=50
        elif opt=='wsl':
            self.fname_cell = '/mnt/d/vgmar/model_data/anatomy/model24-30-heart-cell.igb'
            self.fname_tcc = ''
            self.ctypes = np.array([113,114,115,116,117,118,119,250])
            self.fname_vm = '/mnt/d/vgmar/model_data/exp906/exp906k71_short_vm_afull.iga.gz'
            self.fname_out='/mnt/d/vgmar/model_data/tests.mat'
            self.n_processes=1
            self.save_interval=10
        elif opt=='usi':
            self.fname_cell = '/scratch/marques/EX0008/EX0008_FB0_FL200s01_vm_afull.iga.gz'
            self.fname_tcc = '' #useless
            self.ctypes = np.array([113,114,115,116,117,118,119,250])
            self.fname_vm = '/scratch/marques/EX0008/EX0008_FB0_FL200s01_vm_afull.iga.gz'
            self.fname_out='/scratch/marques/EX0008/EX0008_FB0_FL200s01_psfinder.mat'
            self.n_processes=9
            self.save_interval=2000
        self.Tau=10
        self.Viso  = -40 # Mean of the first few seconds of lead (x,y,z) = 250,300,120           
        self.Nt = np.inf#
        self.tBegin = 0
        self.tEnd = 1000
        self.tInt = 1
    # TODO include sampling frequency somewhere
    '''

