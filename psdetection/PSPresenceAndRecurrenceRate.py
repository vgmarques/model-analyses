# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 15:02:31 2021

DESCRIPTION: Organized code with the analyses done for CinC 2021

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Import libraries and make some useful functions for parallelization

import os,sys
import numpy as np
import pandas as pd
import multiprocessing as mp
from itertools import repeat
try:
    import pickle5 as pickle
except:
    import pickle
    
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))
import pslibrary as psl
from RecurrenceComputation import RecurrenceComputation

def PSDFLoop(DF):
    return np.min(DF,axis=0)

def MakeRPs(DM,estimatedAFCL,tBegin,tEnd,sampleShift=5,threshold= 0.15):
    RP, _, recurrenceThreshold, _ = \
                recComp.ComputeRecurrencePlot(DM, 1000,
                                              estimatedAFCL,recurrenceThreshold= threshold)          
    RP  = RP[tBegin//sampleShift:tEnd//sampleShift,:][:,tBegin//sampleShift:tEnd//sampleShift]
    return RP

#%% Configuration of recurrence analysis, paths and convenient variables

# Recurrence analysis
SampleShift = 5
recComp = RecurrenceComputation()
recComp.SampleShift = SampleShift
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1 # Only used if no threshold is passed

# Paths
PSPATH =    '/users/vgonalve/exec/RPAnalyses/review/final'
RECPATH =   '/users/vgonalve/exec/RPAnalyses/recurrence'
SAVEPATH =  '/users/vgonalve/exec/RPAnalyses/review/final'
FILENAME =  'PSPresenceAndRQA_16Positions.pkl'
# Convenient Variables
AFCL_TH = 5
tBegin = 2500
tEnd = 15000
fs = 1000
NProcesses = 18
ArrayRadius = 7
NElectrodes = 16

#%% Get experiment names from the provided paths, exclude some of them
# due to problems in PS detection

experimentNames = []
for file in os.listdir(PSPATH):
    if file.startswith('exp906c') and file.endswith('th0.csv'):
        experimentNames.append(file.split('_')[0])
experimentNames = np.sort(experimentNames)


#%% Prepare output

# The variable below contain a dict with 16 keys per experiment
# Each entry is a dict as well with the RQA results 
# WholeRQA[experiment][catheter][rqa_parameter]

WholeRQA = dict()
WholePercentages = dict() # This variable has a % of intervals with PS per experiment, prior to dilation

# The two variables below contain a dict with 16 keys per experiment
# Each entry has a dict with the RQA values
# GlobalRR_In[experiment][catheter][rqa_parameter]
GlobalRR_In = dict()
GlobalRR_Out = dict()

PSDict_In = dict()
PSDict_Out = dict()

#%% Loop over experiments

for experiment in experimentNames:
    print(experiment)
    
    # Open previously generated distance matrices and estimated AFCLs
    # This is generate with the code: CalcSaveDistanceMatrices.py
    with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'rb') as output:
        DistanceMatrices =  pickle.load(output)
        EstimatedAFCLs =  pickle.load(output)
    meanAFCL = np.mean([EstimatedAFCLs[key] for key in EstimatedAFCLs])
    
    # Open DataFrame with all PS data (>100ms) and distance to catheter centers
    # This is generated with the code: Distance along shell (PSs x Catheters)
    InputDF = pd.read_csv(os.path.join(PSPATH,experiment+'_PSPositionsAndDistances_th0.csv'))
    InputDF = InputDF[InputDF.columns[1:]]
    # Limit DF to time boundaries
    InputDF = InputDF.loc[(InputDF['t']>=tBegin) & (InputDF['t']<tEnd)]  
    InputDF['t'] = InputDF['t']-tBegin # To avoid problems with indexing


    # Calculate the lowest distance between the catheters and PSs at each t
    PSUnder = np.zeros((tEnd-tBegin,16))
    with mp.Pool(10) as pool:
        minDF = pool.starmap(PSDFLoop, zip([InputDF.loc[InputDF['t']==t,:] \
                                            for t in np.unique(InputDF['t'])]))
    minDF = pd.DataFrame(minDF,columns = InputDF.columns)
    times = np.unique(InputDF['t']).astype(int) # Maybe something went wrong in isolated time steps
    minDF = minDF[minDF.columns[5:]] # Only the distances, ignoring specific positions
    
    # Check if any PSs are within ArrayRadius
    PSUnder[times,:] = np.asarray(minDF<=ArrayRadius)
    WholePercentages[experiment] =np.sum(PSUnder,axis=0)/PSUnder.shape[0]*100 # % with PSs for the thole RP
    
    # Make the recurrence part of the analyses
    
    # Set output variables
    PSDict_In[experiment] = np.zeros((tEnd//SampleShift-tBegin//SampleShift,NElectrodes),dtype=bool)
    PSDict_Out[experiment] = np.zeros((tEnd//SampleShift-tBegin//SampleShift,NElectrodes),dtype=bool)
    WholeRQA[experiment] = dict()
    GlobalRR_In[experiment] = dict()
    GlobalRR_Out[experiment] = dict()

    # Parallelize the generation of RPs, which takes some time, but do not erode
    # Since eroding also takes some time, I put it inside the next loop so it
    # could also be in parallel
    
    with mp.Pool(NProcesses) as pool:
        RPDict = pool.starmap(MakeRPs, zip([DistanceMatrices[cat] for cat in DistanceMatrices],
                                           [EstimatedAFCLs[cat] for cat in DistanceMatrices],
                                           repeat(tBegin),repeat(tEnd)))    
    
    # Iterate over catheters
    for cId,cat in enumerate(DistanceMatrices):
        print(cat)
        # Erode RP
        RP = recComp.ErodeRP(RPDict[cId],DistanceMatrices[cat])
        afclLineLenght =int(EstimatedAFCLs[cat]*1000//recComp.SampleShift)

        # RQA with minimal diagonal length (does not make a difference for RR)
        rqa = recComp.ComputeRQA(RP,EstimatedAFCLs[cat],fs,minDiagonalLine=afclLineLenght)
        WholeRQA[experiment][cat] = rqa

        # Get portions of signal with PSs underneath, downsampling to match
        # size of RPs
        blockPresence = PSUnder[:,cId][::SampleShift]

        # Make of1 AFCL size to count presences under this interval as a single block
        if int(meanAFCL*1000)%2==0:
            kernel = np.ones(int(meanAFCL*1000)+1)
        else:
            kernel = np.ones(int(meanAFCL*1000))
        kernel /= len(kernel)

        # Closing operation to merge intervals
        dilatedBlocks = np.convolve(blockPresence, kernel, mode="same")
        dilatedBlocks = dilatedBlocks.astype(bool)

        erodedBlocks = np.convolve(~dilatedBlocks, kernel, mode="same")
        erodedBlocks = ~erodedBlocks.astype(bool)

        erodedBlocks = np.convolve(erodedBlocks, np.ones(2)/2, mode="same").astype(bool) #small dilation to get all moments with blocks
        erodedBlocks = np.roll(erodedBlocks,-1,0) # Adjust sample shift

        erodedBlocks = erodedBlocks[:int((tEnd-tBegin)/SampleShift)]

        PSDict_In[experiment][:,cId] = erodedBlocks

        # Find block intervals
        erodedBlocks = np.append((np.append(0,erodedBlocks)),0)
        blockUp,blockDown =  psl.GetEdges(erodedBlocks.astype(bool))

        RQA_in = dict()
        for bId in range(len(blockUp)):
            lims = np.array([blockUp[bId], blockDown[bId]],dtype=int)
            if lims[1]-lims[0]<=AFCL_TH*meanAFCL*1000//SampleShift: continue # Exclude intervals shorter than 5 AFCL
            rqa = recComp.ComputeRQA(RP[lims[0]:lims[1],:][:,lims[0]:lims[1]],EstimatedAFCLs[cat],fs,minDiagonalLine=afclLineLenght)
            RQA_in[bId] = rqa

        GlobalRR_In[experiment][cat] = RQA_in

        # Do the same thing for intervals w/o PSs
        dilatedBlocks = ~erodedBlocks[1:-1].astype(bool) # Opposite of the PS intervals

        # Small erosion to avoid overlap
        erodedBlocks = np.convolve(~dilatedBlocks,np.ones(5)/5,mode='same')
        erodedBlocks = ~erodedBlocks.astype(bool)

        erodedBlocks = erodedBlocks[:int((tEnd-tBegin)/SampleShift)]

        PSDict_Out[experiment][:,cId] = erodedBlocks
        erodedBlocks = np.append((np.append(0,erodedBlocks)),0)
        blockUp,blockDown =  psl.GetEdges(erodedBlocks.astype(bool))
        RQA_out = dict()
        for bId in range(len(blockUp)):
            lims = np.array([blockUp[bId], blockDown[bId]],dtype=int)
            if lims[1]-lims[0]<=5*meanAFCL*1000//SampleShift: continue
            rqa = recComp.ComputeRQA(RP[lims[0]:lims[1],:][:,lims[0]:lims[1]],EstimatedAFCLs[cat],fs,minDiagonalLine=afclLineLenght)
            RQA_out[bId] = rqa    

        GlobalRR_Out[experiment][cat] = RQA_out

    # Save to avoid losing data
    with open(os.path.join(SAVEPATH,FILENAME),'wb') as output:
        pickle.dump(PSDict_In, output)
        pickle.dump(PSDict_Out, output)
        pickle.dump(GlobalRR_In, output)
        pickle.dump(GlobalRR_Out, output)
        pickle.dump(WholeRQA, output)
        pickle.dump(WholePercentages, output)