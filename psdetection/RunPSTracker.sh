#!/bin/bash -l
#SBATCH --job-name="jobName"
#SBATCH --mail-type=ALL
#SBATCH --mail-user=v.goncalvesmarques@maastrichtuniversity.nl
#SBATCH --time=requestedHours
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=15
#SBATCH --constraint=gpu
#SBATCH --account=s1074
#SBATCH --dependency=singleton

module load daint-gpu
module load cray-python
module load matplotlib


# Paths and filenames
EXPNAME=experimentName
PSFINDERPATH=psFinderPath
BASENAME=baseName
OUTPATH=outPath

# Shortest trajectory allowed
DURATION_TH=durationTh #ms

FNAMEIN=${PSFINDERPATH}/${EXPNAME}_full.psfinder
FNAMEOUT_PSTRACKER=${OUTPATH}/${EXPNAME}.pstracker
BASENAME_CONVERTED=${OUTPATH}/${EXPNAME}_th

FNAMEOUT_CONVERTED=${BASENAME_CONVERTED}${DURATION_TH}.pslist


PSTRACKERPARAMS='--allow_branching
                -cluster_maxdist=4
                -traject_maxdist=10
                -fname_out='${FNAMEOUT_PSTRACKER}
      


# Combine different outputs from psfinder
srun python CombinePSFinderOutput.py $PSFINDERPATH $EXPNAME

# Runs pstracker
srun python PSTracker.py $PSTRACKERPARAMS $FNAMEIN

mkdir ${OUTPATH}

# Converts to .txt (final output)
srun python ConvertPSTrackerOutput.py -fname_out=${BASENAME_CONVERTED}0.pslist ${FNAMEOUT_PSTRACKER}

srun python ConvertPSTrackerOutput.py -MinTrajectoryDuration=${DURATION_TH} \
-fname_out=${BASENAME_CONVERTED}${DURATION_TH}.pslist ${FNAMEOUT_PSTRACKER}
