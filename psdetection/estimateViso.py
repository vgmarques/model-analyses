import sys
import os
import subprocess
import time
import numpy as np
import matplotlib.pyplot as plt

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb




fname_vm = '/users/vgonalve/exec/psdetection/exp906c04_vm_afull.iga.gz'
with subprocess.Popen(["gzip","-dc",fname_vm],stdout=subprocess.PIPE) as gz:
    with subprocess.Popen(['iga2igb',"-x","0:1:-1","-y","0:1:-1","-z","0:1:-1",
			   '-t','0:1:1000','-z','120','-y','300','-x','250',"-","-"],stdin=gz.stdout,stdout=subprocess.PIPE) as iga:#'-z','120','-y','300','-x','250',
        hdr = igb.ReadHeader(iga.stdout.read(1024),opened = True)
        nx,ny,nz,nt = hdr['x'],hdr['y'],hdr['z'],hdr['t']
        dtype = np.short
        csize = nx*ny*nz*nt*dtype().nbytes
        vmf = np.frombuffer(iga.stdout.read(csize),dtype=dtype).reshape((nt,nz,ny,nx))
        val = vmf.squeeze().astype(np.float32)
        val *= hdr['facteur']
        val += hdr['zero']

fig,ax = plt.subplots(1)
ax.plot(val)
plt.show()

print('Viso estimate: %0.3f'%(np.mean(val)))