#%%
import os,sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import multiprocessing as mp
import pickle

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb


## Relevant parameters 

RelevantTrajectories = {}
MinimumDurationTrajectory = 0#100 # ms
SegmentDuration = 1000 # ms
tBegin = 1000

#%% Load all projected PSs per experiment in a certain folder
exp = 'EX0008'
PSPATH = '/mnt/d/vgmar/model_data/%s/psdetection/filtered'%exp# filtered
experimentNames = []
for file in os.listdir(PSPATH):
    if file.startswith(exp) and file.endswith('_PSPositions.csv'):
        experimentNames.append(file.split('_PSPositions.csv')[0])


#%% Segment PS databases per segment

SegmentDFs = {}
for experimentIndex,experimentName in enumerate(experimentNames):

    InputDF = pd.read_csv(os.path.join(PSPATH,experimentName+'_PSPositions.csv'))
    InputDF = InputDF[InputDF.columns[1:]]

    tSteps = np.unique(InputDF['t']).astype(int)
    if tBegin is None:
        tBegin = tSteps.min()
    nSegments = (tSteps.max()-tBegin)//SegmentDuration

    SegDF = []
    for t in range(nSegments):
        SegDF.append(InputDF.loc[(InputDF['t']>=tBegin+t*SegmentDuration)&\
                    (InputDF['t']<tBegin+(t+1)*SegmentDuration),['t','x','y','z','traj']])
    SegmentDFs[experimentIndex] = SegDF

#%% Now, for each segmented dataframe, get median position of relevant trajectories

OutputDF = pd.DataFrame(columns = ['experiment','segment','x','y','z','traj'])

for experimentIndex,experimentName in enumerate(experimentNames):

    for segmentIndex,segment in enumerate(SegmentDFs[experimentIndex]):

        # Get Trajectory durations
        uniqueTrajectories = np.unique(segment['traj'])
        trajectoryDurations = np.zeros(len(uniqueTrajectories))
        for i,traj in enumerate(uniqueTrajectories):
            duration = segment.loc[segment['traj']==traj,['t']]
            trajectoryDurations[i] = duration.max()-duration.min()

        # This is the relevant trajectory
        uniqueTrajectories = uniqueTrajectories[uniqueTrajectories>=MinimumDurationTrajectory]
        for traj in uniqueTrajectories:
            x,y,z = np.mean(np.asarray(segment.loc[segment['traj']==traj,['x','y','z']]),axis=0)
            averageTrajectoryDF = pd.DataFrame({'experiment':[experimentName],
                                               'segment':[segmentIndex],
                                               'x':[int(x)],'y':[int(y)],'z':[int(z)],
                                               'traj':[traj]})
            
            OutputDF = OutputDF.append(averageTrajectoryDF,ignore_index =True)


#%% Convert all x,y,z positions to anatomy indexes
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
# %%
anatomyInds = []

positions = np.asarray(OutputDF[['x','y','z']])
th = 2
for i,row in enumerate(positions):
    x,y,z = row
    idx = np.linalg.norm(Anatomy-np.array(row,int),axis=1)

    anatomyInds.append(np.where(idx==np.min(idx))[0][0])

OutputDF = OutputDF.assign(anatomyIndex = anatomyInds)

#%% Save
outfile = os.path.join(PSPATH,
                '%s_AverageRelevantTrajectoryPosition.csv'%exp)
OutputDF.to_csv(outfile)
print('Saved: %s'%outfile)
