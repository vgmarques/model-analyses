'''
This function is the Python adaptation of psfinder.m
'''


import sys
import os
import subprocess
import time
#from pyccmc import igb_read
import numpy as np
import numpy.matlib as npm
import scipy.io as sio
from scipy.spatial.distance import pdist,cdist,squareform
import pickle

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
from DataClasses import PSCluster,PStrackerResults,PSFinderOptions
import UserInterfaceFunctions as ui




def psf_cluster_points(cpos, maxdist):
    '''
    This function is meant to cluster the PS locations found at a single
    time instant, by whatever method. If everything works as it should,
    the only reason to do this is to recognize vortex filaments as single
    entities. These filaments can be quite long; theoretically they can
    span the whole atria. If we measured the distance to the center of
    such a "cluster" we would need a very large threshold, which would
    effectively merge all PSs in the whole atria into one
    cluster. Therefore we compute the distance to the nearest element
    of the cluster instead. Then we can use a more reasonable distance
    threshold -- theoretically sqrt(3)*(1+eps) should suffice.
    '''
    Nc = cpos.shape[0]
    if Nc>1:
        '''
        Here we consider two PSs "linked" if the distance between them
        is below the threshold. We assign a "cluster label" to each PS.
        This is an arbitrary but unique number. Then we loop over all
        connections to assign the lowest label to both members of the
        connection. The inner loop is vectorized using find() and min().
        The previous order-N method was not correct; it found too many
        clusters.
        '''
        dst = cdist(cpos,cpos)
        linked = dst<maxdist
        #
        clab = np.arange(Nc,dtype = int)#  initial cluster label (maybe start at 1 here exceptionally)
        for i in range(1,Nc):
            jj = np.where(linked[i,:])[0]
            if len(jj)!=0:
                clab[jj] = np.min(clab[jj])
        #
        labels = np.unique(clab)
        Nk = len(labels) #nr of clusters
        centers = np.zeros((Nk,3))#
        clusind = np.zeros(Nc,dtype=int)#  cluster index (consecutive labels)
        #
        for k in range(Nk):
            ii = np.where(clab==labels[k])[0] #!
            clusind[ii] = k
            centers[k,:] = np.mean(cpos[ii,:],axis = 0) # force mean along 1st dim!
    elif Nc==1:
        centers = cpos
        clusind = 0
    else:
        centers = []
        clusind = []
    #
    return centers,clusind

def psf_track_filaments(rps, allow_branching, maxdist):
    '''
    This function creates trajectories by matching entire clusters, looking at
    the shortest distance between any combination of cluster memebrs. It works
    on a list of per-sample results. It returns the same list, with a struct
    member "tras" added to each alement. This gives the trajectory numbers of
    all points, similar to the clusind member, which should be already present.
    In contrast to the previous function it is not supposed to be called in a
    loop; it does its work when all points for all samples have been detected.
    
    psf_track_filaments(results.persample, ...
                        opt.allow_branching, ...
                        opt.traject.maxdist);
    '''
    #
    #rps = results['persample']
    trax = {}
    trax['start'] = -np.ones_like(rps.clusind[0]) # initialize results
    trax['end'] = -np.ones_like(rps.clusind[0])
    rps.tras[0] = rps.clusind[0] #trajectory index for first sample
    
    Ntra = len(np.unique(rps.clusind[0])) # nr of trajectory labels assigned so far
    #
    for t in range(1,len(rps.points)):
        Npcur,nCols_t = rps.points[t].shape
        Npprev,nCols_t_1 = rps.points[t-1].shape
        #
        # If this slice is not empty, but the previous is
        if Npprev==0 and Npcur!=0:
            Nccur  = np.max(rps.clusind[t])+1#np.max(rps[t].clusind)#  nr of clusters in this sample
            Ncprev=-1
        elif Npcur!=0:
            dst = cdist(rps.points[t-1],rps.points[t])
            Ncprev = np.max(rps.clusind[t-1])+1#np.max(rps[t-1].clusind) !!!!
        #
        # If this slide is empty, the trajectory is also empty
        # if Npcur==0:
        #     rps.tras[t] = []
        if Npcur!=0:            
            # If ran from t=0, might have empty slices
            Nccur  = np.max(rps.clusind[t])+1#np.max(rps[t].clusind)#  nr of clusters in this sample
            #
            #link to previous trajectories if possible
            rps.tras[t] = np.ones(Npcur,dtype = int)*np.nan
            if not Ncprev==-1:
                #
                if allow_branching:
                    maxk = Nccur
                else:
                    maxk = np.min([Ncprev, Nccur])
                #
                for k in range(maxk):
                    best = np.min(dst)
                    ind = np.asarray(np.where(dst==best)).T#  best-matching points
                    # Some deal breaker conditions
                    if best>maxdist or len(ind)==0:
                        break 
                    #
                    i,j = ind[0,:] # select one if there are multiple best matches
                    ii = np.where(rps.clusind[t-1]==rps.clusind[t-1][i])[0]# all pts in cluster
                    jj = np.where(rps.clusind[t]==rps.clusind[t][j])[0]
                    #
                    rps.tras[t][jj] = rps.tras[t-1][i].astype(int) #  copy trajectory index
                    print('t=%d cluster %d assigned to tra %d'%(t, rps.clusind[t][j], rps.tras[t-1][i]))
                    #
                    if not allow_branching:
                        print('hey')
                        dst[ii,:] = np.inf # mark previous cluster as used
                    #
                    dst[:,jj] = np.inf # mark current cluster as used
                #end for
            #end if
        #
        #create new trajectories if we ran out of matches
            ii = np.where(np.isnan(rps.tras[t]))[0]#points without tra label
            #
            if len(ii)!=0: #deal with 0x1 empty
                clusters_todo = np.unique(rps.clusind[t][ii])
                for c in clusters_todo:
                    jj = np.where(rps.clusind[t]==c)[0]
                    rps.tras[t][jj] = Ntra
                    if Ntra<len(trax['start']):
                        trax['start'][Ntra] = t#  new type of trajectory info
                    else:
                        trax['start'] = np.append(trax['start'],[t])
                        trax['end'] = np.append(trax['end'],[-1])
                    #trax['end'][Ntra] = 0 # end not yet found
                    print('t=%d cluster %d new tra %d'%(t, c, Ntra))
                    Ntra = Ntra + 1
            #if Npcur==0:
        #Set end time on finished trajectories (it would take a lot of time
        #to figure this out later)
        try:
            tprev  = np.unique(rps.tras[t-1]).astype(int)
        except:
            tprev  = [] # slice empty
        try:
            tcur  = np.unique(rps.tras[t]).astype(int)
        except:
            tcur = [] # slice empty
        ii  = np.where(~np.isin(tprev,tcur))[0]
        if len(ii)!=0:
            trax['end'][tprev[ii]] = t
    ii  = np.where(trax['end']==-1)[0]        
    trax['end'][ii] = t
    return rps,trax,Ntra   
#################################################################################

# def LoadPSFinderOutput(fname_in):
#     DataIn = sio.loadmat(fname_in)['results'][0,0]
#     Results = PStrackerResults()
#     Results.Npoints = DataIn[0].squeeze()
#     persample_temp= DataIn[1]
#     #
#     # Matlab or Python origin (I am not sure this will always work. TODO)
#     if len(persample_temp[0])==1:
#         persample_temp = persample_temp[0]
#         indexCorrection=0
#         persample_temp['points'][0] = persample_temp['points'][0][0,:]
#     else:
#         indexCorrection=1
#     # The core of the results
#     Results.persample = PSCluster()
#     Results.persample.points = persample_temp['points'][0].squeeze()-indexCorrection
#     Results.persample.clusind = np.empty_like(persample_temp['points'][0].squeeze())
#     Results.persample.centers = np.empty_like(persample_temp['points'][0].squeeze())
#     Results.persample.tras = np.empty_like(persample_temp['points'][0].squeeze())
#     #
#     #Other stuff
#     Results.number_of_ps = []#DataIn[2].squeeze()
#     Results.trax = []#DataIn[3].squeeze()
#     #
#     return Results
def LoadPSFinderOutput(fname_in):
    with open(fname_in, 'rb') as input:
        PartialResults = pickle.load(input)
        partialInfo = pickle.load(input)

    # The core of the results
    PartialResults.persample.points = PartialResults.persample.points
    PartialResults.persample.clusind = np.empty_like(PartialResults.persample.points)
    PartialResults.persample.centers = np.empty_like(PartialResults.persample.points)
    PartialResults.persample.tras = np.empty_like(PartialResults.persample.points)
    #
    #Other stuff
    PartialResults.number_of_ps = []#DataIn[2].squeeze()
    PartialResults.trax = []#DataIn[3].squeeze()
    #
    return PartialResults,partialInfo


def main(opt):
    # Convert back from matlab
    print('Loading %s'%opt.fname_in)
    Results,info = LoadPSFinderOutput(opt.fname_in)

    Nt = np.prod(np.shape(Results.persample.points))
    # Detect clusters for each sample
    print('Clustering PSs...')
    Nptotal = 0
    Results.number_of_ps = np.zeros(Nt,dtype=int)
    for T in range(Nt):
        xyz = Results.persample.points[T]#+1 #FIXME Why +1?
        if len(xyz)==0:
            # If no PSs have been detected, skip
            Results.persample.clusind[T] = []
            Results.persample.centers[T] = []
            Results.number_of_ps[T] = 0
        else:
            [centers, clusind] = psf_cluster_points(xyz, opt.cluster_maxdist)
            Results.persample.clusind[T] = clusind
            Results.persample.centers[T]= centers
            Results.number_of_ps[T] = centers.shape[0]
            Nptotal = Nptotal + xyz.shape[0]

    print('Making trajectories...')
    rps, trax, Ntra = psf_track_filaments(Results.persample,opt.allow_branching, opt.traject_maxdist)
    Results.persample = rps
    Results.trax = trax
    Results.Ntra = Ntra

    #### Save ####
    # sio.savemat(opt.fname_out,{'results':Results})
    with open(opt.fname_out, 'wb') as output:
        pickle.dump(Results, output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(info, output, pickle.HIGHEST_PROTOCOL)
    print('Saved '+opt.fname_out)

if __name__ == '__main__':

    parser = ui.MyParser(description='Parallel implementation of phase singularity detection algorithm. Check https://doi.org/10.1063/1.1497505 for more info.',
                        fromfile_prefix_chars='+',
                        usage = 'python psfinder.py +ParameterFile.par [PARAMETERS] expName')

    parser.add_argument('fname_in',action='store',type=str,
                help = 'Name of the PsFinder output file.')
    parser.add_argument('-fname_out',action='store',type=str,default='-',
                    help = 'Name for the output .mat file (with extension)')
    parser.add_argument('--allow_branching',action='store_true',
                    help = 'Whether to allow trajectory branching. Recommended')
    parser.add_argument('-cluster_maxdist',action='store',type=int,default=4,
                    help = 'Maximum distance for clustering (same time instant). Default 4.')
    parser.add_argument('-traject_maxdist',action='store',type=int,default=10,
                    help = 'Maximum distance for trajectories (different time instants). Default 10.')
    parser.add_argument('-n_processes',
                        action='store',default =10,type=int,
                        help = 'Number of parallel processes. Default 10.')

    opt = parser.parse_args()

    if opt.fname_out=='-':
        opt.fname_out=opt.fname_in[:-13]+'_psfinder.mat'
    main(opt)

''' Only for debugging

class options:
    def __init__(self,host='daint'):
        if host=='daint':
            self.fname_in='/users/vgonalve/exec/psdetection/exp906c04_psfinder.mat'
            self.fname_out='/users/vgonalve/exec/psdetection/exp906c04_pstracker.mat'
            self.allow_branching = True
            self.cluster_maxdist = 4   #max distance (mesh steps) for clustering
            self.traject_maxdist = 10 # max distance (mesh steps) for tracking
        elif host=='wsl':
            self.fname_in='/mnt/d/vgmar/model_data/exp906/pstracker/Control/exp906c74_psfinder_vgm.mat'
            self.fname_out='/mnt/d/vgmar/model_data/exp906/pstracker/Control/test.mat'
            self.allow_branching = True
            self.cluster_maxdist = 4   #max distance (mesh steps) for clustering
            self.traject_maxdist = 10 # max distance (mesh steps) for tracking
        elif host=='usi':
            self.fname_in='/home/marques/exec/marques/psdetection/exp906c13_psfinder_full.mat'
            self.fname_out='/home/marques/exec/marques/psdetection/texp906c12_pstracker_full.mat'
            self.allow_branching = True
            self.cluster_maxdist = 4   #max distance (mesh steps) for clustering
            self.traject_maxdist = 10 # max distance (mesh steps) for tracking
opt = options('usi')
print(opt.fname_in)
#main(opt)

'''