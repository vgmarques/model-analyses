# -*- coding: utf-8 -*-
"""
Created on Sat Apr  3 10:36:01 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import numpy as np
import os,sys
# import scipy.io as sio
import pickle5 as pickle
import matplotlib.pyplot as plt
# from numba import jit
from scipy.spatial.distance import cdist,squareform
# import multiprocessing
# from joblib import Parallel, delayed
# import time
# import scipy.signal as sig
import pandas as pd

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
sys.path.append(os.path.join(upperFolder,'recurrence'))
import IgbHandling as igb
# from CatheterObjects import CatheterClass,CatheterGroup
import egm_processing as egmp
import quick_visualization as qv
# from RecurrenceComputation import RecurrenceComputation
# from detect_peaks import detect_peaks


import vtk
# from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray,vtk_to_numpy
from StandardVTKObjects import *
# from DataClasses import PSCluster,PStrackerResults
from RecurrenceComputation import RecurrenceComputation


expName = 'exp906c74'

#%%
psPath = 'D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered'#'D:\\vgmar\\Documents\\GitHub\\USI\\model-analyses\\psdetection'
scale = 0.2
#exp906c74_VGM or ex906c74_Ali exp906c74_vgm_20
# exp906cc##-PS_th100

# PSpos,Traj,trajDurations = egmp.MakeTrajectoryDicts(os.path.join(psPath,'exp906c74_VGM.txt'),scale)

#Test to work with dataframes instead
PS_DF = pd.read_csv(os.path.join(psPath,expName+'_pstracker_th100.txt'),sep=' ',
                    dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                 header = 1,names=['t','x','y','z','traj'],engine='c',
                 skipinitialspace=True)

PS_DF['x'] *= scale
PS_DF['y'] *= scale
PS_DF['z'] *= scale

PS_DF = PS_DF.query('t<=10000')

#Make traj durations
unTraj = np.unique(PS_DF['traj'])
unTimes = np.unique(PS_DF['t'])
initTime=unTimes[0]

trajDurations = np.zeros((len(unTraj),2),dtype=int)
trajPresence = np.zeros((len(unTraj),len(unTimes)))

for i,traj in enumerate(unTraj):
    traj_df = PS_DF.query('traj=='+str(traj))
    uniqueTimes = np.unique(traj_df['t'])
    trajDurations[i,:] = [uniqueTimes.min(), uniqueTimes.max()]
    lims = trajDurations[i,:] - initTime
    trajPresence[i,lims[0]:lims[1]+1] +=1

    
numSimPSs=np.sum(trajPresence,axis=0)

_,_ = qv.SimultaneousPSs(trajDurations,[unTimes.min(),unTimes.max()])
#%% Get the electrode Data
fnameElec = 'D:\\vgmar\\model_data\\exp906\\electrode_data\\Control\\'+expName+'_CatheterData.catG'#'/mnt/d/vgmar/model_data/exp906/electrode_data/Control/exp906c74_CatheterData.catG'#
with open(fnameElec, 'rb') as output:
    EGroup = pickle.load(output)
Fields = list(EGroup.Catheters.keys())
 
CatheterPatchesClose = list()
CatheterPatchesNear = list()
for j,key in enumerate(EGroup.Catheters):
    elec = EGroup.Catheters[key]
    # EGroup.Catheters[key].Electrodes=EGroup.Catheters[key].Catheters
    # Make lists of patches (close and near) for plotting
    elec.GetTissuePatch(elec.CatheterCenterIndex,EGroup.Anatomy,radiusMultiplier=1,geodesicThreshold = 7.5)
    CatheterPatchesClose.append(elec.Patch+elec.GlobalPatchCenter)
    elec.GetTissuePatch(elec.CatheterCenterIndex,EGroup.Anatomy,radiusMultiplier=1,geodesicThreshold = 15)
    CatheterPatchesNear.append(elec.Patch+elec.GlobalPatchCenter)

#%% Check distance from electrode center

## Test to upgrade this algorithm to put the distance

proximityDict = egmp.GetDistancsPSCatheter(EGroup,PS_DF,
                                           initialTime=trajDurations.min(),
                                           endTime=trajDurations.max())

ClosePresence,NearPresence = egmp.PSProximity(proximityDict,trajDurations,thresholds=[7,15])



#%% Some plots
#Plot Signals
qv.HdGridPlot(EGroup.Catheters['PLA'],time_interval = [2500,10000],t = 7550/1000)


# Plot PS presence
fig,ax = qv.PSDistance(proximityDict,trajDurations,[7,15])
ax[0,0].set_xlim([2500,10000])
qv.PSPresence(EGroup,ClosePresence,NearPresence,msAxis=True,initialTime=2500)

# Plot survival of rotors
qv.PSSurvival(trajDurations,msAxis=True)

#%% Make Heatmap


HM=egmp.MakeHeatmap(EGroup.Anatomy,PS_DF.query('t<=10000'))

renWin = qv.PSHeatmap(EGroup.Anatomy,HM,filter=False)

#%%
def MakePSActors(nparray,scale,sphereColor = (1,1,1)):
    nparray = nparray*scale
    nCoords = nparray.shape[0]
	#
    verts = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
	#
    PSData = vtk.vtkPolyData()
	#
    verts.SetData(numpy_to_vtk(nparray))
    cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
						   np.arange(nCoords, dtype = np.int64)]).T.flatten()
    cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
    PSData.SetPoints(verts)
    PSData.SetVerts(cells)
	#
    PSmapper = vtk.vtkPolyDataMapper()
    PSmapper.SetInputDataObject(PSData)
	#
    PSActor = vtk.vtkActor()
    PSActor.SetMapper(PSmapper)
    # 	PSActor.GetProperty().SetRepresentationToPoints()
    PSActor.GetProperty().SetRepresentationToSurface()
    PSActor.GetProperty().SetColor(sphereColor)
    PSActor.GetProperty().RenderPointsAsSpheresOn()
    PSActor.GetProperty().SetPointSize(10)
	#
    return PSActor,PSData

catCenters = np.zeros((len(EGroup.Catheters.keys()),3))
for i,key in enumerate(EGroup.Catheters.keys()):
    elec = EGroup.Catheters[key]
    catCenters[i,:] = elec.CatheterCenter 
    
trajDiff = trajDurations[:,1]-trajDurations[:,0]

ind = np.where(trajDiff==np.max(trajDiff))[0][0]
ind=58
test = PS_DF.query('traj==%i'%unTraj[ind])

AnatomyActor,_ = MakePSActors(EGroup.Anatomy,1)
PSActor,_ = MakePSActors(np.asarray(pd.DataFrame(test,columns=['x','y','z'])),
                         1,sphereColor=(0,1,1))

N =2
print(Fields[N])
# PatchActor,_ = MakePSActors(CatheterPatchesNear[N], 1,sphereColor = (1,0,1))
# PatchActor2,_ = MakePSActors(CatheterPatchesClose[N], 1,sphereColor = (1,0,0))


Sphere = vtk.vtkSphereSource()
Sphere.SetCenter(catCenters[N,:])
Sphere.SetRadius(15)
# Mapper
SphereMapper = vtk.vtkPolyDataMapper()
SphereMapper.SetInputConnection(Sphere.GetOutputPort())
# Actor
SphereActor = vtk.vtkActor()
SphereActor.SetMapper(SphereMapper)
SphereActor.GetProperty().SetOpacity(0.5)
SphereActor.GetProperty().SetColor((1,0,1))

renderer = vtk.vtkRenderer()
renderer.AddActor(AnatomyActor)
# renderer.AddActor(PatchActor)
# renderer.AddActor(PatchActor2)
# renderer.AddActor(SphereActor)
renderer.AddActor(PSActor)



renderer.ResetCamera()

renWin = OneViewRenderWindow(renderer,windowSize = [1800,800],
                        OffScreenRendering = False)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(renWin)
renWin.Render()
interactor.Start()


#%% Animate activations
from matplotlib import animation

code = 'ILA_2'
initialTime=0
endTime=7500
tStep=5
catheter = EGroup.Catheters[code]
catheter.Electrodes=catheter.Catheters
#%%
RC = RecurrenceComputation()
activationPhase,CLs = egmp.ComputeActivationPhaseSignal(catheter,catheter.Activations)
Data = activationPhase



Xpoints = np.unique(catheter.Electrodes[:,0])
Ypoints = np.unique(catheter.Electrodes[:,1])

Data = Data[initialTime:endTime,:]
Data = Data[::tStep,:]

fig, ax = plt.subplots(1,figsize=(9.5,8))
# ax.set(xlim=(Xpoints.min(),Xpoints.max()),ylim=(Ypoints.min(),Ypoints.max()))
ax.set(xlim=(-8,8),ylim=(-6,6))
ax.set_xticks([-6,-2,0,2,6]); ax.set_xticklabels([-6,-2,0,2,6],fontsize = 18)
ax.set_yticks([-4.5,-1.5,0,1.5,4.5]); ax.set_yticklabels([-4.5,-1.5,0,1.5,4.5],fontsize = 18)

ax.autoscale(False)
ax.set_xlabel('Relative position (mm)',fontsize = 20)
ax.set_ylabel('Relative position (mm)',fontsize = 20)
    
# cax = ax.scatter(Xpoints,Ypoints,
#                     c = Data[:-1,:-1,1000],
#                     cmap = qv.phase_cmap,vmin = -np.pi,vmax = np.pi)
cax = ax.scatter(catheter.Electrodes[:,0],catheter.Electrodes[:,1],
                   c = Data[10,:],s=10000,marker='s',
                   vmin=-np.pi,vmax=np.pi,cmap=qv.phase_cmap)


cbar = fig.colorbar(cax, ticks=[-np.pi, 0, np.pi])
cbar.set_label('Phase (rad)', fontsize = 24)
cbar.ax.set_yticklabels(['$-\pi$', '0', '$+\pi$'], fontsize = 24)   
fig.suptitle('Frame %s '%(initialTime+2500), fontsize = 28)

def animSP(i,tStep,initialTime,cax,fig,Data):
    cax.set_array(Data[int((i)+initialTime),:].flatten())
    fig.suptitle('Time (ms) %s '%((i*tStep)+initialTime+2500), fontsize = 28)
    return fig

anim = animation.FuncAnimation(fig, animSP, interval = 60, frames=int(Data.shape[0]),
                           repeat_delay = 200,fargs=(tStep,initialTime,cax,fig,Data))
save=1
if save:
    writer = animation.FFMpegWriter(fps = 20)
    anim.save(code+'.mp4', writer=writer)
else:
    plt.show()

#%% Specific activation snapshots

data,hdr = igb.Load('D:\\vgmar\\model_data\\exp906\\exp906c74f_ve_egm_endo_epi.igb',returnRawSignal=True)
data = data[2500:10000,:]
data = data*hdr['facteur']+hdr['zero']
test = data[:,catheter.PatchIndexes]



qv.PlotSnapshot3D(catheter.Patch,test,t=5000)


#%% Explore individual signals

signals = EGroup.Catheters['ILA_1'].EGMData.T
fs = EGroup.Catheters['ILA_1'].SamplingFrequency

signals_on = signals[:,6464:7102]
signals_close = signals[:,5177:6464]

fig,ax  =plt.subplots(2,1)
ax[0].plot(signals_on.T)
ax[1].plot(signals_close.T)

freq1,PxxOn = sig.welch(signals_on,fs,nfft=2048)
freq2,PxxClose = sig.welch(signals_close,fs,nfft=2048)

fig,ax = plt.subplots(2,1,sharex=True)
DF = np.zeros((PxxOn.shape[0],2))
for i in range(PxxOn.shape[0]):
    ax[0].plot(freq1,PxxOn[i,:])
    DF[i,0] = freq1[np.where(PxxOn[i,:]==np.max(PxxOn[i,:]))[0][0]]
    ax[1].plot(freq2,PxxClose[i,:])
    DF[i,1] = freq1[np.where(PxxClose[i,:]==np.max(PxxClose[i,:]))[0][0]]

    
ax[0].set_xlim([0,20])

#%% Explore linear (?) relationship between number of waves and wave generation
trajDurations = trajDurations.astype(int)
totalTime = np.max(trajDurations)-np.min(trajDurations)
initialTime = np.min(trajDurations)

NumberOfWaves = np.zeros((len(trajDurations),totalTime))

test = np.zeros_like(trajDurations)
for i in range(trajDurations.shape[0]):
    NumberOfWaves[i,
                  trajDurations[i,0]-initialTime:\
                      trajDurations[i,1]-initialTime]= 1  
        
NumberOfWaves = np.sum(NumberOfWaves,axis=0)

changeTimes = np.unique(trajDurations[:,0])

WaveGenerationRate=np.zeros(totalTime)

CreatedWaves = [len(np.where(trajDurations[:,0]==i)[0]) for i in changeTimes]

WaveGenerationRate = CreatedWaves[:-1]/np.diff(changeTimes)
    
test = NumberOfWaves[changeTimes-initialTime]

#%% 

#%%
# PSPlot = PsPresence
# N = PSPlot.shape[0]

# Fields = ['RPV', 'LPV', 'PLA', 'ILA_1', 'ILA_2', 'ILA_3', 'ALA', 'LA_Roof', 'LA_AS', 'LAA', 
#           'SRA','IRA',  'RAA', 'PECT', 'CSO']
# originalFields = list(EGroup.Electrodes.keys())

# Indexes = [np.where(np.isin(originalFields,f))[0][0] for f in Fields]
# PSPlot = PSPlot[:,Indexes]

# FieldNames = ['Right PVs','Left PVs',  'Posterior LA',
#     'Inferior LA_1','Inferior LA_2','Inferior LA_3',
#     'Anterior LA','LA Roof','Antero-Septal LA','LA Appendage',
#     'Superior RA','Inferior RA',
#     'RA Appendage','Pectinate muscles','CS opening']

# recurrencesFile = 'D:\\vgmar\\Documents\\Bitbucket\\Stef-Software\\vgm-scripts\\exp906k71_RecurrencePresence.mat'

# R = sio.loadmat(recurrencesFile)['BinaryRecurrence'][:2500,:]
# R[np.isnan(R)] =0

# regions = np.asarray([sig.resample(R[::5,i],R.shape[0]) for i in range(R.shape[1])]).T
# regions[regions<0.5] = 0
# regions[regions>=0.5] = 1

# onsets = [detect_peaks(np.diff(regions[:,i])) for i in range(regions.shape[1])]

# toPlot = list()
# for i,C in enumerate(onsets):
#     tmp = list()
#     for peak in C:
#         if regions[peak+1,i]==1:
#             if peak==C[-1]:
#                 tmp.append(regions.shape[0]-1)
#                 tmp.append(peak)
#             else:
#                 tmp.append(peak)
#         else:
#             if len(tmp)==0:
#                 tmp.append(0)
#                 tmp.append(peak)
#             else:
#                 tmp.append(peak)
#     toPlot.append(tmp)


# fig,AX = plt.subplots(15,1,sharex = True,figsize = [15,10])
# timeAxPS  = np.arange(0,PSPlot.shape[0]/1000,1/1000)
# timeAxRP  = np.arange(0,R.shape[0]/1000,1/1000)

# for i in range(len(toPlot)):
#     AX[i].plot(timeAxPS,PSPlot[:,i],'k',linewidth = 2)#/PSPlot[:,i].max()
#     # Npairs = len(toPlot[i])//2
#     # for k in range(Npairs):
#     #     AX[i].axvspan(timeAxRP[toPlot[i][k*2]],timeAxRP[toPlot[i][k*2+1]],color = 'lightgray')
#     a = np.where(R[:,i])[0]
#     AX[i].vlines(timeAxRP[a],0,np.max(PSPlot),color = 'gray',alpha= 0.5)
#     AX[i].spines['top'].set_visible(False)
#     AX[i].spines['right'].set_visible(False)
#     AX[i].spines['bottom'].set_visible(False)
#     # AX[i].set_ylim([0,1])
#     AX[i].set_ylim([0,np.max(PSPlot)])
#     # AX[i].set_yticks([0.5])
#     AX[i].set_yticks([np.max(PSPlot)/2])
#     AX[i].set_yticklabels([FieldNames[i]],fontsize = 16)
#     AX[i].grid()

# AX[i].set_xlabel('Time (s)',fontsize = 20)
# AX[i].set_xticks(np.linspace(0,R.shape[0]/1000,11))
# AX[i].set_xticklabels(np.linspace(0,R.shape[0]/1000,11),fontsize = 20)
# fig.suptitle('PS presence x Recurrence', fontsize = 24)
# fig.tight_layout()


# #%%
# # Project onto endocardium
# '''
# def ProjectPS(points,anatomy):
#     distances = cdist(anatomy,points)
#     minDistances = [np.where(distances[:,i]==np.min(distances[:,i]))[0][0] for i in range(points.shape[0])]
#     return anatomy[minDistances,:]

# # @jit(nopython=True,parallel=True)
# # def ProjectPS_Numba(X,Y):
# #     M1 = X.shape[0]
# #     M2 = Y.shape[0]
# #     D = np.zeros((M1, M2))
# #     for i in range(M1):
# #         for j in range(M2):
# #             d = 0.0
# #             for k in range(3):
# #                 tmp = X[i, k] - Y[j, k]
# #                 d += tmp * tmp
# #             D[i, j] = np.sqrt(d)
# #     minDistances = [np.where(D[:,i]==np.min(D[:,i]))[0][0] for i in range(X.shape[0])]
# #     return minDistances

# # @jit
# # def FindPSPresence(projectedSPs,ElectrodePatches):
    
# #     PsPresence = np.zeros((len(projectedSPs),len(ElectrodePatches)))
# #     for i in range(len(projectedSPs)):        
# #         for j,Patch in enumerate(ElectrodePatches):
# #             presence = np.isin(projectedSPs[i][:,0],Patch[:,0])*\
# #                         np.isin(projectedSPs[i][:,1],Patch[:,1])*\
# #                         np.isin(projectedSPs[i][:,2],Patch[:,2])
# #             PsPresence[i,j] = np.sum(presence)
# #     return PsPresence



# num_cores = multiprocessing.cpu_count()

# myList = np.array_split(nonUniqueList[:,0],10000)

# anatomy = EGroup.Anatomy
# ti = time.time()
# print('Started calculating distances')
# ProjectedSPs = Parallel(n_jobs=2)(delayed(ProjectPS)(i,anatomy) for i in myList)
# print('Finished calculating distances')
# print('Elapsed Time: %0.3f s'%(time.time()-ti))

# ContinuousProjectedSPs = ProjectedSPs[0]
# for i in range(1,len(ProjectedSPs)):
#     ContinuousProjectedSPs = np.append(ContinuousProjectedSPs,ProjectedSPs[i],axis=0)
# '''

# #%%

# def Points3D(Points,color = (1,1,1),radius = 5):
#     import vtk
#     from vtk.util.numpy_support import numpy_to_vtk
#     nparray = nparray*scale
#     nCoords = nparray.shape[0]
#     nElem = nparray.shape[1]
#     #
#     verts = vtk.vtkPoints()
#     cells = vtk.vtkCellArray()
#     scalars = None
#     #
#     PSData = vtk.vtkPolyData()
#     #
#     verts.SetData(numpy_to_vtk(nparray))
#     cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
#     					   np.arange(nCoords, dtype = np.int64)]).T.flatten()
#     cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
#     PSData.SetPoints(verts)
#     PSData.SetVerts(cells)
#     #
#     PSmapper = vtk.vtkPolyDataMapper()
#     PSmapper.SetInputDataObject(PSData)
#     #
#     PSActor = vtk.vtkActor()
#     PSActor.SetMapper(PSmapper)
#     PSActor.GetProperty().SetRepresentationToPoints()
#     PSActor.GetProperty().SetColor(color)
#     PSActor.GetProperty().RenderPointsAsSpheresOn()
#     PSActor.GetProperty().SetPointSize(radius)

#     renderer = vtk.vtkRenderer()
#     renderer.AddActor(PSActor)
#     renderer.ResetCamera()
    
#     renWin = OneViewRenderWindow(renderer,windowSize = [1800,800],
#                             OffScreenRendering = False)
#     interactor = vtk.vtkRenderWindowInteractor()
#     interactor.SetRenderWindow(renWin)
#     renWin.Render()
#     interactor.Start()
# 			
    