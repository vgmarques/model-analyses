# -*- coding: utf-8 -*-
"""
Created on Thu May 27 11:26:28 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
#%%
import os,sys
import numpy as np
import pickle
upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
from DataClasses import PSCluster,PStrackerResults
import UserInterfaceFunctions as ui

#%%
        
def LoadPSFinderOutput(fname_in):
    with open(fname_in, 'rb') as input:
        PartialResults = pickle.load(input)
        partialInfo = pickle.load(input)
    #
    # The core of the results
    PartialResults.persample.points = np.asarray(PartialResults.persample.points,dtype=object)
    PartialResults.persample.tsteps = np.asarray(PartialResults.persample.tsteps,dtype=object)
    PartialResults.persample.clusind = np.ones_like(PartialResults.persample.points)*np.nan
    PartialResults.persample.centers = np.ones_like(PartialResults.persample.points)*np.nan
    PartialResults.persample.tras = np.ones_like(PartialResults.persample.points)*np.nan
    #
    #Other stuff
    PartialResults.number_of_ps = []#DataIn[2].squeeze()
    PartialResults.trax = []#DataIn[3].squeeze()
    #
    return PartialResults,partialInfo



#



#%%
def main(args):
    arr = []
    for file in os.listdir(args.inputFolder):
            if file.startswith(args.basename) and file.endswith('.psfinder'): #before: .mat
                arr.append(file)
    arr = np.sort(arr)
    if len(arr)==0: RuntimeError('No psfinder file found')
    globalResults = PStrackerResults()            
    globalResults.persample = PSCluster()
    globalResults.number_of_ps = []
    globalResults.trax = []
    print(arr)
    for fname in arr:
        print(fname)
        fname_in = os.path.join(args.inputFolder,fname)
        PartialResults,partialInfo = LoadPSFinderOutput(fname_in)

        if not PartialResults==-1:
            for t in range(len(PartialResults.persample.points)):
                globalResults.persample.points.append(PartialResults.persample.points[t])
                globalResults.persample.tsteps.append(PartialResults.persample.tsteps[t])
                globalResults.persample.clusind.append(PartialResults.persample.clusind[t])
                globalResults.persample.centers.append(PartialResults.persample.centers[t])
                globalResults.persample.tras.append(PartialResults.persample.tras[t])

    # info={'error':'Merged file misses the info, FIXME'}
    # OutputResults = {'results':globalResults,**info}
    # OutputResults = {'results':globalResults}
    outputResults = PStrackerResults()            
    outputResults.persample = PSCluster()
    outputResults.number_of_ps = []
    outputResults.trax = []
    visited = []
    for t in np.argsort(globalResults.persample.tsteps):
        if t in visited: continue
        outputResults.persample.points.append(globalResults.persample.points[t])
        outputResults.persample.tsteps.append(globalResults.persample.tsteps[t])
        outputResults.persample.clusind.append(globalResults.persample.clusind[t])
        outputResults.persample.centers.append(globalResults.persample.centers[t])
        outputResults.persample.tras.append(globalResults.persample.tras[t])
        visited.append(t)

    # sio.savemat(os.path.join(args.inputFolder,args.basename+'_full.mat'),OutputResults)
    with open(os.path.join(args.inputFolder,args.basename+'_full.psfinder'), 'wb') as output:
        pickle.dump(outputResults, output, pickle.HIGHEST_PROTOCOL)
        pickle.dump(partialInfo, output, pickle.HIGHEST_PROTOCOL)

    print('Wrote file '+os.path.join(args.inputFolder,args.basename+'_full.psfinder'))
    print('Beware of running this function twice! Make sure to delete the previous _full.mat file before,\
        or it will get appended to the end of the new file.')

if __name__ == '__main__':

    parser = ui.MyParser(description='Merge PSFinder.py output',
                        fromfile_prefix_chars='+',
                        usage = 'python CombinePSFinderOutput.py inputFolder basename')

    parser.add_argument('inputFolder',action='store',type=str,
                    help = 'Folder in which to search the files beginning with basename.')   
    parser.add_argument('basename',action='store',type=str,
                    help = 'Basename of the individual part files.')



    opt = parser.parse_args()
    main(opt)

''' For debug only
class options:
    def __init__(self,host='daint'):
        if host=='wsl':
            self.basename = 'test_psfinder'
            self.inputFolder = 'D:\\vgmar\\model_data\\psfinder_test'
        elif host=='usi':
            self.basename='exp906c13_psfinder'
            self.inputFolder='/scratch/marques/psdetection'
            self.tBegin=0
        else:
            self.basename = 'exp906a71'
            self.inputFolder = '/users/vgonalve/exec/PSDetection/psdetection'
args = options('usi')
'''