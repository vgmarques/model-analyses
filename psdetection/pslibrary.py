# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 14:59:55 2021

DESCRIPTION: This library has some of the common functions I use for the analy-
sis of phase singularities and trajectories

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os,sys
import numpy as np
import pandas as pd

#%% Auxiliary
def GetEdges(signal):
    signal = np.append((np.append(0,signal.astype(bool))),0)
    blockUp = np.where(np.diff(signal)>0)[0]
    blockDown = np.where(np.diff(signal)<0)[0]-1
    
    return blockUp,blockDown

def readPSFile(experiment,path,threshold=0,scale=0.2):
    PS_DF = pd.read_csv(os.path.join(path,experiment+'_pstracker_th%s.txt'%threshold),sep=' ', 
                        dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                        header = 1,names=['t','x','y','z','traj'],engine='c',
                        skipinitialspace=True,skiprows=0)
    
    ## Adjust scale
    PS_DF['x'] *= scale
    PS_DF['y'] *= scale
    PS_DF['z'] *= scale
    
    return PS_DF