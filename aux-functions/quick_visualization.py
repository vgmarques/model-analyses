# -*- coding: utf-8 -*-
"""
Created on Tue May 11 11:47:25 2021

DESCRIPTION: Quick visualization library, with many functions to plot in 
2D and 3D

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.gridspec import GridSpec
from matplotlib import animation
from copy import deepcopy

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))

import IgbHandling as igb
from CatheterObjects import CatheterClass,CatheterGroup

# from detect_peaks import detect_peaks

import vtk
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray,vtk_to_numpy
from StandardVTKObjects import *
from igbhexagrid import IGBUnstructuredGrid 

#%% General 2D
def QuickAdjustPlot(fig,ax,xlabel = 'xlabel',ylabel = 'ylabel',legend=True):
    fig.set_figwidth(14)
    fig.set_figheight(8)

    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_linewidth(2)
    ax.spines['left'].set_linewidth(2)

    ax.tick_params(labelsize=18)
    ax.set_xlabel(xlabel,fontsize = 20)
    ax.set_ylabel(ylabel,fontsize = 20)

    if legend:
        ax.legend(loc=2,fontsize = 16)

    fig.tight_layout()

    return fig,ax

#%% 2D

#HDGrid plot

def HdGridPlot(Catheter,timeInterval = None,t = None,plotActivations = False,plotPhase = False):
    
    fs = Catheter.SamplingFrequency
    if plotPhase:
        sig = Catheter.PhaseSignal
        if sig is None: RuntimeError('Please calculate the phase signal.')
    else:
        sig = Catheter.EGMData
    
    # Make time axis
    if timeInterval is None:
        time = np.linspace(0,sig.shape[0]/fs,sig.shape[0])
    else:
        sig = sig[timeInterval[0]:timeInterval[1],:]
        time = np.linspace(timeInterval[0]/fs,timeInterval[1]/fs,sig.shape[0])

    if plotActivations:
        activationsToPlot = deepcopy(Catheter.Activations)
        if timeInterval is not None:
            for i in range(len(activationsToPlot)):
                tmpActs = np.asarray(activationsToPlot[i])
                inds = np.where((tmpActs>=timeInterval[0])*(tmpActs<timeInterval[1]))
                activationsToPlot[i] = tmpActs[inds]-timeInterval[0]
    
    # Make labels
    if sig.shape[1]!=16 and sig.shape[1]!=64:
        print('Wrong number of signals')
        return -1
    elif sig.shape[1]==16:
        names = ['A4','A3','A2','A1',
                 'B4','B3','B2','B1',
                 'C4','C3','C2','C1',
                 'D4','D3','D2','D1']
    elif sig.shape[1]==64:
        names = ['A8','A7','A6','A5','A4','A3','A2','A1',
                 'B8','B7','B6','B5','B4','B3','B2','B1',
                 'C8','C7','C6','C5','C4','C3','C2','C1',
                 'D8','D7','D6','D5','D4','D3','D2','D1']   
        
    # Make Figure
    fig,ax = plt.subplots(4,4,sharex = True,sharey = True,figsize = [16,10])
    ref_plots = {}
    ref_vlines = {}
    
    for i in range(sig.shape[1]):
        ref_plots[i] = ax[3-i%4,i//4].plot(time,sig[:,i],'k',linewidth = 2)
        if t is not None:
            ref_vlines[i] = ax[3-i%4,i//4].axvline(t/1000,color = 'r',linestyle = 'dashed',linewidth = 2)
    
        ax[3-i%4,i//4].spines['right'].set_visible(False);
        ax[3-i%4,i//4].spines['top'].set_visible(False);
        ax[3-i%4,i//4].spines['bottom'].set_linewidth(2);
        ax[3-i%4,i//4].spines['left'].set_linewidth(2);
        ax[3-i%4,i//4].text(time[-2],0.9*np.nanmax(sig),names[i],color = 'gray',fontsize = 22)
        
        if plotActivations:
            ax[3-i%4,i//4].vlines(time[activationsToPlot[i]],np.nanmin(sig),np.nanmax(sig),
                                  color = 'b',linestyle = 'solid',linewidth = 1) #TODO: when using timeIntervals, this crashes
        
        if i%4==0:
            ax[3-i%4,i//4].set_xlabel('Time (s)',fontsize = 20)            
            ax[3-i%4,i//4].tick_params(labelsize = 16)            
            
        if i//4==0:
            ax[3-i%4,i//4].set_ylabel('Ampl. (mV)',fontsize = 20)
            ax[3-i%4,i//4].tick_params(labelsize = 16)   
            if plotPhase:
                ax[3-i%4,i//4].set_yticks([-np.pi,0,np.pi])
                ax[3-i%4,i//4].set_yticklabels([r'$-\pi$',0,r'$\pi$'])
                ax[3-i%4,i//4].set_ylabel('Phase (rad)',fontsize = 20)

    fig.tight_layout()

def HDGridPlot2D(signal,timeStep,colormap='gnuplot',
                cbarLabel=None,cbarTicks=None,cbarTickLabels=None):
    xi = np.array([-6., -2.,  2.,  6., -6., -2.,  2.,  6., -6., -2.,  2.,  6., -6., -2.,  2.,  6.])
    yi = np.array([-4.5, -4.5, -4.5, -4.5, -1.5, -1.5, -1.5, -1.5,  1.5,  1.5,  1.5,1.5,  4.5,  4.5,  4.5,  4.5])

    fig, ax = plt.subplots(1,figsize=(7.2,5.75))

    ax.set(xlim=(yi[0]-1.5,yi[-1]+1.5), ylim=(xi[0]-2.5,xi[-1]+2.5))
    ax.autoscale(False)

    ax.set_xlabel('Position (mm)',fontsize = 18)
    ax.set_ylabel('Position (mm)',fontsize = 18)
    ax.tick_params(labelsize=16)

    cax = ax.scatter(yi,xi,c =signal[timeStep,:],cmap = colormap,#origin='lower',
                    marker='s',s=5000,
                    vmin = -np.pi,vmax = np.pi)#,shading='nearest')
    if cbarLabel is not None:
        cbar = fig.colorbar(cax, ticks=cbarTicks,ax = ax)
    else:
        cbar = fig.colorbar(cax,ax = ax)

    if cbarLabel is not None: cbar.set_label(cbarLabel, fontsize = 24)
    if cbarTickLabels is not None: cbar.ax.set_yticklabels(cbarTickLabels, fontsize = 24)

    return fig,ax   

# Number of simultaneous PSs
def SimultaneousPSs(trajDurations,timeRange,fs=1000,msAxis=True):
    # Time range should be in ms
    # trajDurations has the begin and and of each trajectory as columns
    
    trajPresence = np.zeros((len(trajDurations),timeRange[1]-timeRange[0]))

    for i,lims in enumerate(trajDurations):
        lims = lims-int(timeRange[0])
        trajPresence[i,lims[0]:lims[1]+1] +=1
    
    numSimPSs=np.sum(trajPresence,axis=0)
    
    if  msAxis:
        fs = fs/1000
        timeRange = np.array(timeRange)*1/fs
        xLabel='Time (ms)'
    else:
        xLabel='Time (s)'
        
    TimeAxis= np.arange(timeRange[0],timeRange[1],1/fs)
    
    fig,ax = plt.subplots(1,figsize=[10,8])
    ax.plot(TimeAxis,numSimPSs)
    ax.set_xlim(timeRange)
    ax.set_ylim([0,np.max(numSimPSs)])
    ax.set_xlabel(xLabel,fontsize=20)
    ax.set_ylabel('Number of simultaneous PSs',fontsize=20)
    ax.tick_params(axis='both', labelsize=18)   
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(2)
    ax.spines['bottom'].set_linewidth(2)
    fig.suptitle('Number of simultaneous PS trajectories',fontsize = 22)

    return fig,ax

#% PS presence

def PSPresence(EGroup,closePresence,nearPresence,
               initialTime=0,endTime=-1,msAxis=False):
    '''
    Plots the presence of rotors wrt. the electrodes. The dictionaries with
    this info need to be generated beforehand with egmp. xxxx

    Parameters
    ----------
    EGroup : CatheterGroup
        Group of catheters.
    closePresence : np.array
        Integer array with the time slots in the rows and catheters in columns.
        Indicates the presence and number of trajectories ON the catheters
    nearPresence : np.array
        Integer array with the time slots in the rows and catheters in columns.
        Indicates the presence and number of trajectories CLOSE TO, BUT NOT ON the catheters
    initialTime : int, optional
        Initial time in ms. The default is 0.
    endTime : int, optional
        Final time in ms. The default is -1.
    msAxis : boolean, optional
        Plots axis in ms if true. The default is False.

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''
    
    if endTime==-1:
        endTime=len(closePresence)+initialTime
        
    Catheters = EGroup.Catheters
    Fields = list(Catheters.keys())
    fs = Catheters[Fields[0]].SamplingFrequency
    
    if not msAxis:
        TimeAxis=np.linspace(initialTime,endTime,len(closePresence))*1/fs
    else:
        TimeAxis=np.linspace(initialTime,endTime,len(closePresence))

    
    fig,ax = plt.subplots(4,4,figsize=[14,8],sharex=True,sharey=True)
    
    for i in range(len(Fields)):
        ax[i%4,i//4].plot(TimeAxis,nearPresence[:,i],'gray')
        ax[i%4,i//4].plot(TimeAxis,closePresence[:,i],'r')
        ax[i%4,i//4].set_title(Fields[i],fontsize=20)
        if i%4==3:
            ax[i%4,i//4].spines['bottom'].set_linewidth(2)
            if msAxis:
                ax[i%4,i//4].set_xlabel('Time (ms)')
            else:
                ax[i%4,i//4].set_xlabel('Time (s)')

        else:
            ax[i%4,i//4].spines['bottom'].set_visible(False)
            ax[i%4,i//4].get_xaxis().set_visible(False) 
        if i//4==0:
            ax[i%4,i//4].spines['left'].set_linewidth(2)
            ax[i%4,i//4].set_ylabel('# of PS Trajectories') 
        else:
            ax[i%4,i//4].spines['left'].set_visible(False)
            ax[i%4,i//4].get_yaxis().set_visible(False)    
        ax[i%4,i//4].spines['top'].set_visible(False)
        ax[i%4,i//4].spines['right'].set_visible(False)   
            
    ax[3,3].plot(TimeAxis,np.zeros_like(TimeAxis),'w')
    ax[3,3].spines['bottom'].set_linewidth(2)
    if msAxis:
        ax[i%4,i//4].set_xlabel('Time (ms)')
    else:
        ax[i%4,i//4].set_xlabel('Time (s)')
    ax[3,3].spines['top'].set_visible(False)
    ax[3,3].spines['right'].set_visible(False)
    ax[3,3].spines['left'].set_visible(False)
    ax[3,3].get_yaxis().set_visible(False)
    fig.suptitle('PS presence',fontsize=24)
    fig.tight_layout()
    
    return fig

# PS Distance

def PSDistance(proximityDict,trajDurations,presenceThresholds):
    fig,ax = plt.subplots(4,4,sharex=True,sharey=True,figsize=[18.42, 10.11] )
    ax[-1,-1].spines['right'].set_visible(False);
    ax[-1,-1].spines['top'].set_visible(False);
    ax[-1,-1].spines['bottom'].set_linewidth(2);
    ax[-1,-1].spines['left'].set_visible(False);
    ax[-1,-1].set_xlabel('Time (ms)',fontsize = 18)            
    ax[-1,-1].tick_params(labelsize = 16)            
    
    Fields = list(proximityDict.keys())
    
    for i,cat in enumerate(Fields):
        catDict = proximityDict[cat]
    
        ax[i//4,i%4].axhspan(0,presenceThresholds[0],alpha = 0.2,color = 'k')
        ax[i//4,i%4].axhspan(presenceThresholds[0],presenceThresholds[1],alpha = 0.2,color = 'gray')
        ax[i//4,i%4].set_title(Fields[i],fontsize =20)
        for key in catDict:
            lims = trajDurations[key]
            try:
                ax[i//4,i%4].plot(np.arange(lims[0],lims[1],1),catDict[key])
            except ValueError:
                ax[i//4,i%4].plot(np.arange(lims[0],lims[1]+1,1),catDict[key])

        ax[i//4,i%4].spines['right'].set_visible(False);
        ax[i//4,i%4].spines['top'].set_visible(False);
        ax[i//4,i%4].spines['bottom'].set_linewidth(2);
        ax[i//4,i%4].spines['left'].set_linewidth(2);
        
        if i%4==3:
            ax[i%4,i//4].set_xlabel('Time (ms)',fontsize = 18)            
            ax[i%4,i//4].tick_params(labelsize = 16)            
            
        if i//4==0:
            ax[3-i%4,i//4].set_ylabel('Distance (mm)',fontsize = 18)
            ax[3-i%4,i//4].tick_params(labelsize = 16)            

    fig.tight_layout()
    
    return fig,ax

#% PS survival

def PSSurvival(trajDurations,fs=1000,msAxis=False):
    
    initialTime=np.min(trajDurations[:,0])
    endTime=np.max(trajDurations[:,1])
    
    trajDurations = (trajDurations-initialTime).astype(int)
    
    if not msAxis:
        TimeAxis=np.arange(initialTime,endTime+.1,1)*1/fs
    else:
        TimeAxis=np.arange(initialTime,endTime+.1,1)
        
    fig,ax =plt.subplots(1,figsize=[8,8])
    ax.plot(TimeAxis[trajDurations[:,0]],np.arange(trajDurations.shape[0]),'k') # not sure about it
    for i,line in enumerate(trajDurations):
        ax.hlines(i,TimeAxis[line[0]],TimeAxis[line[1]],'b')
        
    if msAxis:
        ax.set_xlabel('Time (ms)',fontsize = 20)
    else:
        ax.set_xlabel('Time (s)',fontsize = 20)
    ax.set_ylabel('Trajectory code',fontsize = 20)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(2)
    ax.spines['bottom'].set_linewidth(2)
    
    
    fig.suptitle('Trajectory survival',fontsize = 22)

    fig.tight_layout()
    return fig

# PS and distance matrices (TODO not ideal)
def PSandDistanceMatrix(catheterName,proximityDict,trajDurations,presenceThresholds,
                       distanceMatrixDict,timeSpan):
    catDict = proximityDict[catheterName]
    tBegin = timeSpan[0]
    tEnd = timeSpan[1]
    timeAxes = np.linspace(tBegin,tEnd,distanceMatrixDict[catheterName].shape[0])
    
    fig = plt.figure(figsize=(8,10)) 
    gs=GridSpec(3,3, height_ratios=[1,5,0.25],width_ratios=[.1,8,.1]) 
    
    
    ax0 = plt.subplot(gs[0,1:-1])
    ax0.set_xlabel('Time (ms)',fontsize=16)
    ax0.set_ylabel('Distance (mm)',fontsize=16)
    ax0.set_xlim([tBegin,tEnd])
    ax0.tick_params(labelsize = 14)    
    
    ax1 = plt.subplot(gs[1,:],sharex=ax0)
    ax1.set_xlabel('Time (ms)',fontsize=16)
    ax1.set_ylabel('Time (ms)',fontsize=16)
    ax1.tick_params(labelsize = 14)    
    
    ax2 = plt.subplot(gs[2,1:-1])
    
    for key in catDict:
        lims = trajDurations[key]
        ax0.plot(np.arange(lims[0],lims[1]+1,1),catDict[key])
    ax0.axhspan(0,presenceThresholds[0],alpha = 0.2,color = 'k')
    ax0.axhspan(presenceThresholds[0],presenceThresholds[1],alpha = 0.2,color = 'gray')
    fig.suptitle(catheterName,fontsize =22)
        
    #
    im = ax1.imshow(distanceMatrixDict[catheterName],vmin=0,vmax=1,cmap='jet',
              origin='lower',extent = [tBegin,tEnd,tBegin,tEnd])
    # im = ax1.pcolormesh(timeAxes,timeAxes,distanceMatrixDict[catheterName],
    #                     cmap='jet',vmin=0,vmax=1,shading='auto')
    ax1.set_xlabel('Time (ms)',fontsize=16)
    ax1.set_ylabel('Time (ms)',fontsize=16)
    
    cbar = fig.colorbar(im, cax=ax2,orientation='horizontal')
    cbar.set_label("Distance",fontsize=16)
    
    fig.subplots_adjust(top=0.941,bottom=0.067,left=0.015,right=0.96,hspace=0.335,wspace=0.424)
    
    return fig


def DataFrameBoxplots(dataFrame,featureList,position,label,titles=None,
                      color=None,xtick=None,fig=None,
                      nCols=4,width=0.25,notch=False,append=False):
    
    if fig is None:
        nRows=int(len(featureList)//nCols+(len(featureList)%nCols!=0))
        fig,ax = plt.subplots(nRows,nCols,figsize=[16,10])
    else:
        nRows=int(len(featureList)//nCols+(len(featureList)%nCols!=0))
        ax = fig.get_axes()
        ax = np.asarray(ax).reshape(nRows,nCols)
    
    for i,feature in enumerate(featureList):
        if append:
            tmp_ticks,ind=np.unique(ax[i//nCols,i%nCols].get_xticks(),return_index=True)
            tmp_labels=np.asarray(ax[i//nCols,i%nCols].get_xticklabels())[ind]
            
        bplot1 = ax[i//nCols,i%nCols].boxplot(dataFrame[feature], positions=[position], sym='',
                                  widths=width,notch=notch,patch_artist=True,medianprops=dict(color='k'))
        
        if titles is None:
            ax[i//nCols,i%nCols].set_title(feature,fontsize=20)
        else:
            ax[i//nCols,i%nCols].set_title(titles[i],fontsize=20)

        if append:
            if xtick is None: # and ~np.isin(position,tmp_ticks):
                ax[i//nCols,i%nCols].set_xticks(np.append(tmp_ticks,position))
                ax[i//nCols,i%nCols].set_xticklabels(np.append(tmp_labels,plt.Text(position,0,label)))

            else:
                ax[i//nCols,i%nCols].set_xticks(np.append(tmp_ticks,xtick))
                ax[i//nCols,i%nCols].set_xticklabels(np.append(tmp_labels,plt.Text(xtick,0,label)))
                
        else:
            if xtick is None:
                ax[i//nCols,i%nCols].set_xticks([position])
            else:
                ax[i//nCols,i%nCols].set_xticks([xtick])
            ax[i//nCols,i%nCols].set_xticklabels([label])
            
        ax[i//nCols,i%nCols].grid(axis='y')
        ax[i//nCols,i%nCols].tick_params('both',labelsize=16)
        
        if color is not None:
            for patch in bplot1['boxes']:
                patch.set_facecolor(color)
        
    return fig,bplot1
  

def SignalAndActivations(catheterObj,initialTime=2500):
    
    
    catheterData = catheterObj.EGMData
    

    names = ['A4','A3','A2','A1',
             'B4','B3','B2','B1',
             'C4','C3','C2','C1',
             'D4','D3','D2','D1']
    time = np.linspace(initialTime,initialTime+catheterData.shape[0],catheterData.shape[0])
        
    fig,ax = plt.subplots(4,4,sharex = True,sharey = True,figsize = [16,10])
    ref_plots = {}
    ref_vlines = {}
    lims = [catheterData.min(), catheterData.max()]
    for i in range(catheterData.shape[1]):
        
        
        ref_plots[i] = ax[3-i%4,i//4].plot(time,catheterData[:,i],'k',linewidth = 1)
        
        acts = catheterObj.Activations[i]
        ref_plots[i] = ax[3-i%4,i//4].vlines(time[acts],lims[0],lims[1],linestyle='dashed',color='b',linewidth = 1)

        ax[3-i%4,i//4].spines['right'].set_visible(False);
        ax[3-i%4,i//4].spines['top'].set_visible(False);
        ax[3-i%4,i//4].spines['bottom'].set_linewidth(2);
        ax[3-i%4,i//4].spines['left'].set_linewidth(2);
        ax[3-i%4,i//4].text(0.9*(initialTime+time.shape[0]),lims[1],names[i],color = 'gray',fontsize = 22)
        if i%4==0:
            ax[3-i%4,i//4].set_xlabel('Time (s)',fontsize = 20)            
            ax[3-i%4,i//4].tick_params(labelsize = 16)            
            
        if i//4==0:
            ax[3-i%4,i//4].set_ylabel('Norm. ampl.',fontsize = 20)
            ax[3-i%4,i//4].tick_params(labelsize = 16)   
    fig.tight_layout()
    fig.show()      


def Animate2DGrid(gridLocations,signal,
                    initialFrame,endFrame=1,step=1,
                    vmin=None,vmax=None,
                    colormap=None,save=False,saveFile=None,
                    cbarLabel=None,cbarTicks=None):

    if endFrame==-1: endFrame = signal.shape[0]
    if colormap is None: colormap = 'gnuplot' 
    if vmin is None: vmin=np.nanmin(signal)
    if vmax is None: vmax=np.nanmax(signal)
    signal[np.isnan(signal)] = vmin

    sampleSteps = np.arange(initialFrame,endFrame,step,dtype=int)
    nFrames = sampleSteps//step

    fig, ax = plt.subplots(1,figsize=(7.2,5.75))

    vUnique = np.unique(gridLocations[:,1])
    vSpace = np.abs(vUnique[1]-vUnique[0])
    hSpace = np.abs(gridLocations[1,0]-gridLocations[0,0])
    
    ax.set(xlim=(gridLocations[0,1]-vSpace/2,gridLocations[-1,1]+vSpace/2), 
            ylim=(gridLocations[0,0]-hSpace/2,gridLocations[-1,0]+hSpace/2))
    ax.autoscale(False)

    ax.set_xlabel('Position (mm)',fontsize = 18)
    ax.set_ylabel('Position (mm)',fontsize = 18)
    ax.tick_params(labelsize=16)

    cax = ax.scatter(gridLocations[:,1],gridLocations[:,0],
                    c =signal[sampleSteps[0],:],cmap = colormap,
                    marker='s',s=5000,
                    vmin = vmin,vmax = vmax)#,shading='nearest')
    
    if cbarTicks is not None:
        cbar = fig.colorbar(cax, ticks = cbarTicks[0],ax = ax)
        cbar.ax.set_yticklabels(cbarTicks[1], fontsize = 24) 
    else:  
        cbar = fig.colorbar(cax,ax = ax) 
    if cbarLabel is not None: 
        cbar.set_label(cbarLabel, fontsize = 24)
 

    fig.suptitle('t= %d ms'%(sampleSteps[0]), fontsize = 28)

    def animSP(i,signal):
        cax.set_array(signal[i,:])
        fig.suptitle('t= %d ms'%(i), fontsize = 28)
        return fig

    anim = animation.FuncAnimation(fig, animSP, interval = 60, frames=sampleSteps,
                            repeat_delay = 200,fargs=(signal,))
 
    if save:
        writer = animation.FFMpegWriter(fps = 25)
        if saveFile is None:
            anim.save('output_video.mp4', writer=writer)
        else:
            anim.save(os.path.join(os.getcwd(),'Videos','%s_%s_%d.mp4'%(experiment,cat,k)), writer=writer)
    else:
        plt.show()


# Conduction velocity
def PlotConductionVelocity(CatheterObj,intervalPreferentiality,pointOrigin=True):
    # xi = CatheterObj.ElectrodesTemplate[:,0].reshape(4,4).T
    # xi = xi.flatten()
    # yi = CatheterObj.ElectrodesTemplate[:,1].reshape(4,4).T
    # yi = yi.flatten()
    Nr,Nc = CatheterObj.GetGridSize()

    Angles = intervalPreferentiality['angle'].reshape(Nr,Nc)

    Vx = np.asarray(intervalPreferentiality['additionalInformation']['vx'])#np.cos(intervalPreferentiality['angle'])
    Vy = np.asarray(intervalPreferentiality['additionalInformation']['vy'])#np.sin(intervalPreferentiality['angle'])

    Vx = np.array([np.nanmean(element) for element in Vx])    
    Vy = np.array([np.nanmean(element) for element in Vy])

    # Electrode names
    if CatheterObj.CatheterModel=='HDGrid-4':
        names = ['A4','A3','A2','A1',
                'B4','B3','B2','B1',
                'C4','C3','C2','C1',
                'D4','D3','D2','D1']

    fig,ax = plt.subplots(1,figsize=(7.25,7.25))
    ax.scatter(CatheterObj.ElectrodesTemplate[:,1],
            CatheterObj.ElectrodesTemplate[:,0],
            marker='s',s=200,color='lightgrey',
            label='Electrode positions')
    ax.scatter(CatheterObj.ElectrodesTemplate[0,1],
            CatheterObj.ElectrodesTemplate[0,0],
            marker='s',s=200,color='darkred',label='$1^{st}$ electrode')
    ax.scatter(CatheterObj.ElectrodesTemplate[3,1],
            CatheterObj.ElectrodesTemplate[3,0],
            marker='s',s=200,color='red',label='Direction of $1^{st}$ spline')
    ax.quiver(CatheterObj.ElectrodesTemplate[:,1],
            CatheterObj.ElectrodesTemplate[:,0],
            Vy,Vx,label='Preferential direction',
            scale=8,width=.015)

    if pointOrigin:
        VxM = np.mean(Vx)
        VyM = np.mean(Vy)
        ax.quiver(0,0,-VyM,-VxM,color = 'r',scale = 3,width = 0.025,label='Move catheter there')

    if CatheterObj.CatheterModel=='HDGrid-4':
        for i in range(len(names)):
            ax.text(CatheterObj.ElectrodesTemplate[i,1]+.25,CatheterObj.ElectrodesTemplate[i,0]+.25,
                    names[i],color = 'gray',fontsize = 12)

    ax.set_xlabel('Position (mm)',fontsize = 18)
    ax.set_ylabel('Position (mm)',fontsize = 18)
    ax.set_aspect('equal')
    ax.tick_params(labelsize=16)
    fig.legend(fontsize=14,loc=9,ncol=2)

def PlotPreferentiality(CatheterObj,intervalPreferentiality,pointOrigin=True):


    Pref = intervalPreferentiality['angle']#.reshape(len(xi),len(yi))
    # Electrode names
    if CatheterObj.CatheterModel=='HDGrid-4':
        names = ['A4','A3','A2','A1',
                'B4','B3','B2','B1',
                'C4','C3','C2','C1',
                'D4','D3','D2','D1']

    fig,ax = plt.subplots(1,figsize=(10,8))
    cax = ax.scatter(CatheterObj.ElectrodesTemplate[:,1],
            CatheterObj.ElectrodesTemplate[:,0],
            marker='s',s=200,c=Pref,
            vmin=0,vmax=1)
    
    if pointOrigin:
        VxM = np.mean(Vx)
        VyM = np.mean(Vy)
        ax.quiver(0,0,-VyM,-VxM,color = 'r',scale = 3,width = 0.025,label='Move catheter there')

    if CatheterObj.CatheterModel=='HDGrid-4':
        for i in range(len(names)):
            ax.text(CatheterObj.ElectrodesTemplate[i,1]+.25,CatheterObj.ElectrodesTemplate[i,0]+.25,
                    names[i],color = 'gray',fontsize = 12)
    cbar = fig.colorbar(cax)
    cbar.set_label('Preferentiality',fontsize=18)
    ax.set_xlabel('Position (mm)',fontsize = 14)
    ax.set_ylabel('Position (mm)',fontsize = 14)
    ax.set_aspect('equal')
    ax.tick_params(labelsize=12)

def RecurrencePlot(RP,timeAxes,lims = None):
    if lims is None:
        lims = [timeAxes[0],timeAxes[-1]]
    rng = lims[1]-lims[0]

    fig,ax = plt.subplots(1,figsize=(11,9.6))
    xi,yi = np.where(RP)
    ax.scatter(timeAxes[xi],timeAxes[yi],s=10/rng,color='k')

    ax.set_xticks(np.linspace(lims[0],lims[1]+1,11))
    ax.set_xlim(lims)
    ax.set_yticks(np.linspace(lims[0],lims[1]+1,11))
    ax.set_ylim(lims)
    ax.set_ylabel('Time (ms)',fontsize = 18)
    ax.set_xlabel('Time (ms)',fontsize = 18)
    ax.tick_params(labelsize=16)
    fig.tight_layout()
    fig.show()

#%% 3D
# Make VTK Screenshot
def MakeVTKScreenshot(renWin,fname,fpath = '.'):
    renWin.Render()
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renWin)
    w2if.Update()
    writer = vtk.vtkPNGWriter()
    writer.SetFileName(os.path.join(fpath,fname+".png"))
    writer.SetInputConnection(w2if.GetOutputPort())
    writer.Write()

#% Heatmaps

def PSHeatmap(Anatomy,HM,vmin=0,vmax=-1,filter=True):
    if Anatomy.shape[0]!=HM.shape[0]:
        RuntimeError('Anatomy does not match heatmap shape')
        
    # Make Poly Data
    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()

    pid = np.arange(Anatomy.shape[0])
    vdata = vtk.vtkFloatArray()
    
    for i,p in enumerate(Anatomy):
        pid[i] = points.InsertNextPoint(p.astype(float))
        vdata.InsertNextValue(HM[i])
    
    vertices.InsertNextCell(len(pid), pid)
    # Set the points and vertices we created as the geometry and topology of the polydata
    Object = vtk.vtkPolyData()
    Object.SetPoints(points)
    Object.SetVerts(vertices)
    # Also add the scalar vector
    Object.GetPointData().SetScalars(vdata)
    
    #Slight filter
    if filter: 
        FilterOut = SmoothFilters(Object,numberOfIterations = 10, relaxationFactor =1)
        FilterOut=FilterOut.GetOutput()
    else:
        FilterOut=Object
    
    if vmax==-1:
        vmax = np.max(HM)
    # elif vmax>0 and vmax<1:
    #     vmax = np.percentile(HM,vmax*100) # Not sure if I'll keep this
    
    # Plot
    lut = HeatmapColormap(len(pid))
    lut.Build()
    
    
    
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(FilterOut)
    mapper.SetScalarRange([vmin,vmax])
    mapper.ScalarVisibilityOn()
    mapper.SetLookupTable(lut)
    
    # Colorbar
    colorbarActor = ColorbarActor(mapper,title = 'No. trajectories')
    
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(15)
    
    PosteriorRenderer = StandardViewRenderer([actor],cameraType='Posterior',
                                    viewportSplit='horizontal-left')
    PosteriorRenderer.SetBackground(1,1,1)
    AnteriorRenderer = StandardViewRenderer([actor],cameraType='Anterior',
                                    colorbarActor = colorbarActor,
                                    viewportSplit='horizontal-right')
    AnteriorRenderer.SetBackground(1,1,1)
    
    # vtk.vtkRenderer()
    # renderer.AddActor(actor)
    # renderer.AddActor2D(colorbarActor)
    
    
    renderWindow = TwoViewsRenderWindow(PosteriorRenderer,AnteriorRenderer,windowSize = [1800,800],
                         OffScreenRendering = False)
    
    # renderWindow = vtk.vtkRenderWindow()
    # renderWindow.SetSize(900,800)
    # renderWindow.AddRenderer(renderer)
    
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)
    
    renderWindow.Render()
    renderWindowInteractor.Start()
    
    return renderWindow

def MakeIgbUnstructuredGridActor(FullCell,validCells,Data,vmin=-1,vmax=-1,smoothFilter=False,cmap='egm',
                                returnData = False):
    # At the moment, this function is basically for plotting the anatomy, not everything possible. for that, use MakePolyDataActor
    # Data here is a 1D array

    # Make "Fake" igb unstructured grid object
    grid = IGBUnstructuredGrid(None,valid_cells=validCells,units=1)
    grid.cell = FullCell
    DataLength = len(grid.get_cubes()[0])
    if DataLength!=Data.shape[0]:
        RuntimeError('Anatomy does not match heatmap shape')
        return -1

    grid = grid.to_vtkUnstructuredGrid()

    vdata = numpy_to_vtk(Data)#vtk.vtkFloatArray()
    # for element in Data: vdata.InsertNextValue(element)

    grid.GetCellData().SetScalars(vdata)

    # Set limits, colormaps, etc
    if vmax==-1: vmax = np.nanmax(Data)
    if vmin==-1: vmin = np.nanmin(Data)

    CMAPDICT = {'frontiers':FrontiersColormap(DataLength),
                'heatmap':HeatmapColormap(DataLength),
                'egm':EGMColormap(DataLength),
                'phase':PhaseColormap(DataLength),
                'jet':JetColormap(DataLength),
                'viridis':ViridisColormap(DataLength)}

    CbarTitleDict = {'frontiers':'Vm (mV)',
                    'heatmap': 'PS Count',
                    'egm':'Amplitude (mV)',
                    'phase':'Phase (rad)',
                    'viridis':'Curl',
                    'jet':'Time (ms)'} # I will eventually have to change this

    lut = CMAPDICT[cmap]
    lut.Build()

    # Make used VTK objects
    geometry = vtk.vtkGeometryFilter()
    geometry.SetInputData(grid)
    geometry.Update()
    if smoothFilter:
        SmoothFilter =vtk.vtkSmoothPolyDataFilter()
        SmoothFilter.SetInputConnection(geometry.GetOutputPort())
        SmoothFilter.SetNumberOfIterations(2)
        SmoothFilter.SetRelaxationFactor(1)
        SmoothFilter.FeatureEdgeSmoothingOff()
        SmoothFilter.BoundarySmoothingOn()
        SmoothFilter.Update()
        Obj = SmoothFilter.GetOutput()
    else:
        Obj = geometry.GetOutput()

    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputDataObject(Obj)
    mapper.SetScalarRange([vmin,vmax])
    mapper.ScalarVisibilityOn() 
    mapper.SetLookupTable(lut)
    # mapper.Modified()
    
    cbarActor = ColorbarActor(mapper,title = CbarTitleDict[cmap])
    cbarActor.SetHeight(0.9)
    cbarActor.SetLabelFormat('%-#2g')
    cbarActor.SetNumberOfLabels(11)
    # cbarActor.Modified()
    
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    # actor.GetProperty().SetPointSize(5)
    # actor.GetProperty().SetRenderPointsAsSpheres(0)
    actor.GetProperty().SetOpacity(1)
    # actor.GetProperty().SetEdgeVisibility(False)
    actor.GetProperty().SetInterpolationToGouraud()

    if not returnData:
        return actor,cbarActor
    else:
        return actor,cbarActor,Obj

def MakePolyDataActor(Anatomy,Data,t=0,vmin=-1,vmax=-1,filter=False,cmap='egm'):
    if Anatomy.shape[0]!=Data.shape[0]:
        RuntimeError('Anatomy does not match heatmap shape')
    
    plotData = Data[t,:]
    # Make Poly Data
    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()

    pid = np.arange(Anatomy.shape[0])
    vdata = vtk.vtkFloatArray()
    
    for i,p in enumerate(Anatomy):
        pid[i] = points.InsertNextPoint(p.astype(float))
        vdata.InsertNextValue(plotData[i])
    
    vertices.InsertNextCell(len(pid), pid)
    # Set the points and vertices we created as the geometry and topology of the polydata
    Object = vtk.vtkPolyData()
    Object.SetPoints(points)
    Object.SetVerts(vertices)
    # Also add the scalar vector
    Object.GetPointData().SetScalars(vdata)
    
    #Slight filter
    if filter: 
        FilterOut = SmoothFilters(Object,numberOfIterations = 10, relaxationFactor = 5)
        FilterOut = FilterOut.GetOutput()
    else:
        FilterOut=Object
    
    if vmax==-1:
        vmax = np.max(plotData)
    if vmin==-1:
        vmin = np.min(plotData)
    # elif vmax>0 and vmax<1:
    #     vmax = np.percentile(HM,vmax*100) # Not sure if I'll keep this
    
    # Plot
    CMAPDICT = {'frontiers':FrontiersColormap(len(pid)),
                'heatmap':HeatmapColormap(len(pid)),
                'egm':EGMColormap(len(pid)),
                'phase':PhaseColormap(len(pid)),
                'jet':JetColormap(len(pid))}
    CbarTitleDict = {'frontiers':'Vm (mV)',
                    'heatmap': 'PS Count',
                    'egm':'Amplitude (mV)',
                    'phase':'Phase (rad)',
                    'jet':'Time (ms)'} # I will eventually have to change this
    
    lut = CMAPDICT[cmap]
    lut.Build()
    
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(FilterOut)
    mapper.SetScalarRange([vmin,vmax])
    mapper.ScalarVisibilityOn() 
    mapper.SetLookupTable(lut)
    
    
    cbarActor = ColorbarActor(mapper,title = CbarTitleDict[cmap])
    cbarActor.SetHeight(0.9)
    cbarActor.SetLabelFormat('%-#5g')
    cbarActor.SetNumberOfLabels(11)
    # cbarActor.Modified()
    
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(15)
    
    return actor,cbarActor

def MakePSActors(nparray,scale,sphereColor = (1,1,1)):
    nparray = nparray*scale
    nCoords = nparray.shape[0]
	#
    verts = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
	#
    PSData = vtk.vtkPolyData()
	#
    verts.SetData(numpy_to_vtk(nparray))
    cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
						   np.arange(nCoords, dtype = np.int64)]).T.flatten()
    cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
    PSData.SetPoints(verts)
    PSData.SetVerts(cells)
	#
    PSmapper = vtk.vtkPolyDataMapper()
    PSmapper.SetInputDataObject(PSData)
	#
    PSActor = vtk.vtkActor()
    PSActor.SetMapper(PSmapper)
    # 	PSActor.GetProperty().SetRepresentationToPoints()
    PSActor.GetProperty().SetRepresentationToSurface()
    PSActor.GetProperty().SetColor(sphereColor)
    PSActor.GetProperty().RenderPointsAsSpheresOn()
    PSActor.GetProperty().SetPointSize(5)
	#
    return PSActor,PSData

def MakeTrajectoryActor(TrajectoryPositions,scale = 1):
    TrajectoryPositions = TrajectoryPositions*scale

    # Make Poly Data
    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()
    
    pid = np.arange(TrajectoryPositions.shape[0])
    vdata = vtk.vtkFloatArray()
    
    for i,p in enumerate(TrajectoryPositions):
        pid[i] = points.InsertNextPoint(p.astype(float))
        vdata.InsertNextValue(i)
    
    vertices.InsertNextCell(len(pid), pid)
    # Set the points and vertices we created as the geometry and topology of the polydata
    Object = vtk.vtkPolyData()
    Object.SetPoints(points)
    Object.SetVerts(vertices)
    # Also add the scalar vector
    Object.GetPointData().SetScalars(vdata)
    
    #Slight filter
    vmax = len(pid)
    vmin = 0
    # elif vmax>0 and vmax<1:
    #     vmax = np.percentile(HM,vmax*100) # Not sure if I'll keep this
    
    # Plot
    lut = JetColormap(len(pid))
    lut.Build()
    
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(Object)
    mapper.SetScalarRange([vmin,vmax])
    mapper.ScalarVisibilityOn()
    mapper.SetLookupTable(lut)
    
    
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetPointSize(10)
    
    return actor,Object

def QuickRenderWindowInteractor(ActorList_3D,ActorList_2D = [],interactor=True,camera='egmPosterior'):
    
    renderer = vtk.vtkRenderer()
    for Actor in ActorList_3D:
        renderer.AddActor(Actor)
    for Actor_2D in ActorList_2D:
        renderer.AddActor2D(Actor_2D)
    
    renderer.ResetCamera()
    colors = vtk.vtkNamedColors()
    bkg = map(lambda x: x, [255, 255, 255, 255])
    colors.SetColor("BkgColor", *bkg)
    renderer.SetBackground(colors.GetColor3d("BkgColor"))	
    
    renWin = OneViewRenderWindow(renderer,windowSize = [1800,800],
                            OffScreenRendering = False)
    if interactor:
        interactor = vtk.vtkRenderWindowInteractor()
        interactor.SetRenderWindow(renWin)
        renWin.Render()
        interactor.Start()
    else:
        SetCamera(renderer,camera)

        return renWin
    

def MakeGlyphActor(positions,velocities,magnitudes = None,source=None,vmin=None,vmax=None,cmap='jet',mode='color',arrowColor = 'DarkRed'):

    # Positions: nx3
    # Angles: nx3
    # Magnitudes: n,1
    
    ## Make poly data for positions
    # Data
    nCoords = positions.shape[0]
    #
    verts = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    #
    poly = vtk.vtkPolyData()
    #
    verts.SetData(numpy_to_vtk(positions))
    cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
                           np.arange(nCoords, dtype = np.int64)]).T.flatten()
    cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
    poly.SetPoints(verts)
    poly.SetVerts(cells)
    
    ## Make velocity array
    velocity = vtk.vtkDoubleArray()
    velocity.SetName("angles")
    velocity.SetNumberOfComponents(3)
    velocity.SetNumberOfTuples(nCoords)
    for i in range(nCoords):
        velocity.SetTuple(i, list(velocities[i]))
    
    ## Make magnitude array
    if magnitudes is None:
        magnitudes = np.ones(nCoords)
    if vmin is None: vmin=np.nanmin(magnitudes)
    if vmax is None: vmax=np.nanmax(magnitudes)
    
    magnitude = vtk.vtkDoubleArray()
    magnitude.SetNumberOfValues(nCoords)
    magnitude.SetName("magnitude")
    for i in range(nCoords):
        magnitude.SetValue(i, magnitudes[i])
        
    ## Put everything in the poly data
    poly.GetPointData().AddArray(velocity)
    poly.GetPointData().AddArray(magnitude)
    if magnitudes is not None: 
        poly.GetPointData().SetActiveScalars("magnitude")
    poly.GetPointData().SetActiveVectors("angles")
    
    ## Actual VTK portion
    CMAPDICT = {'frontiers':FrontiersColormap(len(magnitudes)), 
                'heatmap':HeatmapColormap(len(magnitudes)),
                'egm':EGMColormap(len(magnitudes)),
                'phase':PhaseColormap(len(magnitudes)),
                'jet':JetColormap(len(magnitudes)),
                'viridis':ViridisColormap(len(magnitudes))}
    CbarTitleDict = {'frontiers':'Vm (mV)',
                    'heatmap': 'PS Count',
                    'egm':'Amplitude (mV)',
                    'phase':'Phase (rad)',
                    'viridis':'Density',
                    'jet':'Preferentiality'} # I will eventually have to change this
     
    lut = CMAPDICT[cmap]
    lut.Build()

    # Create glyph (put everything as arguments)
    if source is None:
        source = vtk.vtkArrowSource()
        source.SetShaftRadius(0.05)
        source.SetTipLength(0.25)
        source.SetTipRadius(0.15)
    elif source is 'sphere':
        source = vtk.vtkSphereSource() 
        source.SetRadius(0.8/5)
    
    glyph = vtk.vtkGlyph3D()
    glyph.SetInputData(poly)
    glyph.SetSourceConnection(source.GetOutputPort())
    glyph.SetScaleFactor(3)
    glyph.OrientOn()
    glyph.SetVectorModeToUseVector()
    if magnitudes is not None: 
        if mode=='color':
            glyph.SetColorModeToColorByScalar()
            glyph.SetScaleModeToDataScalingOff()
        elif mode=='scale':
            glyph.SetScaleModeToScaleByScalar ()
    glyph.Update()
    
    
    # Make Actor
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(glyph.GetOutputPort())

    actor = vtk.vtkActor()
    if mode=='color': 
        mapper.SetScalarRange([vmin,vmax])
        mapper.ScalarVisibilityOn() 
        mapper.SetLookupTable(lut)
    else:
        mapper.ScalarVisibilityOff() 
        actor.GetProperty().SetColor(vtk.vtkNamedColors().GetColor3d(arrowColor)) # darkblue
    
    actor.SetMapper(mapper)
 
    return actor

def SplineActors(knots,connections=None,splineColor = [0,0,0],radius=1):
    if connections is None:
        connections = [np.arange(len(knots))]
    #knots are 3D positions
    LineActors = []
    for C in connections:
        for K1,K2 in zip(C[:-1],np.roll(C,-1)[:-1]):
            K1 = knots[K1]
            K2 = knots[K2]
            Line = vtk.vtkLineSource()
            Line.SetPoint1(K1)
            Line.SetPoint2(K2)
            # Make tube
            tubeFilter = vtk.vtkTubeFilter()
            tubeFilter.SetInputConnection(Line.GetOutputPort())
            tubeFilter.SetRadius(radius)
            tubeFilter.SetNumberOfSides(12)
            tubeFilter.Update()
            # Mapper
            mapper = vtk.vtkPolyDataMapper()
            mapper.SetInputConnection(tubeFilter.GetOutputPort())
            # Actor
            LineActors.append(vtk.vtkActor())
            LineActors[-1].SetMapper(mapper)
            LineActors[-1].GetProperty().SetColor(splineColor)
    return LineActors 

#%% Colormaps

# Phase
my_rgbs = np.array(([[1.        , 1.        , 1.        ],
                       [0.44354576, 0.1310866 , 0.90183824],
                       [0.40866014, 0.1994281 , 0.90955883],
                       [0.37377453, 0.2677696 , 0.91727942],
                       [0.33888888, 0.3361111 , 0.92500001],
                       [0.30400327, 0.40445259, 0.9327206 ],
                       [0.26911765, 0.47279412, 0.94044119],
                       [0.23423202, 0.54113561, 0.94816178],
                       [0.19934641, 0.6094771 , 0.95588237],
                       [0.17442811, 0.65829247, 0.96139705],
                       [0.1495098 , 0.70710784, 0.96691179],
                       [0.12459151, 0.75592321, 0.97242647],
                       [0.0996732 , 0.80473852, 0.97794116],
                       [0.0747549 , 0.85355389, 0.9834559 ],
                       [0.0498366 , 0.90236926, 0.98897058],
                       [0.0249183 , 0.95118463, 0.99448532],
                       [0.        , 1.        , 1.        ],
                       [0.07291666, 1.        , 0.92708331],
                       [0.14583333, 1.        , 0.85416669],
                       [0.21875   , 1.        , 0.78125   ],
                       [0.29166666, 1.        , 0.70833331],
                       [0.36458331, 1.        , 0.63541669],
                       [0.4375    , 1.        , 0.5625    ],
                       [0.51041663, 1.        , 0.48958331],
                       [0.58333331, 1.        , 0.41666666],
                       [0.63541663, 1.        , 0.36458331],
                       [0.6875    , 1.        , 0.3125    ],
                       [0.73958331, 1.        , 0.26041666],
                       [0.79166663, 1.        , 0.20833333],
                       [0.84375   , 1.        , 0.15625   ],
                       [0.89583331, 1.        , 0.10416666],
                       [0.94791669, 1.        , 0.05208333],
                       [1.        , 1.        , 0.        ],
                       [1.        , 0.94999999, 0.        ],
                       [1.        , 0.89999998, 0.        ],
                       [1.        , 0.85000002, 0.        ],
                       [1.        , 0.80000001, 0.        ],
                       [1.        , 0.75      , 0.        ],
                       [1.        , 0.70000005, 0.        ],
                       [1.        , 0.65000004, 0.        ],
                       [1.        , 0.60000002, 0.        ],
                       [1.        , 0.55000001, 0.025     ],
                       [1.        , 0.5       , 0.05      ],
                       [1.        , 0.45000002, 0.075     ],
                       [1.        , 0.40000001, 0.1       ],
                       [1.        , 0.35000002, 0.125     ],
                       [1.        , 0.30000001, 0.15000001],
                       [1.        , 0.25      , 0.175     ],
                       [1.        , 0.2       , 0.2       ],
                       [0.96176469, 0.175     , 0.175     ],
                       [0.92352939, 0.15000001, 0.15000001],
                       [0.88529414, 0.125     , 0.125     ],
                       [0.84705883, 0.1       , 0.1       ],
                       [0.80882353, 0.075     , 0.075     ],
                       [0.77058828, 0.05      , 0.05      ],
                       [0.73235297, 0.025     , 0.025     ],
                       [0.69411767, 0.        , 0.        ],
                       [0.66638654, 0.        , 0.        ],
                       [0.63865548, 0.        , 0.        ],
                       [0.61092436, 0.        , 0.        ],
                       [0.5831933 , 0.        , 0.        ],
                       [0.55546218, 0.        , 0.        ],
                       [0.52773112, 0.        , 0.        ],
                       [0.5       , 0.        , 0.        ]]))
x = np.linspace(0,100,my_rgbs.shape[0])
x_int = np.linspace(0,100,256)
phase_cmap = np.zeros((256,3))
phase_cmap[:,0] = np.interp(x_int,x,my_rgbs[:,0])
phase_cmap[:,1] = np.interp(x_int,x,my_rgbs[:,1])
phase_cmap[:,2] = np.interp(x_int,x,my_rgbs[:,2])
phase_cmap = ListedColormap(phase_cmap, name='phase_cmap',N=256)