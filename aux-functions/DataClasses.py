# -*- coding: utf-8 -*-
"""
Created on Mon May  3 14:45:58 2021

DESCRIPTION: This library is used to put together a series of classes I use
to organize the Data. Probably it would be good to separate in different libraries
once it gets more complex. 

It would also possibly be smart to add functions to these classes

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Related to pstracker and psfinder
class PSCluster:
    def __init__(self,points = []):
        self.points = []
        self.centers = []
        self.clusind = []
        self.tsteps = []
        self.tras = []

class PStrackerResults:
    def __init__(self): 
        self.Npoints = 0
        self.persample = PSCluster()
        self.number_of_ps = []
        self.trax = []
        self.Ntra = 0
        
class PSFinderOptions:
    def __init__(self):
        self.fname_cell = ''
        self.fname_tcc = ''
        self.ctypes = []
        self.fname_vm = ''
        self.fname_out=''
        self.n_processes=1
        self.save_interval=500
        self.Tau=10
        self.Viso  = -40 # Mean of the first few seconds of lead (x,y,z) = 250,300,120           
        self.Nt = np.inf#
        self.tBegin = 0
        self.tEnd = 100
        self.tInt = 1
        self.iga2igb='iga2igb'