"""
Created on Thu Oct  28 2021

DESCRIPTION: Contains functions to calculate the position of sources

The organization of the array is in matlab shape due to compatibility with
Stef's software, but at some point I'd like to rearrange to Python.

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Import libraries
import numpy as np
import egm_processing as egmp


class SourceTrackingCalculator:

    def __init__(self,phaseJumpThreshold=1.5*np.pi):
        # Properties that are initialized elsewhere
        self.PSAxis = None
        self.PSLocations = None

        # Properties initialized here
        self.PhaseJumpThreshold=phaseJumpThreshold
    

    #%% Phase singularity Rotor tracking
    def DoubleRingPSDetection(self,CatheterObj):
        
        # Get the x and y coordinates of the template
        xi,yi = CatheterObj.GetGridPositions()
        Nr,Nc = CatheterObj.GetGridSize()
        NElectrodes = Nr*Nc

        # Reshape phase signal to 2D configuration 
        if CatheterObj.PhaseSignal is None: 
            RuntimeError('Please generate a phase signal first')
            return -1

        activationPhaseSignal = np.reshape(CatheterObj.PhaseSignal,(CatheterObj.PhaseSignal.shape[0],Nr,Nc))

        # Extend phase signal for 4x4 rings
        extendedPhaseSignal = np.zeros(((activationPhaseSignal.shape[0],Nr+2,Nc+2)))*np.nan
        extendedPhaseSignal[:,1:-1,1:-1] = activationPhaseSignal        

        # Coordinates for the "cells" which are between measured points
        # more or less like bipolar coordinates
        Grid2x2CoordsX = np.array([np.mean([xi[i],xi[i+1]]) for i in range(len(xi)-1)])
        Grid2x2CoordsY = np.array([np.mean([yi[i],yi[i+1]]) for i in range(len(yi)-1)])
        self.PSAxis = np.vstack([Grid2x2CoordsX,Grid2x2CoordsY]).T

        # Make 2D matrices to hold the PS detections with the 2x2 and 4x4 rings
        # If I add different types of rings, this might need to change

        Cell2x2 = np.zeros((activationPhaseSignal.shape[0],Nr-2+1,Nc-2+1))
        Cell4x4 = np.zeros_like(Cell2x2)

        # Go over 2x2 ring, if there is only one PS jump, mark as PS detected
        for ci in range(Cell2x2.shape[1]):
            for cj in range(Cell2x2.shape[2]):
                #Get signals
                tempPhase = np.array([activationPhaseSignal[:,ci,cj],
                                    activationPhaseSignal[:,ci,cj+1],
                                    activationPhaseSignal[:,ci+1,cj+1],
                                    activationPhaseSignal[:,ci+1,cj],
                                    activationPhaseSignal[:,ci,cj]])

                RingDiff = np.abs(np.diff(tempPhase,axis=0))
                RingDiff[np.isnan(RingDiff)] = 0
                RingDiff = np.sum(RingDiff>=self.PhaseJumpThreshold,axis=0)
                ind = np.where(RingDiff==1)[0] # Only one jump
                Cell2x2[ind,ci,cj] = 1        

        # Go over 4x4 ring, if there is only one PS jump, mark as PS detected
        # On the edges, the array is NaN, so it will simply ignore if the jumps are
        # outside the field of view
        for ci in range(1,Cell4x4.shape[1]+1):
            for cj in range(1,Cell4x4.shape[2]+1):
                #Get signals
                tempPhase = np.array([extendedPhaseSignal[:,ci-1,cj-1],extendedPhaseSignal[:,ci-1,cj],extendedPhaseSignal[:,ci-1,cj+1],
                                    extendedPhaseSignal[:,ci,cj+1],extendedPhaseSignal[:,ci+1,cj+1],
                                    extendedPhaseSignal[:,ci+1,cj],extendedPhaseSignal[:,ci+1,cj-1],
                                    extendedPhaseSignal[:,ci,cj-1],extendedPhaseSignal[:,ci-1,cj-1]])
                RingDiff = np.abs(np.diff(tempPhase,axis=0))
                RingDiff[np.isnan(RingDiff)] = 0
                RingDiff = np.sum(RingDiff>=1.5*np.pi,axis=0)
                ind = np.where(RingDiff==1)[0] # Only one jump
                Cell4x4[ind,ci-1,cj-1] = 1             

        # Overlap both
        self.PSLocations = np.logical_and(Cell2x2,Cell4x4)


    # Activation time
    def ComputeCLCoverage(self,activationGroup,estimatedAFCL):
        '''
        This function calculates the preferential angle of the activation group, treated as an array.
        The main objetive is to estimate the circular variance to identify circular activity x laminar flow

        If the values are low, they indicate more laminar flow, or maybe ectopic foci. Higher values indicate 
        a rotational pattern in the measured area

        activationGroup is a dict(), estimatedAFCL is the estimated AFCL from the activation times
        '''
    
        circularVariances = list()
        for act in activationGroup.values():
            act = act[act!=-1] # Invalid values
            c0 = np.where(act==np.min(act))[0][0]
            normAct = (np.asarray(act)-act[c0])/estimatedAFCL*(2*np.pi)
            circularMean, circularVariance = egmp.CircularMeanAndVariance(normAct)
            circularVariances.append(circularVariance)
        
        return np.asarray(circularVariances)
 
    def ComputeDirectionalPreferentiality(self,intervalPreferentiality):
        '''
        Based on the output of CV calculation, get the directional preferentiality
        '''
        Vx= np.array([np.nanmean(element[~np.isinf(element)]) for element in intervalPreferentiality['additionalInformation']['vx']])
        Vy= np.array([np.nanmean(element[~np.isinf(element)]) for element in intervalPreferentiality['additionalInformation']['vy']])
        angles = np.arctan2(Vy,Vx)
        _, circularVariance = egmp.CircularMeanAndVariance(angles)

        DirectionalPreferentiality = 1-circularVariance
        return DirectionalPreferentiality

    #%% Focal sources

# class CatheterPlacer:
#     def __init__(self,phaseJumpThreshold=1.5*np.pi):
