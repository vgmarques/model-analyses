"""
Created on Thu Oct  28 2021

DESCRIPTION: Contains functions to calculate information on directionality

The class division follows Stef's software, though not all functions have
been translated to Python.

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Import libraries
import numpy as np
import egm_processing as egmp
from copy import deepcopy
from scipy.linalg import svd  
from skimage.measure import label as bwlabeln 



class VelocityCalculator:

    def __init__(self):
        self.NormalConductionThreshold = 0.2   # Block conduction threshold (in mm/ms)
        self.Method = 'Planar'                 # CV computation methods
        self.FullArray = False                 # (T/F): all activations or activations exceeding
                                               # minimal normal conduction threshold (NormalConductionThreshold)
        self.Radius = np.sqrt(3.5**2+3.5**2)        # maximum distance from reference electrode in mm
        self.Hierarchical = False 
        self.CheckUniquePositions = True
        self.UseParallel = False
   
    def GetValidActivations(self,activations, positions, referencePosition, referenceTime):
        positionDifferences = positions-referencePosition
        distances = np.sqrt(positionDifferences[:,0]**2 +positionDifferences[:,1]**2+positionDifferences[:,2]**2)
        activationDifference = activations - referenceTime

        localVelocity = np.abs(distances/activationDifference)
        localVelocity[(distances == 0) * (activationDifference == 0)] = np.inf
        localVelocity[(distances == 0) * (activationDifference != 0)] = np.nan

        validVelocities = localVelocity > self.NormalConductionThreshold 
        validActivations = (validVelocities) * (~np.isnan(activations))
        # select unique (closest) activation for each position
        if not validActivations.any():
            return validActivations

        if self.CheckUniquePositions: 
            uniquePositions,groupIndex = np.unique(positions[validActivations, :],axis=0,return_index=True)
            numberOfPositions = len(np.where(validActivations)[0])
            numberOfUniquePositions = uniquePositions.shape[0]
            if numberOfUniquePositions == numberOfPositions:
                return validActivations

            selectedActivationDifference = activationDifference[validActivations]
            selectedPositionIndices = np.where(validActivations)[0]
            for positionIndex  in range(numberOfUniquePositions):
                currentPositionIndices = np.where(groupIndex == positionIndex)[0]
                if len(currentPositionIndices) > 1:
                    validActivations[selectedPositionIndices[currentPositionIndices]] = False
                    _, minimalPosition = np.min(np.abs(selectedActivationDifference[currentPositionIndices]))
                    validActivations[selectedPositionIndices[currentPositionIndices[minimalPosition]]] = True       
        return validActivations

    def Compute2DActivationWavefront(self,activations, positions,indices = None):
        '''
        COMPUTE2DACTIVATIONWAVEFRONT computes 2D velocity vector by fitting a function through the
        activation times at a (grid of) electrodes:
            - Planar: a plane
            - Biquadratic: a quadratic surface
            - Min_Max: vector from minimum to maximum activation time
        - activations: array of activation times (in ms) for each electrode
        - positions: electrode positions (2D or 3D)
        - indices (optional): indices of electrodes for which to compute
        velocity (all if missing)
        OUTPUT:
        - xVelocity & yVelocity: x,y elements of 2D velocity vectors
        '''
        if indices is None:
            indices = np.arange(len(positions))

        if self.Method=='Planar':
            fhandle = self.ComputePlaneVelocity
        elif self.Method=='Biquadratic':
            RuntimeWarning('Biquadratic velocity has not been implemented in Python yet. Using Planar instead')
            fhandle = self.ComputePlaneVelocity#@self.ComputeBiquadraticVelocity
        elif self.Method=='Min_Max':
            fhandle = self.ComputeMinMaxVelocity
        else:
            fhandle = self.ComputePlaneVelocity


        xVelocity = np.zeros(len(indices))
        yVelocity = np.zeros(len(indices))

        for positionIndex in range(len(indices)):
            currentPosition = positions[indices[positionIndex], : ]
            currentTime = activations[indices[positionIndex]]
            positionDifferences = positions - currentPosition
            distances = np.sqrt(positionDifferences[:,0]**2 + positionDifferences[:,1]**2 + positionDifferences[:,2]**2)
            validPositions = distances <= self.Radius

            if self.FullArray:
                validActivations = ~np.isnan(activations)
            else:
                validActivations = self.GetValidActivations(activations, positions, currentPosition, currentTime)

            validIndices = (validPositions.astype(bool)) * (validActivations.astype(bool))

            xVelocity[positionIndex], yVelocity[positionIndex] =fhandle(activations[validIndices],
                                                                        positions[validIndices, :])
        return xVelocity,yVelocity

    # Methods for velocity calculation
    def ComputePlaneVelocity(self,activations, positions):
        '''
        COMPUTE_PLANE_VELOCITY computes the x- and y-velocity of a certain matrix
        based on a plane fitted onto the data
        '''
        xVelocity = np.nan
        yVelocity = np.nan
        rows = len(activations)
        if rows==0 or rows==1:
            # print('P1')
            return xVelocity,yVelocity
        elif rows==2:
            if self.Hierarchical:
                # print('P2')
                xVelocity, yVelocity = self.ComputeMinMaxVelocity(activations, positions) #!
                return xVelocity,yVelocity

        else:
            Z = activations
            X = np.zeros((rows, 3))
            X[:,0] = 1
            X[:,1:] = positions[:,:2]

            if 1e+10 < np.linalg.cond(np.dot(X.T,X)):
                if self.Hierarchical:
                    # print('P3')
                    xVelocity, yVelocity = ComputeMinMaxVelocity(activations, positions)
                    return xVelocity,yVelocity

            A,resid,rank,s = np.linalg.lstsq(X, Z,rcond=None) # Solve the system Xv = Z for v. A is the solution Z/X
            '''
            The problem here is that the 2D does not quite explain everything that is happening (which is in theory ok)

            If I understood correctly, these angles are in the base of global x,y,z, because the coordinates of the electrodes
            are in this system. However, I want to determine the distances in the plane of the electrode. I believe the easiest
            way to do this is to change the basis of A. TODO: maybe vec_new = np.linalg.inv(np.array([w1, w2, w3])).dot(vec_old)

            The second thing is that I also want to have a Z velocity. This is not essential for determining the arrows, but may
            make the plot better. TODO

            '''            
            r = np.linalg.norm([A[1],A[2]])
            if 1e-1 < r:
                alpha = np.arctan2(A[2], A[1])
                diagonalVelocity = 1 / r
                xVelocity = np.cos(alpha) * diagonalVelocity
                yVelocity = np.sin(alpha) * diagonalVelocity
            else:
                # print('P4')
                xVelocity = np.inf
                yVelocity = np.inf

        return xVelocity, yVelocity

    def ComputeMinMaxVelocity(self, activations, positions):
        # COMPUTEMINMAXVELOCITY determine maximum and minimum grid value and return
        # the corresponding direction
        maxValue = np.max(activations)
        maxIndex = np.where(activations==maxValue)[0][0]
        maxPosition = positions[maxIndex, :]
        minValue = np.min(activations)
        minIndex = np.where(activations==minValue)[0][0]
        minPosition = positions[minIndex, :]
                
        if np.isnan(maxValue):
            xVelocity = np.nan
            yVelocity = np.nan
            return xVelocity, yVelocity
                
        timeDifference = maxValue - minValue
        distance = np.linalg.norm(maxPosition - minPosition, 2)
        if (timeDifference ==0) or (distance == 0):
            xVelocity = np.nan
            yVelocity = np.nan
            return xVelocity, yVelocity

        velocity = distance / timeDifference
        distanceVector = maxPosition - minPosition
        alpha = np.arctan2(distanceVector[0,:], distanceVector[1,:])
        xVelocity = np.cos(alpha) * velocity
        yVelocity = np.sin(alpha) * velocity
        return xVelocity, yVelocity

    # External stuff
    def GroupValidActivations(self,allActivations,allPositions,allElectrodeIndices,minPercentageOfElectrodes=0.6):
        # Determine valid activations
        validActivations = list()
        for positionIndex in range(len(allActivations)):
            currentPosition = allPositions[positionIndex, : ]
            currentTime = allActivations[positionIndex]
            validActivations.append(self.GetValidActivations(allActivations, allPositions, currentPosition, currentTime))

        validActivations = np.asarray(validActivations)

        # Group them and output in a dict
        activationGroups = dict()
        visited = np.zeros(allActivations.shape,dtype=bool)
        NElec = len(np.unique(allElectrodeIndices))
        for actIndex,activation in enumerate(allActivations):
            # Get valid activations for this group
            validIndexes = np.where(validActivations[actIndex,:])[0]

            # check if visited
            if visited[actIndex]: 
                visited[validIndexes] = True
                continue

            # Append as a group if enough electrodes have been activated
            if len(validIndexes)<int(np.round(NElec*minPercentageOfElectrodes)): continue

            actGroup = allActivations[validIndexes]
            activationGroups[np.min(actGroup)] = -np.ones(NElec,dtype=int)
            activationGroups[np.min(actGroup)][allElectrodeIndices[validIndexes]] = actGroup
            # # Mark all as visited
            visited[validIndexes] = True
        
        sortedGroups = dict()
        for key in np.sort(list(activationGroups.keys())):
            sortedGroups[key] = activationGroups[key]

        return sortedGroups


    def AdjustSignalsCV(self,CatheterObj,useRealPositions=False):
        # Get data from catheter objects and organizes in the way the algorithm understands

        numberOfElectrodes = CatheterObj.GetNumberOfChannels()

        if not useRealPositions: 
            electrodePositions = np.hstack([CatheterObj.ElectrodesTemplate,np.ones((numberOfElectrodes,1))])
        else:
            # First, we center the electrode positions
            self.CatheterCenter = CatheterObj.ElectrodeCoordinates.mean(axis=0)
            electrodePositionsC = CatheterObj.ElectrodeCoordinates-self.CatheterCenter

            # Than, we rotate it so that the normal of the electrode plane is aligned with the z axis (which is "ignored" when calculating velocities)
            U,s,V = svd(electrodePositionsC)
            u = V.T[:,0] 
            v = V.T[:,1] 
            n = V.T[:,2]   
            self.Rn =  egmp.MakeRotationalMatrix(n,np.array([0,0,1])) #  Create rotation matrix to align n with z axis
            electrodePositions = np.matmul(self.Rn,electrodePositionsC.T).T 

            # # Lastly, we ignore the normal component and treat as a plane. We alignd this plane with the axis by a second rotational matrix
            # U,s,V = svd(electrodePositionsN[:,:2])
            # u = V.T[:,0] 
            # v = V.T[:,1]
            # self.Ru =  egmp.MakeRotationalMatrix(np.append(u,0),np.array([1,0,0]))
            # electrodePositions = np.matmul(self.Ru,electrodePositionsN.T).T 



        allActivations =[]
        allPositions = []
        allElectrodeIndices = []
        for electrodeIndex in range(numberOfElectrodes):
            electrodeActivations = CatheterObj.Activations[electrodeIndex]
            numberOfActivations = len(electrodeActivations)

            for i in range(numberOfActivations):
                allActivations.append(electrodeActivations[i])
                allPositions.append(electrodePositions[electrodeIndex, :])    
                allElectrodeIndices.append(electrodeIndex)

        allActivations = np.array(allActivations)
        allPositions = np.array(allPositions)
        allElectrodeIndices = np.array(allElectrodeIndices)

        return allActivations,allPositions,allElectrodeIndices

    def AdjustVelocityGlobal(self,xVelocity,yVelocity):
        '''
        The velocity is actually calculated for the electrode locally.
        To plot it in the global positioning, it is necessary to revert all of the transformations
        '''
        localVelocities = np.array([xVelocity,yVelocity,np.zeros(len(xVelocity))]).T
        

        # globalVelocities = localVelocities#np.matmul(np.linalg.inv(self.Ru),localVelocities.T).T #TODO
        globalVelocities = np.matmul(np.linalg.inv(self.Rn),localVelocities.T).T

        return globalVelocities

    def MakeCVDict(self,allActivations,allElectrodeIndices,xVelocity,yVelocity):
        # Gets output of algorithm to make
        ConductionVelocity = dict()
        ConductionVelocity['Activations'] = list()
        ConductionVelocity['xVelocity'] = list()
        ConductionVelocity['yVelocity'] = list()
        for chosenElectrodeIndex in range(len(np.unique(allElectrodeIndices))):
            ConductionVelocity['Activations'].append(allActivations[allElectrodeIndices==chosenElectrodeIndex])
            ConductionVelocity['xVelocity'].append(xVelocity[allElectrodeIndices==chosenElectrodeIndex])
            ConductionVelocity['yVelocity'].append(yVelocity[allElectrodeIndices==chosenElectrodeIndex])
        return ConductionVelocity


class VelocityAnalyzer:

    def __init__(self):
        self.SupranormalThreshold = np.inf

    def ComputePreferentialVelocities(self,electrodeVelocities, bidirectional = True):
        additionalInformationStruct ={'vx':[],'vy':[],
                                    'vxNormalized':[],'vyNormalized':[],
                                    'vxFinal':[],'vyFinal':[],
                                    'normalized':[],'supranormalThreshold':[]}

        numberOfElectrodes = len(electrodeVelocities['xVelocity'])

        preferentialVelocities = {'angle':np.ones(numberOfElectrodes)*np.nan,
                                'value':np.ones(numberOfElectrodes)*np.nan,
                                'additionalInformation':additionalInformationStruct}

        for electrodeIndex in range(0,numberOfElectrodes):
            preferentialAngle, preferentiality = self.ComputeElectrodePreferentialVelocity(electrodeVelocities,electrodeIndex,
                                                                                    bidirectional)
            preferentialVelocities['angle'][electrodeIndex] = preferentialAngle
            preferentialVelocities['value'][electrodeIndex] = preferentiality

            vx = electrodeVelocities['xVelocity'][electrodeIndex]
            vy = electrodeVelocities['yVelocity'][electrodeIndex]
            preferentialVelocities['additionalInformation']['vx'].append(vx)
            preferentialVelocities['additionalInformation']['vy'].append(vy)


        return preferentialVelocities
    
    def ComputeElectrodePreferentialVelocity(self,electrodeVelocities,electrodeIndex, bidirectional = True):
        vx = electrodeVelocities['xVelocity'][electrodeIndex]
        vy = electrodeVelocities['yVelocity'][electrodeIndex]
        V = np.sqrt(vx**2 + vy**2)

        validPositions = (~np.isnan(V)) & (~np.isinf(V)) & (V < self.SupranormalThreshold)
        if validPositions.any():
            angles = np.arctan2(vy[validPositions], vx[validPositions])
            if bidirectional:
                angles = (angles%np.pi) * 2
                circularMean, circularVariance = egmp.CircularMeanAndVariance(angles)
                preferentialAngle = (circularMean/2)% np.pi
            else:
                circularMean, circularVariance = egmp.CircularMeanAndVariance(angles)
                preferentialAngle = circularMean

            angleVariance = circularVariance
            preferentiality = (1 - angleVariance)
        else:
            preferentialAngle = np.nan
            preferentiality = np.nan
            
        return preferentialAngle, preferentiality

    def VelocityAngleComparison(self,currentPositions,currentVelocities,previousPositions,previousVelocities,
                            angularThreshold=np.pi/2,distanceThreshold=20):
        '''
        This function compares what velocities would be expected from interpolating previous points vs. what is actually measured.
        
        If the angle between the new velocities and the old is higher than the angular threshold, this is annotated in the output
        
        The distance threshold (in mm) is used to select only close points.
        '''
        
        AnglesWithPrevious = np.zeros(len(currentPositions))*np.nan
        InterpolatedVelocities = np.zeros_like(currentVelocities)
        
        for i,element in enumerate(currentPositions):
            distances = np.linalg.norm(previousPositions-element,axis=1)
            distances[distances>distanceThreshold] =np.inf
            distances[np.isnan(previousVelocities).any(axis=1)] = np.inf
            if np.sum(~np.isinf(distances))<4: continue
                # return AnglesWithPrevious

            inds = np.argsort(distances)[1:]
            
            # weighted average with weight= d^-2
            InterpolatedVelocity = np.sum(distances[inds]**-2 * previousVelocities[inds].T,axis=1)/np.sum(distances[inds]**-2)
            InterpolatedVelocities[i,:] = InterpolatedVelocity.T
            AnglesWithPrevious[i] = np.arccos(np.dot(InterpolatedVelocity/np.linalg.norm(InterpolatedVelocity),
                            currentVelocities[i,:]/np.linalg.norm(currentVelocities[i,:])))
        
        
        return AnglesWithPrevious

    # Wrappers
    def AnalyzeVelocity(self,intervalOfInterest,ConductionVelocity):
        # Wrapper for the 
        intervalComponentVelocities = deepcopy(ConductionVelocity)
        for electrodeIndex in range(len(ConductionVelocity['Activations'])):
            electrodeActivations = intervalComponentVelocities['Activations'][electrodeIndex]
            validVelocities = np.zeros(len(intervalComponentVelocities['Activations'][electrodeIndex]),dtype=bool)
            validVelocities = (electrodeActivations >= intervalOfInterest[0]) * (electrodeActivations < intervalOfInterest[1])

            intervalComponentVelocities['xVelocity'][electrodeIndex] = intervalComponentVelocities['xVelocity'][electrodeIndex][validVelocities]
            intervalComponentVelocities['yVelocity'][electrodeIndex] = intervalComponentVelocities['yVelocity'][electrodeIndex][validVelocities]
            intervalComponentVelocities['Activations'][electrodeIndex] = intervalComponentVelocities['Activations'][electrodeIndex][validVelocities]

        intervalPreferentiality = self.ComputePreferentialVelocities(intervalComponentVelocities, False)
        return intervalPreferentiality

    def ComputeMeanVelocityGlobal(self,globalVelocity,allActivations,allElectrodeIndices,timeInterval):

        numberOfElectrodes = len(np.unique(allElectrodeIndices))
        
        meanVelocityElectrode = np.zeros((numberOfElectrodes,3))

        for electrodeIndex in range(numberOfElectrodes):
            timeInds = np.where((allActivations>=timeInterval[0]) & (allActivations<timeInterval[1]))[0]

            electrodeInds = np.where(allElectrodeIndices[timeInds]==electrodeIndex)[0]

            try:
                meanVelocityElectrode[electrodeIndex,:] = np.nanmedian(globalVelocity[electrodeInds,:],axis=0)
            except:
                meanVelocityElectrode[electrodeIndex,:] *= np.nan

        return meanVelocityElectrode

    def ComputeMeanVelocityV2(self,intervalPreferentiality,vCalc):

        meanVelocities = np.array([np.cos(intervalPreferentiality['angle']),
                           np.sin(intervalPreferentiality['angle']),
                           np.zeros_like(np.cos(intervalPreferentiality['angle']))]).T

        # meanVelocities = np.matmul(np.linalg.inv(vCalc.Ru),meanVelocities.T).T 
        meanVelocities = np.matmul(np.linalg.inv(vCalc.Rn),meanVelocities.T).T 

        return meanVelocities

    def NextCatheterPlacement(self,Velocities3D,CatheterObj,Anatomy,dS = 10,previousValid=None):
        '''
        Velocities3D: 3D conduction velocities
        CatheterObj: current catheter
        Anatomy: anatomy in shape (N,3)
        dS: distance to move catheter, in mm
        '''
        
        # Determine the direction to move the catheter as the oposite of 
        # the average propagation direction in the current position
        originDir = -np.nanmedian(Velocities3D,axis=0) 

        # Calculate the cosine between the origin direction and the vectors
        # formed by each point and the catheter center
        # Get all points that are in this direction
        catheterVectors = Anatomy-np.tile(CatheterObj.CatheterCenter,(len(Anatomy),1))
        positionCosines = np.dot(catheterVectors,originDir)
        directionAngles = positionCosines>=0
        
        # Select a subset of points that are less than 2*dS mm away from catheter
        # This speeds up the computation of adjacency matrices and geodesic distances
        euclidianDistances = np.linalg.norm(catheterVectors,axis=1)
        closeIdx = euclidianDistances<=2*dS

        ## indexes to track back to anatomy
        if previousValid is not None:
            directionAngles = directionAngles&previousValid

        Anatomy2PatchIdx = np.where(closeIdx&directionAngles)[0] 
   

        extendedPatch = Anatomy[Anatomy2PatchIdx]
        Source =  np.where(euclidianDistances[Anatomy2PatchIdx]==np.min(euclidianDistances[Anatomy2PatchIdx]))[0][0] # Catheter center in patch


        
        # Calculate geodesic distance between catheter center and the extended patch
        AdjacencyMatrix = egmp.MakeAdjacencyMatrix(1,extendedPatch)
        GeodesicDistances = egmp.GeodesicDistances(Source,AdjacencyMatrix)
        GeodesicDistances[np.isnan(GeodesicDistances)] = np.inf
        
        # Find the places where the geodesic distance is around dS
        Patch2RingIdx = np.where(np.abs(GeodesicDistances-dS)<=1)[0] #TODO maybe increase this tolerance
        Anatomy2RingIdx = Anatomy2PatchIdx[Patch2RingIdx]

        # Select element with the highest cosine for next position
        ringCosines = positionCosines[Anatomy2RingIdx]
        nextPos = np.where(ringCosines==np.max(ringCosines))[0][0]
        nextPos = Anatomy2RingIdx[nextPos]
         
        return int(nextPos),directionAngles

    def NextCatheterPlacementV2(self,Velocities3D,CatheterObj,Anatomy,dS = 10,previousInvalid=None):
        '''
        Velocities3D: 3D conduction velocities
        CatheterObj: current catheter
        Anatomy: anatomy in shape (N,3)
        dS: distance to move catheter, in mm
        '''
        
        originDir = -np.nanmedian(Velocities3D,axis=0) 

        # Calculate the cosine between the origin direction and the vectors
        # formed by each point and the catheter center
        # Get all points that are in this direction
        catheterVectors = Anatomy-np.tile(CatheterObj.CatheterCenter,(len(Anatomy),1))
        positionCosines = np.dot(catheterVectors,originDir)
        directionAngles = positionCosines>=0

        # indexes to track back to anatomy
        if previousInvalid is not None:
            directionAngles = directionAngles&~previousInvalid
            # previousInvalidIdx = np.zeros(len(Anatomy),dtype=bool)
            # previousInvalidIdx[previousInvalid] = True

            # directionAngles = fullDirectionAngles & ~previousInvalidIdx
            # closeFlag = directionAngles.sum()>fullDirectionAngles.sum()
        else:
            previousInvalid = np.zeros(len(Anatomy),dtype=bool)
            # closeFlag=False

        # Select a  2*dS mm sphere from catheter
        # This speeds up the computation of adjacency matrices and geodesic distances

        euclidianDistances = np.linalg.norm(catheterVectors,axis=1)
        closeIdx = euclidianDistances<=2*dS
        GlobalDistIdx = np.where(closeIdx)[0]  
        DirectionInPatch = directionAngles[GlobalDistIdx]

        extendedPatch = Anatomy[GlobalDistIdx]
        Source =  np.where(euclidianDistances[GlobalDistIdx]==np.min(euclidianDistances[GlobalDistIdx]))[0][0] # Catheter center in patch

        # Calculate geodesic distance between catheter center and the extended patch
        AdjacencyMatrix = egmp.MakeAdjacencyMatrix(1,extendedPatch)
        GeodesicDistances = egmp.GeodesicDistances(Source,AdjacencyMatrix)
        GeodesicDistances[np.isnan(GeodesicDistances)] = np.inf

        # Mark regions behind the direction of propagation as "invalid" for the following positioning
        patchCloseIdx = GeodesicDistances<=2*dS
        invalidIdx = ~DirectionInPatch&patchCloseIdx # invalid directions closed than 2*dS
        
        previousInvalid[GlobalDistIdx[invalidIdx]] = True

        # validIdx = DirectionInPatch&patchCloseIdx
        GeodesicDistances = GeodesicDistances[DirectionInPatch]

        # Find the places where the geodesic distance is around dS
        Patch2RingIdx = np.where(np.abs(GeodesicDistances-dS)<=1)[0] #TODO maybe increase this tolerance
        Anatomy2RingIdx = GlobalDistIdx[DirectionInPatch][Patch2RingIdx]

        # Select element with the highest cosine for next position
        ringCosines = positionCosines[Anatomy2RingIdx]
        try: 
            nextPos = np.where(ringCosines==np.max(ringCosines))[0][0] 
        except:
            print('Too many invalid regions. Reset previous invalid positions')
            nextPos,previousInvalid,closedFlag,regionIndexes = self.NextCatheterPlacementV2(Velocities3D,
                                                                              CatheterObj,
                                                                              Anatomy,
                                                                              dS,
                                                                              previousInvalid=None)
            return int(nextPos),previousInvalid,closedFlag,regionIndexes
        
        nextPos = Anatomy2RingIdx[nextPos]

        # Check for the presence of an enclosed region
        Regions = np.zeros(shape=np.max(Anatomy,axis=0)+1)
        Regions[Anatomy[~previousInvalid,0],Anatomy[~previousInvalid,1],Anatomy[~previousInvalid,2]] = 1

        labels, nLab = bwlabeln(Regions,connectivity=3,background=0,return_num=True)

        closedFlag=False
        regionIndexes= None
        if nLab>1:
            for L in range(1,nLab+1):
                lx,ly,lz = np.where(labels==L)
                regionCenter = np.median(np.array([lx,ly,lz]),axis=1).astype(int)
                distCenter = np.linalg.norm(CatheterObj.CatheterCenter-regionCenter,axis=0)
                if distCenter<2*dS and len(lx)<1000 and len(lx)>100:
                    print('Enclosed region near electrode')
                    dirInds = np.array([np.where((Anatomy[:,0]==lx[i])&\
                                                (Anatomy[:,1]==ly[i])&\
                                                (Anatomy[:,2]==lz[i]))[0][0] for i in range(len(lx))])

                    if directionAngles[dirInds].any():
                        print('Region in front of electrode')
                        print('label %d'%L)
                        closedFlag=True
                        regionIndexes = dirInds
                        


          
        return int(nextPos),previousInvalid,closedFlag,regionIndexes