import cProfile
import pstats
import argparse

parser = argparse.ArgumentParser(description='Translates cProfile output to human readable format. Run the code before with \
												python -m cProfile -o profiling.out my_script.py. For memory profile use mprof.\
												You can use snakeviz for visualizing results without translating. ')
parser.add_argument('inputFile',action='store',type=str,
						help = 'File in binary format') 
parser.add_argument('-o',action='store',type=str,default = 'output.txt',
						help = 'Translated file, default output.txt') 
parser.add_argument('-s',action='store',type=str,default = 'cumtime',
						help = 'Sort by..., default cumtime') 
opt = parser.parse_args()


stream = open(opt.o, 'w')
stats = pstats.Stats(opt.inputFile, stream=stream)
stats.sort_stats(opt.s)
stats.print_stats()
print('Done!')


