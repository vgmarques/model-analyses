# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 14:00:21 2021

DESCRIPTION: Contains objects to get EGM data from the models.
The CatheterClass does the heavy duty, while CatheterGroup calls its functions

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import numpy as np
import scipy.io as sio
from scipy.linalg import svd  
import egm_processing as egmp

class CatheterClass:
    
    def __init__(self,catheterModel):
        
        # List of class properties that will be defined along the usage of this class
        # Template related 
        self.ElectrodeSpacing = None
        self.NumberOfElectrodesPerSpline = None
        self.ExtraPoints = None
        self.PlotSplines = None
        self.ElectrodesTemplate = None

        # Catheter center
        self.CatheterCenterIndex = None
        self.CatheterCenter = None

        # Patch related 
        self.PatchIndexes = None
        self.Patch = None
        self.GlobalPatchCenter = None
        self.LocalPatchCenter = None
        self.PatchNormal = None
 
        # Electrode related = None
        self.ElectrodeCoordinates = None
        self.ElectrodeIndexes = None

        # Data related
        self.EGMData = None
        self.Activations = None
        
        #################
        # Class Properties that are set here
        self.CatheterModel = catheterModel
        self.GetCatheterTemplate()
        self.Rotation = 0
        self.SpatialResolution = 1.
        self.SamplingFrequency = 1000.
        
    def GetCatheterTemplate(self):   
        '''
        The electrode templates are N x 2 arrays where each point in a 2D plane
        is depicted. Splines should be organized sequentially, i.e. first NElectrodes
        points belong to the first spline, and so on. 

        ExtraPoints and PlotSplines are used only if we want to have a realistic catheter
        representation. This can be safely ignored.

        Because not all catheters have straight splines, it is necessary to define a projection function
        to use (self.ProjectionFunction)
        '''

        if self.CatheterModel=='HDGrid-4' or self.CatheterModel == 'hdgrid':
            self.ElectrodeSpacing = [3,3,3] # mm considering the electrode width, 3 distances for 4 electrodes
            self.NumberOfElectrodesPerSpline = 4    
            self.SplineSpacing = 2
            x = np.arange(self.NumberOfElectrodesPerSpline)*self.ElectrodeSpacing[0] # Number of electrodes  and electrode spacing
            x = x-np.mean(x)
            y = np.arange(4)*self.SplineSpacing # Number of splines  and spline spacing
            y = y-np.mean(y)

            # This needs to be updated
            self.ExtraPoints = None#np.array([[-8,0],[7,-1],[7,1]])
            self.PlotSplines = [[]]#[[-3,0,1,2,3,-2,-1,15,14,13,12,-3],[-3,4,5,6,7,-2,-1,11,10,9,8,-3]]  
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([grid[0].flatten(),grid[1].flatten()]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter
        
        elif self.CatheterModel == 'Pentaray' or self.CatheterModel == 'pentaray':
            self.ElectrodeSpacing = np.array([2,6,2]) # mm considering the electrode width, 3 distances for 4 electrodes
            self.NumberOfElectrodesPerSpline = 4 
            # Rotational pattern
            angles = np.linspace(0,2*np.pi*4/5,5)
            distFromCenter = 3.5
            circle1 = np.array([distFromCenter*np.cos(angles),
                               distFromCenter*np.sin(angles)]).T
            circle2 = np.array([(distFromCenter+self.ElectrodeSpacing[0])*np.cos(angles),
                                (distFromCenter+self.ElectrodeSpacing[0])*np.sin(angles)]).T
            circle3 = np.array([(distFromCenter+self.ElectrodeSpacing[0]+self.ElectrodeSpacing[1])*np.cos(angles),
                                (distFromCenter+self.ElectrodeSpacing[0]+self.ElectrodeSpacing[1])*np.sin(angles)]).T
            circle4 = np.array([(distFromCenter+self.ElectrodeSpacing.sum())*np.cos(angles),
                                (distFromCenter+self.ElectrodeSpacing.sum())*np.sin(angles)]).T
            # Make splines
            spline1 = np.array([circle1[0,:],circle2[0,:],circle3[0,:],circle4[0,:]])
            spline2 = np.array([circle1[1,:],circle2[1,:],circle3[1,:],circle4[1,:]])
            spline3 = np.array([circle1[2,:],circle2[2,:],circle3[2,:],circle4[2,:]])
            spline4 = np.array([circle1[3,:],circle2[3,:],circle3[3,:],circle4[3,:]])
            spline5 = np.array([circle1[4,:],circle2[4,:],circle3[4,:],circle4[4,:]])
            # Make template
            self.ElectrodesTemplate = np.vstack([spline1,spline2,spline3,spline4,spline5])
            # This needs to be updated
            self.ExtraPoints = None 
            self.PlotSplines = None 
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel=='MiniGrid':
            self.NumberOfElectrodesPerSpline = 12    
            self.ElectrodeSpacing = np.ones(self.NumberOfElectrodesPerSpline-1) # mm considering the electrode width, 3 distances for 4 electrodes
            self.SplineSpacing=1
            x = np.arange(self.NumberOfElectrodesPerSpline)*self.ElectrodeSpacing[0] # Number of electrodes  and electrode spacing
            x = x-np.mean(x)
            y = np.arange(12)*self.SplineSpacing # Number of splines  and spline spacing
            y = y-np.mean(y)
            self.ExtraPoints = None
            self.PlotSplines = None   
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([grid[0].flatten(),grid[1].flatten()]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel=='mega':
            self.NumberOfElectrodesPerSpline = 20    
            self.ElectrodeSpacing = np.ones(self.NumberOfElectrodesPerSpline-1) # mm 
            self.SplineSpacing = 1
            x = np.arange(self.NumberOfElectrodesPerSpline)*self.ElectrodeSpacing[0] # Number of electrodes  and electrode spacing
            x = x-np.mean(x)
            y = np.arange(20)*self.SplineSpacing  # Number of splines  and spline spacing
            y = y-np.mean(y)
            # This needs to be updated
            self.ExtraPoints = None
            self.PlotSplines = None
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([grid[0].flatten(),grid[1].flatten()]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel=='mega2':
            self.ElectrodeSpacing = np.ones(9)*2 # mm 
            self.NumberOfElectrodesPerSpline = 10    
            x = np.arange(self.NumberOfElectrodesPerSpline)*self.ElectrodeSpacing[0] 
            y = np.arange(10)*2 # Number of splines  and spline spacing
            y = y-np.mean(y)
            # 
            self.ExtraPoints = None
            self.PlotSplines = None
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([grid[0].flatten(),grid[1].flatten()]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel=='mega3':
            self.ElectrodeSpacing = np.ones(7)*3 # mm
            self.NumberOfElectrodesPerSpline = 7    
            x = np.arange(self.NumberOfElectrodesPerSpline)*self.ElectrodeSpacing[0]
            x = x-np.mean(x)
            y = np.arange(7)*3 # Number of splines and spline spacing
            y = y-np.mean(y)
            # This needs to be updated
            self.ExtraPoints = None
            self.PlotSplines = None
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([grid[0].flatten(),grid[1].flatten()]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel=='lasso':
            #Not yet determined
            r = 10
            angles = np.linspace(0,360,10+1)*np.pi/180
            x = r*np.cos(angles)
            y = r*np.sin(angles) 
            # Make template  
            grid = np.meshgrid(x,y)
            self.ElectrodesTemplate = np.array([x,y]).T
            # Set function to use
            self.ProjectionFunction = self.PlaceCatheter

        elif self.CatheterModel == 'CS_10':
            # CS placement is less flexible than the others, and has been adapted into this code
            # If I must say, very poorly. But it works

            self.NumberOfElectrodesPerSpline = 10
            self.ElectrodeSpacing = [2,8]
            self.ElectrodesTemplate = np.array([[0,0],[0,30]]) # need 42 mm
            self.ExtraPoints = None
            self.SpatialResolution = 1.5
            self.ProjectionFunction = self.PlaceCSCatheter

        elif self.CatheterModel == 'CS_20':
            print('Not yet implemented')            
        else:
            print('Unknown catheter type. No template was created')
            self.ProjectionFunction = self.PlaceCatheter
            return None
    
    ##### External functions    

    def GetTissuePatch(self,catheterCenterIndex,anatomy,radiusMultiplier=1.6,geodesicThreshold =-1):
        
        # catheterCenterIndex can be an index of the anatomy or a point directly
        # Select the proper case here
        if isinstance(catheterCenterIndex,int) or np.isin(catheterCenterIndex.dtype,['int32','int64','int']):
            self.CatheterCenterIndex = catheterCenterIndex
            self.CatheterCenter = anatomy[catheterCenterIndex,:]
        else:
            if catheterCenterIndex.shape[-1]!=3: 
                print('Error! Wrong dimension')
                return -1
            else:
                self.CatheterCenterIndex = None
                self.CatheterCenter = catheterCenterIndex         
        
        # If we want to have a "realistic" electrode shape, add Extra points that can be drawn over with splines
        if self.ExtraPoints is not None:
            CatheterTemplate = np.concatenate([self.ElectrodesTemplate,self.ExtraPoints],axis =0)
        else:
            CatheterTemplate = self.ElectrodesTemplate
        
        # Set the radius of the patch based on the size of the catheter and on radiusMultiplier
        PatchRadius = radiusMultiplier*np.max([np.linalg.norm(CatheterTemplate[i,:]-np.array([0,0]),axis=0) \
                                                for i in range(CatheterTemplate.shape[0])])
        DistancesFromCenter = np.linalg.norm(anatomy-self.CatheterCenter,axis = 1)
        PatchIdx = np.where(DistancesFromCenter<=PatchRadius)[0]
        if self.ProjectionFunction == self.PlaceCSCatheter:
            self.Patch = PatchIdx
            self.GlobalPatchCenter = None
            self.LocalPatchCenter = None
            return 0       

        # Initial patch, before geodesic distance
        Rp = anatomy[PatchIdx,:]

        # Geodesic distance
        AdjacencyMatrix = egmp.MakeAdjacencyMatrix(self.SpatialResolution,Rp)
        dCenter = np.linalg.norm(Rp - self.CatheterCenter,axis=1)
        Source = np.where(dCenter==np.min(dCenter))[0][0]
        GeodesicDistances = egmp.GeodesicDistances(Source,AdjacencyMatrix)
        if geodesicThreshold==-1: 
            geodesicThreshold= int(1.5*np.max(self.ElectrodesTemplate.max(axis=0)-self.ElectrodesTemplate.min(axis=0)))

        GeodesicIdx = np.where(GeodesicDistances<=geodesicThreshold) 
        
        # Patch after geodesic distance
        Rs= Rp[GeodesicIdx]
        self.PatchIndexes = PatchIdx[GeodesicIdx]
        
        PatchCenter = np.mean(Rs,axis=0)
        self.Patch = Rs-PatchCenter
        self.GlobalPatchCenter = PatchCenter
        self.LocalPatchCenter = self.Patch[np.where((Rs == self.CatheterCenter).all(axis=1))[0].squeeze(),:]
        
    def PlaceCatheter(self,anatomy, rotationAngle = 0,offset = 10,returnPlotCatheter = False,
                       closerToMean = True,
                       **kwargs):
        if self.ExtraPoints is not None:
            CatheterTemplate = np.concatenate([self.ElectrodesTemplate,self.ExtraPoints],axis =0)
        else:
            CatheterTemplate = self.ElectrodesTemplate
        # Rotate template if necessary
        R = egmp.MakeRotationalMatrix(rotationAngle)
        CatheterTemplate = np.matmul(CatheterTemplate,R) 
        
        # Fit plane to the points
        U,s,V = svd(self.Patch)
        u = V.T[:,0] 
        v = V.T[:,1]
        n = V.T[:,2]   

        # r =  egmp.MakeRotationalMatrix(u,np.array([1,0,0]))
        # RotatedPatch = np.matmul(self.Patch,r)

        self.PatchNormal = n
        # Create template
        CtA = np.array([electrode[0]*u+electrode[1]*v-offset*n for electrode in CatheterTemplate])
        CtB = np.array([electrode[0]*u+electrode[1]*v+offset*n for electrode in CatheterTemplate])

        # Make sure we are in the right side of the mesh
        distA = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtA,axis = 0))
        distB = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtB,axis = 0))
        
        if distB<distA and (distA>=15 or distB>=15):
            n = -n
            Ct = CtB
        else:
            Ct = CtA     
        
        #Connect template to patch
        # nLines = 20
        # Lines = {}
        # for i,electrode in enumerate(Ct):
        #     line = []
        #     interval = offset/nLines
        #     for l in range(1,nLines+1):
        #           line.append(electrode+l*interval*n)
        #     Lines[i] = np.asarray(line)
            
        # # Get closest point to lines
        # self.ElectrodeCoordinates = np.zeros_like(Ct)
        # self.ElectrodeIndexes = np.zeros(Ct.shape[0],dtype = np.int)
        
        # for key,line in Lines.items():
        #     dists = [np.linalg.norm(RotatedPatch[i,:]-line,axis = 1) \
        #               for i in range(RotatedPatch.shape[0])]
        #     ind = np.where(dists==np.min(dists))[0][0]
        #     self.ElectrodeCoordinates[key,:] = self.Patch[ind,:]+self.GlobalPatchCenter
        #     self.ElectrodeIndexes[key] = self.PatchIndexes[ind]

        # New Version
        # use actual line distances
        TempElectrodeInds = list()
        for i,element in enumerate(Ct):
            lineDists = np.array([egmp.DistanceToLine(P,element,element-n) for P in self.Patch])
            validDists = np.where(lineDists<.7)[0]
            
            pointDists = np.linalg.norm(self.Patch-element,axis=1)
            closestInd = np.where(pointDists[validDists]==np.min(pointDists[validDists]))[0][0]
            TempElectrodeInds.append(validDists[closestInd])

        # Store information
        self.ElectrodeIndexes = self.PatchIndexes[TempElectrodeInds]
        self.ElectrodeCoordinates = self.Patch[TempElectrodeInds,:]+self.GlobalPatchCenter

    def PlaceCSCatheter(self,anatomy,CSDistal,CSProximal,rotationAngle = None):
        # This function in general does not fit with the other electrode placement strategies
        # It has, because of that, quite a few fixes to handle that
        from scipy.spatial.distance import pdist,squareform

        # Make an adjacency matrix that accounts for the larger gaps where the CS
        # was interrupted. FIXME: this should not happen in future versions!

        Rp = anatomy[self.Patch,:]
        Distances = pdist(Rp)
        AdjacencyMatrix = squareform(Distances)/np.sqrt(2)
        LargerAdjacency = np.zeros_like(AdjacencyMatrix)

        LargerAdjacency[LargerAdjacency<=self.SpatialResolution] = -1
        LargerAdjacency[LargerAdjacency>self.SpatialResolution] = 0
        LargerAdjacency = -LargerAdjacency

        AdjacencyMatrix[AdjacencyMatrix<=1] = -1
        AdjacencyMatrix[AdjacencyMatrix>1] = 0
        AdjacencyMatrix = -AdjacencyMatrix

        # Use the larger adjacency matrix where we have few neighbors
        # FIXME: this should not happen in future versions!
        closeNeighbors = np.sum(AdjacencyMatrix==1,axis=1)
        fewCloseNeighbors = np.zeros_like(closeNeighbors)
        fewCloseNeighbors[closeNeighbors>8-1] =0 # empirical
        fewCloseNeighbors[closeNeighbors<=8-1] = 1

        for i in np.where(fewCloseNeighbors)[0]:
            AdjacencyMatrix[:,i] = LargerAdjacency[:,i]
            AdjacencyMatrix[i,:] = LargerAdjacency[i,:]

        dCenter = np.linalg.norm(Rp - anatomy[CSDistal,:],axis=1)
        Source = np.where(dCenter==np.min(dCenter))[0][0]
        # GeodesicDistances = egmp.GeodesicDistances(Source,AdjacencyMatrix)

        # Connect the shortest path based on the geodesic distances
        Isource = np.where(self.Patch==CSDistal)[0][0] # Patch here is the indexes
        Itarget = np.where(self.Patch==CSProximal)[0][0]
        path,StepDists = egmp.GetShortestPath(AdjacencyMatrix,Rp,Isource,Itarget)

        # Get (geodesic) distance between each pair
        splineSpacing = StepDists[path]

        # First one is always on the electrode array
        CSCatheter = [path[0]]
        spacing = self.ElectrodeSpacing

        lastPoint = 0
        i=0
        for i in range(self.NumberOfElectrodesPerSpline-1):
            distLast = np.abs(splineSpacing-splineSpacing[lastPoint])
            validDists = np.abs(distLast-spacing[i%2])<=0.8
            path_i = np.arange(len(path))
            candidates = np.where(validDists*path_i>lastPoint)[0]
            if len(candidates>1):
                candidates = candidates[np.where(distLast[candidates]==np.min(distLast[candidates]))[0][0]]
            else:
                candidates = candidates[0]
            CSCatheter.append(path[candidates])
            lastPoint = candidates
        
        self.ElectrodeIndexes = self.Patch[CSCatheter]
        self.ElectrodeCoordinates = anatomy[self.Patch[CSCatheter],:]

    def GetSignals(self,inputData,**kwargs):
        
        #Catheters to ignore (only used for better 3d visualization)
        IgnoredCatheters = {'HDGrid-4':[],#[16,17,18],
                            'mega':[],'mega2':[],'mega3':[],'MiniGrid':[],'CS_10':[]}
        
        ElectrodeNumbers = np.arange(self.ElectrodeIndexes.shape[0])
        DataIndexes = self.ElectrodeIndexes[~np.isin(ElectrodeNumbers,
                                                 IgnoredCatheters[self.CatheterModel])]
        # Get signal indexes from input data
        if 'rawInput' in kwargs:
            hdr = kwargs['hdr']
            self.EGMData = inputData[:,DataIndexes]*np.float(hdr['facteur']) + float(hdr['zero'])
        else:
            self.EGMData = inputData[:,DataIndexes]
           
        return self.EGMData
    
    def DetectActivations(self,method = 'globalThreshold'):
        self.Activations=egmp.DetectActivations(self.EGMData, self.SamplingFrequency,method= method)
        # return threshold
        
    def GetNumberOfChannels(self):
        return self.EGMData.shape[-1]
    
    def GetNumberOfSamples(self):
        return self.EGMData.shape[0]

    # The following functions make sense for grid-like arrangements
    def GetGridPositions(self):
        return np.unique(self.ElectrodesTemplate[:,0]),np.unique(self.ElectrodesTemplate[:,1])

    def GetGridSize(self):
        Nr = len(np.unique(self.ElectrodesTemplate[:,0]))
        Nc = len(np.unique(self.ElectrodesTemplate[:,1]))
        return Nr,Nc

    def GetPhaseSignal(self,phaseMethod='activation',activationMethod='adaptiveThreshold'):
        if self.Activations is None:
            self.DetectActivations(self,method = activationMethod)
        
        if phaseMethod=='activation':
            # TODO: add kwargs so intrinsicDeflections can be used
            self.PhaseSignal, self.SampleCL = egmp.ComputeActivationPhaseSignal(self, intrinsicDeflections = None)
        elif phaseMethod=='sinusoidal':
            self.PhaseSignal = egmp.ComputeSinusoidalPhase(self)
            self.SampleCL = None
        else:
            RuntimeError('Unknown method. The available options are: activation, sinusoidal')
            return -1




class CatheterGroup:
    '''
    This is essentially a wrapper to call the relevant functions of the Catheter class in a batch
    '''
    
    def __init__(self,catheterModel,catheterPositionDict,anatomy,anatomyShape):
        '''
        # List of class properties that will be defined along the usage of this class
        self.Catheters

        '''
        self.CatheterModel = catheterModel
        self.CatheterPositionDictionary = catheterPositionDict
        self.Anatomy = anatomy
        self.AnatomyShape = anatomyShape
        
    def GetCatheterData(self,anatomy,inputData,**kwargs):
        # Make dict to hold the signals
        self.Catheters = self.CatheterPositionDictionary.copy()
        
        for key in self.CatheterPositionDictionary:
            # Create catheter
            self.Catheters[key] = CatheterClass(self.CatheterModel)
            # Position catheter
            self.Catheters[key].GetTissuePatch(self.CatheterPositionDictionary[key][0],self.Anatomy)    
            self.Catheters[key].ProjectionFunction(self.Anatomy,
                                                rotationAngle = self.CatheterPositionDictionary[key][1])
            # Get Signal
            if 'rawInput' in kwargs:
                self.Catheters[key].GetSignals(inputData,
                                                rawInput=kwargs['rawInput'],
                                                hdr=kwargs['hdr'])
            else:
                self.Catheters[key].GetSignals(inputData)
        if 'getCSData' in kwargs:
            print('Getting CS Data')
            self.CatheterPositionDictionary['CS'] = None
            if kwargs['getCSData']:
                self.Catheters['CS'] = CatheterClass('CS_10') # Fixed catheter for now
                self.Catheters['CS'].GetTissuePatch(kwargs['CSDistal'],self.Anatomy)    
                self.Catheters['CS'].ProjectionFunction(self.Anatomy,kwargs['CSDistal'],kwargs['CSProximal'])

                if 'rawInput' in kwargs:
                    self.Catheters['CS'].GetSignals(inputData,
                                                    rawInput=kwargs['rawInput'],
                                                    hdr=kwargs['hdr'])
                else:
                    self.Catheters['CS'].GetSignals(inputData)
    
    def GetCatheterPositions(self,**kwargs):
        # Make dict to hold the signals
        self.Catheters = self.CatheterPositionDictionary.copy()
        
        for key in self.CatheterPositionDictionary:
            # Create catheter
            self.Catheters[key] = CatheterClass(self.CatheterModel)
            # Position catheter
            self.Catheters[key].GetTissuePatch(self.CatheterPositionDictionary[key][0],self.Anatomy)    
            self.Catheters[key].PlaceCatheter(self.Anatomy,
                                                rotationAngle = self.CatheterPositionDictionary[key][1])  

    def GetActivations(self,method='globalThreshold'):
        for key in self.Catheters:
            self.Catheters[key].DetectActivations(method = method)

    def SaveMat(self,outFile):
        if hasattr(self,"Catheters"):
            #Get rid of Nones, which cannot be converted to matlab
            cats = self.Catheters
            for C in cats.keys():
                for key in cats[C].__dict__:
                    if cats[C].__dict__[key] is None:
                        cats[C].__dict__[key]  = []
            sio.savemat(outFile+'.mat',cats)
        else:
            RuntimeError('Catheter data was not obtained yet.')


#%% Not used now, but possibly useful at some point

# def PlaceCatheterV2(self,anatomy, rotationAngle = 0,offset = 10,tolerance=1, **kwargs):
#     '''
#     In contrast to the 1st version of the electrode placement, I add here some additional constraints
#     for electrodes that are distributed in straight splines.

#     The projection here is done by spline instead of by the whole template. This way, the distances between
#     splines are more or less preserved. Additionally, I use two constraints to make sure the splines are 
#     similar to a perfect straight line, depending on the inter-electrode spacing and its distance to a straight
#     line between the last visited point and the end of the spline.

#     '''
    
#     # Get template and rotate
#     CatheterTemplate = self.ElectrodesTemplate
#     R = egmp.MakeRotationalMatrix(rotationAngle)
#     CatheterTemplate = np.matmul(CatheterTemplate,R) 

#     U,s,V = svd(self.Patch)
#     u = V.T[:,0] 
#     v = V.T[:,1]
#     n = V.T[:,2]   

#     self.PatchNormal = n

#     # Create template for all splines
#     CtA = np.array([electrode[0]*u+electrode[1]*v-offset*n for electrode in CatheterTemplate])
#     CtB = np.array([electrode[0]*u+electrode[1]*v+offset*n for electrode in CatheterTemplate])

#     # Make sure we are in the right side of the atrium
#     distA = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtA,axis = 0))
#     distB = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtB,axis = 0))
    
#     if distB<distA and (distA>=15 or distB>=15):
#         n = -n
#         Ct = CtB
#     else:
#         Ct = CtA

#     # Iterate over splines
#     NSplines = int(len(CatheterTemplate)//self.NumberOfElectrodesPerSpline)
#     TempElectrodeInds = list()
#     for SplineNumber in range(NSplines):
#         splineCt = Ct[SplineNumber*self.NumberOfElectrodesPerSpline:(SplineNumber+1)*self.NumberOfElectrodesPerSpline,:]

#         # 1) Connecting template to patch. Make more lines and get everything that is closer than 1mm from them
#         nLines = 30
#         Lines = {}
#         for i,electrode in enumerate(splineCt):
#             line = []
#             interval = 1.5*offset/nLines
#             for l in range(1,nLines+1):
#                 line.append(electrode+l*interval*n)
#             Lines[i] = np.asarray(line)

#         # Make clusters of candidate points for each electrode
#         connectedDots = list()
#         for key,line in Lines.items():
#             dists = np.asarray([np.linalg.norm(self.Patch[i,:]-line,axis = 1) \
#                     for i in range(self.Patch.shape[0])])
#             ind = np.where(dists<=1)[0]
#             connectedDots.append(np.unique(ind))
#         # 2) For each spline, get first and last points to serve as basis to the spline
#         for i in [0,-1]:
#             candidatePoints = self.Patch[connectedDots[i],:]
#             chosenInd = np.linalg.norm(candidatePoints-splineCt[i,:],axis=1)
#             try:
#                 chosenInd = np.where(chosenInd==np.min(chosenInd))[0][0]
#                 connectedDots[i] = connectedDots[i][chosenInd]
#             except ValueError:
#                 print('Region does not have full contact with tissue. Please reposition the array.')
#                 return -1
#         # 3) Now, iteratively, select dots based on two criteria:
#             # a) The dots are spaced approx. the electrode spacing, given the error of the mesh resolution (tolerance=0.5)
#             # b) From the dots that match (a), get the one closer to the line between the previous point and the other end of the spline

#         tipA = self.Patch[connectedDots[0],:]
#         tipB = self.Patch[connectedDots[-1],:]
#         for i in range(1,len(connectedDots)-1):
#             distFromTip = np.abs(np.linalg.norm(self.Patch[connectedDots[i],:]-tipA,axis=1)-self.ElectrodeSpacing[i])
#             validDistances = (distFromTip <= tolerance*self.ElectrodeSpacing[i])

#             distFromLine = np.array([egmp.DistanceToLine(self.Patch[e,:], tipA, tipB) for e in connectedDots[i]])
#             try:
#                 ind = np.where((distFromLine==np.min(distFromLine[validDistances])))[0][0]
#                 connectedDots[i] = connectedDots[i][ind]
#             except ValueError:
#                 print('Warning! The distance constraint has been ignored since no valid points were found')
#                 ind = np.where((distFromLine==np.min(distFromLine)))[0]
#                 if len(ind)==0: 
#                     print('Region does not have full contact with tissue. Please reposition the array.')
#                     return -1
#                 connectedDots[i] = connectedDots[i][ind[0]]
#             # Next starting point is the point that has just been selected 
#             tipA = self.Patch[connectedDots[i],:]

#         for element in connectedDots:
#             TempElectrodeInds.append(element)

#     # Store information
#     self.ElectrodeIndexes = self.PatchIndexes[TempElectrodeInds]
#     self.ElectrodeCoordinates = self.Patch[TempElectrodeInds,:]+self.GlobalPatchCenter

# def PlaceCatheterV3(self,anatomy, rotationAngle = 0,offset = 1,AllowedDistanceError = 0.25, AllowedAngularError = np.pi/6,**kwargs):
#     '''
#     Yet another attempt.

#     '''
#     DistX = self.SplineSpacing # mm, spline distance

#     # Get template and rotate
#     CatheterTemplate = self.ElectrodesTemplate
#     # R = egmp.MakeRotationalMatrix(rotationAngle)
#     # CatheterTemplate = np.matmul(CatheterTemplate,R)

#     U,s,V = svd(self.Patch)
#     u = V.T[:,0] 
#     v = V.T[:,1]
#     n = V.T[:,2]   
#     self.PatchNormal = n

#     def rotation_matrix(axis, theta):
#         import math
#         """
#         Return the rotation matrix associated with counterclockwise rotation about
#         the given axis by theta radians.
#         """
#         axis = np.asarray(axis)
#         axis = axis/math.sqrt(np.dot(axis, axis))
#         a = math.cos(theta/2.0)
#         b, c, d = -axis*math.sin(theta/2.0)
#         aa, bb, cc, dd = a*a, b*b, c*c, d*d
#         bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
#         return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
#                         [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
#                         [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])


#     R = rotation_matrix(n,rotationAngle)

#     RotatedPatch = np.matmul(self.Patch,R) #TODO Not working as it should


#     # Create template for all splines
#     CtA = np.array([electrode[0]*u+electrode[1]*v-offset*n for electrode in CatheterTemplate])
#     CtB = np.array([electrode[0]*u+electrode[1]*v+offset*n for electrode in CatheterTemplate])

#     # Make sure we are in the right side of the atrium
#     distA = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtA,axis = 0))
#     distB = np.linalg.norm(np.mean(anatomy-self.GlobalPatchCenter,axis = 0)-np.mean(CtB,axis = 0))
    
#     if distB<distA and (distA>=15 or distB>=15):
#         n = -n
#         Ct = CtB
#     else:
#         Ct = CtA

#     # R = egmp.MakeRotationalMatrix(Ct[1,:]-Ct[0,:],u)

#     FilledGrid = np.zeros(self.GetGridSize(),dtype=int)

#     for i in range(FilledGrid.shape[0]-1):
#         for j in range(FilledGrid.shape[1]):
#             try:
#                 DistY = self.ElectrodeSpacing[j]
#             except:
#                 DistY = np.nan

#             if i==0 and j==0:
#                 # First index
#                 startingPoint = Ct[0,:]
#                 dists = np.linalg.norm(RotatedPatch-startingPoint,axis=1)
#                 startingInd = np.where(dists==np.min(dists))[0][0]
#                 FilledGrid[i,j] = startingInd
#             else:
#                 startingInd = FilledGrid[i,j]
#             #    
#             startingPoint = RotatedPatch[startingInd,:]
#             dists = np.linalg.norm(RotatedPatch-startingPoint,axis=1)
            
#             if j<FilledGrid.shape[1]-1:
#                 extraDist = 0
#                 while True:
#                     ## Find position of next electrode in spline
#                     ringInds = np.where((dists>=DistY-(AllowedDistanceError+extraDist))*\
#                                         (dists<DistY+(AllowedDistanceError+extraDist)))[0]

#                     # Find angle between ideal position and ring
#                     B = startingPoint+DistY*u
#                     A = startingPoint

#                     angle = np.arccos([np.dot(B-A,P-A)/(np.linalg.norm(B-A)*np.linalg.norm(P-A)) for P in RotatedPatch[ringInds,:]])
#                     if np.min(angle) > AllowedAngularError: 
#                         extraDist += 0.05
#                         if extraDist>0.5:
#                             FilledGrid[i,j+1] = ringInds[np.where(angle==np.min(angle))[0][0]]
#                             break
#                     else:
#                         FilledGrid[i,j+1] = ringInds[np.where(angle==np.min(angle))[0][0]]
#                         break
            
#             extraDist = 0
#             while True:
#                 ## Find position of  electrode in next spline
#                 ringInds = np.where((dists>=DistX-(AllowedDistanceError+extraDist))*\
#                                     (dists<DistX+(AllowedDistanceError+extraDist)))[0]

#                 # Find angle between ideal position and ring
#                 B = startingPoint+DistX*v
#                 A = startingPoint

#                 angle = np.arccos([np.dot(B-A,P-A)/(np.linalg.norm(B-A)*np.linalg.norm(P-A)) for P in RotatedPatch[ringInds,:]])
#                 if np.min(angle) > AllowedAngularError: 
#                     extraDist += 0.05
#                     if extraDist>0.5:
#                         FilledGrid[i+1,j] = ringInds[np.where(angle==np.min(angle))[0][0]]
#                         break
#                 else:
#                     FilledGrid[i+1,j] = ringInds[np.where(angle==np.min(angle))[0][0]]
#                     break

#     # Store information
#     self.ElectrodeIndexes = self.PatchIndexes[FilledGrid.flatten()]
#     self.ElectrodeCoordinates = self.Patch[FilledGrid.flatten(),:]+self.GlobalPatchCenter
        