# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 14:00:21 2021

DESCRIPTION: Contains functions to interact with the user in different contexts

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import sys
import argparse
import numpy as np
try:
    import vtk
    from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray
    from StandardVTKObjects import HeatmapColormap
    vtkFlag = True
except:
    print('VTK not installed')
    vtkFlag = False



# Class for displaying help instead of only error message when no arguments are provided
class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

# Progress bar for loops
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()

if vtkFlag:

    class ItemSelectionStyle(vtk.vtkInteractorStyleTrackballCamera):

        def __init__(self, data):
            self.AddObserver('LeftButtonPressEvent', self.left_button_press_event)
            self.data = data
            self.selected = None
            self.selected_mapper = vtk.vtkDataSetMapper()
            self.selected_actor = vtk.vtkActor()

        def left_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            pos = self.GetInteractor().GetEventPosition()

            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)

            # Pick from this location.
            picker.Pick(pos[0], pos[1], 0, self.GetDefaultRenderer())
            self.HighlightPick(picker)
            # Forward events
            self.OnLeftButtonDown()
        
        def HighlightPick(self,picker):
            colors = vtk.vtkNamedColors()


            if picker.GetCellId() != -1:
                ids = vtk.vtkIdTypeArray()
                ids.SetNumberOfComponents(1)
                ids.InsertNextValue(picker.GetCellId())

                selection_node = vtk.vtkSelectionNode()
                selection_node.SetFieldType(vtk.vtkSelectionNode.CELL) 
                selection_node.SetContentType(vtk.vtkSelectionNode.INDICES)
                selection_node.SetSelectionList(ids)

                selection = vtk.vtkSelection()
                selection.AddNode(selection_node)

                extract_selection = vtk.vtkExtractSelection()
                extract_selection.SetInputData(0, self.data)
                extract_selection.SetInputData(1, selection)
                extract_selection.Update()

                # In selection
                self.selected = vtk.vtkUnstructuredGrid()
                self.selected.ShallowCopy(extract_selection.GetOutput())

                self.selected_mapper.SetInputData(self.selected)
                self.selected_actor.SetMapper(self.selected_mapper)
                self.selected_actor.GetProperty().EdgeVisibilityOn()
                self.selected_actor.GetProperty().SetColor(colors.GetColor3d('Tomato'))
                self.selected_actor.GetProperty().SetPointSize(15)
                self.selected_actor.GetProperty().SetLineWidth(15)

                self.GetInteractor().GetRenderWindow().GetRenderers().GetFirstRenderer().AddActor(self.selected_actor)

            # Forward events
            self.OnLeftButtonDown()

    class ElectrodePlacementStyle(vtk.vtkInteractorStyleTrackballCamera):

        def __init__(self, data):
            self.AddObserver('LeftButtonPressEvent', self.left_button_press_event)
            self.AddObserver('RightButtonPressEvent', self.right_button_press_event)
            self.data = data
            self.selected = None
            self.selected_mapper = vtk.vtkDataSetMapper()
            self.selected_actor = vtk.vtkActor()

            self.colors = vtk.vtkNamedColors()


            # Related to electrode placement
            self.Rotation=0


        def left_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            pos = self.GetInteractor().GetEventPosition()

            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)

            # Pick from this location.
            picker.Pick(pos[0], pos[1], 0, self.GetDefaultRenderer())
            self.picker = picker
            self.Rotation = 0
            self.PlaceElectrodes('left')
            if picker.GetCellId()!=-1:
                print('Electrode array center index: %d'%picker.GetCellId())

            # Forward events
            self.OnLeftButtonDown()
        
        def right_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            self.Rotation+=20/180*np.pi
            self.Rotation = self.Rotation % (2*np.pi)
            self.Catheter.Rotation = self.Rotation
            print('Electrode rotation: %0.2f rad'%self.Rotation)


            # print('Rotation: %0.3f rad'%self.Rotation)
            self.PlaceElectrodes('right')
            self.OnRightButtonDown()

        # Define function for handling the point picking
        def PlaceElectrodes(self,button):
            picker = self.picker
            if picker.GetCellId() != -1:
                Catheter = self.Catheter

                Index = picker.GetCellId()
                Catheter.GetTissuePatch(Index,self.Anatomy)
                Catheter.ProjectionFunction(self.Anatomy,rotationAngle =self.Rotation)

                # Make Electrode array
                # TODO: A4 in different color
                # TODO: splines
                # All electrodes
                ids = vtk.vtkIdTypeArray()
                ids.SetNumberOfComponents(1)

                for idx in Catheter.ElectrodeIndexes: ids.InsertNextValue(idx) # Extra points are for splines

                selection_node = vtk.vtkSelectionNode()
                selection_node.SetFieldType(vtk.vtkSelectionNode.CELL) 
                selection_node.SetContentType(vtk.vtkSelectionNode.INDICES)
                selection_node.SetSelectionList(ids)

                selection = vtk.vtkSelection()
                selection.AddNode(selection_node)

                extract_selection = vtk.vtkExtractSelection()
                extract_selection.SetInputData(0, self.data)
                extract_selection.SetInputData(1, selection)
                extract_selection.Update()

                # In selection
                self.selected = vtk.vtkUnstructuredGrid()
                self.selected.ShallowCopy(extract_selection.GetOutput())
    
                self.selected_mapper.SetInputData(self.selected)
                self.selected_actor.SetMapper(self.selected_mapper)
                self.selected_actor.GetProperty().EdgeVisibilityOn()
                self.selected_actor.GetProperty().SetColor(self.colors.GetColor3d('Gold'))
                self.selected_actor.GetProperty().SetPointSize(15)
                self.selected_actor.GetProperty().SetLineWidth(15)
                
                self.GetInteractor().GetRenderWindow().GetRenderers().GetFirstRenderer().AddActor(self.selected_actor)


                if button=='left':
                    self.OnLeftButtonDown()
                elif button=='right':
                    self.OnRightButtonDown()

    class FancyElectrodePlacementStyle(vtk.vtkInteractorStyleTrackballCamera):

        def __init__(self, data,verbose=False):
            self.AddObserver('LeftButtonPressEvent', self.left_button_press_event)
            self.AddObserver('RightButtonPressEvent', self.right_button_press_event)
            self.data = data
            self.selected = None
            self.selected_mapper = vtk.vtkPolyDataMapper()
            self.selected_actor = vtk.vtkActor()
            self.verbose=verbose

            self.colors = vtk.vtkNamedColors()


            # Related to electrode placement
            self.Rotation=0


        def left_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            pos = self.GetInteractor().GetEventPosition()

            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)

            # Pick from this location.
            picker.Pick(pos[0], pos[1], 0, self.GetDefaultRenderer())
            self.picker = picker
            self.Rotation = 0
            self.PlaceElectrodes('left')

            # Forward events
            self.OnLeftButtonDown()
        
        def right_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            self.Rotation+=20/180*np.pi
            self.Rotation = self.Rotation % (2*np.pi)
            self.Catheter.Rotation = self.Rotation
            if self.verbose: print('Electrode rotation: %0.2f rad'%self.Rotation)


            # print('Rotation: %0.3f rad'%self.Rotation)
            self.PlaceElectrodes('right')
            self.OnRightButtonDown()

        # Define function for handling the point picking
        def PlaceElectrodes(self,button):
            picker = self.picker
            if picker.GetPointId() != -1:
                Catheter = self.Catheter

                Index = picker.GetPointId()
                IndexPosition = np.array(self.data.GetPoint(Index))
                IndexPosition = np.round(IndexPosition).astype(int)

                Index = np.where((self.Anatomy[:,0]>=IndexPosition[0]-1)*(self.Anatomy[:,0]<=IndexPosition[0]+1)*\
                                (self.Anatomy[:,1]>=IndexPosition[1]-1)*(self.Anatomy[:,1]<=IndexPosition[1]+1)*\
                                (self.Anatomy[:,2]>=IndexPosition[2]-1)*(self.Anatomy[:,2]<=IndexPosition[2]+1))[0][0]
                Index = int(Index)
                if self.verbose: print('Electrode array center index: %d'%Index)


                Catheter.GetTissuePatch(Index,self.Anatomy)
                Catheter.ProjectionFunction(self.Anatomy,rotationAngle =self.Rotation)

                # Make Electrode array
                # TODO: splines
                # All electrodes
                # Make Actor for electrodes
                    # Data
                nCoords = Catheter.ElectrodeCoordinates.shape[0]
                #
                verts = vtk.vtkPoints()
                cells = vtk.vtkCellArray()
                #
                self.selected = vtk.vtkPolyData()
                #
                verts.SetData(numpy_to_vtk(Catheter.ElectrodeCoordinates))
                cells_npy = np.vstack([np.ones(nCoords, dtype = np.int64),
                                    np.arange(nCoords, dtype = np.int64)]).T.flatten()
                cells.SetCells(nCoords, numpy_to_vtkIdTypeArray(cells_npy))
                self.selected.SetPoints(verts)
                self.selected.SetVerts(cells)

                # Mark A4 and A1
                magnitude = vtk.vtkDoubleArray()
                magnitude.SetNumberOfValues(nCoords)
                magnitude.SetName("magnitude")
                for i in range(nCoords):
                    if i==0:
                        magnitude.SetValue(i, 1.)
                    if i==3:
                        magnitude.SetValue(i, 0.5)
                    else:
                        magnitude.SetValue(i, 0.)

                self.selected.GetPointData().SetActiveScalars("magnitude")

                ## Make actual mapper and actor
                lut = HeatmapColormap(nCoords)
                lut.Build()

                sphereSource = vtk.vtkSphereSource()
                sphereSource.SetRadius(1)
                # cubeSource.SetXLength(1.5)
                # cubeSource.SetYLength(1.5)
                # cubeSource.SetZLength(1.5)
                # arrowSource.SetShaftRadius(0.07)
                # arrowSource.SetTipLength(0.5)
                
                glyph = vtk.vtkGlyph3D()
                glyph.SetInputData(self.selected)
                glyph.SetSourceConnection(sphereSource.GetOutputPort())
                glyph.SetScaleFactor(1)
                glyph.SetVectorModeToUseVector()
                glyph.SetColorModeToColorByScalar()
                glyph.SetScaleModeToDataScalingOff ()
                glyph.Update()

                self.selected_mapper.SetInputConnection(glyph.GetOutputPort())
                self.selected_mapper.ScalarVisibilityOn() 
                self.selected_mapper.SetLookupTable(lut)
                self.selected_mapper.SetScalarRange([0,1])
                self.selected_actor.SetMapper(self.selected_mapper)
                self.selected_actor.GetProperty().EdgeVisibilityOff()
                self.selected_actor.GetProperty().SetColor(self.colors.GetColor3d('Gold'))
                # self.selected_actor.GetProperty().SetPointSize(15)
                # self.selected_actor.GetProperty().SetLineWidth(15)
                
                self.GetInteractor().GetRenderWindow().GetRenderers().GetFirstRenderer().AddActor(self.selected_actor)            

                if button=='left':
                    self.OnLeftButtonDown()
                elif button=='right':
                    self.OnRightButtonDown()

    class PointSelectionStyle(vtk.vtkInteractorStyleTrackballCamera):

        def __init__(self, data,verbose=False):
            self.AddObserver('LeftButtonPressEvent', self.left_button_press_event)
            self.data = data
            self.selected = None
            self.selected_mapper = vtk.vtkPolyDataMapper()
            self.selected_actor = vtk.vtkActor()
            self.verbose=verbose

            self.colors = vtk.vtkNamedColors()



        def left_button_press_event(self, obj, event):
            # Get the location of the click (in window coordinates)
            pos = self.GetInteractor().GetEventPosition()

            picker = vtk.vtkCellPicker()
            picker.SetTolerance(0.005)

            # Pick from this location.
            picker.Pick(pos[0], pos[1], 0, self.GetDefaultRenderer())
            self.picker = picker

            if picker.GetPointId() != -1:
                Catheter = self.Catheter

                Index = picker.GetPointId()
                IndexPosition = np.array(self.data.GetPoint(Index))
                IndexPosition = np.round(IndexPosition).astype(int)

                Index = np.where((self.Anatomy[:,0]>=IndexPosition[0]-1)*(self.Anatomy[:,0]<=IndexPosition[0]+1)*\
                                (self.Anatomy[:,1]>=IndexPosition[1]-1)*(self.Anatomy[:,1]<=IndexPosition[1]+1)*\
                                (self.Anatomy[:,2]>=IndexPosition[2]-1)*(self.Anatomy[:,2]<=IndexPosition[2]+1))[0][0]
                Index = int(Index)
                print('Electrode array center index: %d'%Index)


                #
                verts = vtk.vtkPoints()
                cells = vtk.vtkCellArray()
                #
                self.selected = vtk.vtkPolyData()
                #
                verts.SetData(numpy_to_vtk(self.Anatomy[Index].reshape(1,3)))
                cells_npy = np.vstack([np.int64(1),np.int64(1)]).T.flatten()
                cells.SetCells(1, numpy_to_vtkIdTypeArray(cells_npy))
                self.selected.SetPoints(verts)
                self.selected.SetVerts(cells)

    
                sphereSource = vtk.vtkSphereSource()
                sphereSource.SetRadius(2)
                # cubeSource.SetXLength(1.5)
                # cubeSource.SetYLength(1.5)
                # cubeSource.SetZLength(1.5)
                # arrowSource.SetShaftRadius(0.07)
                # arrowSource.SetTipLength(0.5)
                
                glyph = vtk.vtkGlyph3D()
                glyph.SetInputData(self.selected)
                glyph.SetSourceConnection(sphereSource.GetOutputPort())
                glyph.SetScaleFactor(1)
                glyph.SetVectorModeToUseVector()
                glyph.SetColorModeToColorByScalar()
                glyph.SetScaleModeToDataScalingOff ()
                glyph.Update()

                self.selected_mapper.SetInputConnection(glyph.GetOutputPort())
                self.selected_actor.SetMapper(self.selected_mapper)
                self.selected_actor.GetProperty().EdgeVisibilityOff()
                # self.selected_actor.GetProperty().SetPointSize(15)
                # self.selected_actor.GetProperty().SetLineWidth(15)
                self.selected_actor.GetProperty().SetColor(self.colors.GetColor3d('Gold'))

                self.GetInteractor().GetRenderWindow().GetRenderers().GetFirstRenderer().AddActor(self.selected_actor)            

                self.OnLeftButtonDown()