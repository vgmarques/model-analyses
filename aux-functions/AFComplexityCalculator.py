
import scipy.signal as sig
from detect_peaks import detect_peaks
import numpy as np

class AFComplexityCalculator:
    def __init__(self):
        self.DFParameters = {'NumberOfFFTPoints': 2**10,
                            'WindowLength': 2**10,
                            'FFTOverlap': 0.5,
                            'DFRange': [3, 12],
                            'NumberOfPeaks': 5,
                            'PeakRange': 1,
                            'FundamentalFrequency': False,
                            'FundamentalFrequencyThreshold': 0.75}
        self.FWaveDistanceThreshold = 100
        
    #### Dominant Frequency
    def ComputeDominantFrequency(self,CatheterObj, validChannelIndices = None):
        if validChannelIndices is None:
            validChannelIndices = np.arange(CatheterObj.GetNumberOfChannels())

        spectra, _, dominantFrequencyIndex,_ = self.ComputePWelchECGSpectrum(CatheterObj.EGMData[:,validChannelIndices], CatheterObj.SamplingFrequency)

        return dominantFrequencyIndex, spectra
    #

    def ComputePWelchECGSpectrum(self,data, samplingFrequency):
        numberOfSamples,numberOfChannels = data.shape

        psdEstimate = list()

        dominantFrequency = np.ones((numberOfChannels, 1))*np.nan
        dominantFrequencyIndex = np.ones((numberOfChannels, 1))*np.nan
        dominantFrequencyPower = np.ones((numberOfChannels, 1))*np.nan

        windowLength = self.DFParameters['WindowLength'] 
        numberOfFFTPoints = self.DFParameters['NumberOfFFTPoints'] 
        dfRange = self.DFParameters['DFRange'] 
        fundamentalFrequency = self.DFParameters['FundamentalFrequency'] 

        windowOverlap = int(np.round(windowLength / 2))

        if  numberOfSamples < windowLength:
            data = np.vstack([data,np.zeros((windowLength - numberOfSamples, numberOfChannels))])

        for channelIndex in range(numberOfChannels):
            channelData = data[:,channelIndex]
            # spectrum computation. PWELCH with specified window length and overlap
            frequencies,frequencyPower = sig.welch(channelData,
                                                    fs= samplingFrequency,
                                                    nperseg=windowLength,
                                                    noverlap=windowOverlap,
                                                    nfft=numberOfFFTPoints)

            psdEstimate.append({'Frequencies':frequencies,'Data':frequencyPower})


            # dominant frequency identification
            minimalAFFrequency = dfRange[0]
            maximalAFFrequency = dfRange[1]
            validDFFrequencies = (frequencies >= minimalAFFrequency) & (frequencies < maximalAFFrequency)

            peakIndices = detect_peaks(frequencyPower)
            peakValues = frequencyPower[peakIndices]
            peakDFValues = peakValues[validDFFrequencies[peakIndices]]
            peakDFIndices = peakIndices[validDFFrequencies[peakIndices]]

            if len(peakDFIndices)==0:
                dominantFrequency[channelIndex] = np.nan
                dominantFrequencyPower[channelIndex] = np.nan
            else:
                maxPosition = np.where(peakDFValues==np.max(peakDFValues))[0][0]
                if fundamentalFrequency:
                    # search for fundamental frequency in allowed frequencies
                    frequencyResolution = np.mean(np.diff(frequencies))
                    maximumPeakFrequency = frequencies[peakDFIndices[maxPosition]]
                    peakDFFrequencies = frequencies[peakDFIndices]
                    harmonicIndex = 2
                    newHarmonicFrequency = maximumPeakFrequency / harmonicIndex
                    while newHarmonicFrequency >= minimalAFFrequency:
                        newPeakFrequencyDifference = np.min(np.abs(peakDFFrequencies - newHarmonicFrequency))
                        newPeakFrequencyIndex = np.where(np.abs(peakDFFrequencies - newHarmonicFrequency)==newPeakFrequencyDifference)[0][0]
                        if newPeakFrequencyDifference < frequencyResolution:
                            newPeakFrequencyPower = peakDFValues[newPeakFrequencyIndex]
                            fundamentalFrequencyThreshold = self.DFParameters['FundamentalFrequencyThreshold']
                            if newPeakFrequencyPower >= frequencyPower[peakDFIndices[maxPosition]] * fundamentalFrequencyThreshold:
                                maxPosition = newPeakFrequencyIndex

                        harmonicIndex = harmonicIndex + 1
                        newHarmonicFrequency = maximumPeakFrequency / harmonicIndex


                dominantFrequencyIndex[channelIndex] = peakDFIndices[maxPosition]
                dominantFrequency[channelIndex] = frequencies[peakDFIndices[maxPosition]]
                dominantFrequencyPower[channelIndex] = frequencyPower[peakDFIndices[maxPosition]]
                
        return psdEstimate, dominantFrequency, dominantFrequencyIndex.astype(int), dominantFrequencyPower

    def ComputeFWaveAmplitudes(self,data,samplingFrequency=1000):
        nyquistFrequency = samplingFrequency / 2
        b, a = sig.cheby2(3, 20,3 / nyquistFrequency, 'high')

        peakDistanceThreshold = np.floor(self.FWaveDistanceThreshold*samplingFrequency/1000) # self
                            
        numberOfChannels = data.shape[1]
        fWaveIndices = []#np.zeros(numberOfChannels)
        fWaveAmplitudes = []#np.zeros(numberOfChannels)
        fWavePeakValleyIndices= []# = np.zeros(numberOfChannels)

        for channelIndex in range(numberOfChannels):
            signal = data[:,channelIndex]
            signal = sig.filtfilt(b, a, signal)
            peakIndices = detect_peaks(signal,mpd=peakDistanceThreshold)
            peakAmplitudes = signal[peakIndices]
            valleyIndices = detect_peaks(signal,valley=True,mpd=peakDistanceThreshold)
            valleyAmplitudes = signal[valleyIndices]

            # valleyAmplitudes = -valleyAmplitudes #TODO needed?
            
            # filter for zero- crossings
            peakIndices = peakIndices[peakAmplitudes > 0]
            valleyIndices = valleyIndices[valleyAmplitudes < 0]
            
            # take the highest peak in concurrent peaks
            currentPeakIndex = 0
            validPeaks = np.zeros(len(peakIndices),dtype=bool)
            while currentPeakIndex < len(peakIndices):
                nextValleyIndex = np.where(valleyIndices > peakIndices[currentPeakIndex])[0] #second 0 may be wrong
                
                if len(nextValleyIndex)==0:
                    lastPeakBeforeValleyIndex = len(peakIndices)-1
                else:
                    nextValleyIndex = nextValleyIndex[0]
                    lastPeakBeforeValleyIndex = np.where(peakIndices < valleyIndices[nextValleyIndex])[0][-1]

                
                concurrentPeakIndices = peakIndices[currentPeakIndex:lastPeakBeforeValleyIndex]
                
                if len(concurrentPeakIndices)!=0:
                    maxPeakAmplitude = np.max(signal[concurrentPeakIndices])#
                    maxPeakIndex = np.where(signal[concurrentPeakIndices]==maxPeakAmplitude)[0][0]
                else:
                    maxPeakIndex=0

                validPeaks[currentPeakIndex + maxPeakIndex] = True #TODO -1?
                
                currentPeakIndex = lastPeakBeforeValleyIndex +1

            peakIndices = peakIndices[validPeaks]
                
            # % take the deepest valley in concurrent valleys
            currentValleyIndex = 0
            validValleys = np.zeros(len(valleyIndices),bool)
            while currentValleyIndex < len(valleyIndices):
                nextPeakIndex = np.where(peakIndices > valleyIndices[currentValleyIndex])[0] #second 0 may be wrong
                
                if len(nextPeakIndex)==0:
                    lastValleyBeforePeakIndex = len(valleyIndices)-1
                else:
                    nextPeakIndex = nextPeakIndex[0]
                    lastValleyBeforePeakIndex = np.where(valleyIndices < peakIndices[nextPeakIndex])[0][-1]

                concurrentValleyIndices = valleyIndices[currentValleyIndex:lastValleyBeforePeakIndex]

                if len(concurrentValleyIndices)!=0:
                    minValleyAmplitude = np.min(signal[concurrentValleyIndices])#
                    minValleyIndex = np.where(signal[concurrentValleyIndices]==minValleyAmplitude)[0][0]
                else:
                    minValleyIndex=0

                validValleys[currentValleyIndex + minValleyIndex] = True #TODO -1?
                
                currentValleyIndex = lastValleyBeforePeakIndex + 1

            valleyIndices = valleyIndices[validValleys]
            
            ##
            peakAmplitudes = np.zeros(len(peakIndices))*np.nan
            peakValleyIndices = np.zeros(len(peakIndices))*np.nan
            for currentPeakIndex in range(len(peakIndices)):
                nextValleyIndex = np.where(valleyIndices > peakIndices[currentPeakIndex])[0] #second 0 may be wrong

                if len(nextValleyIndex)==0: 
                    break
                else:
                    nextValleyIndex = nextValleyIndex[0]
                
                peakAmplitudes[currentPeakIndex] = signal[peakIndices[currentPeakIndex]] - signal[valleyIndices[nextValleyIndex]]
                peakValleyIndices[currentPeakIndex] = valleyIndices[nextValleyIndex]

            
            validAmplitudes = ~np.isnan(peakAmplitudes)
            fWaveAmplitudes.append(peakAmplitudes[validAmplitudes])
            fWaveIndices.append(peakIndices[validAmplitudes].astype(int))
            fWavePeakValleyIndices.append(peakValleyIndices[validAmplitudes].astype(int))

        return fWaveAmplitudes, fWaveIndices, fWavePeakValleyIndices 