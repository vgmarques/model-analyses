#from pyccmc import igb_read
import os,sys
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb

from itertools import product
import numpy as np

class IGBUnstructuredGrid:
    "Simple wrapper around IGB file to convert it to unstructured grid"
    
    def __init__(self,fname_cell,valid_cells=None,units=1.0):
        ""
    
        self.fname = fname_cell
        self.valid_cells = valid_cells
        self.units = units

    def get_cell(self):
        
        if not hasattr(self,"cell"):
            # read cell file
            self.cell,_ = igb.Load(self.fname)

        return self.cell

    def get_dims(self):
        cell = self.get_cell()
        return cell.shape
    
    def get_cubes(self):
        
        if not hasattr(self,"cubes"):
            cell = self.get_cell()
            valid = np.isin(cell,self.valid_cells)
            self.cubes = np.where(valid)
            self.ncub  = len(self.cubes[0])
        
        return self.cubes

    def get_nodes(self):
        
        if not hasattr(self,"nodes"):
            cell = self.get_cell()
            nz,ny,nx = cell.shape
        
            # cube coordinates
            cz,cy,cx = self.get_cubes()

            # vertex map
            ppt = np.zeros(shape=(nz+1,ny+1,nx+1),dtype=np.bool)
            for i,j,k in product(*[[0,1]]*3):
                ppt[cz+k,cy+j,cx+i] = True

            self.nodes = np.where(ppt)
            
        return self.nodes
 
    def get_ver2nod(self):
        
        tz,ty,tx = self.get_nodes()
        nz,ny,nx = self.get_cell().shape
        
        ver2nod = np.zeros(shape=(nz+1,ny+1,nx+1),dtype=np.int)
        ver2nod[tz,ty,tx] = np.arange(tz.shape[0])
        
        return ver2nod

    def get_points(self):
        "Return the list of points"

        # nodes coordinates
        tz,ty,tx = self.get_nodes()
        pts = self.units*(np.c_[tx,ty,tz].astype(np.float))
        
        return pts
        
    def get_topology(self,ordering=None):
        "Compute topology"
        
        if ordering is None:
            # VTK ordering
            ordering= [[0,0,0],[1,0,0],[1,1,0],[0,1,0],
                       [0,0,1],[1,0,1],[1,1,1],[0,1,1]]
            
        cz,cy,cx = self.get_cubes()
        ver2nod = self.get_ver2nod()
        hexa = np.empty(shape=(self.ncub,8),dtype=np.int)
        for col,(i,j,k) in enumerate(ordering):
            hexa[:,col] = ver2nod[cz+k,cy+j,cx+i]
        
        return hexa
        
    def to_vtkUnstructuredGrid(self):
        "Convert to VTK"

        import vtk
        from vtk.util.numpy_support import numpy_to_vtkIdTypeArray,numpy_to_vtk

        pts  = self.get_points()
        hexa = self.get_topology()
        
        # VTK points
        points = vtk.vtkPoints()
        pdata = numpy_to_vtk(pts)
        points.SetData(pdata)
        
        # VTK cells
        cells = vtk.vtkCellArray()
        cdata_con = np.empty(shape=(hexa.shape[0],9),dtype=np.int64)
        cdata_con[:,0] = 8
        cdata_con[:,1:] = hexa
        cdata_con = numpy_to_vtkIdTypeArray(cdata_con.ravel())
        cells.SetCells(hexa.shape[0],cdata_con)
        
        # VTK grid
        grid = vtk.vtkUnstructuredGrid()
        grid.SetPoints(points)
        grid.SetCells(vtk.VTK_HEXAHEDRON,cells)

        return grid
    
    def to_DOLFIN(self):
        "Convert to DOLFIN mesh"
        
        from dolfin import Mesh,MeshEditor
        mesh = Mesh()
        editor = MeshEditor()
        editor.open(mesh,"hexahedron",3,3,1)

        ordering = [[0,0,0],[1,0,0],[0,1,0],[1,1,0],
                    [0,0,1],[1,0,1],[0,1,1],[1,1,1]]
        pts  = self.get_points()
        hexa = self.get_topology(ordering)        
               
        editor.init_vertices(pts.shape[0])
        for i,p in enumerate(pts):
            editor.add_vertex(i,p)
        editor.init_cells(hexa.shape[0])
        for i,c in enumerate(hexa):
            editor.add_cell(i,c)
        editor.close(False)
        
        return mesh