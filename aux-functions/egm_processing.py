# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
Functions created to do the pre and postprocessing of EGM signals, initially 
for the models but in principle for real EGMs as well

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

#%% Import libraries
#Python
import os
import numpy as np
import scipy.signal as sig
from detect_peaks import detect_peaks
from scipy.spatial.distance import cdist,pdist,squareform
from scipy.interpolate import interp1d
import multiprocessing as mp 
from itertools import repeat
import platform 


import pandas as pd
#%% Constants

#%% Activation detection

def Filter4Activation(data,fs,N = 4,method = 'derivative'):
    
    if method == 'botterom':
        n = data.shape[0]
        # BP 0.7-200 Hz
        FilteredSignal = sig.resample(data,int(n/40),axis = 0) #downsample to fs/40
    
        b,a = sig.butter(N,0.7/(fs/2/40),'low') # fs is lower here
        FilteredSignal = sig.filtfilt(b,a,FilteredSignal,axis = 0)
        FilteredSignal = sig.resample(FilteredSignal,n,axis = 0) 
        FilteredSignal = data-FilteredSignal
        
        b,a = sig.butter(N,40/(fs/2),'low')
        FilteredSignal = sig.filtfilt(b,a,FilteredSignal,axis = 0)    
    
        # BP 40-250  
        b,a = sig.butter(N,[40/(fs/2),250/(fs/2)],'bandpass')    
        FilteredSignal = sig.filtfilt(b,a,FilteredSignal,axis = 0)    
    
        # Rectify
        FilteredSignal = np.abs(FilteredSignal)
        
        # LP 20 Hz
        b,a = sig.butter(N,30/(fs/2),'low')
        FilteredSignal = sig.filtfilt(b,a,FilteredSignal,axis = 0)   
    elif method=='derivative':
        [b,a] = sig.butter(N,[1/(fs/2), 20/(fs/2)],'bandpass')
        FilteredSignal = sig.filtfilt(b,a,data,axis=0)
        FilteredSignal = -np.diff(FilteredSignal,axis=0)
        
    return FilteredSignal
 
def DetectActivations(data,fs,maxFreq = 15,N=1,method='globalThreshold'):
    # Basically detect peaks
    if method=='globalThreshold':
        FilteredSignal = Filter4Activation(data,fs,N,method='botterom')

        PeakLocations = [detect_peaks(row, mpd = int((1/maxFreq)*fs),
                                        mph = 0.1*np.max(row),ax = -1) for row in FilteredSignal.T]
    elif method=='adaptiveThreshold':
        FilteredSignal = Filter4Activation(data,fs,N,method='derivative')
        # Based on the Pan tompkins algorithm with some tweaks
        PeakLocations = list()
        for sigInd in range(FilteredSignal.shape[1]):
            fsignal = FilteredSignal[:,sigInd]
            # Functions to update thresholds and signal/noise values
            UpdateSPKI = lambda PKI,SPKI: 0.125*PKI+0.875*SPKI
            UpdateNPKI = lambda PKI,NPKI:0.125*PKI+0.875*NPKI
            UpdateTH1 = lambda SPKI,NPKI:NPKI+0.25*(SPKI-NPKI)
            UpdateTH2 = lambda TH1:0.5*TH1
            #Parameters (make tunable?)
            lowestAFCL = 200 # ms
            highestAFCL = 100 # ms, not used at the moment
            SPKI = np.mean(fsignal[:300]) # Initial signal level
            NPKI = SPKI/2 # Initial noise level
            #Initial thresholds
            TH1 = UpdateTH1(SPKI,NPKI) 
            TH2 = UpdateTH2(TH1)
            
            # Step 1/3: Pan-Tompkins search for peaks
            lastPeak = 0
            SignalPeaks = []
            SearchbackList = []
            NoiseList = []
            warningCount = 0
            i = 0
            pks = detect_peaks(fsignal,mpd=highestAFCL)
            while i<len(pks):
                peak = pks[i]
                PKI = fsignal[peak]
                # Normal peak Interval
                if peak-lastPeak<=lowestAFCL:
                    if PKI>TH1:
                        # Is signal peak
                        SignalPeaks.append(peak)
                        # Update stuff
                        lastPeak=peak
                        SPKI = UpdateSPKI(PKI,SPKI)
                        TH1 = UpdateTH1(SPKI,NPKI)
                        TH2 = UpdateTH2(TH1)
            
                    elif PKI>TH2: # May be a missed peak
                        SearchbackList.append(peak)
                    else: # Noise peak
                        NoiseList.append(peak)
                        # Update stuff
                        NPKI = UpdateSPKI(PKI,NPKI)
                        TH1 = UpdateTH1(SPKI,NPKI)
                        TH2 = UpdateTH2(TH1)
                    # For all cases here
                    i= i+1
                # Large interval
                else:
                    tmp_array = np.asarray(SearchbackList)
                    possiblePeaks = tmp_array[np.where((tmp_array>lastPeak) * (tmp_array<peak))[0]]
                    if len(possiblePeaks)!=0:
                        finalPeak = np.where(fsignal[possiblePeaks]==np.max(fsignal[possiblePeaks]))[0][0]
                        finalPeak = possiblePeaks[finalPeak]
                        SignalPeaks.append(finalPeak)
                        # Update stuff
                        lastPeak = finalPeak
                        SPKI=0.25*fsignal[finalPeak]+0.75*SPKI
                        TH1 = UpdateTH1(SPKI,NPKI)
                        TH2 = UpdateTH2(TH1)
                        # Do not add 1 to i here! It has to come back to the beginning of the loop and check again
                    else:
                        warningCount+=1
                        lastPeak = peak # Run again the loop, but ignore this warning
            if warningCount!=0:
                RuntimeWarning('Number of unsolved long intervals: %d'%warningCount)
                
            # Part 2/3: search for short intervals, remove them
            # i = 0
            # while True:
            #     if i==len(SignalPeaks)-1: break
            #     interval = SignalPeaks[i+1]-SignalPeaks[i]
            #     if interval<highestAFCL:
            #         pkPair = [SignalPeaks[i+1],SignalPeaks[i]]
            #         vals = fsignal[pkPair]
            #         lowestPeak = np.where(vals==np.min(vals))[0][0]
            #         SignalPeaks.remove(pkPair[lowestPeak])
            #     else:
            # #         i = i+1
                    
            # # Part 3/3: Get back those unsolved long intervals
            # while (np.diff(SignalPeaks)>=lowestAFCL).any():
            #     problems = np.where(np.diff(SignalPeaks)>=lowestAFCL)[0]
            #     for p in problems:
            #         pk_int = [SignalPeaks[p],SignalPeaks[p+1]]
            #         sig_snippet = fsignal[pk_int[0]:pk_int[1]]
            #         extra_pks = detect_peaks(sig_snippet)
            #         if len(extra_pks)==0:
            #             extra_pks=np.where(sig_snippet==np.max(sig_snippet))[0]

            #         add_pk = np.where(sig_snippet[extra_pks] == np.max(sig_snippet[extra_pks]))[0][0]
            #         SignalPeaks.append(extra_pks[add_pk]+SignalPeaks[p])
            #     SignalPeaks.sort()
            
            PeakLocations.append(SignalPeaks)
    return PeakLocations
            
def LinearInterpolatedPhase(signal,activations,fs):
    
    n = signal.shape[-1]
    Phase = np.zeros(signal.shape)*np.nan
    
    if len(signal.shape)>1:

        for i in range(signal.shape[0]):
            xp = []
            fp = []
            for peak in activations[i]:
                xp.append(peak)
                xp.append(peak+1)
                fp.append(np.pi)
                fp.append(-np.pi)
            
            Phase[i,activations[i][0]:activations[i][-1]] = np.interp(np.arange(activations[i][0],
                                                                          activations[i][-1]),
                                                                      xp,fp)
    else:
        xp = []
        fp = []
        for peak in activations:
            xp.append([peak,peak+1])
            fp.append([np.pi,-np.pi])
        
        Phase[activations[0]:activations[-1]] = np.interp(np.arange(activations[0],activations[-1]+1),xp,fp)          
    
    return Phase

def GetAvgActivationPattern(catheter,initialTime,finalTime):
    ActivationList = [np.asarray(L) for L in catheter.Activations]
    ActivationList = [L[(L>initialTime)*(L<finalTime)] for L in ActivationList]
    
    firstAct = np.min([np.min(L) for L in ActivationList])
    lastAct = np.max([np.max(L) for L in ActivationList])
    
    ActivationMatrix = np.zeros((16,lastAct-firstAct+1),dtype = int)
    
    for i,L in enumerate(ActivationList):
        ActivationMatrix[i,L-firstAct] = 1
        
    ActivationPresence = np.convolve(ActivationMatrix.sum(axis=0), np.ones(15))
    ActivationPresence = (ActivationPresence!=0).astype(int)
    
    beginBlocks = np.where(np.diff(ActivationPresence)>0)[0]
    endBlocks = np.where(np.diff(ActivationPresence)<0)[0]
    
    
    beginBlocks = np.append([0],beginBlocks)
    endBlocks = np.append(endBlocks,[lastAct])
    
    ActivationPattern = np.zeros((16,len(beginBlocks)))*np.nan
    for i in range(len(beginBlocks)):
        foundActs = np.sum(ActivationMatrix[:,beginBlocks[i]:endBlocks[i]],axis=1)
        elecInds,nextActs = np.where(ActivationMatrix[:,beginBlocks[i]:endBlocks[i]])
        ActivationPattern[elecInds,i] = nextActs-np.min(nextActs)
    
    return np.nanmedian(ActivationPattern,axis=1)

def GroupActivations(activationList,longestElectrodeAxis = 9,cvThreshold=0.2):
    '''
    This a preprocessing step for generating isochrones. The function groups activations that occur sequentially whitin an AFCL
    The AFCL is estimated from the activation lists themselves.

    The algorithm works as follows:
        - Select a time instant with an activation
            * If the current activation is too close to the previous activation in that electrode, skip
        - Get the next activation at the same electrode
        - Detect all activations in the other electrodes in the window defined by the two previously detected activation times
        - Get the first activation in each electrode and associate them in a group
        - Move the time step to the latest activation in that group+1
        - Do this iterating over the columns 
    Notes:
        - If less than 1/4 of the channels are activated, skip group
    '''
    # Make an "Activation matrix"
    nChannels = len(activationList)
    nSamples = np.max([np.max(act) for act in activationList])+1
    estimatedAFCL = np.median([np.median(np.diff(act)) for act in activationList])

    actMatrix = np.zeros((nChannels,nSamples))
    for electrodeIndex in range(nChannels):
        actMatrix[electrodeIndex,activationList[electrodeIndex]] = 1
        
    # Loop over columns to find groups
    activationGroups = dict()
    j=0
    previousActivations = np.zeros(nChannels)
    while j<nSamples:
        if np.sum(actMatrix[:,j])!=0:
            firstActivatedElectrode = np.where(actMatrix[:,j]!=0)[0][0] # Will select the first if two activate at the same time

            if j-previousActivations[firstActivatedElectrode]<0.7*estimatedAFCL and np.sum(previousActivations)!=0:
                # Activation was too close
                j = j+1
                continue

            if actMatrix[firstActivatedElectrode,j:].sum()>1:
                cycleEnd = np.where(actMatrix[firstActivatedElectrode,j:]!=0)[0][1]+j #next activation in same electrode
            else:
                break

            subActivationMat = actMatrix[:,j:cycleEnd]
            firstActivationPerElectrode = list()
            for row in subActivationMat:
                try:
                    firstActivationPerElectrode.append(np.where(row)[0][0]+j)
                except:
                    firstActivationPerElectrode.append(-1)
            firstActivationPerElectrode = np.asarray(firstActivationPerElectrode,dtype=int)
            activationRange = firstActivationPerElectrode.max()-firstActivationPerElectrode[firstActivationPerElectrode!=-1].min()
            if (firstActivationPerElectrode==-1).sum()>nChannels/4 or activationRange>longestElectrodeAxis/cvThreshold:
                j = j+1
                continue
            else:
                activationGroups[np.min(firstActivationPerElectrode[firstActivationPerElectrode>=0])] = firstActivationPerElectrode
                j = np.max(firstActivationPerElectrode)+1
                previousActivations = firstActivationPerElectrode
        else:
            j = j+1

    return activationGroups

#%% Frequency
def ComputeDCMDominantFrequency(catheterObject,DFRange,NumberOfFFTPoints, preFilter=False):
    
    samplingFrequency = catheterObject.SamplingFrequency
    nyquistFrequency = samplingFrequency / 2
    numberOfChannels = catheterObject.GetNumberOfChannels()
    
    if preFilter:
        bHigh, aHigh = sig.butter(2, np.array([40, 250])/ nyquistFrequency,'bandpass')
        bLow, aLow = sig.butter(2, [20 / nyquistFrequency], 'low')
        
        filteredSignals = sig.filtfilt(bHigh, aHigh, catheterObject.EGMData,axis=0)
        filteredSignals = np.abs(filteredSignals)
        filteredSignals = np.append(np.zeros((100,numberOfChannels)),filteredSignals,axis=0)
        filteredSignals = np.append(filteredSignals,np.zeros((100,numberOfChannels)),axis=0)
        filteredSignals = sig.filtfilt(bLow, aLow, filteredSignals,axis=0)
        filteredSignals = filteredSignals[100:-100, :]
    else:
        filteredSignals = catheterObject.EGMData
    
    channelDominantFrequencies = np.zeros(numberOfChannels)*np.nan
    
    minimalAFFrequency = DFRange[0]
    maximalAFFrequency = DFRange[-1]
    nFFT = NumberOfFFTPoints
        
    for channelIndex in range(numberOfChannels):
        channelData = filteredSignals[:, channelIndex]
        # compute DF
        frequencies,frequencyPower = sig.welch(channelData, nfft = nFFT, fs = samplingFrequency)
        peakIndices = detect_peaks(frequencyPower)
        peakValues = frequencyPower[peakIndices]
        validDFFrequencies = (frequencies >= minimalAFFrequency) *\
            (frequencies < maximalAFFrequency)
        peakDFValues = peakValues[validDFFrequencies[peakIndices]]
        peakDFIndices = peakIndices[validDFFrequencies[peakIndices]]
        maxPosition = np.where(peakDFValues==np.max(peakDFValues))[0][0]
        channelDominantFrequencies[channelIndex] = frequencies[peakDFIndices[maxPosition]]
    return channelDominantFrequencies

#%% Phase analysis

def ComputeActivationPhaseSignal(electrodeObject, intrinsicDeflections = None):
    if intrinsicDeflections is None:
        intrinsicDeflections = electrodeObject.Activations
    numberOfElectrodes = electrodeObject.GetNumberOfChannels()
    numberOfSamples = electrodeObject.GetNumberOfSamples()
    
    activationPhaseSignal = np.ones(shape = (numberOfSamples, numberOfElectrodes))*np.nan
    sampleCycleLength = np.ones(shape = (numberOfElectrodes, 1))*np.nan
    
    for electrodeIndex in range(0,numberOfElectrodes):
        activationIndices = np.asarray(intrinsicDeflections[electrodeIndex])
        sampleCycleLength[electrodeIndex] = np.mean(np.diff(activationIndices))

        # Generate the phase values for interpolating
        piVals = np.ones_like(activationIndices)*np.pi
        
        # Make the +1 values and append
        activationIndices = np.append(activationIndices,activationIndices+1)
        piVals = np.append(piVals,-piVals)
        
        interpFunction = interp1d(activationIndices,piVals,'linear',bounds_error=False)
        activationPhaseSignal[:,electrodeIndex] = interpFunction(np.arange(0,numberOfSamples))
    
    return activationPhaseSignal, sampleCycleLength


def SinusoidalPhaseLoop(currentSignal,period):#(sampleIndex,localDifference,period,sinusoidWavelet,currentSignalLength):
    # Output=[]
    # for tt in range(-int(period / 2),int(period / 2)+1):
    #     if (sampleIndex + tt) > 0 and (sampleIndex + tt) < currentSignalLength:
    #         toAdd = localDifference * sinusoidWavelet[int(tt + period / 2)]
    #         Output.append((sampleIndex + tt, toAdd))
    # return Output
    sinusoidWavelet = np.sin(2 * np.pi * (np.arange(period+1) - period / 2) / period)

    recomposedSignal = np.zeros(currentSignal.shape,dtype = 'float')
    for sampleIndex in range(1,len(recomposedSignal)-1):   
        localDifference = currentSignal[sampleIndex + 1] - currentSignal[sampleIndex -  1]
        if localDifference < 0:
            for tt in range(-int(period / 2),int(period / 2)):
                if (sampleIndex + tt) > 0 and (sampleIndex + tt) < len(currentSignal):
                    recomposedSignal[sampleIndex + tt]= recomposedSignal[sampleIndex + tt] +\
                            localDifference * sinusoidWavelet[int(tt + period / 2)]
    return recomposedSignal


def ComputeSinusoidalPhase(catheterObject):
    # Sinusoidal recomposition + Hilbert transform
    DFRange = [0.5, 20]
    NumberOfFFTPoints = 2**14
    dcmFilter = True

    dominantFrequencies = ComputeDCMDominantFrequency(catheterObject,DFRange,NumberOfFFTPoints,dcmFilter)
    signalPeriodSamples = np.round((1/dominantFrequencies) * catheterObject.SamplingFrequency)
    
    signals = catheterObject.EGMData
    recomposedSignals = np.zeros_like(signals)
    
    if platform.system()=='Linux':
        with mp.Pool(mp.cpu_count()) as pool:
            Output = pool.starmap(SinusoidalPhaseLoop,
                                zip([signals[:, channelIndex] \
                                    for channelIndex in range(catheterObject.GetNumberOfChannels())],
                                    signalPeriodSamples.astype(int),))
        recomposedSignals = np.asarray(Output).T

        hilbertTransform = sig.hilbert(recomposedSignals,axis=0)
        phaseData = np.arctan2(np.real(hilbertTransform), -np.imag(hilbertTransform)) 
    else:
        for channelIndex in range(catheterObject.GetNumberOfChannels()):
            period = int(signalPeriodSamples[channelIndex])
            sinusoidWavelet = np.sin(2 * np.pi * (np.arange(period+1) - period / 2) / period)
            currentSignal = signals[:,channelIndex]
            recomposedSignal = np.zeros_like(currentSignal)
            for sampleIndex in range(1,len(recomposedSignal)-1):   
                localDifference = currentSignal[sampleIndex + 1] - currentSignal[sampleIndex -  1]
                if localDifference < 0:
                    for tt in range(-int(period / 2),int(period / 2)):
                        if (sampleIndex + tt) > 0 and (sampleIndex + tt) < len(currentSignal):
                            recomposedSignal[sampleIndex + tt]= recomposedSignal[sampleIndex + tt] +\
                                    localDifference * sinusoidWavelet[int(tt + period / 2)]
    
            recomposedSignals[:, channelIndex] = recomposedSignal

    hilbertTransform = sig.hilbert(recomposedSignals,axis=0)
    phaseData = np.arctan2(np.real(hilbertTransform), -np.imag(hilbertTransform)) 
    return phaseData



#%% PS processing (TODO: Check which of these functions is actually used)

def MakeTrajectoryDicts(filename,scale):
    # MAybe this should be elsewhere, as it is not properly signal processing
    
    fp = open(filename, "rt")
    lines = fp.readlines()
    fp.close
    PositionPerTimeDict = dict()
    PositionPerTrajectoryDict = dict()
    for line in lines:
        f = line.split()
        (t,x,y,z,traj) = (int(f[0]), float(f[1]), float(f[2]), float(f[3]),int(f[4]))
        if t in PositionPerTimeDict:
            PositionPerTimeDict[t].append([x*scale,y*scale,z*scale])
        else:
            PositionPerTimeDict[t] = [[x*scale,y*scale,z*scale]]
        # Organize by trajectories
        if traj in PositionPerTrajectoryDict:
            PositionPerTrajectoryDict[traj].append([t,x*scale,y*scale,z*scale])
        else:
            PositionPerTrajectoryDict[traj] = [[t,x*scale,y*scale,z*scale]]
    

    TrajectoryDurations = np.zeros((len(PositionPerTrajectoryDict.keys()),2))
    k = 0
    for i in PositionPerTrajectoryDict:
        uniqueTimes = np.unique(np.asarray(PositionPerTrajectoryDict[i])[:,0])
        TrajectoryDurations[k,0] = uniqueTimes.min()
        TrajectoryDurations[k,1] = uniqueTimes.max()
        k = k+1
    
    return PositionPerTimeDict,PositionPerTrajectoryDict,TrajectoryDurations

#% PS Proximity
def PSProximity(proximityDict,trajDurations,thresholds=[7,15]):
    # Based on the distances, checks whether the trajectories are
    # close or near each catheter. Run GetDistancsPSCatheter before this
    
    initialTime=trajDurations.min()
    endTime=trajDurations.max()
    ClosePresence = np.zeros((endTime-initialTime+1,
                              len(proximityDict.keys())),
                              dtype=int)
    NearPresence = np.zeros((endTime-initialTime+1,
                             len(proximityDict.keys())), 
                             dtype=int)
    
    #for each catheter
    for i,catheter in enumerate(proximityDict.keys()):
        for key in proximityDict[catheter].keys():
            lims = trajDurations[key]-initialTime
            close = (proximityDict[catheter][key]<thresholds[0]).astype(int)
            ClosePresence[lims[0]:lims[1]+1,i] += close
            near = ((proximityDict[catheter][key]>thresholds[0]) *\
                    (proximityDict[catheter][key]<thresholds[1])).astype(int)
            NearPresence[lims[0]:lims[1]+1,i] += near
            
    return ClosePresence,NearPresence


def GetDistancsPSCatheter(CGroup,psDataFrame,presenceThreshold=15,
                          initialTime=0,endTime=-1):
    # A presenceThreshold==-1 means that the distance for all trajectories will be returned
    # Make matrix with catheter centers
    Fields = list(CGroup.Catheters.keys())
    catCenters = np.zeros((len(CGroup.Catheters.keys()),3))
    for i,key in enumerate(Fields):
        elec = CGroup.Catheters[key]
        catCenters[i,:] = elec.CatheterCenter   
    
    # Get unique trajectories and prepare results

    unTraj = np.unique(psDataFrame['traj'])
    proximityDict = {key:dict() for key in Fields}

    # For each trajectory, check whether it approaches each catheter. If yes, 
    # add to the corresponding dict
    for i,traj in enumerate(unTraj):
        print('Trajectory %d'%i)
        traj_df = psDataFrame.query('traj=='+str(traj))
        times = np.asarray(traj_df['t'])
        # traj_df = pd.DataFrame(traj_df,columns=['x','y','z'])
        
        traj_centers = [np.mean(traj_df.query('t=='+str(t)),axis=0) for t in np.unique(times)]
        traj_centers = np.asarray(traj_centers)[:,1:-1] #remove time and traj code
        distances = cdist(catCenters,traj_centers)
        
        if presenceThreshold!=-1:
            inds = np.where(np.sum((distances<presenceThreshold),axis=-1))[0]
        else:
            inds = np.arange(distances.shape[0])
        if len(inds)!=0:
            for j in inds:
                # if not far, store trajectory with its number
                proximityDict[Fields[j]][i]=distances[j,:]
        
    return proximityDict

#% Detect blocks with PSs nearby
def PSBlocks(psDict,kernelSize = -1,reverseOutput=False):
    
    # I did this as a sort of image processing idea in 1D to close the gaps and have more uniform blocks
    #####
    offset=int(kernelSize/2)
    paddedData = np.append(np.zeros(offset),
                           np.append(psDict,np.zeros(offset)))
    
    dilatedData = np.zeros_like(psDict)
    for j in range(offset,offset+len(psDict)-1):
        tmp = paddedData[j-offset:j+offset]
        if np.sum(tmp)!=0:
            dilatedData[j-offset]=1
        else:
            dilatedData[j-offset]=0
          
    # Ideally a erosion operation would make sense here, but the results are not so good
    # paddedData = np.append(np.zeros(offset),
    #                np.append(dilatedData,np.zeros(offset)))
    
    # closedData = np.zeros_like(rawData)
    # for j in range(offset,offset+len(rawData)-1):
    #     tmp = paddedData[j-offset:j+offset]
    #     if np.sum(tmp)==len(tmp):
    #         closedData[j-offset]=1
    #     else:
    #         closedData[j-offset]=0    
            
    if reverseOutput:
        dilatedData = np.array(dilatedData==0,dtype=int)
    blocks = ChangeDetection(dilatedData)
    lengths = [b[1]-b[0] for b in blocks]
    
    return blocks,lengths

#% Make heatmaps
def MakeHeatmap(Anatomy,PSDataFrame):
    # I make a heatmap of the centers of each trajectory, mainly to save
    # computational power of projecting every PS onto the endocardium
    
    HM = np.zeros(Anatomy.shape[0])
    
    for traj in np.unique(PSDataFrame['traj']):
        trajDF = PSDataFrame.loc[PSDataFrame['traj']==traj,['t','x','y','z']]
        medDF = [np.median(trajDF.loc[trajDF['t']==t,['x','y','z']],axis=0) for t in np.unique(trajDF['t'])] 
        medDF = np.asarray(medDF)
        
        for row in medDF:
            dist = np.linalg.norm(Anatomy-row,axis=1)
            ind = np.where(dist==np.min(dist))[0][0]
            HM[ind] += 1
    
    return HM

#%% Basic and convenient signal processing operations (maybe belong to a different library)

# Normalize between two values
def Norm2Vals(signal,vals=[0,1]):
    nSignal = (vals[1]-vals[0])*(signal-np.nanmin(signal))/(np.nanmax(signal)-np.nanmin(signal))+vals[0]
    return nSignal

# Detect changes from 0 to 1 and vice versa
def ChangeDetection(array):
    paddedData = np.append(0,np.append(array,0))
    # Now search for blocks in the closedData
    blockStart = np.where(np.diff(paddedData)>0)[0]
    blockEnd = np.where(np.diff(paddedData)<0)[0]
    blockEnd[blockEnd==len(array)]=len(array)-1
    blocks = [[blockStart[k],blockEnd[k]] for k in range(len(blockStart))]
    return blocks

# Linear algebra and geometry related

# Distance to line
# def DistanceToLine(p, a, b):
#     # normalized tangent vector
#     d = np.divide(b - a, np.linalg.norm(b - a))
#     # signed parallel distance components
#     s = np.dot(a - p, d)
#     t = np.dot(p - b, d)
#     # clamped parallel distance
#     h = np.maximum.reduce([s, t, 0])
#     # perpendicular distance component
#     c = np.cross(p - a, d)
#     return np.hypot(h, np.linalg.norm(c))
def DistanceToLine(P,A,B):
    return np.linalg.norm(np.cross(P-A,P-B))/np.linalg.norm(B-A) 

def MakeRotationalMatrix(a,b = None):
    # a can be a vector ( to align with b) or an angle
    if b is not None:
        crossProduct = np.cross( a, b )
        sinA = np.linalg.norm(crossProduct)
        axis = crossProduct / sinA
        cosA = np.dot( a, b )
        oneMinusCosA = 1.0 - cosA

        RotationalMatrix = np.array([[  axis[0] * axis[0] * oneMinusCosA + cosA,
                                        axis[1] * axis[0] * oneMinusCosA - (sinA * axis[2]),
                                        axis[2] * axis[0] * oneMinusCosA + (sinA * axis[1])],
                                        [axis[0] * axis[1] * oneMinusCosA + (sinA * axis[2]),
                                        axis[1] * axis[1] * oneMinusCosA + cosA,
                                        axis[2] * axis[1] * oneMinusCosA - (sinA * axis[0])],
                                        [axis[0] * axis[2] * oneMinusCosA - (sinA * axis[1]),
                                        axis[1] * axis[2] * oneMinusCosA + (sinA * axis[0]),
                                        axis[2] * axis[2] * oneMinusCosA + cosA]])
    else:
        RotationalMatrix = np.array([[np.cos(a), -np.sin(a)],[np.sin(a),np.cos(a)]])
    return RotationalMatrix

def MakeAdjacencyMatrix(spatialResolution,tissuePatch):
    '''
    Generates the adjacency matrix for a cloud of points obtained from an igb

    Parameters
    ----------
    tissuePatch : N,3 np array
        Points of the tissue patch (3D but could be 2D).
    spatialResolution : float, optional
        The spatial resolution of the igb. The default is 1.

    Returns
    -------
    AdjacencyMatrix : TYPE
        DESCRIPTION.

    '''
    Distances = pdist(tissuePatch)
    AdjacencyMatrix = squareform(Distances)/np.sqrt(2)
    AdjacencyMatrix[AdjacencyMatrix<=spatialResolution] = -1
    AdjacencyMatrix[AdjacencyMatrix>spatialResolution] = 0
    AdjacencyMatrix = -AdjacencyMatrix
    return AdjacencyMatrix    

def GeodesicDistances(Source,AdjacencyMatrix,EdgeWeight = 1):
    Distances = np.ones(AdjacencyMatrix.shape[0])*np.nan
    Distances[Source] = 0
    
    Vertex = Source
    InSet = np.ones_like(Distances);   
        
    Visited = []
        
    while True:
        #Include u to sptSet.
        Visited.append(Vertex)
        #Update distance value of all adjacent vertices of u. 
        #To update the distance values, iterate through all adjacent vertices. 
        #For every adjacent vertex v, if the sum of a distance value of u (from source) 
        #and weight of edge u-v, is less than the distance value of v, then update the distance value of v. 
        Neighbors = np.where(AdjacencyMatrix[Vertex,:])[0]
        for i,v in enumerate(Neighbors):
            if Distances[Vertex]+EdgeWeight<Distances[v] or np.isnan(Distances[v]):
                Distances[v] = Distances[Vertex]+EdgeWeight
        InSet[Vertex] = np.nan
        if np.nansum(InSet)==0:
            break
        #check minimum distance
        temp = np.where(~np.isnan(Distances)*(~np.isnan(InSet)))[0]
        # Check if in set        
        try:
            Vertex = temp[np.where(Distances[temp]==np.nanmin(Distances[temp]))[0][0]]    
        except:
            Vertex = np.where(~np.isnan(InSet))[0][0]
    return Distances

def GetShortestPath(adjacencyMatrix,positions,source,target):
    
    PatchLen = adjacencyMatrix.shape[0]
    StepDists = np.ones(PatchLen)*np.inf
    prev = np.ones(PatchLen)*np.nan
    Q = np.arange(PatchLen,dtype=float)

    StepDists[source] = 0
    prev[source] = source

    while np.isnan(Q).sum()<PatchLen: #While not all points have been visited
        #u ← vertex in Q with min dist[u]
        unvisitedIndexes = np.where(~np.isnan(Q))[0]
        u = np.where(StepDists[unvisitedIndexes]==np.nanmin(StepDists[unvisitedIndexes]))[0][0]
        u = unvisitedIndexes[u]
        if u == target:
            # print('Stopped at target')
            break

        #remove u from Q
        Q[u] = np.nan

        neighbors = np.where(adjacencyMatrix[u,:]!=0)[0]
        validNeighbors = neighbors[np.isin(neighbors,Q)]
        for v in validNeighbors:
            alt = StepDists[u]+np.linalg.norm(positions[u]-positions[v])
            if alt<StepDists[v]:
                StepDists[v]=alt
                prev[v] = u
                
    # Connect path
    prev = np.array(prev).astype(int)
    path = []
    u = prev[target]
    while ~np.isin(source,path):
        path.append(u)
        u = prev[u]
    path = np.asarray(path).flatten()
    
    return path,StepDists


def CircularMeanAndVariance(angles):
    r = np.sum(np.exp(1j * angles))
    circularMean = np.angle(r)
    rLength = np.abs(r)/ np.sum(np.ones(angles.shape))
    circularVariance = 1 - rLength
    return circularMean, circularVariance

    