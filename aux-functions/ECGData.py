# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 12:08:07 2020

Implementation of the ECGData class from Stef's software
Note: it does not have the properties or methods of the parent class "handle"

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import numpy as np

class ECGData:
    # Methods

    
    def __init__(self,filename, data, samplingFrequency,**kwargs):
        #Properties
        self.Filename = filename
        self.Data = data        
        self.SamplingFrequency = samplingFrequency
                
                
        if 'electrodeLabels' in kwargs:
            self.ElectrodeLabels = kwargs['electrodeLabels']
        else:
            self.ElectrodeLabels =  {}   
        if 'referenceData' in kwargs:
            self.ReferenceData = kwargs['referenceData']
        else:
            self.ReferenceData = None
        if 'referenceLabels' in kwargs:
            self.ReferenceLabels = kwargs['referenceLabels']
        else:
            self.ReferenceLabels = None         
        if 'type' in kwargs:
            self.Type = kwargs['type']
        else:
            self.Type = None             

    
    def delete(self):
        del self #TODO: does this do the same as in Stef's function? 
        
    def GetNumberOfChannels(self):
        return self.Data.shape[-1]
    
    def GetNumberOfSamples(self):
        return self.Data.shape[0]
        
    def GetTimeRange(self):
        return np.arange(0,
                         self.GetNumberOfSamples()/self.SamplingFrequency,
                         1/self.SamplingFrequency)

    def Copy(self, selectedChannels = -1, timeRange = -1):
        time = self.GetTimeRange()
        
        if timeRange==-1:
            timeRange = [time[0], time[-1]]
        if selectedChannels==-1:
            selectedChannels = np.ones(self.GetNumberOfChannels())
        
        validTime = (time >= timeRange[0]) * (time <= timeRange[1])
        data = self.Data(validTime, selectedChannels)
        labels = self.ElectrodeLabels(selectedChannels)
        ecgDataCopy = ECGData(self.Filename, data,self.SamplingFrequency,labels)
        if self.ReferenceData is not None:
            ecgDataCopy.ReferenceData = self.ReferenceData[validTime, :]
            ecgDataCopy.ReferenceLabels = self.ReferenceLabels
        
        ecgDataCopy.Type = self.Type   
    