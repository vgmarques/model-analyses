'''
This library holds functions related to the igb file handling without the py_ccmc library.

This means: meant for running in windows, where everything is hard to compile
'''

import os
import numpy as np
import sys
import gzip
from itertools import product
#from scipy.linalg import svd  
#from scipy.spatial.distance import pdist,squareform
import subprocess
from scipy.spatial.distance import pdist,squareform

#--
nptype = {'byte': 'uint8',
          'char': 'int8', 
          'short': 'int16',
          'long': 'int32',
          'float': 'float32',
          'double': 'float64',
          'complex': 'complex64'}

byte_multiplier = {'uint8': 1,
                  'int8': 1,
                  'int16': 2,
                  'int32': 4,
                  'float32': 4,
                  'float64': 8,
                  'complex64': 8}

# unicode is not defined in Python 3, it is handled as str
if sys.version_info.major==3:
  unicode = str 
#--


def ReadHeader(filename,opened = False):
    
    if opened:
        buf = filename[:1024]#.decode("UTF-8") # byte file read from zB .gz file
    else:
        fi = open(filename,'rb')
        buf = fi.read(1024)#.decode()

    hdr = buf.decode().split('\r\n')
    cmm = [l.strip()[2:] for l in hdr if l.startswith("#")]
    hdr = sum((l.split() for l in (l.strip() for l in hdr if not l.startswith("#") or l.startswith("comments")) if len(l)>0),[])
    hdr = dict(tuple(ob.split(":")) for ob in hdr)
    hdr['comments'] = cmm
    
    for a in 'xyzt':
        if a in hdr: hdr[a] = int(hdr[a])
    for a in ['zero','facteur']:
        if a in hdr: hdr[a] = float(hdr[a])            
    return hdr

def ReadLeanHeader(iga):
    # Simpler implementation than Ali's
    intro = iga.stdout.read(1024*np.uint8().nbytes)
    if intro.decode()[:8]!='leanIGB1':
        RuntimeError('not lean-IGB version 1 format')
    #
    hdr = {}
    dims = np.frombuffer(iga.stdout.read(16*np.uint32().nbytes),dtype = np.uint32)
    hdr['Nx'] = dims[0]
    hdr['Ny'] = dims[1]
    hdr['Nz'] = dims[2]
    hdr['Nt'] = dims[3]
    hdr['Ne'] = dims[4] # number of foreground elements stored
    hdr['dtype'] = dims[5]
    hdr['arch'] = dims[6]
    #
    scaling = np.frombuffer(iga.stdout.read(2*np.float32().nbytes),dtype = np.float32)
    hdr['facteur'] = scaling[0]
    hdr['zero'] = scaling[1]
    vs = 'coordinates'
    ts = iga.stdout.read(len(vs)).decode()
    if ts!=vs: 
        RuntimeError('Lost track')
    coords = np.frombuffer(iga.stdout.read((hdr['Ne']*3)*np.uint32().nbytes),dtype = np.uint32).reshape(hdr['Ne'],3)
    vs = 'data'
    ts = iga.stdout.read(len(vs)).decode()
    if ts!=vs: 
        RuntimeError('Lost track')
    return hdr,coords
    
def Load(fname_or_file,x =  [0],y= [0],z= [0],t = [0],**kwargs):
    '''
    Loads igb data.
    
    Can slice the igb file to get only the arrays of interest

    FIXME: the slicing is not working!!

    Parameters
    ----------
    fname_or_file : str
        Despite the name, must be a string pointing to the file, not the file itself
    x : list (max. length 2), optional
        The interval in the first axis to read. If [0], reads everything. 
        The default is [0].
    y : list (max. length 2), optional
        The interval in the second axis to read. If [0], reads everything. 
        The default is [0].
    z : list (max. length 2), optional
        The interval in the third axis to read. If [0], reads everything. 
        The default is [0].
    t : list (max. length 2), optional
        The interval in the fourth axis to read. If [0], reads everything. 
        The default is [0].

    Raises
    ------
    RuntimeError
        It has to contain some index.

    Returns
    -------
    data : np array
        Data read from the igb.
    hdr : TYPE
        header of the original igb (disconsiders slices!). FIXME?

    '''
    
    
    if type(fname_or_file) in [str,unicode]:
        fname,extension = os.path.splitext(fname_or_file)
    
    nameStrings = ['t','z','y','x']
    '''
    Deal with compacted data (TODO: needs testing and is not ideal)
    '''
    if extension=='.gz':    
        fname,extension = os.path.splitext(fname)
        if extension=='.igb':
            with subprocess.Popen(["gzip","-dc",fname+'.igb.gz'],stdout=subprocess.PIPE) as gz:
                hdr = ReadHeader(gz.stdout.read(1024),opened = True)
                data = gz.stdout.read()
            print(hdr)
            dtype = nptype[hdr['type']]
            print(dtype)
            data = np.frombuffer(data,dtype=dtype)

            if 't' in hdr: 
                data = data.reshape((hdr['t'],hdr['z'],hdr['y'],hdr['x']))
                Indexes = list()
                for i,element in enumerate([t,z,y,x]):
                    if element==[0]:
                        Indexes.append([0,hdr[nameStrings[i]]])
                    else:
                        Indexes.append(element)

                data = data[Indexes[0][0]:Indexes[0][1],
                            Indexes[1][0]:Indexes[1][1],
                            Indexes[2][0]:Indexes[2][1],
                            Indexes[3][0]:Indexes[3][1]]
                            
            elif 'z' in hdr:
                data = data.reshape((hdr['z'],hdr['y'],hdr['x']))
                Indexes = list()
                for i,element in enumerate([z,y,x]):
                    if element==[0]:
                        Indexes.append([0,hdr[nameStrings[i+1]]])
                    else:
                        Indexes.append(element)

                data = data[Indexes[0][0]:Indexes[0][1],
                            Indexes[1][0]:Indexes[1][1],
                            Indexes[2][0]:Indexes[2][1]]    

            elif 'y' in hdr: # TODO: Needs testing
                data = data.reshape((hdr['y'],hdr['x']))
                Indexes = list()
                for i,element in enumerate([y,x]):
                    if element==[0]:
                        Indexes.append([0,hdr[nameStrings[i+2]]])
                    else:
                        Indexes.append(element)

                data = data[Indexes[0][0]:Indexes[0][1],
                            Indexes[1][0]:Indexes[1][1]] 

            elif 'x' in hdr:# TODO: Needs testing
                data = data.reshape((hdr['x']))
                Indexes = list()
                if x==[0]:
                    Indexes.append([0,hdr[nameStrings[-1]]])
                else:
                    Indexes.append(element)

                data = data[Indexes[0][0]:Indexes[0][1]]

            else:
                raise RuntimeError('this can\'t happen') 
        else:
            raise RuntimeError('this can\'t happen')    
    
    # Deal with uncompacted data (TODO: needs testing and is not ideal)
    
    elif extension == '.igb':
        
        hdr = ReadHeader(fname_or_file)
        
        dtype = nptype[hdr['type']]
        
        if 't' in hdr: 
            # get indexes of slice, if provided
            indexes,final_shape = IgbLoadIndexes([hdr['t'], hdr['z'], hdr['y'], hdr['x']],
                                                 t,z,y,x)
            if final_shape==[hdr['t'], hdr['z'], hdr['y'], hdr['x']]: # If the whole file was selected:
                fi = open(fname_or_file,'rb')
                data = np.fromfile(fi, dtype,offset = 1024)
                fi.close()        
            else: # If a slice is selected
                data = []
                # Separates slices in blocks for efficient reading
                reading_blocks = EfficientIgbIndexes(indexes)
                for block in reading_blocks:                    
                    fi = open(fname_or_file,'rb')
                    fi.read(1024) # skip header
                    data.append(np.fromfile(fi, dtype,
                                            offset = int(byte_multiplier[dtype]*block[0]),
                                            count = len(block)))
                    fi.close()       
                data = np.concatenate([i for i in data],axis = 0)

            # For both cases
            data = np.asarray(data).reshape(final_shape)
                        
        elif 'z' in hdr:
            # get indexes of slice, if provided
            indexes,final_shape = IgbLoadIndexes([hdr['z'], hdr['y'], hdr['x']],
                                                 z,y,x)
            if final_shape==[hdr['z'], hdr['y'], hdr['x']]:# If the whole file was selected:
                fi = open(fname_or_file,'rb')
                data = np.fromfile(fi, dtype,offset = 1024)
                fi.close()   
            else:          
                data = []
                # Separates slices in blocks for efficient reading
                reading_blocks = EfficientIgbIndexes(indexes)
                for block in reading_blocks:                    
                    fi = open(fname_or_file,'rb')
                    fi.read(1024) # skip header
                    data.append(np.fromfile(fi, dtype,
                                            offset = int(byte_multiplier[dtype]*block[0]),
                                            count = len(block)))
                    fi.close()       
                data = np.concatenate([i for i in data],axis = 0)

            #For both cases
            data = np.asarray(data).reshape(final_shape)
            
        elif 'y' in hdr: # TODO: Needs testing
            # get indexes of slice, if provided
            indexes,final_shape = IgbLoadIndexes([hdr['y'], hdr['x']],
                                                 y,x)
            
            if final_shape==[hdr['y'], hdr['x']]:# If the whole file was selected:
                fi = open(fname_or_file,'rb')
                data = np.fromfile(fi, dtype,offset = 1024)
                fi.close()   
                
            else:          
                data = []
                # Separates slices in blocks for efficient reading
                reading_blocks = EfficientIgbIndexes(indexes)
                
                for block in reading_blocks:                    
                    fi = open(fname_or_file,'rb')
                    fi.read(1024) # skip header
                    data.append(np.fromfile(fi, dtype,
                                            offset = int(byte_multiplier[dtype]*block[0]),
                                            count = len(block)))
                    
                    fi.close()       
                data = np.concatenate([i for i in data],axis = 0)
            #For both cases
            data = np.asarray(data).reshape(final_shape)
            
        elif 'x' in hdr:# TODO: Needs testing
            # get indexes of slice, if provided
            indexes,final_shape = IgbLoadIndexes([hdr['x']],x)
            
            if final_shape==[hdr['x']]:# If the whole file was selected:
                fi = open(fname_or_file,'rb')
                data = np.fromfile(fi, dtype,offset = 1024)
                fi.close()      
            else:          
                data = []
                
                # Separates slices in blocks for efficient reading
                reading_blocks = EfficientIgbIndexes(indexes) 
                
                for block in reading_blocks:                    
                    fi = open(fname_or_file,'rb')
                    fi.read(1024) # skip header
                    data.append(np.fromfile(fi, dtype,
                                            offset = int(byte_multiplier[dtype]*block[0]),
                                            count = len(block)))
                    
                    fi.close()       
                data = np.concatenate([i for i in data],axis = 0)
                
            #For both cases
            data = np.asarray(data).reshape(final_shape)        
        else:
            raise RuntimeError("this can't happen")
          
    data = data.squeeze() # Remove singleton dimensions

    # Fix dimensions if the array contains a signal
    if 'returnRawSignal' in kwargs:
        if kwargs['returnRawSignal']:
            return (data, hdr)
        elif 'facteur' in hdr:
            data = data*np.float(hdr['facteur']) + float(hdr['zero'])
    else:
        if 'facteur' in hdr:
            data = data*np.float(hdr['facteur']) + float(hdr['zero'])

    
    return (data, hdr)
    
def WriteHeader(fname_or_file,header_dict):
  #
  # simple implementation of a header writer, may require improvement
  #

    if type(fname_or_file) in [str, unicode]:
        fo = open(fname_or_file, "wb")
    else:
        fo = fname_or_file
    
    hdrstr = ""
    required_keys = ['x', 'y', 'z', 't', 'type', 'architecture']
    for key in header_dict:
      hdrstr += "%s:%s " % (key, header_dict[key])
    for key in header_dict:
        val = header_dict[key]
        if not key in required_keys:
            hdrstr += "%s:%s " % (key, val)
    # for i in range(16):
    #     hdrstr += " " * 62 + "  "
    # hdrstr = hdrstr.ljust(1023, ' ')
    # s = hdrstr[:1023] + "\f"  # form feed
    hdrstr = hdrstr.ljust(1023," ")
    s = hdrstr + "\f"  # form feed
    fo.write(s.encode())

def Write(fname_or_file,header_dict,matrix):
    '''
    Save matrix to igb file

    Parameters
    ----------
    fname_or_file : str
        File name to save.
    header_dict : dict
        Header to save.
    matrix : 1D to 4D matrix. (optimized for 3 and 4D)
        Matrix to save. Important: the indexes in 0 and -1 will be swapped, so 
        there's no need to do this beforehand

    Returns
    -------
    None.
    ''' 
    
    #Write header
    WriteHeader(fname_or_file,header_dict)
    
    # if 'zero' in header_dict:
    #     matrix = matrix  - header_dict['zero']
    # if 'facteur' in header_dict:
    #     matrix = matrix/np.float(header_dict['facteur'])

    #write IGB file body
    fo = open(fname_or_file, 'a');
    dtype = nptype[header_dict['type']]
    # np.swapaxes(matrix,0,-1).astype(dtype).tofile(fo)  
    axes = np.arange(len(matrix.shape))
    np.moveaxis(matrix,axes,np.flipud(axes)).astype(dtype).tofile(fo)  
    fo.close()

def Idx2Coord(v,matrix_shape):
    '''
    Does the opposite of Coord2Idx.

    Parameters
    ----------
    v : np array
        Linear indexes.
    matrix_shape : tuple, list or array
        Total shape of the file for which the coordinates are being converted.

    Returns
    -------
    Up to 4 coordinates, depending on matrix_shape

    '''
    #Error handling
    if len(matrix_shape)>4:
        print('Unsupported number of dimensions!')
        return -1  
    
    # Actual programming
    if len(matrix_shape)==2:
        xe =  np.asarray(v %  matrix_shape[0],dtype = np.int)
        ye = np.asarray((v /  matrix_shape[0] ),dtype = np.int)
        
        return xe,ye
              
    elif len(matrix_shape)==3:
        xe =  np.asarray(v %  matrix_shape[0],dtype = np.int)
        ye = np.asarray((v /  matrix_shape[0] ) % matrix_shape[1],dtype = np.int)
        ze =  np.asarray(v / ( matrix_shape[0]*matrix_shape[1] ),dtype = np.int)
        
        return xe,ye,ze
    
    elif len(matrix_shape)==4:
        xe =  np.asarray(v %  matrix_shape[0],dtype = np.int)
        ye = np.asarray((v /  matrix_shape[0] ) % matrix_shape[1],dtype = np.int)
        ze =  np.asarray(v / ( matrix_shape[0]*matrix_shape[1] )% matrix_shape[2],dtype = np.int)    
        te =  np.asarray(v / ( matrix_shape[0]*matrix_shape[1]*matrix_shape[2]),dtype = np.int)    
        
        return xe,ye,ze,te

def Coord2Idx(matrix_indexes,matrix_shape):
    '''
    Converts coordinates to linear indexes following the propag logic.
    I have extended the funcionalities to work up to 4D. There's probably a
    smarter way to allow that for N dimensions, but since .igb files go just up
    to 4, I don't see the point

    Parameters
    ----------
    matrix_indexes : (N,2-4)
        Coordinates arranged as a matrix.
    matrix_shape : tuple, list or array
        Total shape of the file for which the coordinates are being converted.

    Returns
    -------
    idx : np array
        Linear indexes.
    '''
    
    # Error handling
    if matrix_indexes.shape[-1]!=len(matrix_shape):
        print('Different lengths!')
        return -1
    if matrix_indexes.shape[-1]>4 or len(matrix_shape)>4:
        print('Unsupported number of dimensions!')
        return -1

    # Actual programming
    if matrix_indexes.shape[-1]==2:
        idx = matrix_indexes[:,0] + \
              matrix_shape[0]* matrix_indexes[:,1] 
              
    elif matrix_indexes.shape[-1]==3:
        idx = matrix_indexes[:,0] + \
              matrix_shape[0]* matrix_indexes[:,1] +\
              matrix_shape[0]*matrix_shape[1]* matrix_indexes[:,2]
    
    elif matrix_indexes.shape[-1]==4:
        idx = matrix_indexes[:,0] + \
              matrix_shape[0]* matrix_indexes[:,1] +\
              matrix_shape[0]*matrix_shape[1]* matrix_indexes[:,2] + \
              matrix_shape[0]*matrix_shape[1]*matrix_shape[2]*matrix_indexes[:,3]    

    return idx 

def GetNodesFromCells(cell,cellTypes,returnBool = True):
    # Find cells with cellTypes and return corresponding nodes
    
    nx,ny,nz= cell.shape
    valid = np.isin(cell,cellTypes)
    cx,cy,cz = np.where(valid)
    if returnBool:
        nodes = np.zeros(shape=(nx+1,ny+1,nz+1),dtype=np.bool) 
    else:
        nodes = np.zeros(shape=(nx+1,ny+1,nz+1),dtype=cell.dtype)
    for i,j,k in product(*[[0,1]]*3):
        if returnBool:
            nodes[cx+i,cy+j,cz+k] = True
        else:
            nodes[cx+i,cy+j,cz+k] = cell[cx,cy,cz]

    return nodes

def GetAtrialSurface(cell,
                     innerTypes = [ 113,114,115,116,117,118,119,145,146 ],
                     outerTypes = [104],
                     linear_indexes = True,
                     mode = 'SP'):
    nx,ny,nz = cell.shape
        
    if mode=='SP':
        '''
        This is the implementation suggested by Simone. It seems to get the right nodes
        but for some weird reason, the signals do not look great. It is like some of the
        signals are not correctly detected, while others are ok. I think it might be getting
        some non-conductive nodes.
        '''
        
        # Nodes from inner cells
        InnerNodes = GetNodesFromCells(cell, innerTypes)
        
        if outerTypes is not None:
            # define nodes with outer cells 
            OuterNodes = GetNodesFromCells(cell,outerTypes) 
        
            # Get the intersection
            SurfaceNodes = np.logical_and(InnerNodes,OuterNodes)
        else:
            SurfaceNodes = InnerNodes

        
        dx,dy,dz = np.where(SurfaceNodes)
    
        
        if linear_indexes: 
            return Coord2Idx(np.array([dx,dy,dz]).T,(nx+1,ny+1,nz+1))

        else:
            return dx,dy,dz
        
    elif mode=='cells':
        '''
        This is my less clever yet effective implementation. I am using this currently
        '''
        
        # define NODES with inner cells (atria)
        valid = np.isin(cell,innerTypes)
        cx,cy,cz = np.where(valid)
        surface_matrix = np.zeros((nx,ny,nz))
        for i in range(len(cx)):
            region = cell[cx[i]-1:cx[i]+2,
                          cy[i]-1:cy[i]+2,
                          cz[i]-1:cz[i]+2]

            if np.in1d(outerTypes, region).any():
                surface_matrix[cx[i],cy[i],cz[i]] = 1
                
        dx,dy,dz = np.where(surface_matrix)
        surface_indexes = Coord2Idx(np.array([dx,dy,dz]).T,(nx,ny,nz))    
        
        if linear_indexes:
            return surface_indexes
        else:
            return dx,dy,dz

#%% Internal functions
def IgbLoadIndexes(dimensions,*args):
    '''
    Converts the intervals provided to the function igbload to indexes for 
    binary files

    Parameters
    ----------
    dimensions : tuple, list, or array
        Total shape of the file for which the coordinates are being converted.
    *args : TYPE
        The coordinate intervals. Can contain an unlimited ammount of intervals,
        but must match |dimensions|.
        If [0], then all the indexes are selected

    Returns
    -------
    indexes : 1D array    
        indexes for binary files
    final_shape : list
        The shape the data will have after reading the binaries.

    '''
    
    
    # Add the intervals in a list
    coordinates = []
    n_complete = 0
    for dim in args:
        coordinates.append(dim)
        if dim[0]==0 and len(dim)==1:
            n_complete +=1
        
    # If all dimensions are complete
    if n_complete==len(dimensions):
        return -1,dimensions
        
    # Transform the intervals into index arrays
    n_indexes = 1
    for i,var in enumerate(coordinates):
        if len(var)==2:
                var = np.arange(var[0],var[1])
        elif var[0]==0:
            var = np.arange(dimensions[i])
            
        coordinates[i] = var
        n_indexes *= len(var)
    
    #    Translates the coordinates to indexes
    '''
    IMPORTANT: The dimensions are (x,y,z) when using Coord2Idx BUT
    when reading from .igb files, they are inverted: (z,y,x)
    This means that I need to flip the coordinates to calculate the indexes
    '''
    grid_coordinates =  np.array(list(product(*coordinates)))
    
    indexes = Coord2Idx(np.fliplr(grid_coordinates),np.flipud(dimensions))
        
    final_shape = [len(i) for i in coordinates]
    
    return indexes,final_shape

def EfficientIgbIndexes(indexes):
    '''
    Groups the unordered indexes obtained in IgbLoadIndexes into subsequential 
    indexes in order to improve computation
    
    Parameters
    ----------
    indexes : array
        Disorganized indexes.
    
    Returns
    -------
    sorted_blocks : list of lists
        Blocks of organized indexes.
    '''
    
    sorted_inds = np.sort(indexes)
    sorted_blocks = []
    
    i=0
    while i!=len(sorted_inds)-1:
        temp_list = []
        while True:
            temp_list.append(sorted_inds[i])
            # Reached end
            if i==len(sorted_inds)-1:
                break
            
            #Reached end of block
            if (sorted_inds[i+1]!=sorted_inds[i]+1):
                i += 1
                break
            else:
                i += 1
                
        sorted_blocks.append(temp_list)

    return sorted_blocks

def GetAnatomyIdx(FullCell,validCells=[50,51]):
    FullCell = FullCell.squeeze()
    zz,yy,xx = np.where(np.isin(FullCell,validCells))
    Anatomy = np.array([xx,yy,zz]).T
    return Anatomy

#%% Distances over surface
def MakeAdjacencyMatrix(tissuePatch,SpatialResolution):
    '''
    Generates the adjacency matrix for a cloud of points obtained from an igb

    Parameters
    ----------
    tissuePatch : N,3 np array
        Points of the tissue patch (3D but could be 2D).
    spatialResolution : float, optional
        The spatial resolution of the igb. The default is 1.

    Returns
    -------
    AdjacencyMatrix : TYPE
        DESCRIPTION.

    '''

    Distances = pdist(tissuePatch, 'euclidean').astype(np.int16)
    Distances[Distances<=SpatialResolution*np.sqrt(2)] = -1
    Distances[Distances>SpatialResolution*np.sqrt(2)] = 0
    Distances = -Distances.astype(np.int16)

    AdjacencyMatrix = squareform(Distances)


    return AdjacencyMatrix
def GeodesicDistances(Source,AdjacencyMatrix,EdgeWeight = 1):

    Distances = np.ones(AdjacencyMatrix.shape[0])*np.nan
    Distances[Source] = 0

    Vertex = Source
    InSet = np.ones_like(Distances);   

    Visited = []

    while True:
        #Include u to sptSet.
        Visited.append(Vertex)

        #Update distance value of all adjacent vertices of u. 
        #To update the distance values, iterate through all adjacent vertices. 
        #For every adjacent vertex v, if the sum of a distance value of u (from source) 
        #and weight of edge u-v, is less than the distance value of v, then update the distance value of v. 

        Neighbors = np.where(AdjacencyMatrix[Vertex,:]==1)[0]
        for i,v in enumerate(Neighbors):
            if Distances[Vertex]+EdgeWeight<Distances[v] or np.isnan(Distances[v]):
                Distances[v] = Distances[Vertex]+EdgeWeight

        InSet[Vertex] = np.nan
        if np.nansum(InSet)==0:
            break

        #check minimum distance
        temp = np.where(~np.isnan(Distances)*(~np.isnan(InSet)))[0]

        # Check if in set        
        try:
            Vertex = temp[np.where(Distances[temp]==np.nanmin(Distances[temp]))[0][0]]    
        except:
            Vertex = np.where(~np.isnan(InSet))[0][0]
            
    return Distances

#%% Quick functions to handle different igbs

def LoadCellFile(anatomyPath,modelName,validCells = [50,51]):
    # Load cell file
    # ValidCells: 50 - endo ; 51 - both; 52 - epi
    try:
        FnameCell = os.path.join(anatomyPath,modelName+'-atrial-surface-n.igb')
        FullCell,_ = Load(FnameCell)
    except:
        FnameCell = os.path.join(anatomyPath,modelName+'-atrial-surface-n.igb.gz')
        FullCell,_ = Load(FnameCell)
    FullCell = FullCell.squeeze()
    return FullCell   

def GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51]):
    # Returns the anatomy and the corresponding data indexes

    fi = open(os.path.join(anatomyPath,modelName+'-atrial-surface.save'))
    originalIndexes = np.loadtxt(fi,dtype = int,skiprows = 1,comments = [';',',']) #,delimiter = '\n'
    #
    # Get the cell types for selecting the layer
    fi = open(os.path.join(anatomyPath,modelName+'-atrial-surface.save'))
    buf = fi.read()
    buf = buf.split(', ')
    # to account for the last one as ;
    buf.append('')
    buf[-1] = buf[-2].split('; ')[-1]
    buf[-2] = buf[-2].split('; ')[0]
    cmm = [l.strip()[1:1+2] for l in buf if l.startswith("#")]
    types = np.asarray(cmm,dtype = int)
    # Filter the cell types
    veIndexes = np.where(np.isin(types,validCells))[0]
    anatomyIndexes = originalIndexes[veIndexes]
    
    # Make Anatomy file
    nz,ny,nx = FullCell.shape
    # #
    # vtkCellIndexes = Coord2Idx(Anatomy,(nx,ny,nz))
    ax,ay,az = Idx2Coord(anatomyIndexes,(nx,ny,nz))
    Anatomy = np.array([ax,ay,az]).T

    return Anatomy,veIndexes

def GetAnatomicalInds(filepath,fullShape,anatomy,validCells = [50,51]):
    '''
    This function actually just does the work of getting anatomy indexes
    based on lists I created earlier. Maybe it should not be in this library
    '''

    interAtrialConnections = np.loadtxt(os.path.join(filepath,'atrialConnections.list'),dtype = int) # to exclude if a focal activity is found


    CSInds = np.load(os.path.join(filepath,'CSIdx_Endo.npy'))

    LAInds = np.loadtxt(os.path.join(filepath,'LAPoints.list'),dtype = int)
    LAInds = LAInds[np.isin(LAInds[:,1],validCells),0]
    lx,ly,lz = Idx2Coord(LAInds,fullShape)
    LAInds = np.array([np.where((anatomy[:,0]==lx[i]) & \
                            (anatomy[:,1]==ly[i]) & \
                            (anatomy[:,2]==lz[i]))[0][0]\
                                for i in range(len(lx))])

    RAInds = np.loadtxt(os.path.join(filepath,'RAPoints.list'),dtype = int)
    RAInds = RAInds[np.isin(RAInds[:,1],validCells),0]
    rx,ry,rz = Idx2Coord(RAInds,fullShape)
    RAInds = np.array([np.where((anatomy[:,0]==rx[i]) & \
                            (anatomy[:,1]==ry[i]) & \
                            (anatomy[:,2]==rz[i]))[0][0]\
                                for i in range(len(rx))])
    
    return interAtrialConnections,CSInds,LAInds,RAInds