import numpy as np
from scipy.spatial.distance import cdist,pdist,squareform
from scipy.interpolate import CubicSpline #splprep, splev,splder,BSpline,
from scipy.linalg import svd  
import egm_processing as egmp

'''
This library contains four classes that can be used to connect wavefronts in EGM data (or any data with activation times and
a conduction velocity). 

>> WavemapCalculator
This class discriminate waves from a set of electrode positions and activation times

>> WavemapAnalyzer
This class evaluates each of the waves detected by WavemapCalculator. Follow "WavemapAnalyzer.ComputeAllAnalyses" to 
see the order of the functions

>> Wave
This class has quick functions to create graphs for waves and connect trajectories

>> Graph
This class has some useful graph functions not implemented in native libraries
'''

class WavemapCalculator:

    def __init__(self):
        self.ConductionThreshold = 0.2
        self.DiscontinuousConductionThreshold = 0.05
        self.NeighborRadius = np.sqrt(2.5**2 + 3.5**2)
        self.MergeSearchRadius = 2 * self.NeighborRadius
        self.MergePhaseShiftThreshold = 1 / self.ConductionThreshold
        self.MergeOverlapThreshold = 0.75
        
        self.WaveMerging = False
        self.StartPointElimination = False 
        self.WaveConductionLikelihoodImprovement = True
        
        self.DetectionMethodIndex = 2

        self.CatheterObject=None
        self.NeighborList = None # for enforcing templates

    def GetElectrodeNeighbors(self,electrodePositions, electrodeActivations, electrodeIndex, radius,
                              electrodeNeighborList = None):
        
        electrodePosition = electrodePositions[electrodeIndex, :]
        positionDifferences = electrodePositions - electrodePosition
        electrodeDistances = np.sqrt(positionDifferences[:,0]**2 + positionDifferences[:,1]**2+ positionDifferences[:,2]**2) #TODO should be fully 3D

        if electrodeNeighborList is None:
            validNeigbors = electrodeDistances <= radius
            neighbors = {'positions': np.where(validNeigbors)[0],
                        'activations': np.array(electrodeActivations,dtype = 'object')[validNeigbors], #
                        'distances': electrodeDistances[validNeigbors]}
        else:
            neighbors = {'positions': electrodeNeighborList[electrodeIndex],
                         'activations': np.array(electrodeActivations,dtype = 'object')[electrodeNeighborList[electrodeIndex]], #
                         'distances': electrodeDistances[electrodeNeighborList[electrodeIndex]]}
        return neighbors
    
    def FindFirstActivationIndexAfterCurrent(self,activations, currentActivation):
        position = np.where(activations >= currentActivation)[0]

        if len(position)==0:
            activation = np.nan
            activationIndex = np.nan
        else:
            activation = activations[position[0]]
            activationIndex = position[0]
        
        return activation, activationIndex

    def DetectConnectedActivations(self,catheterObj,activationGroups = None):
        '''
        Groups activations based on the given CV threshold 
        '''

        # Save catheter object and its activations to class. Get data dimensions
        self.CatheterObject = catheterObj
        self.ElectrodeActivations = self.CatheterObject.Activations
        numberOfActivationsPerElectrode = np.array([len(act) for act in self.ElectrodeActivations])
        numberOfActivations = numberOfActivationsPerElectrode.sum()
        numberOfElectrodes = self.CatheterObject.GetNumberOfChannels()
        electrodePositions = self.CatheterObject.ElectrodeCoordinates 
        maximumNumberOfConnections = 9 * numberOfActivations
        
        '''
        As an option, the user can give activation groups based on other methods
        In this case, the activation info is just placed in the correct variables. 
        Otherwise, everything is calculated below
        '''

        if activationGroups is None:

            ### Put the activation info in a single matrix with electrode code, activation code and activation time
            activationData = np.zeros((numberOfActivations, 3))*np.nan
            electrodeActivationStartIndex = np.cumsum(numberOfActivationsPerElectrode) - numberOfActivationsPerElectrode
            electrodeIndices = [] 
            activationIndices = [] 
            for electrodeIndex  in range(numberOfElectrodes):
                electrodeIndices.append(electrodeIndex * np.ones((numberOfActivationsPerElectrode[electrodeIndex])))
                activationIndices.append(np.arange(numberOfActivationsPerElectrode[electrodeIndex]).T)

            activationData[:, 0] = np.hstack([elecInd for elecInd in electrodeIndices])
            activationData[:, 1] = np.hstack([actInd for actInd in activationIndices])
            activationData[:, 2] = np.array([elem for act in self.ElectrodeActivations for elem in act ])

            ### For each of the electrode/activation/activation time groups, get neighbors and check
            # if they belong to the same waves based on the CV threshold 
             
            electrodeConductionIndices = []
            allElectrodeActivations = self.ElectrodeActivations
            neighborRadius = self.NeighborRadius 
            conductionThreshold = self.ConductionThreshold
            
            if self.NeighborList is None:
                NeighborList = []
            for electrodeIndex in range(numberOfElectrodes):

                ## Get neighbors for each electrode
                neighbors = self.GetElectrodeNeighbors(electrodePositions,
                                                       allElectrodeActivations,
                                                       electrodeIndex,
                                                       neighborRadius,
                                                       electrodeNeighborList=self.NeighborList)

                if self.NeighborList is None:
                    NeighborList.append(neighbors['positions'])

                electrodeActivations = allElectrodeActivations[electrodeIndex]
                currentElectrodeConductionIndices = []

                ## For each activation of that electrode, check neighbors who have valid CV differences
                for activationIndex in range(len(electrodeActivations)):
                    activationGlobalIndex = electrodeActivationStartIndex[electrodeIndex] + activationIndex

                    neighborActivations = list()
                    activationIndices = list()
                    for activation in neighbors['activations']:
                        nA,aI = self.FindFirstActivationIndexAfterCurrent(activation, electrodeActivations[activationIndex])
                        neighborActivations.append(nA)
                        activationIndices.append(aI)

                    neighborActivations = np.asarray(neighborActivations)
                    activationIndices = np.asarray(activationIndices)
                    
                    # Calculate CV to neighbors
                    neighborConduction = neighbors['distances']/(neighborActivations - electrodeActivations[activationIndex])
                    validNeighbors = (neighborConduction >= conductionThreshold)

                    ### Set the output, consisting of the global activation indices, the activation index of that electrode (in
                    # space and time) and their activation indices

                    outArray = np.hstack([activationGlobalIndex * np.ones((np.sum(validNeighbors),1)),
                                        electrodeActivationStartIndex[np.array(neighbors['positions'])[validNeighbors]].reshape(-1,1)+\
                                        activationIndices[validNeighbors].reshape(-1,1)]) 

                    for element in outArray: 
                        currentElectrodeConductionIndices.append(element.astype(int))

                electrodeConductionIndices.append(np.asarray(currentElectrodeConductionIndices,dtype=int))

            if self.NeighborList is None: self.NeighborList = NeighborList
            
            # The matrix below indicates the position of edges in the subsequent graph from which waves are extracted
            electrodeConductionIndices = np.array([row for element in electrodeConductionIndices for row in element ])
            
            ### Determine connected components in the graph as waves
            ConnectedWaves = Graph(electrodeConductionIndices,numberOfActivations,connectionType='weak',connect = True)
            components = ConnectedWaves.ConnectedGroups

            numberOfComponents = len(components)
            activationComponentIndex = np.zeros(numberOfActivations,dtype=int)
            for componentIndex in range(numberOfComponents):
                activationComponentIndex[components[componentIndex]] = componentIndex

            # Make a final output dictionary
            detectedWaves = {} 
            for componentIndex in range(numberOfComponents):
                componentMembers = (activationComponentIndex == componentIndex)

                waveMembers = activationData[componentMembers, :].astype(int)
                detectedWaves[componentIndex] = Wave(componentIndex,electrodePositions,self.CatheterObject.ElectrodesTemplate,self.NeighborList)
                detectedWaves[componentIndex].Members = waveMembers
                detectedWaves[componentIndex].ConnectivityMatrix = ConnectedWaves.ConnectivityMatrix[componentMembers,:][:,componentMembers]#[componentMembers,componentMembers]
        else:
            # If activation groups are provided, meaning they were calculated by other methods, than skip the first graph part
            detectedWaves = {} 
            k = 0
            for componentIndex,activationTimes in activationGroups.items():
                numberOfMembers = np.sum(activationTimes!=-1)
                
                waveMembers = np.zeros((numberOfMembers,3),dtype=int)
                waveMembers[:,0] = np.where(activationTimes!=-1)[0]
                waveMembers[:,1] = np.ones(numberOfMembers)*componentIndex
                waveMembers[:,2] = activationTimes[activationTimes!=-1]

                detectedWaves[k] = Wave(componentIndex,electrodePositions,self.CatheterObject.ElectrodesTemplate,self.NeighborList)
                detectedWaves[k].Members = waveMembers
                k +=1

            # # I could make surrogate for these but why bother right now
            # activationData = None
            # activationComponentIndex = None
        
        return detectedWaves #, activationData, activationComponentIndex

    def RemoveSmallWaves(self,detectedWaves,minimumWaveSize = 8):
        ''' 
        Remove waves smaller than minimumWaveSize
        Also remove waves with all the same activation time
        '''
        k = 0
        poplist = []
        for waveIndex,wave in detectedWaves.items():
            uniqueVals = len(np.unique(wave.Members[:,-1]))
            if len(wave.Members)<minimumWaveSize or uniqueVals==1: 
                poplist.append(waveIndex)
                continue
        for element in poplist: detectedWaves.pop(element)

        return detectedWaves

class WavemapAnalyzer:

    def __init__(self,waveGroup,conductionThreshold,radius = np.inf,minimumDuration=0):
        self.Waves = waveGroup # output of WavemapCalculator
        self.ConductionThreshold = conductionThreshold
        self.Radius = radius
        self.MinimumDuration = minimumDuration
        # Things computed by the functions
        self.WaveStartingIndices = None
        self.WaveEndIndices = None
        self.Trajectories = None

        # These two are used for focal sources 
        self.MinMaxAngularDifferences = None
        self.StartEndAngles = None

        self.TrajectoryCurvatures = None
        self.TrajectoryPositions = None

        self.CurvatureScores = None

    def DetectStartAndEndBlocks(self):
        ''' For each wave, define isolated starting and ending points
            Group start/end points if neighboring each other'''

        self.WaveStartingIndices = {}
        self.WaveEndIndices = {}
        for waveIndex,wave in self.Waves.items():
            # Prepare some wave information
            wave.GetSize()
            wave.SetWaveGraph(self.ConductionThreshold,self.Radius)
            

            # Isolated starting regions
            _,startingPointIndices = wave.ComputeAllStartingPoints()
            numberOfStartingPoints = len(startingPointIndices)

            # Group adjacent starting points
            startConnections = wave.WaveGraph.ConnectivityMatrix[startingPointIndices,:][:,startingPointIndices]
            startConnectionIndices = np.vstack(np.where(startConnections)).T
            startGraph = Graph(startConnectionIndices,len(startConnections),connectionType='weak')
            startGroups = startGraph.GetAllConnectedGroups()

            for i,element in enumerate(startGroups):
                startGroups[i] = startingPointIndices[element]
            self.WaveStartingIndices[waveIndex] = startGroups 

            # Isolated ending regions
            _,endPointIndices = wave.ComputeAllEndPoints()
            numberOfEndPoints = len(endPointIndices)

            # Group adjacent end points
            endConnections = wave.WaveGraph.ConnectivityMatrix[endPointIndices,:][:,endPointIndices]
            endConnectionIndices = np.vstack(np.where(endConnections)).T
            endGraph = Graph(endConnectionIndices,len(endConnections),connectionType='weak')
            endGroups = endGraph.GetAllConnectedGroups()
            
            for i,element in enumerate(endGroups):
                endGroups[i] = endPointIndices[element]
            self.WaveEndIndices[waveIndex] = endGroups

    def DetectTrajectories(self,filter = True,selection = 'shortest'):
        '''
        The self.Trajectory is a dict with entries for each wave
        Each wave entry is an object array in which the columns represent:
            - 0: starting points
            - 1: end points
            - 2: paths
        '''

        self.Trajectories = {}
        poplist = []
        for waveIndex,wave in self.Waves.items():

            trajectories = [] 
            
            conductionMatrix,distanceMatrix = wave.LocalConduction(useConnectivityMatrix=False) #FIXME

            conductionMatrix[conductionMatrix<self.ConductionThreshold] = np.nan # If value is not valid CV, just remove
            conductionMatrix[np.isinf(conductionMatrix)] = np.nan # If two electrodes are activated at the same time, don't connect them

            
            # The weights for wave connection in the graph is the inverse of conduction velocity
            # This way, the shortest path is the one with quickest activation intervals 
            graphWeights = 1 / conductionMatrix   

            trajectories = np.zeros((0,3),dtype = object)
            for currentStartIndices in self.WaveStartingIndices[waveIndex]:
                for currentEndPointIndices in self.WaveEndIndices[waveIndex]:
                    startTime = wave.Members[currentStartIndices,2]
                    endTime = wave.Members[currentEndPointIndices,2]
                    disconnectedPoints = False
                    if (endTime.max() - startTime.min()) >= self.MinimumDuration:
                        for currentStartIndex in currentStartIndices:
                            for currentEndIndex in currentEndPointIndices:

                                #TODO: THERE IS AN ISSUE HERE. Multiple trajectories are being ignored when present
                                    #Note on 10/06/2022: I have no clue if this has been solved, have to investigate

                                # This is possibly the reason why many focal point detections are failing!
                                # The solution: for each point in an endpoint group, calculate path. Get the shortest
                                # To represent the group
                                path,distance = wave.GetShortestPath(graphWeights,
                                                                    currentStartIndex,currentEndIndex)



                                if np.isinf(distance):
                                    disconnectedPoints=True
                                    continue
                                if len(path)>1:
                                    trajectories = np.vstack([trajectories,
                                                            np.array([currentStartIndex,currentEndIndex,path],
                                                            dtype=object)])


            if len(trajectories)!=0:
                self.Trajectories[waveIndex] = trajectories
            else:
                poplist.append(waveIndex)
        
        for element in poplist:
            self.Waves.pop(element)

        if filter:
            self.FilterTrajectories(selection)

        self.GetTrajectoryPositions()
    
    def ComputeTrajectoryCurvatures(self):
        '''
        Gets the trajectory dictionaries and computes the curvature 
        of each segment
        '''

        self.TrajectoryCurvatures = {}

        if self.Trajectories is None:
            RuntimeWarning('Trajectories not provided, calculating with default options')
            self.DetectTrajectories()

        for waveIndex,wave in self.Waves.items():
            allElectrodePositions = self.GetPositionsInPlane(wave)
            
            self.TrajectoryCurvatures[waveIndex] = list()# This is the output

            waveStartIndices = self.WaveStartingIndices[waveIndex]
            waveEndIndices =  self.WaveEndIndices[waveIndex]
            waveTrajectories = self.Trajectories[waveIndex] # list

            for trajectoryIndex,trajectory in enumerate(waveTrajectories):
                trajectory = list(trajectory[2])
                if len(trajectory)>=3:
                    # Add groups of start/end points to the corresponding trajectories
                    startingPoints = self.AdjustLimitPosition(waveStartIndices,[trajectory[0]])
                    trajectory[0] = startingPoints
                    endPoints = self.AdjustLimitPosition(waveEndIndices,[trajectory[-1]])
                    trajectory[-1] = endPoints
                    
                    trajectoryPositions = np.zeros((len(trajectory),2))
                    for nodeIndex,node in enumerate(trajectory):
                        member = wave.Members[node, 0]
                        memberPosition = allElectrodePositions[member,:]
                        if len(memberPosition.shape)>1:
                            memberPosition = np.mean(memberPosition,axis=0)
                        trajectoryPositions[nodeIndex,:] = memberPosition
                    
                    # Calculate the changes in angle from segment to segment in the curvature
                    curvature = np.zeros(len(trajectoryPositions)-2)
                    for i in range(len(trajectoryPositions)-2):
                        A = trajectoryPositions[i,:]
                        B = trajectoryPositions[i+1,:]
                        C = trajectoryPositions[i+2,:]
                        
                        S1 = B-A
                        S2 = C-B
                        curvature[i] = np.sign(np.cross(S1,S2))*np.arccos(np.dot(S1/np.linalg.norm(S1),S2/np.linalg.norm(S2)))
                        
                else:
                    curvature = None


                self.TrajectoryCurvatures[waveIndex].append(curvature) 

    def ComputeCurvatureScore(self):
        '''
        Computes the curvature score for each trajectory
        Indicates how much a trajectory is curved in the same direction
        '''
        self.CurvatureScores = dict()
        for waveIndex,trajectoryCurvatures in self.TrajectoryCurvatures.items():
            waveCurvatureScores = []
            for trajectoryIndex,curvature in enumerate(trajectoryCurvatures):
                if curvature is None: 
                    waveCurvatureScores.append(0.)
                    continue
    
                rotationScore = np.nansum(curvature)/(2*np.pi) # previously: sum
                waveCurvatureScores.append(rotationScore)
            self.CurvatureScores[waveIndex] = waveCurvatureScores

    def ComputeMaxDeviationAndArea(self):
        self.MaxDeviationLine = {}
        self.MaxDeviationLineNorm = {}
        self.TrajAreas = {}
        self.TrajAreasNorm = {}

        for waveIndex,wave in self.Waves.items():
            maxDeviation = []
            normMaxDeviation = []
            area = []
            normArea = []
            for trajectoryIndex,trajectory in enumerate(self.Trajectories[waveIndex]):
                if len(trajectory[2])<3: continue
                trajectoryMembers = wave.Members[trajectory[2],0]
                planePositions = self.GetPositionsInPlane(wave)[:,:2]

                trajectoryPositions2D = planePositions[trajectoryMembers]

                # A and B form the line
                A = trajectoryPositions2D[0]
                B = trajectoryPositions2D[-1]
                otherPoints = trajectoryPositions2D[1:-1]

                # Directional part
                pointVectors = otherPoints-A
                baseline = B-A

                distanceToLine = np.abs((B[0]-A[0])*(A[1]-otherPoints[:,1])\
                                        -(A[0]-otherPoints[:,0])*(B[1]-A[1]))/\
                                        np.linalg.norm(baseline)

                distanceToLine = distanceToLine#/np.linalg.norm(baseline)
                direction = np.sign(np.cross(pointVectors,baseline))
                Area = np.abs(np.sum(distanceToLine*direction))

                # put in list
                maxDeviation.append(np.max(distanceToLine))
                area.append(Area)
                normMaxDeviation.append(np.max(distanceToLine)/np.linalg.norm(baseline))
                normArea.append(Area/np.linalg.norm(baseline))
            
            self.MaxDeviationLine[waveIndex] = maxDeviation
            self.MaxDeviationLineNorm[waveIndex] = normMaxDeviation
            self.TrajAreas[waveIndex] = area
            self.TrajAreasNorm[waveIndex] = normArea

        return 0#MaxDeviationLine,MaxDeviationLineNorm,TrajAreas,TrajAreasNorm

    def CalculateWaveAngularDifferences(self):

        # Calculate the largest and the smallest angular difference between trajectories
        if self.TrajectoryPositions is None:
            print(RuntimeWarning('Trajectory positions not provided, calculating...'))
            self.ComputeTrajectoryCurvatures()
        self.MinMaxAngularDifferences = {}
        self.StartEndAngles = {}

        for waveIndex,wave in self.Waves.items():

            Rn = self.GetPositionsInPlane(wave,returnTransformMatrix=True)

            planeTrajectories = list()
            if len(self.TrajectoryPositions[waveIndex])>1:
                for trajectory in self.TrajectoryPositions[waveIndex]:
                    # Get trajectory projection
                    Pos = np.copy(trajectory)
                    Pos -= wave.ElectrodePositions.mean(axis=0)
                    Pos = np.matmul(Rn,Pos.T).T
                    Pos = Pos[:,:2]
                    if len(Pos)>1:
                        planeTrajectories.append(Pos)


                trajAxis = np.zeros((len(planeTrajectories),2))
                for i,trajectory in enumerate(planeTrajectories):
                    trajAxis[i,:] = trajectory[-1,:]-trajectory[0,:]       
                
                angles = np.arccos(1-pdist(trajAxis,'cosine'))
                angles = squareform(angles)
                angles[angles==0] = np.nan #zero difference is not interesting

                # if waveIndex==5: return angles
                # Only take into account angles between trajectories starting at the same point
                ## Get the starting point of each trajectory in the wave
                starts = self.Trajectories[waveIndex]
                starts = [starts[i][0] for i in range(len(starts))] 

                tmpMin = []
                tmpMax = []
                for start in np.unique(starts):
                    inds = np.where(starts==start)[0]
                    tmpAngles = angles[inds,:][:,inds]

                    tmpMin.append(np.nanmin(tmpAngles))
                    tmpMax.append(np.nanmax(tmpAngles))


                self.MinMaxAngularDifferences[waveIndex] = [np.min(tmpMin),np.max(tmpMax)]
                self.StartEndAngles[waveIndex] = angles
            else:
                self.MinMaxAngularDifferences[waveIndex] = [np.nan,np.nan]
                self.StartEndAngles[waveIndex] = None

    def GetTrajectoryPositions(self):
        # Mostly for plotting
        self.TrajectoryPositions = {}
        for waveIndex,wave in self.Waves.items():
            self.TrajectoryPositions[waveIndex] = list()
            
            waveTrajectories = self.Trajectories[waveIndex] # list

            for trajectoryIndex,trajectory in enumerate(waveTrajectories):
                trajectory = trajectory[2]
                trajectoryPosition = np.zeros((len(trajectory),3))

                for i,element in enumerate(trajectory):
                    element = wave.Members[element,0]
                    # Adjust start and end positions
                    if i==0:
                        element = self.AdjustLimitPosition(self.WaveStartingIndices[waveIndex],[element])
                    if i== len(trajectory): 
                        element = self.AdjustLimitPosition(self.WaveEndIndices[waveIndex],[element])

                    elecInds = np.where(np.isin(wave.Members[:,0],element))[0]
                    electrodePos = wave.ElectrodePositions[wave.Members[elecInds,0],:]

                    # Get average if multiple points
                    if len(electrodePos.shape)>1: electrodePos =np.mean(electrodePos,axis=0)
                    trajectoryPosition[i,:] = electrodePos
                self.TrajectoryPositions[waveIndex].append(trajectoryPosition)

    def PlotWaveAndTrajectories(self,waveIndex,type='2d'):
        import matplotlib.pyplot as plt
        wave = self.Waves[waveIndex]

        plotActivations = np.zeros(16)*np.nan
        plotActivations[wave.Members[:,0]] = wave.Members[:,2] 

        if type=='2d':
            Trajectories = self.Trajectories[waveIndex]

            Pos = wave.ElectrodeTemplate
            xx = np.unique(Pos[:,0])
            yy = np.unique(Pos[:,1])

            fig,ax = plt.subplots(1)
            ax.pcolormesh(yy,xx,plotActivations.reshape(4,4).T,shading='nearest')

            plotColors = ['k','m','c','y','r','b','g']
            for tt,trajectory in enumerate(Trajectories):
                trajectory = trajectory[2]
                trajectoryPos = wave.Members[trajectory,0]
                ax.plot(Pos[trajectoryPos[1:-1],1],Pos[trajectoryPos[1:-1],0],'--',color = plotColors[tt])

                # Adjust starting and ending points with >1 point
                # Starting
                sP = [trajectory[0] in sP for sP in self.WaveStartingIndices[waveIndex]]
                sP = np.where(sP)[0][0]
                startingGroup = self.WaveStartingIndices[waveIndex][sP]
                if len(startingGroup)>1:
                    startPos = wave.Members[startingGroup,0]
                    startPos = np.median(Pos[startPos],axis=0)
                else:
                    startPos = Pos[trajectoryPos[0]]
                ax.plot(startPos[1],startPos[0],'o',color = plotColors[tt],markersize=30,markerfacecolor='none')
                ax.plot([startPos[1],Pos[trajectoryPos[1],1]],
                        [startPos[0],Pos[trajectoryPos[1],0]],'--',color = plotColors[tt])
                
                #End
                eP = [trajectory[-1] in eP for eP in self.WaveEndIndices[waveIndex]]
                eP = np.where(eP)[0][0]
                endGroup = self.WaveEndIndices[waveIndex][eP]
                if len(endGroup)>1:
                    endPos = wave.Members[endGroup,0]
                    endPos = np.median(Pos[endPos],axis=0)
                else:
                    endPos = Pos[trajectoryPos[-1]]
                ax.plot(endPos[1],endPos[0],'x',color = plotColors[tt],markersize=30)
                ax.plot([Pos[trajectoryPos[-2],1],endPos[1]],
                        [Pos[trajectoryPos[-2],0],endPos[0]],'--',color = plotColors[tt])


        elif type=='3d':
            Trajectories = self.TrajectoryPositions[waveIndex]
 
            Pos = wave.ElectrodePositions
            xx = Pos[:,0]
            yy = Pos[:,1]
            zz = Pos[:,2]

            fig = plt.figure()
            ax = plt.subplot(projection='3d')
            ax.scatter(xx,yy,zz,c = plotActivations,s=300,alpha=1)
            ax.scatter(xx[0],yy[0],zz[0],marker = 'x',color='r',s=300,alpha=1)
            ax.scatter(xx[3],yy[3],zz[3],marker = 'x',color='y',s=300,alpha=1)


            plotColors = ['k','m','c','y','r']
            for tt,trajectory in enumerate(Trajectories):
                ax.plot(trajectory[:,0],trajectory[:,1],trajectory[:,2],'--',color = plotColors[tt])
                ax.plot(trajectory[0,0],trajectory[0,1],trajectory[0,2],'o',color = plotColors[tt],markersize=30,markerfacecolor='none')
                ax.plot(trajectory[-1,0],trajectory[-1,1],trajectory[-1,2],'x',color = plotColors[tt],markersize=30)

    def ComputeAllAnalyses(self,filter=True,selection='shortest',centralTendencyFunction = 'mean'):
        # Get Trajectories from the individual waves
        self.DetectStartAndEndBlocks()
        self.DetectTrajectories(filter,selection)
        # Analyze the trajectories
        self.ComputeTrajectoryCurvatures()
        self.ComputeCurvatureScore()
        self.CalculateWaveAngularDifferences()
        
    def ComputeWaveSizes(self):
        waveSizes = list()
        for waveIndex,wave in self.Waves.items():
            waveSizes.append(len(wave.Members))
        return np.array(waveSizes) 
    
    #%% Internal functions
    def AdjustLimitPosition(self,limitPoints,trajectoryPoints):
        '''
        Merge starting or ending points that are adjacent to be considered
        as a single origin.

        While trajectories are computed from the individual nodes, for graph
        convenience, we need to analyze them as a group when the starting 
        or ending points belong to the same group
        '''

        for startingPoint in limitPoints:
            if set(startingPoint)-set(trajectoryPoints) != set(startingPoint):
                trajectoryPoints = np.hstack([startingPoint,trajectoryPoints])
                trajectoryPoints = np.unique(trajectoryPoints)
        return trajectoryPoints

    def FilterTrajectories(self,selection = 'shortest'):
        '''
        In situations where multiple trajectories were calculated for starting
        or ending points with >1 point, we need to choose the optimal trajectory

        This function selects either the shortest or longest of the trajectories
        as the definitive one
        '''

        for waveIndex,wave in self.Waves.items():
            trajectory = self.Trajectories[waveIndex]
            startingPoints = self.WaveStartingIndices[waveIndex]
            endPoints = self.WaveEndIndices[waveIndex]
            selectedTrajectories = np.zeros(len(trajectory),bool)
            for currentStartingPoints in startingPoints:
                startIndices = np.isin(trajectory[:,0],currentStartingPoints)
                for currentEndPoints in endPoints:
                    endIndices = np.isin(trajectory[:,1],currentEndPoints)
                    currentTrajectory = trajectory[startIndices & endIndices,:]

                    # If only a single trajectory, skip the filtering
                    if len(currentTrajectory)==1:
                        selectedTrajectories[startIndices & endIndices] = True
                    else:
                        # If there are multiple trajectories, select the shortest
                        trajectoryLengths = [len(T) for T in currentTrajectory[:,2]]

                        if len(trajectoryLengths)==0: continue # short trajectory

                        if selection=='shortest':
                            selectedTrajectory = np.where(trajectoryLengths==np.min(trajectoryLengths))[0]
                        elif selection=='longest':
                            selectedTrajectory = np.where(trajectoryLengths==np.max(trajectoryLengths))[0]
                            
                        selectedTrajectory = np.random.choice(selectedTrajectory) # random if same length

                        trajIndex = np.where(startIndices & endIndices)[0][selectedTrajectory]
                        selectedTrajectories[trajIndex] = True
            self.Trajectories[waveIndex] = trajectory[selectedTrajectories]

    def GetPositionsInPlane(self,wave,returnTransformMatrix=False):
        electrodePositionsOriginal = wave.ElectrodePositions
        electrodePositionsC = electrodePositionsOriginal-electrodePositionsOriginal.mean(axis=0)

        U,s,V = svd(electrodePositionsC)
        u = V.T[:,0] 
        v = V.T[:,1] 
        n = V.T[:,2]   
        Rn =  egmp.MakeRotationalMatrix(n,np.array([0,0,1])) #  Create rotation matrix to align n with z axis
        electrodePositions = np.matmul(Rn,electrodePositionsC.T).T 
        electrodePositions = electrodePositions[:,:2]
        if returnTransformMatrix:
            return Rn
        else:
            return electrodePositions

class WaveClassifier():
    def __init__(self,wAnalyzer,centralTendencyFunction = 'mean',
                reentryThresholds = {'MaxCurvatureScores':0.35,'CLCoverage':0.1},
                focalThresholds = {'NumberOfTrajectories':2,'MinAngles':0.314,
                                    'MaxAngles':1.257,'CLCoverage':0.1,
                                    'DirectionalPreferentiality':0.7}, 
                **kwargs):

        self.WaveAnalysis = wAnalyzer # output of WavemapAnalyzer
        self.CentralTendencyFunction = centralTendencyFunction

        self.ReentryCurvatureScoreThreshold = reentryThresholds['MaxCurvatureScores']
        self.ReentryCLCoverageThreshold = reentryThresholds['CLCoverage']
        
        self.FocalNumberOfTrajectoriesThreshold = focalThresholds['NumberOfTrajectories']
        self.FocalMinTrajectoryAnglesThreshold = focalThresholds['MinAngles']
        self.FocalMaxTrajectoryAnglesThreshold = focalThresholds['MaxAngles']
        self.FocalCLCoverageThreshold = focalThresholds['CLCoverage']
        self.FocalDirectionalPreferentialityThreshold = focalThresholds['DirectionalPreferentiality']

        self.ExternalFeatures = kwargs

    def ComputeAverageValues(self):

        wAnalyzer = self.WaveAnalysis
        
        # TODO: add runtime error for missing stuff
        NumberOfTrajectories = []
        CurvatureScores = []
        CurvatureScores = []
        MaxCurvatureScores = [] 
        MinAngles = []
        MaxAngles = []

        for waveIndex in wAnalyzer.Waves.keys():
            # Number of individual trajectories
            NumberOfTrajectories.append(len(wAnalyzer.Trajectories[waveIndex]))
            # Curvature scores
            # Maximum curvature scores 
            curvatureScore = np.abs(wAnalyzer.CurvatureScores[waveIndex])
            if len(wAnalyzer.CurvatureScores[waveIndex])!=0:
                if np.sum(curvatureScore!=0)!=0.:
                    MaxCurvatureScores.append(np.max(curvatureScore[curvatureScore!=0.]))
                [CurvatureScores.append(np.abs(element)) for element in wAnalyzer.CurvatureScores[waveIndex]]

            # Min/max Angle
            angles = wAnalyzer.MinMaxAngularDifferences[waveIndex]
            if len(angles)==0: continue
            MinAngles.append(angles[0])
            MaxAngles.append(angles[1])
        
        if self.CentralTendencyFunction=='median':
            centralTendency = np.nanmedian
        elif self.CentralTendencyFunction=='mean':
            centralTendency = np.nanmean

        self.NumberOfTrajectories =centralTendency(NumberOfTrajectories)
        self.CurvatureScores =centralTendency(CurvatureScores)
        self.MaxCurvatureScores =centralTendency(MaxCurvatureScores)
        self.MinAngles = centralTendency(MinAngles)
        self.MaxAngles = centralTendency(MaxAngles)

        outputDict = {'NumberOfTrajectories':centralTendency(NumberOfTrajectories),
                      'CurvatureScores':centralTendency(CurvatureScores),
                      'MaxCurvatureScores':centralTendency(MaxCurvatureScores),
                      'MinAngles':centralTendency(MinAngles),
                      'MaxAngles':centralTendency(MaxAngles),
                      'CentralTendency':self.CentralTendencyFunction}

        return outputDict

    def WaveClassificationGroup(self):
        # This function basically implements the thresholds for defining as focal, rotational, or curved
        # It is very hard-coded, which is not great, but I'll change that with time (right)
        if (self.MaxCurvatureScores>self.ReentryCurvatureScoreThreshold) &\
           (np.mean(self.ExternalFeatures['CLCoverage'])>self.ReentryCLCoverageThreshold):
           return 'rotor'

        elif (self.NumberOfTrajectories>=self.FocalNumberOfTrajectoriesThreshold) &\
             (self.MinAngles>self.FocalMinTrajectoryAnglesThreshold) &\
             (self.MinAngles>=self.FocalMaxTrajectoryAnglesThreshold) &\
             (np.mean(self.ExternalFeatures['CLCoverage'])<self.FocalCLCoverageThreshold)&\
             (self.ExternalFeatures['DirectionalPreferentiality']<self.FocalDirectionalPreferentialityThreshold):
            
            return 'focal'

        elif (self.MaxCurvatureScores>0.2) &\
             (self.MaxCurvatureScores<=self.ReentryCurvatureScoreThreshold): 
           return 'curve'
        
        else:
            return 'planar'

    def WaveClassificationSingle(self):
        ''' This function classifies each wave individually, returning one of the following categories:
        - 0: peripheral
        - 1: reentry
        - 2: radial spread
        - 3: curve
        '''
        wAnalyzer = self.WaveAnalysis
        ClassificationResults = []
        for waveIndex in wAnalyzer.Waves.keys():
            # Number of individual trajectories
            numberOfTrajectories = len(wAnalyzer.Trajectories[waveIndex])
            # Maximum curvature scores 
            maxCurvatureScore = np.max(np.abs(wAnalyzer.CurvatureScores[waveIndex]))
            # Min Angle between trajectories
            minAngle = np.min(wAnalyzer.MinMaxAngularDifferences[waveIndex])
            # CLCoverage
            CLCoverage = self.ExternalFeatures['CLCoverage'][waveIndex]
            # Directional Preferentiality
            directionalPreferentiality = self.ExternalFeatures['DirectionalPreferentiality'] # FIXME: One for all waves!

            if (maxCurvatureScore>self.ReentryCurvatureScoreThreshold) &\
               (CLCoverage>self.ReentryCLCoverageThreshold):
               ClassificationResults.append(1) # Rotor

            elif (numberOfTrajectories>=self.FocalNumberOfTrajectoriesThreshold) &\
                 (minAngle>=self.FocalMaxTrajectoryAnglesThreshold) &\
                 (CLCoverage<self.FocalCLCoverageThreshold)&\
                 (directionalPreferentiality<self.FocalDirectionalPreferentialityThreshold):
               ClassificationResults.append(2) # focal
            
            elif(maxCurvatureScore>0.2) &\
                (maxCurvatureScore<=self.ReentryCurvatureScoreThreshold):
               ClassificationResults.append(3) # curve
            else:
                ClassificationResults.append(0) # focal   
        
        return ClassificationResults             





           
class Wave:
    def __init__(self,id,electrodePositions,electrodeTemplate,neighborList=None):
        self.ID = id
        self.ElectrodePositions = electrodePositions
        self.ElectrodeTemplate = electrodeTemplate
        self.NeighborList = neighborList

        self.Peripheral = True
        self.Members = None
        self.WaveGraph = None
        self.ConnectivityMatrix = None
        self.Size = 0
        self.CompactMatrix = None

    def GetShortestPath(self,edgeWeights,source,target):
        path,distance = self.WaveGraph.GetShortestPath(edgeWeights,source,target)
        
        return path,distance

    ##########
    def GetSize(self):
        if self.Members is None: 
            print('No members added') 
            return -1
        else:
            self.Size = len(self.Members)
            return self.Size

    def SetWaveGraph(self, conductionThreshold, radius = np.inf): #SetConnectivityMatrix

        localConduction,distanceMatrix = self.LocalConduction(False)

        if self.NeighborList is None:
            indexes = (localConduction >= conductionThreshold) & (distanceMatrix<= radius) #  removed because in "LocalConduction" the distances of non-neighbors are set to infinity
        else:
            indexes = (localConduction >= conductionThreshold) & (~np.isinf(localConduction >= conductionThreshold))


        self.ConnectivityMatrix = indexes

        connectionIndexes = np.where(indexes)
        connectionIndexes = np.vstack([connectionIndexes[0],connectionIndexes[1]]).T
        if self.Size is None:
            self.WaveGraph = Graph(connectionIndexes,connectionIndexes.max()+1)
        else:
            self.WaveGraph = Graph(connectionIndexes,self.Size)

    def ComputeAllStartingPoints(self,startTolerance=0):
        ''' Get isolated starting regions. Points that have activation time difference
        <startTolerance will be included in the same region
        '''

        validStartingPoints = np.zeros((self.Size),dtype=bool)
        if self.WaveGraph is not None: #self.ConnectivityMatrix is not None: FIXME
            
            uniqueComponents = self.WaveGraph.GetAllGraphPaths(merge=True)  

            # Loop over possible starting points, check if they have indeed earlier starting
            # points than all of their neighbors
            
            convertIndices = {self.Members[i,0]:i for i in range(len(self.Members))} # account for missing points
            for startingCandidate in uniqueComponents.keys():
                candidateActivation = self.Members[startingCandidate,2]
                # Get neighbors and convert to "Members" indices
                neighbors = np.array(self.NeighborList[self.Members[startingCandidate,0]])
                neighbors = np.array([convertIndices[N] for N in neighbors if N in self.Members[:,0]])
                neighbors = neighbors[neighbors!=startingCandidate]
        
                neighborActivations = self.Members[neighbors,2]
                if (candidateActivation<=neighborActivations).all():
                    validStartingPoints[startingCandidate] = True


            # # Test with flexibility of activation time detection
            # startingActivations = np.unique(self.Members[list(uniqueComponents.keys()),2])
            # flexibleStart = np.zeros(self.Size,dtype=bool)
            # for startingActivation in startingActivations:
            #     flexibleStart = np.logical_or(flexibleStart,np.abs(self.Members[:,2]-startingActivation)<=startTolerance) # 1 ms tolerance in act detection

            # validStartingPoints[flexibleStart] = True # FIXME [list(uniqueComponents.keys())] = True

        else:
            startingPointTime = np.min(self.Members[:, 2]) 
            validStartingPoints = self.Members[:, 2] <= startingPointTime+startTolerance #ms tolerance

        startingPoints = self.Members[validStartingPoints,:]
        pointIndices = np.where(validStartingPoints)[0]
        self.StartingPoints = startingPoints
        self.StartingPointIndices = pointIndices

        return validStartingPoints, pointIndices

    def ComputeAllEndPoints(self,endTolerance=0):
        ''' Get isolated starting regions. Points that have activation time difference
        <endTolerance will be included in the same region
        '''

        validEndPoints = np.zeros((self.Size),dtype=bool)
        if self.WaveGraph is not None: #self.ConnectivityMatrix is not None:
            reverseGraph = Graph(self.WaveGraph.ConnectionIndexes,self.Size)
            reverseGraph.ConnectivityMatrix = reverseGraph.ConnectivityMatrix.T 
            reverseGraph.CompactGraph = reverseGraph.MakeCompactGraph(reverseGraph.ConnectionType)

            uniqueComponents = reverseGraph.GetAllGraphPaths(merge=True)  

            convertIndices = {self.Members[i,0]:i for i in range(len(self.Members))} # account for missing points
            for endCandidate in uniqueComponents.keys():
                candidateActivation = self.Members[endCandidate,2]
                # Get neighbors and convert to "Members" indices
                neighbors = np.array(self.NeighborList[self.Members[endCandidate,0]])
                neighbors = np.array([convertIndices[N] for N in neighbors if N in self.Members[:,0]])
                neighbors = neighbors[neighbors!=endCandidate]
        
                neighborActivations = self.Members[neighbors,2]
                if (candidateActivation>=neighborActivations).all():
                    validEndPoints[endCandidate] = True



            # # Test with flexibility of activation time detection
            
            # endActivations = np.unique(self.Members[list(uniqueComponents.keys()),2])
            # flexibleEnd = np.zeros(self.Size,dtype=bool)
            # for endActivation in endActivations:
            #     flexibleEnd = np.logical_or(flexibleEnd,np.abs(self.Members[:,2]-endActivation)<=endTolerance) # 1 ms tolerance in act detection

            # validEndPoints[flexibleEnd] = True #FIXME [list(uniqueComponents.keys())] = True

        else:
            endPointTime = np.max(self.Members[:, 2]) 
            validEndPoints = self.Members[:, 2] >= startingPointTime-startTolerance
            # validEndPoints = self.Members[:, 2] == endPointTime

        endPoints = self.Members[validEndPoints,:]
        pointIndices = np.where(validEndPoints)[0]
        self.EndPoints = endPoints
        self.EndIndices = pointIndices

        return validEndPoints, pointIndices

    def LocalConduction(self, useConnectivityMatrix = True):
        '''
        Returns the conduction velocities and the distance 
        between members of the wave

        If there is no connectivity matrix yet, it is possible
        to manually calculate the distances between electrodes
        and their differences in activation times
        '''

        if self.ConnectivityMatrix is None or not useConnectivityMatrix:
            # Manually calculate distances and activation time differences
            distanceMatrix = squareform(pdist(self.ElectrodePositions[self.Members[:,0], :]))
            activationDifferenceMatrix = cdist(self.Members[:,2].reshape(-1,1),
                                            self.Members[:,2].reshape(-1,1),lambda u,v: v-u)

            if self.NeighborList is not None:
                for electrodeIndex,electrodeCode in enumerate(self.Members[:,0]):
                    distanceMatrix[electrodeIndex,~np.isin(self.Members[:,0],self.NeighborList[electrodeCode])] = np.nan# np.inf would end up ignoring the neighbor constraint due to inf CV

            conductionMatrix = distanceMatrix / activationDifferenceMatrix 
        else:
            # Use the connectivity matrix to get CVs
            # If this option is used, the CV threshold is embedded in the connectivity matrix
            # This means that the only distance and CV values that will be returned in the matrix 
            # are already the ones above the CV threshold
            
            numberOfMembers = self.GetSize()

            distanceMatrixValues = []
            conductionMatrixRowIndices = []
            conductionMatrixColumnIndices = []
            conductionMatrixValues = []

            for memberIndex in range(numberOfMembers):
                memberPosition = self.ElectrodePositions[self.Members[memberIndex, 0], :]
                memberActivation = self.Members[memberIndex, 2]

                connectedMemberIndices = np.where(self.ConnectivityMatrix[:, memberIndex])[0] #members connected to current member
                connectedMemberPositions = self.ElectrodePositions[self.Members[self.ConnectivityMatrix[:, memberIndex], 0], :]
                connectedMemberActivations = self.Members[self.ConnectivityMatrix[:, memberIndex], 2]

                positionDifferences = memberPosition-connectedMemberPositions 
                distances = np.sqrt(np.sum(positionDifferences**2,axis=1))
                activationDifferences = memberActivation-connectedMemberActivations

                conductionMatrixRowIndices.append(connectedMemberIndices)
                conductionMatrixColumnIndices.append(memberIndex * np.ones((len(connectedMemberIndices), 1)))
                conductionMatrixValues.append(distances / activationDifferences)

                distanceMatrixValues.append(distances)

            # Make single arrays from the connections and CV values 
            connectionIndexes = np.vstack([np.hstack([E for E in conductionMatrixRowIndices]),
                                        np.vstack([E for E in conductionMatrixColumnIndices]).flatten()]).T
            xx,yy = connectionIndexes[:,0].astype(int),connectionIndexes[:,1].astype(int)
            conductionMatrixValues = np.hstack([cv for cv in conductionMatrixValues])

            # Make matrices
            conductionMatrix = np.zeros((numberOfMembers,numberOfMembers))
            conductionMatrix[xx,yy] = conductionMatrixValues

            distanceMatrix = np.zeros(conductionMatrix.shape,dtype=float)
            distanceMatrix[xx,yy]  = np.hstack([E for E in distanceMatrixValues])

            # Set non-neighbor as infinity distance
            if self.NeighborList is not None:
                for electrodeIndex,electrodeCode in enumerate(self.Members[:,0]):
                    distanceMatrix[electrodeIndex,~np.isin(self.Members[:,0],self.NeighborList[electrodeCode])] = np.inf

        return conductionMatrix, distanceMatrix

    def SubstituteSimilarMembersByAverage(self): # Not in use
        
        # Group members with same activation times
        memberGroups = list() # members with same activation times
        for memberIndex,member in enumerate(self.Members):
            neighbors = np.array(self.NeighborList[member[0]])

            closeActivations = self.Members[neighbors,2]
            closeActivations = closeActivations == member[2]

            if closeActivations.sum()==1:
                memberGroups.append([member[0]])
            else:
                sameAct = neighbors[closeActivations]
                memberGroups.append(self.Members[sameAct,0])
        
        # Make elements that need to be substituted    
        newElectrodePositions = []
        newMembers = []
        newNeighborList= []
        memberConvertDict = {}
        
        for group in memberGroups:
            if np.isnan(group).any(): 
                continue
            
            pos = self.ElectrodePositions[group]
            pos = np.mean(pos,axis=0)

            newMembers.append(self.Members[group[0],:])

            for G in group:  
                memberConvertDict[G] = len(newMembers)-1
                memberGroups[G] = np.nan

                for E in self.NeighborList[G]:
                    newNeighborList.append(np.unique(E))

            newElectrodePositions.append(pos)
            
        ## Save
        # New members
        self.Members = np.array(newMembers)
        self.Members[:,0] = np.arange(len(self.Members))
        # new NeighborList
        self.NeighborList = []
        for neighborGroup in newNeighborList:
            self.NeighborList.append([memberConvertDict[i] for i in neighborGroup])
        # new Positions
        self.ElectrodePositions = np.array(newElectrodePositions)
        
        # # Adjust other things that may have been calculated already
        # if self.Size is not None: self.GetSize()
        # if self.WaveGraph is not None: self.SetWaveGraph(0.1)

    # The next 3 functions are used to get more accurate trajectories considering starting
    # and ending points that are composed by more than one electrode

    def GetEffectivePositions(self,currentStartingPoint,currentEndPoint):
        positions = self.ElectrodePositions[self.Members[:,0]]

        effectivePositions = list()
        skipElectrodes = []
        newIndexes = {}
        k = 0
        for electrodeIndex,electrodePositions in enumerate(positions):

            if electrodeIndex in skipElectrodes: continue

            if electrodeIndex in currentStartingPoint:
                effectivePositions.append(np.mean(positions[currentStartingPoint],axis=0))
                for sp in currentStartingPoint: 
                    skipElectrodes.append(sp) 
                    newIndexes[sp] = k
            elif electrodeIndex in currentEndPoint:
                effectivePositions.append(np.mean(positions[currentEndPoint],axis=0))
                for ep in currentEndPoint: 
                    skipElectrodes.append(ep) 
                    newIndexes[ep] = k
            else:
                effectivePositions.append(electrodePositions)
                newIndexes[electrodeIndex] = k
            k = k+1
        
        return np.array(effectivePositions),newIndexes

    def GetEffectiveDistanceMatrix(self,effectivePositions,newIndexes):
        distanceMatrix = squareform(pdist(effectivePositions))
        if self.NeighborList is not None:
            for electrodeIndex,electrodeCode in enumerate(self.Members[:,0]):
                currentIndex = newIndexes[electrodeCode]
                remainingIndexes = self.Members[~np.isin(self.Members[:,0],self.NeighborList[electrodeCode]),0] #np.where(~np.isin(self.Members[:,0],self.NeighborList[electrodeCode]))[0]
                remainingIndexes = [newIndexes[rI] for rI in remainingIndexes]

                distanceMatrix[currentIndex,remainingIndexes] = np.inf
        return distanceMatrix

    def GetEffectiveActivationDifferenceMatrix(self,newIndexes):
        originalDifferenceMatrix = cdist(self.Members[:,2].reshape(-1,1),
                                    self.Members[:,2].reshape(-1,1),lambda u,v: v-u)

        correctedIndices = np.unique(list(newIndexes.values()))
        activationDifferenceMatrix = originalDifferenceMatrix[correctedIndices,:][:,correctedIndices]
        return activationDifferenceMatrix

    def GetShortestPathV2(self,conductionMatrix,newIndexes,origin,target):
        # Get path with max increase in activation time within valid CV

        correctedIndices = np.unique(list(newIndexes.values()))
        prev = np.zeros(len(conductionMatrix),dtype=int)
        Electrodes = self.Members[correctedIndices,0]
        Activations = self.Members[correctedIndices,2]

        for elementIndex,element in enumerate(Electrodes):
            previousConnections = conductionMatrix[:,elementIndex]
            previousConnections[np.isinf(previousConnections)] = np.nan
            if ((previousConnections<0) | np.isnan(previousConnections)).all(): 
                prev[elementIndex] = -1
                continue       

            neighbors = np.where((~np.isinf(previousConnections)) * (previousConnections>0.1))[0]
            neighbors = neighbors[np.isin(neighbors,self.NeighborList[elementIndex])]
            neighborActivations = Activations[neighbors]

            activationDifference = np.abs(neighborActivations-Activations[elementIndex])

            previousElement = np.where(activationDifference==activationDifference.max())[0]

            if len(previousElement)>1: previousElement = previousElement[0]
            prev[elementIndex] = neighbors[previousElement]
            
        origin = newIndexes[origin]
        target = newIndexes[target]
        path = [target]
        lastIndex = target
        while origin not in path:
            path.append(prev[lastIndex])
            lastIndex = prev[lastIndex]
            if -1 in path: 
                return None           
                break

        return np.flipud(path)

class Graph:
    def __init__(self,connectionIndexes,size = None,connectionType = 'directional',connect = False):
        '''
        The graph is built by the connections between elements and by the total size of the matrix
        If no size is defined, it will be corresponding to the maximal connection index
        '''

        self.ConnectionIndexes = connectionIndexes # AKA edges
        if size is None:
            self.Size = connectionIndexes.max()+1
        else:
            self.Size = size
        self.ConnectionType = connectionType
        self.ConnectivityMatrix = self.MakeConnectivityMatrix()
        self.CompactGraph = self.MakeCompactGraph(self.ConnectionType)

        if connect:
            self.ConnectedGroups = self.GetAllConnectedGroups()

    def MakeConnectivityMatrix(self,symmetric = False):
        ''' Makes a matrix from the provided edges'''
        connectivityMatrix = np.zeros((self.Size,self.Size))
        connectivityMatrix[self.ConnectionIndexes[:,0],self.ConnectionIndexes[:,1]] = True
        if symmetric:
            connectivityMatrix[self.ConnectionIndexes[:,1],self.ConnectionIndexes[:,0]] = True #ignore direction

        return connectivityMatrix.astype(bool)

    def MakeCompactGraph(self,connectionType):
        ''' Make the graph a Python set
        connectionType is either 'directional' or 'weak'
        '''

        compactGraph = {}
        for i,line in enumerate(self.ConnectivityMatrix):
            inds = np.where(line)[0]
            if len(inds)!=0:
                compactGraph[i] = {element for element in inds}
                if connectionType=='weak':
                    for element in inds:
                        if element in compactGraph.keys():
                            compactGraph[element].add(i)
                        else:
                            compactGraph[element] = {i}
            else:
                compactGraph[i] = set()

        return compactGraph

    def GetAllConnectedGroups(self):
        '''Wrapper for connecting groups using self.GetConnectedGroup for all possible nodes'''
        already_seen = set()
        result = []
        for node in self.CompactGraph:
            # already_seen = set()
            if node not in already_seen:
                connected_group, already_seen = self.GetConnectedGroup(node, already_seen)
                result.append(connected_group)

        filteredResults = list()
        for element in result:
            if ~(np.array([np.isin(element,nodeLists).all() for nodeLists in result]).sum()>1):
                filteredResults.append(element)

        return filteredResults

    def GetConnectedGroup(self,node, already_seen):
        result = []
        nodes = set([node])
        while nodes:
            node = nodes.pop()
            already_seen.add(node)

            for element in self.CompactGraph[node] - already_seen: 
                nodes.add(element)
            result.append(node)

        return result, already_seen

    def TransverseGraph(self, node,connections):  
        # Depth first graph transversion
        if node not in connections:
            connections.add(node)
            for neighbour in self.CompactGraph[node]:
                self.TransverseGraph(neighbour,connections)
        return connections

    def GetAllGraphPaths(self,merge = True):
        # For all elements, transverse the connectivity matrix graph to find where they connect to
        graphPaths = {i:self.TransverseGraph(i,set()) for i in range(self.Size)}

        if merge:
            uniqueComponents = self.MergeGraphPaths(graphPaths)
        else:
            uniqueComponents = graphPaths

        # The remaining keys of the uniqueComponents dict are possible starting points
        # the components are the right side of the dict
        return uniqueComponents

    def MergeGraphPaths(self,graphPaths):

        uniqueComponents = {}
        for fK in graphPaths.keys():
            addFlag=True
            for sK in graphPaths.keys():
                # check if set is within another, but not identical to it
                if graphPaths[fK].issubset(graphPaths[sK]) and graphPaths[fK]!=graphPaths[sK]:
                    addFlag=False
            if addFlag:
                uniqueComponents[fK] = graphPaths[fK]

        # The remaining keys of the uniqueComponents dict are possible starting points
        # the components are the right side of the dict
        return uniqueComponents

    def GetShortestPath(self,edgeWeights,source,target):

        PatchLen = self.ConnectivityMatrix.shape[0]
        StepDists = np.ones(PatchLen)*np.inf
        prev = np.ones(PatchLen)*np.nan
        Q = np.arange(PatchLen,dtype=float)

        StepDists[source] = 0
        prev[source] = source
        while np.isnan(Q).sum()<PatchLen: #While not all points have been visited
            #u ← vertex in Q with min dist[u]
            unvisitedIndexes = np.where(~np.isnan(Q))[0]
            u = np.where(StepDists[unvisitedIndexes]==np.nanmin(StepDists[unvisitedIndexes]))[0][0]
            u = unvisitedIndexes[u]
            if u == target:# and ~np.isnan(prev[u]):
                break

            #remove u from Q
            Q[u] = np.nan

            neighbors = np.where(self.ConnectivityMatrix[u,:]!=0)[0]
            validNeighbors = neighbors[np.isin(neighbors,Q)]
            for v in validNeighbors:
                alt = StepDists[u]+edgeWeights[u,v]
                if alt<StepDists[v]:
                    StepDists[v]=alt
                    prev[v] = u
            # if u == target:
            #     break
                    
        # Connect path
        prev = np.array(prev)
        path = [target]
        u = prev[target]
        if np.isnan(u):
            # print('Broken path')
            return None,np.inf
        else:
            u = int(u)

        while ~np.isin(source,path):
            path.append(u)
            if np.isnan(prev[u]):
                # print('Broken path')
                return None,np.inf
            else:
                u = int(prev[u])
        path = np.asarray(path).flatten()
        
        return np.flipud(path),StepDists[target]



'''
Leftovers from previous implementations

    # def DetectTrajectoriesV2(self,filter = True,selection = 'shortest'):
    #     self.Trajectories = {}
    #     for waveIndex,wave in self.Waves.items():

    #         trajectories = [] #cell(numberOfStartingPoints, numberOfEndPoints);

    #         trajectories = np.zeros((0,3),dtype = object)
    #         for currentStartIndices in self.WaveStartingIndices[waveIndex]:
    #             for currentEndPointIndices in self.WaveEndIndices[waveIndex]:
                    
    #                 # effectivePositions,newIndexes = wave.GetEffectivePositions(currentStartIndices,currentEndPointIndices)
    #                 # effectivePositions = wave.ElectrodePositions[wave.Members[:,0],:]
    #                 # newIndexes = {j:i for i,j in enumerate(wave.Members[:,0])}
    #                 # print(newIndexes)
    #                 correctedIndices = np.unique(list(newIndexes.values()))

    #                 distanceMatrix = wave.GetEffectiveDistanceMatrix(effectivePositions,newIndexes)

    #                 activationDifferenceMatrix = wave.GetEffectiveActivationDifferenceMatrix(newIndexes)

    #                 conductionMatrix = distanceMatrix/activationDifferenceMatrix
                    
    #                 startTime = wave.Members[currentStartIndices,2]
    #                 endTime = wave.Members[currentEndPointIndices,2]
    #                 if (endTime.max() - startTime.min()) >= self.MinimumDuration:
    #                     for currentStartIndex in currentStartIndices:
    #                         for currentEndIndex in currentEndPointIndices:
    #                             # print(currentStartIndex,currentEndIndex)
    #                             path = wave.GetShortestPathV2(conductionMatrix,
    #                                                     newIndexes,currentStartIndex,
    #                                                     currentEndIndex)
    #                             if path is None:
    #                                 disconnectedPoints=True
    #                                 continue
    #                             if len(path)>1:
    #                                 trajectories = np.vstack([trajectories,
    #                                                         np.array([currentStartIndex,currentEndIndex,path],
    #                                                         dtype=object)])

    #         self.Trajectories[waveIndex] = trajectories
    #     if filter:
    #         self.FilterTrajectories(selection)

    #     self.GetTrajectoryPositions()


    # def ComputeWaveSizes(self):
    #     waveSizes = list()
    #     for waveIndex,wave in self.Waves.items():
    #         waveSizes.append(len(wave.Members))
    #     return np.array(waveSizes)

    # def TransverseGraph(self,connections,visited, graph, node):  
    #     # Depth first graph transversion
    #     if node not in visited:
    #         connections.add(node)
    #         visited.add(node)
    #         for neighbour in graph[node]:
    #             self.TransverseGraph(connections,visited, graph, neighbour)
    #     return connections
'''
