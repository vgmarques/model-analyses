import os,sys 
import scipy.io as sio
import pickle

try:
    path = sys.argv[1]
except:
    print('You must input a path')

for file in os.listdir(path):
    if not file.endswith('.catG'):
        continue
    
    fContent = pickle.load(open(os.path.join(path,file),'rb'))

    outputDict = {'Anatomy':fContent.Anatomy,
                  'CatheterModel':fContent.CatheterModel,
                  'Catheters':fContent.Catheters,
                  'AnatomyShape':fContent.AnatomyShape,
                  'CatheterPositionDictionary':fContent.CatheterPositionDictionary}

    sio.savemat(os.path.join(path,file.split('.')[0])+'.mat',outputDict)
    print('Saved %s'%(file.split('.')[0]+'.mat'))