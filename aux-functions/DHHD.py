# -*- coding: utf-8 -*-

import numpy as np
from scipy.spatial import Delaunay,ConvexHull


# Mahe phi gradient
def TriangleArea(triangle,mesh):
    x0 = mesh[triangle[0],0]
    y0 = mesh[triangle[0],1]
    x1 = mesh[triangle[1],0]
    y1 = mesh[triangle[1],1]
    x2 = mesh[triangle[2],0]
    y2 = mesh[triangle[2],1]
    
    return np.abs((x1-x0)*(y2-y0)-(x2-x0)*(y1-y0))

def BuildGradPhi(triangles,positions):
    '''
    gradPhi_ik is given by the length of the edge opposite to
    vertex i, normalized by 2 * the triangle area
    This is a vector (in x and y), pointing from i to the edge

    This is following exactly the definition from the thesis,but is different
    from what was in the code
    '''

    GradPhi = np.zeros((len(triangles),3,2))

    for k,tri in enumerate(triangles):
        a,b,c = positions[tri]
        AB = np.zeros((3,2))

        # pointing to A
        P = np.median([b,c],axis=0)
        mag = np.linalg.norm(b-c)
        AB[0,:] = (a-P)/np.linalg.norm(a-P)*np.cross(b-a,c-a)
        AB[0,:] = AB[0,:]/np.linalg.norm(AB[0,:])*mag
        # pointing to B
        P = np.median([a,c],axis=0)
        mag = np.linalg.norm(a-c)
        AB[1,:] = (b-P)/np.linalg.norm(b-P)*np.cross(c-b,a-b)
        AB[1,:] = AB[1,:]/np.linalg.norm(AB[1,:])*mag
        # pointing to C
        P = np.median([b,a],axis=0)
        mag = np.linalg.norm(b-a)
        AB[2,:] = (c-P)/np.linalg.norm(c-P)*np.cross(a-c,b-c)
        AB[2,:] = AB[2,:]/np.linalg.norm(AB[2,:])*mag

        Area = TriangleArea(tri,positions)
        AB = AB/(Area*2)
        GradPhi[k,:,:] = AB

    return GradPhi

# Get neighboring nodes and triangles for each node
def GetNeighbors(node,triangle):
    #     get the neighboring triangles and neighboring nodes of a node
    #     node: the reference node
    #     triangle: the grid table
    #     neighbor_triangle: the neighbor triangle of the reference node
    #     neighbor_point: the neighbor points of the reference node

    neighbor_triangle=[]

    check = np.sum(triangle==node,axis = 1)
    for i,C in enumerate(check):
        if C:
            neighbor_triangle=np.append(neighbor_triangle,i)

    neighbor_point=[]
    for n_tri in neighbor_triangle:
        temp = triangle[int(n_tri),:]
        for j in range(0,3):
            if np.sum(neighbor_point==temp[j])>0:
                continue
            else:
                neighbor_point = np.append(neighbor_point, temp[j])

    neighbor_point= np.sort(neighbor_point) #   sort in ascending order

    return np.uint(neighbor_triangle),np.uint(neighbor_point)

def BuildS(node_info):
    '''
    The matrix S has size M*N x M*N and contains the inner product
    of delPhi_ik for a given node i with the delPhi_jk at the 
    neighboring node j, across all triangles k

    It is used for the computation of the divergence field:
    S . d = B
    where d is the vectorized curl-free potential and B is calculated
    from the vector field and the mesh.
    '''
    positions = node_info['positions']
    triangles = node_info['triangles']
    GradPhi = node_info['gradphi'] 
    NTri = node_info['NTri'] 
    boundaryIndexes = node_info['boundaries']


    S = np.zeros((len(positions),len(positions)))
    for i,pos in enumerate(positions):
        for k in NTri[i]:
            tri = triangles[k]
            posInTriangle = np.where(tri==i)[0][0]
            phi_i = GradPhi[k,posInTriangle,:]

            for jTri, j in enumerate(tri):
                S[i,j] += np.dot(phi_i,GradPhi[k][jTri])
    
    Ident= np.identity(S.shape[0])
    # replace bclist columns in S1 with columns from in Ident
    S[:,boundaryIndexes]=Ident[:,boundaryIndexes]

    return S

def Build_Xi(node_info,velocities):
    # Information is calculated for the centroids, this is the 
    # only reason why this is not the same as the input gradient
    #
    triangles = node_info['triangles']
    #     generate the Average Vector table
    Xi = np.zeros((len(triangles),2))

    for i in range(len(triangles)):
        # positions of triangle corners
        Xi[i,0] = np.mean(velocities[triangles[i,:],0])
        Xi[i,1] = np.mean(velocities[triangles[i,:],1])

    return Xi

def BuildRHS(node_info,Xi):

    #     Xi: matrix with velocities calculated for centroids
    #     node_info: geometrical information of the mesh

    GradPhi = node_info['gradphi']
    positions = node_info['positions']
    triangles = node_info['triangles']
    NTri = node_info['NTri']
    boundaryIndexes = node_info['boundaries']
    #
    B = np.zeros((len(positions),1))
    C = np.zeros((len(positions),1))

    for i in range(len(positions)):
        for k in NTri[i]:
            tVerts = triangles[k]
            posInTriangle = np.where(tVerts==i)[0][0]
            phi_i = GradPhi[k][posInTriangle]
            for jInd, j in enumerate(tVerts):
                B[i] += np.dot(phi_i,Xi[k,:])
                C[i] += np.cross(phi_i,Xi[k,:])

    
    B[boundaryIndexes] = 0
    C[boundaryIndexes] = 0

    return B,C

def SolveSystem(B,C,S,node_info):
    D = np.matmul(np.linalg.inv(S),B)
    R = np.matmul(np.linalg.inv(S),C)

    D[node_info['boundaries']] = 0
    R[node_info['boundaries']] = 0
    
    return D,R

def GetBoundaries(positions,NPoint):
    hull = ConvexHull(positions)
    hullPoints = positions[hull.vertices,:]

    boundaryIndexes = list()
    for i,point in enumerate(hullPoints):
        idx = hull.vertices[i]
        closePoints = NPoint[idx]
        for pt in closePoints: boundaryIndexes.append(pt)
    boundaryIndexes = np.unique(boundaryIndexes)
    return boundaryIndexes

def GetNodeInfo(positions):
    # mv_x and mv_y are the x and y 1D coordinates of the grid points
    # Not a meshgrid format!

    # Make triangles 
    trimesh = Delaunay(positions,incremental=True)
    triangles = trimesh.simplices

    gradphi = BuildGradPhi(triangles,positions) 

    tri_and_points = [GetNeighbors(i,triangles) for i in range(positions.shape[0])]
        
    NTri = [tri_and_points[i][0] for i in range(positions.shape[0])]
    NPoint = [ tri_and_points[i][1] for i in range(positions.shape[0])]
    
    boundaryIndexes = GetBoundaries(positions,NPoint)

    node_info = {'positions':positions,'triangles':triangles,
                 'gradphi':gradphi,'NTri':NTri,
                 'NPoint':NPoint,'boundaries':boundaryIndexes}
    
    return node_info