# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 16:13:51 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys

import numpy as np
try:
    import pickle5 as pickle
except:
    import pickle

import pandas as pd
import scipy.io as sio

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import UserInterfaceFunctions as ui
import egm_processing as egmp
from RecurrenceComputation import RecurrenceComputation


def main(args):

    experimentNames = []
    for file in os.listdir(args.inputFolderRecurrence):
        if file.startswith(args.basename) and file.endswith(args.extension):
            experimentNames.append(file[:9])
    experimentNames = np.sort(experimentNames)
    print(experimentNames)
    ######## Global output
    '''
        For each of the blocks, the following information is obtained:
            - experiment: simulation from which the block came
            - position: catheter position
            - label: close, near or far
            - iTime: initial time of the block
            - duration: block size in ms
            - threshold: automatic threshold determined by Stef\'s algorithm
            - RR
            - DET
            - Lmax
            - ENT
            - LAM
            - TT
            - Lmean
            - Lmed
        '''
    
    featCols = ['experiment','position','label','iTime','duration',
                'threshold','RR','DET','Lmax','ENT','LAM','TT','Lmean','Lmed']
    OutputDF = pd.DataFrame(columns =featCols )

    for EXP in experimentNames:
        ######################
        # Load PS information
        print('Loading PS information')
    
        with open(os.path.join(args.inputFolderPS,EXP+"_PSDistances.pkl"), 'rb') as output:
            proximityDict = pickle.load(output)
            trajDurations = pickle.load(output)
        
        ClosePresence,NearPresence = egmp.PSProximity(proximityDict,trajDurations,thresholds=[7,20])
    
        #
        ##########################################################################
        # Recurrence analysis
        print('Loading distance matrices')
        with open(os.path.join(args.inputFolderRecurrence,EXP+"_RecurrenceAnalyses.pkl"), 'rb') as output:
            distanceMatrixDict = pickle.load(output)
            activationPhaseSignalDict = pickle.load(output)
            estimatedAFCLDict = pickle.load(output)
        
        with open(os.path.join(args.inputFolderRecurrence,"RecurrenceComputationObj.pkl"), 'rb') as output:
            recComp = pickle.load(output)
    
        #
        ##########################################################################
        # Get features from RPs generated with different thresholds  
        print('Calculating features...')
    
        FarMatrix = np.logical_or(NearPresence,ClosePresence)
        NearMatrix = np.logical_and(NearPresence,np.logical_not(ClosePresence))
        CloseMatrix = np.logical_and(ClosePresence,np.logical_not(NearPresence))
        
        for i,catheter in enumerate(list(proximityDict.keys())):
            print('Calculating features for %s, catheter %s'%(EXP,catheter))
            DM = distanceMatrixDict[catheter]
            estimatedAFCL=estimatedAFCLDict[catheter]
            # Division into blocks
        
            BlocksFar,DurationsFar =  egmp.PSBlocks(FarMatrix[:,i],kernelSize = 1e3*estimatedAFCL,reverseOutput=True)
            BlocksNear,DurationsNear =  egmp.PSBlocks(NearMatrix[:,i],kernelSize = 1e3*estimatedAFCL,reverseOutput=False)
            BlocksClose,DurationsClose =  egmp.PSBlocks(CloseMatrix[:,i],kernelSize = 1e3*estimatedAFCL,reverseOutput=False)
            
            # Recurence plots
            if args.minLineAFCL:
                minLineLength = np.round(estimatedAFCL/recComp.SampleShift)
            else:
                minLineLength = 2
            RP, _, recurrenceThreshold, _ = \
                        recComp.ComputeRecurrencePlot(DM, args.SamplingFrequency, estimatedAFCL)
            RP = recComp.ErodeRP(RP,DM)            # Get properties from blocks
            ## Far
            for j,block in enumerate(BlocksFar):
                if DurationsFar[j]<1e3*estimatedAFCL:
                    continue
                block_ind = np.asarray(block)//recComp.SampleShift
                RP_block = RP[block_ind[0]:block_ind[1],block_ind[0]:block_ind[1]]

                
                rqa,diag = recComp.ComputeRQA(RP_block,estimatedAFCL,args.SamplingFrequency,
                                                  minDiagonalLine = int(minLineLength))
                
                featRow={'experiment':EXP,'position':catheter,
                         'label':'far','iTime':block[0],'duration':block[1]-block[0],
                         'threshold':recurrenceThreshold,'RR':rqa['recurrence_rate'],
                         'DET':rqa['determinism'],'Lmax':rqa['longest_diagonal'],
                         'ENT':rqa['entropy'],'LAM':rqa['laminarity'],
                         'TT':rqa['trapping_time'],'Lmean':np.mean(diag),
                         'Lmed':np.median(diag)}
                OutputDF = OutputDF.append(featRow,ignore_index=True)
            
            ## Near
            for j,block in enumerate(BlocksNear):
                if DurationsNear[j]<1e3*estimatedAFCL:
                    continue
                block_ind = np.asarray(block)//recComp.SampleShift
                RP_block = RP[block_ind[0]:block_ind[1],block_ind[0]:block_ind[1]]

                
                rqa,diag = recComp.ComputeRQA(RP_block,estimatedAFCL,args.SamplingFrequency,
                                                  minDiagonalLine = int(minLineLength))
                
                featRow={'experiment':EXP,'position':catheter,
                         'label':'near','iTime':block[0],'duration':block[1]-block[0],
                         'threshold':recurrenceThreshold,'RR':rqa['recurrence_rate'],
                         'DET':rqa['determinism'],'Lmax':rqa['longest_diagonal'],
                         'ENT':rqa['entropy'],'LAM':rqa['laminarity'],
                         'TT':rqa['trapping_time'],'Lmean':np.mean(diag),
                         'Lmed':np.median(diag)}
                OutputDF = OutputDF.append(featRow,ignore_index=True)

            ## Close
            for j,block in enumerate(BlocksClose):
                if DurationsClose[j]<1e3*estimatedAFCL:
                    continue
                block_ind = np.asarray(block)//recComp.SampleShift
                RP_block = RP[block_ind[0]:block_ind[1],block_ind[0]:block_ind[1]]
                 
                rqa,diag = recComp.ComputeRQA(RP_block,estimatedAFCL,args.SamplingFrequency,
                                                  minDiagonalLine = int(minLineLength))
                
                featRow={'experiment':EXP,'position':catheter,
                         'label':'close','iTime':block[0],'duration':block[1]-block[0],
                         'threshold':recurrenceThreshold,'RR':rqa['recurrence_rate'],
                         'DET':rqa['determinism'],'Lmax':rqa['longest_diagonal'],
                         'ENT':rqa['entropy'],'LAM':rqa['laminarity'],
                         'TT':rqa['trapping_time'],'Lmean':np.mean(diag),
                         'Lmed':np.median(diag)}
                OutputDF = OutputDF.append(featRow,ignore_index=True)        

    ##########################################################################
    ## Save
    print('Saving features')
    OutputDF.to_csv(os.path.join(args.outputPath,args.basename+'_BlockFeatures.csv'))
    print('Done!')

if __name__=='__main__':
    
    parser = ui.MyParser(description='Get RQA features from blocks determined by the proximity of PSs to\
                         the  catheters.',
                        fromfile_prefix_chars='+',
                        usage = 'python ExtractBlockRQAFeatures.py +ParameterFile.par [PARAMETERS] basename')

    # Essential files
    parser.add_argument('basename',action='store',type=str,
                    help = 'Prefix to all relevant files in the input folder')

    
    # Paths
    parser.add_argument('-inputFolderPS',action='store',type=str,default = '.',
                    help = 'Folder containing PSs as a txt file')
    parser.add_argument('-inputFolderRecurrence',action='store',type=str,default = '.',
                    help = 'Folder containing distance matrices in files with extension .pkl')
    parser.add_argument('-outputPath',action='store',type=str,default='.',
                    help = 'Path to output')
    
    # RP params
    parser.add_argument('--minLineAFCL',action='store_true',
                        help = 'If true, determines the minimum line length by the AFCL')
    parser.add_argument('-SamplingFrequency',action='store',type=float,default=1000,
                        help = 'Sampling frequency, default 1000 Hz')

    args = parser.parse_args()
    args.extension='.pkl' #Probably won't change

    
        
    main(args)

    '''
    #For debugging
    class args:
        def __init__(self,opt='wsl'):
            if opt=='wsl':
                self.pathCat = 'd:\\vgmar\\model_data\\exp906\\electrode_data\\Control'#'/mnt/d/vgmar/model_data/exp906/electrode_data/Control'#
                self.inputFolderPS ='d:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Unfiltered'#  '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Unfiltered'
                self.outputPath = 'd:\\vgmar\\model_data\\exp906\\recurrence\\Control'#'/mnt/d/vgmar/model_data/exp906/recurrence/Control' #
            #
            self.saveRecurrenceAnalysis=False
            self.catheterFile ='exp906c04_CatheterData.catG'
            self.tBegin = 5000
            self.tEnd = 10000
            self.psFile = 'exp906c04_pstracker_th0.txt'
            self.scale = 0.2
            self.sampleShift = 5
            self.theilerAFCLNumber = 0.5
            self.expectedAFRecurrenceRate = 1
            self.thresholds = [-1,0,0.05,0.15]
            self.minLineAFCL=True
            self.saveMat=False
            
    args = args()
    main(args)
    '''
                
    
    
    
    