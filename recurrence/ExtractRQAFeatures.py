# -*- coding: utf-8 -*-
"""
Created on Thu Jun 24 16:13:51 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys

import numpy as np
try:
    import pickle5 as pickle
except:
    import pickle

import pandas as pd
import scipy.io as sio

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import UserInterfaceFunctions as ui
import egm_processing as egmp
from RecurrenceComputation import RecurrenceComputation


def main(args):
    print('Loading catheter data...')
    # Load catheter data
    with open(os.path.join(args.pathCat,args.catheterFile), 'rb') as output:
        CGroup = pickle.load(output)
    #
    if args.tEnd==-1:
        args.tEnd=CGroup.Catheters[list(CGroup.Catheters.keys())[0]].EGMData.shape[0]
    #
    print('Time interval: %i to %i ms'%(args.tBegin,args.tEnd))
    #
    # Get specific time points
    for i,key in enumerate(CGroup.Catheters):
        Elec = CGroup.Catheters[key]
        Elec.EGMData = Elec.EGMData[args.tBegin:args.tEnd,:]
        
        for j,actList in enumerate(Elec.Activations):
            actList =np.asarray(actList)
            Elec.Activations[j] = list(actList[(actList>=args.tBegin) * (actList<args.tEnd)]-args.tBegin)
        CGroup.Catheters[key] = Elec
    #
    print('Loading PS data...')
    # Load phase singularity data

    ########################################################################## 
    # Get unique PS trajectories and their durations
    PS_DF = pd.read_csv(os.path.join(args.psPath,args.psFile),sep=' ',
                    dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                    header = 1,names=['t','x','y','z','traj'],engine='c',
                    skipinitialspace=True,skiprows=1)
    ## Adjust scale
    PS_DF['x'] *= args.scale
    PS_DF['y'] *= args.scale
    PS_DF['z'] *= args.scale
    #
    ## Get specific time points
    PS_DF = PS_DF.query('t>=%i and t<%i'%(args.tBegin,args.tEnd))
    
    unTraj = np.unique(PS_DF['traj'])
    #
    trajDurations = np.zeros((len(unTraj),2),dtype=int)
    #
    for i,traj in enumerate(unTraj):
        traj_df = PS_DF.query('traj=='+str(traj))
        uniqueTimes = np.unique(traj_df['t'])
        trajDurations[i,:] = [uniqueTimes.min(), uniqueTimes.max()]
    
    if not args.loadPSFiles:
        print('Calculating proximity between PSs and catheters...')
        #
        #Calculate distances between PSs and catheter centers
        proximityDict = egmp.GetDistancsPSCatheter(CGroup,PS_DF,presenceThreshold=20,
                                                    initialTime=trajDurations.min(),
                                                    endTime=trajDurations.max())        
        if args.savePSFiles:
            print('Saving PS distance analysis')
            f = open(os.path.join(args.psPath,
                                args.catheterFile[:9]+"_PSDistances_%d_%d.pkl"%(args.tBegin,args.tEnd)),"wb")
            pickle.dump(proximityDict,f)
            f.close()  
    else:
        with open(os.path.join(args.psPath,args.catheterFile[:9]+"_PSDistances_%d_%d.pkl"%(args.tBegin,args.tEnd)), 'rb') as output:
            proximityDict = pickle.load(output)
    
    ClosePresence,NearPresence = egmp.PSProximity(proximityDict,trajDurations,thresholds=[7,20])

    #
    ##########################################################################
    # Recurrence analysis
    print('Doing recurrence analysis')
    ## Parameters
    recComp = RecurrenceComputation()
    recComp.SampleShift = args.sampleShift
    recComp.TheilerAFCLNumber = args.theilerAFCLNumber
    recComp.ExpectedAFRecurrenceRate = args.expectedAFRecurrenceRate# (RRmax)
    #
    ## Get distance matrices
    distanceMatrixDict = {}
    activationPhaseSignalDict = {}
    estimatedAFCLDict = {}
    #
    for catheter in CGroup.Catheters.keys():
        Elec = CGroup.Catheters[catheter]
        #
        distanceMatrix, activationPhaseSignal, activationCycleLength  = \
            recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
        estimatedAFCL = np.median(activationCycleLength)
        estimatedAFCL = 1e-3*estimatedAFCL
        #
        #Put in dicts
        distanceMatrixDict[catheter] = distanceMatrix
        activationPhaseSignalDict[catheter] = activationPhaseSignal
        estimatedAFCLDict[catheter] = estimatedAFCL
    #
    ## IF save
    if args.saveRecurrenceAnalysis:
        if not args.saveMat:
            print('Saving recurrence analysis')
            f = open(os.path.join(args.outputPath,
                                args.catheterFile[:9]+"_RecurrenceAnalyses_%d_%d.pkl"%(args.tBegin,args.tEnd)),"wb")
            pickle.dump(distanceMatrixDict,f)
            pickle.dump(activationPhaseSignalDict,f)
            pickle.dump(estimatedAFCLDict,f)
            f.close()
        else:
            sio.savemat(os.path.join(args.outputPath,args.catheterFile[:9]+"_distanceMatrices.mat"),
                        distanceMatrixDict)
            sio.savemat(os.path.join(args.outputPath,args.catheterFile[:9]+"_activationPhase.mat"),
                        activationPhaseSignalDict)
            sio.savemat(os.path.join(args.outputPath,args.catheterFile[:9]+"_estimatedAFCL.mat"),
                        estimatedAFCLDict)
    #
    ##########################################################################
    # Get features from RPs generated with different thresholds  
    print('Calculating features...')
    # Features to be used as columns for a dataframe
    featCols = ['key','perc_near','perc_under','Mean_AFCL']
    for th in args.thresholds:
        if th==-1: 
            s='_Auto'
            featCols.append('TH_Auto')
        else:
            s = str(int(th*100))
        featCols.append('RR'+s)
        featCols.append('DET'+s)
        featCols.append('Lmax'+s)
        featCols.append('ENT'+s)
        featCols.append('LAM'+s)
        featCols.append('TT'+s)
        featCols.append('MeanDiag'+s)
        featCols.append('MedDiag'+s)
    #
    Features = pd.DataFrame(columns =featCols )
    for k,key in enumerate(distanceMatrixDict):
        DM = distanceMatrixDict[key]
        estimatedAFCL = estimatedAFCLDict[key]

        if args.minLineAFCL:
            minLineLength = np.round(estimatedAFCL*CGroup.Catheters[key].SamplingFrequency/recComp.SampleShift)
        else:
            minLineLength = 2
        #
        binNearPresence = NearPresence!=0
        binClosePresence = ClosePresence!=0
        feat_row={'key':key,
                'perc_near':np.sum(binNearPresence[:,k])/NearPresence.shape[0]*100,
                'perc_under':np.sum(binClosePresence[:,k])/ClosePresence.shape[0]*100,
                'Mean_AFCL':np.mean(estimatedAFCLDict[key])}
        offset=4
        for th in args.thresholds:
            if th==-1: #Automatic threshold
                RP, _, recurrenceThreshold, _ = \
                    recComp.ComputeRecurrencePlot(DM, Elec.SamplingFrequency, estimatedAFCL)
                RP = recComp.ErodeRP(RP,DM)        
            
                rqa,diag = recComp.ComputeRQA(RP,estimatedAFCL,Elec.SamplingFrequency,
                                              minDiagonalLine = int(minLineLength))
                tmp= {'TH_Auto':recurrenceThreshold}
                rqa = {**tmp,**rqa}
            else:
                RP, _, recurrenceThreshold, _ = \
                    recComp.ComputeRecurrencePlot(DM, Elec.SamplingFrequency, 
                                                  estimatedAFCL,recurrenceThreshold=th)
                rqa,diag = recComp.ComputeRQA(RP,estimatedAFCL,Elec.SamplingFrequency,
                                              minDiagonalLine = int(minLineLength))
            #
            rqa['mean_diag'] = np.mean(diag)
            rqa['median_diag'] = np.median(diag)
            for i,key in enumerate(rqa):
                feat_row[featCols[i+offset]] = rqa[key] 
            offset += len(rqa)
        #
        Features = Features.append(feat_row,ignore_index=True)
    #
    ##########################################################################
    ## Save
    print('Saving features')
    Features.to_csv(os.path.join(args.outputPath,args.catheterFile[:9]+'_RQAFeatures_%d_%d.csv'%(args.tBegin,args.tEnd)))
    print('Done!')

if __name__=='__main__':
    
    parser = ui.MyParser(description='Get RQA features from catheter data with respect to the position of PSs.\
                         The thresholds must be set manually, and features are calculated for the recurrent plots\
                              obtained in all cases',
                        fromfile_prefix_chars='+',
                        usage = 'python ExtractRQAFeatures.py +ParameterFile.par [PARAMETERS] catheterFile psFile')

    # Essential files
    parser.add_argument('catheterFile',action='store',type=str,
                    help = 'File containing the EGM data in .catG format')
    parser.add_argument('psFile',action='store',type=str,
                    help = 'Text file containing the rotor positions.')
    
    # Paths
    parser.add_argument('-pathCat',action='store',type=str,default='.',
                    help = 'Path to catheter file')
    parser.add_argument('-psPath',action='store',type=str,default='.',
                    help = 'Path to PS file')
    
    parser.add_argument('-outputPath',action='store',type=str,default='.',
                    help = 'Path to output')
    # Timing and spacing
    parser.add_argument('-tBegin',
                        action='store',default =2500,type=int,
                        help = 'Initial time (ms). Default 2500')
    parser.add_argument('-tEnd',
                        action='store',default =-1,type=int,
                        help = 'Final time (ms). Default -1')
    parser.add_argument('-scale',action='store',default =0.2,
                        type=float,
                        help = 'Scale for the PS file. Default 0.2 mm')
    # Recurrence analysis parameters
    parser.add_argument('-sampleShift',action='store',
                        default =5,type=np.int,
                        help = 'Sample shift. Default 5')
    parser.add_argument('-theilerAFCLNumber',action='store',
                        default =0.5,type=float,
                        help = 'TODO theilerAFCLNumber. Default 0.5')
    parser.add_argument('-expectedAFRecurrenceRate',action='store',
                        default =0.5,type=float,
                        help = 'Expected recurrence rate. Default 1')
    parser.add_argument('--saveRecurrenceAnalysis',action='store_true',
                        help = 'Saves the recurrence analysis')
    parser.add_argument('--saveMat',action='store_true',
                        help = 'Saves the recurrence analysis in Matlab')
    parser.add_argument('--minLineAFCL',action='store_true',
                        help = 'If true, determines the minimum line length by the AFCL')
    # RP and feature related
    parser.add_argument('-thresholds',nargs='+',type=float,
                        default = [-1],
                        help='Thresholds for making recurrence plots. A threshold of -1 means that \
                        an automatic threshold will be defined by Stef\'s method Default [-1]')  
    # PS related
    parser.add_argument('--loadPSFiles',action='store_true',
                        help = 'If true, does not calculate the PS distances, but loads from files')
    parser.add_argument('--savePSFiles',action='store_true',
                        help = 'If true, saves files with PS distances')

    args = parser.parse_args()
    if type(args.thresholds) is float:
        args.thresholds = [args.thresholds]
    
        
    main(args)

    '''
    #For debugging
    class args:
        def __init__(self,opt='wsl'):
            if opt=='wsl':
                self.pathCat = 'd:\\vgmar\\model_data\\exp906\\electrode_data\\Control'#'/mnt/d/vgmar/model_data/exp906/electrode_data/Control'#
                self.psPath ='d:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Unfiltered'#  '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Unfiltered'
                self.outputPath = 'd:\\vgmar\\model_data\\exp906\\recurrence\\Control'#'/mnt/d/vgmar/model_data/exp906/recurrence/Control' #
            #
            self.saveRecurrenceAnalysis=False
            self.catheterFile ='exp906c04_CatheterData.catG'
            self.tBegin = 5000
            self.tEnd = 10000
            self.psFile = 'exp906c04_pstracker_th0.txt'
            self.scale = 0.2
            self.sampleShift = 5
            self.theilerAFCLNumber = 0.5
            self.expectedAFRecurrenceRate = 1
            self.thresholds = [-1,0,0.05,0.15]
            self.minLineAFCL=True
            self.saveMat=False
            
    args = args()
    main(args)
    '''
                
    
    
    
    