# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 12:32:45 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import numpy as np
import os,sys
# # import scipy.io as sio
import pickle5 as pickle
import matplotlib.pyplot as plt
# # from numba import jit
# from scipy.spatial.distance import cdist,squareform
# # import multiprocessing
# # from joblib import Parallel, delayed
# # import time
# # import scipy.signal as sig
import pandas as pd

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
sys.path.append(os.path.join(upperFolder,'recurrence'))
# import IgbHandling as igb
# # from CatheterObjects import CatheterClass,CatheterGroup
import egm_processing as egmp
import quick_visualization as qv
# # from RecurrenceComputation import RecurrenceComputation
# # from detect_peaks import detect_peaks


# import vtk

# # from vtkmodules.numpy_interface import dataset_adapter as dsa
# from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray,vtk_to_numpy
# from StandardVTKObjects import *
# # from DataClasses import PSCluster,PStrackerResults
from RecurrenceComputation import RecurrenceComputation

experimentName = 'exp906c74'
tBegin=500
tEnd=15000
#%% Load electrode Data

pathElec = 'D:\\vgmar\\model_data\\exp906\\electrode_data\\Control'
with open(os.path.join(pathElec,experimentName+'_CatheterData.catG'), 'rb') as output:
    EGroup = pickle.load(output)
Fields = list(EGroup.Catheters.keys())
 

for i,key in enumerate(EGroup.Catheters):
    Elec = EGroup.Catheters[key]
    Elec.EGMData = Elec.EGMData[tBegin:tEnd,:]
    
    for j,actList in enumerate(Elec.Activations):
        actList =np.asarray(actList)
        Elec.Activations[j] = list(actList[(actList>=tBegin) * (actList<tEnd)]-tBegin)
    EGroup.Catheters[key] = Elec

#%% Phase singularity processing

## Load data
psPath = 'D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered'#'\\th700'
scale = 0.2

PS_DF = pd.read_csv(os.path.join(psPath,experimentName+'_pstracker_th100.txt'),sep=' ',
                    dtype = {'t':int, 'x':float, 'y':float, 'z':float,'traj':int},
                 header = 1,names=['t','x','y','z','traj'],engine='c',
                 skipinitialspace=True,skiprows=1)


PS_DF['x'] *= scale
PS_DF['y'] *= scale
PS_DF['z'] *= scale

PS_DF = PS_DF.query('t>='+str(tBegin)+' and t<'+str(tEnd))

## Get duration of trajectories and simultaneous presence of SPs

# Unique trajectories and time instants
unTraj = np.unique(PS_DF['traj'])
unTimes = np.unique(PS_DF['t'])
initTime=unTimes[0]

# Put durations in array with first column being begin and second being the end of the trajectory
# Traj presence is the binary matrix of trajectory presence, based on the times of trajDurations
trajDurations = np.zeros((len(unTraj),2),dtype=int)
trajPresence = np.zeros((len(unTraj),len(unTimes)))

for i,traj in enumerate(unTraj):
    traj_df = PS_DF.query('traj=='+str(traj))
    uniqueTimes = np.unique(traj_df['t'])
    trajDurations[i,:] = [uniqueTimes.min(), uniqueTimes.max()]
    lims = trajDurations[i,:] - initTime
    trajPresence[i,lims[0]:lims[1]+1] +=1

#Number of simultaneous SPs
numSimPSs=np.sum(trajPresence,axis=0)


## Calculate distances between PSs and electrode centers
proximityDict = egmp.GetDistancsPSCatheter(EGroup,PS_DF,presenceThreshold=-1,
                                            initialTime=trajDurations.min(),
                                            endTime=trajDurations.max())
# with open(os.path.join(psPath,experimentName+"_PSDistances.pkl"), 'wb') as output:
#             pickle.dump(proximityDict,output)



# with open(os.path.join(psPath,experimentName+"_PSDistances.pkl"), 'rb') as output:
#     proximityDict = pickle.load(output)

ClosePresence,NearPresence = egmp.PSProximity(proximityDict,trajDurations,thresholds=[7,20])

## Source presence heatmap
# HM=egmp.MakeHeatmap(EGroup.Anatomy,PS_DF)
#%% Update of the block above (something not working properly)
psPath = 'D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered'


InputDF = pd.read_csv(os.path.join(psPath,experimentName+'_PSPositionsAndDistances.csv') ,index_col=0)
InputDF = InputDF.loc[(InputDF['t']>= tBegin) & (InputDF['t']< tEnd),:]
InputDF[InputDF<0] = np.nan
InputDF = InputDF.dropna()

unTraj = np.unique(InputDF['traj'])

proximityDict = {cat:{} for cat in EGroup.Catheters.keys()}
trajDurations = {}
for i,traj in enumerate(unTraj):
    print('Trajectory %d'%i)
    TrajDF = InputDF.loc[InputDF['traj']==traj,:]
    if TrajDF['t'].max()-TrajDF['t'].min()>=100:
        TrajDF = [np.mean(TrajDF.loc[TrajDF['t']==t,:]) for t in np.unique(TrajDF['t'])]
        TrajDF = pd.DataFrame(TrajDF,columns=InputDF.columns)
        trajDurations[traj] = [TrajDF['t'].min(),TrajDF['t'].max()]
        for cat in EGroup.Catheters.keys():
            if (TrajDF['D'+cat]<=20).any():
                proximityDict[cat][traj] = TrajDF['D'+cat]
        


#%% Recurrence Portion (Distance matrices atm.)

## Parameters
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1# (RRmax)

## 
distanceMatrixDict = {}
activationPhaseSignalDict = {}
estimatedAFCLDict = {}

for catheter in EGroup.Catheters.keys():
    Elec = EGroup.Catheters[catheter]
    
    distanceMatrix, activationPhaseSignal, activationCycleLength  = \
        recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
    estimatedAFCL = np.median(activationCycleLength)
    estimatedAFCL = 1e-3*estimatedAFCL
    
    #Test
    recurrenceMatrix, recurrenceTime, recurrenceThreshold, distanceMatrix = \
        recComp.ComputeRecurrencePlot(distanceMatrix, Elec.SamplingFrequency, estimatedAFCL)
    #Put in dicts
    distanceMatrixDict[catheter] = distanceMatrix
    activationPhaseSignalDict[catheter] = activationPhaseSignal
    estimatedAFCLDict[catheter] = estimatedAFCL

# # # Save to read in notebook
# f = open(experimentName+"_RecurrenceAnalyses.pkl","wb")
# pickle.dump(distanceMatrixDict,f)
# pickle.dump(activationPhaseSignalDict,f)
# pickle.dump(estimatedAFCLDict,f)
# f.close()

#%% Some pictures

## Signals
Phase,_ = egmp.ComputeActivationPhaseSignal(EGroup.Catheters['ILA_2'])
qv.HdGridPlot(EGroup.Catheters['ILA_2'],timeInterval = [0,1000],plotActivations = True,
              plotPhase=True,phaseSig=Phase)

# qv.SignalAndActivations(EGroup.Catheters['ILA_2'],initialTime=5000)

## PS related only
_,_ = qv.SimultaneousPSs(trajDurations,[unTimes.min(),unTimes.max()])
qv.PSSurvival(np.asarray([trajDurations[key] for key in trajDurations]),msAxis=False)

fig,ax = qv.PSDistance(proximityDict,trajDurations,[7,20])

fig = qv.PSPresence(EGroup,ClosePresence,NearPresence,initialTime=2500,endTime=15000,msAxis=True)

HM = egmp.MakeHeatmap(EGroup.Anatomy,PS_DF)
# renWin = qv.PSHeatmap(EGroup.Anatomy,HM,filter=False)

## Recurrence related only
PlotCats = ['ILA_2','ARA']#,'IRA','SRA','RAA','ARA','PECT','PLA']

for catheter in PlotCats:
    fig,ax = plt.subplots(1,figsize = [9,9])
    ax.imshow(distanceMatrixDict[catheter],vmin=0,vmax=1,cmap='jet',
              origin='lower',extent = [tBegin,tEnd,tBegin,tEnd])
    ax.set_xlabel('Time (ms)',fontsize=20)
    ax.set_ylabel('Time (ms)',fontsize=20)
    fig.suptitle('Distance matrix - %s'%catheter,fontsize=22)
    ax.set_xlim([tBegin,tEnd])
    ax.set_ylim([tBegin,tEnd])

fig,ax = plt.subplots(4,4,figsize = [9,9])
for i,catheter in enumerate(EGroup.Catheters.keys()):
    ax[i//4,i%4].imshow(distanceMatrixDict[catheter],vmin=0,vmax=1,cmap='jet',
          origin='lower',extent = [tBegin,tEnd,tBegin,tEnd])
    ax[i//4,i%4].set_title(catheter)

#%%
## Both
qv.PSandDistanceMatrix('ILA_2',proximityDict,trajDurations,[7,20],
                       distanceMatrixDict,[tBegin,tEnd])
qv.PSandDistanceMatrix('ILA_3',proximityDict,trajDurations,[7,20],
                       distanceMatrixDict,[tBegin,tEnd])
qv.PSandDistanceMatrix('LPV',proximityDict,trajDurations,[7,20],
                       distanceMatrixDict,[tBegin,tEnd])
qv.PSandDistanceMatrix('PLA',proximityDict,trajDurations,[7,20],
                       distanceMatrixDict,[tBegin,tEnd])
qv.PSandDistanceMatrix('LA_Roof',proximityDict,trajDurations,[7,20],
                       distanceMatrixDict,[tBegin,tEnd])


#%% Aditional specific plots
DM = distanceMatrixDict['PLA']

tBegin = 0
tEnd = 10000

DM = DM[int(tBegin/recComp.SampleShift):int(tEnd/recComp.SampleShift),
        int(tBegin/recComp.SampleShift):int(tEnd/recComp.SampleShift)]
estimatedAFCL = estimatedAFCLDict[key]
minLineLength = np.round(estimatedAFCL*1000/recComp.SampleShift)

RP, _, recurrenceThreshold, _ = recComp.ComputeRecurrencePlot(DM,1000,estimatedAFCL)


rqa,diag = recComp.ComputeRQA(RP,minDiagonalLine=minLineLength)

print(np.median(diag))
print(rqa)
plt.figure()
plt.imshow(RP,origin='lower',cmap='gray_r')

        
#%%
i =0
catheter=Fields[i]
estimatedAFCL=1000*estimatedAFCLDict[catheter]

FarMatrix = np.logical_or(NearPresence,ClosePresence)
NearMatrix = np.logical_and(NearPresence,np.logical_not(ClosePresence))
CloseMatrix = np.logical_and(ClosePresence,np.logical_not(NearPresence))

BlocksFar,_ =  egmp.PSBlocks(FarMatrix[:,i],kernelSize = estimatedAFCL,reverseOutput=True)
BlocksNear,_ =  egmp.PSBlocks(NearMatrix[:,i],kernelSize = estimatedAFCL,reverseOutput=False)
BlocksClose,_ =  egmp.PSBlocks(CloseMatrix[:,i],kernelSize = estimatedAFCL,reverseOutput=False)


# ## Test to remove close from near
prNear = np.zeros(len(NearMatrix))
prClose = np.zeros(len(CloseMatrix))
for block in BlocksNear:
    prNear[block[0]:block[1]]=1
    prClose[block[0]:block[1]]=-1
    

for block in BlocksClose:
    prNear[block[0]:block[1]]=0
    prClose[block[0]:block[1]]+=2

prClose = np.array(prClose>0,dtype=int)

BlocksNear = egmp.ChangeDetection(prNear)
BlocksClose = egmp.ChangeDetection(prClose)
##


DM = distanceMatrixDict[catheter]
RP,_,recThreshold,_ = recComp.ComputeRecurrencePlot(DM,1000,estimatedAFCLDict[catheter])

fig,ax = plt.subplots(1)
TimeAxis=np.linspace(5e3,10e3,len(ClosePresence))

ax.imshow(RP,origin='lower',cmap='gray_r',extent = [TimeAxis[0],TimeAxis[-1],TimeAxis[0],TimeAxis[-1]])

for block in BlocksFar:
    block = TimeAxis[block]
    rectangle = plt.Rectangle((block[0],block[0]),block[1]-block[0],block[1]-block[0],
                              facecolor='None',edgecolor='g')
    ax.add_patch(rectangle)
    
for block in BlocksNear:
    block = TimeAxis[block]
    rectangle = plt.Rectangle((block[0],block[0]),block[1]-block[0],block[1]-block[0],
                              facecolor='None',edgecolor='orange')
    ax.add_patch(rectangle)
    
for block in BlocksClose:
    block = TimeAxis[block]
    rectangle = plt.Rectangle((block[0],block[0]),block[1]-block[0],block[1]-block[0],
                              facecolor='None',edgecolor='r')
    ax.add_patch(rectangle)

block = np.array(BlocksFar[0])//recComp.SampleShift
DM_block = DM[block[0]:block[1],block[0]:block[1]]

RP,_,_,_ = recComp.ComputeRecurrencePlot(DM_block,1000,estimatedAFCLDict[catheter])#,recThreshold)
# RP = recComp.ErodeRP(RP,DM_block)
plt.figure()
plt.imshow(RP,origin='lower',cmap='gray_r')
