# -*- coding: utf-8 -*-
"""
Created on Tue Dec  8 15:42:17 20'20

Description: Contains functions to perform recurrence analysis on EGM data

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import sys,os
import numpy as np
from scipy.cluster.hierarchy import dendrogram, linkage,fcluster
from scipy.spatial.distance import pdist,squareform

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
from egm_processing import ComputeActivationPhaseSignal
from detect_peaks import detect_peaks
import multiprocessing as mp
from itertools import repeat


class RecurrenceComputation:
    def __init__(self):
        


        self.DistanceFunction = self.distance_numba  #distance measure for distance matrix
        self.SampleShift = 5;                           # distance between consecutive points in the recurrence computation
        self.TheilerAFCLNumber = 0.5;                     # ignore the diagonal (relative to AFCL)
        self.ExpectedAFRecurrenceRate = 1;              #Expected number of recurrences per AF cycle
        self.MinimumBlockAFCLRecurrenceRate = 0.9;      #minimum RR for repetitive diagonal block
        self.BlockGridAFCLResolution = 1;             #resolution of grid search for diagonal blocks
        self.MinimumBlockAFCLDuration = 1;              #minimum duration of a diagonal block
    
    def distance_numba(self,x,y):
            return 1 - (1 + np.sum(np.cos(x-y))/len(x)) / 2

    def intersect_mtlb(self,a, b): # Surrogate for matlabs intersect
        #https://stackoverflow.com/questions/45637778/how-to-find-intersect-indexes-and-values-in-python
        a1, ia = np.unique(a, return_index=True)
        b1, ib = np.unique(b, return_index=True)
        aux = np.concatenate((a1, b1))
        aux.sort()
        c = aux[:-1][aux[1:] == aux[:-1]]
        
        return c, ia[np.isin(a1, c)], ib[np.isin(b1, c)]
    
    def ComputeDistanceMatrix(self, electrodeObject, intrinsicDeflections):

    
        activationPhaseSignal, activationCycleLength = ComputeActivationPhaseSignal(electrodeObject, intrinsicDeflections)
        
        
        numberOfSamples = electrodeObject.GetNumberOfSamples()
        snapshotSampleIndices = np.arange(0,numberOfSamples,self.SampleShift)
        selectedActivationPhaseSignal = activationPhaseSignal[snapshotSampleIndices, :]
        
        distanceMatrix = squareform(pdist(selectedActivationPhaseSignal, self.DistanceFunction))
       
        return distanceMatrix, activationPhaseSignal, activationCycleLength                   
    
    def ComputeRecurrencePlot(self, distanceMatrix, samplingFrequency, estimatedAFCL,recurrenceThreshold=-1.):
        timeShift = self.SampleShift / samplingFrequency
        afclRecurrenceRate =  timeShift / estimatedAFCL        
        
        # blank the diagonal
        theilerNumber = np.round(self.TheilerAFCLNumber * estimatedAFCL / timeShift).astype(int)
        maskedDistanceMatrix = np.triu(distanceMatrix, theilerNumber) + np.tril(distanceMatrix, -theilerNumber)
        maskedDistanceMatrix[maskedDistanceMatrix == 0] = np.nan

        
        if recurrenceThreshold==-1:
            validDistances = ~np.isnan(maskedDistanceMatrix)
            recurrenceThreshold = np.percentile(maskedDistanceMatrix[validDistances],
                                            100 * self.ExpectedAFRecurrenceRate * afclRecurrenceRate)
        
        numberOfSnapShots = distanceMatrix.shape[0]
        snapShotTime = timeShift * (np.arange(0,(numberOfSnapShots)))
        invalidTimePoints = (np.isnan(distanceMatrix + np.diag(np.empty(shape = (numberOfSnapShots))*np.nan))).all(axis = 0)
        distanceMatrix = distanceMatrix[~invalidTimePoints, :]
        distanceMatrix = distanceMatrix[:,~invalidTimePoints]
        recurrenceTime = snapShotTime[~invalidTimePoints]
        recurrenceMatrix = distanceMatrix < recurrenceThreshold
         
        return recurrenceMatrix, recurrenceTime, recurrenceThreshold, distanceMatrix
    
    def ErodeParallelLoop(self,Column,minimumDistance,distanceColumn):
        thisLine = np.append(0,np.append(Column,0)) # add 0 for when first index is 0
        thisLineDiff = np.diff(thisLine)
        changeInds= np.where(thisLineDiff)[0]
        if len(changeInds)!=0:
            thisLineDown = np.where(thisLineDiff < 0)[0]
            thisLineUp = np.where(thisLineDiff > 0)[0]
            erodedIndex = np.zeros(len(thisLineDown))
            for k in range(len(thisLineDown)):
                cluster = np.arange(thisLineUp[k], thisLineDown[k])
                if minimumDistance:
                    block  = distanceColumn[cluster]
                    try:
                        minIndex = np.where(block==np.nanmin(block))[0][0]
                    except Exception as e:
                        minIndex=0 #all nan slice
                    erodedIndex[k] = cluster[minIndex]
                else:
                    erodedIndex[k]= np.floor(np.mean(cluster))-1 # Subtract 1 because we added a zero earlier
            erodedIndex = np.asarray(erodedIndex,dtype=int)
            return erodedIndex
        else:
            return None # Not sure if I need this

    def ErodeRP(self,RP,distanceMatrix=None,linear=False):
        # mp.freeze_support()
        if distanceMatrix is None:
            minimumDistance=False
            erodedMatrix = np.zeros_like(RP)
            if not linear:
                with mp.Pool(mp.cpu_count()-3) as pool:
                    Output = pool.starmap(self.ErodeParallelLoop, zip([RP[:,columnIndex] for columnIndex in range(RP.shape[-1])],
                                                                    repeat(minimumDistance),
                                                                    repeat(None),))
            else:
                Output = list()
                for Column,minimumDistance,distanceColumn in  zip([RP[:,columnIndex] for columnIndex in range(RP.shape[-1])],
                                                                    repeat(minimumDistance),
                                                                    repeat(None),):
                    Output.append(self.ErodeParallelLoop(Column,minimumDistance,distanceColumn))
        else:
            minimumDistance=True
            distanceColumns = [distanceMatrix[:,columnIndex] for columnIndex in range(distanceMatrix.shape[-1])]
            #
            erodedMatrix = np.zeros_like(RP)
            if not linear:
                with mp.Pool(mp.cpu_count()-3) as pool:
                    Output = pool.starmap(self.ErodeParallelLoop, zip([RP[:,columnIndex] for columnIndex in range(RP.shape[-1])],
                                                                    repeat(minimumDistance),
                                                                   distanceColumns,))
            else:
                Output = list()
                for Column,minimumDistance,distanceColumn in  zip([RP[:,columnIndex] for columnIndex in range(RP.shape[-1])],
                                                                    repeat(minimumDistance),
                                                                    distanceColumns,):
                    Output.append(self.ErodeParallelLoop(Column,minimumDistance,distanceColumn))
        #
        for ii,out in enumerate(Output):
            if out is not None: 
                erodedMatrix[out,ii] = 1

        return erodedMatrix
    
    def ComputeRQA(self,RP,estimatedAFCL,samplingFrequency,minDiagonalLine=2,normByAFCL = True,maxLineThickness = 2):
        '''
        RQA based on the code by: Hui Yang (2021). Tool box of recurrence plot 
        and recurrence quantification analysis, MATLAB Central File Exchange. Retrieved June 24, 2021. 

        Another good reference is: http://www.recurrence-plot.tk/rqa.php

        Parameters
        ----------
        RP : TYPE
            Recurrence plot, as N,N matrix
        minDiagonalLine : TYPE, optional
            Minimum diagonal line length. The default is 2.

        Returns
        -------
        rqa_stat : dict
            Dictionary with the following keys:
            - recurrence_rate: the percentage of recurrence points in an RP
            - determinism: the percentage of recurrence points which form diagonal lines
            - longest_diagonal: The length of the longest diagonal line
            - entropy: the Shannon entropy of the probability distribution of the diagonal line lengths p(l)
            - laminarity: the percentage of recurrence points which form vertical lines
            - trapping_time: The average length of the vertical lines
        diag : np array
            diagonal line length for every diagonal line. Can be used to compute mean or median
        '''
        
        ## Blank diagonal
        timeShift = self.SampleShift / samplingFrequency
        theilerNumber = int(self.TheilerAFCLNumber * estimatedAFCL / timeShift)
        maskedRP = np.triu(RP, theilerNumber) + np.tril(RP, -theilerNumber)
        # maskedRP[maskedRP==0] = np.nan

        
        xi,yi = np.where(maskedRP)
        recurrpt = np.asarray([xi,yi]).T

        if len(recurrpt)==0:
            diag = -1
            rqa_stat = {'recurrence_rate':-1,
                    'determinism':-1,
                    'longest_diagonal':-1,
                    'entropy':-1}
        
            return rqa_stat
        
        # RR computation
        RR = 100*len(recurrpt)/np.prod(RP.shape)
        
        # Determinism
        ## Search for diagonal lines
        k = 1
        diagCode = np.zeros_like(maskedRP,dtype=int)
        for i in range(len(recurrpt)):
            if maskedRP[xi[i]-maxLineThickness:xi[i],
                        yi[i]-maxLineThickness:yi[i]+maxLineThickness+1].any(): # some space for not perfectly diagonal lines
                codes = np.unique(diagCode[xi[i]-maxLineThickness:xi[i],
                                            yi[i]-maxLineThickness:yi[i]+maxLineThickness+1])
                diagCode[xi[i],yi[i]] = codes[codes!=0][0]
            else:
                diagCode[xi[i],yi[i]] = k
                k = k+1
        
        lineCodes, lineCounts = np.unique(diagCode.flatten(),return_counts = True)
        minLengthBool = lineCounts>minDiagonalLine
        lineCodes = lineCodes[minLengthBool][1:] # ignore zeros, get lines longer than minDiagonalLine
        lineCounts = lineCounts[minLengthBool][1:] # ignore zeros
        
        if len(lineCounts)!=0:
            DET = 100*np.sum(lineCounts)/len(recurrpt)
            LMAX = np.max(lineCounts)
            LMEAN = np.mean(lineCounts)
            LMED = np.median(lineCounts)
            
            # Shannon Entropy
            uniqueLengths,countLengths = np.unique(lineCounts,return_counts = True)
            Pl = countLengths/np.sum(countLengths) 
            
            ENT= -np.sum(Pl*np.log(Pl))            
        else:
            DET = -1

        NAFCL = maskedRP.shape[0]/(estimatedAFCL*1000/self.SampleShift) # all in ms

        if normByAFCL:
            
            if DET==-1:
                rqa_stat = {'recurrence_rate':len(recurrpt)/(maskedRP.shape[0]**2)*(estimatedAFCL*1000/self.SampleShift),
                            'determinism':DET,
                            'longest_diagonal':-1,
                            'mean_diagonal':-1,
                            'median_diagonal':-1,
                            'entropy':-1,
                            'NAFCL':-1}
            else:
                rqa_stat = {'recurrence_rate':len(recurrpt)/(maskedRP.shape[0]**2)*(estimatedAFCL*1000/self.SampleShift),
                            'determinism':DET,
                            'longest_diagonal':LMAX/(estimatedAFCL*samplingFrequency/self.SampleShift),
                            'mean_diagonal':LMEAN/(estimatedAFCL*samplingFrequency/self.SampleShift),
                            'median_diagonal':LMED/(estimatedAFCL*samplingFrequency/self.SampleShift),
                            'entropy':ENT,
                            'NAFCL':NAFCL}
        else:
            rqa_stat = {'recurrence_rate':RR,
                        'determinism':DET,
                        'longest_diagonal':LMAX,
                        'mean_diagonal':LMEAN,
                        'median_diagonal':LMED,
                        'entropy':ENT,
                        'NAFCL':NAFCL}    

        return rqa_stat

    def BlocksParallelLoop(self,sampleIndex,numberOfSamples,gridTriangleSizes,numberOfTriangleGridPoints,recurrenceMatrix,estimatedAFCL):
            #
        print(sampleIndex)
        sampleMaximumTriangleSize = np.min([sampleIndex -1, numberOfSamples - sampleIndex]) ##
        numberOfSampleTriangleGridPoints = np.sum(gridTriangleSizes <= sampleMaximumTriangleSize)##
        sampleTriangleRecurrenceRateCorrected = np.zeros((numberOfTriangleGridPoints))
        sampleTriangleAFCL = np.zeros((numberOfTriangleGridPoints))
        for triangleSizeIndex in range(numberOfSampleTriangleGridPoints): 
            triangleSize = gridTriangleSizes[triangleSizeIndex]
            triangleInterval = np.arange(sampleIndex-triangleSize, sampleIndex+triangleSize)
            triangle = recurrenceMatrix[triangleInterval,:][:, triangleInterval]
            #
            # recurrence rate without diagonal
            blockSize = 2 * triangleSize +1
            correctedRecurrenceRate = (np.sum(triangle.flatten()) - blockSize) / blockSize**2
            sampleTriangleRecurrenceRateCorrected[triangleSizeIndex] = correctedRecurrenceRate
            sampleTriangleAFCL[triangleSizeIndex] = np.mean(estimatedAFCL[triangleInterval])
        #
        return sampleTriangleRecurrenceRateCorrected,sampleTriangleAFCL

    def DetectRecurrentBlocks(self,recurrenceMatrix, recurrenceTime, estimatedAFCL):
        sampleTime = np.mean(np.diff(recurrenceTime))
        numberOfSamples = len(recurrenceTime)
        maximumTriangleSize = int(np.ceil(numberOfSamples / 2))
        gridSearchStep = int(np.ceil((np.mean(estimatedAFCL) / sampleTime) * self.BlockGridAFCLResolution))
        gridSampleIndices = np.arange(gridSearchStep,numberOfSamples,gridSearchStep,dtype=int)
        numberOfGridPoints = len(gridSampleIndices)
               
        gridTriangleSizes = np.arange(gridSearchStep,maximumTriangleSize,gridSearchStep,dtype=int)
        numberOfTriangleGridPoints = len(gridTriangleSizes)
        gridTriangleRecurrenceRateCorrected = np.zeros((numberOfGridPoints, numberOfTriangleGridPoints))
        gridTriangleAFCL = np.zeros((numberOfGridPoints, numberOfTriangleGridPoints))
                
        if len(np.asarray(estimatedAFCL).reshape(-1)) == 1:
           estimatedAFCL = np.ones_like(recurrenceTime) * np.mean(estimatedAFCL)


        with mp.Pool(mp.cpu_count()-2) as pool:
            Output = pool.starmap(self.BlocksParallelLoop, zip(gridSampleIndices, repeat(numberOfSamples),repeat(gridTriangleSizes),repeat(numberOfTriangleGridPoints),repeat(recurrenceMatrix),repeat(estimatedAFCL),))

        for ii,out in enumerate(Output):
            gridTriangleRecurrenceRateCorrected[ii] = out[0]
            gridTriangleAFCL[ii] = out[1]

        # normalize recurrence rate to AFCL
        gridTriangleRecurrenceRateCorrected = (gridTriangleAFCL / sampleTime) * gridTriangleRecurrenceRateCorrected
        # sampleMaxRecurrenceRateCorrected = np.max(gridTriangleRecurrenceRateCorrected, axis=-1)
        # maxTriangleSizeIndex = np.where(np.isin(sampleMaxRecurrenceRateCorrected,
        #                                         gridTriangleRecurrenceRateCorrected))[0]
        sampleMaxRecurrenceRateCorrected = np.amax(gridTriangleRecurrenceRateCorrected, axis=-1)
        maxTriangleSizeIndex = np.argmax(gridTriangleRecurrenceRateCorrected, axis=-1)
                
        # compute recurrence rate including diagonal
        sampleMaxRecurrenceRate = np.zeros(len(sampleMaxRecurrenceRateCorrected))
        maxTriangleSizes = gridTriangleSizes[maxTriangleSizeIndex]
        maxTriangleAFCL = np.zeros(len(sampleMaxRecurrenceRateCorrected))
        for gridSampleIndex in range(numberOfGridPoints):
            if sampleMaxRecurrenceRateCorrected[gridSampleIndex] == 0: continue
                    
            sampleIndex = gridSampleIndices[gridSampleIndex]
            triangleSize = maxTriangleSizes[gridSampleIndex]
            maxTriangleAFCL[gridSampleIndex] = gridTriangleAFCL[gridSampleIndex,
                                                                  maxTriangleSizeIndex[gridSampleIndex]]
                    
            triangleInterval = np.arange(sampleIndex-triangleSize, sampleIndex+triangleSize)
            triangle = recurrenceMatrix[triangleInterval,:][:, triangleInterval]
            sampleMaxRecurrenceRate[gridSampleIndex] = np.sum(triangle.flatten()) / np.prod(triangle.shape)
        
        # normalize recurrence rate to AFCL
        sampleMaxRecurrenceRate = (maxTriangleAFCL / sampleTime) * sampleMaxRecurrenceRate
        
        # filter out overlapping block centers (from large blocks to small)
        trianglePeakIndices= detect_peaks(maxTriangleSizes)
        trianglePeakValues = maxTriangleSizes[trianglePeakIndices]
        
        peakSampleIndices = gridSampleIndices[trianglePeakIndices]
                
        # mimimum recurrence rate is based on a purely periodic process
        # with recurrence rate of once per AFCL, corrected for sampling
        
        uncheckedPeaks = np.ones(len(trianglePeakIndices),dtype=bool)
        # check if recurrence density is high enough
        validPeaks = sampleMaxRecurrenceRate[trianglePeakIndices] > self.MinimumBlockAFCLRecurrenceRate
        uncheckedPeaks[~validPeaks] = False
        selectedPeaks = np.zeros(len(trianglePeakIndices),dtype=bool)
        triangleSpan = np.empty(len(trianglePeakIndices),dtype=object)
        
        while uncheckedPeaks.any():
            # find width of largest unchecked triangle
            uncheckedPeakPositions = np.where(uncheckedPeaks)[0]
            uncheckedPeakValues = trianglePeakValues[uncheckedPeaks]
            maxUncheckedSizeIndex = np.argmax(uncheckedPeakValues)
            maxSizeIndex = uncheckedPeakPositions[maxUncheckedSizeIndex]
            
            selectedPeaks[maxSizeIndex]= True
            triangleWidth = trianglePeakValues[maxSizeIndex]
            peakSampleIndex = peakSampleIndices[maxSizeIndex]
            triangleSpan[maxSizeIndex] = np.arange(peakSampleIndex-triangleWidth,peakSampleIndex+triangleWidth)
        
            
            # remove triangles within this triangle
            _,removeInd,_ = self.intersect_mtlb(peakSampleIndices, triangleSpan[maxSizeIndex])
            uncheckedPeaks[removeInd] = False
        
                
        # create output struct
        blockStruct = dict()
        blockStruct['recurrenceTime'] = recurrenceTime[gridSampleIndices]
        blockStruct['recurrenceRate'] = sampleMaxRecurrenceRate
        blockStruct['recurrenceRateCorrected'] = sampleMaxRecurrenceRateCorrected
        blockStruct['triangleSize'] = sampleTime * maxTriangleSizes
        blockStruct['blockDuration'] = sampleTime * ( 2 * maxTriangleSizes + 1)
        blockStruct['trianglePeakIndices'] = trianglePeakIndices
                
        if selectedPeaks.any(): # Convert to intervals
            blockStruct['selectedPeaks'] = selectedPeaks
            
            mins = np.array([np.min(T) for T in triangleSpan[selectedPeaks]])
            maxs = np.array([np.max(T) for T in triangleSpan[selectedPeaks]])
            intervalSamples = np.array([mins,maxs]).T
            
            intervalTime = recurrenceTime[intervalSamples]
            
            blockStruct['rectangles_spans'] = triangleSpan[selectedPeaks]
            blockStruct['rectangles_centers'] = peakSampleIndices[selectedPeaks]
            
            blockStruct['intervals_times'] = intervalTime
            blockStruct['intervals_samples'] = intervalSamples
            
        else: # % no rectangles in this file
            blockStruct['rectangles_spans'] = []
            blockStruct['rectangles_centers'] = []
            blockStruct['intervals_times'] = []
            blockStruct['intervals_samples'] = []
        return blockStruct
    
    def ComputeBlockLinking(self,blockIntervalIndices, recurrenceMatrix):
        numberOfBlocks = len(blockIntervalIndices)
        linkingMatrix = np.ones((numberOfBlocks,numberOfBlocks))*np.nan
        for blockIndex in range(numberOfBlocks):
            blockIndices = blockIntervalIndices[blockIndex, :]
            for crossBlockIndex in range(blockIndex,numberOfBlocks):
                crossBlockIndices = blockIntervalIndices[crossBlockIndex, :]
                recurrenceBlock = recurrenceMatrix[blockIndices[0]:blockIndices[-1],
                                                crossBlockIndices[0]:crossBlockIndices[-1]]
                linkingMatrix[blockIndex, crossBlockIndex] =np.sum(recurrenceBlock.flatten()) / np.prod(recurrenceBlock.shape)
    
        return linkingMatrix
      
    def DetectIntervalClusters(self, recurrenceMatrix, blockStruct, recurrenceTime, estimatedAFCL):
        
        blockIntervalIndices = blockStruct['intervals_samples']
        blockIntervalTimes = blockStruct['intervals_times']
        blockIntervalAFCL = np.mean(estimatedAFCL) *  np.ones(len(blockIntervalIndices))
        if len(blockIntervalAFCL) == 1:
            for blockIndex in range(len(blockIntervalIndices)):
                currentBlockIndices = np.arange(blockIntervalIndices[blockIndex,0],
                                                blockIntervalIndices[blockIndex, -1])
                blockIntervalAFCL[blockIndex] = np.mean(estimatedAFCL) ##!!! This is ignoring the currentBlockIndices 
        
                    
        # select only intervals with a substantial duration
        intervalDuration = np.diff(blockIntervalTimes, axis=1)
        intervalDurationAFCL = intervalDuration / np.mean(estimatedAFCL)
        validIntervals = intervalDurationAFCL >= self.MinimumBlockAFCLDuration
        blockIndices = np.where(validIntervals)[0]
        numberOfBlocks = len(blockIndices)
        blockIntervalIndices = blockIntervalIndices[blockIndices, :]
        blockIntervalTimes = blockIntervalTimes[blockIndices, :]
        blockIntervalAFCL = blockIntervalAFCL[blockIndices]
        
        
        linkingMatrix = self.ComputeBlockLinking(blockIntervalIndices, recurrenceMatrix)
        sampleTime = np.mean(np.diff(recurrenceTime))
        
        linkingAFCL = blockIntervalAFCL.reshape(-1,1)
        for i in range(numberOfBlocks-1): 
            linkingAFCL = np.append(linkingAFCL, blockIntervalAFCL.reshape(-1,1),axis=1)
            
        linkingAFCL = (linkingAFCL + linkingAFCL.T) / 2 
        linkingMatrix = (linkingAFCL / sampleTime) * linkingMatrix;
                    
        # hierarchical clustering        
        
        maximumLinking = np.nanmax(linkingMatrix.flatten())
        if (numberOfBlocks > 1) & (maximumLinking >= self.MinimumBlockAFCLRecurrenceRate):
            symmetricLinkage = np.triu(linkingMatrix, 1) + np.triu(linkingMatrix, 1).T
            symmetricLinkage = -maximumLinking * np.eye(numberOfBlocks) +  maximumLinking - symmetricLinkage
            symmetricLinkage[symmetricLinkage < 0] = 0
            linkageTree = linkage(squareform(symmetricLinkage), method='average')
            clusterIndices = fcluster(linkageTree,
                            t = maximumLinking - self.MinimumBlockAFCLRecurrenceRate,
                            criterion ='distance')
        else:
            if numberOfBlocks == 0:
                clusterIndices = []
            else:
                clusterIndices = 1
                
                    
        numberOfClusters = np.max(clusterIndices)
        clusters = np.zeros((numberOfBlocks, numberOfClusters),dtype = bool)
        for clusterIndex in range(numberOfClusters):
            clusters[:, clusterIndex] = (clusterIndices == clusterIndex+1) #Weird index, but is correct
                    
        clusterDuration = np.ones(numberOfClusters)*np.nan
        clusterTimes = np.zeros((len(recurrenceTime), numberOfClusters),dtype=bool)
        for clusterIndex in range(numberOfClusters):
            clusterIntervals = blockIntervalTimes[clusters[:, clusterIndex], :]
            numberofClusterIntervals = len(clusterIntervals)
            clusterTime = np.zeros(len(recurrenceTime),dtype=bool)
            for intervalIndex in range(numberofClusterIntervals):
                validTime = (recurrenceTime >= clusterIntervals[intervalIndex, 0]) & \
                    (recurrenceTime < clusterIntervals[intervalIndex, -1])
                clusterTime[validTime] = True
            
            clusterDuration[clusterIndex] = np.sum(clusterTime) * sampleTime
            clusterTimes[:, clusterIndex] = clusterTime
    
    
        return linkingMatrix, clusters, clusterDuration, clusterTimes, blockIndices
    