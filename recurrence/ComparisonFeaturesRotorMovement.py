# -*- coding: utf-8 -*-
"""
Created on Wed Jun 30 09:45:52 2021

DESCRIPTION: This script has to be renamed
The idea is to merge the other routines into this to compare meandering x stationary rotors

Here, the block information from the longest PSs will be used to generate recurrence 
matrices and consequent RQA parameters

These will be put in a single DataFrame

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import os,sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle#5 as pickle
import time
import multiprocessing as mp
from itertools import repeat
upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
sys.path.append(os.path.join(upperFolder,'recurrence'))
from RecurrenceComputation import RecurrenceComputation


#%% Load files that have been generated previously:
    # 1) Distances between PSs and catheters
    # 2) Relevant blocks related to the longest PSs
    # 3) Catheter data
    

#%% Code as a function
def calculateFeatures(experiment,BlockIntervals,CGroup):
    PSPATH = '/scratch/marques/featExt'# '/mnt/d/vgmar/model_data/exp906/pstracker/Control/Filtered' #"D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered"

    print('%s initiated'%experiment)



    columns = ['experiment','catheter','begin_block','end_block','mean_min_dist','std_min_dist',
           'threshold','nRR','DET','nLMAX','nLMEAN','nLMED','ENT']
    FeatureDF = pd.DataFrame(columns = columns)

    ### Recurrence computation object
    recComp = RecurrenceComputation()
    recComp.SampleShift = 5
    recComp.TheilerAFCLNumber = 0.5
    recComp.ExpectedAFRecurrenceRate = 1
    
    ti0 = time.time()

    
    ### Load distances 
    PS_DF = pd.read_csv(os.path.join(PSPATH,experiment+'_PSPositionsAndDistances.csv'),header=0)
    del PS_DF['Unnamed: 0']
    

    
    ### Calculate global distance matrices
    for cat in CGroup.Catheters.keys():
        ti1 = time.time()
        
        Elec = CGroup.Catheters[cat]
        #
        distanceMatrix, activationPhaseSignal, activationCycleLength  = \
            recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
        estimatedAFCL = np.median(activationCycleLength)
        estimatedAFCL = 1e-3*estimatedAFCL
        
        minLineLength = np.round(estimatedAFCL*CGroup.Catheters[cat].SamplingFrequency/recComp.SampleShift)
    
        ### Iterate over blocks
        CurrentBlocks = BlockIntervals[experiment]
        
        for bId in range(CurrentBlocks.shape[0]):
            
            dmSampleBlocks = CurrentBlocks[bId,:]//recComp.SampleShift
            ## Calculate recurrence plots and RQA features
            partDM = distanceMatrix[dmSampleBlocks[0]:dmSampleBlocks[1],
                                    dmSampleBlocks[0]:dmSampleBlocks[1]]
            
            RP, _, recurrenceThreshold, _ = \
                recComp.ComputeRecurrencePlot(partDM, Elec.SamplingFrequency, estimatedAFCL)
            RP = recComp.ErodeRP(RP,partDM)        
        
            rqa = recComp.ComputeRQA(RP,estimatedAFCL,Elec.SamplingFrequency,
                                                  minDiagonalLine = int(minLineLength),maxLineThickness=2)
            
            ## Get distances
            partPS = PS_DF.loc[(PS_DF['t']>=CurrentBlocks[bId,0])&(PS_DF['t']<CurrentBlocks[bId,1]),
                               ['t','D'+cat]]
            
            catDists = [np.min(partPS.loc[partPS['t']==t,['D'+cat]]) for t in np.unique(partPS['t'])]
            catDists = np.asarray(catDists).squeeze().astype(float)
            catDists[catDists<0] = np.nan
            
            # Put in output dataframe
            featureRow = {'experiment':experiment,
                          'catheter':cat,
                          'begin_block':CurrentBlocks[bId,0],
                          'end_block':CurrentBlocks[bId,1],
                          'mean_min_dist':np.nanmean(catDists),
                          'std_min_dist':np.nanstd(catDists),
                          'threshold':recurrenceThreshold,
                          'nRR':rqa['recurrence_rate'],
                          'DET':rqa['determinism'],
                          'nLMAX':rqa['longest_diagonal'],
                          'nLMEAN':rqa['mean_diagonal'],
                          'nLMED':rqa['median_diagonal'],
                          'ENT':rqa['entropy']}
            FeatureDF = FeatureDF.append(featureRow,ignore_index = True)

        print('Catheter %s: %.02f s'%(cat,time.time()-ti1))[]
    print('Elapsed time for experiment %s: %.02f s'%(experiment,time.time()-ti0))   
    
    return FeatureDF   

def catheterFeatures(Elec,experiment,BlockIntervals): #    for cat in CGroup.Catheters.keys():

    ### Calculate global distance matrices
    ti1 = time.time()
    #
    distanceMatrix, activationPhaseSignal, activationCycleLength  = \
        recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
    estimatedAFCL = np.median(activationCycleLength)
    estimatedAFCL = 1e-3*estimatedAFCL
    
    minLineLength = np.round(estimatedAFCL*Elec.SamplingFrequency/recComp.SampleShift)

    ### Iterate over blocks
    CurrentBlocks = BlockIntervals[experiment]
    
    for bId in range(CurrentBlocks.shape[0]):
        
        dmSampleBlocks = CurrentBlocks[bId,:]//recComp.SampleShift
        ## Calculate recurrence plots and RQA features
        partDM = distanceMatrix[dmSampleBlocks[0]:dmSampleBlocks[1],
                                dmSampleBlocks[0]:dmSampleBlocks[1]]
        
        RP, _, recurrenceThreshold, _ = \
            recComp.ComputeRecurrencePlot(partDM, Elec.SamplingFrequency, estimatedAFCL)
        RP = recComp.ErodeRP(RP,partDM)        
    
        rqa = recComp.ComputeRQA(RP,estimatedAFCL,Elec.SamplingFrequency,
                                                minDiagonalLine = int(minLineLength),maxLineThickness=2)
        
        ## Get distances
        partPS = PS_DF.loc[(PS_DF['t']>=CurrentBlocks[bId,0])&(PS_DF['t']<CurrentBlocks[bId,1]),
                            ['t','D'+cat]]
        
        catDists = [np.min(partPS.loc[partPS['t']==t,['D'+cat]]) for t in np.unique(partPS['t'])]
        catDists = np.asarray(catDists).squeeze().astype(float)
        catDists[catDists<0] = np.nan
        
        # Put in output dataframe
        featureRow = {'experiment':experiment,
                        'catheter':cat,
                        'begin_block':CurrentBlocks[bId,0],
                        'end_block':CurrentBlocks[bId,1],
                        'mean_min_dist':np.nanmean(catDists),
                        'std_min_dist':np.nanstd(catDists),
                        'threshold':recurrenceThreshold,
                        'nRR':rqa['recurrence_rate'],
                        'DET':rqa['determinism'],
                        'nLMAX':rqa['longest_diagonal'],
                        'nLMEAN':rqa['mean_diagonal'],
                        'nLMED':rqa['median_diagonal'],
                        'ENT':rqa['entropy']}
        FeatureDF = FeatureDF.append(featureRow,ignore_index = True)

    print('Catheter %s: %.02f s'%(cat,time.time()-ti1))[]
    
    return FeatureDF   





#%% Apply code
PSPATH = '/scratch/marques/featExt' #"D:\\vgmar\\model_data\\exp906\\pstracker\\Control\\Filtered"
CATPATH ='/scratch/marques/featExt' #'/mnt/d/vgmar/model_data/exp906/electrode_data/Control' # "D:\\vgmar\\model_data\\exp906\\electrode_data\\Control"

## Blocks
with open(os.path.join(PSPATH,'Exp906_LongSourceBlocks.pkl'), 'rb') as output:
    BlockIntervals = pickle.load(output)

experimentNames = []
for file in os.listdir(PSPATH):
    if file.startswith('exp906c') and file.endswith('_PSPositionsAndDistances.csv'):
        experimentNames.append(file[:9])
experimentNames = np.sort(experimentNames)

### Load catheter data
CGroupList = list()
for experiment in experimentNames:
    print(os.path.join(CATPATH,experiment+'_CatheterData.catG'))
    with open(os.path.join(CATPATH,experiment+'_CatheterData.catG'), 'rb') as output:
        CGroupList.append(pickle.load(output))

### Prepare output

columns = ['experiment','catheter','begin_block','end_block','mean_min_dist','std_min_dist',
           'threshold','nRR','DET','nLMAX','nLMEAN','nLMED','ENT']

OutputDF = pd.DataFrame(columns = columns)

for i,experiment in enumerate(experimentNames):
    CGroup = CGroupList[i]
    catList = [CGroup.Catheters[key] for key in CGroup.Catheters.keys()]

    ti = time.time()
    pool1 = mp.Pool(np.min([len(catList),mp.cpu_count()]))
    # results =  pool1.starmap(calculateFeatures, zip(experimentNames, repeat(BlockIntervals),CGroupList))
    results =  pool1.starmap(catheterFeatures, zip(.Catheters[key], repeat(experiment),BlockIntervals)) #    for cat in CGroup.Catheters.keys():
    pool1.close()    
    print('Elapsed time in first pool: %0.3f s'%(time.time()-ti))

    for FeatureDF in results:
        OutputDF = OutputDF.append(FeatureDF,ignore_index = True)

OutputDF.to_csv(os.path.join(PSPATH,'Exp906_Features.csv'))