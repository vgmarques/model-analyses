# -*- coding: utf-8 -*-
"""
Created on Thu Apr  1 09:58:26 2021

DESCRIPTION:

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import numpy as np
import os,sys
import scipy.io as sio
import pickle5 as pickle
import matplotlib.pyplot as plt
from numba import jit
import scipy.signal as sig

upperFolder = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperFolder,'aux-functions'))
from CatheterObjects import CatheterClass,CatheterGroup
import egm_processing as egmp
from RecurrenceComputation import RecurrenceComputation

#%% Load electrode Data
with open('D:\\vgmar\\model_data\\exp906\\electrode_data\\Control\\exp906c74_CatheterData.catG', 'rb') as input:
    Group = pickle.load(input)
    #%%

tag = 'ILA_1'
Elec = Group.Catheters[tag]
fs = Elec.SamplingFrequency

# [b,a] = sig.butter(4,1/(fs/2),'high')
# Elec.EGMData = sig.filtfilt(b,a,Elec.EGMData)
Elec.EGMData = Elec.EGMData[2500:,:]
Elec.DetectActivations(method = 'adaptiveThreshold')

# Limit to 2500-5000 ms

# for i in range(len(Elec.Activations)):
#     inds = np.where((Elec.Activations[i]>=2500)*(Elec.Activations[i]<=5000))[0]
#     Elec.Activations[i] = np.asarray(Elec.Activations[i][inds])-2500

recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1.1# (RRmax)
'''
#For Block detection
recComp.BlockGridAFCLResolution = 0.2
recComp.MinimumBlockAFCLRecurrenceRate = 0.9#(RRmin)
recComp.MinimumBlockAFCLDuration = 0
testedFraction = 0.05
'''
distanceMatrix, activationPhaseSignal, activationCycleLength  = \
    recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
estimatedAFCL = np.median(activationCycleLength)
estimatedAFCL = 1e-3*estimatedAFCL


# ## Compare output with matlab
# MatlabOut = sio.loadmat('D:\\vgmar\\model_data\\exp906\\electrode_data\\Control\\test_ILA_1.mat')
# distanceMat=MatlabOut['distanceMatrix']
# activationPhaseMat = MatlabOut['activationPhaseSignal']

test_dm = sio.loadmat('C:\\Users\\vgmar\\Área de Trabalho\\dm.mat')['distanceMatrix']
test_act = sio.loadmat('C:\\Users\\vgmar\\Área de Trabalho\\act.mat')['activationPhaseSignal']

plt.plot(test[:,0]-activationPhaseSignal[:,0])
plt.plot()


fig,ax = plt.subplots(2)
ax[0].pcolormesh(distanceMatrix,vmin=0,vmax=1,cmap='jet')
ax[1].pcolormesh(test,vmin=0,vmax=1,cmap='jet')
#%%
recurrenceMatrix, recurrenceTime, recurrenceThreshold, distanceMatrix = \
    recComp.ComputeRecurrencePlot(distanceMatrix, Elec.SamplingFrequency, estimatedAFCL)

fig,ax = plt.subplots(1,figsize=[8,8]) 
ti = np.int(0/recComp.SampleShift)
interval = np.int(2500/recComp.SampleShift)
time = np.linspace(ti*recComp.SampleShift/fs,(ti+interval)*recComp.SampleShift/fs,interval)
ax.pcolormesh(time,time,recurrenceMatrix[ti:ti+interval,ti:ti+interval],cmap = 'binary',shading = 'auto')#,origin='lower')
fig.suptitle(tag)
#fig.show()


fig,ax=plt.subplots(4,4,sharex=True)
for i in range(Elec.GetNumberOfChannels()):
    ax[i//4,i%4].plot(activationPhaseSignal[:,i],'--',c='gray')
    sig = Elec.EGMData[:,i]
    sig = (sig-sig.min())/(sig.max()-sig.min())*2*np.pi-np.pi
    ax[i//4,i%4].plot(sig,'k',linewidth = 2)
    ax[i//4,i%4].spines['right'].set_visible(False)
    ax[i//4,i%4].spines['left'].set_visible(False)
    ax[i//4,i%4].spines['top'].set_visible(False)
    ax[i//4,i%4].spines['bottom'].set_visible(True)
    # ax[i//4,i%4].get_xaxis().set_visible(False)
    ax[i//4,i%4].get_yaxis().set_visible(False)
    if i//4==3:
        ax[i//4,i%4].set_xlabel('Time (ms)')

fig.suptitle(tag)
# fig.show()


fig,ax = plt.subplots(1,figsize = [8,8])
ax.pcolormesh(time,time,distanceMatrix,cmap = 'jet',vmin = 0)

#%%
import cv2

# Threshold the image
img = recurrenceMatrix.astype(np.uint8)#cv2.threshold(recurrenceMatrix, 127, 255, 0)

# Step 1: Create an empty skeleton
size = np.size(img)
erodedRecurrenceMatrix = np.zeros(img.shape, np.uint8)

# Get a Cross Shaped Kernel
# element = cv2.getStructuringElement(cv2.MORPH_RECT, (1,3))
element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))

# Repeat steps 2-4
while True:
    #Step 2: Open the image
    open = cv2.morphologyEx(img, cv2.MORPH_OPEN, element)
    #Step 3: Substract open from the original image
    temp = cv2.subtract(img, open)
    #Step 4: Erode the original image and refine the skeleton
    eroded = cv2.erode(img, element)
    erodedRecurrenceMatrix = cv2.bitwise_or(erodedRecurrenceMatrix,temp)
    img = eroded.copy()
    # Step 5: If there are no white pixels left ie.. the image has been completely eroded, quit the loop
    if cv2.countNonZero(img)==0:
        break

#%%
##%%
import numba
@jit(nopython=True,parallel=True)
def recurrenceBlocksA(recurrenceTime,recurrenceMatrix,estimatedAFCL,BlockGridAFCLResolution):
    sampleTime = np.mean(np.diff(recurrenceTime))
    numberOfSamples = len(recurrenceTime)
    maximumTriangleSize = int(np.ceil(numberOfSamples / 2))
    gridSearchStep = int(np.ceil((estimatedAFCL / sampleTime) * BlockGridAFCLResolution))
    gridSampleIndices = np.arange(gridSearchStep,numberOfSamples,gridSearchStep)
    numberOfGridPoints = len(gridSampleIndices)
    gridTriangleSizes = np.arange(gridSearchStep,maximumTriangleSize,gridSearchStep)
    numberOfTriangleGridPoints = len(gridTriangleSizes)
    gridTriangleRecurrenceRateCorrected = np.zeros((numberOfGridPoints, numberOfTriangleGridPoints))
    print(numberOfGridPoints)
    for gridSampleIndex in range(numberOfGridPoints):
        sampleIndex = gridSampleIndices[gridSampleIndex]
        sampleMaximumTriangleSize = np.array([sampleIndex - 1, numberOfSamples - sampleIndex]).min()
        numberOfSampleTriangleGridPoints = np.sum(gridTriangleSizes <= sampleMaximumTriangleSize)
        sampleTriangleRecurrenceRateCorrected = np.zeros(numberOfTriangleGridPoints)
        for triangleSizeIndex in range(numberOfSampleTriangleGridPoints):
            triangleSize = gridTriangleSizes[triangleSizeIndex]
            triangleInterval = [sampleIndex-triangleSize,sampleIndex+triangleSize]
            # triangle = recurrenceMatrix[triangleInterval, triangleInterval]
            triangle = recurrenceMatrix[triangleInterval[0]:triangleInterval[1],triangleInterval[0]:triangleInterval[1]]
            #recurrence rate without diagonal
            blockSize = 2 * triangleSize + 1
            correctedRecurrenceRate = (np.sum(triangle.flatten()) - blockSize) / blockSize**2
            sampleTriangleRecurrenceRateCorrected[triangleSizeIndex] = correctedRecurrenceRate
        gridTriangleRecurrenceRateCorrected[gridSampleIndex, :] = sampleTriangleRecurrenceRateCorrected
        print(gridSampleIndex)
    return gridTriangleRecurrenceRateCorrected


gridTriangleRecurrenceRateCorrected = recurrenceBlocksA(recurrenceTime,recurrenceMatrix,estimatedAFCL,recComp.BlockGridAFCLResolution)
#% normalize recurrence rate to AFCL
gridTriangleRecurrenceRateCorrected = (estimatedAFCL / sampleTime) * gridTriangleRecurrenceRateCorrected
sampleMaxRecurrenceRateCorrected = np.max(gridTriangleRecurrenceRateCorrected, axis=-1)
maxTriangleSizeIndex = np.where((gridTriangleRecurrenceRateCorrected==sampleMaxRecurrenceRateCorrected))

#% compute recurrence rate including diagonal
sampleMaxRecurrenceRate = zeros(size(sampleMaxRecurrenceRateCorrected));
maxTriangleSizes = gridTriangleSizes(maxTriangleSizeIndex);
