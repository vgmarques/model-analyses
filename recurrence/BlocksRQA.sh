
CODEPATH=/mnt/d/vgmar/Documents/GitHub/USI/model-analyses/recurrence
PATHPS=/mnt/d/vgmar/model_data/exp906/pstracker/Control/Filtered
PATHCAT=/mnt/d/vgmar/model_data/exp906/electrode_data/Control
RECPATH=/mnt/d/vgmar/model_data/exp906/recurrence/Control
OUTPATH=/mnt/d/vgmar/model_data/exp906/recurrence/Control/Blocks


BASENAME=exp906c
EXTCAT=_CatheterData.catG
EXTPS=_pstracker_th100.txt

cd $CODEPATH

# python ComputeDistanceMatrices.py +DistanceMatrixParams.par -outputPath=${RECPATH} \
# -inputFolder=${PATHCAT} $BASENAME

# python ComputePSProximity.py +PSParams.par -outputPath=${PATHPS} \
# -extension=_th100.txt -inputFolderPS=$PATHPS -inputFolderCatheter=${PATHCAT} $BASENAME

python ExtractBlockRQAFeatures.py -inputFolderPS=${PATHPS} -inputFolderRecurrence=${RECPATH} \
-outputPath=${OUTPATH} --minLineAFCL $BASENAME