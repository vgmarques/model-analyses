# -*- coding: utf-8 -*-
"""
Created on Thu Jul 29 13:47:56 2021

DESCRIPTION: Short script to make many distance matrices

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
#%%
import os,sys
import numpy as np
# import pandas as pd
# import matplotlib.pyplot as plt
import pickle#5 as pickle
# import scipy.signal as sig
# from copy import deepcopy

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))
# import quick_visualization as qv
from RecurrenceComputation import RecurrenceComputation

#%%
CATPATH= '/mnt/d/vgmar/model_data/exp915'# 'D:\\vgmar\\model_data\\exp906\\electrode_data\\Control'
RECPATH= '/mnt/d/vgmar/model_data/exp915'# 'D:\\vgmar\\model_data\\exp906\\recurrence\\Control'
EXPCODE = 'exp915f'

experimentNames = []
for file in os.listdir(CATPATH):
    if file.startswith(EXPCODE) and file.endswith('.catG'):
        experimentNames.append(file.split('_')[0])
experimentNames = np.sort(experimentNames)

recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1


for experiment in experimentNames:
    print('---------------Experiment %s---------------'%experiment)
    CGroup = pickle.load(open(os.path.join(CATPATH,experiment+'_CatheterData.catG'),'rb'))
    EstimatedAFCLs = dict()
    
    AllDistanceMatrices = dict()
    for i,catheter in enumerate(CGroup.Catheters.keys()):
        print(catheter)
        Elec = CGroup.Catheters[catheter]
        #
        distanceMatrix, _, activationCycleLength  = \
            recComp.ComputeDistanceMatrix(Elec, Elec.Activations)
        estimatedAFCL = np.median(activationCycleLength)
        distanceMatrix = distanceMatrix.astype(np.float16)
        EstimatedAFCLs[catheter] = 1e-3*estimatedAFCL
        AllDistanceMatrices[catheter] = distanceMatrix


    with open(os.path.join(RECPATH,experiment+'_RecurrenceAnalyses.pkl'),'wb') as output:
        pickle.dump(AllDistanceMatrices,output)
        pickle.dump(EstimatedAFCLs,output)
    
