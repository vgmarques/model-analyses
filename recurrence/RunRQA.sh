
CODEPATH=/mnt/d/vgmar/Documents/GitHub/USI/model-analyses/recurrence
PATHPS=/mnt/d/vgmar/model_data/exp906/pstracker/Control/Filtered
PATHCAT=/mnt/d/vgmar/model_data/exp906/electrode_data/Control
OUTPATH=/mnt/d/vgmar/model_data/exp906/recurrence/Control/AutoTh

BASENAME=exp906c
EXTCAT=_CatheterData.catG
EXTPS=_pstracker_th100.txt

cd $CODEPATH
for CODE in  04 05 12 14  17 18 19 70 71 72 73 74 75 82 83 84 87 88 89 # #   13
do
EXPNAME=${BASENAME}${CODE}
CATNAME=${EXPNAME}${EXTCAT}
PSNAME=${EXPNAME}${EXTPS}

echo "--------${EXPNAME}--------"

python EXtractRQAFeatures.py +RQAParams.par -pathCat=${PATHCAT} -psPath=${PATHPS} -outputPath=${OUTPATH} \
--minLineAFCL --savePSFiles $CATNAME $PSNAME

done
exit 0