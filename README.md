# README #

### Content ###

This repository contains functions used for the (pre) processing of model data obtained with propag-5. The content of each folder is as follows:

* atrial_surface: functions to detect and adjust the atrial surface in torso igb files (or in theory also heart files). 
* aux-functions: libraries and classes that are imported commonly by the other scripts are placed here.
* ecg-analysis: scripts for the handling and processing of non-invasive data
* electrode-data: scripts for positioning catheters on the endocardium and getting data from them
* jupyter: notebooks that use the functions in the other repositories, usually for exploratory or educative purposes
* psdetection: Python parallel implementation of the functions for phase singularity detection and tracking. Also contains some scripts to generate PS heatmaps and analyze the position of PSs.
* recurrence: translation of the Stef's recurrence analysis to Python, with additional functions as needed. Not much work has been done here so far.
* visualization: scripts for 2D and 3D visualization of model data, including transmembrane potentials, electrode positions and PS locations
* wave-detection: functions for wave tracking and breakthrough detection

### Dependencies ###

This repository, in particular the visualization part, depends to some extent on the propag functions iga2igb and igbslice. It also needs gzip and many Python packages (notably numpy, scipy, and especially vtk). Loading and writing IGB files is done inside python, so no other libraries (like pyccmc) are needed here.

The visualization folder is the most likely to need additional functions.

### Highlights ###
The `aux-functions` folder contains most of the hard work, including the Catheter classes which are used to call many other functions, and the IgbHandling library, which deals with the model iga and igb files.

The `recurrence/RecurrenceComputation.py` class contains most of the recurrence analysis part

In `ps-detection`, `PSFinder` and `PSTracker` are the main functions for PS detection in transmembrane potentials. More functions for EGMs will be added soon

Finally, in `wave-detection`, `NewWaveDetection.py` has the breakthrough detection. This name will change soon, and the old wave tracking function will be updated.


### How to use ###

Several of the relevant scripts are organized in a way that can be used from the command line using python alone. These scripts have help functions of their own, but most of them need:

* A .par file (or really any other text file) with the flags in it OR
* The flags directly in the command line. Most flags have a default value, so this can be shorter
* Required arguments cannot be passed in the .par file and thus must be added in the command line.

Specifics of the use of each script are in listed in the script's help.

When not organized in a off-the-box script, the functions might be a bit more chaotic. Some good examples of use are in the notebooks.

### TODO List ###

### Contact ###
E-mail: v.goncalvesmarques@maastrichtuniversity.nl