#%%
import os,sys
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig

sys.path.append('../aux-functions')
import IgbHandling as igb
from detect_peaks import detect_peaks
from AFComplexityCalculator import AFComplexityCalculator
ind = 19
#%% Load ECG
expSeries = 'exp906a'
ecgPath = '/home/vgmarques/data/model-data/%s/ecg'%expSeries #'/home/vgmarques/Downloads/%s/ecg'%expSeries# 

expName = '%s%02d'%(expSeries,ind)
print(expName)

#exp906c
# AF: 04,05,12,14,17,18,19,70 (?),71,72,73,74,75,82,83,84,87,89                         (Control: 35%/ Fibrosis: 50-55%)
# AFL: 13,70 (?),88,                                                                       (Control: 05%/ Fibrosis: 05%)
# Not initiated: 00,01,02,03,06,07,08,09,10,11,15,16,76,77,78,79,80,81,85,86        (Control: 75%/ Fibrosis: 40%)
# 

#exp906a
# AF: 05,13,17,70,71,72,74,84,87,89                                          (Control: 15%/ Fibrosis: 35%)
# AFL: 12,14,15,18,19,75,82,83,88                                                  (Control: 20%/ Fibrosis: 20%)
# Not initiated: 00,01,02,03,04,06,07,08,09,10,11,16,73,76,77,78,79,80,81,85,86 (Control: 60%/ Fibrosis: 45%)

#exp905a
# AF: 05,13 (?),15(?),70,72,73,74,75,84,87,89 (?)                         (Control: 5-15%/ Fibrosis: 35-40%)
# AFL: 17,71 (?),82,83,88                                                 (Control: 20%/ Fibrosis: 25%)
# Not initiated: 00,01,02,03,04,06,07,08,09,10,11,12,14,16,18,19,77,78,79,80,81,85,86, (Control: 75%/ Fibrosis: 40%)
# 
# 70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89

#exp906l
# AF: 05, 12,15,17, 18, 19 (?), 70,71,72,74,75,82,84,87,89                               (Control: 05%/ Fibrosis: 35%)
# AFL: 13,14,85, 88                                           (Control: 20%/ Fibrosis: 25%)

#exp906r
# AF: 05, 12 (?),13,14 (?), 15,17,19,70,71,72,74,75,82 (?),83,84, 87,89                        (Control: 05%/ Fibrosis: 35%)
# AFL:  88,                            (Control: 20%/ Fibrosis: 25%)

try:
    ecg,hdr = igb.Load(os.path.join(ecgPath,expName+'_ecg.igb'))

    if ecg.shape[0]<3500: print('No initiation')
except:
    print('No File')
ind += 1

#%% Stef's function
AFCalc = AFComplexityCalculator()
chosenLeads = np.arange(3)


fWaveAmplitudes, fWaveIndices, fWavePeakValleyIndices = AFCalc.ComputeFWaveAmplitudes(ecg[2500:5000,:])


differencePeaks = []
for fWaveIndex in fWaveIndices:
    differencePeaks.append(np.diff(fWaveIndex))

# #Calculate the std
# distribution = np.empty((0,2),int)
# for lead in chosenLeads:
#     distribution = np.vstack([distribution,np.array([differencePeaks[lead][1:],differencePeaks[lead][:-1]]).T])

#
# normDistribution = distribution-np.median(distribution,axis=0)
# std = 1/len(normDistribution)*np.sum(np.sqrt(normDistribution[:,0]**2+normDistribution[:,1]**2))

# Stack all chosen leads
pcDistribution = np.empty((0,2),int)
for lead in chosenLeads:
    pcDistribution = np.vstack([pcDistribution,np.array([differencePeaks[lead][1:],differencePeaks[lead][:-1]]).T])


pcCentroid = np.median(pcDistribution,axis=0)

centerDistance = np.sqrt(np.sum((pcDistribution-pcCentroid)**2,axis=1))


if np.median(centerDistance)<6:
    print(expName+' is AFL')
else:
    print(expName+' is AF')


#% Poincare
fig,ax = plt.subplots(1,figsize = (8,8))

for lead in chosenLeads:
    ax.scatter(differencePeaks[lead][1:],differencePeaks[lead][:-1],marker = '.')

ax.plot(*np.median(pcDistribution,axis=0).T,'rx',markersize=10)

ax.set_xlabel('FF peak interval (n)',fontsize = 20)
ax.set_ylabel('FF peak interval (n-1)',fontsize = 20)
ax.set_xlim([0,200])
ax.set_ylim([0,200])
#%% 

t0 = 0
t1 = 2500

fig,ax = plt.subplots(3,1,figsize = (10,10))

ax[0].plot(np.arange(t0-2500,t1-2500),ecg[t0:t1,-1])
ax[1].plot(np.arange(t0-2500,t1-2500),ecg[t0:t1,-2])
ax[2].plot(np.arange(t0-2500,t1-2500),ecg[t0:t1,-3])
channelIndexes = [192,193,194]
for channelIndex,channel in enumerate(channelIndexes):
    ax[channelIndex].plot(fWaveIndices[channel],ecg[fWaveIndices[channel]+2500,channel],'r.')
    ax[channelIndex].plot(fWavePeakValleyIndices[channel],ecg[fWavePeakValleyIndices[channel]+2500,channel],'g.')

    ax[channelIndex].set_xlim(t0-2500,t1-2500)

# ax.plot(ecg[t0:t1,channelIndex])
#%%
# Test for leadfields

#bsm V1: 141; V6 = 174


t0 = 0
t1 = 1000

fig,ax = plt.subplots(1,figsize = (16,8))

ax.plot(np.arange(t0,t1),ecg[t0:t1,141],linewidth=2)

ax.vlines([0,280,450,610,765,915,1060,1200,1335,1461,1585,1709,1833,1957],-0.2,0.2,linestyle='dashed',color='gray')
ax.grid()
ax.set_xlim(t0,t1)
ax.set_ylim(-0.2,0.2)