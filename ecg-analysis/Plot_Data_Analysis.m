figure
plot_number = 1;
names_ECG15 = {'Eso 5', 'I', 'II', 'V1','V3','V5','V6','D5'};
for lead_number = 1 : 4
    subplot(4,5,plot_number)
    signal = ecgData.Data(:,lead_number);
    plot(signal)
    hold on
    title (names_ECG15{lead_number},'FontSize',12)
    plot(fWaveIndices{lead_number},signal(fWaveIndices{lead_number}),'r*')
    xlabel('Time (ms)','FontSize',16)
    ylabel('Amplitude (mV)','FontSize',16)
    set(gca,'FontSize',16)
    
    fwave_CL = diff(fWaveIndices{lead_number});
    subplot(4,5,plot_number+1)
    hist(fwave_CL)
    xlabel('F-wave Cycle length (ms)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
    axis square
    xlim ([0 250])
    ylim([0 10])
    s1 = 'mean =';
    s2 = num2str((mean(fwave_CL)));
    s3 = ' median =';
    s4 = num2str(median(fwave_CL));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([median(fwave_CL), median(fwave_CL)], ylim, 'LineWidth', 2, 'Color', 'r');
    line([mean(fwave_CL), mean(fwave_CL)], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
    subplot(4,5,plot_number+2)
    hist(fWaveAmplitudes{lead_number})
    xlabel('F-wave Amplitude(mV)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
%     axis square
    xlim ([0 1])
    ylim([0 10])
    s1 = 'mean =';
    s2 = num2str((mean(fWaveAmplitudes{lead_number})));
    s3 = ' median =';
    s4 = num2str(median(fWaveAmplitudes{lead_number}));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([median(fWaveAmplitudes{lead_number}), median(fWaveAmplitudes{lead_number})], ylim, 'LineWidth', 2, 'Color', 'r');
    line([mean(fWaveAmplitudes{lead_number}), mean(fWaveAmplitudes{lead_number})], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
  
    subplot(4,5,plot_number+3)
    plot(fWaveAmplitudes{lead_number}(1:end-1),fWaveAmplitudes{lead_number}(2:end),'k.','MarkerSize',10)
    xlabel('F-wave amplitude (n)','FontSize',16)
    ylabel('F-wave amplitude (n+1)','FontSize',16)
    xlim ([0 1])
    ylim([0 1])
    %axis equal
    %axis square
    set(gca,'FontSize',16)
    subplot(4,5,plot_number+4)
    plot(fwave_CL(1:end-1),fwave_CL(2:end),'k.','MarkerSize',10)
    xlabel('F-wave Cycle length (n)','FontSize',16)
    ylabel('F-wave Cycle length (n+1)','FontSize',16)
    %axis equal
    %axis square
    xlim ([0 250])
    ylim([0 250])
    set(gca,'FontSize',16)
    plot_number = plot_number + 5;
end
​
figure
plot_number = 1;
names_ECG15 = {'Eso 5', 'I', 'II', 'V1','V3','V5','V6','D5'};
for lead_number = 5 : 8
    subplot(4,5,plot_number)
    signal = ecgData.Data(:,lead_number);
    plot(signal)
    hold on
    title (names_ECG15{lead_number},'FontSize',12)
    plot(fWaveIndices{lead_number},signal(fWaveIndices{lead_number}),'r*')
    xlabel('Time (ms)','FontSize',16)
    ylabel('Amplitude (mV)','FontSize',16)
    set(gca,'FontSize',16)
    
    fwave_CL = diff(fWaveIndices{lead_number});
    subplot(4,5,plot_number+1)
    hist(fwave_CL)
    xlabel('F-wave Cycle length (ms)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
    axis square
    xlim ([0 250])
    ylim([0 10])
    s1 = 'mean =';
    s2 = num2str((mean(fwave_CL)));
    s3 = ' median =';
    s4 = num2str(median(fwave_CL));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([median(fwave_CL), median(fwave_CL)], ylim, 'LineWidth', 2, 'Color', 'r');
    line([mean(fwave_CL), mean(fwave_CL)], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
    subplot(4,5,plot_number+2)
    hist(fWaveAmplitudes{lead_number})
    xlabel('F-wave Amplitude(mV)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
%     axis square
    xlim ([0 1])
    ylim([0 10])
    s1 = 'mean =';
    s2 = num2str((mean(fWaveAmplitudes{lead_number})));
    s3 = ' median =';
    s4 = num2str(median(fWaveAmplitudes{lead_number}));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([median(fWaveAmplitudes{lead_number}), median(fWaveAmplitudes{lead_number})], ylim, 'LineWidth', 2, 'Color', 'r');
    line([mean(fWaveAmplitudes{lead_number}), mean(fWaveAmplitudes{lead_number})], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
  
    subplot(4,5,plot_number+3)
    plot(fWaveAmplitudes{lead_number}(1:end-1),fWaveAmplitudes{lead_number}(2:end),'k.','MarkerSize',10)
    xlabel('F-wave amplitude (n)','FontSize',16)
    ylabel('F-wave amplitude (n+1)','FontSize',16)
    xlim ([0 1])
    ylim([0 1])
    %axis equal
    %axis square
    set(gca,'FontSize',16)
    subplot(4,5,plot_number+4)
    plot(fwave_CL(1:end-1),fwave_CL(2:end),'k.','MarkerSize',10)
    xlabel('F-wave Cycle length (n)','FontSize',16)
    ylabel('F-wave Cycle length (n+1)','FontSize',16)
    %axis equal
    %axis square
    xlim ([0 250])
    ylim([0 250])
    set(gca,'FontSize',16)
    plot_number = plot_number + 5;
end
​
​
%%
​
for analyse = 1:2
    fwave_CL_8leads =[];
    fwave_Amp_8leads = [];
    fwave_CL_poncare_8leadECG =[];
    fwave_Amp_poncare_8leadECG=[];
    fwave_Amp_Normalized_poncare=[];
    if analyse == 1
        channel_number =8;
    else
        channel_number = channelIndex;
    end
    
    for lead_number = 1 : channel_number
        fwave_CL_perlead = diff(fWaveIndices{lead_number});
        fwave_Amp_perlead = fWaveAmplitudes{lead_number};
        max_fwave = max(fwave_Amp_perlead);
        min_fwave = min(fwave_Amp_perlead);
        fwave_CL_8leads = [fwave_CL_8leads;fwave_CL_perlead];
        fwave_Amp_8leads= [fwave_Amp_8leads;fwave_Amp_perlead];
        fwave_CL_poncare_temp = [fwave_CL_perlead(1:end-1) fwave_CL_perlead(2:end)];
        fwave_CL_poncare_8leadECG = [fwave_CL_poncare_8leadECG; fwave_CL_poncare_temp];
        fwave_Amp_poncare_temp = [fwave_Amp_perlead(1:end-1) fwave_Amp_perlead(2:end)];
        fwave_Amp_poncare_8leadECG = [fwave_Amp_poncare_8leadECG;fwave_Amp_poncare_temp];
        
        fwave_Amp_perlead_Normalized = ((fwave_Amp_perlead - min_fwave)./(max_fwave - min_fwave));
        fwave_Amp_Normalized_poncare_temp = [fwave_Amp_perlead_Normalized(1:end-1) fwave_Amp_perlead_Normalized(2:end)];
        fwave_Amp_Normalized_poncare = [fwave_Amp_Normalized_poncare;fwave_Amp_Normalized_poncare_temp];
        
    end
    
    
    
    figure
    subplot(2,3,1)
    hist(fwave_CL_8leads,100)
    xlim ([0 250])
    % ylim([0 10])
    xlabel('F-wave Cycle length (ms)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
    s1 = 'mean =';
    s2 = num2str((mean(fwave_CL_8leads)));
    s3 = ' median =';
    s4 = num2str(median(fwave_CL_8leads));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([median(fwave_CL_8leads), median(fwave_CL_8leads)], ylim, 'LineWidth', 2, 'Color', 'r');
    line([mean(fwave_CL_8leads), mean(fwave_CL_8leads)], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
    
    
    subplot(2,3,4)
    hist(fwave_Amp_8leads,100)
    xlabel('F-wave Amplitude(mV)','FontSize',16)
    ylabel('Number of Events','FontSize',16)
    s1 = 'mean =';
    s2 = num2str((.25*mean(fwave_Amp_8leads)));
    s3 = ' median =';
    s4 = num2str(median(fwave_Amp_8leads));
    title(strcat(s1,s2,s3,s4),'FontSize',14,'FontWeight','Bold')
    line([(median(fwave_Amp_8leads)), (median(fwave_Amp_8leads))], ylim, 'LineWidth', 2, 'Color', 'r');
    line([.25*mean(fwave_Amp_8leads), .25*mean(fwave_Amp_8leads)], ylim, 'LineWidth', 2, 'Color', 'k');
    set(gca,'FontSize',16)
     xlim ([0 1])
    
    
    subplot(2,3,2)
    axis equal
    hold on
    plot(fwave_CL_poncare_8leadECG(:,1),fwave_CL_poncare_8leadECG(:,2),'k.');
    xlabel('F-wave Cycle length (n)','FontSize',16)
    ylabel('F-wave Cycle length (n+1)','FontSize',16)
    xlim ([0 250])
    ylim([0 250])
    set(gca,'FontSize',16)
    [coeff_8lead_AFCL,score_8lead_AFCL,latent_8lead_AFCL] = pca(fwave_CL_poncare_8leadECG);
    subplot(2,3,3)
    biplot(coeff_8lead_AFCL(:,1:2),'scores',score_8lead_AFCL(:,1:2))
    axis equal
    set(gca,'FontSize',16)
    
    
    subplot(2,3,5)
    hold on
    axis equal
   
    plot(fwave_Amp_poncare_8leadECG(:,1),fwave_Amp_poncare_8leadECG(:,2),'k.')
    xlabel('F-wave Amp (n)','FontSize',16)
    ylabel('F-wave Amp (n+1)','FontSize',16)
    xlim ([0 1])
    ylim([0 1])
%     axis equal
    set(gca,'FontSize',16)
    [coeff_8lead_AMP,score_8lead_AMP,latent_8lead_AMP] = pca(fwave_Amp_poncare_8leadECG);
    subplot(2,3,6)
    biplot(coeff_8lead_AMP(:,1:2),'scores',score_8lead_AMP(:,1:2))
    axis equal
    set(gca,'FontSize',16)
    
    if analyse > 0
        CL_centre_of_Mass = mean(fwave_CL_poncare_8leadECG);
        
        for NumberOfPoints_CL = 1 : size(fwave_CL_poncare_8leadECG,1)
            
            centered_Distance_CL(NumberOfPoints_CL,1) = sqrt((fwave_CL_poncare_8leadECG(NumberOfPoints_CL,1)- CL_centre_of_Mass(1))^2 ...
                + (fwave_CL_poncare_8leadECG(NumberOfPoints_CL,2)- CL_centre_of_Mass(2))^2);
            
        end
        
        mean(centered_Distance_CL)
        
        
    end
    cluster_info (analyse) =  mean(centered_Distance_CL);
    centered_Distance_CL = [];
end
​
%%