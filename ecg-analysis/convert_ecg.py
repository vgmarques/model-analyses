'''
Python implementation of Mark's convert_ecg.m function
'''
import os
from symbol import vfpdef
import sys
from termios import VLNEXT
import numpy as np
import argparse
from scipy.io import savemat

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

def read_header(fname):
    fi = open(fname,'rb')
    buf = fi.read(1024)
    hdr = buf.decode().split('\r\n')
    cmm = [l.strip()[2:] for l in hdr if l.startswith("#")]
    hdr = sum((l.split() for l in (l.strip() for l in hdr if not l.startswith("#")) if len(l)>0),[])
    hdr = dict(tuple(ob.split(":")) for ob in hdr)
    hdr['comments'] = cmm
    for a in 'xyzt':
        if a in hdr: hdr[a] = int(hdr[a])
    for a in ['zero','facteur']:
        if a in hdr: hdr[a] = float(hdr[a])            
    return hdr

def main(args):
    expName = args.expName

    if args.option=='forward-solution':
        for i in range(args.initialCode,args.finalCode):

            prefix = expName+'%02df_ve'%i

            # filenames for input
            fname_bsm65 = '%s_bsm65.igb'%prefix
            fname_mbsm  = '%s_mbsm.igb'%prefix
            fname_probe = '%s_probe.igb'%prefix

            # filenames for output
            path = args.dataPath
            name,ext = os.path.splitext(prefix)

            bsmname = os.path.join(path,name+'.mat')
            wctname = os.path.join(path,name+'_wct.igb')

            #Read Amsterdam BSM and compute WCT
            hdr = read_header(os.path.join(path,fname_bsm65))
            ecg,_ = igb.Load(os.path.join(path,fname_bsm65))#.squeeze()
            ecg = hdr['facteur']*ecg + hdr['zero']

            Ns,Nc = ecg.shape
            if Nc!=65:
                RuntimeError('expected 65-channel file')
            
            wct = np.sum(ecg[:,62:65],axis = -1)/3  # Wilson's central terminal     

            bsm65 = np.asarray([ecg[:,i] - wct for i in range(Nc)]).T
    
            #Read Maastricht BSM and subtract WCT
            hdr = read_header(os.path.join(path,fname_mbsm))

            ecg,_ = igb.Load(os.path.join(path,fname_mbsm))#.squeeze()
            ecg = hdr['facteur']*ecg + hdr['zero']

            Ns,Nc = ecg.shape
            if Nc!=184:
                RuntimeError('expected 184-channel file')
            
            mbsm = np.asarray([ecg[:,i] - wct for i in range(Nc)]).T

            #Read probe potentials and subtract WCT
            hdr = read_header(os.path.join(path,fname_probe))

            ecg,_ = igb.Load(os.path.join(path,fname_probe))#.squeeze()
            ecg = hdr['facteur']*ecg + hdr['zero']
            
            Ns,Nc = ecg.shape
            probe = np.asarray([ecg[:,i] - wct for i in range(Nc)]).T

            # .mat file output

            savemat(bsmname,{ 'bsm65':bsm65, 'mbsm':mbsm, 'probe':probe})

            print('The following files were generated: %s'%bsmname)
    
    elif args.option=='leadfields':
        for i in range(args.initialCode,args.finalCode):
            prefix = args.expName+'%02d_ecg.igb'%i
            path = args.dataPath
            name,ext = os.path.splitext(prefix) 

            ecg,_ = igb.Load(os.path.join(path,prefix))#.squeeze()

            # This is wrong. The leads are already referenced to the WCT!
            # wct = np.sum(ecg[:,62:64],axis=1)/3 # Wilson's central terminal
            bsm = ecg #np.asarray([ecg[:,i] - wct for i in range(ecg.shape[-1])]).T
            
            bsmname = os.path.join(args.outPath,name+'_leadfields.mat')
            savemat(bsmname,{ 'bsm':bsm})
            print('The following files were generated: %s'%bsmname)




''' Not being used at the moment so I was lazy
grounding file for use with Blender+datatex
%
fo = fopen(wctname, 'wb');
sl = repmat(' ', 64, 1);
sl(63) = 13; sl(64) = 10;   % CRLF
sb = repmat(sl,1,16);
s = sprintf('x:1 y:1 z:1 t:%d type:float architecture:lsb', Ns);
sb(1:length(s)) = s;
sb(1024) = 12;     % FF
fwrite(fo, sb, 'char');
fwrite(fo, wct, 'float');
fclose(fo);

if 0
  clear ifo
  ifo.scale = 1.6;
  ifo.amplification = 10;
  ifo.paperspeed = 25;
  ifo.tmin = -80;
  ifo.tmax = 560;
  ifo.extend = [60,40];
  mem = show_bsm(bsmname, 'k', 0, ifo);
end
end
'''

if __name__ == '__main__':

    parser = MyParser(description='Python implementation of convert_ecg.m function. \
    -Handle outputs from atrial simulations and prepare for further processing by others.',
    fromfile_prefix_chars='+',
    usage='python convert_ecg.py [OPTIONS] expName initialCode')


    # Required arguments

    parser.add_argument('expName',action='store',type=str,
                        help = 'Experiment base name (e.g. exp904a). The codes and extensions are defined in the initialCode and option flags')
    parser.add_argument('initialCode',action='store',type=int,
                        help = 'Numbers of the first experiment in the batch. Max 2 digits')

    # Optional arguments
    parser.add_argument('-finalCode',action='store',type=int,default = -1,
                        help = 'Number of the last experiment in the batch. Default ignores this parameter')
    parser.add_argument('-dataPath',action='store',type=str,default ='.',
                        help = 'Path to the files')  
    parser.add_argument('-outPath',action='store',type=str,default ='.',
                        help = 'Path to put the output in')       
    parser.add_argument('-option',action='store',type=str,default ='forward-solution',
                        choices = ['forward-solution','leadfields'],
                        help = 'Path to the files')   
    args = parser.parse_args()

    if args.finalCode==-1:
        args.finalCode=args.initialCode+1
    
    main(args)