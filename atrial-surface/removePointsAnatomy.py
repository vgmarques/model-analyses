# -*- coding: utf-8 -*-
"""
Created on Mon May  3 16:02:04 2021

DESCRIPTION: This function adjusts some points that were incorrectly detected 
in the DetectAtrialSurface script. This only has to be done once but it is good
to have it here in case we change the anatomy/spatial resolution

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import numpy as np
import os,sys
sys.path.append('D:\\vgmar\\Documents\\GitHub\\USI\\model-analyses\\aux-functions')
import IgbHandling as igb

fi = open('model24-30Box-atrial-surface_safe.save')
originalIndexes = np.loadtxt(fi,dtype = np.int,skiprows = 1,delimiter = '\n',comments = [';',','])


toRemove = np.array([537780 ,537781 ,553533 ,553534 ,553535 ,553671 ,553672 ,553673 ,553674 ,
                    553812 ,569424 ,569425 ,569426 ,569427 ,569428 ,569429 ,569563 ,569564 ,
                    569565 ,569566 ,569567 ,569704 ,569705 ,585315 ,585316 ,585317 ,585318 ,
                    585319 ,585320 ,585321 ,585322 ,585455 ,585456 ,585457 ,585458 ,585459 ,
                    585460 ,585461 ,585595 ,585596 ,585597 ,585598 ,585599 ,585736 ,601073 ,
                    601074 ,601208 ,601209 ,601210 ,601211 ,601212 ,601213 ,601214 ,601215 ,
                    601347 ,601348 ,601349 ,601350 ,601351 ,601352 ,601353 ,601354 ,601488 ,
                    601489 ,601490 ,601491 ,601492 ,601628 ,601629 ,616964 ,616965 ,616966 ,
                    616967 ,616968 ,616969 ,617101 ,617102 ,617103 ,617104 ,617105 ,617106 ,
                    617107 ,617108 ,617109 ,617240 ,617241 ,617242 ,617243 ,617244 ,617245 ,
                    617246 ,617247 ,617381 ,617382 ,617383 ,617384 ,617385 ,617521 ,617522 ,
                    617660 ,632856 ,632857 ,632858 ,632859 ,632860 ,632861 ,632862 ,632863 ,
                    632994 ,632995 ,632996 ,632997 ,632998 ,632999 ,633000 ,633001 ,633002 ,
                    633135 ,633136 ,633137 ,633139 ,633140 ,633278 ,648753 ,648754 ,648755 ,
                    648756 ,648893 ,648894 ,649031 ,649032 ,649033 ,649170 ,649171 ,664645 ,
                    664646 ,664647 ,664648 ,664649 ,664785 ,664786 ,664787 ,664924 ,664925 ,
                    665062 ,665063 ,665201 ,665339 ,680400 ,680401 ,680538 ,680539 ,680540 ,
                    680541 ,680677 ,680678 ,680679 ,680680 ,680816 ,680817 ,680818 ,680954 ,
                    680955 ,680956 ,681093 ,681094 ,681231 ,696430 ,696431 ,696432 ,696433 ,
                    696570 ,696571 ,696572 ,696708 ,696709 ,696710 ,696847 ,696848 ,696985 ,
                    696986 ,697123 ,697124 ,712322 ,712323 ,712324 ,712325 ,712462 ,712463 ,
                    712464 ,712601 ,712602 ,712739 ,712740 ,712877 ,712878 ,713015 ,713016 ,
                    713154 ,728214 ,728215 ,728216 ,728217 ,728355 ,728356 ,728493 ,728494 ,
                    728631 ,728632 ,728633 ,728770 ,728908 ,729046 ,744247 ,744248 ,744386 ,
                    744524 ,744525 ,744662 ,744663 ,744800 ,744938 ,745076 ,760139 ,760278 ,
                    760416 ,760554 ,760692 ,760830 ,760968])

ReplaceInds = np.isin(originalIndexes,toRemove)


# Types
fi =  open('model24-30Box-atrial-surface_safe.save')
buf = fi.read()
buf = buf.split(', ')
# to account for the last one as ;
buf.append('')
buf[-1] = buf[-2].split('; ')[-1]
buf[-2] = buf[-2].split('; ')[0]
cmm = [l.strip()[1:1+2] for l in buf if l.startswith("#")]
types = np.asarray(cmm,dtype = np.int)
types[ReplaceInds] = 52 


#save
filename ='model24-30Box-atrial-surface.save'
file = open(filename,'w')
file.write('list/nodes egm_endo_epi\n')
for i in range(len(types)):
    if i==len(types)-1:
        file.write('%d; #%d'%(originalIndexes[i],types[i]))
    else:
        file.write('%d, #%d\n'%(originalIndexes[i],types[i]))
file.close()


#%% Now the other files
# Nodes
FnameCell='D:\\vgmar\\model_data\\anatomy\\model30Box\\model24-30Box-atrial-surface-n_safe.igb.gz'
FullCell,hdr= igb.Load(FnameCell)
nz,ny,nx = FullCell.shape
xx,yy,zz = igb.Idx2Coord(originalIndexes[ReplaceInds],(nx,ny,nz))

Tmp = np.copy(FullCell)
Tmp.setflags(write=1)

FullCell = np.swapaxes(Tmp,0,2)

FullCell[xx,yy,zz] = 52

igb.Write('D:\\vgmar\\model_data\\anatomy\\model30Box\\model24-30Box-atrial-surface-n.igb', hdr, FullCell)

#Cells 
FnameCell='D:\\vgmar\\model_data\\anatomy\\model30Box\\model24-30Box-atrial-surface-c_safe.igb.gz'
FullCell,hdr= igb.Load(FnameCell)

xx,yy,zz = igb.Idx2Coord(originalIndexes[ReplaceInds],(FullCell.shape[2],FullCell.shape[1],FullCell.shape[0]))
Tmp = np.copy(FullCell)
Tmp.setflags(write=1)

FullCell = np.swapaxes(Tmp,0,2)

FullCell[xx,yy,zz] = 52

igb.Write('D:\\vgmar\\model_data\\anatomy\\model30Box\\model24-30Box-atrial-surface-c.igb', hdr, FullCell)


#%%
# The outcomes are not the same exactly!!!
# This is ok but bear that in mind


