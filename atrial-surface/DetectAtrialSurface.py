# -*- coding: utf-8 -*-
"""
Created on Thu Aug 13 14:09:46 2020

Detect atrial surface

Important notice: It is possible that some points are not selected properly and 
need to be adjusted after running the first simulations


Version 1.0


@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""
import os,sys
import numpy as np
import argparse
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb


class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write('error: %s\n' % message)
        self.print_help()
        sys.exit(2)

#%%
def main(args):
    # Setup
    
    # read file
    cell,header = igb.Load(os.path.join(args.dataPath,args.torsoOrHeartFile))
    cell = np.swapaxes(cell.squeeze(),0,2)
    nx,ny,nz= cell.shape
    print('Loaded file.\n')
    
    # Detect cells (Victor's method, slower)
    print('Detecting cells...\n')
    EndoCells = igb.GetAtrialSurface(cell,
                         innerTypes = args.atrialCellTypes,
                         outerTypes = args.endoBloodCellTypes,
                         linear_indexes = True,
                         mode = 'cells')
    
    EpiCells = igb.GetAtrialSurface(cell,
                         innerTypes = args.atrialCellTypes,
                         outerTypes = args.epiBloodCellTypes,
                         linear_indexes = True,
                         mode = 'cells')
    
    BothIdx = np.isin(EndoCells,EpiCells)
    BothCells = EndoCells[np.where(BothIdx)[0]]
    EndoCells = EndoCells[np.where(~BothIdx)[0]]
    EpiCells = EpiCells[np.where(~np.isin(EpiCells,EndoCells))[0]]

    nodeCodes = [args.endoCellType,args.commonCellType,args.epiCellType]
    # Cells
    CellHeader = {'x':nx,
                  'y':ny,
                  'z':nz,
                   't':1,
                  'type':'byte',
                  'architecture':'lsb'}
    
    AtrialSurfaceCells = np.zeros((nx,ny,nz))
    AtrialSurfaceCells[igb.Idx2Coord(EndoCells,(nx,ny,nz))] = nodeCodes[0]
    AtrialSurfaceCells[igb.Idx2Coord(BothCells,(nx,ny,nz))] = nodeCodes[1]
    AtrialSurfaceCells[igb.Idx2Coord(EpiCells,(nx,ny,nz))] = nodeCodes[2]
    
    filename = os.path.join(args.outputPath,args.modelName+'-atrial-surface-c.igb')
    
    igb.Write(filename,CellHeader,AtrialSurfaceCells)
    print('Wrote file %s'%filename)

    print('Done!\n')

    
    # Detect nodes
    print('Detecting nodes...\n')

    EndoNodes = igb.GetAtrialSurface(cell,
                         innerTypes = args.atrialCellTypes,
                         outerTypes = args.endoBloodCellTypes,
                         linear_indexes = True,
                         mode = 'SP')
    
    
    EpiNodes = igb.GetAtrialSurface(cell,
                         innerTypes = args.atrialCellTypes,
                         outerTypes = args.epiBloodCellTypes,
                         linear_indexes = True,
                         mode = 'SP')
    
    BothIdx = np.isin(EndoNodes,EpiNodes)
    
    BothNodes = EndoNodes[np.where(BothIdx)[0]]
    EndoNodes = EndoNodes[np.where(~BothIdx)[0]]
    EpiNodes = EpiNodes[np.where(~np.isin(EpiNodes,BothNodes))[0]]
    
    
    print('Done!\n')

    # Save cells and nodes as igbs
    print('Saving igbs...\n')

    #Nodes
    NodeHeader = {'x':nx+1,
                  'y':ny+1,
                  'z':nz+1,
                   't':1,
                  'type':'byte',
                  'architecture':'lsb'}
    
    AtrialSurfaceNodes = np.zeros((nx+1,ny+1,nz+1))
    AtrialSurfaceNodes[igb.Idx2Coord(EndoNodes,(nx+1,ny+1,nz+1))] = nodeCodes[0]
    AtrialSurfaceNodes[igb.Idx2Coord(BothNodes,(nx+1,ny+1,nz+1))] = nodeCodes[1]
    AtrialSurfaceNodes[igb.Idx2Coord(EpiNodes,(nx+1,ny+1,nz+1))] = nodeCodes[2]
    
    filename = os.path.join(args.outputPath,args.modelName+'-atrial-surface-n.igb')
    igb.Write(filename,NodeHeader,AtrialSurfaceNodes)
    print('Wrote file %s\n'%filename)

    # Make a node savelist with corresponding cell types commented
    print('Making save lists...\n')
    
    filename = os.path.join(args.outputPath,args.modelName+'-atrial-surface.save')
    file = open(filename,'w')
    file.write('list/nodes egm_endo_epi\n')
    for i,nodeList in enumerate([EndoNodes,BothNodes,EpiNodes]):
        for j in range(len(nodeList)):
            if j==len(nodeList)-1 and i==2:
                file.write('%d; #%d'%(nodeList[j],nodeCodes[i]))
            else:
                file.write('%d, #%d\n'%(nodeList[j],nodeCodes[i]))
    file.close()
    print('Wrote file %s\n'%filename)

    print('All done!')

#%%
if __name__ == '__main__':
    
    parser = MyParser(description='Detects the cells and nodes corresponding to\
                      the atrial surfaces, giving different codes to endo and epicardial layers. \
                          Intended for -torso-c.igb but should work with -heart-cell.igb files',
                          fromfile_prefix_chars='+',usage='python DetectAtrialSurface.py +DetectAtrialSurface torsoOrHeartFile modelName')
    
    
    parser.add_argument('torsoOrHeartFile',action='store',type=str,
						help = 'Name of the torso or heart cell file')
    parser.add_argument('modelName',action='store',type=str,
						help = 'Name of the model and prefix to output files.')
    
    # Optional Arguments
    parser.add_argument('-dataPath','-dp',dest= 'dataPath',
						action='store',default = '.',type=str,
						help = 'Path to the torso file. Defaults to current directory')
    parser.add_argument('-outputPath','-op',dest= 'outputPath',
						action='store',default = '.',type=str,
						help = 'Path to output the files. Defaults to current directory')
    
    parser.add_argument('-endoCellType',dest= 'endoCellType',
						action='store',default =50,type=int,
						help = 'Cell type corresponding to the endocardial layer. Default 50')
    parser.add_argument('-epiCellType',dest= 'epiCellType',
						action='store',default =52,type=int,
						help = 'Cell type corresponding to the epicardial layer. Default 52')   
    parser.add_argument('-commonCellType',dest= 'commonCellType',
						action='store',default =52,type=int,
						help = 'Cell type corresponding to cells belonging to both endo and epicardium. Default 51')   
    parser.add_argument('-atrialCellTypes',nargs='+', type=int,
						action='store',default =[113,114,115,116,117,118,119],
						help = 'Cell types for the atria. Default 113 to 119')   
    parser.add_argument('-endoBloodCellTypes',nargs='+', type=int,
						action='store',default =[104],
						help = 'Cell types for the blood touching the endocardium. Default 104')   
    parser.add_argument('-epiBloodCellTypes',nargs='+', type=int,
						action='store',default =[0,1,102,120,161,162],
						help = 'Cell types for the blood and organs touching the epicardium. Default 102,120,161,162')            
    
    
    args = parser.parse_args()
    
    main(args)

    