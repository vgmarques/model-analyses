# -*- coding: utf-8 -*-
"""
Created on Fri Aug 20 19:42:56 2021

DESCRIPTION: Make 2 layer model for video plotting

@author: Victor G. Marques, v.goncalvesmarques@maastrichtuniversity.nl
"""

import sys, os
import numpy as np
upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb

modelName = 'model24-29'
modelFolder = '/users/vgonalve/exec/anatomy/model29'
# fullCellFile = '%s-heart-ds350-cell.igb'%modelName
fullCellFile = '%s-heart-cell.igb'%modelName
surfaceCellFile = '%s-atrial-surface-c.igb'%modelName #


#%% Load Full cell, get positions
print('Loading cell file')
FullCell,hdr = igb.Load(os.path.join(modelFolder,fullCellFile))
from copy import deepcopy
FullCell = deepcopy(FullCell)
zz,yy,xx = np.where(~np.isin(FullCell,[113,114,115,116,117,118,119,104]))
FullCell[zz,yy,xx] = 0

zz,yy,xx = np.where(np.isin(FullCell,[113,114,115,116,117,118,119]))
FullCell[zz,yy,xx] = 52


nz,ny,nx = FullCell.shape
#%% Now load surface
print('Getting atrial surface')
if surfaceCellFile is None:
    EndoCells = igb.GetAtrialSurface(np.swapaxes(FullCell,0,2),
                             innerTypes = [52],
                             outerTypes = [104],
                             linear_indexes = True,
                             mode = 'cells')

    OutputCell = np.swapaxes(FullCell,0,2)
    bx,by,bz = np.where(np.isin(OutputCell,[104]))
    OutputCell[bx,by,bz] = 0
    OutputCell[igb.Idx2Coord(EndoCells,(nx,ny,nz))] = 50
else:
    cellSurface,_ = igb.Load(os.path.join(modelFolder,surfaceCellFile))
    OutputCell = np.swapaxes(FullCell,0,2)
    for layerCode in [50,51,52]:
        bz,by,bx = np.where(cellSurface==layerCode) #Not swapped
        OutputCell[bx,by,bz] = layerCode
    zx,zy,zz = np.where(~np.isin(OutputCell,[50,51,52]))
    OutputCell[zx,zy,zz] = 0
        

CellHeader = {'x':nx,
              'y':ny,
              'z':nz,
              't':1,
              'type':'byte',
               'architecture':'lsb'}


igb.Write(os.path.join(modelFolder,'two-layers-%s-heart-cell.igb'%modelName),CellHeader,OutputCell)

