
#%% Load libraries (#TODO IF YOU ARE RUNNING TO SAVE STUFF AGAIN, UNCOMMENT THOSE LINES)
import os,sys
import numpy as np
import vtk
from vtk.util.numpy_support import numpy_to_vtk,numpy_to_vtkIdTypeArray,vtk_to_numpy
import matplotlib.pyplot as plt
from copy import deepcopy

from skimage.measure import label as ConnectedComponents
from skimage.morphology import binary_dilation as imdilation
from skimage.morphology import binary_erosion as imerosion

upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

import IgbHandling as igb
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup
from UserInterfaceFunctions import FancyElectrodePlacementStyle as MouseInteractor
import egm_processing as egmp
from DirectionTrackingCalculator import VelocityCalculator,VelocityAnalyzer
from SourceTrackingCalculator import SourceTrackingCalculator
from RecurrenceComputation import RecurrenceComputation

from WaveCalculators import WavemapCalculator,WavemapAnalyzer


from StandardVTKObjects import EGMColormap

# %load_ext autoreload 
# %autoreload 2
# %matplotlib qt
#%% Load anatomy and initial points
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm


interAtrialConnections,CSInds,LAInds,RAInds = igb.GetAnatomicalInds(anatomyPath,(nx,ny,nz),Anatomy)




initialPoints = np.array([20383, 15497, 5654, 1110, 21897, 12919, 24652, 18574, 3218, 21553, 5736,
                          2548, 8756, 22228, 17290, 9391, 9541, 19127, 15307, 12715]) # Determined by getting the center of 20 k-means clusters

LA_Seeds = np.where(np.isin(initialPoints,LAInds))[0]
RA_Seeds = np.where(np.isin(initialPoints,RAInds))[0]

#%% Load Data
basename = 'EX0008_FB0_FL200s08'
ve_file = '/mnt/d/vgmar/model_data/EX0008/%sf_ve_egm_endo_epi.igb.gz'%basename

egmDataAll,hdr = igb.Load(ve_file,returnRawSignal=True) # Workaround for memory issues
egmDataAll = egmDataAll.squeeze()
egmDataAll = egmDataAll[1000:,veIndexes] # ignore 1s for initialization

#%% Initialize Source and Direction detection calculators
# Direction
vCalc = VelocityCalculator()
vCalc.Radius = np.sqrt(3.5**2+3.5**2)
vAnalyzer = VelocityAnalyzer()

# Sources
STC = SourceTrackingCalculator()

# Recurrence
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1

# Waves
wCalc = WavemapCalculator()
wCalc.NeighborRadius = np.sqrt(2**2+3**2)#*1.05
wCalc.ConductionThreshold = 0.1 # mm/ms
electrodeNeighbors = [[0,1,4,5],[0,1,2,4,5,6],[0,1,2,3,5,6,7],[2,3,6,7],
                      [0,1,4,5,8,9],[0,1,2,4,5,6,8,9,10],[1,2,3,5,6,7,9,10,11],[2,3,6,7,10,11],
                      [4,5,8,9,12,13],[4,5,6,8,9,10,12,13,14],[5,6,7,9,10,11,13,14,15],[6,7,10,11,14,15],
                      [8,9,12,13],[8,9,10,12,13,14],[9,10,11,13,14,15],[10,11,14,15]]
wCalc.NeighborList = electrodeNeighbors
minimumDuration = 0 #ms



#%% Locate sources starting from each loop
# All source tracking algorithms here

Output = np.zeros((len(initialPoints),2)) 
SourceLocations = np.zeros((len(initialPoints),3))
lSegment = 1000
maxSteps = int(egmDataAll.shape[0]/lSegment)
'''
The Output array holds the classification and number of steps for
getting to that result. Possible classification outcomes are:
- 0: Not found in time limit
- 1: rotational source
- 2: focal source
- 3: Source in the other atrium
- 4: enclosed region
- -1: some error

The SourceLocations are the catheter centers when sources were found
'''
# for s01:
# - wrong focal: 3
# - missed other atrium: 11, 12 partially, 
outFolder = '/mnt/d/vgmar/model_data/EX0008/source_tracking_endless'

flog = open(os.path.join(outFolder,'%s_logfile.log'%basename),'w')
flog.write('ip tStep pos rr dirPref pSP CLC nTraj avgCurv maxCurv minAngle maxAngle\n')

AllPositions = []

for ip,IP in enumerate(initialPoints):
    print('------------------ %02d ------------------'%ip)
    # if ip>=2: continue
    # Phase singularities break 4,5,7,6, 

    # With graph (s02, 04-04-2022):
    # Found rotors: 4, 13,17,
    # Too far from source: 14
    # Found focal: 16 / 8,10
    # Enclosed regions: 0,1,2,6,9,11,18,19 (solved by placing in center: 1,2,19)

    # Problems due to the movement of electrodes: 5,7,8,10, (solved: 8,10)

    
    # Sequences that ignored rotational sources: 5?,7,14?,17,18?,19? / 1,6,9,18,19
    # Sequences that ignored focal sources: 15/11
    # wtf: 12
    # Detected focal but did not stop: 3,7
    # Wrong rotor: 15



    if ip in LA_Seeds:
        currentAtriumInds = LAInds 
        chosenAnatomy = Anatomy[LAInds]
        IP = np.where(LAInds==IP)[0][0]
        # OtherChamber = RAInds
    elif ip in RA_Seeds:
        currentAtriumInds = RAInds 
        chosenAnatomy = Anatomy[RAInds]
        IP = np.where(RAInds==IP)[0][0]

    nSteps = 0
    dS = 10 # mm to displace the catheter

    #Stuff to store between loops
    previousCatheterPositions   = np.empty((0,3))
    previousCatheterIndexes   = list()
    previousArrowCenters        = np.empty((0,3))
    previousArrowDirections     = np.empty((0,3))
    previousArrowPreferentiality    = np.empty((0,1))
    foundFlag = False
    invalidNext = None
    closedFlag = False
    loop = 1
    tStep = 0

    while True:
        
        print(tStep)
        # if tStep==18: break
        # Set-up catheter
        CatheterObj = CatheterClass('HDGrid-4')

        if tStep==0 and loop==1:
            CatheterObj.GetTissuePatch(IP,chosenAnatomy)
            nextPos = IP
        else:
            CatheterObj.GetTissuePatch(nextPos,chosenAnatomy)

        try:
            CatheterObj.ProjectionFunction(chosenAnatomy,rotationAngle =0)
        except:
            fail = 0
            while fail<5:
                try:
                    CatheterObj.ProjectionFunction(chosenAnatomy,rotationAngle =0)
                    fail = 0
                    break
                except:
                    print('%d -> problem placing'%CatheterObj.CatheterCenterIndex)
                    dists = np.linalg.norm(CatheterObj.Patch-np.array([0,0,0]),axis=1)
                    newCenterIndex = np.where(dists==np.min(dists[dists!=0]))[0][0]
                    nextPos = CatheterObj.PatchIndexes[newCenterIndex]
                    CatheterObj.GetTissuePatch(nextPos,chosenAnatomy)
                fail +=1
            
            if fail!=0:
                Output[ip,:] = [-1,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                foundFlag = True
                break

        # Signals and activations
        timeInterval = [(tStep%maxSteps)*lSegment, (tStep%maxSteps+1)*lSegment]
        
        CatheterObj.GetSignals(egmDataAll[timeInterval[0]:timeInterval[1],currentAtriumInds],rawInput=True, hdr=hdr)
        CatheterObj.DetectActivations(method = 'adaptiveThreshold')

        ########################################## Recurrence analysis ###########################################
        # distanceMatrix, _, activationCycleLength=recComp.ComputeDistanceMatrix(CatheterObj, CatheterObj.Activations)
        # estimatedAFCL = np.median(activationCycleLength)

        # RP, recurrenceTime, recurrenceThreshold, _ = recComp.ComputeRecurrencePlot(distanceMatrix,
        #                                                                         CatheterObj.SamplingFrequency,
        #                                                                         estimatedAFCL,
        #                                                                         recurrenceThreshold= 0.15)

        # RPE = recComp.ErodeRP(RP,distanceMatrix,linear=True)
        # minLineLength = np.round(estimatedAFCL/recComp.SampleShift)

        # rqa = recComp.ComputeRQA(RPE,estimatedAFCL/1000,CatheterObj.SamplingFrequency,
        #                                             minDiagonalLine = int(minLineLength))
        # print('RR:%0.3f'%rqa['recurrence_rate'])

        ######################################### Direction Calculations #########################################

        allActivations,allPositions,allElectrodeIndices = vCalc.AdjustSignalsCV(CatheterObj,useRealPositions=True)
        xVelocity, yVelocity = vCalc.Compute2DActivationWavefront(allActivations, allPositions)
        ConductionVelocity = vCalc.MakeCVDict(allActivations,allElectrodeIndices,xVelocity,yVelocity)
        intervalPreferentiality = vAnalyzer.AnalyzeVelocity([0,1000],ConductionVelocity)
        
        # Calculate variance of angles
        Vx= np.array([np.nanmean(element[~np.isinf(element)]) for element in intervalPreferentiality['additionalInformation']['vx']])
        Vy= np.array([np.nanmean(element[~np.isinf(element)]) for element in intervalPreferentiality['additionalInformation']['vy']])
        angles = np.arctan2(Vy,Vx)
        _, circularVariance = egmp.CircularMeanAndVariance(angles)

        DirectionalPreferentiality = 1-circularVariance
        Velocities3D = vAnalyzer.ComputeMeanVelocityV2(intervalPreferentiality,vCalc)

        # print('Directional Preferentiality: ',DirectionalPreferentiality)
        ############################################# Source detection ##############################################

        ## PS detection
        CatheterObj.GetPhaseSignal(phaseMethod='sinusoidal')
        STC.DoubleRingPSDetection(CatheterObj)

        # Percentage with any SPs
        psLoc = np.array([STC.PSLocations[i,:,:].any() for i in range(STC.PSLocations.shape[0])])
        PercentageSPs=psLoc.sum()/lSegment
        # print('Percentage SPs: %0.3f'%PercentageSPs)

        ## CL Coverage 
        allActivations,allPositions,allElectrodeIndices = vCalc.AdjustSignalsCV(CatheterObj)
        actGroups = vCalc.GroupValidActivations(allActivations,allPositions,allElectrodeIndices)
        groupKeys = np.array(list(actGroups.keys()))
        actGroups = {key:actGroups[key] for key in groupKeys} 
        estimatedAFCL = np.median([np.median(np.diff(act)) for act in CatheterObj.Activations]) 
        preferentialityFull = STC.ComputeCLDispersion(actGroups,estimatedAFCL)
        CLCoverage = np.mean(preferentialityFull)

        # print('CL Coverage: %0.3f'%CLCoverage)
        ### Decision of source occurrence ###

        ## Wave analyses 
        activationGroups = None#egmp.GroupActivations(CatheterObj.Activations,longestElectrodeAxis = 30,cvThreshold =wCalc.ConductionThreshold)
        waves,activationData,componentNumber = wCalc.DetectActivationComponents(CatheterObj,activationGroups)

        # Remove small waves
        k = 0
        poplist = []
        for waveIndex,wave in waves.items():
            if len(wave.Members)<8: 
                poplist.append(waveIndex)
                continue
        for element in poplist: waves.pop(element)

        # Get Trajectories from the individual waves
        wAnalyzer = WavemapAnalyzer(waves,wCalc.ConductionThreshold,wCalc.NeighborRadius)
        wAnalyzer.MinimumDuration = minimumDuration
        # Get Mean values for all analysees
        WaveAnalysisResults = wAnalyzer.ComputeAllAnalyses(filter=True,selection='shortest',centralTendencyFunction = 'mean')
        # print(WaveAnalysisResults)
        WaveClassification = wAnalyzer.WaveClassification(WaveAnalysisResults)

        # Log
        flog.write('%d %d %d %f %f %f %f %d %f %f %f %f\n'%(ip,tStep,nextPos,-1,DirectionalPreferentiality, #rqa['recurrence_rate']
                    PercentageSPs,CLCoverage,WaveAnalysisResults['NumberOfTrajectories'],WaveAnalysisResults['CurvatureScores'],
                    WaveAnalysisResults['MaxCurvatureScores'],WaveAnalysisResults['MinAngles'],WaveAnalysisResults['MaxAngles'])) #'ip tStep rr dirPref pSP CLC nTraj avgCurv maxCurv minAngle maxAngle'

        if (wAnalyzer.ComputeWaveSizes()>len(CatheterObj.ElectrodesTemplate)).any():
            print('Trajectory Overlap -> rotor')
            WaveClassification = 'rotor'
        
        '''
        This portion is the most likely to change in this code. That is the case because what currently determines
        the sources are some empyrical thresholds that have not been determined based on physiology.
        Apart from that, as more techniques are added to the above, the requirements change.
        '''
        # Focal activity: almost simultaneous activity, lowest CL coverage. No PSs
        print("%0.3f, %0.3f"%(CLCoverage,DirectionalPreferentiality))

        if CLCoverage<=0.1 and  DirectionalPreferentiality<=0.7 and WaveClassification=='focal':#PercentageSPs<=0.05:
            # If it is focal, is it coming from the other atrium?
            if np.isin(currentAtriumInds[CatheterObj.ElectrodeIndexes],interAtrialConnections).any():
                #Then, we have a source in the other atrium
                Output[ip,:] = [3,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                print('%d: Source in other atrium'%ip)
                foundFlag = True
                
            else:
                # This is a real focal source
                Output[ip,:] = [2,nSteps]
                SourceLocations[ip,:]  = CatheterObj.CatheterCenter
                print('%d: Focal source found'%ip)
                foundFlag = True
                
        # Rotational activity: high percentage of time with PSs, higher CL coverage
        elif WaveClassification=='rotor' and CLCoverage >=0.1:# or (WaveClassification=='curvature')) and\
            #   CLCoverage >=0.1 and PercentageSPs>=0.2 and rqa['recurrence_rate']<0.5:
            Output[ip,:] = [1,nSteps]
            SourceLocations[ip,:] = CatheterObj.CatheterCenter
            print('%d: Rotational source found'%ip)
            foundFlag = True
            
        # Nothing found: decide next step
        else:
            # dists = np.linalg.norm(previousCatheterPositions-CatheterObj.CatheterCenter,axis=1)
            # if closedFlag: dS = 5 # if I am moving too close, reduce step

            nextPos,invalidNext,closedFlag,regionIndexes = vAnalyzer.NextCatheterPlacementV2(Velocities3D,CatheterObj,chosenAnatomy,dS,invalidNext)

            if np.isin(currentAtriumInds[nextPos],CSInds):# or np.isin(currentAtriumInds[nextPos],OtherChamber)
                # Activation coming from CS, consider as other atrium
                Output[ip,:] = [3,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                print('%d: Source in other atrium'%ip)
                foundFlag = True
            if closedFlag:
                print('%d: Valid enclosed region found'%ip)
                # print('Changing distance to 5 mm')
                # ds = 5
                Center = np.median(chosenAnatomy[regionIndexes],axis=0)
                DistCenter = np.linalg.norm(chosenAnatomy-Center,axis=1)
                nextPos = np.where(DistCenter==np.min(DistCenter))[0][0]

                # Output[ip,:] = [4,nSteps]
                # SourceLocations[ip,:] = CatheterObj.CatheterCenter
                # foundFlag = True
            if nextPos in previousCatheterIndexes:
                print('Going back and forth:')# changing distance to 5mm')
                # dS = 5
                # Going back and forth
                Center = np.median(chosenAnatomy[[nextPos,
                                                previousCatheterIndexes[-1]]],
                                                axis=0)
                DistCenter = np.linalg.norm(chosenAnatomy-Center,axis=1)
                nextPos = np.where(DistCenter==np.min(DistCenter))[0][0]

            previousCatheterIndexes.append(nextPos)

            # print(nextPos)
        ## Clean-up for next loop
        nSteps += 1
        tStep += 1
        if tStep%maxSteps == 0: loop +=1
        previousCatheterPositions       = np.vstack([previousCatheterPositions,CatheterObj.CatheterCenter])
        previousArrowCenters            = np.vstack([previousArrowCenters,CatheterObj.ElectrodeCoordinates])
        previousArrowDirections         = np.vstack([previousArrowDirections,Velocities3D])
        previousArrowPreferentiality    = np.append(previousArrowPreferentiality,np.array(intervalPreferentiality['value']))
        if foundFlag: break
    AllPositions.append(previousCatheterPositions)
    if not foundFlag:
        Output[ip,:] = [0,nSteps]
        SourceLocations[ip,:] = CatheterObj.CatheterCenter
        print('%d: No source found'%ip)

    

flog.close()     


np.save(os.path.join(outFolder,'%s_ClassificationOutput.npy'%basename),Output)
np.save(os.path.join(outFolder,'%s_SourceLocations.npy'%basename),SourceLocations)
np.save(os.path.join(outFolder,'%s_AllCatCenters.npy'%basename),AllPositions)
print(Output)
#%% Plotting 
nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)


currentSignal = np.zeros(len(Anatomy),dtype=float)
# currentSignal[currentAtriumInds[invalidNext]] = 1
# currentSignal[interAtrialConnections] = 1
currentSignal = currentSignal[rearrangeInds]

AnatomyActor,cbarActor,AnatomyData = qv.MakeIgbUnstructuredGridActor(FullCell,[50,51],currentSignal,
                                            vmin=0,vmax=1,smoothFilter=True,cmap='heatmap',
                                                returnData=True)
AnatomyActor.GetProperty().SetOpacity(0.5)

PreviousDirectionActor = qv.MakeGlyphActor(previousArrowCenters,previousArrowDirections,
                                        magnitudes = previousArrowPreferentiality,
                                        vmin=0,vmax=1,cmap='jet',mode='scale')

LastCatheter = qv.MakeGlyphActor(CatheterObj.ElectrodeCoordinates,np.zeros((16,3)),
                                magnitudes = None,source='sphere',
                                vmin=0,vmax=1,cmap='jet',mode='scale')

CatCenters = qv.MakeGlyphActor(previousCatheterPositions,np.zeros((len(previousCatheterPositions),3)),
                                magnitudes = np.arange(len(previousCatheterPositions)),source='sphere',
                                vmin=0,vmax=previousCatheterPositions.shape[0],cmap='jet',
                                mode='color')



InitialPointActor,_ = qv.MakePolyDataActor(chosenAnatomy[IP,:].reshape(-1,3),np.ones((1,1)),cmap='jet',vmin=0)

# qv.QuickRenderWindowInteractor([AnatomyActor,InitialPointActor,
#                                 PreviousDirectionActor,LastCatheter])
# if tStep>4:
qv.QuickRenderWindowInteractor([AnatomyActor,CatCenters,PreviousDirectionActor])

#%%
# from StandardVTKObjects import *

# renderer = StandardViewRenderer([AnatomyActor,CatCenters,PreviousDirectionActor],
#                                 cameraType='egmAnterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin = OneViewRenderWindow(renderer,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin.Render()
'''
Some observations:
- At least for the source 2, we run exactly into the problem
of not going towards the center of the rotor

- Regions with focal activation require better definitions. This
is a problem with the "source in other atrium" scenario

- The CS behaves differently than bundles. If a next suggested source is in the Cs, should 
be automatically considered other atrium

* Some issues finding the next region depending on the position
in the atria. FIXME done for almost all positions. maybe exclude 51 code?

- The "memory" update needs to be clever. Simply getting the directions of the previous 
positions fails when we have the electrodes going around an area

- There should be a "circuit breaker" for a selected area smaller than X cm^2, meaning
a source is contained there. This could be a hit to go for DHHD or a solution for
meandering sources
'''
# %%
# chosenAnatomyActor,_ = qv.MakePolyDataActor(chosenAnatomy,np.zeros((1,len(chosenAnatomy))),cmap='jet',vmin=0)
# chosenAnatomyActor.GetProperty().SetOpacity(0.1)

# InitialPointActor,_ = qv.MakePolyDataActor(chosenAnatomy[IP,:].reshape(-1,3),np.ones((1,1)),cmap='heatmap',vmin=0)
# InitialPointActor.GetProperty().SetRenderPointsAsSpheres(1)
# InitialPointActor.GetProperty().SetPointSize(60)

# qv.QuickRenderWindowInteractor([chosenAnatomyActor,InitialPointActor])

# PreviousDirectionActor = qv.MakeGlyphActor(previousArrowCenters[3*16:6*16,:],previousArrowDirections[3*16:6*16,:],
#                                             magnitudes = previousArrowPreferentiality[3*16:7*16],
#                                             vmin=0,vmax=1,mode='scale',arrowColor='DarkBlue')
# RightDirectionActor = qv.MakeGlyphActor(previousArrowCenters[-16:,:],previousArrowDirections[-16:,:],
#                                             magnitudes = previousArrowPreferentiality[-16:],
#                                             vmin=0,vmax=1,cmap='jet',mode='scale',arrowColor='DarkBlue')


# CatCenters = qv.MakeGlyphActor(previousCatheterPositions[[3,4,5,-1],:],np.zeros((16,3)),
#                                 magnitudes = np.ones(16)*0.5,source='sphere',
#                                 vmin=0,vmax=1,cmap='jet',
#                                 mode='color')

# qv.QuickRenderWindowInteractor([AnatomyActor,CatCenters,PreviousDirectionActor,RightDirectionActor])

# from StandardVTKObjects import *
# renderer = StandardViewRenderer([CatCenters,PreviousDirectionActor,RightDirectionActor],
#                                 cameraType='egmPosterior',
#                                 colorbarActor = cbarActor,
#                                 textActor = None,
#                                 viewportSplit = None)
# PosWin1 = OneViewRenderWindow(renderer,windowSize = [1200,800],
#                         OffScreenRendering = False)  
# PosWin1.Render()
