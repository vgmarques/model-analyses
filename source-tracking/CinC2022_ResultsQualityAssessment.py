'''
This script complements the source-tracking outputs, for the version developed for CinC2022

In this method, once a position detects a source, the tracking is stopped. This means that the
only "positives" are the last positions. We use phase analysis in transmembrane signals to detect
rotors, and we use the same signals for detecting breakthroughs. 

In this script, I analyze all the positions and compare with the euclidian distance to a reentry or
BT to determine whether it was a correct detection. 

An improvement would be to use geodesic distance, obviously, but for computational reasons I kept it as is

'''

#%% Load libraries
import os,sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import multiprocessing as mp
import pickle
from sklearn.linear_model import LinearRegression


upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

import IgbHandling as igb
import quick_visualization as qv
from CatheterObjects import CatheterClass,CatheterGroup



#%% Determine the experiment names
exp = 'EX0008'
RESULTSPATH = '/mnt/d/vgmar/model_data/%s/source_tracking_v3'%exp
PSPATH = '/mnt/d/vgmar/model_data/%s/psdetection/filtered'%exp# filtered
BTPATH = '/mnt/d/vgmar/model_data/%s/bt_detection'%exp

experimentNames = []
for file in os.listdir(RESULTSPATH):
    if file.startswith(exp) and file.endswith('_ClassificationOutput.npy'):
        experimentNames.append(file.split('_ClassificationOutput.npy')[0])
experimentNames = np.sort(experimentNames)

print(experimentNames)

#%% Load Anatomy
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm
nz,ny,nx = FullCell.shape
linearInds = igb.Coord2Idx(Anatomy,(nx,ny,nz))
rearrangeInds = np.argsort(linearInds)

interAtrialConnections,CSInds,LAInds,RAInds = igb.GetAnatomicalInds(anatomyPath,(nx,ny,nz),Anatomy)

initialPoints = np.array([20383, 15497, 5654, 1110, 21897, 12919, 24652, 18574, 3218, 21553, 5736,
                          2548, 8756, 22228, 17290, 9391, 9541, 19127, 15307, 12715]) # Determined by getting the center of 20 k-means clusters
LA_Seeds = np.where(np.isin(initialPoints,LAInds))[0] # 0,  1,  4,  5,  6,  7,  9, 13, 14, 17, 18, 19
RA_Seeds = np.where(np.isin(initialPoints,RAInds))[0] # 2,  3,  8, 10, 11, 12, 15, 16

#%% Load median trajectory positions
TrajectoryDF = pd.read_csv(os.path.join(PSPATH,'%s_AverageRelevantTrajectoryPosition.csv'%exp))
TrajectoryDF = TrajectoryDF[TrajectoryDF.columns[1:]]
maxSegments = np.max(TrajectoryDF['segment'])+1

#%% Load shortest distance to a trajectory or BT
with open(os.path.join(RESULTSPATH,'%s_DistancesToPS.pkl'%exp),'rb') as file:
    DistancesPerExperimentPS = pickle.load(file)

with open(os.path.join(RESULTSPATH,'%s_DistancesToBT.pkl'%exp),'rb') as file:
    DistancesPerExperimentBT = pickle.load(file)

#%% Iterate the experiments
## A true value means: A trajectory median position was within 7.5 mm from catheter center
inRadius = 8 # mm np.sqrt((4*3)**2+(4*4)**2)/2

TPRot=0
FPRot=0
FNRot=0
TNRot=0

TPList = []
FPList = []
FNList = []
TNList = []

LAExperiments = [0,1,2,3,4]
RAExperiments = [5,6,7]



for experimentIndex,experimentName in enumerate(experimentNames):
    ClassificationResults = np.load(os.path.join(RESULTSPATH,
                                    experimentName+'_ClassificationOutput.npy')).astype(int)
    rotorClassifications = ClassificationResults[:,0]==1 #np.where(ClassificationResults[:,0]==1)[0]
    experimentDistances = DistancesPerExperimentPS[experimentName]#[rotorClassifications,:]
    print(np.prod(experimentDistances.shape)-np.isnan(experimentDistances).sum())

    for i,row in enumerate(experimentDistances):
        row = row[~np.isnan(row)]
        TRUE = row<=inRadius 

        if not rotorClassifications[i]: # BT detected or nothing
            POSITIVE = np.zeros(len(row),dtype=bool)
        else:
            # POSITIVE = np.isnan(np.roll(row,-1)) & (~np.isnan(row)) #next one is nan = stopped. No nan = stopped
            # if POSITIVE.sum()==0: 
            POSITIVE = np.zeros(len(row),dtype =bool)
            POSITIVE[-1] = True # it was the shortest one

        # Get catheter positions for experiment
        TPRot += np.sum(TRUE & POSITIVE)
        FPRot += np.sum(~TRUE & POSITIVE)
        FNRot += np.sum(TRUE & ~POSITIVE)
        TNRot += np.sum(~TRUE & ~POSITIVE)

        # Get indices and append to list: experimentIndex, seed and tStep

        [TPList.append([experimentIndex,i,ind]) for ind in np.where(TRUE & POSITIVE)[0]]
        [FPList.append([experimentIndex,i,ind]) for ind in np.where(~TRUE & POSITIVE)[0]]
        [FNList.append([experimentIndex,i,ind]) for ind in np.where(TRUE & ~POSITIVE)[0]]
        [TNList.append([experimentIndex,i,ind]) for ind in np.where(~TRUE & ~POSITIVE)[0]]


#%%

print('Sensitivity: %0.3f for rotors'%((TPRot)/(TPRot+FNRot)))
print('Specificity: %0.3f for rotors'%((TNRot)/(TNRot+FPRot))) #* Not very useful due to high TN
print('Precision: %0.3f for rotors'%((TPRot)/(TPRot+FPRot)))
print('Negative predictive value: %0.3f for rotors'%((TNRot)/(TNRot+FNRot)))#* Not very useful due to high TN
print('Accuracy: %0.3f for rotors'%((TPRot+TNRot)/(TPRot+TNRot+FNRot+FPRot))) #* Not very useful due to high TN


print('FPR: %0.3f for rotors\n'%((FPRot)/(FPRot+TNRot)))

print('TRUE reentries: %s'%(TPRot+FNRot))
print('TRUE no reentries: %s'%(TNRot+FPRot))
print('Algorithm reentries: %s'%(TPRot+FPRot))
print('Algorithm no reentries: %s'%(FNRot+TNRot))

#%% Iterate the experiments, now for BTs
## A true value means: A trajectory median position was within 7.5 mm from catheter center
inRadius = 8 # mm np.sqrt((4*3)**2+(4*4)**2)/2

TPBT=0
FPBT=0
FNBT=0
TNBT=0

TPListBT = []
FPListBT = []
FNListBT = []
TNListBT = []

LAExperiments = [0,1,2,3,4]
RAExperiments = [5,6,7]



for experimentIndex,experimentName in enumerate(experimentNames):
    ClassificationResults = np.load(os.path.join(RESULTSPATH,
                                    experimentName+'_ClassificationOutput.npy')).astype(int)
    BTClassifications = (ClassificationResults[:,0]!=1)&(ClassificationResults[:,0]>=2) # np.where((ClassificationResults[:,0]!=1)&(ClassificationResults[:,0]>=2))[0] #there's nothing > 3
    experimentDistances = DistancesPerExperimentBT[experimentName]#[BTClassifications,:]


    for i,row in enumerate(experimentDistances):
        # Because there may be no BTs occasionally, we have inf distances after we stopped
        validPositions = ~np.isnan(row)
        row = row[validPositions]
        TRUE = row<=inRadius 

        # Check if a rotor was simultaneously detected. If so, mark ground truth as FALSE
        rotorPresent = DistancesPerExperimentPS[experimentName][i,validPositions]<=inRadius
        TRUE = TRUE & ~rotorPresent

        if not rotorClassifications[i]: # BT detected or nothing
            POSITIVE = np.zeros(len(row),dtype=bool)
        else:
            # POSITIVE = np.isnan(np.roll(row,-1)) & (~np.isnan(row)) #next one is nan = stopped. No nan = stopped
            # if POSITIVE.sum()==0: 
            POSITIVE = np.zeros(len(row),dtype =bool)
            POSITIVE[-1] = True # it was the shortest one

        # Get catheter positions for experiment
        TPBT += np.sum(TRUE & POSITIVE)
        FPBT += np.sum(~TRUE & POSITIVE)
        FNBT += np.sum(TRUE & ~POSITIVE)
        TNBT += np.sum(~TRUE & ~POSITIVE)

        # Get indices and append to list: experimentIndex, seed and tStep

        [TPListBT.append([experimentIndex,i,ind]) for ind in np.where(TRUE & POSITIVE)[0]]
        [FPListBT.append([experimentIndex,i,ind]) for ind in np.where(~TRUE & POSITIVE)[0]]
        [FNListBT.append([experimentIndex,i,ind]) for ind in np.where(TRUE & ~POSITIVE)[0]]
        [TNListBT.append([experimentIndex,i,ind]) for ind in np.where(~TRUE & ~POSITIVE)[0]]


#%%

print('Sensitivity: %0.3f for BT'%((TPBT)/(TPBT+FNBT)))
print('Specificity: %0.3f for BT'%((TNBT)/(TNBT+FPBT))) #* Not very useful due to high TN
print('Precision: %0.3f for BT'%((TPBT)/(TPBT+FPBT)))
print('Negative predictive value: %0.3f for BT'%((TNBT)/(TNBT+FNBT)))#* Not very useful due to high TN
print('Accuracy: %0.3f for BT'%((TPBT+TNBT)/(TPBT+TNBT+FNBT+FPBT))) #* Not very useful due to high TN


print('FPR: %0.3f for BT\n'%((FPBT)/(FPBT+TNBT)))

print('TRUE BT: %s'%(TPBT+FNBT))
print('TRUE no BT: %s'%(TNBT+FPBT))
print('Algorithm BT: %s'%(TPBT+FPBT))
print('Algorithm no BT: %s'%(FNBT+TNBT))

#%% Step Analysis
# Number of steps required for TP
VicinityRadius = 10 # mm

# Load distances
with open(os.path.join(RESULTSPATH,'%s_DistancesToPS.pkl'%exp),'rb') as file:
    DistancesPerExperimentPS = pickle.load(file)

stepsToVicinity = []
stepsToFind = []
Distances = []
NSteps = []
LAExperiments = [0,1,2,3,4]
RAExperiments = [5,6,7]
for experimentIndex,experimentName in enumerate(experimentNames):
    ClassificationResults = np.load(os.path.join(RESULTSPATH,
                                    experimentName+'_ClassificationOutput.npy')).astype(int)

    if experimentIndex in LAExperiments:
        distances = DistancesPerExperimentPS[experimentName][LA_Seeds,:]
        stepsToFind = ClassificationResults[LA_Seeds,1]
    else:
        distances = DistancesPerExperimentPS[experimentName][RA_Seeds,:]
        stepsToFind = ClassificationResults[RA_Seeds,1]

    [Distances.append(dist) for dist in distances[:,0]]
    [NSteps.append(step) for step in stepsToFind]

    # distances[np.isnan(distances)] = np.inf
    withinVicinity = (distances<=VicinityRadius)
    for i,row in enumerate(withinVicinity):
        try:
            numberOfSteps = np.min(np.where(row)[0])
        except:
            numberOfSteps = np.nan
        stepsToVicinity.append(numberOfSteps) # cm

stepsToVicinity = np.array(stepsToVicinity)
    # for step in numberOfSteps: stepsToVicinity.append(step)
#%% Convert to pandas for easier boxplots

OutputDF = pd.DataFrame()
for experimentIndex,experimentName in enumerate(experimentNames):
    if experimentIndex in LAExperiments:
        distances = DistancesPerExperimentPS[experimentName][LA_Seeds,:]
        # stepsToFind = ClassificationResults[]
    else:
        distances = DistancesPerExperimentPS[experimentName][RA_Seeds,:]

    tmpDict = {}
    tmpDict['experiment'] = np.repeat(experimentName,distances.shape[0])
    for step in range(distances.shape[1]):
        tmpDict['step%02d'%step] = distances[:,step]

    OutputDF = OutputDF.append(pd.DataFrame(tmpDict),ignore_index=True)

missing =np.array(len(OutputDF)-OutputDF.isna().sum())

figure = plt.figure(figsize = [14,8])
OutputDF.boxplot()
ax = plt.gca()
ax.set_xlabel('Step',fontsize = 16)
ax.set_ylabel('Distance from a reentry core (mm)',fontsize = 16)
ax.set_xticks(np.arange(1,len(OutputDF.columns)-1+1))
ax.set_xticklabels(np.arange(0,len(OutputDF.columns)-1))
ax.grid(visible=True,axis='y')
ax.tick_params(labelsize=14)

fig,ax = plt.subplots(1)
ax.plot(missing)

#%% Get distance vs. Steps for True positives

stepPerDistance = np.zeros((len(TPList),2))

for i, [experimentIndex,seed,tStep] in enumerate(TPList):
    initialDistance = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,0]
    stepPerDistance[i] = [initialDistance,tStep]

## Get distances vs. PSs for False negatives (steps until distance < 1 cm)

vicinityThreshold = 10 # mm

# stepPerDistanceVicinity = np.zeros((len(FNList),2))
stepPerDistanceVicinity = np.zeros((len(FNList),2))

for i, [experimentIndex,seed,tStep] in enumerate(FNList):
    distances = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinity[i] = [initialDistance,stepsToVicinity]

stepPerDistanceVicinityTP = np.zeros((len(FNList),2))
for i, [experimentIndex,seed,tStep] in enumerate(TPList):
    distances = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinityTP[i] = [initialDistance,stepsToVicinity]
#%%Linear regression to assess "speed" of tracking

model1 = LinearRegression(fit_intercept=False)
model1.fit(stepPerDistance[:,0].reshape(-1,1),stepPerDistance[:,1])
print(model1.coef_*5)

model2 = LinearRegression(fit_intercept=False)
model2.fit(stepPerDistanceVicinity[:,0].reshape(-1,1),stepPerDistanceVicinity[:,1])
print(model2.coef_*5)

model3 = LinearRegression(fit_intercept=False)
model3.fit(stepPerDistanceVicinityTP[:,0].reshape(-1,1),stepPerDistanceVicinityTP[:,1])
print(model3.coef_*5)

xx = np.arange(0,85,1)

#%%
fig,ax = plt.subplots(1,figsize = [14,8])

ax.plot(np.arange(0,85),np.arange(0,85)/5,'--',color='gray',linewidth = 2)
ax.plot(xx,model1.predict(xx.reshape(-1,1)),'teal',linewidth = 2)
ax.plot(xx,model2.predict(xx.reshape(-1,1)),'darkorange',linewidth = 2)
ax.plot(xx,model3.predict(xx.reshape(-1,1)),'darkorchid',linewidth = 2)

ax.scatter(*stepPerDistance.T,marker='.',s = 150,c='darkslategray',
            label = 'Reentry (true positive)')
ax.scatter(*stepPerDistanceVicinityTP.T,marker='s',s = 50,c='mediumpurple',
            label = '<10mm of reentry (true positives)')#,c=colors)
ax.scatter(*stepPerDistanceVicinity.T,marker='x',s = 50,c='orange',
            label = '<10mm of reentry (false negatives)')#,c=colors)
fig,ax = qv.QuickAdjustPlot(fig,ax,xlabel = 'Initial distance (mm)',
                            ylabel = 'Number of steps',legend=True)

fig.set_figwidth(9)
fig.set_figheight(6)

ax.set_xlim([0,90])
ax.set_ylim([0,30])
fig.tight_layout()


#%% Get distance vs. Steps for True positives (V2, include # of steps)

stepPerDistance = np.zeros((len(TPList),2))

for i, [experimentIndex,seed,tStep] in enumerate(TPList):
    initialDistance = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,0]
    stepPerDistance[i] = [initialDistance,tStep]

## Get distances vs. PSs for False negatives (steps until distance < 1 cm)

vicinityThreshold = 10 # mm

# stepPerDistanceVicinity = np.zeros((len(FNList),2))
stepPerDistanceVicinity = np.zeros((len(FNList),2))

for i, [experimentIndex,seed,tStep] in enumerate(FNList):
    distances = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinity[i] = [initialDistance,stepsToVicinity]

stepPerDistanceVicinityTP = np.zeros((len(TPList),2))
for i, [experimentIndex,seed,tStep] in enumerate(TPList):
    distances = DistancesPerExperimentPS[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinityTP[i] = [initialDistance,stepsToVicinity]

stepPerDistanceVicinity = np.vstack([stepPerDistanceVicinity,stepPerDistanceVicinityTP])
#%%Linear regression to assess "speed" of tracking

model1 = LinearRegression(fit_intercept=False)
model1.fit(stepPerDistance[:,0].reshape(-1,1),stepPerDistance[:,1])
print(model1.coef_*5)

model2 = LinearRegression(fit_intercept=False)
model2.fit(stepPerDistanceVicinity[:,0].reshape(-1,1),stepPerDistanceVicinity[:,1])
print(model2.coef_*5)

xx = np.arange(0,85,1)

#%%
fig,ax = plt.subplots(1,figsize = [14,8])


ax.plot(np.arange(0,85),np.arange(0,85)/5,'--',color='gray',linewidth = 3)
ax.plot(xx,model1.predict(xx.reshape(-1,1)),'teal',linewidth = 3)
ax.plot(xx,model2.predict(xx.reshape(-1,1)),'darkorange',linewidth = 3)

ax.scatter(*stepPerDistance.T,marker='x',s = 60,c='darkslategray',linewidth=3,
            label = 'Steps to detected reentry')
ax.scatter(*stepPerDistanceVicinityTP.T,marker='.',s = 150,c='darkorange',
            label = 'Steps to position in vicinity of reentry (<10mm)')#,c=colors)
fig,ax = qv.QuickAdjustPlot(fig,ax,xlabel = 'Initial distance (mm)',
                            ylabel = 'Number of steps',legend=True)

fig.set_figwidth(9)
fig.set_figheight(6)

ax.set_xlim([0,90])
ax.set_ylim([0,30])
fig.tight_layout()





#%%BT


with open(os.path.join(RESULTSPATH,'%s_DistancesToBT.pkl'%exp),'rb') as file:
    DistancesPerExperimentBT = pickle.load(file)


stepPerDistance = np.zeros((len(TPListBT),2))

for i, [experimentIndex,seed,tStep] in enumerate(TPListBT):
    initialDistance = DistancesPerExperimentBT[experimentNames[experimentIndex]][seed,0]
    stepPerDistance[i] = [initialDistance/10,tStep]

## Get distances vs. PSs for False negatives (steps until distance < 1 cm)

vicinityThreshold = 10 # mm

# stepPerDistanceVicinity = np.zeros((len(FNListBT),2))
stepPerDistanceVicinity = np.zeros((len(FNListBT)+len(TPListBT),2))

for i, [experimentIndex,seed,tStep] in enumerate(FNListBT):
    distances = DistancesPerExperimentBT[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinity[i] = [initialDistance/10,stepsToVicinity]

for i, [experimentIndex,seed,tStep] in enumerate(TPListBT):
    i += len(FNList)
    distances = DistancesPerExperimentBT[experimentNames[experimentIndex]][seed,:]
    initialDistance = distances[0]
    stepsToVicinity = np.where(distances<=vicinityThreshold)[0][0]

    stepPerDistanceVicinity[i] = [initialDistance/10,stepsToVicinity]
colors = np.hstack([np.repeat('orange',len(FNListBT)),np.repeat('b',len(TPListBT))])

#%% FP analysis: where are they ocurring?