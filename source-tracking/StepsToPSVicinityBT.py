#%%
import os,sys
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import multiprocessing as mp
import pickle
from scipy.spatial.distance import cdist,squareform


upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
import IgbHandling as igb



exp = 'EX0008'
RESULTSPATH = '/mnt/d/vgmar/model_data/%s/source_tracking_v3'%exp
BTPATH = '/mnt/d/vgmar/model_data/%s/bt_detection'%exp

#%% Anatomy loading

anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])

#By chamber
interAtrialConnections,CSInds,LAInds,RAInds = igb.GetAnatomicalInds(anatomyPath,(nx,ny,nz),Anatomy)

initialPoints = np.array([20383, 15497, 5654, 1110, 21897, 12919, 24652, 18574, 3218, 21553, 5736,
                          2548, 8756, 22228, 17290, 9391, 9541, 19127, 15307, 12715]) # Determined by getting the center of 20 k-means clusters
LA_Seeds = np.where(np.isin(initialPoints,LAInds))[0] # 0,  1,  4,  5,  6,  7,  9, 13, 14, 17, 18, 19
RA_Seeds = np.where(np.isin(initialPoints,RAInds))[0] # 2,  3,  8, 10, 11, 12, 15, 16

#%% Experiment Names
experimentNames = []
for file in os.listdir(RESULTSPATH):
    if file.startswith(exp) and file.endswith('_ClassificationOutput.npy'):
        experimentNames.append(file.split('_ClassificationOutput.npy')[0])
experimentNames = np.sort(experimentNames)
print(experimentNames)

#%% Average PS positions
BTDF = pd.read_csv(os.path.join(BTPATH,'%s_AverageBTPosition.csv'%exp))
BTDF = BTDF[BTDF.columns[1:]]
maxSegments = np.max(BTDF['segment'])+1

#%%
DistancesPerExperiment = {}

for experimentIndex,experimentName in enumerate(experimentNames):
    expBT = BTDF.loc[BTDF['experiment']==experimentName,:]

    #Load catheter positions per experiment
    indices = pd.read_csv(os.path.join(RESULTSPATH,experimentName+'_logfile.log'),sep=' ')
    catCenterIndices = indices.loc[:,['ip','tStep','pos']]
    
    # This changes the original DF
    for index, row in catCenterIndices.iterrows():
        if row['ip'] in LA_Seeds:
            row['pos'] = LAInds[row['pos']]
        else:
            row['pos'] = RAInds[row['pos']]
        
    experimentDistances = np.ones((len(initialPoints),catCenterIndices['tStep'].max()))*np.nan
    for segment in range(catCenterIndices['tStep'].max()):
        trueSegment = segment%maxSegments
        gridPositionSegment = catCenterIndices.loc[catCenterIndices['tStep']==segment,['ip','pos']]
        gridPositions = Anatomy[gridPositionSegment['pos'],:]

        BTPositionSegment = expBT.loc[expBT['segment']==trueSegment,'anatomyIndex']
        BTPositions = Anatomy[BTPositionSegment,:]
        if len(BTPositions)==0: 
            experimentDistances[gridPositionSegment['ip'],segment] = np.inf
            continue

        distances = cdist(gridPositions,BTPositions)
        distances[distances==0] = np.nan
        minDistances = np.ones(len(initialPoints))*np.nan
        minDistances[gridPositionSegment['ip']] = np.nanmin(distances,axis=1)
        experimentDistances[:,segment] =minDistances
        
    DistancesPerExperiment[experimentName] = experimentDistances

#%% Save
with open(os.path.join(RESULTSPATH,'%s_DistancesToBT.pkl'%exp),'wb') as file:
    pickle.dump(DistancesPerExperiment, file)
