
#%% Load libraries (#TODO IF YOU ARE RUNNING TO SAVE STUFF AGAIN, UNCOMMENT THOSE LINES)
import os,sys
import numpy as np
# import matplotlib.pyplot as plt
# from copy import deepcopy


upperDir = os.path.dirname(os.getcwd())
sys.path.append(os.path.join(upperDir,'aux-functions'))
sys.path.append(os.path.join(upperDir,'recurrence'))

import IgbHandling as igb
from CatheterObjects import CatheterClass,CatheterGroup
import egm_processing as egmp
from DirectionTrackingCalculator import VelocityCalculator,VelocityAnalyzer
from SourceTrackingCalculator import SourceTrackingCalculator
from RecurrenceComputation import RecurrenceComputation
from WaveCalculators import WavemapCalculator,WavemapAnalyzer,WaveClassifier
# %load_ext autoreload 
# %autoreload 2
# %matplotlib qt
#%% Load anatomy and initial points
anatomyPath = '/mnt/d/vgmar/model_data/anatomy/model30Box'
modelName = 'model24-30Box'
FullCell = igb.LoadCellFile(anatomyPath,modelName,[50,51])
nz,ny,nx = FullCell.shape
Anatomy,veIndexes = igb.GetDataIndexes(anatomyPath,modelName,FullCell,validCells = [50,51])
scale = 1 # mm


interAtrialConnections,CSInds,LAInds,RAInds = igb.GetAnatomicalInds(anatomyPath,(nx,ny,nz),Anatomy)


initialPoints = np.array([20383, 15497, 5654, 1110, 21897, 12919, 24652, 18574, 3218, 21553, 5736,
                          2548, 8756, 22228, 17290, 9391, 9541, 19127, 15307, 12715]) # Determined by getting the center of 20 k-means clusters

LA_Seeds = np.where(np.isin(initialPoints,LAInds))[0] # 0,  1,  4,  5,  6,  7,  9, 13, 14, 17, 18, 19
RA_Seeds = np.where(np.isin(initialPoints,RAInds))[0] # 2,  3,  8, 10, 11, 12, 15, 16

#%% Load Data
basename = 'EX0008_FB0_FL200s01'
tBegin = 1000
ve_file = '/mnt/d/vgmar/model_data/EX0008/%sf_ve_egm_endo_epi.igb.gz'%basename

egmDataAll,hdr = igb.Load(ve_file,returnRawSignal=True) # Workaround for memory issues
egmDataAll = egmDataAll.squeeze()
egmDataAll = egmDataAll[tBegin:,veIndexes] # ignore 1s for initialization

#%% Initialize Source and Direction detection calculators
# Direction
vCalc = VelocityCalculator()
vCalc.Radius = np.sqrt(3.5**2+3.5**2)
vAnalyzer = VelocityAnalyzer()

# Sources
STC = SourceTrackingCalculator()

# Recurrence
recComp = RecurrenceComputation()
recComp.SampleShift = 5
recComp.TheilerAFCLNumber = 0.5
recComp.ExpectedAFRecurrenceRate = 1

# Waves
wCalc = WavemapCalculator()
wCalc.NeighborRadius = np.sqrt(2**2+3**2)#*1.05
wCalc.ConductionThreshold = 0.1 # mm/ms
electrodeNeighbors = [[0,1,4,5],[0,1,2,4,5,6],[1,2,3,5,6,7],[2,3,6,7],
                      [0,1,4,5,8,9],[0,1,2,4,5,6,8,9,10],[1,2,3,5,6,7,9,10,11],[2,3,6,7,10,11],
                      [4,5,8,9,12,13],[4,5,6,8,9,10,12,13,14],[5,6,7,9,10,11,13,14,15],[6,7,10,11,14,15],
                      [8,9,12,13],[8,9,10,12,13,14],[9,10,11,13,14,15],[10,11,14,15]]
wCalc.NeighborList = electrodeNeighbors
minimumDuration = 0 #ms

reentryThresholds = {'MaxCurvatureScores':0.4,'CLCoverage':0.1}
focalThresholds = {'NumberOfTrajectories':2,'MinAngles':0.314,
                    'MaxAngles':1.257,'CLCoverage':0.1,
                    'DirectionalPreferentiality':0.7}


#%% Locate sources starting from each loop
# All source tracking algorithms here

Output = np.zeros((len(initialPoints),2)) 
SourceLocations = np.zeros((len(initialPoints),3))
lSegment = 1000
maxSteps = int(egmDataAll.shape[0]/lSegment)
'''
The Output array holds the classification and number of steps for
getting to that result. Possible classification outcomes are:
- 0: Not found in time limit
- 1: rotational source
- 2: focal source
- 3: Source in the other atrium
- 4: enclosed region
- -1: some error

The SourceLocations are the catheter centers when sources were found
'''

outFolder = '/mnt/d/vgmar/model_data/EX0008/source_tracking_v3'

flog = open(os.path.join(outFolder,'%s_logfile.log'%basename),'w')
flog.write('ip tStep pos rr dirPref pSP CLC nTraj avgCurv maxCurv minAngle maxAngle\n')

AllPositions = []

for ip,IP in enumerate(initialPoints):
    # if ip!=3: continue
    print('------------------ %02d ------------------'%ip)

    if ip in LA_Seeds:
        currentAtriumInds = LAInds 
        chosenAnatomy = Anatomy[LAInds]
        IP = np.where(LAInds==IP)[0][0]
        # OtherChamber = RAInds
    elif ip in RA_Seeds:
        currentAtriumInds = RAInds 
        chosenAnatomy = Anatomy[RAInds]
        IP = np.where(RAInds==IP)[0][0]

    nSteps = 0
    dS = 5 # mm to displace the catheter

    #Stuff to store between loops
    previousCatheterPositions   = np.empty((0,3))
    previousCatheterIndexes   = list()
    previousArrowCenters        = np.empty((0,3))
    previousArrowDirections     = np.empty((0,3))
    previousArrowPreferentiality    = np.empty((0,1))
    foundFlag = False
    invalidNext = None
    closedFlag = False
    loop = 1
    tStep = 0

    while True:
        
        print(tStep)
        # if tStep==18: break
        # Set-up catheter
        CatheterObj = CatheterClass('HDGrid-4')

        if tStep==0 and loop==1:
            CatheterObj.GetTissuePatch(IP,chosenAnatomy)
            nextPos = IP
        else:
            CatheterObj.GetTissuePatch(nextPos,chosenAnatomy)

        try:
            CatheterObj.ProjectionFunction(chosenAnatomy,rotationAngle =0)
        except:
            fail = 0
            while fail<5:
                try:
                    CatheterObj.ProjectionFunction(chosenAnatomy,rotationAngle =0)
                    fail = 0
                    break
                except:
                    # # Option 1: get closest point to center
                    # print('%d -> problem placing'%CatheterObj.CatheterCenterIndex)
                    # dists = np.linalg.norm(CatheterObj.Patch-np.array([0,0,0]),axis=1)
                    # newCenterIndex = np.where(dists==np.min(dists[dists!=0]))[0][0]
                    # nextPos = CatheterObj.PatchIndexes[newCenterIndex]
                    # CatheterObj.GetTissuePatch(nextPos,chosenAnatomy)

                    # Option 2: get a random index from the current patch
                    nextPos = np.random.choice(CatheterObj.PatchIndexes)
                    CatheterObj.GetTissuePatch(nextPos,chosenAnatomy)

                fail +=1
            
            if fail!=0:
                Output[ip,:] = [-1,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                foundFlag = True
                break
        # print(nextPos)
        # Signals and activations
        timeInterval = [(tStep%maxSteps)*lSegment, (tStep%maxSteps+1)*lSegment]
        
        CatheterObj.GetSignals(egmDataAll[timeInterval[0]:timeInterval[1],currentAtriumInds],rawInput=True, hdr=hdr)
        CatheterObj.DetectActivations(method = 'adaptiveThreshold')

        ########################################## Recurrence analysis ###########################################
        # distanceMatrix, _, activationCycleLength=recComp.ComputeDistanceMatrix(CatheterObj, CatheterObj.Activations)
        # estimatedAFCL = np.median(activationCycleLength)

        # RP, recurrenceTime, recurrenceThreshold, _ = recComp.ComputeRecurrencePlot(distanceMatrix,
        #                                                                         CatheterObj.SamplingFrequency,
        #                                                                         estimatedAFCL,
        #                                                                         recurrenceThreshold= 0.15)

        # RPE = recComp.ErodeRP(RP,distanceMatrix,linear=True)
        # minLineLength = np.round(estimatedAFCL/recComp.SampleShift)

        # rqa = recComp.ComputeRQA(RPE,estimatedAFCL/1000,CatheterObj.SamplingFrequency,
        #                                             minDiagonalLine = int(minLineLength))
        # print('RR:%0.3f'%rqa['recurrence_rate'])

        ######################################### Direction Calculations #########################################

        allActivations,allPositions,allElectrodeIndices = vCalc.AdjustSignalsCV(CatheterObj,useRealPositions=True)
        xVelocity, yVelocity = vCalc.Compute2DActivationWavefront(allActivations, allPositions)
        ConductionVelocity = vCalc.MakeCVDict(allActivations,allElectrodeIndices,xVelocity,yVelocity)
        intervalPreferentiality = vAnalyzer.AnalyzeVelocity([0,1000],ConductionVelocity)
        
        # Calculate variance of angles
        DirectionalPreferentiality = STC.ComputeDirectionalPreferentiality(intervalPreferentiality)
        Velocities3D = vAnalyzer.ComputeMeanVelocityV2(intervalPreferentiality,vCalc)

        # print('Directional Preferentiality: ',DirectionalPreferentiality)
        ############################################# Source detection ##############################################

        ## PS detection
        CatheterObj.GetPhaseSignal(phaseMethod='sinusoidal')
        STC.DoubleRingPSDetection(CatheterObj)

        # Percentage with any SPs
        psLoc = np.array([STC.PSLocations[i,:,:].any() for i in range(STC.PSLocations.shape[0])])
        PercentageSPs=psLoc.sum()/lSegment
        # print('Percentage SPs: %0.3f'%PercentageSPs)

        ## CL Coverage 
        actGroups = vCalc.GroupValidActivations(allActivations,allPositions,allElectrodeIndices)
        groupKeys = np.array(list(actGroups.keys()))
        actGroups = {key:actGroups[key] for key in groupKeys} 
        estimatedAFCL = np.median([np.median(np.diff(act)) for act in CatheterObj.Activations]) 

        CLCoverage = STC.ComputeCLCoverage(actGroups,estimatedAFCL)
        meanCLCoverage = np.mean(CLCoverage)


        ## Wave analyses 
        activationGroups = actGroups#None #egmp.GroupActivations(CatheterObj.Activations,longestElectrodeAxis = 30,cvThreshold =wCalc.ConductionThreshold)
        waves = wCalc.DetectConnectedActivations(CatheterObj,activationGroups)
        waves = wCalc.RemoveSmallWaves(waves,minimumWaveSize = 8)
        wAnalyzer = WavemapAnalyzer(waves,wCalc.ConductionThreshold,wCalc.NeighborRadius)
        wAnalyzer.MinimumDuration = minimumDuration
        wAnalyzer.ComputeAllAnalyses()

        '''
        The classification is within the WaveClassifier class now. 
        I could not think of a smart way to dinamically change the thresholds, so I set all the current
        thresholds in a dictionary. 
        
        Changing the dictionary also requires changing the code, which is why this is incomplete

        However, everything is in a few lines now
        '''
        wClass = WaveClassifier(wAnalyzer,'mean',reentryThresholds,focalThresholds,
                                CLCoverage=CLCoverage,
                                DirectionalPreferentiality = DirectionalPreferentiality)
        
        WaveAnalysisResults = wClass.ComputeAverageValues()
        ClassificationResultGroup = wClass.WaveClassificationGroup()
        ClassificationResultSingle = wClass.WaveClassificationSingle()
        # if np.sum(np.array(ClassificationResultSingle)==1)>=2:
        #     print('Rotor')
        #     print(ClassificationResultSingle)
        #     break
        # elif np.sum(np.array(ClassificationResultSingle)==2)>=2:
        #     print('Focal')
        #     print(ClassificationResultSingle)
        #     break

        ClassificationResult = ClassificationResultGroup

        if (wAnalyzer.ComputeWaveSizes()>len(CatheterObj.ElectrodesTemplate)).any():
            print('Trajectory Overlap -> rotor')
            ClassificationResult = 'rotor' # FIXME: I think this will be a problem

        # Log the results of the analyses before changing the next position
        flog.write('%d %d %d %f %f %f %f %d %f %f %f %f\n'%(ip,tStep,nextPos,-1,DirectionalPreferentiality, #rqa['recurrence_rate']
                    PercentageSPs,meanCLCoverage,WaveAnalysisResults['NumberOfTrajectories'],WaveAnalysisResults['CurvatureScores'],
                    WaveAnalysisResults['MaxCurvatureScores'],WaveAnalysisResults['MinAngles'],WaveAnalysisResults['MaxAngles'])) #'ip tStep rr dirPref pSP CLC nTraj avgCurv maxCurv minAngle maxAngle'

        # Decide if it is necessary to move the catheter
        if ClassificationResult=='focal' or np.sum(np.array(ClassificationResultSingle)==2)>=2:
            # If it is focal, is it coming from the other atrium?
            if np.isin(currentAtriumInds[CatheterObj.ElectrodeIndexes],interAtrialConnections).any():
                #Then, we have a source in the other atrium
                Output[ip,:] = [3,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                print('%d: Source in other atrium'%ip)
                foundFlag = True
            else:
                # This is a real focal source
                Output[ip,:] = [2,nSteps]
                SourceLocations[ip,:]  = CatheterObj.CatheterCenter
                print('%d: Focal source found'%ip)
                foundFlag = True
        # Rotational activity
        elif ClassificationResult=='rotor' or np.sum(np.array(ClassificationResultSingle)==1)>=2:
            Output[ip,:] = [1,nSteps]
            SourceLocations[ip,:] = CatheterObj.CatheterCenter
            print('%d: Rotational source found'%ip)
            foundFlag = True
        else:
            nextPos,invalidNext,closedFlag,regionIndexes = vAnalyzer.NextCatheterPlacementV2(Velocities3D,CatheterObj,chosenAnatomy,dS,invalidNext)

            if np.isin(currentAtriumInds[nextPos],CSInds):
                # Activation coming from CS, consider as other atrium
                Output[ip,:] = [3,nSteps]
                SourceLocations[ip,:] = CatheterObj.CatheterCenter
                print('%d: Source in other atrium'%ip)
                foundFlag = True
            if closedFlag:
                # print('%d: Valid enclosed region found'%ip)
                Center = np.median(chosenAnatomy[regionIndexes],axis=0)
                DistCenter = np.linalg.norm(chosenAnatomy-Center,axis=1)
                nextPos = np.where(DistCenter==np.min(DistCenter))[0][0]
            if nextPos in previousCatheterIndexes:
                print('Going back and forth:')
                # Option 1: choose median (that's what I have been using so far)
                # Center = np.median(chosenAnatomy[[nextPos,
                #                                 previousCatheterIndexes[-1]]],
                #                                 axis=0)
                # DistCenter = np.linalg.norm(chosenAnatomy-Center,axis=1)
                # nextPos = np.where(DistCenter==np.min(DistCenter))[0][0]

                # Option 2: get a random index from the current patch
                nextPos = np.random.choice(CatheterObj.PatchIndexes)
            # print(nextPos)
            previousCatheterIndexes.append(nextPos)

        ## Clean-up for next loop
        previousCatheterPositions       = np.vstack([previousCatheterPositions,CatheterObj.CatheterCenter])
        previousArrowCenters            = np.vstack([previousArrowCenters,CatheterObj.ElectrodeCoordinates])
        previousArrowDirections         = np.vstack([previousArrowDirections,Velocities3D])
        previousArrowPreferentiality    = np.append(previousArrowPreferentiality,np.array(intervalPreferentiality['value']))
        if foundFlag: break
        
        nSteps += 1
        tStep += 1
        if tStep%maxSteps == 0: loop +=1

    AllPositions.append(previousCatheterPositions)
    if not foundFlag:
        Output[ip,:] = [0,nSteps]
        SourceLocations[ip,:] = CatheterObj.CatheterCenter
        print('%d: No source found'%ip)

    
#%% Close log file, save everything else
flog.close()     

np.save(os.path.join(outFolder,'%s_ClassificationOutput.npy'%basename),Output)
np.save(os.path.join(outFolder,'%s_SourceLocations.npy'%basename),SourceLocations)
np.save(os.path.join(outFolder,'%s_AllCatCenters.npy'%basename),AllPositions)
print(Output)


# %%
